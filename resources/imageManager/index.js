/**
 * Created by wangxu on 2017/11/28.
 * 图片资源管理
 */

/**
 * 语音
 * @type {any|*}
 */
//启动页图片资源
const bgLoginWeChat = require('../../resources/imgs/icon_wechat_login.png');
const bgLoginQQ = require('../../resources/imgs/icon_qq_login.png');
const bgLoginPhone = require('../../resources/imgs/icon_phone_login.png');
const bgLoginFaceBook = require('../../resources/imgs/icon_facebook_login.png');

//voice home
const iconArrow = require('../imgs/bg_arrow_right.png');
const voiceGroupBg=require('../imgs/ic_family_bg.png');
const voiceDefaultGroupIcon=require('../imgs/ic_group_family.png');
const defaultPhoto=require('../imgs/me_photo_default.png');
const defaultUser=require('../imgs/ic_default_user.png');
const icVoiceChildAll=require('../imgs/ic_voice_all.png');
const icVoiceChildSing=require('../imgs/ic_voice_sing.png');
const icVoiceChildChat=require('../imgs/ic_voice_chat.png');
const icVoiceChildLove=require('../imgs/ic_voice_love.png');
const icVoiceChildFate=require('../imgs/ic_voice_fate.png');
const icVoiceChildMerry=require('../imgs/ic_voice_merry.png');
const icVoiceChildSeek=require('../imgs/ic_voice_seek.png');
const icVoiceChildGame=require('../imgs/ic_voice_game.png');
const icVoiceChildUndercover=require('../imgs/ic_voice_undercover.png');
const icVoiceHomeDefault=require('../imgs/voice/ic_voice_home_default.png');
//voice contact
const voiceNewFriend=require('../imgs/voice/ic_contact_request.png');
const voiceMaleIcon=require('../imgs/voice/ic_male.png');
const voiceFemaleIcon=require('../imgs/voice/ic_female.png');
//voice me
const voiceSettingIcon=require('../imgs/voice/ic_voice_setting.png');
const voiceShareIcon=require('../imgs/voice/ic_voice_share.png');
const voiceGroupIcon=require('../imgs/voice/ic_voice_group.png');
const voiceWealthIcon=require('../imgs/voice/ic_voice_wealth.png');
const voiceGiftsIcon=require('../imgs/voice/ic_voice_gifts.png');
const voicePopIcon=require('../imgs/voice/ic_voice_home_pop.png');
//voice room
const bgVoiceHome = require('../imgs/icon_voice_room.png');
const bgRankingList = require('../imgs/ic_ranking_list.png');
const bgVoiceSearch = require('../imgs/icon_voice_search.png');
const selectAudioIcon = require('../imgs/icon_selected_audio.png');
const normalAudioIcon = require('../imgs/icon_normal_audio.png');
const icAllVoiceType = require('../imgs/voice/ic_all_voice_types.png');
const icCloseAllVoiceType = require('../imgs/ic_voice_dismiss.png');
const icDefaultBanner = require('../imgs/voice/ic_default_banner.png');
const icDefaultNewVoiceBg = require('../imgs/voice/ic_default_new_voice_bg.png');
//
const icBack  =require('../../resources/imgs/ic_back.png');
const icVoiceBack  =require('../../resources/imgs/voice/ic_voice_back.png');
//launcher
const launcherXiaoyu  =require('../../resources/imgs/voice/ic_launcher_xiaoyu.png');
const launcherWerewolf  =require('../../resources/imgs/ic_launcher.png');
const icHyperchannel  =require('../../resources/imgs/voice/ic_hyperchannel.png');

/**
 * 游戏
 */
//家族徽标
const ic_group_level_val_1 = require('../../resources/imgs/ico_family_info_default_badge.png');
const ic_group_level_val_2 = require('../../resources/imgs/icon_group_level_val_2.png');
const ic_group_level_val_3 = require('../../resources/imgs/icon_group_level_val_3.png');
const ic_group_level_val_4 = require('../../resources/imgs/icon_group_level_val_4.png');
const ic_group_level_val_5 = require('../../resources/imgs/icon_group_level_val_5.png');
const ic_group_level_val_6 = require('../../resources/imgs/icon_group_level_val_6.png');
const ic_group_level_val_7 = require('../../resources/imgs/icon_group_level_val_7.png');
const ic_group_level_val_8 = require('../../resources/imgs/icon_group_level_val_8.png');
const ic_group_level_val_9 = require('../../resources/imgs/icon_group_level_val_9.png');
const ic_group_level_val_10 = require('../../resources/imgs/icon_group_level_val_10.png');
//home
const icGameHomeHead=require('../../resources/imgs/ic_game_home_head.png');
const icHomeGameLine=require('../../resources/imgs/ic_home_game_line.png');
const icHomeContactLine=require('../../resources/imgs/ic_home_contact_line.png');
const icButton=require('../../resources/imgs/ic_button.png');
const icBorderHearder=require('../../resources/imgs/ic_border_hearder.png');
const icHomeNew=require('../../resources/imgs/ic_home_new.png');
const bgGameModelNew=require('../../resources/imgs/bg_game_model_sd.png');
const bgGameModelAd=require('../../resources/imgs/bg_game_model_ad.png');
const bgGameModelSp=require('../../resources/imgs/bg_game_model_sp.png');
//
const helpDialogTitleBg = require('../../resources/imgs/ic_popup_bg_navbar.png');
const addGold = require('../../resources/imgs/home_icon_add_yellow.png');
const addJewel = require('../../resources/imgs/home_icon_add_blue.png');
const jewelIcon = require('../../resources/imgs/home_icon_diamond.png');
const goldIcon = require('../../resources/imgs/home_icon_coin.png');
const createRoomGold = require('../../resources/imgs/home_icon_coin_new.png');
//首页板子

const iconSearch = require('../../resources/imgs/icon_home_search.png');
const iconCreate = require('../../resources/imgs/icon_home_create.png');

const iconAnnouncementHeader = require('../../resources/imgs/ic_announcement_title.png');
const bgWebViewError = require('../../resources/imgs/icon_webview_error.png');
const iconAnnouncementClose = require('../../resources/imgs/ic_announcement_close.png');
//简单模式
const bgSimeple6 = require('../../resources/imgs/bg_simple_6.png');
const bgSimeple9 = require('../../resources/imgs/bg_simple_9.png');
const bgSimeple9Lock = require('../../resources/imgs/bg_simple_9_lock.png');
const bgSimeple10 = require('../../resources/imgs/bg_simple_10.png');
const bgSimeple10Lock = require('../../resources/imgs/bg_simple_10_lock.png');
const bgSimepleTW6 = require('../../resources/imgs/traditionalImas/bg_simple_6.png');
const bgSimepleTW9 = require('../../resources/imgs/traditionalImas/bg_simple_9.png');
const bgSimepleTW9Lock = require('../../resources/imgs/traditionalImas/bg_simple_9_lock.png');
const bgSimepleTW10 = require('../../resources/imgs/traditionalImas/bg_simple_10.png');
const bgSimepleTW10Lock = require('../../resources/imgs/traditionalImas/bg_simple_10_lock.png');
//标准模式
const bgNormalGuard = require('../../resources/imgs/bg_normal_guard.png');
const bgNormalCupid = require('../../resources/imgs/bg_normal_cupid.png');
const bgNormalGuardTW = require('../../resources/imgs/traditionalImas/bg_normal_guard.png');
const bgNormalCupidTW = require('../../resources/imgs/traditionalImas/bg_normal_cupid.png');
//新手模式
const bgPreSimple = require('../../resources/imgs/bg_pre_simple.png');
const bgPreSimpleNew = require('../../resources/imgs/bg_pre_simple_new.png');
const bgPreSimpleTW = require('../../resources/imgs/traditionalImas/bg_pre_simple.png');
const bgPreSimpleNewTW = require('../../resources/imgs/traditionalImas/bg_pre_simple_new.png');
//高阶模式
const bgHighKing = require('../../resources/imgs/bg_high_king.png');
const bgHighKingTW = require('../../resources/imgs/traditionalImas/bg_high_king.png');
//
const game_arrow_icon = require('../../resources/imgs/icon_home_game_arrow.png');
const game_model_icon = require('../../resources/imgs/icon_game_model_ic.png');
//好友在玩默认图
const defaultFriendStatus = require('../../resources/imgs/default_no_more_status.png');
//所搜弹框资源
//
const searchRoomIcon = require('../../resources/imgs/home_search_room.png');
const searchRoomIconActivity = require('../../resources/imgs/home_search_room_activtiy.png');
const searchUserIcon = require('../../resources/imgs/home_search_contact.png');
const searchUserIconActivity = require('../../resources/imgs/home_search_contact_activtiy.png');
const searchGroupIcon = require('../../resources/imgs/home_search_group.png');
const searchGroupIconActivity = require('../../resources/imgs/home_search_group_activtiy.png');
//用户等级图标资源
const villager=require('../../resources/imgs/level/ic_villager.png');//村民
const village_head=require('../../resources/imgs/level/ic_village_head.png');//老村长
const sheriff=require('../../resources/imgs/level/ic_sheriff.png');//警长
const guard=require('../../resources/imgs/level/ic_guard.png');//守卫
const hunter=require('../../resources/imgs/level/ic_hunter.png');//猎人
const paladin=require('../../resources/imgs/level/ic_paladin.png');//骑士
const witch=require('../../resources/imgs/level/ic_witch.png');//女巫
const seer=require('../../resources/imgs/level/ic_seer.png');//预言家
const priest=require('../../resources/imgs/level/ic_priest.png');//大祭司
const bishop=require('../../resources/imgs/level/ic_bishop.png');//主教
const red_bishop=require('../../resources/imgs/level/ic_red_bishop.png');//红衣主教
const pontiff=require('../../resources/imgs/level/ic_pontiff.png');//教皇
const angel=require('../../resources/imgs/level/ic_angel.png');//天使
const demon=require('../../resources/imgs/level/ic_demon.png');//恶魔
const cupid=require('../../resources/imgs/level/ic_cupid.png');//爱神
const sun_god=require('../../resources/imgs/level/ic_sun_god.png');//太阳神
const pluto=require('../../resources/imgs/level/ic_pluto.png');//冥王
const zeus=require('../../resources/imgs/level/ic_zeus.png');//宙斯

//check image
const defaultCheckBox=require('../../resources/imgs/ic_check_box.png');
const defaultCheckBoxNormal=require('../../resources/imgs/ic_check_box_outline_blank.png.png');
//
const ic_read_message=require('../../resources/imgs/ic_read_message.png');
const ic_delete_bg=require('../../resources/imgs/ic_delete_bg.png');
//语音房间资源
const audioLike = require('../../resources/imgs/icon_audio_like.png');
const audioPop = require('../../resources/imgs/icon_audio_pop.png');
const audioCreateHeader = require('../../resources/imgs/ic_default_audio_create.png');
const defaultNoHeader = require('../../resources/imgs/ic_audio_no_user.png');
const voiceHomeIcon = require('../../resources/imgs/ic_room_icon.png');
const voiceHomeIconLock = require('../../resources/imgs/ic_room_icon_lock.png');
const voiceHomeIconTWLock = require('../../resources/imgs/traditionalImas/ic_room_icon_lock.png');
const voiceUserIcon = require('../../resources/imgs/ic_user_icon.png');
const icDefaultUser = require('../../resources/imgs/ic_default_voice_user.png');
const voiceItemBg = require('../../resources/imgs/voice_item_bg.png');
//我的房间
const icSelectHouse=require('../../resources/imgs/ic_select_house.png');
const icTransferHouse=require('../../resources/imgs/ic_transfer_house.png');
const icHouseJewel=require('../imgs/ic_house_jewel.png');
const myHouseIcon=require('../imgs/ic_my_house.png');
const myHouseDefaultBg=require('../imgs/icon_my_house_deault_bg.png');

//查看房契
const bgHouseCard=require('../../resources/imgs/ic_house_bg.png');
const icHouseCard=require('../../resources/imgs/ic_house_certificate_bg.png');
const icWerewolfSeal=require('../../resources/imgs/ic_werewolf_seal.png');
//mini game
const icMiniGameBack  =require('../../resources/imgs/ic_mini_game_back.png');
const ic_intive_mini_game  =require('../../resources/imgs/miniGame/ic_intive_mini_game.png');
const ic_pk_mini_game  =require('../../resources/imgs/miniGame/ic_pk_mini_game.png');
const ic_mini_game_button_bg  =require('../../resources/imgs/miniGame/ic_mini_game_button_bg.png');
const ic_selected_mini_game  =require('../../resources/imgs/miniGame/ic_selected_mini_game.png');
const ic_default_mini_game_  =require('../../resources/imgs/miniGame/ic_default_mini_game_.png');
const icon_more_mini_game  =require('../../resources/imgs/miniGame/icon_more_mini_game.png');
const ic_basketball  =require('../../resources/imgs/miniGame/ic_basketball.png');
const ic_animal_checker  =require('../../resources/imgs/miniGame/ic_animal_checker.png');
const ic_gobang  =require('../../resources/imgs/miniGame/ic_gobang.png');
const ic_mini_game_list_bg  =require('../../resources/imgs/miniGame/ic_mini_game_list_bg.png');
const icon_enter_mini_game  =require('../../resources/imgs/miniGame/icon_enter_mini_game.png');
const ic_home_mini_game_lineLine  =require('../../resources/imgs/miniGame/ic_home_mini_game_lineLine.png');
const ic_default_mini_game_bg  =require('../../resources/imgs/miniGame/ic_default_mini_game_bg.png');
const ic_mini_game_no_data  =require('../../resources/imgs/ic_mini_game_no_data.png');
//
const ico_like_round_1  =require('../../resources/imgs/like/ico_like_round_1.png');
const ico_like_round_2  =require('../../resources/imgs/like/ico_like_round_2.png');
const ico_like_round_3  =require('../../resources/imgs/like/ico_like_round_3.png');
//国内版本游戏资源
const ic_refresh_more  =require('../../resources/imgs/ic_refresh_more.png');
const ic_undercover_bg  =require('../../resources/imgs/gameHome/ic_undercover_bg.png');
const ic_game_werewolf_enter_bg  =require('../../resources/imgs/gameHome/ic_game_werewolf_enter_bg.png');
const changeFaceBg  =require('../../resources/imgs/gameHome/ic_change_face_enter_bg.png');
const ic_mini_game_green  =require('../../resources/imgs/gameHome/ic_mini_game_green.png');
const ic_mini_game_yellow  =require('../../resources/imgs/gameHome/ic_mini_game_yellow.png');
const ic_mini_game_blue  =require('../../resources/imgs/gameHome/ic_mini_game_blue.png');
const ic_mini_game_pink  =require('../../resources/imgs/gameHome/ic_mini_game_pink.png');
const ic_more_mini_game  =require('../../resources/imgs/gameHome/ic_more_mini_game.png');
const ic_game_animal_fight  =require('../../resources/imgs/gameHome/ic_game_animal_fight.png');
const ic_game_catch_thief  =require('../../resources/imgs/gameHome/ic_game_catch_thief.png');
const ic_game_link_five  =require('../../resources/imgs/gameHome/ic_game_link_five.png');
const ic_mini_home_notification  =require('../imgs/gameHome/ic_mini_home_notification.png');
const ic_mini_home_search  =require('../imgs/gameHome/ic_mini_home_search.png');
const ic_mini_home_store  =require('../imgs/gameHome/ic_mini_home_store.png');
const ic_mini_home_group  =require('../imgs/gameHome/ic_mini_home_group.png');
const ic_mini_home_box  =require('../imgs/gameHome/ic_mini_home_box.png');
const ic_werewolf_advanced  =require('../imgs/gameHome/ic_werewolf_advanced.png');
const ic_werewolf_novice  =require('../imgs/gameHome/ic_werewolf_novice.png');
const ic_werewolf_simple  =require('../imgs/gameHome/ic_werewolf_simple.png');
const ic_werewolf_info_bg  =require('../imgs/gameHome/ic_werewolf_info_bg.png');
const ic_werewolf_model_lock  =require('../imgs/gameHome/ic_werewolf_model_lock.png');
const ic_werewolf_info_bg_shadow  =require('../imgs/gameHome/ic_werewolf_info_bg_shadow.png');


export default {
    changeFaceBg,
    ic_werewolf_model_lock,ic_werewolf_info_bg,ic_werewolf_simple,ic_werewolf_novice,ic_werewolf_advanced,ic_werewolf_info_bg_shadow,
    ic_mini_home_group,ic_mini_home_store,ic_mini_home_search,ic_mini_home_notification,ic_mini_home_box,
    ic_mini_game_green,ic_mini_game_yellow,ic_mini_game_blue,ic_more_mini_game,ic_game_animal_fight,ic_game_catch_thief,ic_game_link_five,ic_mini_game_pink,
    ico_like_round_1,ico_like_round_2,ico_like_round_3,ic_refresh_more,ic_undercover_bg,ic_game_werewolf_enter_bg,
    bgLoginWeChat,myHouseIcon,myHouseDefaultBg,
    voiceHomeIconTWLock,bgHouseCard,icHouseCard,icWerewolfSeal,icMiniGameBack,
    ic_intive_mini_game,ic_pk_mini_game,ic_mini_game_button_bg,ic_selected_mini_game,ic_default_mini_game_,icon_more_mini_game,ic_basketball,ic_animal_checker,
    ic_gobang,ic_mini_game_list_bg,icon_enter_mini_game,ic_home_mini_game_lineLine,ic_default_mini_game_bg,
    bgLoginQQ,ic_mini_game_no_data,
    bgLoginFaceBook,
    bgLoginPhone,
    ic_group_level_val_1,
    ic_group_level_val_2,
    ic_group_level_val_3,
    ic_group_level_val_4,
    ic_group_level_val_5,
    ic_group_level_val_6,
    ic_group_level_val_7,
    ic_group_level_val_8,
    ic_group_level_val_9,
    ic_group_level_val_10,
    iconArrow,
    defaultPhoto,
    voiceGroupBg,
    voiceDefaultGroupIcon,
    voiceNewFriend,
    voiceMaleIcon,
    voiceFemaleIcon,
    voiceSettingIcon,
    voiceShareIcon,
    voiceGroupIcon,
    voiceWealthIcon,
    voiceGiftsIcon,
    defaultUser,
    voicePopIcon,
    bgVoiceHome,
    bgVoiceSearch,
    selectAudioIcon,
    normalAudioIcon,
    icAllVoiceType,
    icVoiceChildSing,
    icVoiceChildChat,
    icVoiceChildLove,
    icVoiceChildFate,
    icVoiceChildMerry,
    icVoiceChildSeek,
    icVoiceChildGame,
    icCloseAllVoiceType,
    icVoiceChildUndercover,
    icBack,
    icVoiceBack,
    icVoiceHomeDefault,
    icDefaultBanner,
    launcherXiaoyu,
    launcherWerewolf,
    icHyperchannel,
    icDefaultNewVoiceBg,
    icGameHomeHead,
    icHomeGameLine,
    icHomeContactLine,
    icBorderHearder,
    icButton,
    icHomeNew,
    bgGameModelNew,
    bgGameModelAd,bgGameModelSp,
    helpDialogTitleBg,
    addGold,
    addJewel,
    jewelIcon,
    goldIcon,
    createRoomGold,
    iconSearch,
    iconCreate,
    iconAnnouncementHeader,
    iconAnnouncementClose ,
    bgWebViewError,
    bgSimeple6,
    bgSimeple9,
    bgSimeple9Lock,
    bgSimeple10,
    bgSimeple10Lock,
    bgSimepleTW6,
    bgSimepleTW9,
    bgSimepleTW9Lock,
    bgSimepleTW10,
    bgSimepleTW10Lock,
    bgNormalGuard,
    bgNormalCupid,
    bgNormalGuardTW,
    bgNormalCupidTW,
    bgPreSimple,
    bgPreSimpleNew,
    bgPreSimpleTW,
    bgPreSimpleNewTW,
    bgHighKing,
    bgHighKingTW,
    game_arrow_icon,
    game_model_icon,
    defaultFriendStatus,
    searchRoomIcon,
    searchRoomIconActivity,
    searchUserIcon,
    searchUserIconActivity,
    searchGroupIcon,ic_read_message,ic_delete_bg,bgRankingList,icSelectHouse,icTransferHouse,icHouseJewel,
    searchGroupIconActivity,icVoiceChildAll,defaultCheckBox,defaultCheckBoxNormal,
    villager,village_head,sheriff,guard,hunter,paladin,witch,seer,priest,bishop,red_bishop,pontiff,angel,demon,cupid,sun_god,pluto,zeus,
    audioLike,audioPop,audioCreateHeader,defaultNoHeader,voiceHomeIcon,voiceHomeIconLock,voiceUserIcon,icDefaultUser,voiceItemBg
};