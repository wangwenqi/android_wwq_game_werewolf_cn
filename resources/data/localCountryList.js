/**
 * Created by wangxu on 2017/12/7.
 */
import Language from '../../resources/language';
const getLocalCountry = (data) => {
    let allCityList = [
        {
            c: "853",
            n: Language().Macao,
            p: "aomentebiehangzhengqu",
            sortLetters: "a"
        },
        {
            c: "61",
            n: Language().Australia,
            p: "aodaliya",
            sortLetters: "a"
        },
        {
            c: "507",
            n: Language().Panama,
            p: "banama",
            sortLetters: "b"
        },
        {
            c: "82",
            n: Language().Korea,
            p: "hanguo",
            sortLetters: "h"
        },
        {
            c: "855",
            n: Language().Cambodia,
            p: "jianpuzhai",
            sortLetters: "j"
        },
        {
            c: "60",
            n:Language().malaysia,
            p: "malaixiya",
            sortLetters: "m"
        },
        {
            c: "1",
            n: Language().US,
            p: "meiguo",
            sortLetters: "m"
        },
        {
            c: "265",
            n:Language().Malawi,
            p: "malawei",
            sortLetters: "m"
        },
        {
            c: "81",
            n: Language().Japan,
            p: "riben",
            sortLetters: "r"
        },
        {
            c: "886",
            n:Language().taiwan,
            p: "taiwan",
            sortLetters: "t"
        },
        {
            c: "66",
            n: Language().Thailand,
            p: "taiguo",
            sortLetters: "t"
        },
        {
            c: "852",
            n:Language().hongkong,
            p: "xianggangtebiehangzhengqu",
            sortLetters: "x"
        },
        {
            c: "65",
            n: Language().singapore,
            p: "xinjiapo",
            sortLetters: "x"
        },
        {
            c: "64",
            n:Language().new_Zealand,
            p: "xinxilan",
            sortLetters: "x"
        },
        {
            c: "62",
            n:Language().Indonesia,
            p: "yindunixiya",
            sortLetters: "y"
        },
        {
            c: "86",
            n: Language().china,
            p: "zhongguo",
            sortLetters: "z"
        }
    ];
    data(allCityList);
}
export default {
    getLocalCountry
};