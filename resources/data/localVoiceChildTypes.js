/**
 * Created by wangxu on 2017/12/7.
 */
/**
 * Created by wangxu on 2017/11/14.
 */
import Language from '../../resources/language';
import ImageManager from '../../resources/imageManager';
import * as Constant from '../../support/common/constant';
const getLocalVoiceTypeList = (data) => {
    let list = [
        {
            name: Language().voice_type_all,
            image: ImageManager.icVoiceChildAll
        },
        {
            name: Language().voice_type_sing,
            image: ImageManager.icVoiceChildSing
        },
        {
            name: Language().voice_type_chat,
            image: ImageManager.icVoiceChildChat
        },
        {
            name: Language().voice_type_love,
            image: ImageManager.icVoiceChildLove
        }, {
            name: Language().voice_type_undercover,
            image: ImageManager.icVoiceChildUndercover
        }, {
            name: Language().voice_type_fate,
            image: ImageManager.icVoiceChildFate
        }, {
            name: Language().voice_type_merry,
            image: ImageManager.icVoiceChildMerry
        }, {
            name: Language().voice_type_seek,
            image: ImageManager.icVoiceChildSeek
        }, {
            name: Language().voice_type_game,
            image: ImageManager.icVoiceChildGame
        }
    ]

    data(list);
}
export default {
    getLocalVoiceTypeList
};