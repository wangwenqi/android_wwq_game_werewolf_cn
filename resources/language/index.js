/**
 * Created by vellee on 22/02/2017.
 */

import zh_CN from './zh_CN';
import zh_TW from './zh_TW';
import zh_HK from './zh_HK';
import en_US from './en_US';
import ja_JP from './ja_JP';
import ko_KR from './ko_KR';
import DeviceInfo from 'react-native-device-info';
const ZH = 'zh-CN';
const TW = 'zh-TW';
const HK = 'zh-HK';
const ZH_IOS = 'zh-Hans-CN';//(简体中文)
const ZH_HANT_IOS = 'zh-Hant-CN';//(繁体中文)
const TW_IOS = 'zh-Hant-TW';//(繁体中文-台湾）
const HK_IOS = 'zh-Hant-HK';//(繁体中文-香港)
const JA_JP = 'ja-JP';//日文
const JA_JP_IOS = 'ja';//日文iOS
const KO_KR = 'ko-KR';//韩文
let lg = DeviceInfo.getDeviceLocale();
export default function language() {
    switch (lg) {
        case ZH:
        case ZH_IOS:
            return zh_CN;
        case TW:
        case TW_IOS:
            return zh_TW;
        case HK:
        case HK_IOS:
            return zh_HK;
        case ZH_HANT_IOS:
            return zh_TW;
        case JA_JP:
        case JA_JP_IOS:
            //return ja_JP;//暂时适配成简体中文
            return zh_CN;
        case KO_KR:
            return zh_CN;//暂时适配成简体中文
        default:
            return zh_CN;
    }
    return language;
}