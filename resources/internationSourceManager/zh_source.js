/**
 * Created by wangxu on 2018/1/25.
 */
import ImageManager from '../../resources/imageManager';
export default zh_source = {
    simple9LockSource:ImageManager.bgSimeple9Lock,
    simple9Source:ImageManager.bgSimeple9,
    simple10LockSource:ImageManager.bgSimeple10Lock,
    simple10Source:ImageManager.bgSimeple10,
    simple6Source:ImageManager.bgSimeple6,
    preSimpleSource:ImageManager.bgPreSimple,
    preSimpleNewSource:ImageManager.bgPreSimpleNew,
    normalCupidSource:ImageManager.bgNormalCupid,
    normalGuardSource:ImageManager.bgNormalGuard,
    highKingSource:ImageManager.bgHighKing,
    icRoomIconLock:ImageManager.voiceHomeIconLock,
}