/**
 * Created by wangxu on 2018/1/25.
 */
import ImageManager from '../../resources/imageManager';
export default tr_source = {
    simple9LockSource:ImageManager.bgSimepleTW9Lock,
    simple9Source:ImageManager.bgSimepleTW9,
    simple10LockSource:ImageManager.bgSimepleTW10Lock,
    simple10Source:ImageManager.bgSimepleTW10,
    simple6Source:ImageManager.bgSimepleTW6,
    preSimpleSource:ImageManager.bgPreSimpleTW,
    preSimpleNewSource:ImageManager.bgPreSimpleNewTW,
    normalCupidSource:ImageManager.bgNormalCupidTW,
    normalGuardSource:ImageManager.bgNormalGuardTW,
    highKingSource:ImageManager.bgHighKingTW,
    icRoomIconLock:ImageManager.voiceHomeIconTWLock,
}