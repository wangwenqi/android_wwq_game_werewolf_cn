/**
 * Created by wangxu on 2018/1/25.
 */
import ImageManagerJP from '../../resources/imageManager/jpImageManager';
import ImageManagerZh from '../../resources/imageManager';
export default ja_source = {
    simple9LockSource:ImageManagerJP.bgSimeple9Lock,
    simple9Source:ImageManagerJP.bgSimeple9,
    simple10LockSource:ImageManagerJP.bgSimeple10Lock,
    simple10Source:ImageManagerJP.bgSimeple10,
    simple6Source:ImageManagerJP.bgSimeple6,
    preSimpleSource:ImageManagerJP.bgPreSimple,
    preSimpleNewSource:ImageManagerJP.bgPreSimpleNew,
    normalCupidSource:ImageManagerJP.bgNormalCupid,
    normalGuardSource:ImageManagerJP.bgNormalGuard,
    highKingSource:ImageManagerJP.bgHighKing,
    icRoomIconLock:ImageManagerJP.voiceHomeIconJPLock,
}