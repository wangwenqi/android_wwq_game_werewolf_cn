/**
 * Created by wangxu on 2018/1/25.
 */
import DeviceInfo from 'react-native-device-info';
import  * as Constant from '../../support/common/constant';
import ja_source from './ja_source';
import tr_source from './tr_source';
import zh_source from './zh_source';

let lg = DeviceInfo.getDeviceLocale();
export default function imageSource() {
    switch (lg) {
        case Constant.ZH:
        case Constant.ZH_IOS:
            return zh_source;
        case Constant.TW:
        case Constant.TW_IOS:
        case Constant.HK:
        case Constant.HK_IOS:
        case Constant.ZH_HANT_IOS:
            return tr_source;
        case Constant.JA_JP:
        case Constant.JA_JP_IOS:
            //return ja_source;
            return zh_source;//暂时适配成简体中文
        default:
            return zh_source;
    }
    return imageSource;
}