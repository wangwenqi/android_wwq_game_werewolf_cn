package cn.orangelab.werewolfChina.wxapi;

import android.app.Activity;
import android.os.Bundle;

import orangelab.project.common.bridge.channel.WeChatModule;


public class WXEntryActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WeChatModule.handleIntent(getIntent());
        finish();
    }
}
