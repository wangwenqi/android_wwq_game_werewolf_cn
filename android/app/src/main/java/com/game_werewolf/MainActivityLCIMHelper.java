package com.game_werewolf;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxBus;
import com.avos.avoscloud.im.v2.AVIMTypedMessage;
import com.avos.avoscloud.im.v2.messages.AVIMAudioMessage;
import com.avos.avoscloud.im.v2.messages.AVIMImageMessage;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.datasource.GlobalUserState;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.toolkit.action.Destroyable;
import com.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import orangelab.project.common.activity.LaunchActivity;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.config.GlobalConfig;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.exhibition.ExhibitionPackage;
import orangelab.project.common.exhibition.card.CardEvent;
import orangelab.project.common.exhibition.gift.GiftEvent;
import orangelab.project.common.exhibition.gift.GiftsSupervisor;
import orangelab.project.common.family.FamilyTools;
import orangelab.project.common.manager.AppManager;
import orangelab.project.common.manager.BlackListManager;
import orangelab.project.common.model.EnterRoomPayload;
import orangelab.project.common.system.SystemConstant;
import orangelab.project.common.system.SystemController;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.LeanCloudChatHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.thirdparty.leancloud.chatkit.event.ChatIntviuEvent;
import orangelab.thirdparty.leancloud.chatkit.event.FromChatEvent;
import orangelab.thirdparty.leancloud.chatkit.event.IntviuRoomEvent;
import orangelab.thirdparty.leancloud.chatkit.event.LCIMIMTypeMessageEvent;
import orangelab.thirdparty.leancloud.chatkit.event.PersonalInfoEvent;
import orangelab.thirdparty.leancloud.chatkit.handler.LCIMMessageInterceptor;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMGiftMessage;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMIntviuMessage;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMMiniGameMessage;
import orangelab.thirdparty.leancloud.chatkit.utils.Constant;
import orangelab.thirdparty.leancloud.chatkit.utils.NotificationUtils;
import rx.Subscription;

/**
 * game_werewolf
 * 2017/11/27 下午1:03
 * Mystery
 * <p>
 * 用于处理所有的 LeanCloud 消息
 */

public class MainActivityLCIMHelper implements Destroyable, LCIMMessageInterceptor.Interceptor {

    private static final String TAG = "MainActivity";

    private Context mContext;

    public MainActivityLCIMHelper(Context context) {
        mContext = context;
        EventBus.getDefault().register(this);
        initOutSideExhibitionEvent();
        initLeanCloudMessageIntercept();
    }

    private void initLeanCloudMessageIntercept() {
        LCIMMessageInterceptor.Register(this);
    }

    @Override
    public void destroy() {
        EventBus.getDefault().unregister(this);
        RxBus.getInstance().unregister(this);
        LCIMMessageInterceptor.UnRegister(this);
    }

    private void sendPresentMessage(String type, ExhibitionPackage exhibitionPackage) {
        sendPresentMessage(type, "", exhibitionPackage);
    }

    private void sendPresentMessage(String type, String rebate, ExhibitionPackage exhibitionPackage) {
        LeanCloudChatHelper.sendGiftMessageIntoConversationAsync(
                type,
                exhibitionPackage.getToUserData().userId,
                exhibitionPackage.getToUserData().userName,
                exhibitionPackage.getToUserData().userImageUrl,
                exhibitionPackage.getToUserData().userSex,
                rebate
        );
    }

    private void initOutSideExhibitionEvent() {
        Subscription gift = RxBus.wrapObservableSafely(RxBus.turnToObservable(GiftEvent.GiftForOutSideWrapperEvent.class), giftForMainPageWrapperEvent -> {
            sendPresentMessage(giftForMainPageWrapperEvent.getGift().getGiftType(), giftForMainPageWrapperEvent.getExhibitionPackage());
        });
        RxBus.getInstance().register(this, gift);
        Subscription card = RxBus.wrapObservableSafely(RxBus.turnToObservable(CardEvent.CardForOutSideWrapperEvent.class), cardForMainPageWrapperEvent -> {
            sendPresentMessage(cardForMainPageWrapperEvent.getCard().getRealType(), cardForMainPageWrapperEvent.getExhibitionPackage());
        });
        RxBus.getInstance().register(this, card);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PersonalInfoEvent event) {
        PersonalInfoActivity.Launch(event.getContext(), event.getUserId());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FromChatEvent fromChatEvent) {
        if (fromChatEvent != null) {
            IntviuRoomEvent event = fromChatEvent.event;
            EnterRoomPayload payload = new EnterRoomPayload();
            payload.password = event.getRoomPassword();
            payload.gameType = event.getRoomGameType();
            payload.roomId = event.getRoomId();
            payload.userName = GlobalUserState.getGlobalState().getUserName();
            payload.userId = GlobalUserState.getGlobalState().getUserId();
            payload.userSex = GlobalUserState.getGlobalState().getUserSex();
            payload.avatar = GlobalUserState.getGlobalState().getUserIcon();
            payload.exp = GlobalUserState.getGlobalState().getUserExp();
            payload.token = GlobalUserState.getGlobalState().getToken();
            Intent intent = new Intent(fromChatEvent.activity, LaunchActivity.class);
            IntentDataHelper.setIntentType(intent, IntentDataHelper.TYPE_EVENT_ROOM_FROM_CHAT);
            IntentDataHelper.setEnterRoomPayload(intent, payload);
            fromChatEvent.activity.startActivity(intent);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LCIMIMTypeMessageEvent event) {
        PLog.i(TAG, "onEvent: " + event);
        try {
            if (event != null) {
                AVIMTypedMessage message = event.message;
                if (message instanceof AVIMIntviuMessage) {
                    AVIMIntviuMessage temp = (AVIMIntviuMessage) message;
                    String chatMessage = temp.getText();
                    String userId = "";
                    String userName = "";
                    String userSex = "0";
                    String userIcon = "";
                    String msgType = "";
                    boolean isRead = false;
                    if (temp.getAttrs() != null) {
                        userId = (String) temp.getAttrs().get(Constant.USER_ID);
                        userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                        userSex = (String) temp.getAttrs().get(Constant.USER_SEX);
                        userIcon = (String) temp.getAttrs().get(Constant.USER_ICON);
                        msgType = (String) temp.getAttrs().get(Constant.MSG_TYPE);
                        try {
                            isRead = (boolean) temp.getAttrs().get(Constant.READ);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (FamilyTools.IsFamilyMsg(msgType) && !isRead) {
                            isRead = filterFamilyMsg(msgType);
                        }
                    }
                    WritableMap map = Arguments.createMap();
                    map.putString(Constant.MSG_TYPE, msgType);
                    map.putString(Constant.CHAT_MESSAGE, chatMessage);
                    map.putString(Constant.MESSAGE_TYPE, Constant.MESSAGE_TYPE_CHAT);
                    map.putString(Constant.USER_ID, userId);
                    map.putString(Constant.USER_NAME, userName);
                    map.putString(Constant.USER_SEX, userSex);
                    map.putString(Constant.USER_ICON, userIcon);
                    map.putString(Constant.CONVERSATION_ID, event.conversation.getConversationId());
                    map.putBoolean(Constant.READ, isRead);
                    RoomSocketEngineHelper.sendEventToRn(Constant.EVENT_CHAT, map);
                } else if (message instanceof AVIMMiniGameMessage) {
                    try {
                        AVIMMiniGameMessage miniGameMessage = (AVIMMiniGameMessage) message;
                        String chatMessage = miniGameMessage.getText();
                        if (TextUtils.isEmpty(chatMessage)) {
                            chatMessage = MessageUtils.getString(R.string.mini_game_intviu);
                        }
                        String userId = "";
                        String userName = "";
                        String userSex = "0";
                        String userIcon = "";
                        String msgType = "";
                        boolean isRead = false;
                        String intent = "";
                        if (miniGameMessage.getAttrs() != null) {
                            userId = (String) miniGameMessage.getAttrs().get(Constant.USER_ID);
                            userName = (String) miniGameMessage.getAttrs().get(Constant.USER_NAME);
                            userSex = (String) miniGameMessage.getAttrs().get(Constant.USER_SEX);
                            userIcon = (String) miniGameMessage.getAttrs().get(Constant.USER_ICON);
                            msgType = (String) miniGameMessage.getAttrs().get(Constant.MSG_TYPE);
                            try {
                                isRead = (boolean) miniGameMessage.getAttrs().get(Constant.READ);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                intent = (String) miniGameMessage.getAttrs().get(Constant.INTENT);
                            } catch (Exception e) {
                                //we ignore this  exception
                            }
                            if (FamilyTools.IsFamilyMsg(msgType) && !isRead) {
                                isRead = filterFamilyMsg(msgType);
                            }
                        }
                        WritableMap map = Arguments.createMap();
                        map.putString(Constant.CHAT_MESSAGE, chatMessage);
                        map.putString(Constant.MESSAGE_TYPE, Constant.MESSAGE_TYPE_CHAT);
                        map.putString(Constant.USER_ID, userId);
                        map.putString(Constant.USER_NAME, userName);
                        map.putString(Constant.USER_SEX, userSex);
                        map.putString(Constant.USER_ICON, userIcon);
                        map.putString(Constant.MSG_TYPE, msgType);
                        map.putBoolean(Constant.READ, isRead);
                        map.putString(Constant.INTENT, intent);
                        map.putString(Constant.CONVERSATION_ID, event.conversation.getConversationId());
                        RoomSocketEngineHelper.sendEventToRn(Constant.EVENT_CHAT, map);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (message instanceof AVIMTextMessage) {
                    AVIMTextMessage temp = (AVIMTextMessage) message;
                    String chatMessage = temp.getText();
                    String userId = "";
                    String userName = "";
                    String userSex = "0";
                    String userIcon = "";
                    String msgType = "";
                    boolean isRead = false;
                    String intent = "";
                    if (temp.getAttrs() != null) {
                        userId = (String) temp.getAttrs().get(Constant.USER_ID);
                        userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                        userSex = (String) temp.getAttrs().get(Constant.USER_SEX);
                        userIcon = (String) temp.getAttrs().get(Constant.USER_ICON);
                        msgType = (String) temp.getAttrs().get(Constant.MSG_TYPE);
                        try {
                            isRead = (boolean) temp.getAttrs().get(Constant.READ);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            intent = (String) temp.getAttrs().get(Constant.INTENT);
                        } catch (Exception e) {
                            //we ignore this  exception
                        }
                        if (FamilyTools.IsFamilyMsg(msgType) && !isRead) {
                            isRead = filterFamilyMsg(msgType);
                        }
                    }
                    WritableMap map = Arguments.createMap();
                    map.putString(Constant.CHAT_MESSAGE, chatMessage);
                    map.putString(Constant.MESSAGE_TYPE, Constant.MESSAGE_TYPE_CHAT);
                    map.putString(Constant.USER_ID, userId);
                    map.putString(Constant.USER_NAME, userName);
                    map.putString(Constant.USER_SEX, userSex);
                    map.putString(Constant.USER_ICON, userIcon);
                    map.putString(Constant.MSG_TYPE, msgType);
                    map.putBoolean(Constant.READ, isRead);
                    map.putString(Constant.INTENT, intent);
                    map.putString(Constant.CONVERSATION_ID, event.conversation.getConversationId());
                    RoomSocketEngineHelper.sendEventToRn(Constant.EVENT_CHAT, map);
                } else if (message instanceof AVIMAudioMessage) {
                    AVIMAudioMessage temp = (AVIMAudioMessage) message;
                    String chatMessage = "语音";
                    String userId = "";
                    String userName = "";
                    String userSex = "0";
                    String userIcon = "";
                    boolean isRead = false;
                    String msgType = "";
                    if (temp.getAttrs() != null) {
                        userId = (String) temp.getAttrs().get(Constant.USER_ID);
                        userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                        userSex = (String) temp.getAttrs().get(Constant.USER_SEX);
                        userIcon = (String) temp.getAttrs().get(Constant.USER_ICON);
                        msgType = (String) temp.getAttrs().get(Constant.MSG_TYPE);
                        try {
                            isRead = (boolean) temp.getAttrs().get(Constant.READ);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (FamilyTools.IsFamilyMsg(msgType) && !isRead) {
                            isRead = filterFamilyMsg(msgType);
                        }
                    }
                    WritableMap map = Arguments.createMap();
                    map.putString(Constant.CHAT_MESSAGE, chatMessage);
                    map.putString(Constant.MESSAGE_TYPE, Constant.MESSAGE_TYPE_CHAT);
                    map.putString(Constant.USER_ID, userId);
                    map.putString(Constant.USER_NAME, userName);
                    map.putString(Constant.USER_SEX, userSex);
                    map.putString(Constant.USER_ICON, userIcon);
                    map.putString(Constant.MSG_TYPE, msgType);
                    map.putString(Constant.CONVERSATION_ID, event.conversation.getConversationId());
                    map.putBoolean(Constant.READ, isRead);
                    RoomSocketEngineHelper.sendEventToRn(Constant.EVENT_CHAT, map);
                } else if (message instanceof AVIMImageMessage) {
                    AVIMImageMessage temp = (AVIMImageMessage) message;
                    String chatMessage = "图片";
                    String userId = "";
                    String userName = "";
                    String userSex = "0";
                    String userIcon = "";
                    String msgType = "";
                    boolean isRead = false;
                    if (temp.getAttrs() != null) {
                        userId = (String) temp.getAttrs().get(Constant.USER_ID);
                        userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                        userSex = (String) temp.getAttrs().get(Constant.USER_SEX);
                        userIcon = (String) temp.getAttrs().get(Constant.USER_ICON);
                        msgType = (String) temp.getAttrs().get(Constant.MSG_TYPE);
                        try {
                            isRead = (boolean) temp.getAttrs().get(Constant.READ);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (FamilyTools.IsFamilyMsg(msgType) && !isRead) {
                            isRead = filterFamilyMsg(msgType);
                        }
                    }
                    WritableMap map = Arguments.createMap();
                    map.putString(Constant.CHAT_MESSAGE, chatMessage);
                    map.putString(Constant.MESSAGE_TYPE, Constant.MESSAGE_TYPE_CHAT);
                    map.putString(Constant.USER_ID, userId);
                    map.putString(Constant.USER_NAME, userName);
                    map.putString(Constant.USER_SEX, userSex);
                    map.putString(Constant.USER_ICON, userIcon);
                    map.putString(Constant.MSG_TYPE, msgType);
                    map.putString(Constant.CONVERSATION_ID, event.conversation.getConversationId());
                    map.putBoolean(Constant.READ, isRead);
                    RoomSocketEngineHelper.sendEventToRn(Constant.EVENT_CHAT, map);
                } else if (message instanceof AVIMGiftMessage) {
                    Utils.runSafely(() -> {
                        AVIMGiftMessage temp = (AVIMGiftMessage) message;
                        String giftType = (String) temp.getAttrs().get(Constant.GIFT_TYPE);
                        String chatMessage = temp.getText();
                        if (!GiftsSupervisor.SupportThisGiftType(giftType)) {
                            /**
                             * 如果发送的礼物类型为空，那么就按照文本中的显示，如果发送的礼物类型不为空并且也不支持此类型，那么就展示不支持此类型文案，因为如果是对方送的礼物，giftType
                             * 不会为空，如果是自己送的礼物那么giftType就会为空
                             */
                            chatMessage = MessageUtils.getString(R.string.string_chat_gift_temp_unsupported);
                        }
                        String userId = "";
                        String userName = "";
                        String userSex = "0";
                        String userIcon = "";
                        String msgType = "";
                        boolean isRead = false;
                        if (temp.getAttrs() != null) {
                            userId = (String) temp.getAttrs().get(Constant.USER_ID);
                            userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                            userSex = (String) temp.getAttrs().get(Constant.USER_SEX);
                            userIcon = (String) temp.getAttrs().get(Constant.USER_ICON);
                            if (FamilyTools.IsFamilyMsg(msgType)) {
                                return;
                            }
                            try {
                                isRead = (boolean) temp.getAttrs().get(Constant.READ);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (FamilyTools.IsFamilyMsg(msgType) && !isRead) {
                                isRead = filterFamilyMsg(msgType);
                            }
                        }
                        WritableMap map = Arguments.createMap();
                        map.putString(Constant.CHAT_MESSAGE, chatMessage);
                        map.putString(Constant.MESSAGE_TYPE, Constant.MESSAGE_TYPE_CHAT);
                        map.putString(Constant.USER_ID, userId);
                        map.putString(Constant.USER_NAME, userName);
                        map.putString(Constant.USER_SEX, userSex);
                        map.putString(Constant.USER_ICON, userIcon);
                        map.putString(Constant.CONVERSATION_ID, event.conversation.getConversationId());
                        map.putBoolean(Constant.READ, isRead);
                        RoomSocketEngineHelper.sendEventToRn(Constant.EVENT_CHAT, map);
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            /**
             * 在转换消息时出了问题，那么不在向RN层传递消息
             */
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ChatIntviuEvent event) {
        try {
            if (TextUtils.equals(event.userId, GlobalUserState.getGlobalState().getCurChatUserId())) {
                return;
            }
            Intent startIntent = new Intent(mContext, LaunchActivity.class);
            IntentDataHelper.setIntentType(startIntent, IntentDataHelper.TYPE_NOTIFACTION_FROM_INTVIU);
            IntentDataHelper.setRoomPassword(startIntent, event.roomPassword);
            IntentDataHelper.setRoomId(startIntent, event.roomId);
            int notificationId = event.roomId.hashCode();

            PendingIntent intent = PendingIntent.getActivity(mContext, 0, startIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

            android.support.v7.app.NotificationCompat.Builder mBuilder = new android.support.v7.app.NotificationCompat.Builder(mContext);

            mBuilder.setSmallIcon(R.drawable.notification_small_icon);

            Notification notification = mBuilder.build();

            RemoteViews contentView = new RemoteViews(mContext.getPackageName(), R.layout.layout_notification_chat);

            if (event.isFamilyMsg) {
                contentView.setTextViewText(R.id.notification_title, MessageUtils.getString(R.string.string_family_msg_hint));
            } else {
                contentView.setTextViewText(R.id.notification_title, MessageUtils.getString(R.string.app_name));
            }

            contentView.setImageViewResource(R.id.notification_image, AppManager.GetAppIcon());

            contentView.setTextViewText(R.id.notification_text, "[" + event.userName + "]" + ":" + event.message);

            notification.contentView = contentView;
            notification.flags = Notification.FLAG_AUTO_CANCEL;
            notification.when = System.currentTimeMillis();
            notification.contentIntent = intent;
            notificationManager.notify(notificationId, notification);
        } catch (Exception e) {
        }
    }


    private boolean filterFamilyMsg(String msgType) {
        return TextUtils.equals(msgType, Constant.MSG_TYPE_FAMILY) && GlobalConfig.GetGroupNoticeDisable(mContext);
    }

    @Override
    public boolean intercept(String conversationId, AVIMTypedMessage message) {
        if (TextUtils.isEmpty(conversationId)) {
            /**
             * 会话为空或者其他异常数据直接过滤掉
             */
            return true;
        }

        if (message instanceof AVIMIntviuMessage) {
            return handleInviteMessage(conversationId, (AVIMIntviuMessage) message);
        } else if (message instanceof AVIMMiniGameMessage) {
            return handleMiniGameMessage(conversationId, (AVIMMiniGameMessage) message);
        } else if (message instanceof AVIMTextMessage) {
            return handleTextMessage(conversationId, (AVIMTextMessage) message);
        } else if (message instanceof AVIMAudioMessage) {
            return handleAudioMessage(conversationId, (AVIMAudioMessage) message);
        } else if (message instanceof AVIMImageMessage) {
            return handleImageMessage(conversationId, (AVIMImageMessage) message);
        } else if (message instanceof AVIMGiftMessage) {
            return handleGiftMessage(conversationId, (AVIMGiftMessage) message);
        } else {
            /**
             * 其他数据类型直接过滤掉
             */
            return true;
        }
    }

    private boolean handleMiniGameMessage(String conversationId, AVIMMiniGameMessage message) {
        try {
            /**
             * 基本文本消息类型
             */
            String userId = (String) message.getAttrs().get(Constant.USER_ID);
            String userName = (String) message.getAttrs().get(Constant.USER_NAME);
            String userSex = (String) message.getAttrs().get(Constant.USER_SEX);
            String userIvUrl = (String) message.getAttrs().get(Constant.USER_ICON);
            final String msgType = (String) message.getAttrs().get(Constant.MSG_TYPE);
            String app = (String) message.getAttrs().get(Constant.APP);
            String chatMessage = message.getText();
            if (TextUtils.isEmpty(chatMessage)) {
                chatMessage = MessageUtils.getString(R.string.mini_game_intviu);
            }
            if (TextUtils.isEmpty(app)) {
                if (AppManager.IsWereWolf()) {
                    /**
                     * ignore
                     */
                }
                if (AppManager.IsXiaoYu()) {
                    return true;
                }
            } else {
                if (TextUtils.equals(app, AppManager.GetApp())) {
                    /**
                     * 同一个应用
                     */
                } else {
                    return true;
                }
            }

            if (filter(userId)) {
                return true;
            }

            if (filterFamilyMsg(msgType)) {
                return false;
            }
            /**
             * 分析文本消息类型
             */
            Utils.runSafely(() -> {
                long startTimel = (long) message.getAttrs().get(SystemConstant.START_TIME);
                long endTimel = (long) message.getAttrs().get(SystemConstant.END_TIME);
                if (msgType.contains(SystemConstant.MSG_TYPE_NOPLAY)) {
                    SystemController.updateNoPlayTime(
                            GlobalUserState.getGlobalState().getUserId(),
                            startTimel,
                            endTimel
                    );
                }
                Log.i(TAG, "onMessage: startTime=" + startTimel + ",endTime=" + endTimel + " msgType=" + msgType);
            });

            if (!GlobalUserState.getGlobalState().isGaming() && !TextUtils.equals(conversationId, GlobalUserState.getGlobalState().getCurConversationId())) {
                NotificationUtils.IMPL().onChatNotification(
                        conversationId,
                        mContext,
                        userId,
                        userName,
                        Integer.parseInt(userSex),
                        userIvUrl,
                        chatMessage,
                        FamilyTools.IsFamilyMsg(msgType)
                );
            } else {
                NotificationUtils.IMPL().clearNotification(mContext, userId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        return false;
    }


    private boolean filter(String userId) {
        return BlackListManager.Has(userId);
    }

    private boolean handleInviteMessage(String conversationId, AVIMIntviuMessage result) {
        try {
            String userId = (String) result.getAttrs().get(Constant.USER_ID);
            String userName = (String) result.getAttrs().get(Constant.USER_NAME);
            String userSex = (String) result.getAttrs().get(Constant.USER_SEX);
            String userIvUrl = (String) result.getAttrs().get(Constant.USER_ICON);
            String msgType = (String) result.getAttrs().get(Constant.MSG_TYPE);
            String app = (String) result.getAttrs().get(Constant.APP);
            String chatMessage = result.getText();
            if (TextUtils.isEmpty(chatMessage)) {
                chatMessage = "";
            }
            if (TextUtils.isEmpty(app)) {
                if (AppManager.IsWereWolf()) {
                    /**
                     * ignore
                     */
                }
                if (AppManager.IsXiaoYu()) {
                    return true;
                }
            } else {
                if (TextUtils.equals(app, AppManager.GetApp())) {
                    /**
                     * 同一个应用
                     */
                } else {
                    return false;
                }
            }
            if (filter(userId)) {
                /**
                 * 黑名单中的数据，过滤掉
                 */
                return true;
            }

            if (filterFamilyMsg(msgType)) {
                return false;
            }

            if (!GlobalUserState.getGlobalState().isGaming() && !TextUtils.equals(conversationId, GlobalUserState.getGlobalState().getCurConversationId())) {
                if (!TextUtils.isEmpty((String) result.getAttrs().get(Constant.ROOM_ID))) {
                    String roomId = (String) result.getAttrs().get(Constant.ROOM_ID);
                    String roomType = (String) result.getAttrs().get(Constant.GAME_TYPE);
                    String roomPassword = (String) result.getAttrs().get(Constant.ROOM_PASSWORD);
                    NotificationUtils.IMPL().onChatNotificationIntviu(
                            conversationId,
                            userId,
                            userName,
                            Integer.parseInt(userSex),
                            userIvUrl,
                            chatMessage,
                            roomId,
                            roomType,
                            roomPassword,
                            FamilyTools.IsFamilyMsg(msgType)
                    );
                } else {
                    /**
                     * 邀请消息，没有送房间号，认为此消息为失败消息
                     */
                    return true;
                }
            } else {
                NotificationUtils.IMPL().clearNotification(mContext, userId);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        return false;
    }

    private boolean handleTextMessage(String conversationId, AVIMTextMessage result) {
        try {
            /**
             * 基本文本消息类型
             */
            String userId = (String) result.getAttrs().get(Constant.USER_ID);
            String userName = (String) result.getAttrs().get(Constant.USER_NAME);
            String userSex = (String) result.getAttrs().get(Constant.USER_SEX);
            String userIvUrl = (String) result.getAttrs().get(Constant.USER_ICON);
            final String msgType = (String) result.getAttrs().get(Constant.MSG_TYPE);
            String app = (String) result.getAttrs().get(Constant.APP);
            String chatMessage = result.getText();
            if (TextUtils.isEmpty(chatMessage)) {
                chatMessage = "";
            }
            if (TextUtils.isEmpty(app)) {
                if (AppManager.IsWereWolf()) {
                    /**
                     * ignore
                     */
                }
                if (AppManager.IsXiaoYu()) {
                    return true;
                }
            } else {
                if (TextUtils.equals(app, AppManager.GetApp())) {
                    /**
                     * 同一个应用
                     */
                } else {
                    return true;
                }
            }

            if (filter(userId)) {
                return true;
            }

            if (filterFamilyMsg(msgType)) {
                return false;
            }
            /**
             * 分析文本消息类型
             */
            Utils.runSafely(() -> {
                long startTimel = (long) result.getAttrs().get(SystemConstant.START_TIME);
                long endTimel = (long) result.getAttrs().get(SystemConstant.END_TIME);
                if (msgType.contains(SystemConstant.MSG_TYPE_NOPLAY)) {
                    SystemController.updateNoPlayTime(
                            GlobalUserState.getGlobalState().getUserId(),
                            startTimel,
                            endTimel
                    );
                }
                Log.i(TAG, "onMessage: startTime=" + startTimel + ",endTime=" + endTimel + " msgType=" + msgType);
            });

            if (!GlobalUserState.getGlobalState().isGaming() && !TextUtils.equals(conversationId, GlobalUserState.getGlobalState().getCurConversationId())) {
                NotificationUtils.IMPL().onChatNotification(
                        conversationId,
                        mContext,
                        userId,
                        userName,
                        Integer.parseInt(userSex),
                        userIvUrl,
                        chatMessage,
                        FamilyTools.IsFamilyMsg(msgType)
                );
            } else {
                NotificationUtils.IMPL().clearNotification(mContext, userId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        return false;
    }

    private boolean handleAudioMessage(String conversationId, AVIMAudioMessage result) {
        try {
            String userId = (String) result.getAttrs().get(Constant.USER_ID);
            String userName = (String) result.getAttrs().get(Constant.USER_NAME);
            String userSex = (String) result.getAttrs().get(Constant.USER_SEX);
            String userIvUrl = (String) result.getAttrs().get(Constant.USER_ICON);
            final String msgType = (String) result.getAttrs().get(Constant.MSG_TYPE);
            String app = (String) result.getAttrs().get(Constant.APP);
            if (TextUtils.isEmpty(app)) {
                if (AppManager.IsWereWolf()) {
                    /**
                     * ignore
                     */
                }
                if (AppManager.IsXiaoYu()) {
                    return true;
                }
            } else {
                if (TextUtils.equals(app, AppManager.GetApp())) {
                    /**
                     * 同一个应用
                     */
                } else {
                    return false;
                }
            }
            if (filter(userId)) {
                return true;
            }

            if (filterFamilyMsg(msgType)) {
                return false;
            }

            if (!GlobalUserState.getGlobalState().isGaming() && !TextUtils.equals(conversationId, GlobalUserState.getGlobalState().getCurConversationId())) {
                NotificationUtils.IMPL().onChatNotification(
                        conversationId,
                        mContext,
                        userId,
                        userName,
                        Integer.parseInt(userSex),
                        userIvUrl,
                        "语音",
                        FamilyTools.IsFamilyMsg(msgType)
                );
            } else {
                NotificationUtils.IMPL().clearNotification(mContext, userId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        return false;
    }

    private boolean handleImageMessage(String conversationId, AVIMImageMessage result) {
        try {
            String userId = (String) result.getAttrs().get(Constant.USER_ID);
            String userName = (String) result.getAttrs().get(Constant.USER_NAME);
            String userSex = (String) result.getAttrs().get(Constant.USER_SEX);
            String userIvUrl = (String) result.getAttrs().get(Constant.USER_ICON);
            final String msgType = (String) result.getAttrs().get(Constant.MSG_TYPE);
            String app = (String) result.getAttrs().get(Constant.APP);
            if (TextUtils.isEmpty(app)) {
                if (AppManager.IsWereWolf()) {
                    /**
                     * ignore
                     */
                }
                if (AppManager.IsXiaoYu()) {
                    return true;
                }
            } else {
                if (TextUtils.equals(app, AppManager.GetApp())) {
                    /**
                     * 同一个应用
                     */
                } else {
                    return false;
                }
            }
            if (filter(userId)) {
                return true;
            }

            if (filterFamilyMsg(msgType)) {
                return false;
            }

            if (!GlobalUserState.getGlobalState().isGaming() && !TextUtils.equals(conversationId, GlobalUserState.getGlobalState().getCurConversationId())) {
                NotificationUtils.IMPL().onChatNotification(
                        conversationId,
                        mContext,
                        userId,
                        userName,
                        Integer.parseInt(userSex),
                        userIvUrl,
                        "图片",
                        FamilyTools.IsFamilyMsg(msgType)
                );
            } else {
                NotificationUtils.IMPL().clearNotification(mContext, userId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        return false;
    }

    private boolean handleGiftMessage(String conversationId, AVIMGiftMessage avimGiftMessage) {
        try {
            /**
             * 处理送礼物消息
             */
            String msg = avimGiftMessage.getText();
            String giftType = (String) avimGiftMessage.getAttrs().get(Constant.GIFT_TYPE);
            if (!GiftsSupervisor.SupportThisGiftType(giftType)) {
                msg = MessageUtils.getString(R.string.gifts_chat_type_unsupport);
            }
            String userId = (String) avimGiftMessage.getAttrs().get(Constant.USER_ID);
            String userName = (String) avimGiftMessage.getAttrs().get(Constant.USER_NAME);
            String userSex = (String) avimGiftMessage.getAttrs().get(Constant.USER_SEX);
            String userIvUrl = (String) avimGiftMessage.getAttrs().get(Constant.USER_ICON);
            final String msgType = (String) avimGiftMessage.getAttrs().get(Constant.MSG_TYPE);
            String app = (String) avimGiftMessage.getAttrs().get(Constant.APP);
            if (TextUtils.isEmpty(app)) {
                if (AppManager.IsWereWolf()) {
                    /**
                     * ignore
                     */
                }
                if (AppManager.IsXiaoYu()) {
                    return true;
                }
            } else {
                if (TextUtils.equals(app, AppManager.GetApp())) {
                    /**
                     * 同一个应用
                     */
                } else {
                    return false;
                }
            }
            if (filterFamilyMsg(msgType)) {
                /**
                 * 暂不支持家族送礼物
                 */
                return true;
            }
            if (filter(userId))
                return true;

            if (FamilyTools.IsFamilyMsg(msgType)) {
                /**
                 * 暂不支持家族送礼物
                 */
                return true;
            }

            if (!GlobalUserState.getGlobalState().isGaming() && !TextUtils.equals(conversationId, GlobalUserState.getGlobalState().getCurConversationId())) {
                /**
                 * 这里先加一个过滤，如果是家族消息则不提示
                 */
                NotificationUtils.IMPL().onChatNotification(
                        conversationId,
                        mContext,
                        userId,
                        userName,
                        Integer.parseInt(userSex),
                        userIvUrl,
                        msg,
                        false
                );
            } else {
                NotificationUtils.IMPL().clearNotification(mContext, userId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        return false;
    }
}
