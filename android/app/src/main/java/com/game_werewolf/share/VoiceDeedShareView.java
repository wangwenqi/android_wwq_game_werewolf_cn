package com.game_werewolf.share;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.view.ScreenUtils;
import com.game_werewolf.R;
import com.toolkit.action.Keepable;

import java.util.Calendar;

import orangelab.project.MainApplication;
import orangelab.project.ProjectConfig;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.model.VoiceDeedShareBean;

/**
 * Created by lloydfinch on 29/01/2018.
 * //房契分享图片view
 */

public class VoiceDeedShareView extends RelativeLayout implements Keepable {

    int defaultHeight = ScreenUtils.dip2px(310);
    int defaultWidth = ScreenUtils.dip2px(380);

    private ImageView ivIcon;//图标
    private ImageView ivDeed;//印章
    private ImageView ivQrCode;//二维码
    private View bgDown;//下层图
    private View bgUp;//上层图
    private TextView tvShareTitle;//分享标题
    private ImageView ivHead;
    private TextView tvNickName;
    private TextView tvRoomId;
    private TextView tvUid;
    private TextView tvMessage;
    private TextView tvInscribe;
    private TextView tvTime;


    public VoiceDeedShareView(Context context, VoiceDeedShareBean bean) {
        super(context);
        initView(context);
        initData(bean);
    }

    public VoiceDeedShareView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VoiceDeedShareView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initView(Context context) {

        addView(LayoutInflater.from(context).inflate(R.layout.layout_voice_deed_share, null));
        //需要根据渠道名改变的
        ivIcon = (ImageView) findViewById(R.id.iv_deed_share_icon);
        ivDeed = (ImageView) findViewById(R.id.iv_deed);
        ivQrCode = (ImageView) findViewById(R.id.iv_qr_code);
        bgUp = findViewById(R.id.rl_bg_up);
        bgDown = findViewById(R.id.rl_bg_down);
        tvShareTitle = (TextView) findViewById(R.id.tv_share_title);
        tvMessage = (TextView) findViewById(R.id.tv_message);

        //渠道无关的
        ivHead = (ImageView) findViewById(R.id.iv_head);
        tvNickName = (TextView) findViewById(R.id.tv_nickname);
        tvRoomId = (TextView) findViewById(R.id.tv_room_number);
        tvUid = (TextView) findViewById(R.id.tv_uid);
        tvMessage = (TextView) findViewById(R.id.tv_message);
        tvInscribe = (TextView) findViewById(R.id.tv_inscribe);
        tvTime = (TextView) findViewById(R.id.tv_time);

        initViewCn();

        defaultWidth = defaultWidth > ScreenUtils.getScreenWidth() ? ScreenUtils.getScreenWidth() : defaultWidth;
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(defaultWidth, defaultHeight);
        setLayoutParams(layoutParams);
    }

    //国内版
    private void initViewCn() {
        ivIcon.setImageResource(R.mipmap.ic_launcher);
        ivQrCode.setImageResource(R.mipmap.ico_voice_deed_share_qrcode_cn);
        tvShareTitle.setText(R.string.str_voice_deed_share_title_cn);
        ivDeed.setImageResource(R.mipmap.ico_voice_deed_seal_cn);
        bgDown.setBackgroundResource(R.mipmap.ico_voice_deed_share_bg_down_cn);
        bgUp.setBackgroundResource(R.mipmap.ico_voice_deed_share_bg_up_cn);
    }

    public void initData(VoiceDeedShareBean bean) {

        PicassoUtils.loadImageByDefault(MainApplication.getInstance().getApplicationContext(), bean.headImageUrl, ivHead, R.mipmap.default_head);
        tvNickName.setText(MessageUtils.getString(R.string.str_voice_deed_share_nickname, bean.nickname));
        tvRoomId.setText(MessageUtils.getString(R.string.str_voice_deed_share_room_number, bean.roomId));
        tvUid.setText(MessageUtils.getString(R.string.str_voice_deed_share_uid, bean.uid));

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(bean.time);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int date = calendar.get(Calendar.DATE);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);//使用24小时制
        int minute = calendar.get(Calendar.MINUTE);

        String hourStr = String.valueOf(hour);
        if (hour < 10) {
            hourStr = "0" + hourStr;
        }

        String minuteStr = String.valueOf(minute);
        if (minute < 10) {
            minuteStr = "0" + minuteStr;
        }

        tvInscribe.setText(MessageUtils.getString(R.string.str_voice_deed_share_inscribe, bean.roomId));
        tvTime.setText(MessageUtils.getString(R.string.str_voice_deed_share_time, year, month, date, hour, minuteStr));

        if (ProjectConfig.isKuaiWan()) {
            tvMessage.setText(MessageUtils.getString(R.string.str_voice_deed_share_message_cn, year, month, date, hourStr, minuteStr, bean.nickname, bean.roomId));
        } else {
            tvMessage.setText(MessageUtils.getString(R.string.str_voice_deed_share_message_cn, year, month, date, hourStr, minuteStr, bean.nickname, bean.roomId));
        }
    }

    public Bitmap createBitmap() {
        measure(getLayoutParams().width, getLayoutParams().height);
        layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        draw(canvas);
        return bitmap;
    }
}
