package com.game_werewolf.share;

/**
 * Created by wx on 2017/3/21.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;
import com.game_werewolf.R;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.URLManager;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.URLEncoder;
import java.util.ArrayList;

import cn.intviu.support.AppUtil;
import cn.intviu.support.ShareTools;
import orangelab.project.MainApplication;
import orangelab.project.ProjectConfig;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.dialog.EnterLoadingDialog;
import orangelab.project.common.dialog.IntviuFriendDialog;
import orangelab.project.common.dialog.ShareDialog;
import orangelab.project.common.family.FamilyMemo;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.share.ShareBridgeData;
import orangelab.project.common.utils.LeanCloudChatHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.ShareDialogAdapter;
import orangelab.project.common.utils.ShareMedia;
import orangelab.project.common.utils.Utils;
import orangelab.project.voice.cache.FileCache;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.dialog.ShareImageDialog;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.utils.HttpUtils;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

public class ShareUtilsActivity extends SafeActivity {
    private static final String TAG = "ShareUtilsActivity";

    private String mShareMessage;
    private String mShareTitle;
    private String mShareUrl;

    private RxPermissions rxPermissions;
    private ShareDialog shareDialog;

    private AsyncTask<ShareBridgeData, Integer, Bitmap> asyncTask;
    private EnterLoadingDialog mEnterLoadingDialog;

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rxPermissions = new RxPermissions(this);
        Utils.setWindowStatusBarColor(this, android.R.color.transparent);
        EventBus.getDefault().register(this);


        Intent intent = getIntent();
        if (intent != null) {
            try {
                ShareBridgeData data = (ShareBridgeData) intent.getSerializableExtra(ShareBridgeData.SHARE_COMMON_SINGLE);
                if (data != null) {
                    shareByType(data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 根据分享调起的地方进行不同类型的分享
     *
     * @param data
     */
    private void shareByType(ShareBridgeData data) {
        switch (data.shareOrigin) {
            case ShareBridgeData.SHARE_FROM_GANE:
                shareFromGameOut(data);
                break;
            case ShareBridgeData.SHARE_FROM_VOICE:
                shareFromVoiceOut(data);
                break;
            case ShareBridgeData.SHARE_FROM_MINIGAME:
                shareFromMiniGameOut(data);
                break;
            case ShareBridgeData.SHARE_FROM_SETTING:
                shareFromSettingOut(data);
                break;
            case ShareBridgeData.SHARE_FROM_JS:
                shareFromMiniGameOut(data);
                break;
            case ShareBridgeData.SHARE_FROM_CUSTOM:
                shareFromCustom(data);
                break;
            case ShareBridgeData.SHARE_ONLY_IMAGE:
                shareOnlyImage(data);
                break;
            default:
                break;

        }
    }

    private boolean filterAppExit(String app) {
        if (ShareTools.existsApp(ShareUtilsActivity.this, app)) {
            return true;
        } else {
            runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(com.R.string.share_error)));
            return false;
        }
    }

    /**
     * 游戏房分享
     *
     * @param data
     */
    public void shareFromGameOut(ShareBridgeData data) {
        if (TextUtils.isEmpty(data.userName)) {
            return;
        }
        //标记是分享还是邀请
        ShareMedia.getShareMedia().setType(Constant.INVITE);
        StringBuilder newUrl = new StringBuilder();
        String newUserName = data.userName;
        try {
            newUserName = URLEncoder.encode(newUserName, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        String flavor = ProjectConfig.isKuaiWan() ? "/invite_kuaiwan" : "/invite";
        String url = URLManager.GetDefaultURL() + flavor + "?id=" + data.roomId + "&inviter=" + newUserName
                + "&channel=" + AppUtil.getApkChannel(this);
        newUrl.append(url);
        if (!TextUtils.isEmpty(data.password)) {
            newUrl.append("&password=" + data.password);
        }
        String[] messagesId = MainApplication.getInstance().getResources().getStringArray(com.R.array.share_message_from_game);
        String[] titlesId = MainApplication.getInstance().getResources().getStringArray(com.R.array.share_game_title);
        int id = (int) Math.round(Math.random() * (titlesId.length - 1));//随机产生一个index索引
        String title = titlesId[id];
        String message = String.format(messagesId[id], data.roomId, "");
        if (!TextUtils.isEmpty(data.password)) {
            message = String.format(messagesId[id], data.roomId, "密码为" + data.password + ",");
        }

        mShareMessage = message;
        mShareTitle = title;
        mShareUrl = TextUtils.isEmpty(data.url) ? newUrl.toString() : data.url;
        showShareDialog();

    }

    /**
     * 语音房分享
     *
     * @param data
     */
    public void shareFromVoiceOut(ShareBridgeData data) {

    }

    /**
     * 设置页面分享
     *
     * @param data
     */
    public void shareFromSettingOut(ShareBridgeData data) {

        //标记是分享还是邀请
        ShareMedia.getShareMedia().setType(Constant.SHARE);
        String[] messages = MainApplication.getInstance().getResources().getStringArray(com.R.array.share_message_from_setting);
        String[] titles = MainApplication.getInstance().getResources().getStringArray(com.R.array.share_title);
        int id = (int) Math.round(Math.random() * (titles.length - 1));//随机产生一个index索引
        String message = messages[id];
        String title = titles[id];
        String shareRoute = ProjectConfig.isKuaiWan() ? "/share_kuaiwan?channel=" : "/share?channel=";
        String url = URLManager.GetDefaultURL() + shareRoute + AppUtil.getApkChannel(MainApplication.getInstance());

        mShareMessage = message;
        mShareTitle = title;
        mShareUrl = TextUtils.isEmpty(data.url) ? url : data.url;
        showShareDialog();
    }

    /**
     * js 回调的分享
     *
     * @param title
     * @param url
     * @param message
     * @param shareType
     */
    public final void shareFromJS(String title, String url, String message, String shareType) {
        //标记是分享还是邀请
        if (TextUtils.isEmpty(shareType)) {
            ShareMedia.getShareMedia().setType(Constant.INVITE);
        } else {
            ShareMedia.getShareMedia().setType(shareType);
        }

        mShareMessage = message;
        mShareTitle = title;
        mShareUrl = url;
        showShareDialog();
    }

    /**
     * 小游戏分享¬
     *
     * @param data
     */
    public void shareFromMiniGameOut(ShareBridgeData data) {
        shareFromJS(data.title, data.url, data.message, Constant.INVITE);
    }

    /**
     * 自定义分享，直接使用传递数据进行
     *
     * @param data
     */
    public void shareFromCustom(ShareBridgeData data) {
        ShareMedia.getShareMedia().setType(VoiceConstants.INVITE);

        mShareMessage = data.message;
        mShareTitle = data.title;
        mShareUrl = data.url;

        showShareDialog();
    }

    /**
     * 根据url分享图片
     *
     * @param data
     */
    @SuppressLint("StaticFieldLeak")
    public void shareOnlyImage(ShareBridgeData data) {

        asyncTask = new AsyncTask<ShareBridgeData, Integer, Bitmap>() {

            @Override
            protected Bitmap doInBackground(ShareBridgeData... params) {
                ShareBridgeData data1 = params[0];
                if (data != null) {

                    //本地获取
                    byte[] bytes = FileCache.getInstance().loadFile(data1.url);
                    if (bytes == null) {
                        //网络获取
                        bytes = HttpUtils.doGet(data1.url);
                        //保存在本地
                        FileCache.getInstance().saveFile(data1.url, bytes);
                    }
                    if (bytes != null) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        PLog.e(TAG, "bitmap size: " + bitmap.getByteCount());
                        return bitmap;
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (bitmap != null) {
                    dismissLoading();
                    new ShareImageDialog(ShareUtilsActivity.this, bitmap).show();
                } else {
                    dismissDialogAndFinish();
                }
            }
        };
        showLoading();
        PLog.e(TAG, "bitmap: " + "shareOnlyImage");
        asyncTask.execute(data);
    }

    private View getDialogContentView() {
        ArrayList<Integer> text = new ArrayList<>();
        ArrayList<Integer> image = new ArrayList<>();
        text.add(com.R.string.share_we_chat);
        text.add(com.R.string.share_moments);
        text.add(com.R.string.share_qq);
        text.add(com.R.string.share_facebook);
        text.add(com.R.string.share_line);

        image.add(com.R.mipmap.share_through_weixin);
        image.add(com.R.mipmap.share_through_moments);
        image.add(com.R.mipmap.share_through_qq);
        image.add(com.R.mipmap.icon_facebook);
        image.add(com.R.mipmap.icon_line);

        ListView view = new ListView(this);
        view.setSelector(android.R.color.transparent);
        view.addFooterView(new View(this));
        view.setFooterDividersEnabled(true);
        view.setVerticalScrollBarEnabled(false);
        view.setDividerHeight(0);

        final ShareDialogAdapter adapter = new ShareDialogAdapter(this, text, image);
        view.setAdapter(adapter);
        view.setOnItemClickListener((parent, view1, position, id) -> {

            int itemId = (Integer) adapter.getItem(position);
            if (itemId == com.R.string.share_we_chat) {
                ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN);
                if (filterAppExit(ShareTools.SHARE_WEIXIN)) {
                    ThreadToolKit.Async(() -> shareSoftware(SHARE_MEDIA.WEIXIN));
                }
            } else if (itemId == com.R.string.share_moments) {
                ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN_CIRCLE);
                if (filterAppExit(ShareTools.SHARE_WEIXIN)) {
                    ThreadToolKit.Async(() -> shareSoftware(SHARE_MEDIA.WEIXIN_CIRCLE));
                }
            } else if (itemId == com.R.string.share_qq) {
                ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.QQ);
                if (filterAppExit(ShareTools.SHARE_QQ)) {
                    rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .onBackpressureBuffer(Constant.RX_BACK_PRESSURE)
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<Boolean>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onNext(Boolean aBoolean) {
                                    if (aBoolean) {
                                        if (!TextUtils.isEmpty(mShareMessage)) {
                                            if (mShareMessage.length() > 60) {
                                                StringBuilder message = new StringBuilder();
                                                message.append(mShareMessage.substring(0, 57));
                                                message.append("...");
                                                mShareMessage = message.toString();
                                            }
                                        }
                                        shareSoftware(SHARE_MEDIA.QQ);
                                        if (shareDialog != null && shareDialog.isShowing()) {
                                            shareDialog.dismiss();
                                        }
                                        String category = ShareMedia.getShareMedia().getType();
                                        String type = Constant.SHARE_TO_QQ;
                                        if (TextUtils.equals(category, Constant.INVITE)) {
                                            type = Constant.INVITE_TO_QQ;
                                        }
                                        shareSuccessReported(type);
                                    } else {
                                        Toast.makeText(ShareUtilsActivity.this, com.R.string
                                                        .permission_read_store_error,
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            } else if (itemId == com.R.string.share_intviu) {
                IntviuFriendDialog friendDialog = new IntviuFriendDialog(ShareUtilsActivity.this);
                friendDialog.show();
                dismissDialogOnly();
            } else if (itemId == com.R.string.share_facebook) {
                if (filterAppExit(ShareTools.SHARE_FACEBOOK)) {
                    ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.FACEBOOK);
                    dismissDialogOnly();
                    ThreadToolKit.Async(() -> shareSoftware(SHARE_MEDIA.FACEBOOK));
                }
            } else if (itemId == com.R.string.share_line) {
                if (filterAppExit(ShareTools.SHARE_LINE)) {
                    rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .onBackpressureBuffer(Constant.RX_BACK_PRESSURE)
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<Boolean>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onNext(Boolean aBoolean) {
                                    if (aBoolean) {
                                        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.LINE);
                                        dismissDialogOnly();
                                        ThreadToolKit.Async(() -> shareSoftware(SHARE_MEDIA.LINE));
                                    } else {
                                        Toast.makeText(ShareUtilsActivity.this, com.R.string
                                                        .permission_read_store_error,
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            } else if (itemId == com.R.string.string_intviu_into_family) {
                Utils.runSafely(() -> {
                    LeanCloudChatHelper.SendIntviuToConversationForFamily(
                            GlobalUserState.getGlobalState().getCurRoomId(),
                            GlobalUserState.getGlobalState().getPassword(),
                            FamilyMemo.iMemo.getFamilyLcId()
                    );
                });
            }
        });
        view.setPadding(ScreenUtils.dip2px(8), ScreenUtils.dip2px(8), ScreenUtils.dip2px(8), ScreenUtils.dip2px(8));
        return view;
    }

    private void showShareDialog() {
        if (shareDialog == null) {
            shareDialog = new ShareDialog(this);
            String title = getString(com.R.string.title_share);
            shareDialog.addContentView(getDialogContentView());
            View titleView = LayoutInflater.from(this).inflate(com.R.layout.layout_game_dialog_title, null);
            TextView titleText = (TextView) titleView.findViewById(com.R.id.dialog_title);
            titleText.setText(title);
            titleText.setTextColor(getResources().getColor(android.R.color.black));
            shareDialog.addTitleView(titleView);
            shareDialog.setDismissRunnable(() -> {
                dismissDialogAndFinish();
            });
            shareDialog.setCancelable(false);
            shareDialog.setCanceledOnTouchOutside(false);
            shareDialog.show();
        }
    }

    private void dismissDialogOnly() {
        if (shareDialog != null) {
            shareDialog.dismiss();
        }
    }

    private void dismissDialogAndFinish() {
        dismissDialogOnly();
        finish();
    }

    /**
     * 分享的最终公共入口
     *
     * @param share_media
     */
    private void shareSoftware(SHARE_MEDIA share_media) {
        int shareAppIcon = R.mipmap.ic_launcher;
        UMImage image = new UMImage(ShareUtilsActivity.this.getApplicationContext(), BitmapFactory.decodeResource(getResources(), shareAppIcon));
        UMWeb umWeb = new UMWeb((mShareUrl));
        umWeb.setTitle(mShareTitle);
        umWeb.setThumb(image);
        umWeb.setDescription(mShareMessage);

        new ShareAction(this)
                .withMedia(umWeb)
                .setPlatform(share_media)
                .setCallback(mUmShareListener)
                .share();
    }

    private UMShareListener mUmShareListener = new UMShareListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {
            PLog.i(TAG, "share-->onStart" + share_media);
        }

        @Override
        public void onResult(SHARE_MEDIA platform) {
            PLog.i(TAG, "share-->success" + platform);
            //TODO qq,weixin,line,facebook统一为QQ微信的任务
            String category = ShareMedia.getShareMedia().getType();
            String type = Constant.SHARE_TO_QQ;
            if (TextUtils.equals(category, Constant.INVITE)) {
                type = Constant.INVITE_TO_QQ;
            }
            if (SHARE_MEDIA.QQ != platform) {
                shareSuccessReported(type);
            }
            finish();
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            finish();
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            PLog.i(TAG, "share-->cancel");
            finish();
        }
    };

    private void shareSuccessReported(String type) {
        ApiManager.DoShareApi(type, 1);
    }

    //如果使用的是qq或者新浪精简版jar，需要在您使用分享或授权的Activity（fragment不行）中添加如下回调代码：
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this.getApplicationContext()).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void finish() {
        destroy();
        super.finish();
    }


    private void showLoading() {
        mEnterLoadingDialog = new EnterLoadingDialog(this);
        mEnterLoadingDialog.show();
    }

    private void dismissLoading() {
        if (mEnterLoadingDialog != null) {
            mEnterLoadingDialog.dismiss();
        }
    }

    private void destroy() {
        if (rxPermissions != null) {
            rxPermissions = null;
        }

        if (shareDialog != null) {
            if (shareDialog.isShowing()) {
                shareDialog.dismiss();
            }
            shareDialog = null;
        }

        try {
            if (asyncTask != null && asyncTask.getStatus() == AsyncTask.Status.RUNNING) {
                asyncTask.cancel(true);
            }
        } catch (Exception e) {
            PLog.e(TAG, e.getMessage());
        }

        dismissLoading();

        RxToolKit.UnRegister(this);
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FinishConversationEvent event) {
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VoiceEvent.VoiceShareActivityFinishEvent event) {
        finish();
    }
}
