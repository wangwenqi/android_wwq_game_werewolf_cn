package com.game_werewolf;

import com.androidtoolkit.RxToolKit;
import com.datasource.GlobalUserState;
import com.toolkit.action.Destroyable;

import orangelab.project.common.activity.LaunchActivity;
import orangelab.project.common.model.ServerGet;
import orangelab.project.minigame.MiniGameActivity;
import orangelab.project.minigame.event.MiniGameReMatchEvent;
import orangelab.project.minigame.model.GlobalSocketJoinRoom;
import orangelab.project.minigame.model.MiniGameLaunch;

/**
 * game_werewolf
 * 2018/2/1 下午2:55
 * Mystery
 */

public class MainActivityMiniGameHelper implements Destroyable {

    private MainActivity mActivity;

    public MainActivityMiniGameHelper(MainActivity activity) {
        mActivity = activity;

        RxToolKit.Build(this, GlobalSocketJoinRoom.class).action(value -> {
            if (mActivity != null) {
                mActivity.runOnUiThreadSafely(() -> {
                    if (GlobalUserState.getGlobalState().isGaming()) {
                        /**
                         * 如果在游戏中则不响应此次启动
                         */
                        return;
                    }
                    ServerGet serverGet = value.data;
                    ServerGet.ServerGetMiniGame miniGame = value.data.mini_game;
                    MiniGameLaunch.Builder builder = new MiniGameLaunch.Builder()
                            .type(serverGet.type)
                            .server(serverGet.server)
                            .roomId(serverGet.room_id)
                            .gameName(miniGame.name)
                            .gameIcon(miniGame.icon)
                            .gameUrl(miniGame.url)
                            .landscape(miniGame.landscape)
                            .extraData(serverGet.extraData)
                            .password(serverGet.password)
                            .user_images(serverGet.user_images);

                    try {
                        builder.oppoIcon(value.data.oppo.avatar);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    GlobalUserState.getGlobalState().setGaming(true);
                    MiniGameActivity.Launch(mActivity, builder.build());
                });
            }
        }).register();

        RxToolKit.Build(this, MiniGameReMatchEvent.class).action(value -> {
            if (mActivity != null) {
                mActivity.runOnUiThreadSafely(() -> {
                    LaunchActivity.LaunchForAmusementRandom(mActivity, value.getMiniGameLaunch().getType());
                });
            }
        }).register();
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        if (mActivity != null) {
            mActivity = null;
        }
    }
}
