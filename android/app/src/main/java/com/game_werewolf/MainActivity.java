package com.game_werewolf;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.text.TextUtils;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;

import com.networktoolkit.transport.Constant;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxBus;
import com.androidtoolkit.UIActuator;
import com.networktoolkit.transport.RequestTask;

import org.json.JSONObject;


import cn.intviu.orbit.manager.OrbitGlobalConfig;
import cn.intviu.support.GsonHelper;
import cn.intviu.support.UriDecoder;

import com.datasource.GlobalUserState;
import com.networktoolkit.transport.RequestTaskCallBack;

import orangelab.project.MainApplication;
import orangelab.project.common.RNActivityManager;
import orangelab.project.common.activity.HintActivity;
import orangelab.project.common.activity.LaunchActivity;
import orangelab.project.common.bridge.splashscreen.SplashScreen;
import orangelab.project.common.dialog.MsgDialog;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.event.ActivityEvent;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.exhibition.gift.GiftsSupervisor;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.manager.AppManager;
import orangelab.project.common.manager.BlackListManager;
import orangelab.project.common.model.EnterRoomPayload;
import orangelab.project.common.model.LeanCloudConfig;
import orangelab.project.common.pay.google.GooglePayTask;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;
import rx.Subscription;

public class MainActivity extends ReactActivity {
    private static final String TAG = "MainActivity";
    private static final int IMAGE_PICKER_REQUEST = 61110;//图片选取
    private static final int CAMERA_PICKER_REQUEST = 61111;//相机
    private static final int CANCEL_PICKER_REQUEST = 69;//取消选取照片
    private static final int QQ_REQUEST = 11101;//QQ登录
    private static final int ADV_TIMEOUT = 10000;

    private MsgDialog msgDialog;
    private MsgDialog mContinueDialog = null;
    private boolean ReactNativeInitFinish = false;
    private boolean ReactNativeDataInitFinish = false;

    private GooglePayTask googlePayTask = null;

    private SafeHandler mSafeHandler = new SafeHandler();
    private MainActivityLCIMHelper mMainActivityHelper;
    private MainActivityFloatWindowHelper mFloatWindowHelper;
    private MainActivityMiniGameHelper mActivityMiniRoomSocketEngineHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this);  // here rn启动android由于js要读取index.android.bundle 里面的配置以及资源，导致在加载过程中白屏很长时间
        super.onCreate(savedInstanceState);
        RNActivityManager.INSTANCE.BindRNActivity(this);
        Utils.setWindowStatusBarColor(this, android.R.color.transparent);
        mMainActivityHelper = new MainActivityLCIMHelper(this);
        mFloatWindowHelper = new MainActivityFloatWindowHelper(this);
        mActivityMiniRoomSocketEngineHelper = new MainActivityMiniGameHelper(this);
        googlePayTask = new GooglePayTask(this);

        /**
         * 判断是不是网页端进入
         */
        Uri uri = getIntent().getData();
        if (uri != null) {
        } else {
            if (isFromFront() && !isFromHistory()) {
                PLog.i(TAG, "onCreate: Is From History or isFromFront");
                finish();
                return;
            }
        }

        if (!isFromHistory()) {
            turnToGame(getIntent());
        }
        initListener();
    }

    private void initListener() {
        Subscription sb = RxBus.wrapObservableSafely(RxBus.turnToObservable(ActivityEvent.ReactNativeInitFinishEvent.class), reactNativeInitFinishEvent -> {
            ReactNativeInitFinish = true;
            PLog.i(TAG, "initReactNative Finish: ");
            // checkLeanCloudNode();
        });
        RxBus.getInstance().register(this, sb);

        Subscription action = RxBus.wrapObservableSafely(RxBus.turnToObservable(ActivityEvent.StartPayTaskEvent.class), webViewPayResultEvent -> {
            startGoogleTask();
        });
        RxBus.getInstance().register(this, action);

        Subscription rnAction = RxBus.wrapObservableSafely(RxBus.turnToObservable(ActivityEvent.ReactNativeInitDataFinishEvent.class), reactNativeInitDataFinishEvent -> {
            ReactNativeDataInitFinish = true;
            startGoogleTask();
            startBlackList();
            showRestoreGameInfoIfNeed();
//            if (!Config.HOOK) {
//                GiftsSupervisor.RefreshAndCheckGiftManifestData(null);
//            }
            GiftsSupervisor.RefreshAndCheckGiftManifestData(null);
        });
        RxBus.getInstance().register(this, rnAction);

        Subscription rnLoginOutAction = RxBus.wrapObservableSafely(RxBus.turnToObservable(ActivityEvent.ReactNativeLogOutEvent.class), reactNativeLogOutEvent -> {
            runOnUiThreadSafely(() -> {
                ReactNativeDataInitFinish = false;
            });
        });
        RxBus.getInstance().register(this, rnLoginOutAction);

        initInnerMessageEvent();
    }

    private void startBlackList() {
        BlackListManager.Refresh();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ReportEventUtils.reportActivityResume(this);
        if (ReactNativeInitFinish) {
            // checkLeanCloudNode();
            todDoRecordLogin();
            RoomSocketEngineHelper.userLeaveFromWeb();
        }

        if (ReactNativeDataInitFinish) {
            startGoogleTask();
        }

        if (ReactNativeDataInitFinish) {
            showRestoreGameInfoIfNeed();
        }

        if (ReactNativeDataInitFinish) {
//            if (!Config.HOOK) {
//                GiftsSupervisor.RefreshAndCheckGiftManifestData(null);
//            }
            GiftsSupervisor.RefreshAndCheckGiftManifestData(null);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ReportEventUtils.reportActivityPause(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        AppManager.ActivityOnStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppManager.ActivityOnStop();
    }

    private void startGoogleTask() {
        if (googlePayTask != null) {
            Utils.runSafely(() -> {
                googlePayTask.startTask();
            });
        }
    }

    /**
     * 上报连续登录
     */
    private void todDoRecordLogin() {
        //通知进行登录上报
        WritableMap params = Arguments.createMap();
        RoomSocketEngineHelper.sendEventToRn(Constant.CONTINUE_LOGIN, params);
    }

    private void checkLeanCloudNode() {
        RequestTask.create(
                false,
                "/lean/config",
                "",
                new JSONObject(),
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            LeanCloudConfig config = GsonHelper.getGson().fromJson(response, LeanCloudConfig.class);
                            /**
                             * 处理节点问题
                             */
                            if (!TextUtils.isEmpty(config.host)) {
                                String host = config.host;
                                boolean unmute_send = config.unmute_send;
                                OrbitGlobalConfig.forceConfigUnMuteSendPackage(unmute_send);
                                if (!TextUtils.isEmpty(host)) {
                                    String configHost = Utils.getLeanCloudNode(MainActivity.this);
                                    PLog.i(TAG, "onSuccess: configHost=" + configHost);
                                    if (TextUtils.isEmpty(configHost)) {
                                        /**
                                         * 如果上次为空那么先存入默认值然后获取
                                         */
                                        Utils.saveLeanCloudNode(MainActivity.this, Constant.LEANCLOUD_CN);
                                        configHost = Utils.getLeanCloudNode(MainActivity.this);
                                        PLog.i(TAG, "onSuccess: configHost=" + configHost);
                                    }
                                    PLog.i(TAG, "onSuccess: host=" + host + " lastHost=" + configHost);
                                    //判断配置是否相同
                                    if (TextUtils.equals(configHost, host)) {
                                        /**
                                         * 配置相同则忽略
                                         */
                                    } else {
                                        WritableMap map = Arguments.createMap();
                                        map.putString(Constant.LEANCLOUD_NODE_RN_CONFIG, host);
                                        RoomSocketEngineHelper.sendEventToRn(Constant.LEANCLOUD_NODE_RN, map);
                                        /**
                                         * 更改配置，并且重新启动应用
                                         */
                                        Utils.saveLeanCloudNode(MainActivity.this, host);
                                        HintActivity.showHint(MainActivity.this);
                                    }
                                }
                            }

                            /**
                             * 处理LeanCloud server代理地址问题
                             */
                            if (Utils.targetIsNotNull(config.leancloud)) {

                                boolean configHasChange = false;

                                String api = config.leancloud.api;
                                String rtm = config.leancloud.rtm;

                                PLog.i(TAG, "new leancloud config-> api:" + api + ",rtm:" + rtm);

                                String lastApi = Utils.getLeanCloudServerApi(MainActivity.this);
                                String lastRtm = Utils.getLeanCloudServerRtm(MainActivity.this);

                                PLog.i(TAG, "old leancloud config-> api:" + lastApi + ",rtm:" + lastRtm);

                                if (TextUtils.equals(api, lastApi)) {
                                    /**
                                     * 地址相同不作处理
                                     */
                                } else {
                                    configHasChange = true;
                                    /**
                                     * 保存新地址
                                     */
                                    Utils.saveLeanCloudServerApi(MainActivity.this, api);
                                }

                                if (TextUtils.equals(rtm, lastRtm)) {
                                    /**
                                     * 地址相同不作处理
                                     */
                                } else {
                                    configHasChange = true;
                                    /**
                                     * 保存新地址
                                     */
                                    Utils.saveLeanCloudServerRtm(MainActivity.this, rtm);
                                }

                                if (configHasChange) {
                                    /**
                                     * 发生了地址改变
                                     */
                                    HintActivity.showHint(MainActivity.this);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {

                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }
        ).execute();
    }

    /**
     * MainActivity是否在已经在栈顶
     *
     * @return
     */
    private boolean isFromFront() {
        return (getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0;
    }

    /**
     * @return true 从历史记录中进入
     * false 正常进入应用
     */
    private boolean isFromHistory() {
        return (getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0;
    }

    private void turnToGame(Intent intent) {
        if (intent != null) {
            Uri uri = intent.getData();
            if (uri != null) {
                PLog.i(TAG, "onNewIntent uri:" + uri.toString());
                UriDecoder.CodeItem item = UriDecoder.actionDecodeUri(uri);
                if (item != null && !TextUtils.isEmpty(item.roomId)) {
                    turnToGameInner(item.roomId, item.password);
                }
            } else {
                if (TextUtils.equals(
                        IntentDataHelper.getIntentType(getIntent()),
                        IntentDataHelper.TYPE_NOTIFACTION_FROM_INTVIU
                )) {
                    turnToGameInner(
                            IntentDataHelper.getRoomID(getIntent()),
                            IntentDataHelper.getRoomPassword(getIntent())
                    );
                }
            }
        }
    }

    private void turnToGameInner(String roomId, String roomPassword) {
        final WritableMap params = Arguments.createMap();
        params.putString(Constant.ROOM_CODE, roomId);
        params.putString(Constant.ROOM_PASSWORD, roomPassword);
        UIActuator.postDelay(() -> {
            if (MainApplication.getInstance().getNativeJSModule() != null) {
                MainApplication.getInstance().getNativeJSModule().sendEvent(Constant
                        .EVENT_START_BROWSABLE, params);
            }
        }, 2000);
    }

    private void initInnerMessageEvent() {
        Subscription sub = RxBus.wrapObservableSafely(RxBus.turnToObservable(ViewEvent.LeaveMessageEvent.class), leaveMessageEvent -> {
            if (msgDialog != null && msgDialog.isShowing()) {
                return;
            }
            msgDialog = new MsgDialog(MainActivity.this,
                    MessageUtils.getString(R.string.dialog_hint),
                    MessageUtils.getString(R.string.leave_warning_string),
                    v -> msgDialog.dismiss()
            );
            msgDialog.show();
        });
        RxBus.getInstance().register(this, sub);
    }

    @Override
    protected String getMainComponentName() {
        return BuildConfig.app_module;
    }

    @Override
    public void finish() {
        super.finish();
        release();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        release();
    }

    private void release() {
        RxBus.getInstance().unregister(this);
        RNActivityManager.INSTANCE.UnRegisterRNActivity();
        mSafeHandler.release();
        if (googlePayTask != null) {
            googlePayTask.release();
            googlePayTask = null;
        }
        if (mMainActivityHelper != null) {
            mMainActivityHelper.destroy();
            mMainActivityHelper = null;
        }
        if (mFloatWindowHelper != null) {
            mFloatWindowHelper.destroy();
            mFloatWindowHelper = null;
        }
        if (mActivityMiniRoomSocketEngineHelper != null) {
            mActivityMiniRoomSocketEngineHelper.destroy();
            mActivityMiniRoomSocketEngineHelper = null;
        }
        try {
            MainApplication.getInstance().getNativeJSModule().unRegisterBridgeFilter();
        } catch (Exception e) {
            e.printStackTrace();
            //in case NativeJSModule is null
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //TODO wangxu react-native-qq 需要实现onActivityResult requestCode=11101
        //TODO 但是react-native-image-crop-picker 自身以及处理如果是选取头像一定要实现picker的onActivityResult requestCode=61110
        // 或requestCode=61111或requestCode=69
        if (requestCode == CAMERA_PICKER_REQUEST || requestCode == IMAGE_PICKER_REQUEST || requestCode ==
                CANCEL_PICKER_REQUEST) {
            if (MainApplication.getInstance().getPickerModule() != null) {
                MainApplication.getInstance().getPickerModule().onActivityResult(this, requestCode, resultCode, data);
            } else {
                PLog.i(TAG, "onActivityResult: getPickerModule==null");
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            ReactInstanceManager mReactInstanceManager = MainApplication.getInstance().getReactNativeHost()
                    .getReactInstanceManager();
            if (mReactInstanceManager != null) {
                mReactInstanceManager.onActivityResult(this, requestCode, resultCode, data);
            }
            MainApplication.getCallbackManager().onActivityResult(requestCode, resultCode, data);
        }
    }

    public void runOnUiThreadSafely(Runnable runnable) {
        mSafeHandler.postSafely(runnable);
    }

    private void showRestoreGameInfoIfNeed() {
        if (mContinueDialog != null && mContinueDialog.isShowing()) {
            return;
        }
        ApiManager.DisConnectInfoApi(value -> {
            runOnUiThreadSafely(() -> {
                if (value != null && !TextUtils.isEmpty(value.room_id) && !TextUtils.isEmpty(value.type)) {
                    MsgDialog.MsgDialogBuilder builder = MsgDialog.MsgDialogBuilder.build().setAutoDismiss(true)
                            .setLeftButtonText(MessageUtils.getString(R.string.werewolf_continue_game_cancel))
                            .setRightButtonText(MessageUtils.getString(R.string.werewolf_continue_game_confirm))
                            .setLeftClick(v -> {
                                ApiManager.EscapeGameApi();
                                if (mContinueDialog != null && mContinueDialog.isShowing()) {
                                    mContinueDialog.dismiss();
                                }
                            })
                            .setRightClick(v -> {
                                EnterRoomPayload payload = new EnterRoomPayload();
                                payload.password = value.password;
                                payload.gameType = value.type;
                                payload.roomId = value.room_id;
                                payload.userId = GlobalUserState.getGlobalState().getUserId();
                                payload.token = GlobalUserState.getGlobalState().getToken();
                                Intent intent = new Intent(this, LaunchActivity.class);
                                IntentDataHelper.setIntentType(intent, IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN);
                                IntentDataHelper.setEnterRoomPayload(intent, payload);
                                startActivity(intent);
                                if (mContinueDialog != null && mContinueDialog.isShowing()) {
                                    mContinueDialog.dismiss();
                                }
                            })
                            .setMessage(MessageUtils.getString(R.string.werewolf_continue_game))
                            .setTitle(MessageUtils.getString(R.string.dialog_hint));
                    mContinueDialog = new MsgDialog(this, builder);
                    mContinueDialog.show();
                }
            });
        });
    }
}
