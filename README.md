README #

# Game Of Werewolf #
## Guide line##

### Develop tools ###
* webstorm 2016.3.3
* chrome extension(Redux DevTools,React Developer Tools)
* xcode 

### Necessary skills ###
* ES6
* react-native
* redux
* immutable js
* native base
* 


### What is this repository for? ###

* Game of werewolf develop by react and node.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
### How to use? ###
  * npm install
  * react-native run-ios/run-android

### Dependencies ###
  * nodejs@#6.9.5;
  * react-native@#0.4.1;
  * xcode;

* Database configuration
* How to run tests
* Deployment instructions

### How to debug? ###
 * [三分钟学习RN debug](http://www.tuicool.com/articles/qUjI3aa)
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Redux ####
* [Redux action standard](https://github.com/acdlite/flux-standard-action)
* [Redux doc](http://cn.redux.js.org/index.html)
### ES6 ###
* [ES6 箭头函数](http://www.cnblogs.com/xueandsi/p/6032578.html)
### FLEX ###
* [React 布局](http://reactnative.cn/docs/0.41/layout-props.html)