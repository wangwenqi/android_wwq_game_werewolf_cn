#!/bin/bash 
# xcode Automatic 自动证书管理，会自动打一个develop的包，等到archive的时候，会重新签名成发布的。所以本地要安装adhoc和app store的pp文件
set -e
envriment="D"
archiveit="N"
target="E"
commandNum=0
while getopts "e:a:t:n:" arg #选项后面的冒号表示该选项需要参数
do
    case $arg in
        e)
			# echo "环境参数 arg:$OPTARG" #参数存在$OPTARG中
			envriment=$OPTARG
			;;
        a)
			# echo "是不是要打包 arg:$OPTARG"
			archiveit=$OPTARG
			;;
		t)
			# echo "发布位置" 
			target=$OPTARG
			;;
		n)
			commandNum=$OPTARG
			;;
        ?)  #当有不认识的选项的时候arg为?
			echo "unkonw argument"
			exit 1
		;;
    esac
done

des="./GVoice/libs"
simi=""
iphone=""
rightlib=1 # 0是正确的1是错误的
rightname="libGCloudVoice.a"
function checkname {
	archs="$(lipo -info "$1" | rev | cut -d ':' -f1 | rev)"
	if [[ "$archs" =~ armv7 ]]; then
		name=`echo "$1" | rev | cut -c 3-3 | rev`
		if [[ "$name" =~ e ]];then
			rightlib=0
		fi
		local lastPath=`echo "$1" | rev | cut -d '/' -f1 | rev`
		# echo $lastPath
		iphone=$1
	
	 else
	 	local lastPath=`echo "$1" | rev | cut -d '/' -f1 | rev`
		simi=$1
     fi
}

function changename {
	if [ $envriment == "D" ];then
		mv $iphone ./GVoice/libs/libGCloudVoice_i.a
		mv $simi ./GVoice/libs/libGCloudVoice.a
	else
		mv $simi ./GVoice/libs/libGCloudVoice_s.a
		mv $iphone ./GVoice/libs/libGCloudVoice.a
	fi
}

for file in $des/*; do
	checkname $file
done
changename

if [[ "$commandNum" == "1" ]];then
	echo "打包中……"
	xcodebuild archive -workspace game_werewolf.xcworkspace -scheme  game_werewolf -archivePath ~/Desktop/archive/werewolf.xcarchive -sdk iphoneos -configuration Release  | xcpretty
	if [ `echo $?`=="0" ];then
		say 打包成功
	else
		say 打包失败，请重新再试
	fi
fi 

if [ "$commandNum" == "2" ];then

	echo "导出为APPStore版本"
		xcodebuild -exportArchive  -archivePath ~/Desktop/archive/werewolf.xcarchive -exportPath  ~/Desktop/archive/ipa  -exportOptionsPlist ./ExportOptions_A.plist | xcpretty
		if [ `echo $?` == "0" ];then
			say 导出成功
		else
			say 导出失败，请重新再试
		fi
fi

if [ "$commandNum" == "5" ];then
	 echo "导出为adhoc版本"
	 xcodebuild -exportArchive  -archivePath ~/Desktop/archive/werewolf.xcarchive -exportPath  ~/Desktop/archive/ipa  -exportOptionsPlist ./ExportOptions_H.plist | xcpretty
		if [ `echo $?` == "0" ];then
			say 导出成功
			echo "上传到Fir"
			fir publish ~/Desktop/archive/ipa/game_werewolf.ipa --no-open --password test123
			if [ `echo $?` == "0" ];then
				say 上传成功
			else
				say 上传失败，请重新再试
			fi
		 else
			say 导出失败，请重新再试
		 fi
fi

if [ "$commandNum" == "4" ];then
	 echo "导出为开发版本"
	 xcodebuild -exportArchive  -archivePath ~/Desktop/archive/werewolf.xcarchive -exportPath  ~/Desktop/archive/ipa  -exportOptionsPlist ./ExportOptions_D.plist | xcpretty
		if [ `echo $?` == "0" ];then
			say 导出成功
			echo "上传到Fir"
			fir publish ~/Desktop/archive/ipa/game_werewolf.ipa --no-open --password test123
			if [ `echo $?` == "0" ];then
				say 上传成功
			else
				say 上传失败，请重新再试
			fi
		 else
			say 导出失败，请重新再试
		 fi
fi


if [ "$commandNum" == "3" ];then
	 echo "导出为企业版本"
	 xcodebuild -exportArchive  -archivePath ~/Desktop/archive/werewolf.xcarchive -exportPath  ~/Desktop/archive/ipa  -exportOptionsPlist ./ExportOptions_E.plist | xcpretty
		if [ `echo $?` == "0" ];then
			say 导出成功
			echo "上传到Fir"
			fir publish ~/Desktop/archive/ipa/game_werewolf.ipa --no-open --password test123
			if [ `echo $?` == "0" ];then
				say 上传成功
			else
				say 上传失败，请重新再试
			fi
		 else
			say 导出失败，请重新再试
		fi
fi


# if [ "$archiveit" == "Y" ];then
#  #    pod install
#  #    npm install
# 	# xcodebuild clean
# 	echo "打包中……"
# 	xcodebuild archive -workspace game_werewolf.xcworkspace -scheme game_werewolf -archivePath ~/Desktop/archive/werewolf.xcarchive | xcpretty
# 	if [ `echo $?`=="0" ];then
# 		say 打包成功
# 	else
# 		say 打包失败，请重新再试
# 	fi
# 	if [ "$target" == "A" ];then
# 		echo "导出为APPStore版本"
# 		xcodebuild -exportArchive  -archivePath ~/Desktop/archive/werewolf.xcarchive -exportPath  ~/Desktop/archive/ipa  -exportOptionsPlist ./ExportOptions_A.plist | xcpretty
# 		if [ `echo $?` == "0" ];then
# 			say 导出成功
# 		else
# 			say 导出失败，请重新再试
# 	fi
# 	else
# 		xcodebuild -exportArchive  -archivePath ~/Desktop/archive/werewolf.xcarchive -exportPath  ~/Desktop/archive/ipa  -exportOptionsPlist ./ExportOptions_E.plist | xcpretty
# 		if [ `echo $?` == "0" ];then
# 			say 导出成功
# 			echo "上传到Fir"
# 			fir publish ~/Desktop/archive/ipa/game_werewolf.ipa --no-open --password test123
# 			if [ `echo $?` == "0" ];then
# 				say 上传成功
# 			else
# 				say 上传失败，请重新再试
# 			fi
# 		else
# 			say 导出失败，请重新再试
# 		fi

# 	fi 
	
	
# fi





