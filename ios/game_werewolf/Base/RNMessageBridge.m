//
//  RNMessageBridge.m
//  game_werewolf
//
//  Created by Tony on 2017/8/19.
//  Copyright © 2017年 orangelab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(RNMessageSender, NSObject)

@end
