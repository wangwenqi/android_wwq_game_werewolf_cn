//
//  REMainViewController.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

@objc(MainViewController)
class MainViewController: RotationViewController, FBInterstitialAdDelegate {
  
    var interstitialAd: FBInterstitialAd?
  
    func setConfig() -> Void {
      NotificationCenter.default.addObserver(forName: NotifyName.kSocketDidOpen.name(), object: nil, queue: nil) { (notify) in
        SwiftConverter.shareInstance.sendMessageEnter()
      }
    }
  
    fileprivate var type: String?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      interstitialAd = FBInterstitialAd.init(placementID: KeyType.kFBAdKey.keys())
      if let interAd = interstitialAd {
        interAd.delegate = self
      }
      self.interstitialAd?.load()
    }

    func interstitialAdDidLoad(_ interstitialAd: FBInterstitialAd) {
      if interstitialAd.show(fromRootViewController: self) {
        print("广告展示成功")
      } else {
        print("广告并未展示")
      }
    }
  
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
      return .portrait
    }
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
      return .portrait
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


