//
//  RN_MessageSender.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/19.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

@objc(RNMessageSender)
class RNMessageSender: RCTEventEmitter {

  class func emitEvent(name: NSString, andPayload: NSDictionary) {
    NotificationCenter.default.post(name: Notification.Name.init("event-emitted"),
                                    object: self,
                                    userInfo: ["eventName": name, "payload": andPayload])
  }
  
  class func emitEvent(name: NSString) {
    self.emitEvent(name: name, andPayload: [:])
  }

  override func supportedEvents() -> [String]! {
    return ["EVENT_USER_LEAVE", "EVENT_CHAT", "LEAVE_EVENT_CHAT", "LEANCLOUD_NODE_RN", "CONTINUE_LOGIN", "EVENT_USER_LEAVE_FROM_WEB","EVENT_USER_START","ReactNativeBridge"]
  }

  override func startObserving() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(emitEventInternal(_:)),
                                           name: NotifyName.kEventEmitted.name(),
                                           object: nil)
  }
  
  override func stopObserving() {
    NotificationCenter.default.removeObserver(self)
  }

  
  
  func emitEventInternal(_ notification: Notification)  {
    let userInfo = notification.userInfo
    if let name = userInfo?["eventName"] as? String, let body = userInfo?["payload"] {
      self.sendEvent(withName: name, body: body)
    }
  }
  
}
