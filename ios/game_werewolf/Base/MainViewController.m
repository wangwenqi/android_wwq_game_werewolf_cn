//
//  MainViewController.m
//  game_werewolf
//
//  Created by 边宁 on 2017/3/26.
//  Copyright © 2017年 orangelab. All rights reserved.
//

#import "MainViewController.h"
#import "game_werewolf-Swift.h"
#import <React/RCTBridgeModule.h>
#import <React/RCTLog.h>
#import "MBProgressHUD.h"



@interface MainViewController ()<RCTBridgeModule>

@property(nonatomic, copy) NSString *type;
@end

@implementation MainViewController

RCT_EXPORT_MODULE();

- (void)viewDidLoad {
  [super viewDidLoad];
  // 放出广告
  self.interstitialAd = [[FBInterstitialAd alloc] initWithPlacementID:@"341507119614410_347856522312803"];
  self.interstitialAd.delegate = self;
  // For auto play video ads, it's recommended to load the ad
  // at least 30 seconds before it is shown
  [self.interstitialAd loadAd];
}

// MARK: 广告
- (void)interstitialAdDidLoad:(FBInterstitialAd *)interstitialAd
{
  NSLog(@"Ad is loaded and ready to be displayed");
  [interstitialAd showAdFromRootViewController:self];
}

-(void)setConfig {
  
  [[NSNotificationCenter defaultCenter] addObserverForName: @"socketDidOpen" object:NULL queue:NULL usingBlock:^(NSNotification * _Nonnull note) {
    NSLog(@"-------------------------------------------------");
    [[SwiftConverter shareInstance] sendMessageEnter];
  }];
  
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterRoom:) name:@"enter" object:NULL];
}

//  (this.state.password, this.state.game_type, this.state.roomId, this.state.userName,
//   this.state.userId, this.state.userImage, '',
RCT_EXPORT_METHOD(enter:(NSString *)password type: (NSString *)type :(NSString *)roomId :(NSString *)userName :(NSString *)userId :(NSString *)userImage :(NSString *)token)
{
  [[NSNotificationCenter defaultCenter]
   postNotificationName:SwiftConverter.enterToGameRoomNotificationName
   object:Nil
   userInfo:@{
              @"password": password,
              @"type": type,
              @"roomId": roomId,
              @"userName": userName,
              @"userId": userId,
              @"userImage": userImage,
              @"token": token
            }];
}

// RN调用友盟分享
RCT_EXPORT_METHOD(share)
{
  [[SwiftConverter shareInstance] share];
}

//检查更新
RCT_EXPORT_METHOD(checkUpdate)
{
  [[SwiftConverter shareInstance] checkUpdate];
}

RCT_EXPORT_METHOD(checkServiceRegion) {
  [[SwiftConverter shareInstance] checkServiceRegion];
}

//是否可以退出
RCT_EXPORT_METHOD(couldLogout:(RCTResponseSenderBlock)callback) {
    callback(@[[NSNull null],[[SwiftConverter shareInstance] couldLogout]]);
}
//充值
RCT_EXPORT_METHOD(openStoreRecharge) {
  [[SwiftConverter shareInstance] openStoreRecharge];
}
//常见问题
RCT_EXPORT_METHOD(openPayFAQ) {
  [[SwiftConverter shareInstance] openPayFAQ];
}
//商城
RCT_EXPORT_METHOD(onOpenStore) {
  [[SwiftConverter shareInstance] storeCenter];
}
//去任务中心
RCT_EXPORT_METHOD(gameAndMissionCenter) {
  [[SwiftConverter shareInstance] gameAndMissionCenter];
}

RCT_EXPORT_METHOD(loginSuccess:(NSString *)userId userName: (NSString *)name :(NSInteger)sex :(NSString *)avator :(NSString *)token :(BOOL)isTourist)
{
  [[SwiftConverter shareInstance] loginSuccessWithUserId:userId userName:name sex:sex avator:avator token:token isTourist:isTourist];
}

RCT_EXPORT_METHOD(modifyUserInfo:(NSString *)name :(NSInteger)sex :(NSString *)avator) {
  [[SwiftConverter shareInstance] modifyUserInfoWithUserName:name sex:sex avator:avator];
}

RCT_EXPORT_METHOD(didSelectUser:(NSString *)userId type: (NSString *)type sex:(NSString *)sex icon:(NSString *)icon name:(NSString *)name)
{
  NSLog(@"%@     %@        ", userId, type);
  [[SwiftConverter shareInstance] didSelectUserWithUerId:userId type:type sex:sex icon:icon name:name];
}

// 添加好友成功
RCT_EXPORT_METHOD(sendUserWelcomeMessage:(NSString *)userId userName: (NSString*)name)
{
  [[SwiftConverter shareInstance] sendUserWelcomeMessageWithUserId:userId and: name];
}

//用户反馈
RCT_EXPORT_METHOD(feedBack){
  [[SwiftConverter shareInstance] feedBack];
}

//请求好评
RCT_EXPORT_METHOD(appStoreReviewAndRating) {
  
  [[SwiftConverter shareInstance] appStoreReviewAndRating];
}
//获取当前版本号
RCT_EXPORT_METHOD(getCurrentVersion:(RCTResponseSenderBlock)callback)
{
  callback(@[[NSNull null],[[SwiftConverter shareInstance] getCurrentVersion]]);
}
//查看所有礼物详情
RCT_EXPORT_METHOD(showAllGifts:(NSString *)userId) {
  [[SwiftConverter shareInstance] showAllGiftsWithUserid:userId];
}

//权限请求

RCT_EXPORT_METHOD(checkPermission){
  [[SwiftConverter shareInstance] permissionCheck];
}

//设置地区
RCT_EXPORT_METHOD(setLocation:(NSString *)location) {
  [[SwiftConverter shareInstance] setLocationWithLc:location];
}

//登陆成功
RCT_EXPORT_METHOD(loginWithPlatefrom:(NSString *)plateform) {
  [[SwiftConverter shareInstance] loginWithPlateform:plateform];
}

//RN统计
RCT_EXPORT_METHOD(mobAnalytics:(NSString *)mobName) {
  [[SwiftConverter shareInstance] mobAnalyticsWithMobName:mobName];
}

-(void)enterRoom: (NSNotification *)note {
  
  [Utils runInMainThread:^{
    [XBHHUD hide];
    GameRoomViewController *controller =  [[SwiftConverter shareInstance] getGameRoomWithNotification:note];

    UIViewController *rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    // 判断 UINavigationController 是在点击创建房间时 RN也是弹出了一个viewcontroller，这样会导致弹不出界面。
    if(rootViewController.presentedViewController != NULL && [rootViewController.presentedViewController isKindOfClass:UINavigationController.class]) {
      // 从聊天消息弹出来了以后，需要找到 presentedViewController 才能弹出
      [rootViewController.presentedViewController presentViewController:controller animated:YES completion:NULL];
    } else {
      // 正常弹出的情况是这个
      [rootViewController presentViewController:controller animated:YES completion:NULL];
    }
  }];
}
@end
