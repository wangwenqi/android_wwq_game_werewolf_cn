//
//  RNMainVCBridge.m
//  game_werewolf
//
//  Created by Tony on
//  Copyright © 2017年 com.36kr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <React/RCTBridgeModule.h>


@interface RCT_EXTERN_MODULE(MainViewController,UIViewController)

RCT_EXTERN_METHOD(enter:(NSString * _Nullable)password :(NSString * _Nullable)type :(NSString * _Nullable)roomId :(NSString * _Nullable)userName :(NSString * _Nullable)userId :(NSString * _Nullable)userImage :(NSString * _Nullable)token)
RCT_EXTERN_METHOD(share)                  // RN调用友盟分享
RCT_EXTERN_METHOD(openPayFAQ)             // 支付常见问题
RCT_EXTERN_METHOD(checkUpdate)            //检查更新
RCT_EXTERN_METHOD(checkServiceRegion)
RCT_EXTERN_METHOD(gameAndMissionCenter)   //去任务中心
RCT_EXTERN_METHOD(rnStartMiniGame:(NSString * _Nonnull)gametype)      //开始小游戏
RCT_EXTERN_METHOD(rnStartMiniGameIntviu:(NSString * _Nonnull)title :(NSString * _Nonnull)message :(NSString * _Nonnull)url)  //小游戏邀请
RCT_EXTERN_METHOD(loginSuccess:(NSString * _Nonnull)userId userName:(NSString * _Nonnull)userName :(NSInteger)sex :(NSString * _Nonnull)avator :(NSString * _Nonnull)token :(BOOL)isTourist)
RCT_EXTERN_METHOD(modifyUserInfo:(NSString * _Nonnull)name :(NSInteger)sex :(NSString * _Nonnull)avator)

RCT_EXTERN_METHOD(didSelectUser:(NSString * _Nonnull)userId type:(NSString * _Nonnull)type sex:(NSString * _Nonnull)sex icon:(NSString * _Nonnull)icon name:(NSString * _Nonnull)name convId:(NSString * _Nonnull)convId)
RCT_EXTERN_METHOD(sendUserWelcomeMessage:(NSString * _Nonnull)userId userName:(NSString * _Nonnull)userName)
RCT_EXTERN_METHOD(feedBack)
RCT_EXTERN_METHOD(appStoreReviewAndRating)
RCT_EXTERN_METHOD(openStoreRecharge)
RCT_EXTERN_METHOD(openWebView:(NSString * _Nonnull)url)
RCT_EXTERN_METHOD(getCurrentVersion:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(showAllGifts:(NSString * _Nonnull)userId)
RCT_EXTERN_METHOD(checkPermission)
RCT_EXTERN_METHOD(setLocation:(NSString * _Nonnull)location)
RCT_EXTERN_METHOD(loginWithPlatefrom:(NSString * _Nonnull)plateform)
RCT_EXTERN_METHOD(mobAnalytics:(NSString * _Nonnull)mobName)
RCT_EXTERN_METHOD(couldLogout:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(onOpenStore)
RCT_EXTERN_METHOD(openCallUp)
RCT_EXTERN_METHOD(updateRequestUrl:(NSString *)url)
RCT_EXTERN_METHOD(sendMessageToNative:(NSString *)json)
RCT_EXTERN_METHOD(openLeaderboard)
RCT_EXTERN_METHOD(getDeviceId:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getGiftInfo:(NSString * _Nonnull)type :(RCTResponseSenderBlock)callback)





@end
