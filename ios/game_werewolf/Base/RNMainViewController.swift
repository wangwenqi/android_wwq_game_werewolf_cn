//
//  RNMainViewController.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

extension MainViewController {
  @objc func enter(_ password: NSString?, _ type: NSString?,_ roomId :NSString?,_ userName: NSString?,_ userId: NSString?,_ userImage:NSString?,_ token: NSString?)
  {
    if let id = userId as? String {
      CurrentUser.shareInstance.id = id
    }
    if CurrentUser.shareInstance.loadingGame {
       MobClick.event("MultipleAccess", label:CurrentUser.shareInstance.id ?? "yk")
      return
    }
    CurrentUser.shareInstance.loadingGame = true
    Utils.runInMainThread {
      XBHHUD.showLoading()
    }
    NotificationCenter.default.post(
      name: NotifyName.kEnterToGameRoom.name(),
      object: nil,
      userInfo: [
        "password": (password ?? ""),
        "type": (type ?? ""), // (type ?? "") //"pre_simple" ?? ""
        "roomId": (roomId ?? ""),
        "userName": (userName ?? ""),
        "userId": (userId ?? ""),
        "userImage": (userImage ?? ""),
        "token": (token ?? "")
      ])
  }

  
  // RN调用友盟分享
  @objc func share() {
    SwiftConverter.shareInstance.share()
  }
  
  //检查更新
  @objc func checkUpdate() {
    SwiftConverter.shareInstance.checkUpdate()
  }
  
  @objc func checkServiceRegion() {
    SwiftConverter.shareInstance.checkServiceRegion()
  }
  
  //去任务中心
  @objc func gameAndMissionCenter() {
    SwiftConverter.shareInstance.gameAndMissionCenter()
  }
  
  //去商城
  
  @objc func openStoreRecharge(){
    SwiftConverter.shareInstance.openStoreRecharge();
  }
  
  // 打开通用的URL
  @objc func openWebView(_ url: NSString){
    SwiftConverter.shareInstance.openWebView(url: url as String);
  }
  
  @objc func loginSuccess(_ userId: NSString, userName: NSString, _ sex: NSInteger, _ avator: NSString, _ token: NSString, _ isTourist: Bool) {
    SwiftConverter.shareInstance.loginSuccess(userId: userId as String, userName: userName as String, sex: sex, avator: avator as String, token: token as String, isTourist: isTourist)
  }
  
  @objc func couldLogout(_ callback : RCTResponseSenderBlock) {
    callback([NSNull(), SwiftConverter.shareInstance.couldLogout()])
  }
  
  @objc func modifyUserInfo(_ name: NSString,_ sex: NSInteger,_ avator: NSString) {
    SwiftConverter.shareInstance.modifyUserInfo(userName: name as String, sex: sex, avator: avator as String)
  }
  
  @objc func didSelectUser(_ userId: NSString, type: NSString, sex: NSString, icon: NSString, name: NSString, convId:NSString) {
    SwiftConverter.shareInstance.didSelectUser(uerId: userId as String, type: type as String, sex: sex as String, icon: icon as String, name: name as String, convId: convId as String);
  }
  
  // 添加好友成功
  @objc func sendUserWelcomeMessage(_ userId: NSString, userName: NSString) {
    print("=====\(userId)")
    SwiftConverter.shareInstance.sendUserWelcomeMessage(userId: userId as String, and: userName as String)
  }
  
  //用户反馈
  @objc func feedBack()  {
    SwiftConverter.shareInstance.feedBack()
  }
  
  @objc func onOpenStore() {
    SwiftConverter.shareInstance.storeCenter()
  }
  
  //请求好评
  @objc func appStoreReviewAndRating() {
    SwiftConverter.shareInstance.appStoreReviewAndRating()
  }
  
  // 支付相关问题
  @objc func openPayFAQ() {
    SwiftConverter.shareInstance.openPayFAQ()
  }
  
  //获取当前版本号
  @objc func getCurrentVersion(_ callback : RCTResponseSenderBlock) {
    callback([NSNull(), SwiftConverter.shareInstance.getCurrentVersion()])
  }
  
  //获取礼物图片
//  @objc func getGiftInfo(type:NSString,  _ callback: RCTResponseSenderBlock) {
//    callback([NSNull(), SwiftConverter.shareInstance.getCurrentVersion()])
//  }
  
  @objc func getGiftInfo(_ type:NSString, _ callback:RCTResponseSenderBlock) {
    callback([NSNull(),shareGift.shareInstance.rnGetThumailImaegBy(type: type as String)])
  }
  
  //获取当前UUID
  @objc func getDeviceId(_ callback : RCTResponseSenderBlock) {
    callback([NSNull(), Utils.getUUID() ?? ""])
  }
  
  //查看所有礼物详情
  @objc func showAllGifts(_ userId: NSString) {
    SwiftConverter.shareInstance.showAllGifts(userid: userId as String)
  }
  
  //权限请求
  @objc func checkPermission() {
    SwiftConverter.shareInstance.permissionCheck()
  }
  
  //设置地区
  @objc func setLocation(_ location: NSString) {
    SwiftConverter.shareInstance.setLocation(lc: location as String)
  }
  
  //登陆成功
  @objc func loginWithPlatefrom(_ plateform: NSString) {
    SwiftConverter.shareInstance.loginWith(Plateform: plateform as String)
  }
  
  //RN统计
  @objc func mobAnalytics(_ mobName: NSString) {
    SwiftConverter.shareInstance.mobAnalytics(mobName: mobName as String)
  }
  
  //进入个性房间
  @objc func openCallUp() {
    SwiftConverter.shareInstance.openCallUp()
  }
  
  //进入排行榜
  @objc func openLeaderboard() {
    SwiftConverter.shareInstance.openLeaderboard()
  }
  
  //开始游戏
  @objc func rnStartMiniGame(_ gameType:String) {
    SwiftConverter.shareInstance.rnStartMiniGame(gameType)
  }
  
  //开启游戏分享邀请
  @objc func rnStartMiniGameIntviu(_ title:String, _ message:String, _ url:String) {
    SwiftConverter.shareInstance.rnStartMiniGameIntviu(title, message: message, url)
  }

  @objc func sendMessageToNative(_ json:String) {
    SwiftConverter.shareInstance.sendMessageToNative(json)
  }
  
}
