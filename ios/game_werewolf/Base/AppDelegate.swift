//
//  REAppDelegate.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/19.
//  Copyright © 2017年 orangelab. All rights reserved.
//


import UIKit
import ChatKit
//import CocoaLumberjack
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var params = [String: String]()
  var window: UIWindow?
  
  func application(_ application: UIApplication,
                   continue userActivity: NSUserActivity,
                   restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
 
    let keyArray = ["roomid", "password", "type"]
    if(userActivity.activityType == NSUserActivityTypeBrowsingWeb) {
      if let query = userActivity.webpageURL?.query {
        _ = query.components(separatedBy: "&").map({ (tmpString) -> [String: String] in
          let tmpArray = tmpString.components(separatedBy: "=")
          if let key = tmpArray.first, keyArray.contains(key) {
            return [key: tmpArray.last ?? ""]
          }
          return [tmpArray.last ?? "": tmpArray.first ?? ""]
        }).reduce(params, { (reslut, object) -> [String: String] in
          for (key, value) in object {
            params[key] = value
          }
          return params
        })
        
        if SwiftConverter.shareInstance.isGaming() {
          XBHHUD.showError(NSLocalizedString("您已经在房间中", comment: ""))
          return true
        }

        CurrentUser.shareInstance.currentRoomPassword = params["password"] ?? ""
        CurrentUser.shareInstance.currentRoomType = params["type"] ?? ""
        CurrentUser.shareInstance.currentRoomID = params["roomid"] ?? ""
        
        if CurrentUser.shareInstance.token.length > 1 {
          NotificationCenter.default.post(
            name: NotifyName.kEnterToGameRoom.name(),
            object: nil,
            userInfo: [
              "password": CurrentUser.shareInstance.currentRoomPassword,
              "type": CurrentUser.shareInstance.currentRoomType,
              "roomId": CurrentUser.shareInstance.currentRoomID,
              "userName": CurrentUser.shareInstance.name,
              "userId": CurrentUser.shareInstance.id,
              "userImage": CurrentUser.shareInstance.avatar,
              "token": CurrentUser.shareInstance.token
            ])
        }
        
        
      }
    }
    return true;
  }
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // warning 测试广告
    //    FBAdSettings.addTestDevice(FBAdSettings.testDeviceHash())

    //生成UUID
    Utils.checkAndGenerateUniqueIdentifier()
    //检查是否需要下载新的礼物信息
    GiftSourceManager.checkVersion()
    //本地日志
    #if DEBUG
//      DDLog.add(DDTTYLogger.sharedInstance, with: .info)
//      DDLog.add(DDASLLogger.sharedInstance, with: .info)
//      let fileLogger: DDFileLogger = DDFileLogger() // File Logger
//      fileLogger.rollingFrequency = TimeInterval(60*60*24)  // 24 hours
//      fileLogger.logFileManager.maximumNumberOfLogFiles = 7
//      DDLog.add(fileLogger)
    #endif
    if let _ = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] {
      SwiftConverter.shareInstance.appRunning = false
    } else {
      SwiftConverter.shareInstance.appRunning = true
    }
    self.window = UIWindow.init(frame: UIScreen.main.bounds)
    // 正常逻辑?
    let state = DevSetting(rawValue: SwiftConverter.shareInstance.getCurrentVersion()) ?? DevSetting.release
    switch state {
      
    case .test, .release:
      var jsCodeLocation: URL?
      #if DEBUG
        jsCodeLocation = RCTBundleURLProvider.sharedSettings().jsBundleURL(forBundleRoot: "index.ios", fallbackResource: "")
      #else
        jsCodeLocation = URL.init(fileURLWithPath: Bundle.main.path(forResource: "index.ios.jsbundle", ofType: "")!)
      #endif
      
      var bundelName = "game_werewolf"
      #if XIAOYU
        bundelName = "xiaoyu"
      #else
      #endif
      let rootVC = SwiftConverter.shareInstance.getAdViewController()
      rootVC.afterADCallback = { _ in
        let rootView = RCTRootView.init(bundleURL: jsCodeLocation,
                                        moduleName: "game_werewolf",
                                        initialProperties: [:],
                                        launchOptions: launchOptions)
        rootView?.backgroundColor = UIColor.rgb(1.0, 1.0, 1.0)
        let newRootVC = MainViewController()
        newRootVC.view = rootView
        self.window?.rootViewController = newRootVC
        // 这里不需要了
//        newRootVC.setConfig()
      }
      self.window?.rootViewController = rootVC
      self.window?.makeKeyAndVisible()
    case .dev:
      // 程序启动直接进入游戏房间, 方便调试
//      let controller = SwiftConverter.shareInstance.getEnterRoomController()
//      self.window?.rootViewController = controller
        print("dd")
    default:
      print("当前环境为" + state.rawValue)
    }
    
    // 初始化友盟分享
    SwiftConverter.shareInstance.initialize()
    self.window?.makeKeyAndVisible()
    
    // 初始化Bugly
    Bugly.start(withAppId: SwiftConverter.shareInstance.buglyKey)

    NotificationCenter.default.addObserver(forName: NotifyName.kAbortApp.name(),
                                           object: nil,
                                           queue: nil) { (note) in
//      abort()
    }
    //初始化数据
    Utils.gnerateDefaultSL()
    let leanCloudID = KeyType.kLeanCloudId_CN.keys()
    let leanCloudKey = KeyType.kleanCloudKey_CN.keys()
    //leancloud 代理
    if let cached = Utils.getCachedSVList() {
      if let api = cached["api"] as? String {
        if api != "" {
           AVOSCloud.setServerURLString(api , for: .API)
        }
      }
      if let rtm = cached["rtm"] as? String {
        if rtm != "" {
             AVOSCloudIM.defaultOptions().rtmServer = rtm
        }
      }
    }else{
      Utils.gnerateDefaultSL()
      AVOSCloud.setServerURLString("https://intviu-api.leancloud.cn", for: .API)
      AVOSCloudIM.defaultOptions().rtmServer = "wss://intviu-rtm.leancloud.cn"
    }
    AVOSCloud.setApplicationId(leanCloudID, clientKey: leanCloudKey)
    LCChatKit.setAppId(leanCloudID, appKey: leanCloudKey)
    #if DEBUG
    //leancloud log 
    AVOSCloud.setAllLogsEnabled(true)
    AVOSCloud.setLogLevel(AVLogLevelVerbose)
    #endif
    LCChatKitExample.invokeThisMethodInDidFinishLaunching()
    LCChatKit.sharedInstance().disableSingleSignOn = true
    self.remotePush()
    // 解决RN闪屏问题
    SplashScreen.show()
    //网络情况监听
     NetworkManager.shared.startNetworkReachabilityObserver()
    return true
  }
  
  func remotePush() {
    if UserDefaults.standard.object(forKey: "PERMISSION_REMOTE") != nil || UserDefaults.standard.object(forKey: "PERMISSION_NOT_FIRST") != nil {
      //PERMISSION_REMOTE 新用户首次注册不直接显示推送，PERMISSION_NOT_FIRST 老用户安装不影响，可以直接使用
      let settings = UIUserNotificationSettings.init(types: [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound], categories: nil)
      UIApplication.shared.registerUserNotificationSettings(settings)
      UIApplication.shared.registerForRemoteNotifications()
    }
  }
  
  func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    
    // 微信登陆: wxa7b819d69796b241://oauth?code=001U3wJD0Tk9ng2pVgHD094uJD0U3wJb&state=123
    // 微信(和微信朋友圈)分享: wxa7b819d69796b241://platformId=wechat
    // QQ登陆: tencent1106115686://qzapp/mqzone/0?generalpastboard=1
    // QQ分享: QQ41edfc66://response_from_qq?source=qq&source_scheme=mqqapi&error=0&version=1&sdkv=3.1
    let absoluteString = url.absoluteString
    if absoluteString.hasPrefix("wx") {    // 微信
      if absoluteString.contains("oauth") { // 登录行为
        return RCTLinkingManager.application(application,
                                             open:url,
                                             sourceApplication:sourceApplication,
                                             annotation:annotation)
      }
    }
    
    if absoluteString.hasPrefix("tencent") {    // QQ
      if absoluteString.contains("generalpastboard=1") {
        return RCTLinkingManager.application(application,
                                             open:url,
                                             sourceApplication:sourceApplication,
                                             annotation:annotation)
      }
    }
    
    if absoluteString.hasPrefix("line3rdp.cn.orangelab.werewolf") {
      LineAdapter.handleOpen(url)
    }
    // 分享回来行为
    return UMSocialManager.default().handleOpen(url, sourceApplication: sourceApplication, annotation: annotation)
    // TODO 这边需要测试区分登陆和分享的区别
    //  if ([scheme rangeOfString: @"fb"].location == 0) {
    //    [[FBSDKApplicationDelegate sharedInstance]
    //     application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    //  }
  }
  
  //- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
  //{
  //  BOOL result = [[UMSocialManager defaultManager]  handleOpenURL:url options:options];
  //  if (!result) {
  //    // 其他如支付等SDK的回调
  //    [RCTLinkingManager application:app openURL:url sourceApplication:nil annotation:nil];
  //  }
  //  return result;
  //}
  
  // 成功注册pushToken
  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    AVOSCloud.handleRemoteNotifications(withDeviceToken: deviceToken)
  }
  // 注册失败
  func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    
  }
  
  //// 成功收到通知
  //-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
  ////  [[SwiftConverter shareInstance] didReceiveRemoteNotificationWithUserInfo:userInfo];
  //}
  
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//    if UIApplication.shared.applicationState == .inactive ||  UIApplication.shared.applicationState == .background || SwiftConverter.shareInstance.appRunning == false  {
//      SwiftConverter.shareInstance.remoteInfo = userInfo as? [String : Any]
//    }
    SwiftConverter.shareInstance.didReceiveRemoteNotification(userInfo: userInfo as! [String : Any])
  }

  
  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    LCChatKitExample.invokeThisMethod(inApplicationWillResignActive: application)
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    // 发送leaveApp
    RequestManager().get(url: RequestUrls.leaveApp, success: nil, error: nil)
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    SwiftConverter.shareInstance.willEnterForeground()
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    RequestManager().get(url: RequestUrls.resumeApp, success: nil, error: nil)
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    LCChatKitExample.invokeThisMethod(inApplicationWillTerminate: application)
  }
  
  func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
    
    if self.window?.rootViewController != nil {
      if  Utils.getCurrentVC(vc: (self.window?.rootViewController)!) is MicroGameViewController {
        return UIInterfaceOrientationMask.allButUpsideDown
      }else{
        return  UIInterfaceOrientationMask.portrait
      }
    }else{
      return  UIInterfaceOrientationMask.portrait
    }
   }

}
