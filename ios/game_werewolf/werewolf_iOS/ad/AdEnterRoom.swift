
//
//  AdEnterRoom.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/22.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class AdEnterRoom: UIView {

  var nativeAd: FBNativeAd?
  
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var adMediaView: FBMediaView!
    @IBOutlet weak var downLoadButton: UIButton!
    @IBOutlet weak var adChoiceView: FBAdChoicesView!
    @IBOutlet weak var detailButton: UILabel!
    
  class func adStart(_ frame: CGRect, nativeAd: FBNativeAd?) -> AdEnterRoom {
    let view = Bundle.main.loadNibNamed("AdEnterRoom", owner: nil, options: nil)?.last as! AdEnterRoom
    view.frame = frame
    if let tmpAd = nativeAd {
      view.nativeAd = tmpAd
    }
    return view
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    if let adInfo = nativeAd {
      adInfo.unregisterView()
      adInfo.registerView(forInteraction: self, with: nil)
      self.adMediaView.nativeAd = adInfo
      adInfo.icon?.loadAsync(block: { [weak self](image) in
        guard let weak = self else { return }
        weak.titleImageView.image = image
      })
      self.titleLabel.text = adInfo.title
      self.subTitleLabel.text = adInfo.body
      self.detailButton.text = adInfo.socialContext
      self.downLoadButton.setTitle(adInfo.callToAction, for: .normal)
      self.adChoiceView.nativeAd = nativeAd
      self.adChoiceView.corner = .topRight
      self.adChoiceView.isHidden = false
    }
  }
  
}
