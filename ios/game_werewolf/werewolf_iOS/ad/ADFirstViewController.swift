//
//  ADFirstViewController.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/7/25.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation
import UIKit;

class ADFirstViewController:RotationViewController {
  
  var afterADCallback:(() -> Void)?;
  
  let guanggao = FBInterstitialAd.init(placementID: KeyType.kFBAdKey.keys());
  
  var showTimmer:Timer? = nil;
  
  override func viewDidLoad() {
    guanggao.delegate = self;
    print("加载广告:::\(Date())");
    showTimmer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(ADFirstViewController.goInGame), userInfo: nil, repeats: false);
//    showTimmer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(ADFirstViewController.goInGame), userInfo: nil, repeats: false);
    
    RequestManager().get(url: RequestUrls.showAdConfig, success: { [weak self] (response) in
      if let result = response.bool,
        result == true  {
        self?.guanggao.load();
      } else {
        self?.goInGame();
      }
    }, error: { [weak self] (code, message) in
      self?.goInGame();
    });
  }
  
  func goInGame() {
    showTimmer?.invalidate();
    if let callback = self.afterADCallback {
      callback();
    }
  }
  
  func forceGoInGame(isTimeout: Bool) {
    goInGame();
  }
  
  deinit {
    print("这个viewcontroller被清除:::\(Date())");
  }
}

// 广告
extension ADFirstViewController: FBInterstitialAdDelegate {
  func interstitialAdDidLoad(_ interstitialAd: FBInterstitialAd) {
    print("----interstitialAdDidLoad-----")
//    let alert = UIAlertView.init(title: "广告", message: "interstitialAdDidLoad", delegate: nil, cancelButtonTitle: "好");
//    alert.show();
    showTimmer?.invalidate();
    interstitialAd.show(fromRootViewController: self);
    SplashScreen.hide();
  }
  
  func interstitialAdDidClick(_ interstitialAd: FBInterstitialAd) {
    print("----interstitialAdDidClick-----")
//    let alert = UIAlertView.init(title: "广告", message: "interstitialAdDidClick", delegate: nil, cancelButtonTitle: "好");
//    alert.show();
    forceGoInGame(isTimeout: false);
  }
  
  func interstitialAdDidClose(_ interstitialAd: FBInterstitialAd) {
    print("----interstitialAdDidClose-----")
//    let alert = UIAlertView.init(title: "广告", message: "interstitialAdDidClose", delegate: nil, cancelButtonTitle: "好");
//    alert.show();
    forceGoInGame(isTimeout: false);
  }
  
  func interstitialAd(_ interstitialAd: FBInterstitialAd, didFailWithError error: Error) {
    print("----error:\(error.localizedDescription)-----")
//    let alert = UIAlertView.init(title: "广告", message: "\(error.localizedDescription)-\(error)", delegate: nil, cancelButtonTitle: "好");
//    alert.show();
    forceGoInGame(isTimeout: false);
  }
}
