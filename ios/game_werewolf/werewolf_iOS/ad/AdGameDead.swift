//
//  AdGameDead.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/22.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class AdGameDead: UIView {

  var nativeAd: FBNativeAd?
  
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var adMediaView: FBMediaView!
    @IBOutlet weak var downButton: UIButton!
    @IBOutlet weak var adChoiceView: FBAdChoicesView!
    @IBOutlet weak var freeDetailLabel: UILabel!
  
  class func adStart(_ frame: CGRect, _ nativeAd: FBNativeAd?) -> AdGameDead {
    let view = Bundle.main.loadNibNamed("AdGameDead", owner: nil, options: nil)?.last as! AdGameDead
    view.frame = frame
    if let ad = nativeAd {
      view.nativeAd = ad
    }
    return view
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    if let adInfo = nativeAd {
      adInfo.unregisterView()
      adInfo.registerView(forInteraction: self, with: nil)
      self.adMediaView.nativeAd = adInfo
      adInfo.icon?.loadAsync(block: { [weak self](image) in
        guard let weak = self else { return }
        weak.titleImageView.image = image
      })
      self.titleLabel.text = adInfo.title
      self.detailLabel.text = adInfo.body
      self.freeDetailLabel.text = adInfo.socialContext
      self.downButton.setTitle(adInfo.callToAction, for: .normal)
      self.adChoiceView.nativeAd = nativeAd
      self.adChoiceView.corner = .topRight
      self.adChoiceView.isHidden = false
    }
  }

}
