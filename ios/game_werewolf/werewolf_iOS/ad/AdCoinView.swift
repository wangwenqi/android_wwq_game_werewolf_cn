//
//  AdCoinView.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/21.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class AdCoinView: UIView, FBNativeAdDelegate {

  var nativeAd: FBNativeAd?
  
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var adTitleLabel: UILabel!
    @IBOutlet weak var adBodyLabel: UILabel!
    @IBOutlet weak var sponsoreLabel: UILabel!
    @IBOutlet weak var adChooseButton: FBAdChoicesView!
    @IBOutlet weak var adCoverMediaView: FBMediaView!
    @IBOutlet weak var adCallBackButton: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
  override func awakeFromNib() {
    super.awakeFromNib()
  }

  class func adStart(_ frame: CGRect, _ nativeAd :FBNativeAd?) -> AdCoinView {
    let view = Bundle.main.loadNibNamed("AdCoinView", owner: nil, options: nil)?.last as! AdCoinView
    view.frame = frame
    view.adChooseButton.isHidden = true
    if let ad = nativeAd {
      view.nativeAd = ad
    }
    return view
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    if let adInfo = nativeAd {
      adInfo.unregisterView()
      adInfo.registerView(forInteraction: self, with: nil)
      self.adCoverMediaView.nativeAd = adInfo
      adInfo.icon?.loadAsync(block: { [weak self](image) in
        guard let weak = self else { return }
        weak.titleImageView.image = image
      })
      self.adTitleLabel.text = adInfo.title
      self.adBodyLabel.text = adInfo.body
      self.sponsoreLabel.text = adInfo.socialContext
      self.adCallBackButton.setTitle(adInfo.callToAction, for: .normal)
      self.adChooseButton.nativeAd = nativeAd
      self.adChooseButton.corner = .topLeft
      self.adChooseButton.isHidden = false
    }
  }
  
  override func showLoading() {
    super.showLoading()
  }
  
}

