//
//  OLSystemMessageCell.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/3.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import ChatKit

class OLSystemMessageCell: LCCKChatTextMessageCell {
  
  override static func classMediaType() -> AVIMMessageMediaType {
    return AVIMMessageMediaType(rawValue: Int8(kAVIMMessageMediaTypeSystem))!;
  }
}
