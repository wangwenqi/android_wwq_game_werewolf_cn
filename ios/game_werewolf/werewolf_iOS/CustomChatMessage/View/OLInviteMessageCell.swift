//
//  OLInviteMessageCell.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/5/25.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation
import ChatKit;


class OLInviteMessageCell: LCCKChatMessageCell, LCCKChatMessageCellSubclassing {
  
  var inviteView:OLInviteView?
  
  static func classMediaType() -> AVIMMessageMediaType {
    return AVIMMessageMediaType(rawValue: Int8(kAVIMMessageMediaTypeInvite))!
  }
  
  // MARK: - LCCKChatMessageCell
  override func setup() {
    super.setup()
    self.messageContentBackgroundImageView.isUserInteractionEnabled = true
    self.contentView.addSubview(self.messageContentView)
    self.contentView.addSubview(self.messageContentBackgroundImageView)
    self.contentView.addSubview(self.avatarImageView)
    self.contentView.addSubview(self.nickNameLabel)
    self.updateConstraintsIfNeeded()
    
    //添加点击事件    
    let tap = UITapGestureRecognizer.init(target: self, action: #selector(avatarTap(tap:)))
    self.avatarImageView.isUserInteractionEnabled = true
    self.avatarImageView.addGestureRecognizer(tap)
    
  }
  
  func avatarTap(tap:UITapGestureRecognizer) {
    if (tap.state == .ended) {
      if self.messageOwner != .self {
        self.delegate.messageCellTappedHead(self)
      }
    }
  }
  
  override func updateConstraints() {
    
    super.updateConstraints()
    
    self.messageContentBackgroundImageView.snp.makeConstraints { (make) in
      make.width.equalTo(240)
      make.height.equalTo(90)
    }
    
  }
  
  override func configureCell(withData message: Any!) {
    super.configureCell(withData: message);
    if let inviteMessage = message as? AVIMTypedMessage {
      let view = getInviteView();
      if let attributes = inviteMessage.attributes as? [String:Any] {
        view.attributes = attributes;
      }
    }
  }
}

extension OLInviteMessageCell {
  func getInviteView() -> OLInviteView {
    if (inviteView != nil) {
      return inviteView!;
    }
    inviteView = Bundle.main.loadNibNamed("OLInviteView", owner: nil, options: nil)?.last as! OLInviteView;
    
    self.messageContentBackgroundImageView.addSubview(inviteView!);
    
    inviteView?.mas_makeConstraints({ (make:MASConstraintMaker?) in
      make?.height.equalTo()(self.inviteView?.superview);
      make?.width.equalTo()(self.inviteView?.superview);
      make?.center.equalTo()(self.inviteView?.superview);
    });
    
    return inviteView!;
  }
}
