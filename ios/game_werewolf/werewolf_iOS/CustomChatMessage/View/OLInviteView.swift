//
//  OLInviteView.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/5/25.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation

class OLInviteView: UIView {
    
    
    @IBOutlet weak var roomNameLable: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var passwordLable: UILabel!
  
  var attributes:[String:Any] = [:] {
    didSet {
      reloadView();
    }
  };
  
  override func awakeFromNib() {
    super.awakeFromNib();
    self.setup();
  }
  
  func setup() {
    self.isUserInteractionEnabled = true;
    let tapGest = UITapGestureRecognizer(target: self, action: #selector(inviteClicked));
    self.addGestureRecognizer(tapGest);
  }
  
  func reloadView() {
    print(attributes)
    // child_type
//    "GAME_TYPE": audio, "conversationType": 0,
    if let gameType = attributes["GAME_TYPE"] as? String, gameType == "audio" {
      titleLabel.text = "邀请你加入语音房"
      if let roomType = attributes["child_type"] as? String, !roomType.isEmpty {
        titleLabel.text = "邀请你加入\(roomType)"
      }
    }
    if let roomId = attributes[INVITE_MSG_KEY_ROOM_ID] as? String {
        let localRoom = NSLocalizedString("房间号", comment: "")
        roomNameLable.text = "\(localRoom) \(roomId)"
    }
    var password = NSLocalizedString("无", comment: "");
    if let pwd = attributes[INVITE_MSG_KEY_PASSWORD] as? String, pwd != "" {
        password = pwd;
    }
    let localPass = NSLocalizedString("密码", comment: "")
    passwordLable.text = "密码 \(password)"
  }
  
  func inviteClicked() {
    //如果在游戏中就不进去
    if SwiftConverter.shareInstance.isGaming() {
      XBHHUD.showError(NSLocalizedString("您已经在一局游戏中,不能再进入游戏", comment: ""))
      return
    }
    if CurrentUser.shareInstance.loadingGame {
      MobClick.event("MultipleAccess", label:CurrentUser.shareInstance.id ?? "yk")
      return
    }
    CurrentUser.shareInstance.loadingGame = true
    if let roomId = attributes[INVITE_MSG_KEY_ROOM_ID] as? String,
      let password = attributes[INVITE_MSG_KEY_PASSWORD] as? String,
      let type = attributes["GAME_TYPE"] as? String {
      NotificationCenter.default.post(
        name: NotifyName.kEnterToGameRoom.name(),
        object: nil,
        userInfo: [
          "password": password,
          "type": type,
          "roomId": roomId,
          "userName": CurrentUser.shareInstance.name,
          "userId": CurrentUser.shareInstance.id,
          "userImage": CurrentUser.shareInstance.avatar,
          "token": CurrentUser.shareInstance.token
        ]);
    }
  }
}
