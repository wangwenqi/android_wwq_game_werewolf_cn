//
//  OLGiftView.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/7/3.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation

class OLGiftView: UIView {
  
    @IBOutlet weak var giftImageView: UIImageView!
    
    @IBOutlet weak var giftDesLabel: UILabel!
  
    @IBOutlet weak var rebateLabel: UILabel!
    @IBOutlet weak var popularLabel: UILabel!
    var attributes:[String:Any] = [:] {
    didSet {
      //返利
      if let reabate = attributes["GIFT_REBATE"] as? String {
        if reabate == "" || reabate.isEmpty {
           rebateLabel.isHidden = true
        }else{
           rebateLabel.isHidden = false
           rebateLabel.text = NSLocalizedString("钻石", comment:"") + "+\(reabate)"
        }
      }else{
        rebateLabel.isHidden = true 
      }
      if let type = attributes["GIFT_TYPE"] as? String {
         let paths = shareGift.shareInstance.getThumbnailImagePopularAndName(type: type)
          if let imagePath = paths["gift_image"] as? String,
            let name = paths["gift_name"] as? String{
            if imagePath != "" {
              giftImageView.image = UIImage(contentsOfFile: imagePath)
            }else{
              if let image =  OLGiftType.getCardPic(type: type) {
                 giftImageView.image = image
                 giftDesLabel.text = OLGiftType.getGiftDescription(type: type)
              }else{
                 giftImageView.image = OLGiftType.getGiftPic(type: "");
                 giftDesLabel.text = OLGiftType.getGiftDescription(type: "")
              }
            }
            if let name = paths["gift_name"] as? String {
              if name != "" {
                giftDesLabel.text = name
              }else{
                if let image =  OLGiftType.getCardPic(type: type) {
                  giftImageView.image = image
                  giftDesLabel.text = OLGiftType.getGiftDescription(type: type)
                }else{
                  giftImageView.image = OLGiftType.getGiftPic(type: "");
                  giftDesLabel.text = OLGiftType.getGiftDescription(type: "")
                }
              }
            }
            
            if let pop = paths["gift_pop"] as? String {
                popularLabel.isHidden = false
                popularLabel.text = NSLocalizedString("人气", comment: "") + "+\(pop)"
            }
          }
      } else {
        giftImageView.image = OLGiftType.getGiftPic(type: "");
        giftDesLabel.text = OLGiftType.getGiftDescription(type: "");
      }
    }
  }
}
