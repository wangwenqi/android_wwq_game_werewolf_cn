//
//  OLGameInvite.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/19.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit


class OLGameInvite: UIView {
  
  @IBOutlet weak var countDownView: UIView!
  @IBOutlet weak var countDownLable: UILabel!
  @IBOutlet weak var inviteTitle: UILabel!
  @IBOutlet weak var contentPaddingBottom: NSLayoutConstraint!
  @IBOutlet weak var gameImageView: UIImageView!
  @IBOutlet weak var topView: UIView!
  @IBOutlet weak var topImage: UIImageView!
  @IBOutlet weak var refuseInvite: UIButton!
  @IBOutlet weak var bgImageView: UIImageView!
    
  var gameType:String?
  var gameName:String?
  var gameCountDown:Int64?
  var gameIcon:String?
  var userID:String?
  var timer:Timer?
  var gameSendTime:Int64?
  var isSelf:Bool = false
  var canNotClick = false
    
   @IBAction func cancleButtonClicked(_ sender: Any) {
    if isSelf == false {
      let payload = ["type":"refuse_invite","game_type":gameType,"invite_user_id":userID]
      messageDispatchManager.sendControlMessage(type:ControlMessage.play_mini_game.rawValue, payLoad: payload)
    }
   }

   var attributes:[String:Any] = [:] {
      didSet {
        reloadView();
      }
   }
  
  var inviteIsNotAvaliableBlock:(()->Void)?
  var inviteIsAvaliableBlock:(()->Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib();
    self.setup();
  }

  func reloadView() {
    /*
    "USER_SEX": "\(CurrentUser.shareInstance.sex)",
    "USER_ICON": CurrentUser.shareInstance.avatar,
    "USER_NAME": CurrentUser.shareInstance.name,
    "USER_ID": CurrentUser.shareInstance.id,
    "APP": Config.app,
    "MINI_GAME_TYPE": "wzq",
    "MINI_GAME_NAME":"wzq",
    "MINI_COUNTDOWN_TIME":20, //单位秒
    "CHAT_TIME": Utils.getCurrentTimeStamp()
    */

    if let user_id = attributes["USER_ID"] as? String {
      userID = user_id
      if CurrentUser.shareInstance.id == userID {
        isSelf = true
      }else{
        isSelf = false
      }
    }
    if let type = attributes["MINI_GAME_TYPE"] as? String{
      gameType = type
    }
    if let name = attributes["MINI_GAME_NAME"] as? String{
      gameName = name
      //邀请说明
      inviteTitle.text = gameName
    }
    if let time = attributes["MINI_COUNTDOWN_TIME"] as? Int64{
      gameCountDown = time
    }
    if let url = attributes["MINI_GAME_ICON"] as? String{
      gameIcon = url
      if let imagUrl = URL.init(string: url) {
         gameImageView.sd_setImage(with: imagUrl, placeholderImage: UIImage.init(named: "room_head_default"))
      }
    }
    if let sendTime = attributes["CHAT_TIME"] as? Int64{
      if gameCountDown == nil {
        return
      }
      gameSendTime = sendTime
//      timer?.invalidate()
      if Utils.getCurrentTimeStamp() - sendTime >= gameCountDown! * 1000 {
        //如果现在的时间，已经超过倒计时的时间，就显示为0，并且所有按钮都不不能按
        canNotClick = true
        notAvaliable()
      }else{
         //启动倒计时
          timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(checkTime), userInfo: nil, repeats: true)
      }
    }
  }
  
  func checkTime() {
    if gameCountDown == nil {
      return
    }
    if Utils.getCurrentTimeStamp() - Int64(gameSendTime!) >= gameCountDown! * 1000 {
      //如果现在的时间，已经超过倒计时的时间，就显示为0，并且所有按钮都不不能按
      timer?.invalidate()
      canNotClick = true
      notAvaliable()
    }else{
      self.countDownView.isHidden = false
      isAvalialbe()
      let stime =  (Int64(gameSendTime!) + gameCountDown! * 1000 - Utils.getCurrentTimeStamp())/1000
      self.countDownLable.text = "\(stime)s"
    }
  }
  func setup() {
    self.isUserInteractionEnabled = true
    let tapGest = UITapGestureRecognizer(target: self, action: #selector(inviteClicked))
    self.addGestureRecognizer(tapGest)
  }
  
  func isAvalialbe() {
    if self.isSelf == false {
      self.refuseInvite.isHidden = false
    }else{
      self.refuseInvite.isHidden = true
    }
    self.refuseInvite.isEnabled = true
    self.topView.backgroundColor = UIColor.hexColor("ff3eb1")
    if let lange = Bundle.main.preferredLocalizations.first {
      if lange.contains("Hans") {
        //简体
        self.topImage.image = UIImage.init(named: "挑战hans")
      }else if lange.contains("Hant") {
        //繁体
        self.topImage.image = UIImage.init(named: "挑战hant")
      }else{
        self.topImage.image = UIImage.init(named: "挑战hans")
      }
    }
    self.bgImageView.image = UIImage.init(named: "挑战背景")
    canNotClick = false
  }
  
  func notAvaliable() {
      self.countDownView.isHidden = true
      self.refuseInvite.isEnabled = false
      self.refuseInvite.isHidden = false
      self.topView.backgroundColor = UIColor.hexColor("858585")
      if let lange = Bundle.main.preferredLocalizations.first {
        if lange.contains("Hans") {
          //简体
          self.topImage.image = UIImage.init(named: "结束hans")
        }else if lange.contains("Hant") {
          //繁体
          self.topImage.image = UIImage.init(named: "结束hant")
        }else{
        self.topImage.image = UIImage.init(named: "结束hans")
        }
      }
      //背景变灰
     self.bgImageView.image = UIImage.init(named: "结束背景")
  }
  
  func inviteClicked() {
    //如果在游戏中就不进去
    if SwiftConverter.shareInstance.isGaming() {
      XBHHUD.showError(NSLocalizedString("您已经在一局游戏中,不能再进入游戏", comment: ""))
      return
    }
    if isSelf {
      return
    }
    if canNotClick {
       return 
    }
    let payload = ["type":"accept_invite","game_type":gameType,"invite_user_id":userID]
    messageDispatchManager.sendControlMessage(type:ControlMessage.play_mini_game.rawValue, payLoad: payload)
  }
}
