//
//  OLGiftInputView.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/7/3.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation
import ChatKit;

let kAVIMMessageMediaTypeGiftInput = 1;

class OLGiftInputView:LCCKInputViewPlugin, LCCKInputViewPluginSubclassing,giftSend {
  func sendGift(typeString: String, to: PlayerView?) {
    //    let gift = OLGiftType.changeGiftTypeToOLGiftType(type: type);
    let sendMessage = OLGiftMessage.init(attibutes: [
      "USER_SEX": "\(CurrentUser.shareInstance.sex)",
      "USER_ICON": CurrentUser.shareInstance.avatar,
      "USER_NAME": CurrentUser.shareInstance.name,
      "APP": Config.app,
      "USER_ID": CurrentUser.shareInstance.id,
      GIFT_MSG_KEY_GITF_TYPE:typeString ,
      "CHAT_TIME": Utils.getCurrentTimeStamp()
      ]);
    self.conversationViewController.sendCustomMessage(sendMessage);
  }
  
  func sendGift(type: giftType, to: PlayerView?) {
  
  }
  

  
  // MARK: - LCCKInputViewPluginSubclassing
  public static func classPluginType() -> LCCKInputViewPluginType {
    return LCCKInputViewPluginType(rawValue: UInt(kAVIMMessageMediaTypeGiftInput))!;
  }
  
  // MARK: - LCCKInputViewPluginDelegate
  override var pluginIconImage: UIImage! {
    return UIImage.init(named: "me_btn_gift")!;
  }
  override var pluginTitle: String! {
    return NSLocalizedString("送礼物", comment: "");
  }
  override func pluginDidClicked() {
    var giftViewInChat = giftView.initGiftViewWith(envtype: .CHAT, People: self.conversationViewController.peerName);
    giftViewInChat.envType = .CHAT
    giftViewInChat.toID = self.conversationViewController.peerID
    Utils.getKeyWindow()?.addSubview(giftViewInChat)
    giftViewInChat.animation = "fadeIn"
    giftViewInChat.animate()
    giftViewInChat.chatSendGift = {(type) in
//      if let strong = self {
//        let gift = OLGiftType.changeGiftTypeToOLGiftType(type: type);
        let sendMessage = OLGiftMessage.init(attibutes: [
          "USER_SEX": "\(CurrentUser.shareInstance.sex)",
          "USER_ICON": CurrentUser.shareInstance.avatar,
          "USER_NAME": CurrentUser.shareInstance.name,
          "USER_ID": CurrentUser.shareInstance.id,
          GIFT_MSG_KEY_GITF_TYPE: type,
          "APP": Config.app,
          "CHAT_TIME": Utils.getCurrentTimeStamp()
          ]);
        self.conversationViewController.sendCustomMessage(sendMessage);
//      }
    }
  }
}
