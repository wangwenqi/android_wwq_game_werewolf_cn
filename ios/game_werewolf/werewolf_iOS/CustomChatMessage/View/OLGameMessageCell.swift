//
//  OLGameMessageCell.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/19.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import Foundation
import ChatKit

class OLGameMessageCell:LCCKChatMessageCell, LCCKChatMessageCellSubclassing {
  static func classMediaType() -> AVIMMessageMediaType {
     return AVIMMessageMediaType(rawValue: Int8(kAVIMMessageMediaTypeGameInvite))!;
  }
  
  var gameInviteView:OLGameInvite?;
  
  override func setup() {
    super.setup()
    self.messageContentBackgroundImageView.isUserInteractionEnabled = true
    self.contentView.addSubview(self.messageContentView)
    self.contentView.addSubview(self.messageContentBackgroundImageView)
    self.contentView.addSubview(self.avatarImageView)
    self.contentView.addSubview(self.nickNameLabel)
    self.updateConstraintsIfNeeded()
  }
  
  override func updateConstraints() {
    super.updateConstraints()
    self.messageContentBackgroundImageView.snp.makeConstraints { (make) in
      make.width.equalTo(165)
      make.height.equalTo(200)
    }
  }
  
  override func configureCell(withData message: Any!) {
    super.configureCell(withData: message);
    if let gameInviteMessage = message as? AVIMTypedMessage {
      let view = getGameInviteView();
      if let attributes = gameInviteMessage.attributes as? [String:Any] {
        view.attributes = attributes;
      }
    }
  }
}


extension OLGameMessageCell {
  func getGameInviteView() -> OLGameInvite {
    if (gameInviteView != nil) {
      return gameInviteView!;
    }
    gameInviteView = Bundle.main.loadNibNamed("OLGameInvite", owner: nil, options: nil)?.last as! OLGameInvite;
    self.messageContentBackgroundImageView.addSubview(gameInviteView!);
    gameInviteView?.mas_makeConstraints({ (make:MASConstraintMaker?) in
      make?.height.equalTo()(self.gameInviteView?.superview);
      make?.width.equalTo()(self.gameInviteView?.superview);
      make?.center.equalTo()(self.gameInviteView?.superview);
    });
    return gameInviteView!;
  }
}
