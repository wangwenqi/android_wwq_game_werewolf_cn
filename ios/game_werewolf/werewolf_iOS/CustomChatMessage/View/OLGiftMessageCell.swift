//
//  OLGiftMessageCell.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/7/3.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation
import ChatKit;

class OLGiftMessageCell: LCCKChatMessageCell, LCCKChatMessageCellSubclassing {
  
  var giftView:OLGiftView?;
  
  static func classMediaType() -> AVIMMessageMediaType {
    return AVIMMessageMediaType(rawValue: Int8(kAVIMMessageMediaTypeGift))!;
  }
  
  // MARK: - LCCKChatMessageCell
  override func setup() {
    super.setup();
    self.messageContentBackgroundImageView.isUserInteractionEnabled = true;
    self.contentView.addSubview(self.messageContentView);
    self.contentView.addSubview(self.messageContentBackgroundImageView);
    self.contentView.addSubview(self.avatarImageView);
    self.contentView.addSubview(self.nickNameLabel);
    self.updateConstraintsIfNeeded();
    //点击头像
    let tap = UITapGestureRecognizer.init(target: self, action: #selector(avatarTap(tap:)))
    self.avatarImageView.isUserInteractionEnabled = true
    self.avatarImageView.addGestureRecognizer(tap)
  }
  
  func avatarTap(tap:UITapGestureRecognizer) {
    if (tap.state == .ended) {
      if self.messageOwner != .self {
        self.delegate.messageCellTappedHead(self)
      }
    }
  }
  
  override func updateConstraints() {
    super.updateConstraints();
    _ = self.messageContentBackgroundImageView?.mas_makeConstraints({ (make:MASConstraintMaker?) in
      make?.width.equalTo()(200);
      make?.height.equalTo()(60);
    });
  }
  
  override func configureCell(withData message: Any!) {
    super.configureCell(withData: message);
    if let inviteMessage = message as? AVIMTypedMessage {
      let view = getInviteView();
      if let attributes = inviteMessage.attributes as? [String:Any] {
        view.attributes = attributes;
      }
    }
  }
}

extension OLGiftMessageCell {
  func getInviteView() -> OLGiftView {
    if (giftView != nil) {
      return giftView!;
    }
    giftView = Bundle.main.loadNibNamed("OLGiftView", owner: nil, options: nil)?.last as! OLGiftView;
    
    self.messageContentBackgroundImageView.addSubview(giftView!);
    
    giftView?.mas_makeConstraints({ (make:MASConstraintMaker?) in
      make?.height.equalTo()(self.giftView?.superview);
      make?.width.equalTo()(self.giftView?.superview);
      make?.center.equalTo()(self.giftView?.superview);
    });
    
    return giftView!;
  }
}

