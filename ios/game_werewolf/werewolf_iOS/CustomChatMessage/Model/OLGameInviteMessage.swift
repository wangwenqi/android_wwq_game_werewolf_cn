//
//  OLGameInviteMessage.swift
//  xiaoyu
//
//  Created by Hang on 2018/1/19.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import Foundation
import ChatKit

let kAVIMMessageMediaTypeGameInvite = 103

class OLGameInviteMessage:AVIMTypedMessage, AVIMTypedMessageSubclassing {
  static func classMediaType() -> AVIMMessageMediaType {
     return AVIMMessageMediaType(rawValue: Int8(kAVIMMessageMediaTypeGameInvite))!;
  }
  
  override init() {
    super.init();
  }
  
  init(attibutes:[String : Any]) {
    super.init();
    self.setMessageData(attibutes: attibutes);
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder);
  }
  
  func setMessageData(attibutes:[String : Any]) {
    self.attributes = attibutes;
    self.lcck_setObject(NSLocalizedString("邀请您加入游戏", comment: ""), forKey: LCCKCustomMessageTypeTitleKey);
    self.lcck_setObject(NSLocalizedString("这是一条游戏邀请消息，当前版本过低无法显示，请尝试升级APP查看", comment: ""), forKey: LCCKCustomMessageDegradeKey);
    self.lcck_setObject(NSLocalizedString("有人向您发送了一条游戏邀请消息，请打开APP查看", comment: ""), forKey: LCCKCustomMessageSummaryKey);
    // 只有单聊
    self.lcck_setObject("0", forKey: LCCKCustomMessageConversationTypeKey);
//    var roomId = "";
//    if let id = attibutes["ROOM_ID"] as? String {
//      roomId = id;
//    }
//    let localInvate = NSLocalizedString("邀请您加入游戏房间", comment: "")
//    self.text = "\(localInvate) \(roomId)"
  }
}

