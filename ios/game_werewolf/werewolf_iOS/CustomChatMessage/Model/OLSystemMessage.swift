//
//  OLSystemMessage.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/3.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import ChatKit;


let kAVIMMessageMediaTypeSystem = 102
//
class OLSystemMessage: AVIMTypedMessage,AVIMTypedMessageSubclassing{
//
  override init() {
    super.init();
  }
  
  init(attibutes:[String : Any]) {
    super.init();
    self.setMessageData(attibutes: attibutes);
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder);
  }
  
  // MARK: - AVIMTypedMessageSubclassing
  public static func classMediaType() -> AVIMMessageMediaType {
    return AVIMMessageMediaType(rawValue: Int8(kAVIMMessageMediaTypeSystem))!;
  }
  
}

// 数据相关的处理
extension OLSystemMessage {
  func setMessageData(attibutes:[String : Any]) {
    self.attributes = attibutes;
    self.lcck_setObject(NSLocalizedString("您收到一条系统消息", comment: ""), forKey: LCCKCustomMessageTypeTitleKey);
    self.lcck_setObject(NSLocalizedString("这是一条系统消息，当前版本过低无法显示，请尝试升级APP查看", comment: ""), forKey: LCCKCustomMessageDegradeKey);
    self.lcck_setObject(NSLocalizedString("这是一条系统消息，请打开APP查看", comment: ""), forKey: LCCKCustomMessageSummaryKey);
    // 只有单聊
    self.lcck_setObject("0", forKey: LCCKCustomMessageConversationTypeKey);
    var defaultText = NSLocalizedString("系统消息", comment: "");
//    if let gitString = attibutes[GIFT_MSG_KEY_GITF_TYPE] as? String {
//      let giftType = OLGiftType.getGiftDescription(type: gitString);
//      defaultText = NSLocalizedString("赠送礼物 \(giftType)", comment: "");
//    }
    self.text = defaultText;
  }
}

