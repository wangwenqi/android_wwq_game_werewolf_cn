//
//  OLGiftMessage.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/7/3.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation
import ChatKit;

let kAVIMMessageMediaTypeGift = 101;
let GIFT_MSG_KEY_GITF_TYPE = "GIFT_TYPE";

class OLGiftMessage: AVIMTypedMessage, AVIMTypedMessageSubclassing {
  
  // 从cell初始化会用到
  override init() {
    super.init();
  }
  
  init(attibutes:[String : Any]) {
    super.init();
    self.setMessageData(attibutes: attibutes);
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder);
  }
  
  // MARK: - AVIMTypedMessageSubclassing
  public static func classMediaType() -> AVIMMessageMediaType {
    return AVIMMessageMediaType(rawValue: Int8(kAVIMMessageMediaTypeGift))!;
  }
}

enum OLGiftType:String {
  case flower_1 // 1花
  case flower_3 //3 花
  case flower_99 //99 花
  case cabbage // 烂菜叶
  case egg // 鸡蛋
  case clap // 鼓掌
  case chocolate // 巧克力
  case heart // 心
  case lollipop //棒棒糖
  case kiss // 么么哒
  case dim_ring // 钻戒
  case tidy // 泰迪
  case wedding_dress // 婚纱
  case crown // 皇冠
  case ferrari // 法拉利
  case cake // 蛋糕
  case ship // 船
  case airplane // 飞机
  case rocket // 火箭
  case gold //金币
  case dim //钻石
  
  case seer // 预言家
  case werewolf // 狼人
  case people // 平民
  case witch // 女巫
  case hunter // 猎人
  case cupid // 爱神
  
  case append_time // 延时卡
  case check // 查验卡
  case werewolf_king
  case Iguard = "guard"
  case magician
  case demon
  case unknown
  

  static func getGiftPic(type: String) -> UIImage {
    if let image = UIImage.init(named: "message_gift_\(type)") {
      return image;
    }
    return UIImage.init(named: "message_gift_default")!;
  }
  
  static func getCardPic(type:String) -> UIImage? {
    if let image = UIImage.init(named: "message_gift_\(type)") {
    return image;
    }
    return nil
  }
  
  static func changeGiftTypeToOLGiftType(type: giftType) -> OLGiftType {
    switch type {
    case .Airplane:
      return OLGiftType.airplane;
    case .Boat:
      return OLGiftType.ship;
    case .Cake:
      return OLGiftType.cake;
    case .Car:
      return OLGiftType.ferrari;
    case .Chocolate:
      return OLGiftType.chocolate;
    case .Clap:
      return OLGiftType.clap;
    case .Crown:
      return OLGiftType.crown;
    case .Egg:
      return OLGiftType.egg;
    case .Heart:
      return OLGiftType.heart;
    case .Kiss:
      return OLGiftType.kiss;
    case .Lollipop:
      return OLGiftType.lollipop;
    case .Ring:
      return OLGiftType.dim_ring;
    case .Rocket:
      return OLGiftType.rocket;
    case .Rose:
      return OLGiftType.flower_1;
    case .Rose3:
      return OLGiftType.flower_3;
    case .Rose99:
      return OLGiftType.flower_99;
    case .Teddy:
      return OLGiftType.tidy;
    case .Vegetable:
      return OLGiftType.cabbage;
    case .Weddingdress:
      return OLGiftType.wedding_dress;

    case .Prophet:
      return OLGiftType.seer;
    case .Wolf:
      return OLGiftType.werewolf;
    case .Civilian:
      return OLGiftType.people;
    case .Witch:
      return OLGiftType.witch;
    case .Hunter:
      return OLGiftType.hunter;
    case .Lover:
      return OLGiftType.cupid;
      
    case .delayTime:
      return OLGiftType.append_time;
    case .checkID:
      return OLGiftType.check;
    case .Iguard:
      return OLGiftType.Iguard;
    case .Werewolf_king:
      return OLGiftType.werewolf_king
    case .Magican:
      return OLGiftType.magician
    case .Demon:
      return OLGiftType.demon
      
    default:
      return .unknown;
    }
  }
  
  static func changeOLGiftTypeToGiftType(type: String) -> giftType {
    switch type {
    case OLGiftType.airplane.rawValue:
      return .Airplane;
    case OLGiftType.ship.rawValue:
      return .Boat;
    case OLGiftType.cake.rawValue:
      return .Cake;
    case OLGiftType.ferrari.rawValue:
      return .Car;
    case OLGiftType.chocolate.rawValue:
      return .Chocolate;
    case OLGiftType.clap.rawValue:
      return .Clap;
    case OLGiftType.crown.rawValue:
      return .Crown;
    case OLGiftType.egg.rawValue:
      return .Egg;
    case OLGiftType.heart.rawValue:
      return .Heart;
    case OLGiftType.kiss.rawValue:
      return .Kiss;
    case OLGiftType.lollipop.rawValue:
      return .Lollipop;
    case OLGiftType.dim_ring.rawValue:
      return .Ring;
    case OLGiftType.rocket.rawValue:
      return .Rocket;
    case OLGiftType.flower_1.rawValue:
      return .Rose;
    case OLGiftType.flower_3.rawValue:
      return .Rose3;
    case OLGiftType.flower_99.rawValue:
      return .Rose99;
    case OLGiftType.tidy.rawValue:
      return .Teddy;
    case OLGiftType.cabbage.rawValue:
      return .Vegetable;
    case OLGiftType.wedding_dress.rawValue:
      return .Weddingdress;
    case OLGiftType.seer.rawValue:
      return .Prophet;
    case OLGiftType.werewolf.rawValue:
      return .Wolf;
    case OLGiftType.people.rawValue:
      return .Civilian;
    case OLGiftType.witch.rawValue:
      return .Witch;
    case OLGiftType.hunter.rawValue:
      return .Hunter;
    case OLGiftType.cupid.rawValue:
      return .Lover;
      
    case OLGiftType.append_time.rawValue:
      return .delayTime;
    case OLGiftType.check.rawValue:
      return .checkID;
    case OLGiftType.werewolf_king.rawValue:
      return .Werewolf_king
    case OLGiftType.Iguard.rawValue:
      return .Iguard
    case OLGiftType.magician.rawValue:
      return .Magican
    case OLGiftType.demon.rawValue:
      return .Demon
    default:
      return .Rose;
    }
  }
  

  static func getGiftDescription(type: String) -> String {
    switch type {
    case "flower_1":
      return NSLocalizedString("一朵玫瑰花", comment: "");
    case "flower_3":
      return NSLocalizedString("三朵玫瑰花", comment: "");
    case "flower_99":
      return NSLocalizedString("九十九朵玫瑰花", comment: "");
    case "cabbage":
      return NSLocalizedString("烂菜叶", comment: "");
    case "egg":
      return NSLocalizedString("臭鸡蛋", comment: "");
    case "clap":
      return NSLocalizedString("啪啪啪", comment: "");
    case "chocolate":
      return NSLocalizedString("巧克力", comment: "");
    case "heart":
      return "520";
    case "lollipop":
      return NSLocalizedString("棒棒糖", comment: "");
    case "kiss":
      return NSLocalizedString("么么哒", comment: "");
    case "dim_ring":
      return NSLocalizedString("钻戒", comment: "");
    case "tidy":
      return NSLocalizedString("小熊", comment: "");
    case "wedding_dress":
      return NSLocalizedString("婚纱", comment: "");
    case "crown":
      return NSLocalizedString("皇冠", comment: "");
    case "ferrari":
      return NSLocalizedString("法拉利", comment: "");
    case "cake":
      return NSLocalizedString("蛋糕", comment: "");
    case "ship":
      return NSLocalizedString("爱情的轮船", comment: "");
    case "airplane":
      return NSLocalizedString("私人飞机", comment: "");
    case "rocket":
      return NSLocalizedString("幸福火箭", comment: "");
    case "gold":
      return NSLocalizedString("金币", comment: "");
    case "dim":
      return NSLocalizedString("钻石", comment: "");
      
    case "seer":
      return NSLocalizedString("预言家卡", comment: "");
    case "werewolf": 
      return NSLocalizedString("狼人卡", comment: "");
    case "people":
      return NSLocalizedString("平民卡", comment: "");
    case "witch":
      return NSLocalizedString("女巫卡", comment: "");
    case "hunter":
      return NSLocalizedString("猎人卡", comment: "");
    case "cupid":
      return NSLocalizedString("爱神卡", comment: "");
      
    case "append_time":
      return NSLocalizedString("延时卡", comment: "");
    case "check":
      return NSLocalizedString("查验卡", comment: "");
    case "werewolf_king":
      return NSLocalizedString("白狼王卡", comment: "");
    case "Iguard":
      return NSLocalizedString("守卫卡", comment: "");
    case "guard":
      return NSLocalizedString("守卫卡", comment: "");
    case "magician":
      return NSLocalizedString("魔术师卡", comment: "");
    case "demon":
      return NSLocalizedString("恶魔卡", comment: "");
    default:
      return NSLocalizedString("请更新版本查看", comment: "");
    }
  }
}

// 数据相关的处理
extension OLGiftMessage {
  func setMessageData(attibutes:[String : Any]) {
    self.attributes = attibutes;
    self.lcck_setObject(NSLocalizedString("有人赠送您礼物", comment: ""), forKey: LCCKCustomMessageTypeTitleKey);
    self.lcck_setObject(NSLocalizedString("这是一条赠送礼物消息，当前版本过低无法显示，请尝试升级APP查看", comment: ""), forKey: LCCKCustomMessageDegradeKey);
    self.lcck_setObject(NSLocalizedString("这是一条赠送礼物消息，请打开APP查看", comment: ""), forKey: LCCKCustomMessageSummaryKey);
    // 只有单聊
    self.lcck_setObject("0", forKey: LCCKCustomMessageConversationTypeKey);
    var defaultText = NSLocalizedString("赠送礼物", comment: "");
    if let gitString = attibutes[GIFT_MSG_KEY_GITF_TYPE] as? String {
      let giftType = OLGiftType.getGiftDescription(type: gitString);
      let localsend = NSLocalizedString("赠送礼物", comment: "")
      defaultText = "\(localsend) \(giftType)"
    }
    self.text = defaultText;
  }
}
