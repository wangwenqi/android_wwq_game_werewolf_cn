//
//  ShowRoleView.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/23.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import WebKit

class ShowRoleView: BaseNoticeView {
    
    var web:WKWebView?

    @IBOutlet weak var content: UIView!

    @IBAction func goBack(_ sender: Any) {
        Utils.hideNoticeView()
    }
    
    override func awakeFromNib() {

        self.backgroundColor = UIColor.clear
        self.web = WKWebView()
        self.web?.backgroundColor = UIColor.clear
        self.web?.isOpaque = false
        self.web?.sizeToFit()
        self.content.addSubview(web!)
        self.web?.snp.makeConstraints({ (make) in
          make.edges.equalToSuperview()
        })
        self.height = 500
        showRole()
    }
}

extension ShowRoleView {
    
    //wkwebview ios8 不能加载本地file
    func fileURLForBuggyWKWebView8(fileURL: URL) throws -> URL {
        // Some safety checks
        if !fileURL.isFileURL {
            throw NSError(
                domain: "BuggyWKWebViewDomain",
                code: 1001,
                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("URL must be a file URL.", comment:"")])
        }
        try! fileURL.checkResourceIsReachable()
        
        // Create "/temp/www" directory
        let fm = FileManager.default
        let tmpDirURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("www")
        try! fm.createDirectory(at: tmpDirURL, withIntermediateDirectories: true, attributes: nil)
        
        // Now copy given file to the temp directory
        let dstURL = tmpDirURL.appendingPathComponent(fileURL.lastPathComponent)
        let _ = try? fm.removeItem(at: dstURL)
        try! fm.copyItem(at: fileURL, to: dstURL)
        
        // Files in "/temp/www" load flawlesly :)
        return dstURL
    }
    
    func showRole() {
  
        var fileURL = URL(fileURLWithPath: Bundle.main.path(forResource:"sm", ofType: "html")!,isDirectory:false)
        let urlWithParams = fileURL.absoluteString + "?lg=\(CurrentUser.shareInstance.currentLang)"
        let finalUrl = URL.init(string: urlWithParams)
        let request = NSURLRequest.init(url: finalUrl!)
        if #available(iOS 9.0, *) {
          self.web?.load(request as URLRequest)
        }else{
            do {
                fileURL = try fileURLForBuggyWKWebView8(fileURL: fileURL)
                self.web?.load(URLRequest(url: fileURL))
            } catch let error as NSError {
                print("Error: " + error.debugDescription)
            }
        }
    }
}

