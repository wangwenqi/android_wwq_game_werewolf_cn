//
//  FamilyRequestTableViewCell.swift
//  game_werewolf
//
//  Created by Hang on 2017/10/23.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class FamilyRequestTableViewCell: UITableViewCell {
  
    var user_id:String?
    //MARK: outltes
    @IBOutlet weak var requestInvite: XBHButton!
    @IBOutlet weak var level: levelLable!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var ava: UIImageView!
    @IBOutlet weak var sex: UIImageView!
    //MARK: actions
    @IBAction func requestInviteClicked(_ sender: Any) {
       if let button = sender as? XBHButton {
        if button.isSelected {
          return
        }
        XBHHUD.showLoading()
        RequestManager().get(url:RequestUrls.addToGroup + self.user_id!, success: { (json) in
             XBHHUD.hide()
              button.isSelected = true
              button.layer.cornerRadius = 8
              button.layer.masksToBounds = true
              button.layer.borderWidth = 1
              button.layer.borderColor =  UIColor.hexColor("f47103").cgColor
              button.backgroundColor = UIColor.white
              button.setTitle(NSLocalizedString("已通过", comment:""), for: .selected)
              button.startColor = UIColor.white
              button.endColor = UIColor.white
              button.setTitleColor(UIColor.hexColor("7A4D0E"), for: .normal)
              button.setTitleColor(UIColor.hexColor("7A4D0E"), for: .selected)
              button.setButtonSoftShadowColor(color:  UIColor.hexColor("ffaa00"))
              button.refrshBackgroundColor()
          }) { (code, message) in
            XBHHUD.showError(message)
          }
      }
    }
    //MARK: liftCycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      self.level.layer.borderColor = UIColor.black.cgColor
      self.level.layer.borderWidth = 1
      self.level.layer.cornerRadius = 3
      self.level.layer.masksToBounds = true
      
      self.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0) //or UIEdgeInsetsMake(top, left, bottom, right)
      self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func config(json:JSON) {
      if let name = json["name"].string {
          self.name.text = name
      }
      if let image = json["image"].string {
        let url = URL.init(string: image)
        self.ava.sd_setImage(with: url, placeholderImage: UIImage.init(named:"room_head_default"))
      }
      //性别
      let sex = json["sex"].stringValue
      if Int(sex) == 1 {
        self.sex.image = UIImage.init(named: "ic_male")
      } else {
        self.sex.image = UIImage.init(named: "ic_female")
      }
      //等级
      if let level = json["game"]["level"].int {
        self.level.text = "LV \(level)"
      }
      
      if let userid = json["id"].string {
        self.user_id = userid
      }
    }
  
    func setAcceptStatus() {
      requestInvite.isSelected = true
      requestInvite.layer.cornerRadius = 8
      requestInvite.layer.masksToBounds = true
      requestInvite.layer.borderWidth = 1
      requestInvite.layer.borderColor =  UIColor.hexColor("f47103").cgColor
      requestInvite.backgroundColor = UIColor.white
      requestInvite.setTitle(NSLocalizedString("已通过", comment:""), for: .selected)
      requestInvite.startColor = UIColor.white
      requestInvite.endColor = UIColor.white
      requestInvite.setTitleColor(UIColor.hexColor("7A4D0E"), for: .normal)
      requestInvite.setTitleColor(UIColor.hexColor("7A4D0E"), for: .selected)
      requestInvite.setButtonSoftShadowColor(color:  UIColor.hexColor("ffaa00"))
      requestInvite.refrshBackgroundColor()
    }
}
