//
//  FamilyMemberListViewController.swift
//  game_werewolf
//
//  Created by Hang on 2017/10/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import MJRefresh

class FamilyMemberListViewController: RotationViewController {
    //MARK: Variable
    var data:JSON? {
      didSet{
        masterCheck()
      }
    }
    var isMaster = false
    var hasNew = false
    var changeMasterModel = false
    var currentPage = 0
    var pageLimit = 20
    //MARK: outlets
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
      super.viewDidLoad()
      self.navigationItem.title = NSLocalizedString("家族成员", comment:"")
      let register = UINib(nibName: "FamilyMemberListCell", bundle: nil)
      tableview.register(register, forCellReuseIdentifier: "MEMBERLIST")
      tableview.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
      tableview.delegate = self
      tableview.backgroundColor = UIColor.white
      tableview.dataSource = self
      setUpUI()
      self.tableview.mj_header =  MJRefreshNormalHeader(refreshingBlock: { [weak self] in
        self?.pullRefresh()
      })
    }
  
  override func viewWillAppear(_ animated: Bool) {
     currentPage = 0
     getData()
  }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
  
  //MARK: UI
  
    func setUpUI() {
      navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "room_bg_navbar"), for: .default)
      navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "room_icon_leftarrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(FamilyMemberListViewController.leftClicked))
      if changeMasterModel {
        return
      }
      if let require = data?["require_count"].int {
        let moreButton = UIButton()
        if require > 0 {
          moreButton.setImage(UIImage(named:"小红点-iOS"), for: .normal)
        }else{
          moreButton.setImage(UIImage(named:"成员按钮-iOS"), for: .normal)
        }
        if #available(iOS 11, *) {
          moreButton.snp.makeConstraints { (make) in
            make.width.equalTo(27)
            make.height.equalTo(19)
          }
        } else {
          moreButton.frame = CGRect(x: 0, y: 0, width: 27, height: 19)
        }
        moreButton.addTarget(self, action: #selector(FamilyMemberListViewController.rightClicked), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: moreButton)
      }
    }
  
  //MARK: action
  
  func rightClicked() {
    self.performSegue(withIdentifier: "ShowRequest", sender: nil)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ShowRequest" {
      if let list = segue.destination as? FamilyRequestListViewController {
        if let json = sender as? JSON {
          list.data = json["datas"].array
        }
      }
    }
    
    if segue.identifier == "SHOWEDIT" {
      if let list = segue.destination as? FamilyEditViewController {
        if let json = sender as? JSON {
          list.data = json
        }
      }
    }
  }
  
  func leftClicked() {
    self.performSegue(withIdentifier: "listToDetail", sender: self)
//    navigationController?.popViewController(animated: true)
  }
  
  func masterCheck() {
    //是不是族长
    if let lists = data?["datas"].array {
      let own = lists.first(where: { (list) -> Bool in
        return list["title"].stringValue == "owner"
      })
      if let own_id = own?["id"].string {
        if own_id == CurrentUser.shareInstance.id {
          //自己是族长
          self.isMaster = true
        }
      }
    }
  }
  
  func removeBy(id:String) {
    XBHHUD.showLoading()
    RequestManager().get(url: RequestUrls.groupRemove + id, success: { (json) in
      XBHHUD.showSuccess(NSLocalizedString("移除成功", comment: ""))
      XBHHUD.hide()
      self.currentPage = 0
      self.getData()
    }) { (code, message) in
      XBHHUD.showError(message)
    }
  }
  
  func changeMaterTo(id:String) {
    XBHHUD.showLoading()
    RequestManager().get(url: RequestUrls.changeMater + id, success: {[weak self] (json) in
      XBHHUD.hide()
      XBHHUD.showSuccess(NSLocalizedString("转让族长成功", comment: ""))
      self?.leaveCurrentView()
    }) { (code, message) in
      XBHHUD.showError(message)
    }
  }
  
  func leaveCurrentView() {
    let currentvc = Utils.getCurrentVC(vc:(Utils.getKeyWindow()?.rootViewController)!)
    if (currentvc?.isKind(of: FamilyMemberListViewController.self))! {
      currentvc?.dismiss(animated: true, completion: {
        let lastVC = Utils.getCurrentVC(vc:(Utils.getKeyWindow()?.rootViewController)!)
        if (lastVC?.isKind(of: UserInfoViewController.self))! {
          lastVC?.dismiss(animated: true, completion:{
          })
        }
        if (lastVC?.isKind(of:FamilyChatViewController.self))! {
          lastVC?.dismiss(animated: true, completion:{
          })
        }
      })
    }else{
      self.dismiss(animated: true, completion: {
        self.leaveCurrentView()
      })
    }
  }
  
  func pullRefresh() {
    currentPage = 0
    XBHHUD.showLoading()
    let params = ["limit":"\(pageLimit)","skip":String(currentPage * pageLimit)] as [String : Any]
    RequestManager().get(url: RequestUrls.groupMembers,parameters: params , success: {[weak self] (response) in
      //隐藏遮挡
      XBHHUD.hide()
      if self?.currentPage == 0 {
        self?.data = response
        if self?.data != nil {
          if  (self?.data?["datas"].count)! == self?.pageLimit {
            self?.tableview.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: {
              self?.currentPage = (self?.currentPage)! + 1
              self?.getData()
            })
          }
        }
        self?.setUpUI()
        self?.tableview.mj_header.endRefreshing()
        self?.tableview.reloadData()
      }
    }) { (code, message) in
      self.tableview.mj_header.endRefreshing()
      XBHHUD.showError(message)
    }
  }
  
  func setPositionRefresh() {
    currentPage = 0
    XBHHUD.showLoading()
    let params = ["limit":"\(pageLimit)","skip":String(currentPage * pageLimit)] as [String : Any]
    RequestManager().get(url: RequestUrls.groupMembers,parameters: params , success: {[weak self] (response) in
      //隐藏遮挡
      XBHHUD.hide()
      XBHHUD.showSuccess(NSLocalizedString("设置职位成功", comment: ""))
      if self?.currentPage == 0 {
        self?.data = response
        if self?.data != nil {
          if  (self?.data?["datas"].count)! == self?.pageLimit {
            self?.tableview.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: {
              self?.currentPage = (self?.currentPage)! + 1
              self?.getData()
            })
          }
        }
        self?.setUpUI()
        self?.tableview.mj_header.endRefreshing()
        self?.tableview.reloadData()
      }
    }) { (code, message) in
      self.tableview.mj_header.endRefreshing()
      XBHHUD.showError(message)
    }
  }
  
  func getData() {
    XBHHUD.showLoading()
    let params = ["limit":"\(pageLimit)","skip":String(currentPage * pageLimit)] as [String : Any]
    RequestManager().get(url: RequestUrls.groupMembers,parameters: params , success: {[weak self] (response) in
      //隐藏遮挡
      XBHHUD.hide()
      if self?.currentPage == 0 {
        self?.data = response
        if self?.data != nil {
          if  (self?.data?["datas"].count)! == self?.pageLimit {
            self?.tableview.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: {
              self?.currentPage = (self?.currentPage)! + 1
              self?.getData()
            })
          }
        }
        self?.setUpUI()
        self?.tableview.reloadData()
      }else{
        if self?.data != nil {
          if let _ = response["datas"].array {
            if response["datas"].arrayValue.count == 0 {
              self?.tableview.mj_footer.endRefreshingWithNoMoreData()
            }else{
              if let users = response["datas"].array {
                if  users.count != 0 {
                  if var arr = self?.data?["datas"].array {
                    arr.append(contentsOf: users)
                    self?.data?["datas"].arrayObject = arr
                  }
                }
                self?.tableview.mj_footer.endRefreshing()
              }
            }
            self?.setUpUI()
            self?.tableview.reloadData()
          }
        }
      }
    }) { (code, message) in
      self.tableview.mj_footer.endRefreshing()
      XBHHUD.showError(message)
    }
  }
  
  //MARK: delegate
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //移除旧的
    for view in self.tableview.subviews {
      //移除旧的
      if view.isKind(of: SelectActionView.self) {
        //移除旧的
        if let selectAction = view as? SelectActionView {
          selectAction.diss()
        }
      }
    }
  }
}

extension FamilyMemberListViewController : UITableViewDelegate,UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return data?["datas"].array == nil ? 0 : (data?["datas"].array!.count)!
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "MEMBERLIST", for: indexPath) as! FamilyMemberListTableViewCell
    if let json = data?["datas"].array?[indexPath.row] {
      cell.config(json: json)
    }
    cell.selectionStyle = .none
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 90
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
  
  }
  
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    if changeMasterModel {
      return false
    }else{
      if let json = self.data?["datas"].array?[indexPath.row] {
        if let user_id = json["id"].string{
          if user_id == CurrentUser.shareInstance.id {
            return true
          }else{
            return true
          }
        }else{
          return true
        }
      }else{
        return true
      }
    }
  }
  
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    let remove = UITableViewRowAction(style: .normal, title: NSLocalizedString("移出家族", comment:"")) { action, index in
        tableView.setEditing(false, animated: true)
        if let json = self.data?["datas"].array?[indexPath.row] {
          if let user_id = json["id"].string{
            self.removeBy(id: user_id)
          }
        }
      }
    remove.backgroundColor = UIColor.red
    let info = UITableViewRowAction(style: .normal, title: NSLocalizedString("查看信息", comment: "")) { action, index in
      tableView.setEditing(false, animated: true)
      if let json = self.data?["datas"].array?[indexPath.row] {
        if let user_id = json["id"].string,
           let name = json["name"].string,
           let sex = json["sex"].int,
           let image = json["image"].string
        {
          let controller = UserInfoViewController()
          controller.peerID = user_id
          controller.peerSex = String(sex)
          controller.peerName = name
          controller.peerIcon = image
          controller.comeFromFamily = true
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
              let nav = RotationNavigationViewController.init(rootViewController: controller)
              self.present(nav, animated: true, completion: nil)
          })
        }
      }
    }
   info.backgroundColor = UIColor.hexColor("FDAC2B")
    let change =  UITableViewRowAction(style: .normal, title:NSLocalizedString("转让族长", comment: "")) { action, index in
        if let json = self.data?["datas"].array?[indexPath.row] {
          if let user_id = json["id"].string{
            self.changeMaterTo(id: user_id)
          }
        }
    }
    change.backgroundColor = UIColor.hexColor("FDAC2B")
    let givePosition =  UITableViewRowAction(style: .normal, title:NSLocalizedString("设置职位", comment: "")) { action, index in
      tableView.setEditing(false, animated: true)
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.75, execute: { [weak self] in
        let cell = tableView.cellForRow(at: indexPath) as! FamilyMemberListTableViewCell
        var rect = CGRect()
        rect.origin = cell.nickName.frame.origin
        rect.origin.y -= 5
        rect.size.width = 150
        rect.size.height = 180
        let cellRect = cell.section2.convert(rect, to: cell)
        var viewcell = cell.convert(cellRect, to: tableView)
        var tooHigh = false
        //这里判断高度是不是超过了最大高度
        let totalHeight = viewcell.origin.y + viewcell.height + 64
        if totalHeight > Screen.height + tableView.contentOffset.y {
          viewcell.origin.y = viewcell.origin.y - viewcell.height + 20
          tooHigh = true
        }
        for view in (self?.tableview.subviews)! {
          //移除旧的
          if view.isKind(of: SelectActionView.self) {
            //移除旧的
            if let selectAction = view as? SelectActionView {
              selectAction.diss()
            }
          }
        }
        let action = SelectActionView.generaterViewBy(type:.POSITION,frame:viewcell)
        action?.userInfo = cell.data
        action?.frame = viewcell
        action?.changeTrangleIf(tooHeight: tooHigh)
        action?.setPostionSucess = { [weak self] in
          self?.currentPage = 0
          self?.setPositionRefresh()
        }
        tableView.addSubview(action!)
        tableView.bringSubview(toFront: action!)
      })
    }
    givePosition.backgroundColor = UIColor.hexColor("808080")
    if changeMasterModel {
      return [change]
    }else{
      if isMaster {
        if let json = self.data?["datas"].array?[indexPath.row] {
          if let user_id = json["id"].string{
            if user_id == CurrentUser.shareInstance.id {
              return [info]
            }else{
              return  [info,givePosition,remove]
            }
          }
        }
        return [info,remove,givePosition]
      }else{
        return [info]
      }
    }
    
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if changeMasterModel {
      if let json = self.data?["datas"].array?[indexPath.row] {
        if let user_id = json["id"].string,
          let name = json["name"].string{
          if user_id != CurrentUser.shareInstance.id {
            let alter =  UIAlertController(title:"提示", message:NSLocalizedString("确定要把族长转让给", comment: "") + " \(name)吗", preferredStyle: .alert)
            let action = UIAlertAction(title:NSLocalizedString("确定", comment: ""), style: .default) { [weak self] (action) in
              self?.changeMaterTo(id: user_id)
            }
            let cancleAction = UIAlertAction(title:NSLocalizedString("取消", comment: ""), style:.cancel) { (action) in
            }
            alter.addAction(action)
            alter.addAction(cancleAction)
            Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)?.present(alter, animated: true, completion: nil)
          }
        }
      }
    }else{
      
      for view in self.tableview.subviews {
        //移除旧的
        if view.isKind(of: SelectActionView.self) {
          //移除旧的
          if let selectAction = view as? SelectActionView {
            selectAction.diss()
          }
        }
      }
    }
  }
}
