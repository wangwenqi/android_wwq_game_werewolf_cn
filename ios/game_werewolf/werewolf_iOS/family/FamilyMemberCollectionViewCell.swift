//
//  FamilyMemberCollectionViewCell.swift
//  game_werewolf
//
//  Created by Hang on 2017/10/17.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class FamilyMemberCollectionViewCell: UICollectionViewCell {
    
  @IBOutlet weak var memberAva: UIImageView!
  @IBOutlet weak var memberLable: UILabel!
  @IBOutlet weak var memberBg: UIImageView!
  @IBOutlet weak var name: UILabel!
    
  override func awakeFromNib() {
    
  }
  
  func config(json:JSON) {
    if let image = json["image"].string {
      let url = URL.init(string: image)
      self.memberAva.sd_setImage(with: url, placeholderImage: UIImage.init(named:"room_head_default"))
      self.memberAva.layer.masksToBounds = true
      self.memberAva.layer.cornerRadius = ((Screen.width-86)/4 - 20)/2
    }
    print(json.dictionaryObject)
    if let name = json["name"].string {
      self.name.text = name
    }
    isMember()
    if let title = json["title"].string {
      if title == "owner" {
        isMaster()
      }
        if title != "memeber" {
            setTitle(title)
        }
    }
  }
  
  func setTitle(_ title:String) {
    memberBg.isHidden = false
    memberLable.isHidden = false
    if title == "deputy" {
       memberLable.text = NSLocalizedString("副族长", comment: "")
    }else if title == "elder" {
        memberLable.text = NSLocalizedString("长老", comment: "")
    }else if title == "elite" {
      memberLable.text = NSLocalizedString("精英", comment: "")
    }
  }
    
  func isMaster() {
    memberBg.isHidden = false
    memberLable.isHidden = false
    memberLable.text = NSLocalizedString("族长", comment: "")
  }
  
  func isMember() {
    memberBg.isHidden = true
    memberLable.isHidden = true
  }
}
