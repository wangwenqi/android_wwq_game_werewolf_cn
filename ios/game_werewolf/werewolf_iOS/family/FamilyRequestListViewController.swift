//
//  FamilyRequestListViewController.swift
//  game_werewolf
//
//  Created by Hang on 2017/10/23.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import MJRefresh

class FamilyRequestListViewController: RotationViewController {
    var data:[JSON]? {
      didSet{
        if data != nil {
          if data?.count == 0 {
            self.bottom.isHidden = true
          }
        }
      }
    }
    var currentPage = 0
    var pageLimit = 20
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var bottom: UIView!
    override func viewDidLoad() {
      super.viewDidLoad()
      getData()
      self.navigationItem.title = NSLocalizedString("申请列表", comment:"")
      let register = UINib(nibName: "FamilyRequest", bundle: nil)
      tableview.register(register, forCellReuseIdentifier: "FamilyRequest")
      tableview.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
      tableview.delegate = self
      tableview.backgroundColor = UIColor.white
      tableview.dataSource = self
      setUpUI()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  //MARk: UI
  func setUpUI() {
    navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "room_bg_navbar"), for: .default)
    navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "room_icon_leftarrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(FamilyRequestListViewController.leftClicked))
    //隐藏下边
    if data != nil {
      if data?.count == 0 {
        self.bottom.isHidden = true
      }
    }
  }
  //MARK:action
  func leftClicked() {
    navigationController?.popViewController(animated: true)
  }
  
  @IBOutlet weak var clearAll: XBHButton!
  @IBAction func clearAll(_ sender: Any) {
    XBHHUD.showLoading()
    RequestManager().get(url:RequestUrls.reject_all, success: {[weak self] (json) in
        XBHHUD.hide()
        self?.currentPage = 0
        self?.getData()
    }) { (code, message) in
        XBHHUD.showError(message)
    }
  }
  
  @IBAction func acceptAll(_ sender: Any) {
    XBHHUD.showLoading()
    RequestManager().get(url:RequestUrls.accept_all, success: {[weak self] (json) in
      XBHHUD.hide()
      DispatchQueue.main.async {
        if let users = json["userIds"].array {
          self?.setAllAccept(json: users)
        }
      }
    }) { (code, message) in
      XBHHUD.showError(message)
    }
  }
  
  func setAllAccept(json:[JSON]) {
    for datas in json {
      if let user_id = datas.string {
        let acceptJson = self.data?.first(where: { (user) -> Bool in
          return user["id"].stringValue == user_id
        })
        if acceptJson != nil {
          let index = self.data?.index(of: acceptJson!)
          if index != nil {
            let indexPaths = NSIndexPath(row:index!, section: 0)
            let cell = tableview.cellForRow(at: indexPaths as IndexPath) as? FamilyRequestTableViewCell
            cell?.setAcceptStatus()
          }
        }
      }
    }
  }
  
  func getData() {
    XBHHUD.showLoading()
    let params = ["limit":"\(pageLimit)","skip":String(currentPage * pageLimit)] as [String : Any]
    RequestManager().get(url:RequestUrls.groupRequset,parameters: params , success: {[weak self] (response) in
      //隐藏遮挡
      XBHHUD.hide()
      if self?.currentPage == 0 {
        self?.data = response["datas"].arrayValue
        if self?.data != nil {
          if  (self?.data?.count)! == self?.pageLimit {
            self?.tableview.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: {
              self?.currentPage = (self?.currentPage)! + 1
              self?.getData()
            })
          }
        }
        self?.tableview.reloadData()
      }else{
        if self?.data != nil {
          if let _ = response["datas"].array {
            if response["datas"].arrayValue.count == 0 {
              self?.tableview.mj_footer.endRefreshingWithNoMoreData()
            }else{
              if let users = response["datas"].array {
                if  users.count != 0 {
                  if var arr = self?.data {
                    arr.append(contentsOf: users)
                    self?.data = arr
                  }
                }
                self?.tableview.mj_footer.endRefreshing()
              }
            }
            self?.tableview.reloadData()
          }
        }
      }
    }) { (code, message) in
      XBHHUD.showError(message)
    }
  }
}

extension FamilyRequestListViewController : UITableViewDelegate,UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return data == nil ? 0 : data!.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyRequest", for: indexPath) as! FamilyRequestTableViewCell
    if let json = data?[indexPath.row] {
      cell.config(json: json)
    }
    cell.selectionStyle = .none
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 90
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //TODO:点击查看详情
    if let json = data?[indexPath.row] {
      let controller = UserInfoViewController()
      controller.peerID = json["id"].stringValue
      controller.peerSex = json["sex"].stringValue
      controller.peerName = json["name"].stringValue
      controller.peerIcon = json["image"].stringValue
      if let isFriend = json["is_friend"].string {
        controller.isFriend = isFriend == "false" ? false : true
      } else if let isFriend = json["is_friend"].bool {
        controller.isFriend = isFriend;
      }
      controller.isTourist = false
      controller.isself = json["id"].stringValue == CurrentUser.shareInstance.id ? true : false
      controller.comeFromFamily = true
      let rootVC = RotationNavigationViewController.init(rootViewController: controller)
      self.present(rootVC, animated: true, completion: nil)
    }
  }
}

