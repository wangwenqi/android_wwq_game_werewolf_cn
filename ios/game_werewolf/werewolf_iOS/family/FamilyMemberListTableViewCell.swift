//
//  FamilyMemberListTableViewCell.swift
//  game_werewolf
//
//  Created by Hang on 2017/10/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class FamilyMemberListTableViewCell: UITableViewCell {

    @IBOutlet weak var ava: UIImageView!
    @IBOutlet weak var section2: UIView!
    @IBOutlet weak var nickName: UILabel!
    @IBOutlet weak var level: levelLable!
    @IBOutlet weak var identify: UILabel!
    @IBOutlet weak var active_point: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var avaWidth: NSLayoutConstraint!
    @IBOutlet weak var sex: UIImageView!
  
    var data:JSON?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.level.layer.cornerRadius = 3
        self.level.layer.borderColor = UIColor.black.cgColor
        self.level.layer.borderWidth = 1
        self.level.layer.masksToBounds = true
        if Utils.getCurrentDeviceType() == .iPhone4 || Utils.getCurrentDeviceType() == .iPhone5 {
          self.avaWidth.constant = 50
          self.ava.layer.cornerRadius = 25
        }else{
          self.avaWidth.constant = 60
          self.ava.layer.cornerRadius = 30
        }
        self.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0) //or UIEdgeInsetsMake(top, left, bottom, right)
        self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
    }
  
    func config(json:JSON) {
      //名字
      self.data = json
      if let name = json["name"].string {
        self.nickName.text = name
      }
      if let image = json["image"].string {
        let url = URL.init(string: image)
        self.ava.sd_setImage(with: url, placeholderImage: UIImage.init(named:"room_head_default"))
      }
      //性别
      let sex = json["sex"].stringValue
      if Int(sex) == 1 {
        self.sex.image = UIImage.init(named: "ic_male")
      } else {
        self.sex.image = UIImage.init(named: "ic_female")
      }
      //等级
      if let level = json["game"]["level"].int {
         self.level.text = "LV \(level)"
      }
      //身份
      if let identify = json["title"].string {
        if identify == "owner" {
          self.identify.text = NSLocalizedString("族长", comment:"")
          self.identify.textColor = UIColor.hexColor("F15C00")
        }else if identify == "deputy"{
          self.identify.text = NSLocalizedString("副族长", comment:"")
          self.identify.textColor = UIColor.hexColor("838383")
        }else if identify == "elder" {
          self.identify.text = NSLocalizedString("长老", comment:"")
          self.identify.textColor = UIColor.hexColor("838383")
        }else if identify == "elite" {
          self.identify.text = NSLocalizedString("精英", comment:"")
          self.identify.textColor = UIColor.hexColor("838383")
        }else if identify == "memeber" {
           self.identify.text = NSLocalizedString("成员", comment:"")
        }
        
       }
      //活跃度
      if let active_point = json["active_point"].int {
        self.active_point.text = "\(active_point)"
      }
      //状态
      if let status = json["status"].string {
        if let oneLineStatus =  Int((status.split(":").first) ?? "0") {
          if oneLineStatus >= 1 {
            self.status.text = NSLocalizedString("在线中", comment:"")
            self.status.textColor = UIColor.hexColor("3ca490")
          }else{
            if status.split(":").count >= 2 {
              if let times = status.split(":")[1] as? String {
                  if let timeInterval = Int(times) {
                    let timeWithSeconds = lround(Double(timeInterval/1000))
                    let datetime =  Date(timeIntervalSince1970:TimeInterval(timeWithSeconds))
                    self.status.text = Utils.timeAgoSinceDate(date: datetime as NSDate, numericDates: false)
                  }
                 self.status.textColor = UIColor.hexColor("B2B2B2")
              }
            }
          }
        }
      }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    
  }
}
