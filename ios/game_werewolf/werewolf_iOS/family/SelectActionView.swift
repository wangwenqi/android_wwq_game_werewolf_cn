//
//  SelectActionView.swift
//  game_werewolf
//
//  Created by Hang on 2017/10/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

enum RoleType {
  case POSITION
  case NORMAL
}

class SelectActionView: SpringView {
  
    //MARK: variable
    var tags:Int = -1
    var data:JSON?
//    var seeProfile:()->()?
    //MARK: outlets
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var bg: UIView!
    @IBOutlet weak var userInfoButton: UIButton!
    @IBOutlet weak var kickOut: UIButton!
    @IBOutlet weak var transformButton: UIButton!
    
    @IBOutlet weak var deputyButton: UIButton!
    @IBOutlet weak var elder: UIButton!
    @IBOutlet weak var elite: UIButton!
    @IBOutlet weak var memeber: UIButton!
  
    var userInfo:JSON?
    typealias sucess = ()->()
    var setPostionSucess:sucess?
    
    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
        //画出三角形
        self.frame = CGRect(x: 100, y: 100, width: 150, height: 180)
      
    }

  static func generaterViewBy(type:RoleType,frame:CGRect) -> SelectActionView? {
      if type == .NORMAL {
       let view = Bundle.main.loadNibNamed("SelectAction", owner: nil, options: nil)?.last as? SelectActionView
        view?.transformButton.isHidden = true
        view?.frame = frame
        return view
      }else{
        let view = Bundle.main.loadNibNamed("SelectAction", owner: nil, options: nil)?.last as? SelectActionView
        view?.frame = frame
        return view
      }
    }
  
    func changeTrangleIf(tooHeight:Bool) {
      let path = UIBezierPath()
      if tooHeight {
     
        path.move(to: CGPoint(x: 10 , y: 170))
        path.addLine(to: CGPoint(x: 0, y: 160))
        path.addLine(to: CGPoint(x: 10, y: 150))
        path.addLine(to: CGPoint(x: 10, y: 170))
        path.close()
        path.lineWidth = 1
        path.fill()

      }else{
        path.move(to: CGPoint(x: 10 , y: 10))
        path.addLine(to: CGPoint(x: 0, y: 20))
        path.addLine(to: CGPoint(x: 10, y: 30))
        path.addLine(to: CGPoint(x: 10, y: 10))
        path.close()
        path.lineWidth = 1
        path.fill()
      }
      
      let shapeLayer = CAShapeLayer()
      shapeLayer.path = path.cgPath
      shapeLayer.fillColor = UIColor.hexColor("EDEDF3").cgColor
      self.layer.addSublayer(shapeLayer)
    }
  
    //MARK: animation
    func diss() {
      self.animation = "fadeInRight"
      self.animate()
      self.removeFromSuperview()
    }
  
    func show() {
      
    }
    
    //MARK: Action
    @IBAction func setToDeputy(_ sender: Any) {
      if let id = userInfo?["id"].string {
        let url = RequestUrls.setPosition + "\(id)" + "/deputy"
        setPostion(url: url)
      }
    }
    @IBAction func setToElder(_ sender: Any) {
      if let id = userInfo?["id"].string {
        let url = RequestUrls.setPosition + "\(id)" + "/elder"
        setPostion(url: url)
      }
    }
    @IBAction func setToEite(_ sender: Any) {
      if let id = userInfo?["id"].string {
        let url = RequestUrls.setPosition + "\(id)" + "/elite"
        setPostion(url: url)
      }
    }
    
    @IBAction func setToMember(_ sender: Any) {
      if let id = userInfo?["id"].string {
        let url = RequestUrls.setPosition + "\(id)" + "/memeber"
        setPostion(url: url)
      }
    }
  
  func setPostion(url:String) {
      diss()
      XBHHUD.showLoading()
      RequestManager().get(url: url, success: {  (json) in
        XBHHUD.hide()
        if self.setPostionSucess != nil {
          self.setPostionSucess!()
        }
      }) { (code, message) in
        XBHHUD.showError(message)
      }
    }
}
