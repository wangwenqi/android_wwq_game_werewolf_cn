//
//  FanilyEditViewController.swift
//  game_werewolf
//
//  Created by Hang on 2017/10/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import CameraViewController
//import ALCameraViewController
import Qiniu
import ChatKit


class FamilyEditViewController: RotationViewController {
  
    //MARK: variable
    var baseHeight:CGFloat = 0
    var data:JSON?
    var isOwner = false
    var lc_id = ""
    let semaphore = DispatchSemaphore(value: 1)
    var croppingParameters: CroppingParameters {
      return CroppingParameters(isEnabled: true, allowResizing: true, allowMoving: true)
    }
    //MARK: outlets
    @IBOutlet weak var quitButton: XBHButton!
    @IBOutlet weak var cachedImageView: NSLayoutConstraint!
    @IBOutlet weak var chooseImageWidth: NSLayoutConstraint!
    @IBOutlet weak var messageBlock: UISwitch!
    @IBOutlet weak var editableView: UIView!
    @IBOutlet weak var editView: SpringView!
    @IBOutlet weak var chooseImage: UIButton!
    @IBOutlet weak var unionImageView: UIImageView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var shortName: UITextField!
    @IBOutlet weak var familyDes: UITextField!
    @IBOutlet weak var familyDesTextView: UITextView!
    @IBOutlet weak var saveButton: XBHButton!
    @IBOutlet weak var showEditButton: UIButton!
    @IBOutlet weak var familyDesHeight: NSLayoutConstraint!
    //MARK: Action
    @IBAction func editUnion(_ sender: Any) {
      if let button = sender as? UIButton {
        button.isSelected = !button.isSelected
        editView.isHidden = !button.isSelected
      }
    }
  @IBAction func selectImage(_ sender: Any) {
    let cameraViewController = CameraViewController(croppingParameters: croppingParameters, allowsLibraryAccess: true) { [weak self] image, asset in
      self?.unionImageView.isHidden = false
      if image != nil {
       self?.unionImageView.image = image
      }
      self?.dismiss(animated: true, completion: nil)
    }
    present(cameraViewController, animated: true, completion: nil)
  }
  
  @IBAction func saveFamilyInfo(_ sender: Any) {
    //点击保存
    var name = ""
    var shortName = ""
    var des = ""
    if self.name.text != nil {
      if (self.name.text?.isEmpty)! {
        XBHHUD.showError(NSLocalizedString("家族名不能为空", comment: ""))
        return
      }else{
        name = self.name.text!
        name = name.trimmingCharacters(in: .whitespaces)
        if name == "" {
          XBHHUD.showError(NSLocalizedString("家族名不能为空", comment: ""))
          return
        }
        if fmailyNameCheck(string: name) == false {
          return
        }
      }
    }
    
    if self.shortName.text != nil {
      if (self.shortName.text?.isEmpty)! {
        XBHHUD.showError(NSLocalizedString("家族简称不能为空", comment: ""))
        return
      }else{
        shortName = self.shortName.text!
        if checkAllchinese(text: shortName) == false {
          XBHHUD.showError(NSLocalizedString("家族简称格式错误，只能为两个汉字", comment: ""))
          return
        }
      }
    }
    
    if self.familyDesTextView.text != nil {
      if (self.familyDesTextView.text?.isEmpty)! {
        XBHHUD.showError(NSLocalizedString("家族介绍不能为空", comment: ""))
        return
      }else{
        des = self.familyDesTextView.text!
        let result = des.trimmingCharacters(in:.whitespaces)
        if result == "" {
          XBHHUD.showError(NSLocalizedString("家族介绍不能为空", comment: ""))
          return
        }
        //是不是在2-200个之间
        if des.length < 2 || des.length > 200 {
          XBHHUD.showError(NSLocalizedString("家族介绍在2-200个字之间", comment: ""))
          return
        }
      }
    }
    
    if self.unionImageView.image != nil {
      //家族图标不为空，不上传
      /**
       *    上传完成后的回调函数
       *
       *    @param info 上下文信息，包括状态码，错误值
       *    @param key  上传时指定的key，原样返回
       *    @param resp 上传成功会返回文件信息，失败为nil; 可以通过此值是否为nil 判断上传结果
       */
      XBHHUD.showLoading()
      let imageFilePath = OcMethod.getImagePath(self.unionImageView.image)
      RequestManager().post(url: RequestUrls.qnToken, success: {[weak self] (json) in
        if let token = json["token"].string {
          if let path = imageFilePath {
                let para = ["name":name,"short_name":shortName,"desc":des]
                self?.uploadImage(imagePath: path, token: token,para:para)
          }else{
              XBHHUD.showError(NSLocalizedString("上传图片失败", comment: ""))
          }
        }else{
           XBHHUD.showError(NSLocalizedString("上传图片失败", comment: ""))
        }
      }, error: { (code, message) in
        XBHHUD.showError(message)
      })
    }else{
      if let url = data?["group"]["image"].string {
        let para = ["name":name,"short_name":shortName,"desc":des,"image":url,"image_id":""]
        upload(para: para)
      }else{
        XBHHUD.showError("未知错误")
      }
    }
  }
  
  @IBAction func leaveGroup(_ sender: Any) {
    if isOwner {
        XBHHUD.showLoading()
        let params = ["limit":"20","skip":"0"]
        RequestManager().get(url: RequestUrls.groupMembers,parameters: params, success: { [weak self] (json) in
          XBHHUD.hide()
          self?.performSegue(withIdentifier: "ChangeMaster", sender: json)
        }) { (code, message) in
          XBHHUD.hide()
          XBHHUD.showError(message)
        }
    }else{
      XBHHUD.showLoading()
      RequestManager().get(url: RequestUrls.quit, success: { (json) in
        XBHHUD.hide()
        //退出当前页面
        //告诉RN退出
        SwiftConverter.shareInstance.sendMessagesToRN(json: JSON(["action":"ACTION_QUIT_FAMILY"]))
        CurrentUser.shareInstance.familyInfo = nil //退出的时候把家族信息设置为空
        self.leaveCurrentView()
      }, error: { (code, message) in
        XBHHUD.hide()
        XBHHUD.showError(message)
      })
    }
  }
  
  @IBAction func blockGroup(_ sender: Any) {
  }

  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ChangeMaster" {
      if let list = segue.destination as? FamilyMemberListViewController {
        if let json = sender as? JSON {
          list.data = json
          list.changeMasterModel = true
        }
      }
    }
  }
  
  func leaveCurrentView() {
    let currentvc = Utils.getCurrentVC(vc:(Utils.getKeyWindow()?.rootViewController)!)
    if (currentvc?.isKind(of: FamilyEditViewController.self))! {
      currentvc?.dismiss(animated: true, completion: {
        let lastVC = Utils.getCurrentVC(vc:(Utils.getKeyWindow()?.rootViewController)!)
        if (lastVC?.isKind(of: UserInfoViewController.self))! {
          lastVC?.dismiss(animated: true, completion:{
          })
        }
        if (lastVC?.isKind(of:FamilyChatViewController.self))! {
          lastVC?.dismiss(animated: true, completion:{
          })
        }
      })
    }else{
      self.dismiss(animated: true, completion: {
        self.leaveCurrentView()
      })
    }
  }

  func uploadImage(imagePath:String,token:String,para:[String:String]) {
    let uploadManager = QNUploadManager()
    let uploadOption = QNUploadOption.init(mime: nil, progressHandler: { (str,percent) in
    }, params: nil, checkCrc: false, cancellationSignal:nil)
    uploadManager?.putFile(imagePath, key: nil, token:token, complete: { (info, key,rep) in
//      self.semaphore.signal()
      if rep != nil {
        if let imageUrl = rep!["url"] as? String {
          var params = para
          params["image"] = imageUrl
          if let key = rep!["key"] as? String {
             params["image_id"] = key
          }
          self.upload(para: params)
        }
      }else{
        XBHHUD.showError(NSLocalizedString("上传图片失败", comment: ""))
      }
    }, option: uploadOption)
  }
  
  func upload(para:[String:String]) {
    self.view.endEditing(true)
    XBHHUD.showLoading()
    RequestManager().post(url: RequestUrls.updateGroup, parameters:para, success: {[weak self] (json) in
      XBHHUD.hide()
      self?.editView.isHidden = true
      self?.showEditButton.isSelected = false
      XBHHUD.showSuccess("修改成功")
      self?.data = json
      self?.setUpUI()
    }) { (code, message) in
      XBHHUD.hide()
      XBHHUD.showError(message)
    }
  }
  
  func leftClicked() {
    self.performSegue(withIdentifier: "unwindDetail", sender: self)
  }
  
    //MARK: LiftCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
      
        let client = LCChatKit.sharedInstance().client
        let query = client?.conversationQuery()
        query?.getConversationById(lc_id, callback: { (conversation, error) in
          if conversation != nil {
            DispatchQueue.main.async {
              self.messageBlock.isOn = (conversation?.muted)!
            }
          }
        })
        //notification
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  

    override func viewWillAppear(_ animated: Bool) {
      self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
      let muted = self.messageBlock.isOn
      let lcid = self.lc_id
      if lcid != nil {
        Utils.saveMute(mute:muted,muteid:lcid)
      }
    }
  
  //MARK: UI
  func setUpUI() {
    if Utils.getCurrentDeviceType() == .iPhone4 || Utils.getCurrentDeviceType() == .iPhone5 {
        self.familyDesHeight.constant = 40
    }
    self.navigationItem.title = NSLocalizedString("家族设置", comment: "")
    self.editableView.isHidden = false
    self.chooseImage.layer.cornerRadius = 8
    self.chooseImage.layer.masksToBounds = true
    self.chooseImage.layer.borderWidth = 1
    self.chooseImage.layer.borderColor = UIColor.hexColor("D9D9D9").cgColor
    setUpNavigation()
    self.shortName.delegate = self
    self.shortName.tag = 42
    self.name.delegate = self
    self.name.tag = 41
    self.familyDesTextView.tag = 43
    self.familyDesTextView.layer.borderColor = UIColor.hexColor("E6E5E6").cgColor
    self.familyDesTextView.layer.cornerRadius = 5
    self.familyDesTextView.layer.masksToBounds = true
    self.familyDesTextView.layer.borderWidth = 1
    self.familyDesTextView.delegate = self
    quitButton.titleLabelShadowColor = UIColor.clear
    saveButton.titleLabelShadowColor = UIColor.clear
    self.unionImageView.layer.cornerRadius = 3
    self.unionImageView.layer.masksToBounds = true
    if let ownId = data?["group"]["own_id"].string {
      if ownId == CurrentUser.shareInstance.id {
        //说明自己是管理员
        self.isOwner = true
        self.editableView.isHidden = false
        self.editView.isHidden = false
        self.showEditButton.isSelected = true
        //把退出改为转让族长
        self.quitButton.setTitle(NSLocalizedString("转让族长", comment: ""), for: .normal)
      }else{
        self.editableView.isHidden = true
      }
    }
    if let name = data?["group"]["name"].string {
      self.name.text = name
    }
    if let short = data?["group"]["short_name"].string {
      self.shortName.text = short
    }
    
    if let des = data?["group"]["desc"].string {
      self.familyDesTextView.text = des
    }
    
    if let lc_id = data?["group"]["lc_id"].string {
      self.lc_id = lc_id
    }
    self.unionImageView.setNeedsLayout()
    self.unionImageView.layoutIfNeeded()
    if Utils.getCurrentDeviceType() == .iPhone5 ||   Utils.getCurrentDeviceType() == .iPhone4 {
      self.cachedImageView.constant = 45
      self.chooseImageWidth.constant = 45
    }else{
      self.cachedImageView.constant = 80
      self.chooseImageWidth.constant = 80
    }
   
    if let image = data?["group"]["image"].string {
      let url = URL.init(string: image)
      self.unionImageView.isHidden = false
      self.unionImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named:"ico_family_default"))
    }
  }
  
  func checkAllchinese(text:String) -> Bool{
    let hanzi = "[\\u4e00-\\u9fa5]"
    let hanziRegx = try! NSRegularExpression(pattern: hanzi, options:[])
    let newRange = NSRange(location: 0, length:text.length)
    let hanziCount = hanziRegx.numberOfMatches(in: text, options: [], range: newRange)
    if hanziCount != 2 {
      return false
    }
    if text.length != 2 {
      return false
    }
    return true
  }
  //家族名称
  func fmailyNameCheck(string:String) -> Bool{
    var string = string.trimmingCharacters(in: .whitespaces)
    let hanzi = "[\\u4e00-\\u9fa5]"
    let number = "[0-9]"
    let eng = "[a-zA-Z]"
    let numRegx = try! NSRegularExpression(pattern: number, options:[])
    let engRegx = try! NSRegularExpression(pattern: eng, options:[])
    let hanziRegx = try! NSRegularExpression(pattern: hanzi, options:[])
    //range
    let newRange = NSRange(location: 0, length:string.length)
    let hanziCount = hanziRegx.numberOfMatches(in: string, options: [], range: newRange)
    let engCount = engRegx.numberOfMatches(in: string, options: [], range: newRange)
    let numCount = numRegx.numberOfMatches(in: string, options:[], range: newRange)
    if hanziCount + engCount + numCount  < string.length {
      //说明有别的字符串
      //包含表情
        if hanziCount * 2 + engCount + numCount > 16 {
          //汉字和英文加起来大于16
          //有问题
          XBHHUD.showError("家族名称最大8个字符")
          return false
        }else{
          //如果小于16
          //
          let emojiCount = string.length - (hanziCount+engCount+numCount)
          if hanziCount * 2 + engCount + numCount + emojiCount > 16 {
            //如果加起来大于16，就是不正确的
             XBHHUD.showError("家族名称最大8个字符")
             return false
          }
        }
    }else{
      //只包含数字和引文
      if hanziCount * 2 + engCount + numCount > 16 {
        //汉字和英文加起来大于16
        //有问题
         XBHHUD.showError("家族名称最大8个字符")
         return false
      }
    }
    return true
  }
  
  func checkIfBiggerThan(num:Int,minNum:Int,string:String,showError:Bool) -> Bool {
    //正则表达式
    let hanzi = "[\\u4e00-\\u9fa5]"
    let number = "[0-9]"
    let eng = "[a-zA-Z]"
    let numRegx = try! NSRegularExpression(pattern: number, options:[])
    let engRegx = try! NSRegularExpression(pattern: eng, options:[])
    let hanziRegx = try! NSRegularExpression(pattern: hanzi, options:[])
    //range
    let newRange = NSRange(location: 0, length:string.length)
    let hanziCount = hanziRegx.numberOfMatches(in: string, options: [], range: newRange)
    let engCount = engRegx.numberOfMatches(in: string, options: [], range: newRange)
    if hanziCount + engCount < string.length {
      if showError {
         XBHHUD.showError( NSLocalizedString("只能包含汉字和字母", comment: ""))
      }
      return true
    }
    if hanziCount * 2 + engCount > num {
      if showError {
        XBHHUD.showError("家族名称最多\(num/2)个汉字或者\(num)个字母")
      }
      return true
    }else{
      if hanziCount * 2 + engCount < minNum {
        if showError {
           XBHHUD.showError("家族名称最少\(minNum/2)个汉字或者\(minNum)个字母")
        }
        return true
      }
      return false
    }
  }
  
  func setUpNavigation() {
    navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "room_bg_navbar"), for: .default)
    navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "room_icon_leftarrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(FamilyEditViewController.leftClicked))
  }
  
  //MARK: delegate
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.view.endEditing(true)
  }
  
  //MARK: notifaction
  
  func keyBoardWillShow(notification: Notification) {
    let userInfo = notification.userInfo
    let keyBoardBounds = (userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    var rect = self.view.frame
    rect.origin.y = 0
    rect.origin.y =  rect.origin.y - (keyBoardBounds.size.height)/2
    self.view.frame = rect
  }
  
  func keyBoardWillHide(notification: Notification) {
    var rect = self.view.frame
    rect.origin.y = 64
    self.view.frame = rect
  }
}

extension FamilyEditViewController:UITextViewDelegate {
  

    func textViewDidChange(_ textView: UITextView) {
      let length = textView.text?.characters.count
      if length ?? 0 > 200 {
        XBHHUD.showError(NSLocalizedString("家族介绍最多200字符", comment: ""))
        if let text = textView.text {
          let index =  text.index(text.startIndex , offsetBy: 200)
          textView.text = text.substring(to: index)
        }
      }
    }
}

extension FamilyEditViewController : UITextFieldDelegate {
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    if textField.tag == 42 {
      if checkAllchinese(text: textField.text ?? "") == false {
        XBHHUD.showError(NSLocalizedString("家族简称格式错误，只能为两个汉字", comment: ""))
        textField.text = ""
      }
    }
    if textField.tag == 41 {
//      if checkIfBiggerThan(num: 16,minNum: 2, string: textField.text ?? "", showError: true) {
//        textField.text = ""
//      }
      if fmailyNameCheck(string: textField.text ?? "") == false {
         textField.text = ""
      }
    }
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    let textLength = textField.text?.length ?? 0
    //是不是汉字或者英文
    let hanzi = "[\\u4e00-\\u9fa5]"
    let eng = "[a-zA-Z]"
    let engRegx = try! NSRegularExpression(pattern: eng, options:[])
    let hanziRegx = try! NSRegularExpression(pattern: hanzi, options:[])
    //获取即将被取代的文字
    var replaceString:String = ""
    if let replace:NSString = textField.text as? NSString {
      if range.location <= replace.length && range.length > 0 {
         replaceString = replace.substring(with: range)
      }
    }
    
    //ranged
    let rRange = NSRange(location: 0, length:replaceString.length)
    let newRange = NSRange(location: 0, length:string.length)
    let orange = NSRange(location: 0, length:(textField.text?.length ?? 0))
    //要替换的文字
    let rhanziCount = replaceString == "" ? 0 : hanziRegx.numberOfMatches(in: replaceString, options: [], range: rRange)
    let rengCount = replaceString == "" ? 0 : engRegx.numberOfMatches(in: replaceString, options: [], range: rRange)
    //已经存在的文字
    let ohanziCount = textField.text == nil ||  textField.text == ""  ? 0 :hanziRegx.numberOfMatches(in: textField.text!, options: [], range: orange)
    let oengCount = textField.text == nil || textField.text == ""  ? 0 :engRegx.numberOfMatches(in: textField.text!, options: [], range: orange)
    //新输入的文字
    let hanziCount = hanziRegx.numberOfMatches(in: string, options: [], range: newRange)
    let engCount = engRegx.numberOfMatches(in: string, options: [], range: newRange)
    let spaceCount = string == "" ? 1 : 0
    //总长度
    let total = ohanziCount * 2 + oengCount + hanziCount * 2 + engCount - (rhanziCount * 2 + rengCount)
    let newLength = textLength + string.length - range.length
    if textField.tag == 42 {
      if hanziCount + spaceCount  + engCount  == 0 {
        XBHHUD.showError(NSLocalizedString("家族简称必须是汉字", comment: ""))
        textField.text = ""
        return false
      }
      if total <= 4 {
        return true
      }else{
        XBHHUD.showError( NSLocalizedString("家族简称必须为两个汉字", comment: ""))
        return false
      }
    }
    
    if textField.tag == 41 {
//      if hanziCount + engCount + spaceCount == 0 {
//        XBHHUD.showError(NSLocalizedString("家族名称必须是汉字或者字母", comment: ""))
//        return false
//      }
      if total <= 16 {
        return true
      }else{
        XBHHUD.showError(NSLocalizedString("家族名称最多8个字符", comment: ""))
        return false
      }
    }
    return true
  }
}

extension String {
  func nsRange(from range: Range<String.Index>) -> NSRange {
    let from = range.lowerBound.samePosition(in: utf16)
    let to = range.upperBound.samePosition(in: utf16)
    return NSRange(location: utf16.distance(from: utf16.startIndex, to: from),
                   length: utf16.distance(from: from, to: to))
  }
}

