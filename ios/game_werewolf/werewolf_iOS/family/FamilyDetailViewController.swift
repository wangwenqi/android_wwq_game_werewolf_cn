//
//  FamilyDetailViewController.swift
//  game_werewolf
//
//  Created by Hang on 2017/10/17.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class FamilyDetailViewController: RotationViewController {

    //MARK:varibable
    var isMaster = false
    var belongTo = false {
      didSet{
        if belongTo {
          self.setButton.isHidden = false
          self.rightArrow.isHidden = false
         }
         configHeight()
      }
    }
    var aleradyHaveFamily = false {
      didSet{
        configHeight()
      }
    }
    //MARK: header
    
    @IBOutlet weak var levelImage: UIImageView!
    @IBOutlet weak var activePoint: UILabel!
    @IBOutlet weak var setButton: UIButton!
    @IBOutlet weak var unionImage: UIImageView!
    @IBOutlet weak var unionName: UILabel!
    @IBOutlet weak var unionLevel: UILabel!
    @IBOutlet weak var rightArrow: UIButton!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    //MARK: content
    @IBOutlet weak var unionDes: UITextView!
    @IBOutlet weak var unionCurrentPeople: UILabel!
    //MARK: bottom
    @IBOutlet weak var unionBottomView: UIView!
    @IBOutlet weak var unionCollectionView: FamilyDetailCollectionView!
    @IBOutlet weak var newRequest: UIImageView!
    
    @IBOutlet weak var unionCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var gidLalbe: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentscrollView: UIScrollView!
//  @IBOutlet weak var requestButton: UIScrollView!
    @IBOutlet weak var requestButton: XBHButton!
    @IBOutlet weak var requestButtonPaddingBottom: NSLayoutConstraint!
    @IBOutlet weak var levelButton: XBHButton!
  
    @IBAction func requestJoin(_ sender: Any) {
      XBHHUD.showLoading()
      RequestManager().get(url: RequestUrls.requestJoin + (datas?["group"]["_id"].string ?? ""), success: { (json) in
        XBHHUD.hide()
        XBHHUD.showSuccess("申请已经发送")
      }) { (code, messaeg) in
        XBHHUD.showError(messaeg)
      }
    }
  
    var datas:JSON?
    var members:[JSON]?
    let defaultImageView = UIView()
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        dealWithData()
    }
  
    override func viewWillAppear(_ animated: Bool) {
      self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
      self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: UI
    func setUpUI() {
      if contentscrollView.contentOffset.y > 40 {
        setButton.layer.borderColor = UIColor.black.cgColor
      }else{
        setButton.layer.borderColor = UIColor.white.cgColor
      }
       setButton.layer.borderWidth = 1
       setButton.layer.masksToBounds = true
      //人数
      self.unionCurrentPeople.backgroundColor = UIColor.hexColor("B1E4FD")
      self.unionCurrentPeople.layer.cornerRadius = 10
      self.unionCurrentPeople.layer.masksToBounds = true
      //描述
      self.unionDes.layer.cornerRadius = 5
      self.unionDes.layer.borderColor = UIColor.hexColor("D9D9D9").cgColor
      self.unionDes.layer.borderWidth = 1
      //底部
      unionBottomView.layer.cornerRadius = 5
      unionBottomView.layer.masksToBounds = true
      unionBottomView.layer.borderColor = UIColor.hexColor("C2C2C2").cgColor
      unionBottomView.layer.borderWidth = 1
        //
      self.unionCollectionView.delegate = self
      self.unionCollectionView.dataSource = self
      self.contentscrollView.backgroundColor = UIColor.init(patternImage: UIImage.init(named:"srollviewBg")!)
      self.contentscrollView.delegate = self
    }
  
    func configHeight() {
      let singleWidth = (Screen.width-86)/4
      let row = ceilf((Float(members == nil ? 0 : (members?.count)!) / Float(4)))
      var rect = unionCollectionView.frame
      rect.size.height = CGFloat(Float(singleWidth ) * row + (row) * 10 + 10 + 10)
      var contentRect = rect
      contentRect.size.height = rect.size.height
      self.unionCollectionHeight.constant = rect.size.height
      //隐藏申请按钮
      if belongTo  {
        requestButtonPaddingBottom.constant = -40
      }else{
        requestButton.startColor = UIColor.hexColor("FFF82E")
        requestButton.endColor = UIColor.hexColor("FFF82E")
        requestButton.setTitleColor(UIColor.white, for: .normal)
        requestButton.buttonShdowColor = UIColor.hexColor("FED156")
      
      }
      if aleradyHaveFamily {
        requestButtonPaddingBottom.constant = -40
      }
      requestButton.titleLabelShadowColor = UIColor.clear
      requestButton.refrshBackgroundColor()
    }
  
    func dealWithData() {
      if let json = self.datas {
       if let gruops = json["group"].dictionary {
        //判断自己是不是属于这个家族
        CurrentUser.shareInstance.getFamilyInfo(sucess: { (family) in
          if family != nil {
            if family?.group_id == gruops["_id"]?.string {
              //自己属于这个家族
              self.belongTo = true
            }else{
              //自己不属于这个家族,但是自己有家族
              self.belongTo = false
              self.aleradyHaveFamily = true
            }
          }else{
            self.belongTo = false
          }
         })
         if let name = gruops["name"]?.string {
            self.unionName.text = name
          }
          if let levelStr = gruops["level"]?.string {
            self.levelButton.setTitle(levelStr, for: .normal)
          }
          if let des = gruops["desc"]?.string {
            self.unionDes.text = des
          }
          if let count = gruops["member_count"]?.int {
            if let max_members = gruops["max_members"]?.int {
               self.unionCurrentPeople.text = "\(count)" + "/" + "\(max_members )"
            }
          }
          if let gid = gruops["gid"]?.int {
            self.gidLalbe.text = "ID:\(gid)"
          }
          //根据家族等级设置button颜色
          if let level_val = gruops["level_val"]?.int {
            configLevelButton(level_val)
          }
          //设置活跃值
          if let activePoint = gruops["active_point"]?.int {
            self.activePoint.text = NSLocalizedString("活跃值", comment: "") + " " + "\(activePoint)"
          }
          if let levelImage = gruops["level_image"]?.string {
            let url = URL.init(string: levelImage)
            self.levelImage.sd_setImage(with: url, placeholderImage: UIImage.init(named:"勋章初级图标"))
          }
          if let level_val = gruops["level_val"]?.int {
            let level  = level_val > 10 ? 10 : level_val
            self.levelImage.image = UIImage.init(named: "level\(level)")
          }
          self.unionCollectionView.setNeedsLayout()
          self.unionCollectionView.layoutIfNeeded()
          if let member = gruops["members"]?.arrayValue {
            self.members = member
            self.unionCollectionView.reloadData()
          }
          if let image = gruops["image"]?.string {
            let url = URL.init(string: image)
            self.unionImage.sd_setImage(with: url, placeholderImage: UIImage.init(named:"ico_family_default"))
          }
        
          if let ownId = gruops["own_id"]?.string {
            if ownId == CurrentUser.shareInstance.id {
              //说明自己是管理员
              self.isMaster = true
            }
          }
          self.unionImage.setNeedsLayout()
          self.unionImage.layoutIfNeeded()
          self.unionImage.layer.cornerRadius = self.unionImage.size.width / 2
          self.unionImage.layer.masksToBounds = true
          self.unionImage.layer.borderWidth = 2
          self.unionImage.layer.borderColor = UIColor.hexColor("f47103").cgColor
         //有没有新的申请
          if let new = gruops["require_count"]?.int {
            if new == 0 {
              self.newRequest.isHidden = true
            }else{
              self.newRequest.isHidden = false
            }
          }else{
              self.newRequest.isHidden = true
          }
         //设置跳转
//          self.unionCollectionView.toMem = {
//             self.showMemberList("ss")
//          }
        }
      }
    }
    
    func configLevelButton(_ level:Int) {
        if level >= 1 && level <= 3 {
           self.levelButton.setTitleColor(UIColor.hexColor("852600"), for: .normal)
            levelButton.setBackgroundImage(UIImage.init(named: "1-3"), for: .normal)
        }
        if level >= 4 && level <= 6 {
            self.levelButton.setTitleColor(UIColor.hexColor("212121"), for: .normal)
            levelButton.setBackgroundImage(UIImage.init(named: "4-6"), for: .normal)
        }
        if level >= 7 && level <= 9 {
            self.levelButton.setTitleColor(UIColor.hexColor("4c4121"), for: .normal)
             levelButton.setBackgroundImage(UIImage.init(named: "7-9"), for: .normal)
        }
        if level >= 10  {
            self.levelButton.setTitleColor(UIColor.hexColor("6c1321"), for: .normal)
            levelButton.setBackgroundImage(UIImage.init(named: "10-12"), for: .normal)
        }
        levelButton.titleLabelShadowColor = UIColor.clear
    }
    //MARK: ACTION
    @IBAction func performUnwindSegue(segue: UIStoryboardSegue) {
      if segue.identifier == "unwindDetail" {
        if let edit = segue.source as? FamilyEditViewController{
          if edit.data != nil {
           self.datas = edit.data
            setUpUI()
            dealWithData()
            self.unionCollectionView.reloadData()
          }
        }
      }
      if segue.identifier == "listToDetail" {
        //从列表回来的
        if let list = segue.source as? FamilyMemberListViewController {
          //重新获取一下信息，然后刷新
          if let jsonData = list.data {
            if let gid = jsonData["groupId"].string {
              XBHHUD.showLoading()
             let para = ["maxUser":"30"]
              RequestManager().get(url:RequestUrls.groupInfo + gid,parameters: para, success: { (json) in
                XBHHUD.hide()
                self.datas = json
                self.setUpUI()
                self.dealWithData()
                self.unionCollectionView.reloadData()
              }) { (code, message) in
                XBHHUD.showError(message)
              }
            }
          }
        }
      }
    }
    //家族等级介绍
    @IBAction func seeFamilylevel(_ sender: Any) {
        let web = StoreCenterViewController()
        web.type = .FamilyLevel
        let nav = RotationNavigationViewController(rootViewController: web)
        DispatchQueue.main.async {
          self.present(nav, animated: true, completion: nil)
        }
    }
    //返回
    @IBAction func goBack(_ sender: Any) {
      if (self.navigationController?.viewControllers.first?.isKind(of: FamilyDetailViewController.self))! {
         self.dismiss(animated: true, completion: nil)
      }else{
        self.navigationController?.popViewController(animated: true)
      }
    }
  
    @IBAction func showEdition(_ sender: Any) {
      self.performSegue(withIdentifier: "SHOWEDIT", sender: datas)
    }
  
    @IBAction func showMemberList(_ sender: Any) {
      if let gid = datas?["group"]["_id"].string {
        if belongTo {
          XBHHUD.showLoading()
          let params = ["limit":"20","skip":"0"]
          RequestManager().get(url: RequestUrls.groupMembers,parameters: params , success: { [weak self] (json) in
              XBHHUD.hide()
              self?.performSegue(withIdentifier: "SHOWLIST", sender: json)
          }) { (code, message) in
              XBHHUD.showError(message)
          }
        }
      }
    }
  
  override func viewDidLayoutSubviews() {

    //添加默认的图
    var contains = false
    for v in self.contentscrollView.subviews {
      if v.isEqual(defaultImageView) {
        contains = true
      }
    }
    if contains == false {
      self.contentscrollView.addSubview(defaultImageView)
      defaultImageView.backgroundColor = UIColor.white
      defaultImageView.snp.makeConstraints { (make) in
        make.bottom.equalTo(self.contentscrollView.snp.bottom).offset(300)
        make.width.equalToSuperview()
        make.height.equalTo(300)
      }
      self.contentscrollView.bringSubview(toFront: defaultImageView)
    }
  }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "SHOWLIST" {
      if let list = segue.destination as? FamilyMemberListViewController {
        if let json = sender as? JSON {
          list.data = json
          list.isMaster = self.isMaster
        }
      }
    }
    
    if segue.identifier == "SHOWEDIT" {
      if let list = segue.destination as? FamilyEditViewController {
        if let json = sender as? JSON {
          list.data = json
        }
      }
    }
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if scrollView.contentOffset.y > 40 {
      self.setButton.setTitleColor(UIColor.black, for: .normal)
      setButton.layer.borderWidth = 1
      setButton.layer.borderColor = UIColor.black.cgColor
      backButton.layer.cornerRadius = 15
      backButton.backgroundColor = UIColor.black.withAlphaComponent(0.5)
      
    }else{
      self.setButton.setTitleColor(UIColor.white, for: .normal)
      setButton.layer.borderWidth = 1
      setButton.layer.borderColor = UIColor.white.cgColor
      backButton.layer.cornerRadius = 0
      backButton.backgroundColor = UIColor.clear
    }
    scrollView.bounces = false//(scrollView.contentOffset.y < 0);
  }
}



extension FamilyDetailViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return members == nil ? 0 : (members?.count)!
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "familyMember", for: indexPath) as! FamilyMemberCollectionViewCell
        if let data = members?[indexPath.row] {
          cell.config(json:data)
          collectionView.setNeedsDisplay()
      }
      return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if let json = members?[indexPath.row] {
      let controller = UserInfoViewController()
      controller.peerID = json["user_id"].stringValue
      controller.peerSex = json["sex"].stringValue
      controller.peerName = json["name"].stringValue
      controller.peerIcon = json["image"].stringValue
      controller.isTourist = false
      controller.isself = json["id"].stringValue == CurrentUser.shareInstance.id ? true : false
      controller.comeFromFamily = true
      let rootVC = RotationNavigationViewController.init(rootViewController: controller)
      self.present(rootVC, animated: true, completion: nil)
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width:(Screen.width-86)/4, height:(Screen.width-86)/4)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top:10.0, left: 0.0, bottom: 0.0, right: 10.0)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 10
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 10
  }
}
