//
//  UIViewController+Extension.swift
//  interview
//
//  Created by QiaoYijie on 16/8/17.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation

extension UIViewController {
  
  // 从storyBoard加载一个控制器
  class func loadFromStoryBoard(_ storyBoardName: String,identifier: String) -> UIViewController {
    
    let storyBoard = UIStoryboard.init(name: storyBoardName, bundle: nil)
    let viewController = storyBoard.instantiateViewController(withIdentifier: identifier)
    
    return viewController
  }
  
  class func topWindow() -> UIWindow? {
    return UIApplication.shared.keyWindow
  }
  
  static func topViewController(_ viewController: UIViewController? = nil) -> UIViewController?
  {
    let viewController = viewController ?? (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController
    
    if let navigationController = viewController as? UINavigationController, !navigationController.viewControllers.isEmpty
    {
      return topViewController(navigationController.viewControllers.last)
    } else if let tabBarController = viewController as? UITabBarController,
      let selectedController = tabBarController.selectedViewController
    {
      return topViewController(selectedController)
    } else if let presentedController = viewController?.presentedViewController {
      return topViewController(presentedController)
    }
    
    return viewController
  }
  
  
}
