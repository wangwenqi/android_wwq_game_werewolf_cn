//
//  UIColor+Extension.swift
//  interview
//
//  Created by QiaoYijie on 16/7/4.
//  Copyright © 2016年 orangelab. All rights reserved.
//



import UIKit

extension UIColor {
    
    /// RGB, alpha = 1
    class func rgb(_ r: CGFloat,_ g: CGFloat,_ b: CGFloat) -> UIColor {
        return self.init(red: r/255, green: g/255, blue: b/255, alpha: CGFloat(1.0))
    }
    
    /// RGBA
    class func rgba(_ r: CGFloat,_ g: CGFloat,_ b: CGFloat,_ a: CGFloat) -> UIColor {
        return self.init(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
    
    /// RGB值
    class func rgbValue(_ rgbValue: UInt) -> UIColor {
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16)/255.0, green: CGFloat((rgbValue & 0x00FF00) >> 8)/255.0, blue: CGFloat(rgbValue & 0x0000FF)/255.0, alpha: CGFloat(1.0)
        )
    }
    
    /// RGB Hex
    class func hexColor(_ hexColor: NSString) -> UIColor {
        
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        //        var alpha: CGFloat = 1.0
        var hexColorCode = hexColor as String
        
        hexColorCode = hexColorCode.hasPrefix("#") ? hexColorCode : "#" + hexColorCode
        
        if hexColorCode.hasPrefix("#") {
            let index   = hexColorCode.characters.index(hexColorCode.startIndex, offsetBy: 1)
            let hex     = hexColorCode.substring(from: index)
            let scanner = Scanner(string: hex)
            var hexValue: CUnsignedLongLong = 0
            
            if scanner.scanHexInt64(&hexValue) {
                if hex.characters.count == 6 {
                    red   = CGFloat((hexValue & 0xFF0000) >> 16) / 255.0
                    green = CGFloat((hexValue & 0x00FF00) >> 8)  / 255.0
                    blue  = CGFloat(hexValue & 0x0000FF) / 255.0
                } else if hex.characters.count == 8 {
                    red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                    green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                    blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                    //                    alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
                } else {
                    print("invalid hex code string, length should be 7 or 9")
                    return UIColor.black
                }
            } else {
                print("scan hex error")
                return UIColor.black
            }
        }
        return UIColor(red:CGFloat(red), green: CGFloat(green), blue:CGFloat(blue), alpha: 1.0)
    }
    
    // 随机色
    class func randomColor() -> UIColor {
        let red: CGFloat = CGFloat(arc4random()).truncatingRemainder(dividingBy: 256.0)
        let green: CGFloat = CGFloat(arc4random()).truncatingRemainder(dividingBy: 256.0)
        let blue: CGFloat = CGFloat(arc4random()).truncatingRemainder(dividingBy: 256.0)
        let color = UIColor(red: CGFloat(red/255.0), green: CGFloat(green/255.0), blue: CGFloat(blue/255.0), alpha: 1)
        return color
    }
    
}
