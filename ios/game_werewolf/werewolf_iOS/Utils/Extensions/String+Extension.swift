//
//  String+Extension.swift
//  MIDIMusic
//
//  Created by QiaoYijie on 16/11/18.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation


extension String{
  
  /// NSLocalizedString shorthand
  var localized: String {
    return NSLocalizedString(self, comment: "")
  }
  
  // "Hello World".slicing(from: 6, length: 5) -> "World"
  // 截取字符串 range
  public func slicing(from i: Int, length: Int) -> String? {
    guard length >= 0, i >= 0, i < characters.count  else {
      return nil
    }
    guard i.advanced(by: length) <= characters.count else {
      return slicing(at: i)
    }
    guard length > 0 else {
      return ""
    }
    return self[safe: i..<i.advanced(by: length)]
  }
  
  //  "Hello World".slicing(at: 6) -> "World"
  public func slicing(at i: Int) -> String? {
    guard i < characters.count else {
      return nil
    }
    return self[safe: i..<characters.count]
  }
  
  // 妈妈再也不用担心我越界了
  // "Hello World!"[3] -> "l"
  // "Hello World!"[20] -> nil
  public subscript(safe i: Int) -> String? {
    guard i >= 0 && i < characters.count else {
      return nil
    }
    return String(self[index(startIndex, offsetBy: i)])
  }
  
  //  "Hello World!"[6..<11] -> "World"
  //  "Hello World!"[21..<110] -> nil
  public subscript(safe range: CountableRange<Int>) -> String? {
    guard let lowerIndex = index(startIndex, offsetBy: max(0,range.lowerBound), limitedBy: endIndex) else {
      return nil
    }
    guard let upperIndex = index(lowerIndex, offsetBy: range.upperBound - range.lowerBound, limitedBy: endIndex) else {
      return nil
    }
    return self[lowerIndex..<upperIndex]
  }
  
  /// SwifterSwift: Safely subscript string within a closed range.
  ///
  ///    "Hello World!"[6...11] -> "World!"
  ///    "Hello World!"[21...110] -> nil
  ///
  /// - Parameter range: Closed range.
  public subscript(safe range: ClosedRange<Int>) -> String? {
    guard let lowerIndex = index(startIndex, offsetBy: max(0,range.lowerBound), limitedBy: endIndex) else {
      return nil
    }
    guard let upperIndex = index(lowerIndex, offsetBy: range.upperBound - range.lowerBound + 1, limitedBy: endIndex) else {
      return nil
    }
    return self[lowerIndex..<upperIndex]
  }
  
  
    //分割字符
    func split(_ s:String)->[String]{
        if s.isEmpty{
            var x=[String]()
            for y in self.characters{
                x.append(String(y))
            }
            return x
        }
        return self.components(separatedBy: s)
    }
    //去掉左右空格
    func trim()->String{
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    //是否包含字符串
    func has(_ s:String)->Bool{
        if (self.range(of: s) != nil) {
            return true
        }else{
            return false
        }
    }
    //是否包含前缀
    func hasBegin(_ s:String)->Bool{
        if self.hasPrefix(s) {
            return true
        }else{
            return false
        }
    }
    //是否包含后缀
    func hasEnd(_ s:String)->Bool{
        if self.hasSuffix(s) {
            return true
        }else{
            return false
        }
    }
    //统计长度
    //    func length()->Int{
    //        return self.characters.count
    //    }
    //统计长度(别名)
    func size()->Int{
        return self.characters.count
    }
    /*
     //截取字符串
     func substr(range:Int...)->String{
     if range[0]==0{
     return self.substringToIndex(range[1])
     }else{
     return self.substringFromIndex(range[0])
     }
     }
     //重复字符串
     func repeat(times: Int) -> String {
     var result = ""
     for i in 0..times {
     result += self
     }
     return result
     }*/
    //反转
    func reverse()-> String{
        let s=Array(self.split("").reversed())
        var x=""
        for y in s{
            x+=y
        }
        return x
    }
    func toJsonObject() -> (NSDictionary?,NSError?) {
        //TODO 这里需要判断下是不是utf-8
        let data:Data = self.data(using: String.Encoding.utf8)!;
        var error:NSError?;
        var json:NSDictionary? = nil;
        
        do {
            json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary;
        } catch let err as NSError {
            error = err;
        }
        
        return (json,error);
    }
    //去掉offer中多余的 UDP\\/TLS\\/
    func clearOffer() -> String {
        let range = NSMakeRange(0, self.characters.count);
        let regex = try? NSRegularExpression(pattern: "UDP\\/TLS\\/", options: NSRegularExpression.Options.allowCommentsAndWhitespace);
        let res:String = regex!.stringByReplacingMatches(in: self, options: NSRegularExpression.MatchingOptions(), range: range, withTemplate: "");
        return res;
    }
    //限制时间
    func limitVideoSpeed(_ speed:String) -> String {
        var lines = self.split("\r\n");
        
        for (i, value) in lines.enumerated() {
            if value.hasPrefix("m=video") {
                lines.insert("b=AS:\(speed)", at: i+1)
                break
            }
        }
        
        return lines.joined(separator: "\r\n");
    }
    
    //在string的某些文字下划横线
    func stringByAddLineString(_ lineString: String) -> NSAttributedString {
        let str = self as NSString
        let attributeString = NSMutableAttributedString(string: self)
        attributeString.addAttributes([NSUnderlineStyleAttributeName: NSNumber(value: Int(1) as Int)], range: str.range(of: lineString))
        
        return attributeString
        
    }
    
    // 计算string的size
    func stringSize(_ maxSize: CGSize, attributes: [String: AnyObject] = [NSFontAttributeName: UIFont.systemFont(ofSize: 17)]) -> CGSize {
        
        let text = NSAttributedString(string: self, attributes: attributes)
        let size = text.boundingRect(with: maxSize, options: [NSStringDrawingOptions.usesLineFragmentOrigin, NSStringDrawingOptions.usesFontLeading], context: nil).size
        return size
    }
    
    //返回一个高亮显示的字符串
    func highLightedStringWith(_ highString: String, color: UIColor) -> NSAttributedString {
        let str = self as NSString
        let str2 = NSMutableAttributedString(string: self)
        str2.addAttribute(NSForegroundColorAttributeName, value: color, range: str.range(of: highString))
        return str2
    }
    
    //特殊化一个字符串某个字符的font
    func speciallyFontStringWith(_ specialString: String, font: UIFont) -> NSAttributedString {
        let str = self as NSString
        let str2 = NSMutableAttributedString(string: self)
        str2.addAttribute(NSFontAttributeName, value: font, range: str.range(of: specialString))
        return str2
    }
    
    // 高亮并特殊化font
    func speciallyStringFontAndColorWith(_ specialString: String, font: UIFont,color:UIColor) -> NSAttributedString {
        let str = self as NSString
        let str2 = NSMutableAttributedString(string: self)
        str2.addAttribute(NSFontAttributeName, value: font, range: str.range(of: specialString))
        str2.addAttribute(NSForegroundColorAttributeName, value: color, range: str.range(of: specialString))
        
        return str2
    }
    
    func dateFromString() -> Date {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = formatter.date(from: self) ?? Date()
        
        return date
    }
  
  func toPointer() -> UnsafePointer<UInt8>? {
    guard let data = self.data(using: String.Encoding.utf8) else { return nil }
    
    let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: data.count)
    let stream = OutputStream(toBuffer: buffer, capacity: data.count)
    
    stream.open()
    data.withUnsafeBytes({ (p: UnsafePointer<UInt8>) -> Void in
      stream.write(p, maxLength: data.count)
    })
    
    stream.close()
    
    return UnsafePointer<UInt8>(buffer)
  }
  
  // 计算文字尺寸
  func sizeWithGivenSize(_ size: CGSize,font: UIFont,paragraph: NSMutableParagraphStyle? = nil) -> CGSize {
    var attributes = [String:AnyObject]()
    attributes[NSFontAttributeName] = font
    if let p = paragraph {
      attributes[NSParagraphStyleAttributeName] = p
    }
    attributes[NSFontAttributeName] = font
    return (self as NSString).boundingRect(with: size,
                                           options: [.usesLineFragmentOrigin,.usesFontLeading],
                                           attributes: attributes,
                                           context: nil).size
  }
  
  var containsEmoji: Bool {
    for scalar in unicodeScalars {
      switch scalar.value {
      case
      0x00A0...0x00AF,
      0x2030...0x204F,
      0x2120...0x213F,
      0x2190...0x21AF,
      0x2310...0x329F,
      0x1F000...0x1F9CF:
        return true
      default:
        continue
      }
    }
    return false
  }
  
  
}



