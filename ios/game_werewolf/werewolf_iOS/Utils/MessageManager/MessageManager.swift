//
//  MessageManager.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/16.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import SocketIO
//import CocoaLumberjack

let SOCKET_DISSCONNECT_FROM_SRVER_NOTIFICATION = "SOCKET_DISSCONNECT_FROM_SRVER_NOTIFICATION";

class MessageManager: NSObject {
  
  static let MAX_RETRY_TIMES = 18;
  
  var currentRetryTime = 1;
  
  var socketIO: SocketIOClient?
  var timer = Timer()
  static let shareInstance = MessageManager()
  fileprivate override init () {
    super.init()
    // 监听connect_info消息
//    onMessageReceived(messageType: .connect_info, delegate: self, selector: #selector(MessageManager.didReceiveConnectInfo(notification:)))
  }
  
  #if DEBUG
  let logView = MessageLogView.view()
  #endif
  
  // 是否正在重连
  var reconnecting = false
  var reconnectTime = 0 {
    didSet {
      Print.dlog("reconnect count \(reconnectTime)")
      if reconnectTime >= currentRetryTime {
        self.close();
        NotificationCenter.default.post(name: Notification.Name.init(GameMessageType.reconnect.rawValue), object: JSON.init(1001))
        XBHHUD.showError(NSLocalizedString("连接失败，请稍后再试", comment: ""));
        resetRoomInfo()
        self.reconnectTime = 0;
        if SwiftConverter.shareInstance.isGaming() == false{
          resetRoomInfo()
        }
      }
    }
  }
  
  // 打开
  func open(roomid:String) {
    
//    getServer(roomid: roomid, canNotPlay: false);
    //判断是否可以玩游戏
    var canNotPlay = false
    if  UserDefaults.standard.object(forKey: "NOPLAY") != nil {
      let dics = UserDefaults.standard.object(forKey: "NOPLAY") as! NSMutableDictionary
      if dics[CurrentUser.shareInstance.id] != nil {
        let dic = dics[CurrentUser.shareInstance.id] as! NSMutableDictionary
        let noplay = dic["end"] as! String
        if Int(noplay) == -1 {
          //说明被永久封了
          canNotPlay = true
          getServer(roomid: roomid, canNotPlay: canNotPlay)
        }else{
          //不是永久封
          let givenEnd = dic["end"] as! String    //后台指定的禁玩结束时间
            //直接以当前时间为准
            if Double(givenEnd)! - Double(Utils.getCurrentTimeStamp()) > 0 {
              //指定的结束时间大于现在的时间
              canNotPlay = true
              getServer(roomid: roomid, canNotPlay: canNotPlay)
            }else{
              //这个时候清空一下数据
//              couldIPlayThisGame(roomid: roomid)
              getServer(roomid: roomid, canNotPlay: false)
            }
        }
      }else{
        //有封禁记录的不是现在的账户
//          couldIPlayThisGame(roomid: roomid)
          getServer(roomid: roomid, canNotPlay: false)
      }
    }else{
     //在当前设备上没有被封记录，请求查看自己是否可以玩游戏
//      couldIPlayThisGame(roomid: roomid)
      getServer(roomid: roomid, canNotPlay: false)
    }
  }
  
  func couldIPlayThisGame(roomid:String) {
      //说明可以了，直接请求
      //是不是超过30分钟
      if Date().timeIntervalSince1970 - (CurrentUser.shareInstance.playble.nextrequestTime) > 0 {
        //超过下一次的请求时间
        playableCheck(roomid:roomid)
      }else{
        getServer(roomid: roomid, canNotPlay: CurrentUser.shareInstance.playble.canNotPlay)
      }
  }
  
  func playableCheck(roomid:String) {
    RequestManager().get(url: (RequestUrls.canplay + "?access_token=\(CurrentUser.shareInstance.token)"), parameters: ["access_token":CurrentUser.shareInstance.token], success: {[weak self] (json) in
      let nextTime = Date().timeIntervalSince1970 + 1800
      let canNotPlay = json["isNoPlay"].boolValue
      let playable = Playable(nextrequestTime: nextTime, canNotPlay: canNotPlay)
      CurrentUser.shareInstance.playble = playable
      self?.getServer(roomid: roomid, canNotPlay: canNotPlay)
    }) {[weak self] (code, message) in
      print("\(message)")
      XBHHUD.hide()
      //返回错误，就使用上一次的缓存
      self?.getServer(roomid: roomid, canNotPlay: CurrentUser.shareInstance.playble.canNotPlay)
    }
  }
  
  func canNotPlayNotice(message:String) {
    DispatchQueue.main.async { [weak self] in
      XBHHUD.hide()
      let notice = Bundle.main.loadNibNamed("canNotPlayNotice", owner: nil, options: nil)?.last as! canNotPlayNotice
      notice.backgroundColor = UIColor.hexColor("E3DDEA")
      notice.textview.text = message
      notice.textview.backgroundColor = UIColor.hexColor("E3DDEA")
      Utils.showAlertView(view: notice)
      //清除房间信息
      self?.resetRoomInfo()
    }
  }
  
  func getServer(roomid:String,canNotPlay:Bool) {
    
    if canNotPlay == true {
        let message = NSLocalizedString("尊敬的用户您好： 因用户多次举报您违反快玩小游戏吧社区友好愉快玩耍规定，现快玩小游戏吧暂时禁玩您的账号，如有疑问请联系suppoet@orangelab.cn", comment: "")
        canNotPlayNotice(message:message)
        return
    }else{
      if let _ = socketIO {
        self.close();
      }
      let lc = UserDefaults.standard.object(forKey: "LOCATION") == nil ? "" :  UserDefaults.standard.object(forKey: "LOCATION") as! String
      var para = ["room_id":roomid,"user_id":CurrentUser.shareInstance.id,"lc":lc,"level":CurrentUser.shareInstance.currentRoomType]
      if roomid == "-1" {
        para = ["room_id":"","user_id":CurrentUser.shareInstance.id,"lc":lc,"level":CurrentUser.shareInstance.currentRoomType]
      }
      RequestManager().post(
        url: RequestUrls.getSocketServerHost,
        parameters: para,
        success: { [weak self] (response) in
          if let serverString = response["server"].string,
            let url = URL.init(string: serverString) {
            
            print("----rommid\(roomid)------\(url)------lc------\(serverString)-----roomtype----\(CurrentUser.shareInstance.currentRoomType)")
            var secureConfig = true;
            
            if serverString.has("ws://") {
              secureConfig = false;
            }
            self?.socketIO = SocketIOClient.init(socketURL: url, config: [.log(false), .voipEnabled(false),.path("/werewolf"), .forceWebsockets(true), .connectParams(["room_id" : roomid, "room_type":CurrentUser.shareInstance.currentRoomType]), .extraHeaders(RequestManager.header), .secure(secureConfig)])
            self?.observer()
            self?.socketIO?.reconnectWait = 3;
            self?.socketIO?.connect()
            
            if let strongSelf = self {
              //开始心跳检测
              self?.timer = Timer.scheduledTimer(timeInterval: 15.0, target: strongSelf, selector: #selector(MessageManager.timerAction), userInfo: nil, repeats: true)
            }
            
          } else {
            Print.dlog(NSLocalizedString(" 未获取到连接地址", comment: ""));
            XBHHUD.showError(NSLocalizedString("错误:未获取到连接地址。请稍后再试", comment: ""));
            self?.resetRoomInfo()
          }
        }, error: { [weak self] (code, message) in
          Print.dlog(NSLocalizedString(" 错误的socketIO URL ", comment: ""))
          if code == 1009 {
            self?.canNotPlayNotice(message: message)
          }else{
            let localError = NSLocalizedString("错误:", comment: "")
            XBHHUD.showError("\(localError)\(message)");
            self?.resetRoomInfo()
          }
      });
    }
  }
  
  func resetRoomInfo() {
    CurrentUser.shareInstance.currentRoomID = ""
    CurrentUser.shareInstance.realCurrentRoomID = ""
    CurrentUser.shareInstance.from = ""
    CurrentUser.shareInstance.inRoomStatus = .none
    CurrentUser.shareInstance.loadingGame = false
  }
  
  func didReceiveConnectInfo(notification: Notification) {
    let jsonData = notification.object as! JSON
    
    if LocalSaveVar.connectID == "" {
      LocalSaveVar.connectID = jsonData["connect_id"].stringValue
    }
    
    if reconnecting == true {
//      sendMessage(type: .reconnect, payLoad: ["connect_id": LocalSaveVar.connectID])
      reconnecting = false
    } else {
      NotificationCenter.default.post(name: Notification.Name.init(GameMessageType.socketDidOpen.rawValue), object: nil, userInfo: nil)
    }
  }
  
  func timerAction() {
    let sendData: [String: Any] = [
      "type": "ping",
      "msg_id":Utils.getCurrentTimeStamp()
    ]
    let messageString = Utils.dicToJSONStr(sendData as NSDictionary)
    socketIO?.emit("message", messageString ?? "")
  }
  
  func close() {
    timer.invalidate()
    socketIO?.removeAllHandlers();
    socketIO?.disconnect()
    socketIO = nil;
    LocalSaveVar.connectID = ""
    XBHHUD.hide()
    reconnecting = false
  }
  
  // 监听事件
  internal func observer() {
    // 初始化成功
    socketIO?.on("connect", callback: { [weak self] (data, ack) in
      Print.dlog("SocketIO Connected")
      self?.reconnectTime = 0
    })
    
    // 连接失败
    socketIO?.on("error", callback: {(data, ack) in
      Print.dlog("SocketIO error: \(data)")
    })
    
    // 重连
    socketIO?.on("reconnect", callback: { [weak self] (data, ack) in
      Print.dlog("SocketIO reconnect:\(data)")
      self?.reconnecting = true
      XBHHUD.showLoading(NSLocalizedString("网络异常，正在重连...", comment: ""));
    })
    
    socketIO?.on("reconnectAttempt", callback: { [weak self] (data, ack) in
      Print.dlog("SocketIO reconnectAttempt:\(data)")
      self?.reconnectTime += 1
    })
    
    socketIO?.on("disconnect", callback: { (data, ack) in
      Print.dlog("SocketIO disconnect:\(data)")
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: SOCKET_DISSCONNECT_FROM_SRVER_NOTIFICATION), object:JSON.init(rawValue: 9999));
    })

    socketIO?.on("response", callback: { [weak self] (data, emit:SocketAckEmitter) in
      let jsonString = data[0] as? String ?? ""
      let json = JSON.init(parseJSON: jsonString)
//      self?.didReceivemessage(message: json)
    })
    
    socketIO?.on("message", callback: { [weak self] (data, ack) in
      let jsonString = data[0] as? String ?? ""
      let json = JSON.init(parseJSON: jsonString)
//      self?.didReceivemessage(message: json)
    })
  }
  
  // 发送消息
  func sendMessage(type: GameMessageType, payLoad: [String: Any] = [String: Any]()) {

    let sendData: [String: Any] = [
      "type": type.rawValue,
      "payload":payLoad
    ]
    let messageString = Utils.dicToJSONStr(sendData as NSDictionary)
    #if DEBUG
//        DDLogError(messageString!)
    #endif

    Print.dlog("\n\n\n *********** send message *************************************************** \n\(messageString ?? "") \n *********** did send message **************************************************** \n\n ")

    socketIO?.emit("message", messageString ?? "")
  }

  // 收到消息
  fileprivate func didReceivemessage(message: JSON) {
    Print.dlog("\n\n\n *********** did receive message *************************************************** \n\(message.debugDescription) \n *********** did receive message **************************************************** \n\n ")
      #if DEBUG
//          logView.addConversation(type: .receive, message: message.debugDescription)
//             DDLogError(message.debugDescription)
      #endif

    // 重连是否成功的结果直接抛到外面
    if message["type"].stringValue == "reconnect" {
      XBHHUD.hide()
      NotificationCenter.default.post(name: Notification.Name.init(message["type"].stringValue), object: message["code"])

    } else {
      if message["code"].intValue != 1000 && !message["message"].stringValue.isEmpty {
        if message["type"].stringValue != "leave" {
          XBHHUD.showError(message["message"].stringValue)
        }
        //进入房间产生错误，重制房间信息
        if message["type"].stringValue == "enter" {
          self.resetRoomInfo()
        }
        return
      }
      if CurrentUser.shareInstance.inRoomStatus == .none{
        //断线重连情况特殊处理，需要把restore_roomd的信息缓冲
        if message["type"].stringValue == "restore_room"{
            CurrentUser.shareInstance.cachedMessages.append([message["type"].stringValue:message["payload"]])
        }else{
         NotificationCenter.default.post(name: Notification.Name.init(message["type"].stringValue), object: message["payload"])
        }
      }else{
        if CurrentUser.shareInstance.inRoomStatus == .inRoom {
          NotificationCenter.default.post(name: Notification.Name.init(message["type"].stringValue), object: message["payload"])
        }else{
          //如果没尽到房间就缓存所有的消息
          CurrentUser.shareInstance.cachedMessages.append([message["type"].stringValue:message["payload"]])
        }
      }
    }
  }
//  func sendMessage(type: GameMessageType, payLoad: [String: Any] = [String: Any]()) {
//    
//    let sendData: [String: Any] = [
//      "type": type.rawValue,
//      "payload":payLoad
//    ]
//    let messageString = Utils.dicToJSONStr(sendData as NSDictionary)
//    
//    
//    Print.dlog("\n\n\n *********** send message *************************************************** \n\(messageString ?? "") \n *********** did send message **************************************************** \n\n ")
//    
//    socketIO?.emit("message", messageString ?? "")
//  }
//  
//  // 收到消息
//  fileprivate func didReceivemessage(message: JSON) {
//    Print.dlog("\n\n\n *********** did receive message *************************************************** \n\(message.debugDescription) \n *********** did receive message **************************************************** \n\n ")
//      #if DEBUG
//          logView.addConversation(type: .receive, message: message.debugDescription)
//      #endif
//    // 重连是否成功的结果直接抛到外面
//    if message["type"].stringValue == "reconnect" {
//      XBHHUD.hide()
//      NotificationCenter.default.post(name: Notification.Name.init(message["type"].stringValue), object: message["code"])
//      
//    } else {
//      if message["code"].intValue != 1000 && !message["message"].stringValue.isEmpty {
//        if message["type"].stringValue != "leave" {
//          XBHHUD.showError(message["message"].stringValue)
//        }
//        return
//      }
//      if CurrentUser.shareInstance.inRoomStatus == .none{
//         NotificationCenter.default.post(name: Notification.Name.init(message["type"].stringValue), object: message["payload"])
//      }else{
//        if CurrentUser.shareInstance.inRoomStatus == .inRoom {
//          NotificationCenter.default.post(name: Notification.Name.init(message["type"].stringValue), object: message["payload"])
//        }else{
//          //如果没尽到房间就缓存所有的消息
//          CurrentUser.shareInstance.cachedMessages.append([message["type"].stringValue:message["payload"]])
//        }
//      }
//    }
//  }
  
  // 提供给外部掉用的监听方法
  func onMessageReceived(messageType: GameMessageType, delegate: Any, selector: Selector) {
    NotificationCenter.default.addObserver(delegate, selector: selector, name: messageType.notificationName, object: nil)
  }
}
