//
//  SocketManager.swift
//  game_werewolf
//
//  Created by Tony on 2017/9/30.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import SocketIO

class SocketManager {
  
  var socketIO: SocketIOClient?
  var currentRetryTime = 1
  var reconnecting = false {
    willSet{
      if newValue == true && reconnecting == false {
        //第一次从false变为true，发一个通知,正在重连
        NotificationCenter.default.post(name:NSNotification.Name(rawValue:MICRO_GAME_DISCONNECT), object: nil, userInfo:nil)
      }
      if newValue == false && reconnecting == true {
          //第一次从false变为true，发一个通知,正在重连
      NotificationCenter.default.post(name:NSNotification.Name(rawValue:MICRO_GAME_CONNECT), object: nil, userInfo:nil)
      }
    }
  }   // 是否正在重连
  var time: Timer?
  // 在还没有连接上的时候，尝试重连一次，当连接上了以后，这边需要把状态进行修改。
  static let MAX_RETRY_TIMES = 18;
  
  var reconnectTime = 0 {
    didSet {
      Print.dlog("reconnect count \(reconnectTime)")
      if reconnectTime >= currentRetryTime {
        self.close();
        NotificationCenter.default.post(name: Notification.Name.init(GameMessageType.reconnect.rawValue), object: JSON.init(1001))
        XBHHUD.showError(NSLocalizedString("连接失败，请稍后再试", comment: ""));
        self.reconnectTime = 0;
        if SwiftConverter.shareInstance.isGaming() == false{
          resetRoomInfo()
        }
      }
    }
  }
  
  func close() -> Void {
    time?.fireDate = Date.distantFuture
    socketIO?.removeAllHandlers()
    socketIO?.disconnect()
    socketIO = nil
    reconnecting = false
  }
  
  func resetRoomInfo() {
    CurrentUser.shareInstance.currentRoomID = ""
    CurrentUser.shareInstance.realCurrentRoomID = ""
    CurrentUser.shareInstance.from = ""
    CurrentUser.shareInstance.inRoomStatus = .none
    CurrentUser.shareInstance.loadingGame = false
  }
  
  @objc func timerAction() {
    let sendData: [String: Any] = [
      "type": "ping",
      "msg_id":Utils.getCurrentTimeStamp()
    ]
    let messageString = Utils.dicToJSONStr(sendData as NSDictionary)
    socketIO?.emit("message", messageString ?? "")
  }
  
  // 监听事件
  func connectSocketIO(url: URL, roomid: String, secureConfig: Bool) -> Void {
    self.socketIO = SocketIOClient.init(socketURL: url,config: [.log(false), .voipEnabled(false), .path("/werewolf"), .forceWebsockets(true), .connectParams(["room_id" : roomid, "room_type":CurrentUser.shareInstance.currentRoomType]), .extraHeaders(RequestManager.header), .secure(secureConfig)])
    if let _ = time {
      time?.fireDate = Date.init()
    } else {
      time = Timer.scheduledTimer(timeInterval: 15.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
      time?.fire()
    }
    self.observer()
    self.socketIO?.reconnectWait = 3;
    self.socketIO?.connect()
  }

  
   func observer() {
    // 初始化成功
    socketIO?.on("connect", callback: { [weak self] (data, ack) in
      Print.dlog("SocketIO Connected")
      self?.reconnectTime = 0
      self?.currentRetryTime = SocketManager.MAX_RETRY_TIMES;
    })
    
    // 连接失败
    socketIO?.on("error", callback: {(data, ack) in
      Print.dlog("SocketIO error: \(data)")
    })
    
    // 重连
    socketIO?.on("reconnect", callback: { [weak self] (data, ack) in
      Print.dlog("SocketIO reconnect:\(data)")
      self?.reconnecting = true
      XBHHUD.showLoading(NSLocalizedString("网络异常，正在重连...", comment: ""));
      XBHHUD.hidenSuccess = {
        self?.close()
        NotificationCenter.default.post(name: Notification.Name.init(GameMessageType.reconnect.rawValue), object: JSON.init(1001))
        self?.reconnectTime = 0
      }
    })
    
    socketIO?.on("reconnectAttempt", callback: { [weak self] (data, ack) in
      Print.dlog("SocketIO reconnectAttempt:\(data)")
      self?.reconnectTime += 1
    })
    
    socketIO?.on("disconnect", callback: { (data, ack) in
      Print.dlog("SocketIO disconnect:\(data)")
      self.currentRetryTime = 1
    })
    
    socketIO?.on("response", callback: { (data, emit:SocketAckEmitter) in
      let jsonString = data[0] as? String ?? ""
      let json = JSON.init(parseJSON: jsonString)
      messageDispatchManager.didReceivemessage(message: json)
    })
    
    socketIO?.on("message", callback: { (data, ack) in
      let jsonString = data[0] as? String ?? ""
      let json = JSON.init(parseJSON: jsonString)
      //获取extraData
      if data.count > 1 {
        let extraDataString = data[1] as? String ?? ""
        if extraDataString.isEmpty == false {
          messageDispatchManager.microExtraData = extraDataString
        }
      }
      messageDispatchManager.didReceivemessage(message: json)
    })
  }

}
