//
//  MessageDispatchManager.swift
//  game_werewolf
//
//  Created by Tony on 2017/9/30.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

let messageDispatchManager = MessageDispatchManager()

class MessageDispatchManager:NSObject {
  
  let MAX_RETRY_TIMES = 18;
  var socketManager: SocketManager = SocketManager();
  var controlManager: ControlSocketManager = ControlSocketManager();
  var viewController: UIViewController?
  // 每次进房间的数据
  internal var password  = ""
  internal var type      = ""
  internal var roomId    = ""
  internal var userName  = ""
  internal var userId    = ""
  internal var userImage = ""
  internal var token     = ""

  internal var appRunning = false
  internal var remoteInfo:[String:Any]?;
  
  var reconnecting:Bool = false;
  var serverTime:TimeInterval?
  var microExtraData:String = ""
  fileprivate override init() {
    // 监听connect_info消息
//    onMessageReceived(messageType: .connect_info, delegate: self, selector: #selector(MessageDispatchManager.didReceiveConnectInfo(notification:)))
    super.init()
//    NotificationCenter.default.addObserver(self, selector: #selector(onNotificication(notification:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil);//程序即将进入前台
//    NotificationCenter.default.addObserver(self, selector: #selector(onNotificication(notification:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil);
  }
  // 打开
  func open(roomid:String,type:String) {
    // getServer(roomid: roomid, canNotPlay: false);
    //判断是否可以玩游戏
    //如果是lobby类型，说明是常连socket，不用判断禁止玩，直接连接
    if type == "lobby" {
       getLobbyServer()
       return
    }
    var canNotPlay = false
    if  UserDefaults.standard.object(forKey: "NOPLAY") != nil {
      let dics = UserDefaults.standard.object(forKey: "NOPLAY") as! NSMutableDictionary
      if dics[CurrentUser.shareInstance.id] != nil {
        let dic = dics[CurrentUser.shareInstance.id] as! NSMutableDictionary
        let noplay = dic["end"] as! String
        if Int(noplay) == -1 {
          //说明被永久封了
          canNotPlay = true
          getServer(roomid: roomid, canNotPlay: canNotPlay)
        }else{
          //不是永久封
          let givenEnd = dic["end"] as! String    //后台指定的禁玩结束时间
          //直接以当前时间为准
          if Double(givenEnd)! - Double(Utils.getCurrentTimeStamp()) > 0 {
            //指定的结束时间大于现在的时间
            canNotPlay = true
            getServer(roomid: roomid, canNotPlay: canNotPlay)
          }else{
            //这个时候清空一下数据
            //              couldIPlayThisGame(roomid: roomid)
            getServer(roomid: roomid, canNotPlay: false)
          }
        }
      }else{
        //有封禁记录的不是现在的账户
        //       couldIPlayThisGame(roomid: roomid)
        getServer(roomid: roomid, canNotPlay: false)
      }
    }else{
      //在当前设备上没有被封记录，请求查看自己是否可以玩游戏
      //      couldIPlayThisGame(roomid: roomid)
      getServer(roomid: roomid, canNotPlay: false)
    }
  }
  
  func couldIPlayThisGame(roomid:String) {
    //说明可以了，直接请求
    //是不是超过30分钟
    if Date().timeIntervalSince1970 - (CurrentUser.shareInstance.playble.nextrequestTime) > 0 {
      //超过下一次的请求时间
      playableCheck(roomid:roomid)
    }else{
      getServer(roomid: roomid, canNotPlay: CurrentUser.shareInstance.playble.canNotPlay)
    }
  }
  
  func playableCheck(roomid:String) {
    RequestManager().get(url: (RequestUrls.canplay + "?access_token=\(CurrentUser.shareInstance.token)"), parameters: ["access_token":CurrentUser.shareInstance.token], success: {[weak self] (json) in
      let nextTime = Date().timeIntervalSince1970 + 1800
      let canNotPlay = json["isNoPlay"].boolValue
      let playable = Playable(nextrequestTime: nextTime, canNotPlay: canNotPlay)
      CurrentUser.shareInstance.playble = playable
      self?.getServer(roomid: roomid, canNotPlay: canNotPlay)
    }) {[weak self] (code, message) in
      print("\(message)")
      XBHHUD.hide()
      //返回错误，就使用上一次的缓存
      self?.getServer(roomid: roomid, canNotPlay: CurrentUser.shareInstance.playble.canNotPlay)
    }
  }
  
  func canNotPlayNotice(message:String) {
    DispatchQueue.main.async { [weak self] in
      XBHHUD.hide()
      let notice = Bundle.main.loadNibNamed("canNotPlayNotice", owner: nil, options: nil)?.last as! canNotPlayNotice
      notice.backgroundColor = UIColor.hexColor("E3DDEA")
      notice.textview.text = message
      notice.textview.backgroundColor = UIColor.hexColor("E3DDEA")
      Utils.showAlertView(view: notice)
      //清除房间信息
      self?.resetRoomInfo()
    }
  }
  
  func getLobbyServer() {
    let lc = UserDefaults.standard.object(forKey: "LOCATION") == nil ? "" :  UserDefaults.standard.object(forKey: "LOCATION") as! String
    let para = ["room_id":"","user_id":CurrentUser.shareInstance.id,"lc":lc,"level":"lobby"]
    let url = RequestUrls.getSocketServerHost
    RequestManager().post(
      url: url,
      parameters: para,
      success: { [weak self] (response) in
        if let serverString = response["server"].string,
          let url = URL.init(string: serverString) {
          print("----------\(url)------lc------\(serverString)-----roomtype----lobby")
          var secureConfig = true;
          if serverString.has("ws://") {
            secureConfig = false;
          }
          if CurrentUser.shareInstance.isControlSocketServerGet == false  {
             self?.controlManager.connectSocketIO(url: url, roomid: "", secureConfig: secureConfig)
           }
          }
      }, error: { [weak self] (code, message) in
        Print.dlog(NSLocalizedString(" 错误的socketIO URL ", comment: ""))
        if code == 1009 {
          self?.canNotPlayNotice(message: message)
        }
    });
  }
  
  func getServer(roomid:String,canNotPlay:Bool) {
    if canNotPlay == true {
      let message = NSLocalizedString("尊敬的用户您好： 因用户多次举报您违反快玩小游戏吧社区友好愉快玩耍规定，现快玩小游戏吧暂时禁玩您的账号，如有疑问请联系suppoet@orangelab.cn", comment: "")
      canNotPlayNotice(message:message)
      return
    }else{
      let lc = UserDefaults.standard.object(forKey: "LOCATION") == nil ? "" :  UserDefaults.standard.object(forKey: "LOCATION") as! String
      var para = ["room_id":roomid,"user_id":CurrentUser.shareInstance.id,"lc":lc,"level":CurrentUser.shareInstance.currentRoomType]
      if roomid == "-1" {
        para = ["room_id":"","user_id":CurrentUser.shareInstance.id,"lc":lc,"level":CurrentUser.shareInstance.currentRoomType]
      }
      var url = RequestUrls.getSocketServerHost
      if CurrentUser.shareInstance.currentRoomType == "audio" {
        url = RequestUrls.getAudioSocketServerGet
      }
      RequestManager().post(
        url: url,
        parameters: para,
        success: { [weak self] (response) in
          if let serverString = response["server"].string,
            let url = URL.init(string: serverString) {
            
            print("----rommid\(roomid)------\(url)------lc------\(serverString)-----roomtype----\(CurrentUser.shareInstance.currentRoomType)")
            var secureConfig = true;
            
            if serverString.has("ws://") {
              secureConfig = false;
            }
            if let serverGetLevel = response["type"].string {
              CurrentUser.shareInstance.currentRoomType = serverGetLevel
            }
            //在这里获取小游戏的信息
            if let microGame = response["mini_game"].dictionary,
              let _ = microGame["icon"]?.string,
              let _ = microGame["url"]?.string,
              let _ = microGame["name"]?.string{
              CurrentUser.shareInstance.microGame = microGame
            }
            if CurrentUser.shareInstance.currentRoomType.contains("game") && CurrentUser.shareInstance.microGame != nil {
              //小游戏，先加载游戏界面
              if let controller = self?.viewController as? MicroGameViewController {
                controller.socketConnect = ["url":url,"roomid":roomid,"secureConfig":secureConfig]
                controller.gameInfo = response
                // extraData
                if let extraData = response["extraData"].string {
                  self?.microExtraData = extraData
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                  Utils.getCurrentVC(vc: (UIApplication.shared.keyWindow?.rootViewController)!)?.present(controller, animated: true, completion: {
                      XBHHUD.hide()
                  })
                })
              }
            }else{
              //不是小游戏，先连接服务器
              self?.socketManager.connectSocketIO(url: url, roomid: roomid, secureConfig: secureConfig)
            }
          } else {
            Print.dlog(NSLocalizedString(" 未获取到连接地址", comment: ""));
            XBHHUD.showError(NSLocalizedString("错误:未获取到连接地址。请稍后再试", comment: ""));
            self?.resetRoomInfo()
          }
          
        }, error: { [weak self] (code, message) in
          Print.dlog(NSLocalizedString(" 错误的socketIO URL ", comment: ""))
          if code == 1009 {
            self?.canNotPlayNotice(message: message)
          }else{
            let localError = NSLocalizedString("错误:", comment: "")
            XBHHUD.showError("\(localError)\(message)");
            self?.resetRoomInfo()
          }
      });
    }
  }
  
  func close() {
    LocalSaveVar.connectID = ""
    XBHHUD.hide()
    socketManager.close()
    viewController = nil;
  }
  
  func resetRoomInfo() {
    CurrentUser.shareInstance.currentRoomID = ""
    CurrentUser.shareInstance.realCurrentRoomID = ""
    CurrentUser.shareInstance.from = ""
    CurrentUser.shareInstance.inRoomStatus = .none
    CurrentUser.shareInstance.loadingGame = false
  }
  
  func sendMicroGameMessage(type:String, payLoad: [String: Any] = [String: Any]()) {
    let sendData: [String: Any] = [
      "type": type,
      "payload":payLoad
    ]
    let messageString = Utils.dicToJSONStr(sendData as NSDictionary)
    if microExtraData.isEmpty == false {
      //不为空，就传过去
      Print.dlog("\n\n\n *********** send message *************************************************** \n\(messageString ?? "") \n *********** did send message **************************************************** \n\n ")
      socketManager.socketIO?.emit("message", messageString ?? "",microExtraData)
    }else{
      Print.dlog("\n\n\n *********** send message *************************************************** \n\(messageString ?? "") \n *********** did send message **************************************************** \n\n ")
      socketManager.socketIO?.emit("message", messageString ?? "")
    }
  }
  
  // 发送消息
  func sendMessage(type: GameMessageType, payLoad: [String: Any] = [String: Any]()) {
    
    let sendData: [String: Any] = [
      "type": type.rawValue,
      "payload":payLoad
    ]
    let messageString = Utils.dicToJSONStr(sendData as NSDictionary)
    Print.dlog("\n\n\n *********** send message *************************************************** \n\(messageString ?? "") \n *********** did send message **************************************************** \n\n ")
    socketManager.socketIO?.emit("message", messageString ?? "")
  }
  

  // 收到消息
  func didReceivemessage(message: JSON) {
    Print.dlog("\n\n\n *********** did receive message *************************************************** \n\(message.debugDescription) \n *********** did receive message **************************************************** \n\n ")
    #if DEBUG
      //      logView.addConversation(type: .receive, message: message.debugDescription)
    #endif
    // 重连是否成功的结果直接抛到外面
    if message["type"].stringValue == "reconnect" {
      XBHHUD.hide()
      NotificationCenter.default.post(name: Notification.Name.init(message["type"].stringValue), object: message["code"])
      
    } else {
      if message["code"].intValue != 1000 && !message["message"].stringValue.isEmpty {
        if message["type"].stringValue != "leave" {
          XBHHUD.showError(message["message"].stringValue)
        }
        //进入房间产生错误，重制房间信息
        if message["type"].stringValue == "enter" {
          self.resetRoomInfo()
          if self.type.contains("game") {
            //如果是小游戏的话，就直接退出
            if let microGame = self.viewController as? MicroGameViewController {
              let outGame = Selector("action_enter_error")
              if microGame.responds(to: outGame) {
                  microGame.action_enter_error()
              }
            }
          }
        }
        return
      }
      print(message)
      if let actionName = message["type"].string,
        let controller = self.viewController {
        let selector = Selector("action_\(actionName):");
        print("执行方法:\(selector), type: \(type); 数据: \(message)")
        if self.type == "audio" {
          if let tmpRoot = controller as? UINavigationController,
            let chatVoiceController = tmpRoot.viewControllers.first as? ChatVoiceRoomViewController {
            if chatVoiceController.responds(to: selector) {
              chatVoiceController.perform(selector, with: message["payload"].dictionaryObject);
            } else {
              print("controller 的 \(selector) 方法没有");
            }
          } else {
            print("controller 的 类型不对");
          }
        } else if self.type.contains("game"){
          //小游戏
          if let microGame = controller as? MicroGameViewController {
                microGame.runDispatch(message)
          } else {
            print("controller 的 类型不对");
          }
        }else{
          //狼人杀游戏
          if let gameController = controller as? GameRoomViewController {
            if gameController.responds(to: selector) {
               gameController.runDispatch(message)
            } else {
              print("controller 的 \(selector) 方法没有");
            }
          } else {
            print("controller 的 类型不对");
          }
        }
      } else {
        print("服务器没有传过来type 或者 controller 还为nil, 本条消息忽略. 服务器发回的消息:\(message.debugDescription)");
      }
    }
  }
  
  
  internal func setRoomInfo(password: String, type: String, roomId: String, userName: String, userId: String, userImage: String, token: String) {
    self.password  = password;
    self.type      = type;
    self.roomId    = roomId;
    self.userName  = userName;
    self.userId    = userId;
    self.userImage = userImage;
    self.token     = token;
    self.viewController = nil
    // 初始化type
    if type == "audio" {
      self.viewController =  UINavigationController.init(rootViewController: ChatVoiceRoomViewController());
    } else if type.contains("game") {
      //小游戏
      self.viewController = UIViewController.loadFromStoryBoard("MicroGame", identifier: "MicroGame") as! MicroGameViewController;
    } else if type == "lobby" {
        //这个是全局socket，不用展示UI
    } else {
      self.viewController = UIViewController.loadFromStoryBoard("Main", identifier: "GameRoomViewController") as! GameRoomViewController;
    }
    CurrentUser.shareInstance.id                  = userId;
    CurrentUser.shareInstance.name                = userName;
    CurrentUser.shareInstance.avatar              = userImage;
    CurrentUser.shareInstance.currentRoomType     = type == "lobby" ? "" : type
    CurrentUser.shareInstance.currentRoomID       = roomId
    CurrentUser.shareInstance.currentRoomPassword = password
    CurrentUser.shareInstance.token               = token
    RequestManager.appendHeader(key: "x-access-token", value: token)
    //添加did
    RequestManager.appendHeader(key: "did", value:Utils.getUUID() ?? "")
    CurrentUser.shareInstance.loadingGame = false
  }
  
  // 提供给外部掉用的监听方法
  func onMessageReceived(messageType: GameMessageType, delegate: Any, selector: Selector) {
    NotificationCenter.default.addObserver(delegate, selector: selector, name: messageType.notificationName, object: nil)
  }
  
  func isReconnecting() -> Bool {
    return socketManager.reconnecting;
  }
  
  func setReconnectingStatus(_ status:Bool) {
    socketManager.reconnecting = status;
  }

}
