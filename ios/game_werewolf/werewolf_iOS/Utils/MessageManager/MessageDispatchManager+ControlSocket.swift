//
//  MessageDispatchManager+ControlSocket.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/30.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import Foundation
//全局socket连接

protocol ConrolSocketDelegate {
  // 建立连接的第一条消息
  func action_connect_info(_ rawData: [String:Any]);
  // 建立连接成功后，给服务器发送enter消息，服务器回复的enter消息
  func action_enter(_ rawData: [String:Any]);
  //有玩家离开
  func action_leave(_ rawData: [String:Any])
  //系统通知
  func action_notify_server(_ rawData: [String:Any])
}


enum ControlMessage:String {
  case connect_info
  case enter
  case reconnect
  case play_mini_game
}

extension MessageDispatchManager {
  
  func isControlReconnecting() -> Bool {
    return controlManager.reconnecting;
  }
  
  func setControlReconnectingStatus(_ status:Bool) {
    controlManager.reconnecting = status;
  }
  
  // 收到全局消息
  func didReceiveControlmessage(message: JSON) {
    Print.dlog("\n\n\n *********** Controlmessage did receive message *************************************************** \n\(message.debugDescription) \n *********** Controlmessage did receive message **************************************************** \n\n ")
    // 重连是否成功的结果直接抛到外面
    if message["type"].stringValue == "reconnect" {
      XBHHUD.hide()
      NotificationCenter.default.post(name: Notification.Name.init(message["type"].stringValue), object: message["code"])
    } else {
      if message["code"].intValue != 1000 && !message["message"].stringValue.isEmpty {
        if message["type"].stringValue != "leave" {
          XBHHUD.showError(message["message"].stringValue)
        }
        //进入房间产生错误，重制房间信息
        if message["type"].stringValue == "enter" {
           XBHHUD.showError(message["message"].stringValue)
        }
        return
      }
      print(message)
      //计算本地与服务器时差
      if messageDispatchManager.serverTime == nil {
        //计算矫正时间
        if let server = message["msg_time"].int64 {
          messageDispatchManager.serverTime = TimeInterval(Utils.getCurrentTimeStamp() - server)
        }
      }
      self.runDispatch(message)
    }
  }
  
  func sendControlMessage(type:String, payLoad: [String: Any] = [String: Any]()) {
    let sendData: [String: Any] = [
      "type": type,
      "payload":payLoad
    ]
    let messageString = Utils.dicToJSONStr(sendData as NSDictionary)
    Print.dlog("\n\n\n *********** Control message *************************************************** \n\(messageString ?? "") \n *********** did send Control message **************************************************** \n\n ")
    controlManager.socketIO?.emit("message", messageString ?? "")
  }
  
  func runDispatch(_ rawData:JSON) {
    let jsonData = rawData
    if let actionName = jsonData["type"].string {
      //消息转发
      let selector = Selector("action_\(actionName):")
      if self.responds(to: selector) {
        self.perform(selector, with: jsonData["payload"].dictionaryObject)
      }
    }
  }
  
  @objc func action_connect_info(_ rawData: [String:Any]) {
    
    let data = JSON.init(rawData)
    
    if LocalSaveVar.connectID == "" {
      LocalSaveVar.controlconnectID = data["connect_id"].stringValue
    }
//    if(messageDispatchManager.isControlReconnecting()) {
//      messageDispatchManager.sendControlMessage(type:ControlMessage.reconnect.rawValue, payLoad: ["connect_id": LocalSaveVar.controlconnectID]);
//      messageDispatchManager.setControlReconnectingStatus(false)
//    }
    messageDispatchManager.sendControlMessage(type:ControlMessage.enter.rawValue, payLoad: [
      "room_id":  "",
      "password": "",
      "type"    : "lobby",
      "token": CurrentUser.shareInstance.token,
      "extra" : [
        "pt": "\(Config.platform)",
        "v": "\(Config.buildVersion)",
        "b": "\(Config.appStoreVersion)",
        "sv": "\(Config.serverVersion)",
        "tz": "\(Config.currentTimeZoneGMT)",
        "lg": "\(Config.systemLanage)"
      ],
      "user": [
        "id": CurrentUser.shareInstance.id,
        "name": CurrentUser.shareInstance.name,
        "avatar": CurrentUser.shareInstance.avatar,
        "level": CurrentUser.shareInstance.level,
        "experience": CurrentUser.shareInstance.experience
      ]
      ])
  }
  //连接socket成功
  @objc func action_enter_server(_ rawData: [String : Any]) {
    let data = JSON.init(rawData)
  }
  
  func action_play_mini_game_server(_ rawData: [String : Any]) {
     let data = JSON.init(rawData)
     if let control = data["type"].string {
      print("全局socket==========" + "\(control)")
      switch control {
        case "invite_is_vaild":
          XBHHUD.showError(NSLocalizedString("邀请已经失效", comment: ""))
        case "join_room":
          //双方同意，开始进入游戏
          //先进入展示界面
          join_room(data["data"])
        case "invited_success":
          XBHHUD.showTextOnly(text: NSLocalizedString("邀请发送成功", comment: ""))
        case "invite_be_refused":
            XBHHUD.showTextOnly(text:NSLocalizedString("邀请被拒绝", comment: ""))
      default:
        print("/(control)" + "没有被实现")
      }
     }
  }
  
  //系统消息
  func action_notify_server(_ rawData: [String : Any]) {
      let data = JSON.init(rawData)
      if let notify = data["type"].string {
        switch notify {
        case "change_server":
          XBHHUD.showError("游戏已经失效")
          change_server(data)
        default:
          print("/(control)" + "没有被实现")
        }
      }
  }
  
  func change_server(_ data:JSON) {
    //断开连接
    controlManager.close()
    if let server =  data["data"]["server"].string,
       let url = URL.init(string: server){
      let extraData = data["data"]["extraData"].string ?? ""
//      var level = data["data"]["level"].string ?? ""
//      var type = data["data"]["type"].string ?? ""
      self.microExtraData = extraData
      var secureConfig = true;
      if server.has("ws://") {
        secureConfig = false;
      }
      controlManager.changeConnectSocketIO(url: url, roomid: "", secureConfig: secureConfig)
    }
  }
  
  func join_room(_ json:JSON) {
    //先看看是不是有东西挡住
    //这个时候检查有没有送礼物和游戏列表
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "giftviewout"), object: nil)
    if SwiftConverter.shareInstance.isGaming() {
      //说明有别的游戏存在,直接返回
      return
    }
    if Utils.getCurrentVC(vc: (UIApplication.shared.keyWindow?.rootViewController)!) is MicroGameViewController {
      XBHHUD.showError(NSLocalizedString("必须在聊天界面才能进游戏", comment: ""))
      return
    }
    if let room_id = json["room_id"].string,
      let type = json["type"].string {
        var password = json["password"].string ?? ""
        messageDispatchManager.setRoomInfo(password: password, type: type, roomId: room_id, userName:CurrentUser.shareInstance.name, userId: CurrentUser.shareInstance.id, userImage: CurrentUser.shareInstance.avatar, token: CurrentUser.shareInstance.token)
    }
    if let microGame = json["mini_game"].dictionary,
      let _ = microGame["icon"]?.string,
      let _ = microGame["url"]?.string,
      let _ = microGame["name"]?.string{
      CurrentUser.shareInstance.microGame = microGame
    }
    if let controller = self.viewController as? MicroGameViewController,
      let serverString = json["server"].string,
      let roomid = json["room_id"].string,
      let url = URL.init(string: serverString)
      {
      var secureConfig = true;
      if serverString.has("ws://") {
          secureConfig = false;
      }
      if let extraData = json["extraData"].string {
          self.microExtraData = extraData
      }
      if let url = json["mini_game"]["url"].string {
        controller.gameurl = url
      }
      controller.isPVP = true
       if let oppoInfo = json["oppo"].dictionary {
          controller.oppoInfo = oppoInfo
       }
      controller.socketConnect = ["url":url,"roomid":roomid,"secureConfig":secureConfig]
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
        Utils.getCurrentVC(vc: (UIApplication.shared.keyWindow?.rootViewController)!)?.present(controller, animated: true, completion: {
            XBHHUD.hide()
        })
      })
    }
  }
}
