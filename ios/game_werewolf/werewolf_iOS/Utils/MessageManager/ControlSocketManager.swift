//
//  ControlSocketManager.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/30.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import Foundation
import SocketIO

class ControlSocketManager {
  var socketIO: SocketIOClient?
  var currentRetryTime = 1
  var reconnecting = false    // 是否正在重连
  var timer:Timer?
  var reconnectTime = 0 {
    didSet {
      Print.dlog("reconnect count \(reconnectTime)")
      if reconnectTime > 18 {
          self.socketIO?.reconnectWait = 6
      }else if reconnectTime > 18 && reconnectTime < 100 {
          self.socketIO?.reconnectWait = 12
      }else if reconnectTime >= 100 {
         self.socketIO?.reconnectWait = 15
      }
    }
  }
  
  func close() -> Void {
    socketIO?.removeAllHandlers()
    socketIO?.disconnect()
    //    socketIO = nil
    reconnecting = false
  }
  
  func logOut() {
    socketIO?.removeAllHandlers()
    socketIO?.disconnect()
    socketIO = nil
    reconnecting = false
    CurrentUser.shareInstance.isControlSocketServerGet = false
  }
  
  func resetRoomInfo() {
    //不需要重置
  }
  
  @objc func timerAction() {
    let sendData: [String: Any] = [
      "type": "ping",
      "msg_id":Utils.getCurrentTimeStamp()
    ]
    let messageString = Utils.dicToJSONStr(sendData as NSDictionary)
    socketIO?.emit("message", messageString ?? "")
  }
  
  // 监听事件
  func connectSocketIO(url: URL, roomid: String, secureConfig: Bool) -> Void {
    if self.socketIO?.status == .connecting || self.socketIO?.status == .connected || self.socketIO?.status == .disconnected {
      //如果正在连接，就不再处理了
      print("正在连接中，不在处理，谢谢")
      return
    }
    self.socketIO = SocketIOClient.init(socketURL: url,config: [.log(false), .voipEnabled(false), .path("/connector"), .forceWebsockets(true), .connectParams(["room_id" : roomid, "room_type":"lobby"]), .extraHeaders(RequestManager.header), .secure(secureConfig), .reconnectAttempts(-1)])
    self.observer()
    self.socketIO?.connect()
  }
  
  func changeConnectSocketIO(url: URL, roomid: String, secureConfig: Bool) -> Void {
    self.socketIO = SocketIOClient.init(socketURL: url,config: [.log(false), .voipEnabled(false), .path("/connector"), .forceWebsockets(true), .connectParams(["room_id" : roomid, "room_type":"lobby"]), .extraHeaders(RequestManager.header), .secure(secureConfig), .reconnectAttempts(-1)])
    self.observer()
    self.socketIO?.connect()
  }
  
  func observer() {
    // 初始化成功
    socketIO?.on("connect", callback: { [weak self] (data, ack) in
      Print.dlog("SocketIO Connected")
      //开始发ping
      //是不是连接成功了
      if CurrentUser.shareInstance.isControlSocketServerGet == false {
        CurrentUser.shareInstance.isControlSocketServerGet = true
      }
      if self?.timer == nil {
        self?.timer = Timer.init(timeInterval: 15, target: self, selector: #selector(self?.timerAction), userInfo: nil, repeats: true)
        RunLoop.current.add((self?.timer)!, forMode: .commonModes)
      }
      self?.reconnectTime = 0
      self?.socketIO?.reconnectWait = 3
    })
    
    // 连接失败
    socketIO?.on("error", callback: {(data, ack) in
      Print.dlog("SocketIO error: \(data)")
    })
    
    // 重连
    socketIO?.on("reconnect", callback: { [weak self] (data, ack) in
      Print.dlog("SocketIO reconnect:\(data)")
      self?.reconnecting = true
    })
    
    socketIO?.on("reconnectAttempt", callback: { [weak self] (data, ack) in
      Print.dlog("SocketIO reconnectAttempt:\(data)")
      self?.reconnectTime += 1
    })
    
    socketIO?.on("disconnect", callback: { (data, ack) in
      Print.dlog("SocketIO disconnect:\(data)")
      self.currentRetryTime = 1
    })
    
    socketIO?.on("response", callback: { (data, emit:SocketAckEmitter) in
      let jsonString = data[0] as? String ?? ""
      let json = JSON.init(parseJSON: jsonString)
      messageDispatchManager.didReceiveControlmessage(message: json)
    })
    
    socketIO?.on("message", callback: { (data, ack) in
      let jsonString = data[0] as? String ?? ""
      let json = JSON.init(parseJSON: jsonString)
      messageDispatchManager.didReceiveControlmessage(message: json)
    })
  }
}

