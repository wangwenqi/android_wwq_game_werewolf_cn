//
//  MicroGameMessage.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/24.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import Foundation

let MICRO_GAME_USER_LEAVE = "MICRO_GAME_USER_LEAVE"
let MICRO_GAME_USER_RETURN = "MICRO_GAME_USER_RETURN"
let MICRO_GAME_DISCONNECT = "MICRO_GAME_DISCONNECT"
let MICRO_GAME_CONNECT = "MICRO_GAME_CONNECT"
let MICRO_GAME_REPLAY = "MICRO_GAME_REPLAY"

protocol MicroGameDelegate {
  //进入游戏过程
  func action_connect_info(_ rawData: [String:Any]);
  //建立连接成功后，给服务器发送enter消息，服务器回复的enter消息
  func action_enter_server(_ rawData: [String:Any]);
  //加入游戏
  func action_join_server(_ rawData: [String:Any]);
  //离开游戏
  func action_leave_server(_ rawData: [String:Any]);
  //准备好
  func action_ready_server(_ rawData: [String:Any]);
  //开始游戏
  func action_start_game_server(_ rawData: [String:Any]);
  //游戏结束
  func action_game_over_server(_ rawData: [String:Any]);
}

protocol GameLinkFiveDelegate {
  //游戏移动
  func action_move_server(_ rawData: [String:Any]);
}

protocol MicroGameAudioDelegate {
  func action_audio_connect_server(_ rawData: [String:Any]);
}

