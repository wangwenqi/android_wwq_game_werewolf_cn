//
//  EnterChatRoomMessage.swift
//  game_werewolf
//
//  Created by xiaozao on 2018/1/19.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import Foundation

protocol EnterChatRoomMessage {

  func action_enter(_ rawData: [String:Any]);
  // 有新的玩家进入
  func action_join(_ rawData: [String:Any]);
  //有玩家离开
  func action_leave(_ rawData: [String:Any]);
  // 房主变更
  func action_update_master(_ rawData: [String:Any]);
  // 有玩家开始发言
  func action_speak(_ rawData: [String:Any]);
  // 有玩家取消发言
  func action_unspeak(_ rawData: [String:Any]);
  // 锁位置
  func action_lock(_ rawData: [String:Any]);
  // 踢出房间
  func action_kick_out(_ rawData: [String:Any]);
  // 聊天
  func action_chat(_ rawData: [String:Any]);
  // 重连
  func action_reconnect(_ rawData: [String:Any]);
  // 重连后的房间信息
  func action_restore_room(_ rawData: [String:Any]);
  // 系统消息，如:换房主的通知
  func action_system_msg(_ rawData: [String:Any]);
  // 服务器更换了
  func action_olive_changed(_ rawData: [String:Any]);
  // 修改密码
  func action_change_password(_ rawData: [String:Any]);
  // 收到添加好友的消息
  func action_add_friend(_ rawData: [String:Any]);
  //赠送卡片
  func action_send_card(_ rawData: [String:Any]);
  //赠送礼物
  func action_send_gift(_ rawData: [String:Any]);
  // 玩家准备
  func action_prepare(_ rawData: [String:Any]);
  // 游戏开始+抢角色
  func action_start(_ rawData: [String:Any]);
  // 角色分配
  func action_assigned_role(_ rawData: [String:Any]);
  // 投票
  func action_vote(_ rawData: [String:Any]);
  // 投票结果
  func action_vote_result(_ rawData: [String:Any]);
  // 轮麦
  func action_speech(_ rawData: [String:Any]);
  //游戏结束
  func action_game_over(_ rawData: [String:Any]);
  // 死亡信息
  func action_death_info(_ rawData: [String:Any]);
  // // 更改语音房标题
  func action_update_title(_ rawData: [String:Any])
  // 上麦
  func action_up_seat(_ rawData: [String:Any])
  // 下麦
  func action_down_seat(_ rawData: [String:Any])
  // 抱人上麦
  func action_force_seat(_ rawData: [String:Any])
  // 移交房主
  func action_handover_master(_ rawData: [String:Any])
  // 房间模式修改
  func action_change_user_state(_ rawData: [String:Any])
  // 玩游戏gif
  func action_show_emoticon(_ rawData: [String:Any])
  // 黑名单
  func action_block_user_list(_ rawData: [String:Any])
  // 玩家取消准备
  func action_unprepare(_ rawData: [String:Any]);
  // 获取词语
  func action_uc_get_words_result(_ rawData: [String:Any])
  // 猜词提示框
  func action_uc_guess_word(_ rawData: [String:Any])
  // 猜测的词语是啥
  func action_uc_guess_word_result(_ rawData: [String:Any])
  // 开始配置
  func action_uc_start_config(_ rawData: [String:Any])
  // 结束配置
  func action_uc_stop_config(_ rawData: [String:Any])
  // 更新配置
  func action_uc_update_config(_ rawData: [String:Any])
  // 断线信息
  func action_disconnected(_ rawData: [String:Any])
  // 重连
  func actino_connected(_ rawData: [String:Any])
  // 请求自由模式
  func action_request_free_mode(_ rawData: [String:Any])
  // 接受了自由模式
  func action_accept_free_mode(_ rawData: [String:Any])
  // 拒绝了自由模式
  func action_reject_free_mode(_ rawData: [String:Any])
  // 建立连接的第一条消息
  func action_connect_info(_ rawData: [String:Any]);
  // 点赞
  func action_like_room(_ rawData: [String:Any])
  // 游戏gif
  func action_show_game_emoticon(_ rawData: [String:Any])
  // 购买房契得到的update_config
  func action_update_config(_ rawData: [String:Any])
  // 购买房契
  func action_purchase_room_result(_ rawData: [String:Any])
  // 移交房契
  func action_handover_creator_result(_ rawData: [String:Any])
}
