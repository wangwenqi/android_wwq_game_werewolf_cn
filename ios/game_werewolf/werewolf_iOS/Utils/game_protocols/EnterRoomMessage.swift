//
//  EnterRoomMessage.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/10/10.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation

protocol EnterRoomMessage {
  // 建立连接的第一条消息
  func action_connect_info(_ rawData: [String:Any]);
  // 建立连接成功后，给服务器发送enter消息，服务器回复的enter消息
  func action_enter(_ rawData: [String:Any]);
  // 系统消息，如:换房主的通知
  func action_system_msg(_ rawData: [String:Any]);
  // 有新的玩家进入
  func action_join(_ rawData: [String:Any]);
  //有玩家离开
  func action_leave(_ rawData: [String:Any]);
  // 房主变更
  func action_update_master(_ rawData: [String:Any]);
  // 玩家准备
  func action_prepare(_ rawData: [String:Any]);
  // 玩家取消准备
  func action_unprepare(_ rawData: [String:Any]);
  // 聊天
  func action_chat(_ rawData: [String:Any]);
  // 踢出房间
  func action_kick_out(_ rawData: [String:Any]);
  // 游戏开始+抢角色
  func action_start(_ rawData: [String:Any]);
  // 抢角色结果
  func action_apply_role_result(_ rawData: [String:Any]);
  // 角色分配
  func action_assigned_role(_ rawData: [String:Any]);
  // 天黑
  func action_sunset(_ rawData: [String:Any]);
  // 天亮
  func action_sunup(_ rawData: [String:Any]);
  // 有玩家开始发言
  func action_speak(_ rawData: [String:Any]);
  // 有玩家取消发言
  func action_unspeak(_ rawData: [String:Any]);
  // 轮麦
  func action_speech(_ rawData: [String:Any]);
  //杀人
  func action_kill(_ rawData: [String:Any]);
  // 狼人准备杀人
  func action_wake_to_kill(_ rawData: [String:Any]);
  // 狼人杀人最终结果
  func action_kill_result(_ rawData: [String:Any]);
  // 预言家查人
  func action_check(_ rawData: [String:Any]);
  // 查人结果
  func action_check_result(_ rawData: [String:Any]);
  // 女巫救人
  func action_save(_ rawData: [String:Any]);
  // 女巫毒人
  func action_poison(_ rawData: [String:Any]);
  // 死亡信息
  func action_death_info(_ rawData: [String:Any]);
  // 投票
  func action_vote(_ rawData: [String:Any]);
  // 投票结果
  func action_vote_result(_ rawData: [String:Any]);
  // 猎人带走
  func action_take_away(_ rawData: [String:Any]);
  // 猎人带走结果
  func action_take_away_result(_ rawData: [String:Any]);
  //游戏结束
  func action_game_over(_ rawData: [String:Any]);
  // 锁位置
  func action_lock(_ rawData: [String:Any]);
  // 禁言
  func action_mute_all(_ rawData: [String:Any]);
  // 解除禁言
  func action_unmute_all(_ rawData: [String:Any]);
  // 爱神连接
  func action_link(_ rawData: [String:Any]);
  // 爱神连接结果
  func action_link_result(_ rawData: [String:Any]);
  // 是否竞选警长
  func action_apply(_ rawData: [String:Any]);
  // 竞选警长结果
  func action_apply_result(_ rawData: [String:Any]);
  // 放弃竞选
  func action_give_up_apply(_ rawData: [String:Any]);
  // 最后的警长
  func action_sheriff_result(_ rawData: [String:Any]);
  // 警长交接 
  func action_hand_over(_ rawData: [String:Any]);
  // 警长交接结果
  func action_hand_over_result(_ rawData: [String:Any]);
  // 重连
  func action_reconnect(_ rawData: [String:Any]);
  // 重连后的房间信息
  func action_restore_room(_ rawData: [String:Any]);
  // 长时间未操作
  func action_unactive_warning(_ rawData: [String:Any]);
  // 修改密码
  func action_change_password(_ rawData: [String:Any]);
  // 使用延迟卡
  func action_append_time(_ rawData: [String:Any]);
  // 使用延迟卡的结果
  func action_append_time_result(_ rawData: [String:Any]);
  // 使用查验卡的结果
  func action_card_check_result(_ rawData: [String:Any]);
  // 服务器更换了
  func action_olive_changed(_ rawData: [String:Any]);
  // 收到添加好友的消息
  func action_add_friend(_ rawData: [String:Any]);
  //守卫
  func action_protect(_ rawData: [String:Any]);
  //守卫结果
  func action_protect_result(_ rawData: [String:Any]);
  //收到自爆的消息
  func action_boom(_ rawData: [String:Any]);
  //自爆带人
  func action_boom_away(_ rawData: [String:Any]);
  //自爆带人结果
  func action_boom_away_result(_ rawData: [String:Any]);
  // 是否可以自爆
  func action_boom_state(_ rawData: [String:Any]);
  // 召集令消息
  func action_export(_ rawData: [String:Any]);
  //毒人结果
  func action_poison_result(_ rawData: [String:Any]);
  //救人结果
  func action_save_result(_ rawData: [String:Any]);
  // 更新配置
  func action_update_config(_ rawData: [String:Any]);
  //发言顺序
  func action_speech_direction(_ rawData: [String:Any]);
  //发言顺序结果
  func action_speech_direction_result(_ rawData: [String:Any]);
  //请求更换房主
  func action_request_reset_master(_ rawData: [String:Any]);
  //拒绝更换房主
  func action_reject_reset_master(_ rawData: [String:Any]);
   //发送召集令结果
  func action_export_pay(_ rawData: [String:Any]);
  //赠送卡片
  func action_send_card(_ rawData: [String:Any]);
  //赠送礼物
  func action_send_gift(_ rawData: [String:Any]);
  //狼人夜间说话功能
  func action_can_speak(_ rawData: [String:Any]);
  //魔术师
  func action_exchange(_ rawData: [String:Any]);
  func action_exchange_result(_ rawData: [String:Any]);
  //恶魔👿
  func action_demon_check(_ rawData: [String:Any]);
  func action_demon_check_result(_ rawData: [String:Any]);
  //观战
   func action_observer_join(_ rawData: [String:Any])
   func action_observer_leave(_ rawData: [String:Any])
}
