//
//  XBHButton.swift
//  interview
//
//  Created by QiaoYijie on 17/6/16.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import UIKit
import QuartzCore;

@IBDesignable
class XBHButton: UIButton {
  
  @IBInspectable var startColor: UIColor = UIColor.clear
  @IBInspectable var endColor: UIColor = UIColor.clear
  
  @IBInspectable var titleLabelShadowColor: UIColor = UIColor.blue;
  @IBInspectable var titleLabelShadowOffset: CGSize = CGSize.init(width: 1, height: 1);
  
  // 按下按钮出现的颜色 确认 Highlighted Adjusts Image 没有勾选上
  @IBInspectable var customHighlightColor: UIColor = UIColor.clear;
  
  @IBInspectable var buttonShdowColor: UIColor = UIColor.clear;
  
  fileprivate let gradLayerKey = "forGrad";
  fileprivate let highlightLayerKey = "highlightLayer";
  fileprivate let shadowLayerKey = "shadowLayer";
  
  //圆角 直接改变是不可以的，在plus机型下，拿到的bound是不正确的。
  @IBInspectable var cornerRadius: CGFloat = 0.0;
  
  //边框宽度
  @IBInspectable var borderWidth: CGFloat = 0.0 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }
  
  //边框颜色
  @IBInspectable var borderColor: UIColor = UIColor.white {
    didSet {
      layer.borderColor = borderColor.cgColor
    }
  }
  
  override func draw(_ rect: CGRect) {
    super.draw(rect);
    if startColor == UIColor.clear && endColor == UIColor.clear,
      let bgColor = self.backgroundColor {
      startColor = bgColor;
      endColor = bgColor;
    }
    self.setTitleSoftShadowColor(color: titleLabelShadowColor, offset: titleLabelShadowOffset);
    self.refrshBackgroundColor();
    self.setButtonSoftShadowColor(color: buttonShdowColor);
    self.setButtonRadius(radius: cornerRadius);
    
    if let tl = self.titleLabel {
      self.bringSubview(toFront: tl);
    }
    
    if let im = self.imageView {
      self.bringSubview(toFront: im);
    }
  }
  
  func setButtonRadius(radius: CGFloat) {
    let gradient = getGradientLayer();
    
    var mask = gradient.mask as? CAShapeLayer;
    
    if  mask == nil {
      mask = CAShapeLayer.init();
    }
    
    print("bounds:\(bounds)");
    mask?.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath;
    
    gradient.mask = mask;
  }
  
  func setTitleSoftShadowColor(color:UIColor, offset:CGSize) {
    if(color == UIColor.clear) {
      return;
    }
    // 阴影层的扩散半径
    self.titleLabel?.layer.shadowRadius = 6.0;
    //阴影层的透明度
    self.titleLabel?.layer.shadowOpacity = 0.8;
    //阴影层的颜色，设为已存的颜色
    self.titleLabel?.layer.shadowColor = color.cgColor;
    //阴影层的偏移，设为已存的偏移
    self.titleLabel?.layer.shadowOffset = offset;
  }
  
  func setButtonSoftShadowColor(color:UIColor) {
    
    var shadowLayer = self.getSublayerByKey(key: shadowLayerKey);
    
    if shadowLayer == nil {
      shadowLayer = CALayer();
      shadowLayer?.setValue("1", forKey: shadowLayerKey);
      shadowLayer?.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath;
      shadowLayer?.shadowOffset = CGSize.init(width: 0, height: 2);
      shadowLayer?.shadowOpacity = 1.0
      shadowLayer?.shadowRadius = 0.0
      shadowLayer?.zPosition = -2;
      if let sl = shadowLayer {
        layer.addSublayer(sl);
      }
    }
    
    shadowLayer?.shadowColor = color.cgColor;
  }
  
  
  func refrshBackgroundColor() {
    
    let gradient = getGradientLayer();
    
    // 设置的背景颜色无法使用 需要设置
    //        if let bColor = self.backgroundColor {
    //            self.backgroundColor = UIColor.yellow;
    //            self.setBackgroundImage(UIImage, for: .default);
    //            gradient.backgroundColor = bColor.cgColor;
    //            return;
    //        }
    
    gradient.colors = [startColor.cgColor, endColor.cgColor];
  }
  
  func addCustomHighlightLayer() {
    
    var highlightLayer = getSublayerByKey(key: highlightLayerKey);
    
    if highlightLayer == nil {
      highlightLayer = CALayer.init();
      highlightLayer?.backgroundColor = self.customHighlightColor.cgColor;
      highlightLayer?.frame = bounds;
      highlightLayer?.isHidden = true;
      highlightLayer?.zPosition = 10;
      highlightLayer?.setValue("1", forKey: highlightLayerKey);
      if let light = highlightLayer {
        layer.addSublayer(light);
      }
    }
    
    highlightLayer?.isHidden = false;
    
    getSublayerByKey(key: shadowLayerKey)?.isHidden = true;
  
  }
  
  func removeCustomHighlightLayer() {
    let highlightLayer = getSublayerByKey(key: highlightLayerKey);
    highlightLayer?.isHidden = true;
    getSublayerByKey(key: shadowLayerKey)?.isHidden = false;
  }
  
  fileprivate func getGradientLayer() -> CAGradientLayer {
    var gradient = self.getSublayerByKey(key: gradLayerKey) as? CAGradientLayer;
    
    if let gl = gradient {
      return gl;
    }
    
    gradient = CAGradientLayer()
    if let gl = gradient {
      gl.setValue("1", forKey: gradLayerKey);
      gl.zPosition = -1;
      gl.frame = bounds;
      layer.addSublayer(gl);
      return gl;
    } else {
      return CAGradientLayer();
    }
  }
  
  func getSublayerByKey(key :String) -> CALayer? {
    if let layers = self.layer.sublayers {
      for layer in layers {
        if layer.value(forKey: key) as? String == "1" {
          return layer;
        }
      }
    }
    return nil;
  }
    
  override var isEnabled: Bool {
    didSet{
      if isEnabled == false {
        self.layer.borderColor = UIColor.hexColor("715C97").cgColor
      }
    }
  }
  
}
