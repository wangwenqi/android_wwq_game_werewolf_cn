//
//  XBHLabel.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/15.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class XBHLabel: LTMorphingLabel {
    
    //圆角
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        
        didSet {
            
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
        }
    }
    
    //边框宽度
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        
        didSet {
            
            layer.borderWidth = borderWidth
        }
    }
    
    //边框颜色
    @IBInspectable var borderColor: UIColor = UIColor.white {
        
        didSet {
            
            layer.borderColor = borderColor.cgColor
        }
    }
    
}
