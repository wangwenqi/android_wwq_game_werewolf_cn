//
//  XBHView.swift
//  interview
//
//  Created by QiaoYijie on 16/7/23.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import UIKit

class XBHView: SpringView {

    //圆角
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        
        didSet {
            
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
        }
    }
    
    //边框宽度
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        
        didSet {
            
            layer.borderWidth = borderWidth
        }
    }
    
    //边框颜色
    @IBInspectable var borderColor: UIColor = UIColor.white {
        
        didSet {
            
            layer.borderColor = borderColor.cgColor
        }
    }

}
