//
//  RotationNavigationViewController.swift
//  game_werewolf
//
//  Created by Hang on 2018/2/1.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

class RotationNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
  
  override var shouldAutorotate: Bool {
    return false
  }
  
//  override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
//    return UIInterfaceOrientationMask.portrait
//  }
//  override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
//    return UIInterfaceOrientation.portrait
//  }

}
