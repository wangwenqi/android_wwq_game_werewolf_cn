//
//  NetworkManager.swift
//  game_werewolf
//
//  Created by Hang on 2018/2/1.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {
  
  //shared instance
  static let shared = NetworkManager()
  var lastStatus = NetworkReachabilityManager.NetworkReachabilityStatus.unknown {
    willSet{
      if lastStatus == .notReachable && newValue == .reachable(.ethernetOrWiFi) {
        self.checkControlSocket()
      }
      if lastStatus == .notReachable && newValue == .reachable(.wwan) {
        self.checkControlSocket()
      }
    }
  }
  let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.baidu.com")
  
  func startNetworkReachabilityObserver() {
    
    reachabilityManager?.listener = { status in
      self.lastStatus = status
      switch status {
      case .notReachable:
        print("The network is not reachable")
      case .unknown :
        print("It is unknown whether the network is reachable")
      case .reachable(.ethernetOrWiFi):
        print("The network is reachable over the WiFi connection")
      case .reachable(.wwan):
        print("The network is reachable over the WWAN connection")

      }
    }
    // start listening
    reachabilityManager?.startListening()
  }
  //全局socket
  func checkControlSocket() {
      SwiftConverter.shareInstance.connectControlSocket()
  }
}
