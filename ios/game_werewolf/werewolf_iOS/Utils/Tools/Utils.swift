
//
//  Utils.swift
//  MIDIMusic
//
//  Created by QiaoYijie on 16/11/18.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation
import AudioToolbox
import AVFoundation;
import DateToolsSwift
import ChatKit
import KeychainSwift

enum deviceType {
  case iPhone4
  case iPhone5
  case iPhone6
  case iPhone6P
}

let kIsIPhoneX = UIScreen.main.nativeBounds.height == 2436;
let kSafeAreaBottomHeight:CGFloat = kIsIPhoneX ? 34.0 : 0.0;

class PlayerClass: NSObject, AVAudioPlayerDelegate {
  deinit {
    print("PlayerClass deinit");
  }
  
  func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
    print("------audioPlayerDecodeErrorDidOccur error-------------\(error)");
  }
  
  func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
    print("------audioPlayerDidFinishPlaying--\(flag)")
  }
}

class Utils: NSObject {
  //MARK: UserDefaults
  // 增/改
  class func defaultsSaveObject(_ value: Any?, key: String) {
    let defaults = UserDefaults.standard
    defaults.set(value, forKey: key)
    defaults.synchronize()
  }
  // 查
  class func defaultsReadObjectWithKey(_ key: String) -> Any? {
    let defaults = UserDefaults.standard
    return defaults.object(forKey: key) as Any?
  }
  // 判断是都含有
  class func defaultsHadObjectWith(_ key: String) -> Bool {
    if defaultsReadObjectWithKey(key) != nil {
      return true
    }
    return false
  }
  
  // 删
  class func defaultsRemoveObjectWithKey(_ key: String) {
    let defaults = UserDefaults.standard
    if defaultsHadObjectWith(key) {
      defaults.removeObject(forKey: key)
      defaults.synchronize()
    }
  }
  
  // 新线程中运行
  class func runInNewThread(_ action: @escaping () -> Void){
    DispatchQueue.global().async {
      action()
    }
  }
  
  // 主线程中运行
  class func runInMainThread(_ action: @escaping () -> Void){
    DispatchQueue.main.async(execute: {
      action()
    })
  }
  
  // 延时执行方法
  class func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
      deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
  }
  
  // 生成随机字符串
  class func getRandomStringOfLength(length: Int) -> String {
    
    let characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    var ranStr = ""
    for _ in 0..<length {
      let index = Int(arc4random_uniform(UInt32(characters.characters.count)))
      ranStr.append(characters[characters.index(characters.startIndex, offsetBy: index)])
    }
    return ranStr
  }
  
  // 字典转换成json
  class func dicToJSONStr(_ data:NSDictionary) -> String? {
    if let dataStr = try? JSONSerialization.data(withJSONObject: data, options: []) {
      if let string = NSString(data: dataStr, encoding: String.Encoding.utf8.rawValue) as String? {
        return string;
      }
    }
    return nil;
  }
  
  //背景蒙板
  static var backGroundView: UIView = {
    
    let view = UIView()
    view.frame = Screen.bounds
    view.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
    return view
  }()
  
  // noticeView
  class func showNoticeView(view: BaseNoticeView) {
    
    hideNoticeView()
    var window = UIApplication.shared.keyWindow
    window = getKeyWindow()
    window?.addSubview(Utils.backGroundView)
    UIView.animate(withDuration: 0.2, animations: {
      backGroundView.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
    })
    backGroundView.addSubview(view)
    view.animation = "slideDown"
    view.curve = "easeIn"
    view.duration = 0.5
    view.damping = 0.4
    view.animate()
  }
  
  class func showAlertView(view: BaseNoticeView) {
    hideNoticeView()
    
    var window = UIApplication.shared.keyWindow
    window = getKeyWindow()
    window?.addSubview(backGroundView)
    UIView.animate(withDuration: 0.2, animations: {
      backGroundView.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
    })
    backGroundView.addSubview(view)
    view.animation = "zoomIn"
    view.curve = "easeOut"
    view.duration = 0.45
    view.damping = 0.5
    view.animate()
  }
  
  class func getKeyWindow() -> UIWindow?  {
    let window = UIApplication.shared.keyWindow
      //弹出alertview时keywindow会变为_UIAlertControllerShimPresenterWindow
    if (window?.isKind(of: NSClassFromString("_UIAlertControllerShimPresenterWindow")!))! {
      return  UIApplication.shared.windows.count >= 0 ? UIApplication.shared.windows[0] : nil
    }else{
      return window
    }
  }
  //获取当前的vc
  class func getCurrentVC(vc:UIViewController) -> UIViewController? {
    
    if (vc.presentedViewController != nil) {
          return self.getCurrentVC(vc: vc.presentedViewController!)
    }else if vc.isKind(of: UINavigationController.self) {
        let nav = vc as! UINavigationController
      if nav.viewControllers.count > 0 {
          return nav.topViewController
      }else{
          return vc
      }
    }else{
      return vc
    }
  }
  
  //获取当前的设备类型
  
  static func getCurrentDeviceType() ->deviceType{
    let length = UIScreen.main.bounds.size.height
    
    if length == 568 {
      //iphone5
      return deviceType.iPhone5
    }else if length == 667 {
      //iphone6
      return deviceType.iPhone6
    }else if length == 736 {
        return deviceType.iPhone6P
    }else if length < 568 {
        return deviceType.iPhone4
    }else{
      return deviceType.iPhone6P
    }
  }
    
  class func screenShot(view:UIView) -> UIImage {
    
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0);
    
    view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
    
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    
    UIGraphicsEndImageContext()
    
    return image
  
  }
  
  class func getCurrentNoticeViews() -> [UIView] {
    return backGroundView.subviews;
  }
  
  class func hideNoticeView() {
    
    
    UIApplication.shared.keyWindow?.endEditing(true)
    
    for view in backGroundView.subviews {
      if view.gestureRecognizers?.count != nil && (view.gestureRecognizers?.count)! > 0 {
        for g in view.gestureRecognizers! {
          view.removeGestureRecognizer(g)
        }
      }
      view.removeFromSuperview()
    }
    backGroundView.removeFromSuperview()
  }
  
  class func getNoticeView() -> UIView {
    return backGroundView;
  }
  
  class func hideAnimated() {
    UIView.animate(withDuration: 0.1, animations: {
      backGroundView.alpha = 0.0
    }, completion: { (flag) in
      for view in backGroundView.subviews {
        view.removeFromSuperview()
      }
      backGroundView.removeFromSuperview()
      backGroundView.alpha = 1.0
    })
  }
  
  static func getCachedBLContent() -> NSMutableDictionary? {
    let file = FileManager.default
    let id = CurrentUser.shareInstance.id
    let cachePath = NSHomeDirectory() + "/Documents/\(id).BL"
    if file.fileExists(atPath: cachePath) {
      if let dic = NSKeyedUnarchiver.unarchiveObject(withFile: cachePath) as?NSMutableDictionary {
        return dic
      }else{
        return nil
      }
    }
    return nil
  }
  
  static private func generateRNMessage(user_id:String) -> JSON {
    let data:Dictionary<String,Any> = ["action":"black_list_add","params":["user_id":user_id],"options":["needcallback":false,"sync":true]]
    let json = JSON.init(data)
    return json
  }
  
  static func addToBL(id:String) {
    if let BL = getCachedBLContent() {
        BL[id] = "Y"
        saveBL(BL)
    }
    //通知RN
    SwiftConverter.shareInstance.sendMessagesToRN(json: generateRNMessage(user_id: id))
  }
  
  static func remove(id:String) {
    if let BL = getCachedBLContent() {
      BL.removeObject(forKey: id)
      saveBL(BL)
    }
    //通知RN
    SwiftConverter.shareInstance.sendMessagesToRN(json: generateRNMessage(user_id: id))
  }
  
  static func downloadBL() {
    DispatchQueue.global().async {
//      if let BL = getCachedBLContent() {
//        //文件存在，看看是不是已经更新过
//        print("\(BL)")
//      }else{
        //文件不存在就更新一下
        //获取最新的黑名单,然后存在本地，存在本地的值应该是Dictionary，OC和swift都需要读取
        RequestManager().get(url: RequestUrls.allBlockList, success: { (json) in
          if let arr = json["users"].array {
            let finalDic = NSMutableDictionary()
            arr.forEach({ (item) in
              if let id = item["id"].string {
                finalDic[id] = "Y"
              }
            })
            saveBL(finalDic)
          }
        }, error: { (code, message) in
          
        })
//      }
    }
  }
  
  static func saveMute(mute:Bool,muteid:String) {
    let client = LCChatKit.sharedInstance().client
    let query = client?.conversationQuery()
    query?.getConversationById(muteid, callback: { (conversation, error) in
      if mute {
        conversation?.mute(callback: {(result, error) in
//          if result {
//            let muted = [CurrentUser.shareInstance.id:mute]
//            UserDefaults.standard.set(muted, forKey:"MUTEFAMILY")
//            UserDefaults.standard.synchronize()
//          }else{
//             let muted = [CurrentUser.shareInstance.id:!mute]
//            UserDefaults.standard.set(muted, forKey:"MUTEFAMILY")
//            UserDefaults.standard.synchronize()
//          }
        })
      }else{
        conversation?.unmute(callback: { (result, error) in
//          if result {
//            let muted = [CurrentUser.shareInstance.id:mute]
//            UserDefaults.standard.set(muted, forKey:"MUTEFAMILY")
//            UserDefaults.standard.synchronize()
//          }else{
//            let muted = [CurrentUser.shareInstance.id:!mute]
//            UserDefaults.standard.set(muted, forKey:"MUTEFAMILY")
//            UserDefaults.standard.synchronize()
//          }
        })
      }
    })
  }
  
  static func getMute() -> Dictionary<String,Any>? {
    return UserDefaults.standard.object(forKey:"MUTEFAMILY") as? Dictionary
  }
  
  static func gnerateDefaultSL() {
    let file = FileManager.default
    let cachePath = NSHomeDirectory() + "/Documents/SV.LS"
    if file.fileExists(atPath: cachePath) == false {
      //生成默认的地址
     let serverList = NSMutableDictionary.init(dictionary:["api":"https://intviu-api.leancloud.cn","rtm":"wss://intviu-rtm.leancloud.cn"])
      saveSV(serverList)
    }
  }
  
  static func saveBL(_ dic:NSMutableDictionary) {
    let id = CurrentUser.shareInstance.id
    let cachePath = NSHomeDirectory() + "/Documents/\(id).BL"
    NSKeyedArchiver.archiveRootObject(dic, toFile: cachePath)
    do {
      var fileUrl = URL.init(fileURLWithPath: cachePath)
      var resourceValues = URLResourceValues()
      resourceValues.isExcludedFromBackup = true
      try  fileUrl.setResourceValues(resourceValues)
    } catch {
      print("Error: \(error)")
    }
  }
  
  //保存服务器地址到本地
  static func saveSV(_ dic:NSMutableDictionary) {
    let cachePath = NSHomeDirectory() + "/Documents/SV.LS"
    NSKeyedArchiver.archiveRootObject(dic, toFile: cachePath)
    do {
      var fileUrl = URL.init(fileURLWithPath: cachePath)
      var resourceValues = URLResourceValues()
      resourceValues.isExcludedFromBackup = true
      try  fileUrl.setResourceValues(resourceValues)
    } catch {
      print("Error: \(error)")
    }
  }
  //获取服务器地址
  static func getCachedSVList() -> NSMutableDictionary? {
    let file = FileManager.default
    let cachePath = NSHomeDirectory() + "/Documents/SV.LS"
    if file.fileExists(atPath: cachePath) {
      if let dic = NSKeyedUnarchiver.unarchiveObject(withFile: cachePath) as?NSMutableDictionary {
        return dic
      }else{
        return nil
      }
    }
    return nil
  }
  //获取当前的时间戳
  class func getCurrentTimeStamp() -> Int64 {
    return NSNumber(value: Date().timeIntervalSince1970*1000 as Double).int64Value;
  }
  
  static let audioDelegate:AVAudioPlayerDelegate = PlayerClass();
  
  static var player:AVAudioPlayer?;
  
  static var systemSoundID: SystemSoundID = 0
  //播放语音
  class func playSound(name: String) {
    
//    AudioServicesDisposeSystemSoundID(systemSoundID)
//    
//    let vibrate = UInt32(kSystemSoundID_Vibrate)
//    AudioServicesPlaySystemSound(vibrate)
//    let soundPath = Bundle.main.path(forResource: name, ofType: ".mp3")
//    
//    guard soundPath != nil else { return }
//    
//    AudioServicesCreateSystemSoundID(NSURL(fileURLWithPath: soundPath!), &Utils.systemSoundID)
//    AudioServicesPlaySystemSound(Utils.systemSoundID)
    
    let soundPath = Bundle.main.path(forResource: name, ofType: ".mp3");
    guard soundPath != nil else { return }
    do {
      player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: soundPath!));
      player?.delegate = audioDelegate;
      if let audioPlayer = player, audioPlayer.isPlaying {
        audioPlayer.stop();
      }
      player?.volume = 10.0;
      if let audioPlayer = player, audioPlayer.prepareToPlay() {
        audioPlayer.play();
      }
    } catch let error as NSError {
      print("---------------error:\(error)");
    }
    
  }
 
  // 停止播放
  class func stopPlaySound() {
//    AudioServicesDisposeSystemSoundID(systemSoundID)
    if let audioPlayer = player, audioPlayer.isPlaying {
      audioPlayer.play();
      player = nil;
    }
  }
  
  static func getTaskType(platform: UMSocialPlatformType, type: ShareContentType) -> String? {
    switch platform {
    case .wechatSession :
      return addPrifix(type, key: "weixin_qq");
    case .wechatTimeLine :
      return addPrifix(type, key: "wxwall");
    case .QQ:
      return addPrifix(type, key: "weixin_qq");
    case .facebook:
      return addPrifix(type, key: "weixin_qq");
    case .line:
      return addPrifix(type, key: "weixin_qq");
    case .whatsapp:
      return addPrifix(type, key: "weixin_qq");
    // 游戏内分享
    case .userDefine_Begin:
      return addPrifix(type, key: "friend");
    default:
      return addPrifix(type, key: "weixin");
    }
  }
  
  static func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
    let calendar = NSCalendar.current
    let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
    let now = NSDate()
    let earliest = now.earlierDate(date as Date)
    let latest = (earliest == now as Date) ? date : now
    let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
    
    if (components.year! >= 2) {
      return "\(components.year!)年前"
    } else if (components.year! >= 1){
      if (numericDates){
        return "一年前"
      } else {
        return "一年前"
      }
    } else if (components.month! >= 2) {
      return "\(components.month!)个月前"
    } else if (components.month! >= 1){
      if (numericDates){
        return "一个月前"
      } else {
        return "一个月前"
      }
    } else if (components.weekOfYear! >= 2) {
      return "\(components.weekOfYear!)周前"
    } else if (components.weekOfYear! >= 1){
      if (numericDates){
        return "一周前"
      } else {
        return "一周前"
      }
    } else if (components.day! >= 2) {
      return "\(components.day!)天前"
    } else if (components.day! >= 1){
      if (numericDates){
        return "昨天"
      } else {
        return "昨天"
      }
    } else if (components.hour! >= 2) {
      return "\(components.hour!)小时前"
    } else if (components.hour! >= 1){
      if (numericDates){
        return "一小时前"
      } else {
        return "一小时前"
      }
    } else if (components.minute! >= 2) {
      return "\(components.minute!)分钟前"
    } else if (components.minute! >= 1){
      if (numericDates){
        return "1分钟前"
      } else {
        return "1分钟前"
      }
    } else if (components.second! >= 3) {
      return "\(components.second!)秒前"
    } else {
      return NSLocalizedString("刚刚", comment: "")
    }
    
  }
  
  static func getUUID() -> String? {
    let uuidKey = "cn.orangelab.werewolf.UUID"
    let keychain = KeychainSwift()
    let uuid = keychain.get(uuidKey)
    return uuid
  }
  
  static func checkAndGenerateUniqueIdentifier() {
    if let oldUUID = getUUID() {
     //已经存在就不做操作
    }else{
      generateNewKey()
    }
  }
  
  static func generateNewKey() {
    let uuid =  UIDevice.current.identifierForVendor!.uuidString
    let uuidKey = "cn.orangelab.werewolf.UUID"
    let keychain = KeychainSwift()
    keychain.set(uuid, forKey: uuidKey)
  }
  
  //
  static func arrayToJsonString(from object: Any) -> String? {
    if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
      let objectString = String(data: objectData, encoding: .utf8)
      return objectString
    }
    return nil
  }
  
  
  //分享
  static func UMShare(_ title: String, detail: String, url: String, icon:UIImage, type: ShareContentType) {
    //title
    UMSocialUIManager.setPreDefinePlatforms([NSNumber(integerLiteral:UMSocialPlatformType.wechatSession.rawValue),NSNumber(integerLiteral:UMSocialPlatformType.wechatTimeLine.rawValue),NSNumber(integerLiteral:UMSocialPlatformType.QQ.rawValue)]) //,,
    
    UMSocialShareUIConfig.shareInstance().sharePageGroupViewConfig.sharePageGroupViewPostionType = UMSocialSharePageGroupViewPositionType.bottom
    UMSocialShareUIConfig.shareInstance().sharePageScrollViewConfig.shareScrollViewPageItemStyleType = UMSocialPlatformItemViewBackgroudType.none
    UMSocialShareUIConfig.shareInstance().shareTitleViewConfig.shareTitleViewTitleString = NSLocalizedString("分享", comment: "")
    UMSocialShareUIConfig.shareInstance().shareTitleViewConfig.shareTitleViewPaddingTop = 10
    
    UMSocialShareUIConfig.shareInstance().sharePageScrollViewConfig.shareScrollViewPageMaxColumnCountForPortraitAndBottom = 3
    
    UMSocialShareUIConfig.shareInstance().sharePageScrollViewConfig.shareScrollViewPageColumnSpace = 130
    UMSocialShareUIConfig.shareInstance().shareCancelControlConfig.isShow = false
    
    // UMSocialUIManager.setShareMenuViewDelegate(self)
    
    UMSocialUIManager.showShareMenuViewInWindow { (platformType, Diction) in
      let message = UMSocialMessageObject()
      if type == .normal || type == .Invite {
        if platformType == .line { //最多支持图文
          message.text = detail
        }else if platformType == .whatsapp { //不支持图文
          message.text = detail
        }else{
          let shareContent = UMShareWebpageObject()
          shareContent.webpageUrl = url
          shareContent.title = title
          shareContent.descr = detail
          shareContent.thumbImage = icon
          message.shareObject = shareContent
        }
        //UM统计
        MobClick.event("\(type.description)_\(platformType.description)")
      }else if type == .pictureOnly {
        let shareContent = UMShareImageObject()
        shareContent.shareImage = icon
        message.shareObject = shareContent
        
      }
      UMSocialManager.default().share(to: platformType, messageObject:message , currentViewController: nil, completion: { (data,error) in
        /*
        if error != nil {
          print("\(error)")
        } else {
          
        }*/
      })
      if let sendType = Utils.getTaskType(platform: platformType, type: type) {
        print(NSLocalizedString("----------发送数据:\(sendType);;;count:\(1)", comment: ""));
        RequestManager().post(
          url: RequestUrls.sendTaskData,
          parameters: ["type": sendType, "count": 1],
          success: { (response) in
            
        }, error: { (code, message) in
        });
      }
    }
  }
}

//分型内容的类型
enum ShareContentType {
  case Invite
  case normal
  //纯图片
  case pictureOnly
  //纯文字
  case textOnly
  
  case webUrl //图片文字url
  
  case picAndText //图文
}

let sharePrifix = "share_to_";
let intviuPrifix = "intviu_to_";

func addPrifix(_ type: ShareContentType, key: String) -> String? {
  
  if key == "___sb___" {
    return nil;
  }
  
  if type == .Invite {
    return "invite";
  } else {
    return "\(sharePrifix)\(key)";
  }
  // return "\(type == .Invite ? intviuPrifix : sharePrifix)\(key)";
}




extension UMSocialPlatformType {
  
  var description : String {
    switch self {
    case .wechatSession:
      return "UMSocialPlatformType_WechatSession"
    case .wechatTimeLine:
      return "UMSocialPlatformType_WechatTimeLine"
    case .QQ:
      return "UMSocialPlatformType_QQ"
    case .facebook:
      return "UMSocialPlatformType_Facebook"
    case .line:
      return "UMSocialPlatformType_Line"
    case .whatsapp:
      return "UMSocialPlatformType_Whatsapp"
    default:
      return "UMSocialPlatformType_WechatSession"
    }
  }
}

extension ShareContentType {
  var description : String {
    switch self {
    case .Invite:
      return "Invite"
    case .normal:
      return "Share"
    default:
      return "Share"
    }
  }
}


extension String {
  func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
    
    return ceil(boundingBox.height)
  }
  
  func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
    
    return ceil(boundingBox.width)
  }
}
