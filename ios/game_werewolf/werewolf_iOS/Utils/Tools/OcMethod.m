//
//  OcMethod.m
//  game_werewolf
//
//  Created by Hang on 2017/10/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <ChatKit/LCCKChat.h>

@implementation OcMethod:NSObject

+ (NSString *)getImagePath:(UIImage *)Image {
  NSString *filePath = nil;
  NSData *data = nil;
  if (UIImageJPEGRepresentation(Image,0.7) == nil) {
     data = UIImagePNGRepresentation(Image);
  } else {
     data = UIImageJPEGRepresentation(Image, 0.7);
  }
  
  //图片保存的路径
  //这里将图片放在沙盒的documents文件夹中
  NSString *DocumentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
  //文件管理器
  NSFileManager *fileManager = [NSFileManager defaultManager];
  
  //把刚刚图片转换的data对象拷贝至沙盒中
  [fileManager createDirectoryAtPath:DocumentsPath withIntermediateDirectories:YES attributes:nil error:nil];
  NSString *ImagePath = [[NSString alloc] initWithFormat:@"/theFirstImage.png"];
  [fileManager createFileAtPath:[DocumentsPath stringByAppendingString:ImagePath] contents:data attributes:nil];
  
  //得到选择后沙盒中图片的完整路径
  filePath = [[NSString alloc] initWithFormat:@"%@%@", DocumentsPath, ImagePath];
  return filePath;
}


@end
