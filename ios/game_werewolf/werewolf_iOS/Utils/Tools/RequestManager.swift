//
//  RequestManager.swift
//  game_werewolf
//
//  Created by QiaoYijie on 2017/4/17.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation
import Alamofire

class RequestManager: NSObject {
  
  override init () {
    super.init()
  }
  
  // 请求头信息
  static var header = [
    "pt":"\(Config.platform)",
    "v":"\(Config.buildVersion)",
    "b":"\(Config.appStoreVersion)",
    "sv": "\(Config.serverVersion)",
    "tz": "\(Config.currentTimeZoneGMT)",
    "lg": "\(Config.systemLanage)",
    "did": Utils.getUUID() ?? "",
    "app": Config.app
  ]
  
  static func appendHeader(key: String, value: String) {
    header[key] = value
  }
  
  private func requestHandler(response:DataResponse<Data>, success: ((JSON) -> Void)?, error: ((Int, String) -> Void)?) {
    switch response.result {
    case .success(let value):
        let json = JSON.init(data: value)
      
        if json["code"].intValue == 1000 {
          if success != nil {
            success!(json["data"])
          }
        } else {
          if error != nil {
            error!(json["code"].intValue, json["message"].stringValue)
          }
        }
    case .failure:
      if let e = response.result.error {
        if let ne = e as? NSError {
         if ne.code == -999 {
          //取消
          if error != nil {
            error!(-999,NSLocalizedString("取消网络请求", comment: ""))
          }
        }else{
            if error != nil {
              error!(-500, NSLocalizedString("请检查您的网络", comment: ""))
            }
          }
        }
      }
    }
  }

  private func request(url: String, method: HTTPMethod, parameters: [String: Any], success: ((JSON) -> Void)?, error: ((Int, String) -> Void)?) {
    
    if method == .get {
      var newUrl = url;
      var params:[String] = [];
      if parameters.count > 0 {
        for (key, value) in parameters {
          if let val = value as? String {
            params.append("\(key)=\(val)");
          }
        }
        let perFix = newUrl.has("?") ? "&" : "?";
        newUrl += "\(perFix)\(params.joined(separator: "&"))";
      }

      Alamofire.request(newUrl, headers: RequestManager.header).responseData { (response) in

        Print.dlog("\n\n\n ================ http response =================================================== \nurl: \(newUrl)\nhttpMethod: \(method)\nparameters: \(parameters)\nheaders: \(RequestManager.header)\nresponse: \(response.description)=============== http response ================================================================== \n\n ")
        self.requestHandler(response: response, success: success, error: error);

      }
    } else {
      Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: RequestManager.header).responseData { (response) in
        
        Print.dlog("\n\n\n ================ http response =================================================== \nurl: \(url)\nhttpMethod: \(method)\nparameters: \(parameters)\nheaders: \(RequestManager.header)\nresponse: \(response.description)=============== http response ================================================================== \n\n ")

        self.requestHandler(response: response, success: success, error: error);
        
      }
    }
  }
  
  // POST
  func post(url: String, parameters: [String: Any] = [:], success: ((JSON) -> Void)?, error: ((Int, String) -> Void)?) {
    request(url: url, method: .post, parameters: parameters, success: success, error: error)
  }
  
  // GET
  func get(url: String, parameters: [String: Any] = [:], success: ((JSON) -> Void)?, error: ((Int, String) -> Void)?) {
    request(url: url, method: .get, parameters: parameters, success: success, error: error)
  }
  
  //下载文件
  static func downloadFile(url:String,toPath:String,success:@escaping ()-> Void,error:@escaping ()->()) {
      var fileFormat = ""
      if let file = url.split("/").last {
      //得到文件名
      if let format = file.split(".").last {
        fileFormat = format
      }
    }
      //得到文件名
     let hash = url.sha1()
     //查看图片是否已经下载
     let cachePath = NSHomeDirectory() + "/Documents/LWSource/\(hash)." + "\(fileFormat)"
     if FileManager.default.fileExists(atPath: cachePath) {
        success()
     }else{
     //如果都没有找到，然后在进行下载
      let destination: DownloadRequest.DownloadFileDestination = { _, _ in
        let pathComponent = "/LWSource/\(hash)." + "\(fileFormat)"
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(pathComponent)
        return (fileURL, [.createIntermediateDirectories, .removePreviousFile])
      }
      //下载图片
      Alamofire.download(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, to: destination).responseJSON(completionHandler: { (response) in
        if let status = response.response?.statusCode {
          switch status {
            case 200:
              success()
          default:
            //把下载失败的文件删除
            do {
                try FileManager.default.removeItem(atPath: cachePath)
            }catch{
              //删除文件错误
            }
            error()
          }
        }
      })
    }
  }
  

  static func getFolderList(documentsUrl:String) -> [String]? {
    do {
      // Get the directory contents urls (including subfolders urls)
      let directoryContents = try FileManager.default.contentsOfDirectory(atPath: documentsUrl)
      print(directoryContents)
      return directoryContents
    } catch {
      print(error.localizedDescription)
      return nil
    }
  }
}


