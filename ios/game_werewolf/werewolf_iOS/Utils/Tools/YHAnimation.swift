//
//  YHAnimation.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/22.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import pop


enum animationType:String {
  case SlideLeft = "slideLeft"
  case SlideRight = "slideRight"
  case SlideDown = "slideDown"
  case SlideUp = "slideUp"
  case SqueezeLeft = "squeezeLeft"
  case SqueezeRight = "squeezeRight"
  case SqueezeDown = "squeezeDown"
  case SqueezeUp = "squeezeUp"
  case FadeIn = "fadeIn"
  case FadeOut = "fadeOut"
  case FadeOutIn = "fadeOutIn"
  case FadeInLeft = "fadeInLeft"
  case FadeInRight = "fadeInRight"
  case FadeInDown = "fadeInDown"
  case FadeInUp = "fadeInUp"
  case ZoomIn = "zoomIn"
  case ZoomOut = "zoomOut"
  case Fall = "fall"
  case Shake = "shake"
  case Pop = "pop"
  case FlipX = "flipX"
  case FlipY = "flipY"
  case Morph = "morph"
  case Squeeze = "squeeze"
  case Flash = "flash"
  case Wobble = "wobble"
  case Swing = "swing"
}

enum SLocation:String {
  
  case screenTopMid
  case screenTopRight
  case screenTopLeft
  
  case screenleftTop
  case screenLeftMid
  case screenLeftBottom
  
  case screenRightTop
  case screenRightMid
  case screenRightBottom
  
  case screenBottomLeft
  case screenBottomMid
  case screenBottomRight
  
  case screenMid
  
  case screenLeftBottomCorner
  case screenRightTopCorner
  case screenRightBottomCorner
  case screenLeftTopCorner
  
  
  func getLocation() -> CGPoint {
    let width = Screen.width
    let height = Screen.height
    switch self {
    case .screenTopMid:
      return CGPoint(x: width/2, y: 0)
    case .screenTopLeft:
      return CGPoint(x: width/4, y: 0)
    case .screenTopRight:
      return CGPoint(x: width/4*3, y: 0)
      
    case .screenleftTop:
      return CGPoint(x:0, y: height/4)
    case .screenLeftMid:
      return CGPoint(x:0, y: height/2)
    case .screenLeftBottom:
      return CGPoint(x:0, y: height/4*3)
      
    case .screenRightMid:
      return CGPoint(x:width, y: height/2)
    case .screenRightTop:
      return CGPoint(x:width, y: height/4)
    case .screenRightBottom:
      return CGPoint(x:width, y: height/4*3)
      
    case .screenBottomMid:
      return CGPoint(x:width/2, y: height)
    case .screenBottomLeft:
      return CGPoint(x:width/4, y: height)
    case .screenBottomRight:
      return CGPoint(x:width/4*3, y: height)
      
    case .screenMid:
      return CGPoint(x:width/2, y: height/2)
      
    case .screenRightTopCorner:
      return CGPoint(x: width, y: 0)
    case .screenLeftTopCorner:
      return CGPoint(x: 0, y: 0)
    case .screenLeftBottomCorner:
      return CGPoint(x: 0, y: height)
    case .screenRightBottomCorner:
      return CGPoint(x: width, y: height)
    default:
        return CGPoint(x:width/2, y: height/2)
    }
  }
}

enum giftSenderAndReciver:String {
  case fromAbove
  case fromFront
  case toAbove
  case toFront
}

enum timeFunction:String{
  case accelerate
  case decelerate
  case declerateAccelerate
  case accelerateDeclerate
  case linear
  
  func getTF() -> CAMediaTimingFunction {
    switch self {
    case .accelerate:
      return CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
    case .decelerate:
       return CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
    case .accelerateDeclerate:
       return CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    case .declerateAccelerate:
     return CAMediaTimingFunction(controlPoints: 0.55, 0.055, 0.675, 0.19)
    case .linear:
      return CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    default:
      return CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    }
  }
}

struct animationSequences {
  var animations:[Any] = []
  mutating func add(animation:Any) {
    animations.append(animation)
  }
}

class gift:YYAnimatedImageView {
  //得到动画的图片或者gif
  //得到具体动画位置，路径
  //开始动画
  //如果是自己送的，然后广播
  var form:UIView
  var to:UIView
  var typeSting:String
  var animationType:String?
  var animationConfig:JSON?
  var animationControl:Int = 0
  var animationSequence:[Any] = [] {
    didSet{
      if animationSequence.count == 0 {
        DispatchQueue.main.async {
           self.layer.removeAllAnimations()
           self.removeFromSuperview()
        }
      }
    }
  }
  var pView:UIView?
  var repeatePlay = false
  var beginPoint:CGPoint = CGPoint(x: -10, y: -10)
  var midPoint:CGPoint = CGPoint(x: -10, y: -10)
  var endPoint:CGPoint = CGPoint(x: -10, y: -10)
  var defaultGif:String = ""
  var defaultGifConfig:[String:JSON]?
  var midGif = ""
  var midGifConfig:[String:JSON]?
  var lastGif = ""
  var lastGifConfig:[String:JSON]?
  var gameType:currentRoomType = .GAME
  var onePointRepeate = 3
  var currentPlayTime = 0
  var firstPathAnimation = false
  var secPathAnimation = false

  
  init(form:UIView,to:UIView,type:String,view:UIView,config:NSDictionary) {
    self.form = form
    self.to = to
    self.typeSting = type
    self.pView = view
    self.animationConfig = JSON.init(config)
    super.init(frame: CGRect(x: 100, y: 100, width: 300, height: 300))
  }
  
  static func getAnimationImage(form:UIView,to:UIView,type:String,view:UIView,config:NSDictionary) -> gift {
    let gg = gift.init(form: form, to: to, type: type, view: view, config: config)
    gg.form = form
    gg.to = to
    gg.typeSting = type
    gg.pView = view
    gg.autoPlayAnimatedImage = false
    gg.animationConfig = JSON.init(config)
    return gg
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  

  
  func getLocation(lc:giftSenderAndReciver) -> CGPoint {
    if pView == nil {
      pView! = UIView()
    }
    let formToView = form.superview?.convert(form.center, to: pView!) ?? CGPoint(x: 0, y: 0)
    let toToView =  to.superview?.convert(to.center, to: pView!) ?? CGPoint(x: 0, y: 0)
    switch lc {
    case .fromAbove:
      return formToView
    case .fromFront:
      if gameType == .GAME {
        var point = CGPoint(x: 0, y: 0)
        if let player = form as? PlayerView {
          if player.tag > 6 {
            point = CGPoint(x: formToView.x - form.width/2 - 15 - 20, y: (formToView.y))
          }else{
            //左边
            point = CGPoint(x:formToView.x + form.width/2 + 15 + 20, y: (formToView.y))
          }
          return point
        }
      }else{
        return formToView
      }
    case .toAbove:
      return  toToView
    case .toFront:
      if gameType == .GAME {
        var point = CGPoint(x: 0, y: 0)
        if let player = to as? PlayerView {
          if player.tag > 6 {
          //右边
            point = CGPoint(x: (toToView.x) - to.width/2 - 15 - 20, y: (toToView.y))
          }else{
            point = CGPoint(x:toToView.x + to.width/2 + 15 + 20, y: (toToView.y))
          }
        }
        return point
      }else{
        return  toToView
      }
    }
    return CGPoint(x: 0, y: 0)
  }
  
  func parserConfig(type:currentRoomType) {
    let configJson = animationConfig!
    self.gameType = type
    var start:CGPoint = CGPoint(x: 0, y: 0)
    var end:CGPoint = CGPoint(x: 0, y: 0)
    //动画类型
    self.defaultGifConfig = configJson["defaultIOSImage"].dictionaryValue
    self.animationType = configJson["type"].stringValue
    //是不是执行动画
    let reg = GiftSourceManager.getGifBy(url: self.defaultGifConfig!)
      //如果路径不存在就直接return
    if reg == nil {
      DispatchQueue.main.async {
        self.removeFromSuperview()
      }
      return
    }
      
    if configJson["type"].stringValue == "OnePointAnim"  {
      let first = configJson["point"].arrayValue.first
      if let firstAnim  = first!["anim"].dictionary {
        animationSequence.append(firstAnim)
      }else{
        animationSequence.append("YHfake")
      }
      if let startPoint = first!["location"].string {
        start = getPointLocation(point: startPoint)
        beginPoint = start
      }
      if animationSequence.count > 0 {
        executeAnimation()
      }
    }
    if configJson["type"].stringValue == "TwoPointAnim" {
      //TwoPointAnim
      //这个是两个点的动画，只有一个路径
      if configJson["point"].arrayValue.count != 2 {
        return
      }
      let first = configJson["point"].arrayValue.first
      let sec = configJson["point"].arrayValue.last
      let path = configJson["path"].arrayValue.first
      if let startPoint = first!["location"].string {
        start = getPointLocation(point: startPoint)
        beginPoint = start
      }
      //第一个点 1
      if let firstAnim  = first!["anim"].dictionary {
        if let gifName = firstAnim["imageIOS"]?.array{
          if let name = gifName.first?.string {
            self.defaultGif = name
          }
        }
        animationSequence.append(firstAnim)
      }else{
        animationSequence.append("YHfake")
      }
     //第一个路径 2
    animationSequence.append(gnerateTwoAnimation(fisrtAnimation:true, json:path!))
      //第二个点 3
      if let endP = sec!["location"].string {
        end = getPointLocation(point: endP)
        midPoint = end
      }
      if let secAnim = sec!["anim"].dictionary {
        if let gifName = secAnim["imageIOS"]?.array{
          if let name = gifName.first?.dictionary {
            self.midGifConfig = name
            let reg = GiftSourceManager.getGifBy(url: self.defaultGifConfig!)
              //如果路径不存在就直接return
              if reg == nil {
                DispatchQueue.main.async {
                  self.removeFromSuperview()
                }
                return
              }
          }
        }
        animationSequence.append(secAnim)
      }else{
        //如果没有动画，就填充一个默认
        animationSequence.append("YHfake")
      }
      executeAnimation()
    }
    if configJson["type"].stringValue == "ThreePointAnim" {
     if configJson["point"].arrayValue.count != 3 {
        return
     }
     if let first = configJson["point"].array?.first,
      let sec = configJson["point"].array?[1],
      let last = configJson["point"].array?.last,
      let path = configJson["path"].array?.first,
      let lastPath = configJson["path"].array?.last {
        if let startPoint = first["location"].string {
          start = getPointLocation(point: startPoint)
          beginPoint = start
        }
        midPoint = CGPoint(x: Screen.width/2, y: Screen.height/2)
        if let endP = last["location"].string {
          end = getPointLocation(point: endP)
          endPoint = end
        }
        //第一个点动画 1
        if let firstAnim  = first["anim"].dictionary {
          if let gifName = firstAnim["imageIOS"]?.array{
            if let name = gifName.first?.dictionary {
              self.defaultGifConfig = name
              let reg = GiftSourceManager.getGifBy(url: self.defaultGifConfig!)
                //如果路径不存在就直接return
                if reg == nil {
                  DispatchQueue.main.async {
                    self.removeFromSuperview()
                  }
                  return
                }
            }
          }
          animationSequence.append(firstAnim)
        }else{
          animationSequence.append("YHfake")
        }
         //路径动画 2
      animationSequence.append(gnerateTwoAnimation(fisrtAnimation:true, json:path))
        //第二个点动画 3
        if let secAnim = sec["anim"].dictionary {
          if let gifName = secAnim["imageIOS"]?.array{
            if let name = gifName.first?.dictionary {
              self.midGifConfig = name
              let reg = GiftSourceManager.getGifBy(url: self.defaultGifConfig!)
                //如果路径不存在就直接return
                if reg == nil {
                  DispatchQueue.main.async {
                    self.removeFromSuperview()
                  }
                  return
                }
            }
          }
          animationSequence.append(secAnim)
        }else{
          animationSequence.append("YHfake")
        }
        //第二个路径 4
        animationSequence.append(gnerateTwoAnimation(fisrtAnimation:false, json:lastPath))
        //最后点的动画 5
        if let lastAnim = last["anim"].dictionary {
          if let gifName = lastAnim["imageIOS"]?.array{
            if let name = gifName.first?.dictionary {
              self.lastGifConfig = name
              let reg = GiftSourceManager.getGifBy(url: self.defaultGifConfig!)
                //如果路径不存在就直接return
                if reg == nil {
                  DispatchQueue.main.async {
                    self.removeFromSuperview()
                  }
                  return
                }
            }
          }
          animationSequence.append(lastAnim)
        }else{
          animationSequence.append("YHfake")
        }
        executeAnimation()
      }
    }
  }
  
  func randomControlPoint() -> CGPoint {
    let w = Screen.width/2 - 60
    let h = Screen.height/2 - 60
    let originPoint = CGPoint(x: Screen.width/2 - w, y: Screen.height/2 - h)
    return  CGPoint(x: originPoint.x + CGFloat(arc4random_uniform(UInt32(2*w))), y: originPoint.y + CGFloat(arc4random_uniform(UInt32(2*h))))
  }
  
  func gnerateTwoAnimation(fisrtAnimation:Bool,json:JSON)  -> CAAnimationGroup? {
        var straightLine = false
        let duration = (json["duration"].int ?? 2000) / 1000
        if let anim = json["anim"].dictionary {
          if fisrtAnimation {
            firstPathAnimation = true
          }else{
            secPathAnimation = true
          }
          if let repeatNeed = anim["needRepeat"]?.bool {
            self.repeatePlay = repeatNeed
          }
        }
        var timefnc = "accelerate"
        var f = CGPoint(x: Screen.width / 2, y: Screen.height / 2)
        var t = CGPoint(x: Screen.width / 2, y: Screen.height / 2)
        if let form = json["from"].string {
          f = getPointLocation(point: form)
        }
        if let to = json["to"].string {
          t = getPointLocation(point: to)
          if to.hasBegin("screen"){
            //包含scrren说明时直线
//            straightLine = true
          }
        }
        if let tf = json["interpolator"].string {
          timefnc = tf
        }
        let timefuction = timeFunction.init(rawValue: timefnc)?.getTF()
        let pointEnd = t//to.superview?.convert(to.center, to: view)
        let pointBegin = f//form.superview?.convert(form.center, to: view)
        let path = UIBezierPath()
        path.move(to: pointBegin)
        if straightLine {
          path.addLine(to:pointEnd)
        }else{
          let control = randomControlPoint()
          path.addQuadCurve(to: pointEnd , controlPoint: control)
        }
        let position = CAKeyframeAnimation(keyPath: "position")
        position.path = path.cgPath
        position.duration = CFTimeInterval(duration)
//        position.isRemovedOnCompletion = false
//        position.fillMode = kCAFillModeForwards
        let fromScale = json["fromScale"].double ?? 1
        let toScale = json["toScale"].double ?? 1
        let scale = CAKeyframeAnimation(keyPath: "transform.scale")
        scale.values = [fromScale,toScale]
//        scale.fillMode = kCAFillModeForwards
//        scale.isRemovedOnCompletion = false
        scale.duration = CFTimeInterval(duration)
        let fadeAndScale = CAAnimationGroup()
        fadeAndScale.fillMode = kCAFillModeForwards
        fadeAndScale.isRemovedOnCompletion = false
        fadeAndScale.timingFunction = timefuction
        fadeAndScale.animations = [ position, scale]
        fadeAndScale.duration = CFTimeInterval(duration)
        return fadeAndScale
  }
  
  func totalAnmiationDispatch() {
    //第三个动画
    if animationControl == 0 {
      var shouldStartOnBegin = false
      var sacles = 1.0
      var width = 100.0
      let anim = animationSequence.first
      if let pointAni = anim as? [String:JSON] {
        shouldStartOnBegin = true
        if let sacle = pointAni["scale"]?.double {
          sacles = Double(sacle)
        }
        if let repeate =  pointAni["needRepeat"]?.bool {
          self.repeatePlay = repeate
        }
      }
      var gifImage:YYImage?
      if let path = GiftSourceManager.getGifBy(url: defaultGifConfig!){
        if let gift = YYImage(contentsOfFile: path) {
          gifImage = gift
        }else{
          animationSequence.removeAll()
          return
        }
      }else{
        animationSequence.removeAll()
        return
      }
      //这肯定是第一
      self.image = gifImage
      width = width * sacles
      self.frame = CGRect(x: width, y: width, width: width, height: Double((gifImage?.size.height)! / (gifImage?.size.width)! * CGFloat(width)))
      self.center = self.beginPoint
      if shouldStartOnBegin {
        self.startAnimating()
      }
      self.addObserver(self, forKeyPath:"currentAnimatedImageIndex", options:[.new,.old], context:nil)
      if shouldStartOnBegin == false {
        if animationSequence.count > 0 {
          animationSequence.remove(at: 0)
          animationControl = animationControl + 1
          executeAnimation()
        }
      }
    }else{
      //不是第一次
      //可能是第一个第二个路径
      //在1,3位置
      let anim = animationSequence.first
      if let animation = anim as? CAAnimationGroup {
        //说明是个动画
        animation.delegate = self
        self.layer.add(animation, forKey:"Animation\(animationControl)")
        if firstPathAnimation {
          self.startAnimating()
        }
        if secPathAnimation {
          self.startAnimating()
        }
        return
      }
      //这个是第第2第个4个
      if let pointAni = anim as? [String:JSON] {
        var gifImage:YYImage?
        if animationControl != 4 {
          //这个是中间的位置
          if midGifConfig != nil {
            if midGifConfig! == defaultGifConfig! {
              self.startAnimating()
              self.center = midPoint
            }else{
              //要重新生成frame
              if let path = GiftSourceManager.getGifBy(url: midGifConfig!){
                if let gift = YYImage(contentsOfFile: path) {
                  gifImage = gift
                }else{
                  animationSequence.removeAll()
                  return
                }
              }else{
                animationSequence.removeAll()
                return
              }
              self.image = gifImage
              self.startAnimating()
              self.center = midPoint
            }
            if let repeate =  pointAni["needRepeat"]?.bool {
              self.repeatePlay = repeate
            }
          }else{
            self.startAnimating()
            self.center = midPoint
          }
          //是不是循环播放
        }else{
          if lastGifConfig != nil {
            if lastGifConfig! == midGifConfig! {
              self.startAnimating()
              self.center = endPoint
            }else{
              //要重新生成frame
              //要重新生成gif
              if let path = GiftSourceManager.getGifBy(url: lastGifConfig!){
                if let gift = YYImage(contentsOfFile: path) {
                  gifImage = gift
                }else{
                  animationSequence.removeAll()
                  return
                }
              }else{
                animationSequence.removeAll()
                return
              }
              self.image = gifImage
              self.startAnimating()
              self.center = endPoint
            }
            if let repeate =  pointAni["needRepeat"]?.bool {
              self.repeatePlay = repeate
            }
          }else{
            self.startAnimating()
            self.center = endPoint
          }
        }
      }else{
        //说明时填充的，直接结束就行
        if animationSequence.count > 0 {
          animationSequence.remove(at: 0)
          animationControl = animationControl + 1
          if animationSequence.count == 0 {
            self.alpha = 0
            self.layer.removeAllAnimations()
          }else{
            executeAnimation()
          }
        }
      }
    }
  }
  
  func executeAnimation() {
    DispatchQueue.main.async {
      self.totalAnmiationDispatch()
    }
  }

  func getGiftWithName(name:String) -> String {
    return name
  }

  func getPointLocation(point:String) -> CGPoint{
    var real = CGPoint(x: 0, y: 0 )
    if let s =  giftSenderAndReciver(rawValue:point) {
      real = getLocation(lc: s)
    }else{
      if let ss = SLocation(rawValue:point) {
        real = ss.getLocation()
      }
    }
    return real
  }

  deinit {
    print("==============================================================")
    print("==============================================================")
    NotificationCenter.default.removeObserver(self)
    self.removeFromSuperview()
  }
}

extension gift:CAAnimationDelegate {
  
  func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
    
    if animationSequence.count > 0 {
      if animationSequence.count == 0 {
        self.center = endPoint
        self.layer.removeAllAnimations()
      }else{
        //删除上一个动画
            animationSequence.remove(at: 0)
            animationControl = animationControl + 1
            if animationSequence.count > 0 {
              executeAnimation()
            }else{
              self.alpha = 0
            }
        }
      }
    }
}
extension gift {
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    
    if keyPath == "currentAnimatedImageIndex" {
      let new = change?[NSKeyValueChangeKey.newKey] as! Int
      let old = change?[NSKeyValueChangeKey.oldKey] as! Int
      if new < old {
        if animationSequence.count > 0 {
          if repeatePlay == false {
            self.stopAnimating()
          }
          if animationControl % 2 == 0 {
            animationSequence.remove(at: 0)
            animationControl = animationControl + 1
            if animationSequence.count == 0 {
                self.alpha = 0
            }else{
              executeAnimation()
            }
         }
        }
      }
    }
  }
}
