//
//  RotationViewController.swift
//  game_werewolf
//
//  Created by Hang on 2018/2/1.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

class RotationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  override var shouldAutorotate: Bool {
     return false
  }

//  override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
//    return .portrait
//  }
//  override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
//    return .portrait
//  }
}
