//
//  XBHHUD.swift
//  interview
//
//  Created by QiaoYijie on 16/8/24.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation
import Alamofire
import SocketIO

class XBHHUD: NSObject {
  
  static var root = UIApplication.shared.keyWindow
  
  static var currentHUD = QBProgressHUD()
  
  static var hidenSuccess:(()->Void)? = nil
  
  static var isHiden = false
  
  //显示转圈
  class func showInView(_ view: UIView, mode: QBProgressHUDMode = .indeterminate) -> QBProgressHUD {
    
    currentHUD = QBProgressHUD.showAdded(to: view, animated: true)
    currentHUD.mode = mode
    currentHUD.backgroundView.style = QBProgressHUDBackgroundStyle.solidColor;
    currentHUD.backgroundView.color = UIColor(white: 0, alpha: 0.2)
    
    switch mode {
    case .indeterminate:
      currentHUD.label.text = NSLocalizedString("loading", comment: NSLocalizedString("loading - 加载中", comment: ""))
    default:
      currentHUD.label.text = ""
    }
    
    return currentHUD
  }
  
  //只显示文字（在底部）
  class func showTextOnly(text: String) {
    root = UIApplication.shared.keyWindow
    
    let hud = QBProgressHUD.showAdded(to: root!, animated: true)
    hud.mode = .text
    hud.label.text = text
    hud.label.numberOfLines = 0
    hud.offset = CGPoint(x: 0, y: QBProgressMaxOffset)
    hud.hide(animated: true, afterDelay: 1.5)
  }
  
  static func showTextOnlyTop(text:String) {
    
    root = UIApplication.shared.keyWindow
    let hud = QBProgressHUD.showAdded(to: root!, animated: true)
    hud.mode = .text
    hud.label.text = text
    hud.label.numberOfLines = 0
    hud.offset = CGPoint(x: 0, y: 120)
    hud.hide(animated: true, afterDelay: 2)
  }
  
  // 显示加载中
  class func showLoading(_ title: String = NSLocalizedString("loading...", comment: NSLocalizedString("loading - 加载中", comment: ""))) {
    Utils.runInMainThread {
      currentHUD.hide(animated: true)
      
      currentHUD = QBProgressHUD.showAdded(to: root!, animated: true)
      currentHUD.backgroundView.style = QBProgressHUDBackgroundStyle.solidColor
      currentHUD.backgroundView.color = UIColor(white: 0.0, alpha: 0.4)
      currentHUD.label.text = title
      currentHUD.animationType = .zoom
      currentHUD.mode = .customView
      self.isHiden = false
      
      let imageView = UIView()
      imageView.snp.makeConstraints { (make) in
        make.width.height.equalTo(36)
      }
      
      let layer = CAShapeLayer()
      layer.lineWidth = 2
      layer.fillColor = UIColor.clear.cgColor
      layer.strokeColor = UIColor.rgb(68, 68, 68).cgColor
      layer.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
      layer.lineCap = kCALineCapRound
      
      let path = UIBezierPath(arcCenter: CGPoint(x: 18, y: 18), radius: 18, startAngle: degreesToRadians(270), endAngle: degreesToRadians(180), clockwise: true)
      layer.path = path.cgPath
      imageView.layer.addSublayer(layer)
      
      let strokeEndAnimation = CAKeyframeAnimation(keyPath: "strokeEnd")
      strokeEndAnimation.duration = 0.5
      strokeEndAnimation.values = [0.0,1]
      strokeEndAnimation.keyTimes = [0.0,1]
      
      let rotaAni = CABasicAnimation(keyPath: "transform.rotation.z")
      rotaAni.fromValue = degreesToRadians(0)
      rotaAni.toValue = degreesToRadians(720)
      rotaAni.autoreverses = true
      
      let animation = CABasicAnimation(keyPath: "strokeEnd")
      animation.toValue = UIColor.black.cgColor
      
      let group = CAAnimationGroup()
      group.repeatCount = 1000
      group.duration = 2.0
      group.animations = [strokeEndAnimation, rotaAni, animation]
      
      layer.add(group, forKey: nil)
      
      currentHUD.customView = imageView
      
      var angleFlag: Double = 1.0
      
      UIView.animate(withDuration: 1, delay: 0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.curveLinear], animations: {
        imageView.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI / 180 * angleFlag))
      }, completion: { (flag) in
        angleFlag += 1
      })
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
        if self.isHiden == false {
          let superview = currentHUD.customView?.superview
          let button = UIButton()
          button.setImage(UIImage(named:"approval_icon_cancel"), for: .normal)
          button.addTarget(self, action: #selector(hiden), for: .touchDown)
          let superPoint = superview?.superview?.convert((superview?.frame.origin)!, to: superview?.superview)
          button.frame = CGRect(x: (superPoint?.x)! + (superview?.frame.width)! - 10, y: (superPoint?.y)! - 10, width: 20, height: 20)
          superview?.superview?.addSubview(button)
          superview?.superview?.bringSubview(toFront: button)
        }
      })
    }
  }
  
  static func hiden() {
    let sessionManager = Alamofire.SessionManager.default
    sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
      dataTasks.forEach { $0.cancel() }
      uploadTasks.forEach { $0.cancel() }
      downloadTasks.forEach { $0.cancel() }
    }
    //断开socket
    if SwiftConverter.shareInstance.isGaming() == false {
      messageDispatchManager.close()
      messageDispatchManager.socketManager.currentRetryTime = 1
      //这个时候就把信息reset
      if CurrentUser.shareInstance.inRoomStatus != .inRoom {
      CurrentUser.shareInstance.currentRoomID = ""
      CurrentUser.shareInstance.realCurrentRoomID = ""
      CurrentUser.shareInstance.from = ""
      CurrentUser.shareInstance.inRoomStatus = .none
      CurrentUser.shareInstance.loadingGame = false
      }
    }
  
    currentHUD.hide(animated: true)
    if self.hidenSuccess != nil {
      hidenSuccess!()
    }
    //执行后设置为空
    self.hidenSuccess == nil
  }
  
  static func showCanNotHidden(title:String) {
    Utils.runInMainThread {
      currentHUD.hide(animated: true)
      
      currentHUD = QBProgressHUD.showAdded(to: root!, animated: true)
      currentHUD.backgroundView.style = QBProgressHUDBackgroundStyle.solidColor
      currentHUD.backgroundView.color = UIColor(white: 0.0, alpha: 0.4)
      currentHUD.label.text = title
      currentHUD.animationType = .zoom
      currentHUD.mode = .customView
      
      let imageView = UIView()
      imageView.snp.makeConstraints { (make) in
        make.width.height.equalTo(36)
      }
      
      let layer = CAShapeLayer()
      layer.lineWidth = 2
      layer.fillColor = UIColor.clear.cgColor
      layer.strokeColor = UIColor.rgb(68, 68, 68).cgColor
      layer.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
      layer.lineCap = kCALineCapRound
      
      let path = UIBezierPath(arcCenter: CGPoint(x: 18, y: 18), radius: 18, startAngle: degreesToRadians(270), endAngle: degreesToRadians(180), clockwise: true)
      layer.path = path.cgPath
      imageView.layer.addSublayer(layer)
      
      let strokeEndAnimation = CAKeyframeAnimation(keyPath: "strokeEnd")
      strokeEndAnimation.duration = 0.5
      strokeEndAnimation.values = [0.0,1]
      strokeEndAnimation.keyTimes = [0.0,1]
      
      let rotaAni = CABasicAnimation(keyPath: "transform.rotation.z")
      rotaAni.fromValue = degreesToRadians(0)
      rotaAni.toValue = degreesToRadians(720)
      rotaAni.autoreverses = true
      
      let animation = CABasicAnimation(keyPath: "strokeEnd")
      animation.toValue = UIColor.black.cgColor
      
      let group = CAAnimationGroup()
      group.repeatCount = 1000
      group.duration = 2.0
      group.animations = [strokeEndAnimation, rotaAni, animation]
      
      layer.add(group, forKey: nil)
      
      currentHUD.customView = imageView
      
      var angleFlag: Double = 1.0
      
      UIView.animate(withDuration: 1, delay: 0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.curveLinear], animations: {
        imageView.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI / 180 * angleFlag))
      }, completion: { (flag) in
        angleFlag += 1
      })
    }
  }
  
  //成功
  class func showSuccess(_ title: String = NSLocalizedString("Success", comment: NSLocalizedString("Success - 成功", comment: ""))) {
    show(title, image: "success")
  }
  
  //失败
  class func showError(_ title: String = NSLocalizedString("Error", comment: NSLocalizedString("Error - 失败", comment: ""))) {
    XBHHUD.hide()
    show(title, image: "error")
  }
  
  fileprivate class func show(_ title: String, image: String) {
    
    Utils.runInMainThread {
      let hud = QBProgressHUD.showAdded(to: root!, animated: true)
      hud.mode = .customView
      let imageView = UIImageView(image: UIImage(named: image))
      
      hud.customView = imageView
      hud.backgroundView.style = QBProgressHUDBackgroundStyle.solidColor
      hud.backgroundView.color = UIColor(white: 0.0, alpha: 0.4)
      hud.label.text = title
      hud.label.numberOfLines = 0
      hud.animationType = .zoom
      
      hud.hide(animated: true, afterDelay: 2.0)
    }
    
    
  }
  
  //获取当前HUD
  fileprivate class func getCurrentHUD(_ view: UIView) -> QBProgressHUD {
    
    let hud = QBProgressHUD(for: view)
    return hud!
  }
  
  //隐藏
  class func hide() {
    DispatchQueue.main.async {
      self.isHiden = true
      currentHUD.hide(animated: true)
    }
  }
}
