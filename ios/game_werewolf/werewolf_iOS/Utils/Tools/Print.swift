//
//  Print.swift
//  interview
//
//  Created by QiaoYijie on 16/7/6.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import UIKit

class Print {
    /*
     TARGETS -> Build Setting -> custom flags -> Other Swift Flags -> DEBUG
     开发时添加 -D DEBUG
     上线前删除 -D DEBUG 注销print打印
     
     使用： Print.dlog(message: String)
     */
    class func dlog(_ message: String = "", file: String = #file, function: String = #function, lineNum: Int = #line) {
        #if DEBUG
            let date = Date(timeIntervalSinceNow: 0)
            let formater = DateFormatter()
            formater.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
            let dateString = formater.string(from: date)
            print("< \(dateString) \(NSURL(string: file)!.lastPathComponent),\(function),(\(lineNum)) > \(message)")
        #endif
    }
    
}
