//
//  gameAndMissionCenterViewController.swift
//  game_werewolf
//
//  Created by Hang on 2017/7/10.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import MJRefresh

class gameAndMissionCenterViewController: RotationViewController {
  
    var oWeb:UIWebView?
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "room_icon_leftarrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(gameAndMissionCenterViewController.leftClicked))
      self.oWeb = UIWebView()
      self.view.backgroundColor = UIColor.hexColor("EADBBD")
      self.oWeb?.backgroundColor = UIColor.hexColor("EADBBD")
      var weburl = Config.urlHost + "/task?access_token=" + "\(CurrentUser.shareInstance.token)"
      let request = URLRequest(url: URL(string:weburl)!)
      oWeb?.loadRequest(request)
      self.view.addSubview(oWeb!)
      self.oWeb?.snp.makeConstraints({ (make) in
        make.edges.equalToSuperview()
      })
      self.oWeb?.delegate = self
      self.oWeb?.scrollView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [weak self] in
        self?.oWeb?.reload()
        self?.oWeb?.scrollView.mj_header.endRefreshing()
      })
    }
  
    func leftClicked() {
      if (self.oWeb?.canGoBack)! {
        self.oWeb?.goBack()
      }else{
        self.dismiss(animated: true, completion: nil)
      }
    }

    override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    deinit {
      print("webView deinit")
    }
  

}

extension gameAndMissionCenterViewController:UIWebViewDelegate {

  
  func webViewDidStartLoad(_ webView: UIWebView) {
      XBHHUD.showLoading()
      XBHHUD.hidenSuccess = {
        self.leftClicked()
      }
  }
  
  func webViewDidFinishLoad(_ webView: UIWebView) {
      XBHHUD.hide()
      self.navigationItem.title = self.oWeb?.stringByEvaluatingJavaScript(from: "document.title")
  }
  
  func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
     XBHHUD.hide()
  }
  

}


