//
//  CurrentUser.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import SwiftyStoreKit

struct Playable {
  var nextrequestTime:TimeInterval
  var canNotPlay:Bool
}

enum gameRoomEnterStatus {
  case begin
  case inRoom
  case none
}

struct Family {
  var is_owner = false
  var group_id = ""
  var gid = ""
  var name = ""
  var short_name = ""
  var image = ""
  var lc_id = ""
  var member_count = 0
}

struct unCheckReceipt {
  var recept:String
  var productid:productInfo
}

class CurrentUser: NSObject {
  
  static let shareInstance = CurrentUser()
  fileprivate override init () {}
  enum VoiceGameState {
    case isStartState
    case isVotingState
    case isVotedState
    case isGameOverStates
    case isUpdateConfig
    case isNotAGame // 非游戏房间
    case isSpeechState
  }
  var isGameing: VoiceGameState = .isNotAGame
  var messageShow = true
  var id = "\(Int(arc4random() % 999 + 10))"
  var ownerType = ""
  //    var id = "987654321"
  var name = "qiaoyijie_yh_temp_fake_name"
  var gameType = 1    //  0 狼人杀 1 语音房间
  var groupID: String?
  //    var name = ""
  var avatar = "http://files.xiaobanhui.com/images/user_head_56a9aa12226bb.jpeg?t=1488866318"
  var level = 1
  var experience = 1
  var isMaster = false
  var ticket: String?
  var can_cut_speaker = false
  var roomOwnerId = ""
  var key1 = ""
  var key2 = ""
  // 自己是不是观战者
  var is_observer = false {
    didSet{
      if is_observer {
        CurrentUser.shareInstance.isMaster = false
      }
    }
  }
  // 当前玩家是否已经死亡
  var isDeath = false
  var currentPosition = -1
  var currentRoomType = ""   // simple
  var currentRole: GameRole = .none
  var currentRoomID = ""
  var currentRoomPassword = ""
  var currentControlRoomID = ""
  var token = ""
  var from = ""
  var familyInfo:Family?
  var playble:Playable = Playable(nextrequestTime: 0, canNotPlay: false) //是否可以玩游戏
  var sex = 1 // 1man   2woman
  var isTourist:Bool = false;
  var currentLang = Bundle.main.preferredLocalizations.count > 0 ?
    Bundle.main.preferredLocalizations[0] : "zh-Hans-CN"; //当前玩家的语言
  var realCurrentRoomID = ""  {
    didSet {
      print("DEINIT-YH")
      if realCurrentRoomID == "" {
        print("DEINIT-YH-1")
        currentRoomPassword = ""
        currentRoomType = ""
        cachedMessages.removeAll()
        loadingGame = false
        currentPosition = -1
        is_observer = false
        microGame = nil
      }
    }
  }//当前真实的房间号
  var unCheckReceipt:unCheckReceipt? //未验证的凭据
  // 本地缓存的卡牌数据
  var isNight = false
  var cachedCardData:[JSON] = [];
  var inRoomStatus:gameRoomEnterStatus = .none {
    didSet{
      if inRoomStatus == .inRoom {
        if cachedMessages.count > 0 {
          cachedMessages.forEach({ (dic) in
            for (key,value) in dic {
              NotificationCenter.default.post(name: Notification.Name.init(key), object: value)
            }
          })
        }
        cachedMessages.removeAll()
      }
    }
  }
  var loadingGame = false
  var cachedMessages:Array<Dictionary<String,JSON>> = Array()
  var microGame:[String:JSON]? //小游戏配置
  var isControlSocketServerGet:Bool = false
  
  func clearCurrentInformation() {
    currentRole = .none
    isDeath = false
    isNight = false
  }

  //家族信息获取
  func getFamilyInfo(sucess:@escaping (Family?)->()){
      RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id": self.id], success: {[weak self] (json) in
          if let group = json["group"].dictionary {
            var family = Family()
            if let is_owner = group["is_owner"]?.bool {
              family.is_owner = is_owner
            }
            if let group_id = group["group_id"]?.string {
              family.group_id = group_id
            }
            if let gid = group["gid"]?.string {
              family.gid = gid
            }
            if let name = group["name"]?.string {
              family.name = name
            }
            if let short_name = group["short_name"]?.string {
              family.short_name = short_name
            }
            if let image = group["image"]?.string {
              family.image = image
            }
            if let lc_id = group["lc_id"]?.string {
              family.lc_id = lc_id
            }
            if let member_count = group["member_count"]?.int {
              family.member_count = member_count
            }
            //设置家族信息
            self?.familyInfo = family
            if self?.familyInfo != nil {
              sucess((self?.familyInfo!)!)
            }else{
              sucess(nil)
            }
          }else{
            sucess(nil)
          }
        }, error:{ (code, message) in
          if self.familyInfo != nil {
            //不能用缓存
            //会导致问题，现在如果获取失败就直接返回nil
            sucess(nil)
          }else{
            sucess(nil)
          }
      })
  }
  
  func getUserCards(callback: ((_ data:[JSON]) -> Void)?) {
    RequestManager().post(
      url: RequestUrls.unusedCards,
      parameters: [:],
      success: { (response) in
        var cardDatas:[JSON] = [];
        if let dic = response["cards"].dictionary {
          for (key, value) in dic {
            if dic[key]?.dictionary?["count"]?.intValue != 0 {
              var tempDic = Dictionary<String,JSON>()
              tempDic["type"] = JSON.init(stringLiteral: key)
              tempDic[key] = value
              cardDatas.append(JSON.init(tempDic))
            }
          }
        }
        self.cachedCardData = cardDatas;
        callback?(cardDatas);
    }, error: { (code, message) in
      self.cachedCardData = [];
      callback?([]);
    });
  }
}
