//
//  BaseViewController.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
  
  var timerActionBlock: ((_ remainTime: Int) -> Void)?
  
  var timer = Timer()
  var remainTime = 0
  
  var liveTime = 0 {
    
    didSet {
      timer.invalidate()
      
      liveTime -= 1
      remainTime = liveTime
      self.timer = Timer.init(timeInterval: 1.0, target: self, selector: #selector(BaseNoticeView.tAction), userInfo: nil, repeats: true)
      RunLoop.current.add(self.timer, forMode: RunLoopMode.commonModes)
    }
  }
  
  final func tAction() {
    if timerActionBlock != nil {
      Utils.runInMainThread { [weak self] in
        self?.timerActionBlock!(self!.remainTime)
      }
    }
    
    if remainTime <= 0 {
      timer.invalidate()
    }
    
    remainTime -= 1
  }
  
  override var shouldAutorotate: Bool {
    return false
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
    return UIInterfaceOrientationMask.portrait
  }
  override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
    return UIInterfaceOrientation.portrait
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
    timer.invalidate()
  }
  
}
