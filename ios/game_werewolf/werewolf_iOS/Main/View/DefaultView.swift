//
//  DefaultView.swift
//  game_werewolf
//
//  Created by QiaoYijie on 2017/6/3.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class DefaultView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
  
  @IBOutlet weak var tipView: UILabel!
  
  class func view() -> DefaultView {
    let view = DefaultView.loadFromNib("DefaultView") as! DefaultView
    return view
  }
}
