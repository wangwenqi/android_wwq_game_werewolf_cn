//
//  AuthorizationView.swift
//  game_werewolf
//
//  Created by Hang on 2017/7/15.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit




class AuthorizationView: BaseNoticeView {
  
    let noUIPscope = PermissionScope()
    var allButton:[authorizeButton]? = []
    var shuoldRequestPermission:[authorizeButton]? = []

    @IBOutlet weak var bgview: UIView!
    @IBOutlet weak var MicrophoneView: UIView!
    @IBOutlet weak var remotepushView: UIView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var remotePush: authorizeButton!
    @IBOutlet weak var mediaTypeAudio: authorizeButton!
    @IBOutlet weak var location: authorizeButton!
    @IBOutlet weak var openAll: UIButton!
    @IBAction func openAll(_ sender: Any) {
      if UserDefaults.standard.bool(forKey: "PERMISSION_NOT_FIRST") == true {
        //跳转到设置界面
        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
        if #available(iOS 10.0, *) {
          UIApplication.shared.open(settingsUrl!, options: [ : ], completionHandler: { Success in
            
          })
        } else {
            UIApplication.shared.openURL(settingsUrl!)
        }
        Utils.hideNoticeView()
      }else{
        if shuoldRequestPermission?.count != 0 {
          let firstrequest = self.shuoldRequestPermission?.first
          let permission = (firstrequest?.type)!
          switch permission {
          case .microphone:
            noUIPscope.requestMicrophone()
          case .notifications:
            noUIPscope.requestNotifications()
          case .locationInUse:
            noUIPscope.requestLocationInUse()
          default:
            break
          }
        }else{
          // 所有的权限都已经开启了
          Utils.hideNoticeView()
          UserDefaults.standard.set(true, forKey: "PERMISSION_NOT_FIRST")
        }
      }
    }
    @IBAction func close(_ sender: Any) {
      Utils.hideNoticeView()
    }
  
  override func awakeFromNib() {
    
    //UI config
    self.bgview.layer.cornerRadius = 10
    self.bgview.layer.masksToBounds = true
    self.MicrophoneView.layer.borderWidth = 1.5
    self.MicrophoneView.layer.borderColor = UIColor.hexColor("AE78F1").cgColor
    self.MicrophoneView.layer.cornerRadius = 6
    self.MicrophoneView.layer.masksToBounds = true
    self.remotepushView.layer.borderWidth = 1.5
    self.remotepushView.layer.borderColor = UIColor.hexColor("AE78F1").cgColor
    self.remotepushView.layer.cornerRadius = 6
    self.remotepushView.layer.masksToBounds = true
    self.locationView.layer.borderWidth = 1.5
    self.locationView.layer.borderColor = UIColor.hexColor("AE78F1").cgColor
    self.locationView.layer.cornerRadius = 6
    self.locationView.layer.masksToBounds = true
    self.openAll.layer.cornerRadius = 10
    self.openAll.layer.masksToBounds = true
    self.closeButton.isHidden = true
      
    //function
    remotePush.type = .notifications
    mediaTypeAudio.type = .microphone
    location.type = .locationInUse
    allButton?.append(mediaTypeAudio)
    allButton?.append(remotePush)
    allButton?.append(location)
    noUIPscope.addPermission(NotificationsPermission(notificationCategories:nil), message: "notifications")
    noUIPscope.addPermission(MicrophonePermission(), message: "microphone")
    noUIPscope.addPermission(LocationWhileInUsePermission(), message: "地理位置")
    shuoldRequestPermission?.append(contentsOf: allButton!)
  
    //不是第一次
    if UserDefaults.standard.bool(forKey: "PERMISSION_NOT_FIRST") == true {
      //权限检查
      self.noUIPscope.configuredPermissions.forEach({ (pm) in
          noUIPscope.statusForPermission(pm.type, completion: { (staus) in
            let button = allButton?.first(where: {$0.type == pm.type})
            button!.status = staus
          })
      })
      self.closeButton.isHidden = false
      self.openAll.setTitle("去设置", for: .normal)
    }
    //如果是从旧版本升级上来的话，不是第一次打开，也有一些权限已经打开,所以也要设置一下
    self.noUIPscope.configuredPermissions.forEach({ (pm) in
      noUIPscope.statusForPermission(pm.type, completion: { (staus) in
        let button = allButton?.first(where: {$0.type == pm.type})
        button!.status = staus
        if staus != .unknown {
          if (self.shuoldRequestPermission?.contains(button!))! {
            let index = self.shuoldRequestPermission?.index(of: button!)
            self.shuoldRequestPermission?.remove(at: index!)
          }
        }
      })
    })
    
    noUIPscope.onAuthChange = {
      (finished, results) in
      print("auth change",finished,results)
      if UserDefaults.standard.object(forKey: "PERMISSION_NOT_FIRST") == nil {
        results.forEach({ (permission) in
          let changedButton = self.allButton?.first(where: { $0.type == permission.type })
          changedButton?.status = permission.status
          if permission.status != .unknown {
            if (self.shuoldRequestPermission?.contains(changedButton!))! {
              let index = self.shuoldRequestPermission?.index(of: changedButton!)
              self.shuoldRequestPermission?.remove(at: index!)
            }
          }
        })
        if self.shuoldRequestPermission?.count == 0 {
          // 说明已经请求完毕
          UserDefaults.standard.set(true, forKey: "PERMISSION_NOT_FIRST")
          UserDefaults.standard.synchronize()
          Utils.hideNoticeView()
        }else{
          //请求下一个权限
          let firstrequest = self.shuoldRequestPermission?.first
          let permission = (firstrequest?.type)!
          switch permission {
          case .microphone:
            self.noUIPscope.requestMicrophone()
          case .notifications:
            self.noUIPscope.requestNotifications()
          case .locationInUse:
            self.noUIPscope.requestLocationInUse()
          default:
            break
          }
        }
      }
    }
  }
}


