//
//  changeMaster.swift
//  game_werewolf
//
//  Created by Hang on 2017/10/13.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

enum viewnoticeType:String {
  case CHANGEMASTER
  case LEAVE
}

class changeMaster: BaseNoticeView {
  
    var type:viewnoticeType = .CHANGEMASTER
    @IBOutlet weak var textDuration: UILabel!
  
    @IBAction func refused(_ sender: Any) {
      messageDispatchManager.sendMessage(type:.reset_master_result, payLoad: ["allow":false])
      Utils.hideNoticeView()
    }
    
    @IBAction func confirm(_ sender: Any) {
       messageDispatchManager.sendMessage(type:.reset_master_result, payLoad: ["allow":true])
        Utils.hideNoticeView()
    }
    
    override func awakeFromNib() {
      self.backgroundColor = UIColor.hexColor("EDEDF3")
       setCenter()
    }
  
    override func timerAction(remainTime: Int) {
      textDuration.text = "\(remainTime)s"
      if remainTime == 0 {
        //同意更换房主
        messageDispatchManager.sendMessage(type:.reset_master_result, payLoad: ["allow":true])
        Utils.hideNoticeView()
      }
    }

  static func view() -> changeMaster {
      let view = Bundle.main.loadNibNamed("changeMaster", owner: nil, options: nil)?.last as! changeMaster
      view.frame = CGRect(x: 0, y: 0, width: Screen.width*0.8, height: 200)
      view.setCenter()
      return view
    }
}
