//
//  GameOverView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/23.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class GameOverView: BaseNoticeView {
  
  var isWin = false
  var winType = ""
  var peopleArr = [(String, JSON)]()
  var werewolfArr = [(String, JSON)]()
  var thirdPardArr = [(String, JSON)]()
  var currentAva:UIImage?
  var nativeAd: FBNativeAd?
  var experience:[String:JSON]?
  
  @IBOutlet weak var lightImage: UIImageView!
  @IBOutlet weak var contentView: XBHView!
  @IBOutlet weak var titleImage: UIImageView!
  @IBOutlet weak var experienceLabel: XBHLabel!
  @IBOutlet weak var conformButton: UIButton!

  var scrollview:gameOverScrolleView?
  
  static var scrollable = false
  
  class func view(people: [(String, JSON)], werewolf: [(String, JSON)], thirdPard: [(String, JSON)], selfIsWin: Bool, winType: String,userIcon:UIImage, _ showAd: Bool, _ nativeAds: FBNativeAd?,exp:[String:JSON]?) -> GameOverView {
    let view = GameOverView.loadFromNib("GameOverView") as! GameOverView
    view.nativeAd = nativeAds
    view.isWin = selfIsWin
    view.winType = winType
    view.peopleArr = people
    view.werewolfArr = werewolf
    view.thirdPardArr = thirdPard
    if exp != nil {
       view.experience = exp
    }

    var winArr = [(String, JSON)]()
    var loseArr1 = [(String, JSON)]()
    var loseArr2 = [(String, JSON)]()
    var loseName1 = ""
    var loseName2 = ""
    
    
    view.currentAva = userIcon
    
    if winType == "people" {
        winArr = people
        loseArr1 = werewolf
        loseArr2 = thirdPard
        loseName1 = "werewolf"
        loseName2 = "third_party"
    } else if winType == "werewolf" {
        winArr = werewolf
        loseArr1 = people
        loseArr2 = thirdPard
        loseName1 = "people"
        loseName2 = "third_party"
    } else {
        winArr = thirdPard
        loseArr1 = people
        loseArr2 = werewolf
        loseName1 = "people"
        loseName2 = "werewolf"
    }
    
    if people.count + werewolf.count + thirdPard.count >= 6 {
      //可以滑动
      scrollable = true
    }
    
    if selfIsWin {

      view.titleImage.image = UIImage.init(named: "popup_topbar_win")
      view.conformButton.setBackgroundImage(UIImage.init(named: "popup_btn_win"), for: .normal)
      view.contentView.borderColor = UIColor.hexColor("E24D31")
      view.lightImage.image = UIImage.init(named: "light_win")
      
      if CurrentUser.shareInstance.currentRoomType == "simple" || CurrentUser.shareInstance.currentRoomType == "pre_simple"{
        view.experienceLabel.text = NSLocalizedString("经验＋30", comment: "")
      } else {
        view.experienceLabel.text = NSLocalizedString("经验＋50", comment: "")
      }
      if exp != nil {
        if let exps = exp?["win"]?.int  {
          view.experienceLabel.text = NSLocalizedString("经验＋\(exps)", comment: "")
        }
      }
    } else {
      view.titleImage.image = UIImage.init(named: "popup_topbar_lose")
      view.conformButton.setBackgroundImage(UIImage.init(named: "popup_btn_lose"), for: .normal)
      view.contentView.borderColor = UIColor.hexColor("4337FF")
      view.lightImage.image = UIImage.init(named: "light_lose")
      view.experienceLabel.text = NSLocalizedString("经验＋10", comment: "")
      if exp != nil {
        if let exps = exp?["lose"]?.int  {
          view.experienceLabel.text = NSLocalizedString("经验＋\(exps)", comment: "")
        }
      }
    }
    
  
    
    var maxHeight: CGFloat = 185
    // 胜利阵营部分
    maxHeight = view.customGroup(winName: winType, groupDataArr: winArr, isWin: true, minHeight: maxHeight)
    // 失败阵营部分
    
    if loseArr1.count == 0 {
      maxHeight = view.customGroup(winName: loseName2, groupDataArr: loseArr2, isWin: false, minHeight: maxHeight)
    } else if loseArr2.count == 0 {
      maxHeight = view.customGroup(winName: loseName1, groupDataArr: loseArr1, isWin: false, minHeight: maxHeight)
    } else {
      maxHeight = view.customGroup(winName: loseName1, groupDataArr: loseArr1, isWin: false, minHeight: maxHeight)
      maxHeight = view.customGroup(winName: loseName2, groupDataArr: loseArr2, isWin: false, minHeight: maxHeight)
    }
    //添加分享部分
    
    maxHeight = view.makeSharePart(baseHeight: maxHeight, selfWin: selfIsWin, showAd: showAd)
    view.height =  (view.scrollview?.maxY)! + 40
    if view.height > Screen.bounds.height * 0.85 {
      view.height = Screen.bounds.height * 0.85
      view.scrollview?.frame = CGRect(x: 15, y: (view.scrollview?.baseHeight)!, width: view.frame.width - 30, height: Screen.bounds.height * 0.85 - 40 - (view.scrollview?.baseHeight)!)
    }
   
    view.setCenter()
    return view
  }
  
  func makeSharePart(baseHeight:CGFloat,selfWin:Bool, showAd: Bool) -> CGFloat{
    
    //线条
    var maxH = baseHeight + 8
    let insetMargin: CGFloat = 70 - 15
    let line = UIView.init(frame: CGRect.init(x: insetMargin, y: maxH, width: (scrollview?.width)! - insetMargin * 2, height: 1))
    line.backgroundColor = UIColor.hexColor("C99A79")
    
//    addSubview(line)
    addToParentView(view: line)
    
    let labelW: CGFloat = 100
    let labelH: CGFloat = 20
    
    // 标题
    let groupLabel = UILabel.init(frame: CGRect.init(x: ((scrollview?.width)! - labelW) * 0.5, y: line.minY - labelH * 0.5, width: labelW, height: labelH))
    groupLabel.backgroundColor = contentView.backgroundColor
    groupLabel.textAlignment = .center
    if selfWin {
      groupLabel.text = NSLocalizedString("炫耀一下", comment: "")
    }else{
       groupLabel.text = NSLocalizedString("喊人报仇", comment: "")
    }
    groupLabel.textColor = line.backgroundColor
    groupLabel.font = UIFont.systemFont(ofSize: 14)
//    addSubview(groupLabel)
    addToParentView(view: groupLabel)
    
    //加载分享view 分享图片生成
    var myava = self.currentAva
    let localSimple = NSLocalizedString("人简单局", comment: "")
    let view = Bundle.main.loadNibNamed("shareGame", owner: nil, options: nil)?.last as! shareGame
    view.setUpShare(with: selfWin ? RType.WIN : RType.LOSE, gametype:    peopleArr.count + werewolfArr.count + thirdPardArr.count == 12 ? NSLocalizedString("12人标准局", comment: "") : "\(peopleArr.count + werewolfArr.count + thirdPardArr.count)\(localSimple)", ava: myava)
    let share = Bundle.main.loadNibNamed("gameResultShare", owner: nil, options: nil)?.last as! gameResultShareMenu
    share.frame = CGRect(x: 15, y: maxH + 10 , width:(scrollview?.width)! - 30,height: 70)
    share.setUp()
    share.shareImage = Utils.screenShot(view: view)
    addToParentView(view: share)
    maxH = share.maxY
    
    if showAd {
    let adWidth = scrollview?.width ?? 200
    print(scrollview?.width)
    // 6 117.5 300 width 326
    // 6P 0 300 363
    // 5s 117 200 274
    let adHeight: CGFloat = adWidth < 350 ? 117 : 0
    let adView = AdCoinView.adStart(CGRect(x: 10, y: maxH, width: adWidth - 20, height: adWidth * 314 / 600 + adHeight), nativeAd)
    addToParentView(view: adView)
    adView.layoutIfNeeded()
      let needAdd: CGFloat = adWidth < 300 ? 200 : 300
    maxH = adView.maxY + needAdd * FinishTaskView.kScale
    }
    
    scrollview?.frame = CGRect(x: 15, y: (scrollview?.baseHeight)!, width: self.frame.width - 30, height: maxH)
     scrollview?.contentSize = CGSize(width: 0, height: (scrollview?.height)!)
    return maxH
    
  }
   
  func addToParentView(view:UIView) {
    if type(of: self).scrollable {
//      var frame = view.frame
//      frame.origin.x -= CGFloat(15)
//      view.frame = frame
      scrollview?.addSubview(view)
    }else{
      addSubview(view)
    }
  }
  
  func customGroup(winName:String, groupDataArr: [(String, JSON)], isWin: Bool, minHeight: CGFloat) -> CGFloat {
    
    var maxH = minHeight + 8
    if type(of:self).scrollable {
      if scrollview == nil {
        scrollview = gameOverScrolleView()
        scrollview?.baseHeight = minHeight
        scrollview?.frame = CGRect(x: 15, y: minHeight, width: self.frame.width - 30, height: 200)
        scrollview?.backgroundColor = UIColor.hexColor("fffcef")
        scrollview?.bounces = false
        maxH = 8
        addSubview(scrollview!)
        bringSubview(toFront: titleImage)
      }
    }
    

    let insetMargin: CGFloat = 70 - 15
    
    let line = UIView.init(frame: CGRect.init(x: insetMargin, y: maxH, width: (scrollview?.width)! - insetMargin * 2, height: 1))
    line.backgroundColor = UIColor.hexColor("C99A79")
    
    addToParentView(view: line)
//    addSubview(line)
    
    let labelW: CGFloat = 100
    let labelH: CGFloat = 20
    
    let groupLabel = UILabel.init(frame: CGRect.init(x: ((scrollview?.width)! - labelW) * 0.5, y: line.minY - labelH * 0.5, width: labelW, height: labelH))
    groupLabel.backgroundColor = contentView.backgroundColor
    groupLabel.textAlignment = .center
    if winName == "people" {
      groupLabel.text = NSLocalizedString("好人阵营", comment: "")
    } else if winName == "werewolf" {
      groupLabel.text = NSLocalizedString("狼人阵营", comment: "")
    } else {
      groupLabel.text = NSLocalizedString("情侣阵营", comment: "")
    }
    groupLabel.textColor = line.backgroundColor
    groupLabel.font = UIFont.systemFont(ofSize: 14)
     addToParentView(view: groupLabel)
//    addSubview(groupLabel)
    
    let imageWH: CGFloat = 25
    
    let image = UIImageView.init(frame: CGRect.init(x: line.minX - 10 - imageWH, y: line.minY - imageWH * 0.5, width: imageWH, height: imageWH))

    
    if isWin {
      image.image = UIImage.init(named: "popup_icon_win")
    } else {
      image.image = UIImage.init(named: "popup_icon_lose")
    }
//    addSubview(image)
    addToParentView(view: image)
    
    maxH = CGFloat(image.maxY)
    
    let outMargin: CGFloat = 50 - 15
    let margin: CGFloat = 6
    let maxColumn: Int = 4
    let iconWH = (((scrollview?.width)! - outMargin * 2) - (CGFloat(maxColumn) + 1) * margin) / CGFloat(maxColumn)
    let iconY: CGFloat = CGFloat(maxH)
    
    for (i, data) in groupDataArr.enumerated() {
      
      let lineNumber = i / maxColumn
      let columnNumber = i % maxColumn
      
      let icon = UIImageView()
      
      icon.image = UIImage.init(named: "room_head_default")
      if let imageUrl = URL.init(string: data.1["avatar"].stringValue) {
        icon.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named: "room_head_default"))
      }
      
      icon.frame = CGRect.init(x: outMargin + margin + CGFloat(columnNumber) * (iconWH + margin), y: iconY + margin + 3 + CGFloat(lineNumber) * (iconWH + margin + 20), width: iconWH, height: iconWH)
      icon.contentMode = .scaleAspectFill
      icon.clipsToBounds = true
      icon.layer.cornerRadius = iconWH * 0.5
//      addSubview(icon)g
      addToParentView(view: icon)
      
      let positionLabel = UILabel()
      positionLabel.textAlignment = .center
      positionLabel.backgroundColor = CustomColor.roomGreen
      positionLabel.textColor = UIColor.white
      
      if let position = Int(data.1["position"].stringValue) {
        positionLabel.text = "\(position + 1)"
        if CurrentUser.shareInstance.currentPosition == position + 1 {
          positionLabel.backgroundColor = CustomColor.roomYellow
        }
      }
      positionLabel.sizeToFit()
      positionLabel.font = UIFont.systemFont(ofSize: 13)
      positionLabel.frame = CGRect.init(x: icon.minX, y: icon.minY, width: iconWH * 0.3, height: iconWH * 0.3 + 3)
      positionLabel.layer.cornerRadius = 3
      positionLabel.clipsToBounds = true
//      addSubview(positionLabel)
      addToParentView(view: positionLabel)
      
      let nameLabel = UILabel()
      nameLabel.textAlignment = .center
      nameLabel.textColor = UIColor.darkGray
      nameLabel.text = data.1["name"].stringValue
      nameLabel.sizeToFit()
      nameLabel.font = UIFont.systemFont(ofSize: 11)
      nameLabel.frame = CGRect.init(x: icon.minX, y: icon.maxY, width: iconWH, height: 13)
      addToParentView(view: nameLabel)
//      addSubview(nameLabel)
      
      let roleLabel = UILabel()
      roleLabel.textAlignment = .center
      roleLabel.textColor = groupLabel.textColor
      roleLabel.text = GameRole.getRole(name: data.0).descriptionString
      roleLabel.sizeToFit()
      roleLabel.font = UIFont.systemFont(ofSize: 10)
      roleLabel.frame = CGRect.init(x: nameLabel.minX, y: nameLabel.maxY, width: iconWH, height: 13)
      addToParentView(view: roleLabel)
//      addSubview(roleLabel)
      
      maxH = roleLabel.maxY + margin
    }
    return maxH
  }
  
  
  
  @IBAction func conformClicked(_ sender: Any) {
    Utils.hideNoticeView()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.clear
    self.width = Screen.width * 0.95
    setCenter()
  }
  
}
