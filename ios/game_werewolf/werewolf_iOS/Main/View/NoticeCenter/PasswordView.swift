//
//  PasswordView.swift
//  game_werewolf
//
//  Created by QiaoYijie on 2017/4/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class PasswordView: BaseNoticeView, UITextFieldDelegate {
  
  @IBOutlet weak var passwordSwitch: UISwitch!
  @IBOutlet weak var passwordView: passwordImageView!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var passwordSwitchButton: UIButton!
  @IBOutlet weak var levelView: UIView!
  @IBOutlet weak var levelTitle: UILabel!
  @IBOutlet weak var content: UIView!
  @IBOutlet weak var setingPaddingTop: NSLayoutConstraint!
  @IBOutlet weak var passWord2Padding1: NSLayoutConstraint!
  @IBOutlet weak var passWord3Padding2: NSLayoutConstraint!
  @IBOutlet weak var password1: UILabel!
  @IBOutlet weak var password2: UILabel!
  @IBOutlet weak var password3: UILabel!
  @IBOutlet weak var password4: UILabel!
  @IBOutlet weak var passwordHeight: NSLayoutConstraint!
  @IBOutlet weak var passwordImageWidth: NSLayoutConstraint!
  @IBOutlet weak var chooseGodView: UIView!
  @IBOutlet weak var chooseEvilView: UIView!
  @IBOutlet weak var roleSelectdView: UIView!
  @IBOutlet weak var roleSeletedViewHeight: NSLayoutConstraint!
  @IBOutlet weak var chooseGodTitle: UILabel!
  @IBOutlet weak var chooseEvilTitle: UILabel!
  @IBOutlet weak var firstSelectedButton: UIButton!
  @IBOutlet weak var secSelectedButton: UIButton!
  @IBOutlet weak var thirdSelectedButton: UIButton!
  //默认类型
  let goodPeople = ["YNLS","YNLM"]
  let evelPeople = ["LLLW","LLLE"]
  //选择结果
  var goodPeoleRoles = "YNLS"
  var evelPeopleRoles = "LLLW"
  //需不需要显示人物选择
  var needShowChoosePeople:Bool?  {
    didSet{
      if needShowChoosePeople! {
        //设置人物
        if let config = configuration?["config"]["high_king"].dictionary {
          if let people = config["people_parts"]?.string {
              chooseGodTitle.text = getTitle(people)
               goodPeoleRoles = people
          }else{
              chooseGodTitle.text = getTitle("GD")//NSLocalizedString("预+巫+守+猎", comment: "")
          }
          if let evil = config["werewolf_parts"]?.string {
             chooseEvilTitle.text = getTitle(evil)
             evelPeopleRoles = evil
          }else{
             chooseEvilTitle.text = getTitle("ED")//NSLocalizedString("三狼+王", comment: "")
          }
        }else{
           chooseGodTitle.text =  getTitle("GD")//NSLocalizedString("预+巫+守+猎", comment: "")
           chooseEvilTitle.text = getTitle("ED")//NSLocalizedString("三狼+王", comment: "")
        }
      }
    }
  }
  //需不需要观战
  var needShowWatch = false
  //配置
  var configuration:JSON?
  //上次的选择
  var lastChooseBtn = 0
  var levelConfig = 0
  var levelLimits:[JSON]? {
    didSet{
      if levelLimits != nil {
       levelLocalLimits = generateContentBy(limits: levelLimits!)
      }else{
        levelLocalLimits = levelLimit
      }
    }
  }
  var levelLocalLimits:[String]?
  var min_level = 0
  lazy var levelLimit = [NSLocalizedString("无限制", comment: ""),NSLocalizedString("15级", comment: ""),NSLocalizedString("30级", comment: ""),NSLocalizedString("50级", comment: "")]
  var alertView:SpringView = SpringView()
    
  var password = "" {
    didSet {
      if password == "" {
        passwordSwitch.isOn = false
        passwordSwitchButton.isSelected = false
        hideTextField()
      } else {
         passwordSwitch.isOn = true
         passwordSwitchButton.isSelected = true
         showTextField()
         //给password赋值
        if password.length == 4 {
         password1.text = password.slicing(from: 0, length: 1)
         password2.text = password.slicing(from: 1, length: 1)
         password3.text = password.slicing(from: 2, length: 1)
         password4.text = password.slicing(from: 3, length: 1)
        }
      }
    }
  }
  
  @IBAction func confirmButtonClicked(_ sender: Any) {
    
    if passwordSwitchButton.isSelected {
      if passwordTextField.text?.characters.count != 4 {
        XBHHUD.showError(NSLocalizedString("密码必须为4位数字", comment: ""))
        return
      }
    }
    var para:[String:Any] = [String:Any]()
    if needShowChoosePeople! {
      para["people_parts"] = goodPeoleRoles
      para["werewolf_parts"] = evelPeopleRoles
      if passwordSwitchButton.isSelected {
          para["password"] =  passwordTextField.text ?? ""
      }else{
         para["password"] = ""
      }
    }else{
      if passwordSwitchButton.isSelected {
        para["password"] =  passwordTextField.text ?? ""
      }else{
        para["password"] = ""
      }
    }
    para["level"] = levelConfig
    messageDispatchManager.sendMessage(type: .update_config, payLoad: para)
    Utils.hideAnimated()
  }
  
  class func view(passWord: String,needShow:Bool,needWatch:Bool,configuration:JSON?) -> PasswordView {
    let view = PasswordView.loadFromNib("PasswordView") as! PasswordView
    view.passwordTextField.text = passWord
    view.configuration = configuration
    view.needShowChoosePeople = needShow
    view.needShowWatch = needWatch
    view.password = passWord
    view.config()
    return view
  }
  
  func config() {
    if needShowChoosePeople == false {
      roleSeletedViewHeight.constant = 0
      roleSelectdView.isHidden = true
    }
    if Screen.size.width > 320 {
      let padding = (self.passwordView.frame.width - 50 * 4) / 3
      passWord2Padding1.constant = padding
      passWord3Padding2.constant = padding
    }else{
       passwordHeight.constant = 65
       passwordImageWidth.constant = 45
       let padding = (self.passwordView.frame.width - 45 * 4) / 3
       passWord2Padding1.constant = padding
       passWord3Padding2.constant = padding
    }
    //notification
    NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
  }
  
  func keyBoardWillShow(notification: Notification) {
    let userInfo = notification.userInfo
    let keyBoardBounds = (userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    let resultPoint = CGPoint(x: self.center.x, y: (Screen.height - keyBoardBounds.height - 44)/2)
    self.center = resultPoint
  }
  
  func keyBoardWillHide(notification: Notification) {
    setCenter()
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    passwordTextField.resignFirstResponder()
  }
  
  func hideTextField() {
    if needShowChoosePeople!{
         height = 400
      }else{
         height = 280
      }
      setCenter()
      self.passwordView.isHidden = true
      passwordTextField.isHidden = true
      endEditing(true)
      layoutIfNeeded()
  }
    
  @IBAction func switchClicked(_ sender: Any) {
    let button = sender as? UIButton
    button?.isSelected = !(button?.isSelected)!
    if (button?.isSelected)! {
        showTextField()
    }else{
        hideTextField()
    }
    layoutIfNeeded()
  }
    
  func generateContentBy(limits:[JSON]) -> [String] {
    var result:[String] = []
    for value in limits {
      if let level = value.int {
        if level == 0 {
          let localTarget = NSLocalizedString("无限制", comment: "")
          result.append(localTarget)
        }else{
          let target = NSLocalizedString("级", comment: "")
          let localTarget = "\(level)\(target)"
          result.append(localTarget)
        }
      }
    }
    return result
  }
  
  func setLevel(level:Int,minLevel:Int) {
    if level > minLevel {
        self.levelTitle.text = getNewLevelText(levels: level)//    getLevelText(level: level)
      self.levelConfig = getlevelBy(text:self.levelTitle.text!)
    }else{
      self.levelTitle.text = NSLocalizedString("无限制", comment: "")
      self.levelConfig = 0
    }
  }
  
  func getNewLevelText(levels:Int) -> String {
    if levels == 0 {
       return NSLocalizedString("无限制", comment: "")
    }else{
        let target = NSLocalizedString("级", comment: "")
        return "\(levels)\(target)"
    }
  }
  
  func getLevelText(level:Int) -> String {
    levelConfig = level
    switch level {
    case 15:
      return NSLocalizedString("15级", comment: "")
    case 0:
      return NSLocalizedString("无限制", comment: "")
    case 30:
      return NSLocalizedString("30级", comment: "")
    case 50:
      return NSLocalizedString("50级", comment: "")
    default:
      return NSLocalizedString("无限制", comment: "")
    }
  }
  
  func getTitle(_ str:String) -> String {
    switch str {
    case "YNLS":
      return NSLocalizedString("预+巫+守+猎", comment: "")
    case "YNLM":
      return NSLocalizedString("预+巫+魔术师+猎", comment: "")
    case "LLLW":
      return NSLocalizedString("三狼+王", comment: "")
    case "LLLE":
      return NSLocalizedString("三狼+恶魔", comment: "")
    case "ED":
      return NSLocalizedString("三狼+王", comment: "")
    case "GD":
      return NSLocalizedString("预+巫+守+猎", comment: "")
    default:
      return NSLocalizedString("错误", comment:"")
    }
  }
  
  func getTypeByContent(_ str:String) -> String {
    switch str {
    case NSLocalizedString("预+巫+守+猎", comment: ""):
      return "YNLS"
    case NSLocalizedString("预+巫+魔术师+猎", comment: ""):
      return "YNLM"
    case NSLocalizedString("三狼+王", comment: ""):
      return "LLLW"
    case NSLocalizedString("三狼+恶魔", comment: ""):
      return "LLLE"
    default:
      return "FAIL"
    }
  }
  
  func showTextField() {
    if needShowChoosePeople!{
        height = 500
      }else{
        height = 370
      }
      setCenter()
      self.passwordView.isHidden = false
      passwordTextField.isHidden = false
      passwordTextField.becomeFirstResponder()
      layoutIfNeeded()
  }
  
  func handleiPhone5() {
    var rect = self.frame
    rect.origin = CGPoint(x: rect.origin.x, y: 20)
    self.frame = rect
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
  }
    @IBAction func switchChanged(_ sender: Any) {
      print("\(passwordSwitch.isOn)")
      if passwordSwitch.isOn {
        showTextField()
      } else {
        hideTextField()
      }
      layoutIfNeeded()
    }

  
    @IBAction func chooseLevel(_ sender: Any) {
      if let button = sender as? UIButton {
        if self.roleSelectdView.subviews.contains(alertView) {
            if lastChooseBtn == button.tag {
            alertView.animation = "fadeOut"
            alertView.animateNext {
              for view in self.alertView.subviews {
                view.removeFromSuperview()
              }
              self.alertView.removeFromSuperview()
            }
            lastChooseBtn = 0
            return
          }
        }
        if self.content.subviews.contains(alertView) {
            if lastChooseBtn == button.tag {
              alertView.animation = "fadeOut"
              alertView.animateNext {
                for view in self.alertView.subviews {
                  view.removeFromSuperview()
                }
                self.alertView.removeFromSuperview()
              }
              lastChooseBtn = 0
              return
          }
        }
        if button.tag == 22 {
            levelLimitView()
          }else if button.tag == 21 {
            eveilChooseView()
          }else {
            godChooseView()
          }
          lastChooseBtn = button.tag
      }
   }
  
   //选择魔鬼
   func eveilChooseView() {
    for view in self.alertView.subviews {
      view.removeFromSuperview()
    }
    self.alertView.removeFromSuperview()
    let copys = evelPeople
    let show = copys.filter({getTitle($0) != self.chooseEvilTitle?.text})
      let begeinY = self.roleSelectdView.frame.maxY
      let width = self.chooseEvilView.width
      let beginX = self.chooseEvilView.frame.origin.x
      alertView.frame = CGRect(x: beginX, y: begeinY - 10, width: width, height: 35)
      for text in show {
        let btn = UIButton()
        let index = show.index(of: text)
        if index != nil {
          btn.tag = 200+index!
        }
        btn.frame = CGRect(x: 0, y: 10 + show.index(of:text)! * 25, width: Int(width), height: 25)
        btn.setTitle(getTitle(text), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightHeavy)
        btn.setTitleColor(UIColor.hexColor("745FFF"), for: .normal)
        let gap = UIView()
        btn.addTarget(self, action: #selector(handleChoose(sender:)), for: .touchUpInside)
        gap.frame = CGRect(x: btn.minX, y: btn.maxY, width: width, height: 1)
        gap.backgroundColor = UIColor.hexColor("7A40FB")
        alertView.addSubview(btn)
        alertView.addSubview(gap)
      }
      alertView.backgroundColor = UIColor.hexColor("D3CEFF")
      alertView.layer.cornerRadius = 3
      alertView.layer.borderColor = UIColor.hexColor("7A40FB").cgColor
      alertView.layer.borderWidth = 2
      alertView.layer.masksToBounds = true
//      self.content.addSubview(alertView)
      self.content.insertSubview(alertView, at: 3)
//      self.content.sendSubview(toBack: alertView)
      alertView.animation = "morph"
      alertView.animate()
   }
   //选择神
   func godChooseView() {
      for view in self.alertView.subviews {
        view.removeFromSuperview()
      }
      self.alertView.removeFromSuperview()
      let copys = goodPeople
      let show = copys.filter({getTitle($0) != self.chooseGodTitle.text})
      let begeinY = self.chooseGodView.frame.maxY
      let width = self.chooseGodView.width
      let beginX = self.chooseGodView.frame.origin.x
      alertView.frame = CGRect(x: beginX, y: begeinY - 10, width: width, height:35)
      for text in show {
        let btn = UIButton()
        let index = show.index(of: text)
        if index != nil {
          btn.tag = 100+index!
        }
        btn.frame = CGRect(x: 0, y: 10 + show.index(of:text)! * 25, width: Int(width), height: 25)
        btn.setTitle(getTitle(text), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightHeavy)
        btn.setTitleColor(UIColor.hexColor("745FFF"), for: .normal)
        let gap = UIView()
        btn.addTarget(self, action: #selector(handleChoose(sender:)), for: .touchUpInside)
        gap.frame = CGRect(x: btn.minX, y: btn.maxY, width: width, height: 1)
        gap.backgroundColor = UIColor.hexColor("7A40FB")
        alertView.addSubview(btn)
        alertView.addSubview(gap)
      }
      alertView.backgroundColor = UIColor.hexColor("D3CEFF")
      alertView.layer.cornerRadius = 3
      alertView.layer.borderColor = UIColor.hexColor("7A40FB").cgColor
      alertView.layer.borderWidth = 2
      alertView.layer.masksToBounds = true
      self.roleSelectdView.addSubview(alertView)
      self.roleSelectdView.bringSubview(toFront: self.chooseGodView)
      alertView.animation = "morph"
      alertView.animate()
   }

   func levelLimitView() {
      //已经存在就不再重新显示
    for view in self.alertView.subviews {
      view.removeFromSuperview()
    }
    self.alertView.removeFromSuperview()
      let copys = levelLocalLimits
      if let show = copys?.filter({$0 != self.levelTitle?.text}) {
        let begeinY = self.levelView.frame.maxY
        let width = self.levelView.width
        let beginX = self.levelView.frame.origin.x
        alertView.frame = CGRect(x: beginX, y: begeinY - 10, width: width, height:85)
        for text in show {
          let btn = UIButton()
          let index = show.index(of: text)
          if index != nil {
            btn.tag = 300+index!
          }
          btn.frame = CGRect(x: 0, y: 10 + show.index(of:text)! * 25, width: Int(width), height: 25)
          btn.setTitle(text, for: .normal)
          btn.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightHeavy)
          btn.setTitleColor(UIColor.hexColor("745FFF"), for: .normal)
          let gap = UIView()
          btn.addTarget(self, action: #selector(handleChoose(sender:)), for: .touchUpInside)
          gap.frame = CGRect(x: btn.minX, y: btn.maxY, width: width, height: 1)
          gap.backgroundColor = UIColor.hexColor("7A40FB")
          alertView.addSubview(btn)
          alertView.addSubview(gap)
        }
        alertView.backgroundColor = UIColor.hexColor("D3CEFF")
        alertView.layer.cornerRadius = 3
        alertView.layer.borderColor = UIColor.hexColor("7A40FB").cgColor
        alertView.layer.borderWidth = 2
        alertView.layer.masksToBounds = true
        self.content.addSubview(alertView)
        self.content.bringSubview(toFront: self.levelView)
        alertView.animation = "morph"
        alertView.animate()
      }
    }
      
    func handleChoose(sender:UIButton) {
      let tag = sender.tag
      if tag >= 300 {
        let text = sender.titleLabel?.text
        self.levelConfig = getlevelBy(text:text!)
        self.levelTitle.text = text
        alertView.animation = "fadeOut"
        alertView.animateNext {
          for view in self.alertView.subviews {
            view.removeFromSuperview()
          }
          self.alertView.removeFromSuperview()
        }
      }
      if tag >= 200 && tag < 300 {
        let text = sender.titleLabel?.text
        self.evelPeopleRoles = getTypeByContent(text!)
        self.chooseEvilTitle.text = text
        alertView.animation = "fadeOut"
        alertView.animateNext {
          for view in self.alertView.subviews {
            view.removeFromSuperview()
          }
          self.alertView.removeFromSuperview()
        }
      }
      if tag >= 100 && tag < 200 {
        let text = sender.titleLabel?.text
        self.goodPeoleRoles = getTypeByContent(text!)
        self.chooseGodTitle.text = text
        alertView.animation = "fadeOut"
        alertView.animateNext {
          for view in self.alertView.subviews {
            view.removeFromSuperview()
          }
          self.alertView.removeFromSuperview()
        }
      }
    }
  
  func getType(title:String) -> String {
    switch title {
    case NSLocalizedString("无限制", comment: ""):
      return ""
    case NSLocalizedString("15级", comment: ""):
      return ""
    case NSLocalizedString("30级", comment: ""):
      return ""
    case NSLocalizedString("50级", comment: ""):
      return ""
    default:
      return "2"
    }
  }
  
  func getlevelBy(text:String) -> Int {
    var levelString = text
    if levelString == NSLocalizedString("无限制", comment: "") {
      return 0
    }else{
      if CurrentUser.shareInstance.currentLang == "ja" {
        if let level = Int(levelString.dropLast(3)) {
          return level
        }
      }else{
        if let level = Int(levelString.dropLast()) {
          return level
        }
      }
    }
    return 0
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    passwordTextField.delegate = self
    levelView.layer.borderColor = UIColor.hexColor("7A40FB").cgColor
    levelView.layer.borderWidth = 2
    chooseGodView.layer.borderColor = UIColor.hexColor("7A40FB").cgColor
    chooseGodView.layer.borderWidth = 2
    chooseEvilView.layer.borderColor = UIColor.hexColor("7A40FB").cgColor
    chooseEvilView.layer.borderWidth = 2
    passwordView.clicked = { [weak self] in
      self?.passwordTextField.becomeFirstResponder()
    }
    firstSelectedButton.imageView?.contentMode = .scaleAspectFit
    secSelectedButton.imageView?.contentMode = .scaleAspectFit
    thirdSelectedButton.imageView?.contentMode = .scaleAspectFit
  }
  // Fix 房间密码只能输入4位
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    switch range.location {
    case 0:
      password1.text = string
    case 1:
      password2.text = string
    case 2:
      password3.text = string
    case 3:
      password4.text = string
    default:
      break
    }
    let textLength = textField.text?.length ?? 0
    if range.length + range.location > textLength {
      return false
    }
    let newLength = textLength + string.length - range.length
    return newLength <= 4
  }

}
