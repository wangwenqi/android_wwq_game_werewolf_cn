//
//  canNotPlayNotice.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/2.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class canNotPlayNotice: BaseNoticeView {

    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var confirmButton: UIButton!
    
    override func awakeFromNib() {
      
      self.backgroundColor = UIColor.hexColor("B9C4E4")
      setCenter()
    }
    
    @IBAction func confirm(_ sender: Any) {
        Utils.hideNoticeView()
    }
    override func layoutSubviews() {
//      self.textview.text = NSLocalizedString("尊敬的用户您好： 因用户多次举报您违反终极狼人杀社区友好愉快玩耍规定，现终极狼人杀暂时禁玩您的账号，如有疑问请联系suppoet@orangelab.cn", comment: "")
      let text = self.textview.text
      let he = text?.height(withConstrainedWidth: self.textview.frame.width, font: UIFont.boldSystemFont(ofSize: 14))
      self.height = 61 + he! + 80
      setCenter()
    }
}

