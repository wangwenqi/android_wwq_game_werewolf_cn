//
//  payFail.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/11.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import SwiftyStoreKit

class payFail: BaseNoticeView{
    
   @IBOutlet weak var textview: UITextView!
   @IBOutlet weak var retry: UIButton!
   @IBOutlet weak var retryLater: UIButton!
   @IBOutlet weak var title: UILabel!
  
  
    var text:String?{
      didSet{
//        self.textview.text = text
//        self.title.text = NSLocalizedString("提示", comment: "")
//        self.retry.setTitle(NSLocalizedString("确定", comment: ""), for: .normal)
//        self.retryLater.isHidden = true
//        self.trailing.constant = 20
//        self.layoutSubviews()
      }
    }
  
   override func layoutSubviews() {
    
    let text = self.textview.text
    let he = text?.height(withConstrainedWidth: self.textview.frame.width, font: UIFont.boldSystemFont(ofSize: 14))
    self.height = 56 + he! + 70
    setCenter()
   }
  
    @IBAction func retryClick(_ sender: Any) {
    
      let receipt = CurrentUser.shareInstance.unCheckReceipt?.recept
      let purchase = CurrentUser.shareInstance.unCheckReceipt?.productid
      Utils.hideNoticeView()
      XBHHUD.showCanNotHidden(title:NSLocalizedString("正在验证购买，请耐心等待", comment: ""))
      RequestManager().post(url: RequestUrls.iap, parameters:["receipt":receipt], success: { (json) in
        //把返回值刷成ReceiptInfo
        var sucess = false
        if json.dictionary != nil {
          let info = json.dictionaryValue["purchase"]?.arrayValue
          info?.forEach({ (item) in
            let receiptItem = item.dictionaryValue//ReceiptItem(receiptInfo: item)
            if receiptItem["productId"]?.stringValue == purchase?.productId {
              if receiptItem["op_result"]?.intValue == 1000 ||  receiptItem["op_result"]?.intValue == 2000 {
                  SwiftyStoreKit.finishTransaction((purchase?.transaction)!)
                  CurrentUser.shareInstance.unCheckReceipt = nil
                  sucess = true
              }
            }
          })
          //显示成功
          if sucess {
            XBHHUD.hide()
            XBHHUD.showSuccess(NSLocalizedString("购买成功", comment: ""))
            let noti = Notification.Name(rawValue: "PAYSS")
            NotificationCenter.default.post(name: noti, object: nil)
          }else{
            let payFail = Bundle.main.loadNibNamed("payFail", owner: nil, options: nil)?.last as! payFail
            payFail.backgroundColor = UIColor.hexColor("E3DDEA")
            payFail.textview.backgroundColor = UIColor.hexColor("E3DDEA")
            Utils.showNoticeView(view: payFail)
          }
        }
      }) { (code, message) in
        XBHHUD.hide()
        let localerror = NSLocalizedString("验证失败:", comment: "")
        XBHHUD.showError("\(localerror)\(message)")
      }
    }
    
    @IBAction func retryLaterClick(_ sender: Any) {
      Utils.hideNoticeView()
    }
}
