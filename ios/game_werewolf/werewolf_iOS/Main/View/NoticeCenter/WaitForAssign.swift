//
//  WaitForAssign.swift
//  game_werewolf
//
//  Created by QiaoYijie on 2017/3/29.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class WaitForAssign: BaseNoticeView {
  @IBOutlet weak var titleLabel: LTMorphingLabel!
  
  @IBOutlet weak var contentImage: UIImageView!
  class func view() -> WaitForAssign {
    let view = WaitForAssign.loadFromNib("WaitForAssign") as! WaitForAssign
    return view
  }
  
  override func timerAction(remainTime: Int) {
    titleLabel.text = NSLocalizedString("分配角色中(\(remainTime)s)", comment: "")
    if remainTime == 0 {
      Utils.hideNoticeView()
    }
  }
  
  override func awakeFromNib() {
    self.backgroundColor = UIColor.clear
    titleLabel.morphingEffect = .evaporate
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    height = 300
    setCenter()
  }
}
