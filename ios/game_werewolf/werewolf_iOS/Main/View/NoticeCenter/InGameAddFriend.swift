//
//  InGameAddFriend.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/22.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

struct addFriendRequest {
  var id:String
  var avaImage:String
  var name:String
}

protocol addFriendRequestDelegate : class {
  func remove()
  func removeAllTheSame()
}

extension addFriendRequestDelegate {
  func removeAllTheSame() {
  }
}

class InGameAddFriend: SpringView {

    @IBOutlet weak var ava: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var refusedButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var backview: UIView!
  
    var id:String?
  
    weak var delegate:addFriendRequestDelegate?
  
    override func awakeFromNib() {
        self.ava.layer.cornerRadius = 30
        self.ava.layer.masksToBounds = true
        self.ava.layer.borderWidth = 2
        self.backview.layer.cornerRadius = 20
        self.backview.layer.masksToBounds = true
        self.backview.layer.borderColor = UIColor.hexColor("ffe727").cgColor
        self.backview.layer.borderWidth = 2
        self.ava.layer.borderColor = UIColor.hexColor("ffe727").cgColor
    }
  
  func render(request:addFriendRequest) {
      let url = URL(string: request.avaImage)
      self.id = request.id
      self.ava.sd_setImage(with: url, placeholderImage:UIImage.init(named: "room_head_default"))
      self.title.text = "\(request.name)"
  }
  
  @IBAction func refusedClicked(_ sender: Any) {
    self.animation = "fall"
    self.animateNext {
      self.delegate?.remove()
      self.removeFromSuperview()
    }
    RequestManager().post(url: RequestUrls.rejectFriend, parameters: ["friend_id":id!], success: { (json) in
        XBHHUD.showTextOnly(text: NSLocalizedString("拒绝好友请求", comment: ""))
  
    }) { (code, message) in
      
    }
  }
  
  deinit {
    print("=======================")
  }
  
  @IBAction func confirmClicked(_ sender: Any) {
    self.animation = "fall"
    self.animateNext {
      self.delegate?.remove()
      self.removeFromSuperview()
    }
    RequestManager().post(url: RequestUrls.acceptFriend, parameters: ["friend_id":id!], success: { [weak self] (json) in
      XBHHUD.showTextOnly(text: NSLocalizedString("好友添加成功", comment: ""))
      self?.tellRN(id:(self?.id)!)
    }) { (code, message) in
    }
  }
  
  func tellRN(id:String) {
    let data:Dictionary<String,Any> = ["action":"ACTION_ADD_FRIEND","params":["user_id":id],"options":["needcallback":false,"sync":true]]
    let json = JSON.init(data)
    SwiftConverter.shareInstance.sendMessagesToRN(json: json)
  }
}
