//
//  GameOverCollectionViewCell.swift
//  game_werewolf
//
//  Created by Hang on 2017/12/7.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class GameOverCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var avaImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var number: UILabel!
  
    func config(_ player:(String, JSON)) {
      avaImage.image = UIImage.init(named: "room_head_default")
      if let imageUrl = URL.init(string: player.1["avatar"].stringValue) {
        avaImage.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named: "room_head_default"))
      }
      avaImage.layer.cornerRadius = (((Screen.width - 10) - 7 * 10)/8)/2//20//avaImage.size.width/2
      avaImage.layer.masksToBounds = true
      avaImage.layer.borderColor = UIColor.hexColor("6E2EFF").cgColor
      avaImage.layer.borderWidth = 2
      if let position = Int(player.1["position"].stringValue) {
        number.text = "\(position + 1)"
        if CurrentUser.shareInstance.currentPosition == position + 1 {
          number.backgroundColor = CustomColor.newRoomYellow
          number.textColor = UIColor.darkGray
          avaImage.layer.borderColor = CustomColor.newRoomYellow.cgColor
        }else{
          number.backgroundColor = CustomColor.roomPink
          number.textColor = UIColor.white
        }
      }
      number.layer.cornerRadius = 7
      number.layer.masksToBounds = true
      name.text = player.1["name"].stringValue
      role.text = GameRole.getRole(name: player.0).descriptionString
    }
}
