//
//  NormalAlertView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/26.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class NormalAlertView: BaseNoticeView {
  
  enum ButtonType {
    case oneButton
    case twoButton
    case unActiveAlert
    case delayTouchable
  }
  
  var conformClickBlock: (() -> Void)?
  var cancelClickBlock: ((_ timeOver: Bool) -> Void)?
  
  var type: ButtonType = .oneButton {
    didSet {
      if type == .oneButton {
        centerConformButton.isHidden = false
        cancelButton.isHidden = true
        confirmButton.isHidden = true
      } else if type == .twoButton || type == .delayTouchable {
        centerConformButton.isHidden = true
        confirmButton.isEnabled = type == .delayTouchable ? false : true
        cancelButton.isHidden = false
        confirmButton.isHidden = false
      } else if type == .unActiveAlert {
        centerConformButton.isHidden = true
        cancelButton.isHidden = false
        confirmButton.isHidden = false
        cancelButton.setTitle("离开", for: .normal)
        confirmButton.setTitle("继续玩", for: .normal)
      }
    }
  }
  
  @IBOutlet weak var cancelButton: UIButton!
  @IBOutlet weak var confirmButton: UIButton!
  @IBOutlet weak var centerConformButton: UIButton!
  @IBOutlet weak var titleLabel: LTMorphingLabel!
  var defaulsString = ""
  
  var detailLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .center
    label.textColor = CustomColor.newTextColor
    label.font = UIFont.systemFont(ofSize: 14)
    label.numberOfLines = 0
    return label
  }()
  
  @IBAction func cancelButtonClicked(_ sender: Any) {
    Utils.hideAnimated()
    if cancelClickBlock != nil {
      cancelClickBlock!(false)
    }
  }
  
  @IBAction func conformClicked(_ sender: Any) {
    conform()
  }
  
  @IBAction func centerConformButtonClicked(_ sender: Any) {
    conform()
  }
  
  func conform() {
    Utils.hideAnimated()
    if conformClickBlock != nil {
      conformClickBlock!()
    }
  }
  
  class func view(type: ButtonType, title: String, detail: String) -> NormalAlertView {
    let view = NormalAlertView.loadFromNib("NormalAlertView") as! NormalAlertView
    view.type = type
    view.detailLabel.text = detail
    view.titleLabel.text = title
    view.defaulsString = detail
    let margin: CGFloat = 25
    let labelW = view.width - margin * 2
    
    let labelH = detail.stringSize(CGSize.init(width: labelW, height: CGFloat.greatestFiniteMagnitude), attributes: [NSFontAttributeName: view.detailLabel.font]).height
    
    view.detailLabel.frame = CGRect.init(x: margin, y: view.titleLabel.maxY + margin, width: labelW, height: labelH)
    
    view.height = view.detailLabel.maxY + 54 + margin
    view.setCenter()
    
    return view
  }
  
  override func timerAction(remainTime: Int) {
    if remainTime == 0 {
       detailLabel.text = self.defaulsString
    }else{
       detailLabel.text = self.defaulsString + "(\(remainTime))"
    }

    if type == .delayTouchable {
      confirmButton.isEnabled = false
      if remainTime == 0 {
        confirmButton.isEnabled = true
      }
    }else{
      if remainTime == 0 {
        Utils.hideAnimated()
        if cancelClickBlock != nil {
          cancelClickBlock!(true)
        }
      }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.clear
    titleLabel.morphingEffect = .evaporate
    addSubview(detailLabel)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
  }
  
}
