//
//  systemMsg.swift
//  game_werewolf
//
//  Created by Hang on 2017/9/19.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class systemDialog: BaseNoticeView {

  @IBOutlet weak var textview: UITextView!
  
 @IBOutlet weak var timeLable: UILabel!
    
  override func awakeFromNib() {
    self.backgroundColor = UIColor.hexColor("AFBCDE")
  }
  
  static func view(title:String) -> systemDialog {
    let view = Bundle.main.loadNibNamed("systemMsg", owner: nil, options: nil)?.last as! systemDialog
    view.frame = CGRect(x: 0, y: 0, width: Screen.width*0.8, height: 200)
    view.backgroundColor = UIColor.hexColor("AFBCDE")
    view.textview.backgroundColor = UIColor.hexColor("AFBCDE")
    view.setTitle(title: title)
    view.setCenter()
    return view
  }
  
  func setTitle(title:String) {
    self.textview.text = title
  }
  
  override func layoutSubviews() {
    if let text = self.textview.text {
      if text != "" {
        let he = text.height(withConstrainedWidth:Screen.width*0.8 - 40, font: UIFont.boldSystemFont(ofSize: 14))
        self.height =  he + 100
        setCenter()
      }
    }
  }
  
  override func timerAction(remainTime: Int) {

    timeLable.text = "(\(remainTime)s)"
    if remainTime == 0 {
      Utils.hideNoticeView()
    }
  }
  
  @IBAction func confirm(_ sender: Any) {
      Utils.hideNoticeView()
  }
  
}
