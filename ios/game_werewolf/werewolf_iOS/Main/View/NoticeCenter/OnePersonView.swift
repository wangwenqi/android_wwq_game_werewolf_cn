//
//  OnePersonView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/23.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class OnePersonView: BaseNoticeView {
  @IBOutlet weak var titleLabel: LTMorphingLabel!
  @IBOutlet weak var playerView: UIView!
  @IBOutlet weak var detailLabel: UILabel!
  @IBOutlet weak var conformButton: UIButton!
  @IBOutlet weak var avaborderImage: UIImageView!
  @IBOutlet weak var sherrifeLabel: UILabel!
    
  var playerCellView = VotePlayerCellView.view(json: (1, JSON.init(parseJSON: "")))
  
  var playerData = (0, JSON.init(parseJSON: "")) {
    didSet {
      if type == .boom {
        setNeedsLayout()
        let imageView = UIImageView()
        let image = isKing ? UIImage(named: "message_gift_werewolf_king") : UIImage(named: "message_gift_werewolf")
        playerView.addSubview(imageView)
        imageView.snp.makeConstraints({ (make) in
          make.edges.equalToSuperview()
        })
//        imageView.frame = playerView.bounds//CGRect(x:0, y: 0, width: (image?.size.width)!, height: (image?.size.height)!)
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
      }else{
        let view = VotePlayerCellView.view(json: playerData)
        view.frame = playerView.bounds
        playerView.addSubview(view)
        playerCellView = view
        if type == .sheriff {
            sherrifeLabel.isHidden = false
            sherrifeLabel.text = view.nameLabel.text
            view.nameLabel.isHidden = true
            avaborderImage.width = view.width
            view.shrrifeImage.isHidden = false
            view.bringSubview(toFront: view.numberLabel)
        }
      }
    }
  }
  
  var type: viewType = .none {
    didSet {
      playerCellView.checkImage.image = UIImage.init(named: "room_icon_badge")
      if type == .sheriff {
        titleLabel.text = NSLocalizedString("晋升警长", comment: "")
        
      } else if type == .boom {
         titleLabel.text = NSLocalizedString("狼人自爆", comment: "")
      } else {
        titleLabel.text = NSLocalizedString("接替警长", comment: "")
      }
    }
  }
  
  var isKing:Bool = false
  
  override func timerAction(remainTime: Int) {
    var local = ""
    if type == .sheriff {
      local = NSLocalizedString("晋升警长", comment: "")
    } else if type == .handOver {
      local = NSLocalizedString("接替警长", comment: "")
    } else if type == .boom {
      local = NSLocalizedString("狼人自爆", comment: "")
    }else {
      local = NSLocalizedString("查验结果", comment: "")
    }
    let remain = "(\(remainTime)s)"
//    let  str = NSMutableAttributedString.init(string: local + remain)
//    let showRange = NSRange(location: 0, length: 4)
//    let remainRange = NSRange(location: 4 , length: remain.length)
//    str.addAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 14, weight: UIFontWeightThin)], range:remainRange)
//    str.addAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 14, weight: UIFontWeightHeavy)], range:showRange)
    titleLabel.text = local + remain
    if remainTime == 0 {
      Utils.hideNoticeView()
    }
  }
  
  @IBAction func conformButtonClicked(_ sender: Any) {
    Utils.hideNoticeView()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
  }
  
  class func view(role: GameRole, playerData: (Int, JSON), detailRole:Bool) -> OnePersonView {
    let view = SeerCheckView.loadFromNib("OnePersonView") as! OnePersonView
    view.playerData = playerData
    
    var roleString = ""
    
    if detailRole {
      roleString = role.descriptionString;
    } else {
      if role == .werewolf || role == .werewolf_king || role == .demon {
        roleString = NSLocalizedString("狼人", comment: "")
      } else {
        roleString = NSLocalizedString("好人", comment: "")
      }
    }
    
    let finalStr = NSLocalizedString("他的身份是\(roleString)", comment: "")
    let attStr = finalStr.highLightedStringWith(roleString, color: CustomColor.roomPink)
    view.detailLabel.attributedText = attStr
    
    return view
  }
  
  static func view(isgod:Bool,playerData: (Int, JSON)) -> OnePersonView {
    let view = SeerCheckView.loadFromNib("OnePersonView") as! OnePersonView
    view.playerData = playerData
    var roleString = "是神"
    if isgod {
      roleString = NSLocalizedString("是神", comment: "")
    }else{
      roleString = NSLocalizedString("不是神", comment: "")  //
    }
    let finalStr = NSLocalizedString("他的身份\(roleString)", comment: "")
    let attStr = finalStr.highLightedStringWith(roleString, color: CustomColor.roomPink)
    view.detailLabel.attributedText = attStr
    return view
  }
  
  class func view(role: GameRole, playerData: (Int, JSON)) -> OnePersonView {
    
    return view(role: role, playerData: playerData, detailRole: false);
  }
  
  class func viewSheriff(type: viewType, data: (Int, JSON), data2: (Int, JSON) = (0, JSON.init(parseJSON: ""))) -> OnePersonView {
    
    let view = SeerCheckView.loadFromNib("OnePersonView") as! OnePersonView
    view.type = type
    
    if type == .sheriff {
      view.playerData = data
      let localNewP = NSLocalizedString("号玩家被选为警长", comment: "")
      view.detailLabel.text = "[\(data.0)]\(localNewP)"
    } else if type == .handOver {
      if data2.0 == 0 {
        let localNone = NSLocalizedString("号玩家撕掉了警徽，未让任何人继承警长。", comment: "")
        view.playerData = data
        view.detailLabel.text = "[\(data.0)]\(localNone)"
      } else {
        let localOld = NSLocalizedString("号玩家选择", comment: "")
        let localNew = NSLocalizedString("号玩家继承警长", comment: "")
        view.detailLabel.text = "[\(data.0)]\(localOld)[\(data2.0)]\(localNew)"
        view.playerData = data2
      }
    }
    return view
  }
  
  static func viewBoom(type:viewType,isKing:Bool,data: (Int, JSON), data2: (Int, JSON) = (0, JSON.init(parseJSON: ""))) -> OnePersonView {
    
    let view = SeerCheckView.loadFromNib("OnePersonView") as! OnePersonView
    view.type = type
    view.isKing = isKing
    view.playerData = data
    let localNewP = NSLocalizedString("号玩家自爆", comment: "")
    view.detailLabel.text = "[\(data.0)]\(localNewP)"
    return view
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.clear
    titleLabel.morphingEffect = .evaporate
  }
  
  enum viewType {
    case sheriff
    case handOver
    case none
    case boom
  }
}
