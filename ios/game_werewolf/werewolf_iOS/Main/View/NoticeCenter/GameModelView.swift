//
//  GameModelView.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/25.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

typealias selectModel = (Int) -> ()

class GameModelView: BaseNoticeView {

    var selectIndex:selectModel?
    
    @IBOutlet weak var backImage: UIImageView!
    @IBAction func selectModel(_ sender: Any) {
        let button = sender as! UIButton
        if selectIndex != nil {
          selectIndex!(button.tag)
          self.animation = "fall"
          self.animateNext {
            self.removeFromSuperview()
          }
        }
    }
  
    deinit {
      print("++++gamenmodel")
    }
    
    override func awakeFromNib() {
        self.backImage.isUserInteractionEnabled = true
    }
}
