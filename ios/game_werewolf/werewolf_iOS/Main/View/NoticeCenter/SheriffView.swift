//
//  SheriffView.swift
//  game_werewolf
//
//  Created by QiaoYijie on 2017/3/30.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class SheriffView: BaseNoticeView {
  
  @IBOutlet weak var titleLabel: LTMorphingLabel!
  
  
  class func view() -> SheriffView {
    let view = SeerCheckView.loadFromNib("SheriffView") as! SheriffView
    return view
  }
  
  override func timerAction(remainTime: Int) {
    let localIfJoin = NSLocalizedString("是否参选警长", comment: "")
//    titleLabel.text = "\(localIfJoin)\(remainTime)s"
    titleLabel.morphingEffect = .evaporate
    let remain = "(\(remainTime)s)"
    let str = NSMutableAttributedString.init(string: localIfJoin + remain)
//    let showRange = NSRange(location: 0, length: localIfJoin.length)
//    let remainRange = NSRange(location: localIfJoin.length , length: remain.length)
//    str.addAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 14, weight: UIFontWeightThin)], range:remainRange)
//    str.addAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 14, weight: UIFontWeightHeavy)], range:showRange)
    titleLabel.text = localIfJoin + remain
    if remainTime == 0 {
      Utils.hideNoticeView()
    }
  }
  
  @IBAction func giveUpClicked(_ sender: Any) {
    messageDispatchManager.sendMessage(type: .apply, payLoad: ["apply": false])
    Utils.hideNoticeView()
    
  }
  
  @IBAction func conformClicked(_ sender: Any) {
    messageDispatchManager.sendMessage(type: .apply, payLoad: ["apply": true])
    Utils.hideNoticeView()
  }
  override func awakeFromNib() {
     self.backgroundColor = UIColor.clear
  }
}
