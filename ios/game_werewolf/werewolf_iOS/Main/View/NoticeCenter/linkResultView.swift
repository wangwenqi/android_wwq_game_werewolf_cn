//
//  linkResultView.swift
//  game_werewolf
//
//  Created by QiaoYijie on 2017/3/30.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class linkResultView: BaseNoticeView {
  
  @IBOutlet weak var titleLabel: LTMorphingLabel!
  @IBOutlet weak var detailLabel: UILabel!
  
  @IBOutlet weak var view1: UIView!
  @IBOutlet weak var view2: UIView!
  
  var playerView1: VotePlayerCellView?
  var playerView2: VotePlayerCellView?
  
  enum LinkViewType {
    case conform
    case notice
  }
  
  var type: LinkViewType = .conform {
    didSet {
      if type == .conform {
        titleLabel.text = NSLocalizedString("确认情侣", comment: "")
      } else {
        titleLabel.text = NSLocalizedString("你被爱神射中啦", comment: "")
        detailLabel.isHidden = true
      }
    }
  }
  
  @IBAction func conformButtonClicked(_ sender: Any) {
    Utils.hideAnimated()
  }
  
  class func view(type: LinkViewType, player1: (Int, JSON), player2: (Int, JSON)) -> linkResultView {
    let view = linkResultView.loadFromNib("linkResultView") as! linkResultView
    view.type = type
    let localNum = NSLocalizedString("号", comment: "")
    let localAnd = NSLocalizedString("和", comment: "")
    let localCP = NSLocalizedString("成为了情侣", comment: "")
    
    let player1Str = "[\(player1.0)]\(localNum)"
    let player2Str = "[\(player2.0)]\(localNum)"
    
    let str = "\(player1Str)\(localAnd)\(player2Str)\(localCP)"
    view.detailLabel.text = str
    
    view.playerView1 = VotePlayerCellView.view(json: player1)
    view.playerView2 = VotePlayerCellView.view(json: player2)
    view.view1.addSubview(view.playerView1!)
    view.view2.addSubview(view.playerView2!)
    return view
  }
  
  override func timerAction(remainTime: Int) {
    if type == .conform {
      let localConfirm = NSLocalizedString("确认情侣", comment: "")
      titleLabel.text = "\(localConfirm)(\(remainTime))"
    } else {
      let localChose = NSLocalizedString("你被爱神射中啦", comment: "")
      titleLabel.text = "\(localChose)(\(remainTime))"
    }
    
    if remainTime <= 0 {
      Utils.hideAnimated()
    }
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    playerView1?.frame = view1.bounds
    playerView2?.frame = view2.bounds
    if type == .conform {
      height = 310
    } else {
      height = 260
    }
    setCenter()
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.clear
    titleLabel.morphingEffect = .evaporate
  }
}
