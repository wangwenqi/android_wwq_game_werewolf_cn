//
//  PlayerAlertView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/25.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import Alamofire

class PlayerAlertView: BaseNoticeView {
    
    @IBOutlet weak var addFriendButtonHeight: NSLayoutConstraint!
  
@IBOutlet weak var lockButtonPaddingProfile: NSLayoutConstraint!
  @IBOutlet weak var profilePage: UIButton!
  @IBOutlet weak var iconImage: XBHImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var levelLabel: UILabel!
  @IBOutlet weak var addFriendButton: UIButton!
  @IBOutlet weak var lockButton: UIButton!
  @IBOutlet weak var kickOutButton: UIButton!
  @IBOutlet weak var winPercentLabel: UILabel!
  @IBOutlet weak var allCountLabel: UILabel!
  @IBOutlet weak var winCountLabel: UILabel!
  @IBOutlet weak var loseCountLabel: UILabel!
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var moveToBlackListbutton: UIButton!
    @IBOutlet weak var runRate: UILabel!
    
 @IBOutlet weak var winCountCenter: NSLayoutConstraint!
    
  
  enum PlayAlertType {
    case normal
    case master
  }
  
  var type: PlayAlertType = .normal {
    didSet {
      if type == .normal {
        lockButton.isHidden = true
        kickOutButton.isHidden = true
        addFriendButton.isHidden = true
        reportButton.isHidden = true;
        moveToBlackListbutton.isHidden = true;
      }
    }
  }
  
  var isFriend = true {
    didSet {
        let isSelfInfo = CurrentUser.shareInstance.currentPosition == playerView.playerData["position"].intValue + 1;
      // 如果不是自己
      if !isSelfInfo {
        reportButton.isHidden = false;
        moveToBlackListbutton.isHidden = false;
      }
      
      if !isSelfInfo && !isFriend {
        addFriendButton.isHidden = false
      } else {
        addFriendButton.isHidden = true
      }
      
      layoutSubviews()
    }
  }
  
  var isTourist = false
  
  var playerView = PlayerView() {
    didSet {
      self.nameLabel.text = "昵称：\(playerView.playerData["name"].stringValue)"
      self.levelLabel.text = "等级：\(playerView.playerData["level"].stringValue)"
      
      iconImage.image = UIImage.init(named: "room_head_default")
      if let iconUrl = URL.init(string: playerView.playerData["avatar"].stringValue) {
        iconImage.setImage(url: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
      }
      
      // 请求用户信息
      RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id":playerView.playerData["id"].stringValue], success: { [weak self] (json) in
        let winCount = json["game"]["win"].intValue
        let loseCount = json["game"]["lose"].intValue
        let totalCount = winCount + loseCount
        self?.isFriend = json["is_friend"].boolValue
        
        self?.levelLabel.text = "等级：\(json["game"]["level"].intValue)"
        let total = "狼人杀\(totalCount)局"
        let win  = "胜\(winCount)局"
        let lose = "败\(loseCount)局"
        let final = total + " " + win + " " + lose
        let att = NSMutableAttributedString(string: final)
        att.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSMakeRange(0, total.length))
        att.addAttribute(NSForegroundColorAttributeName, value: UIColor.hexColor("5193EB"), range:NSMakeRange(total.length+1, win.length))
        att.addAttribute(NSForegroundColorAttributeName, value: UIColor.hexColor("EC3C1A"), range:NSMakeRange(total.length+1+win.length+1, lose.length))
        self?.winCountLabel.attributedText = att
        
        let escape = json["game"]["escape"].intValue
        self?.layoutSubviews()
        
        if winCount == 0 && loseCount == 0 {
          self?.winPercentLabel.text = "胜率：0%"
        } else {
          self?.winPercentLabel.text = "胜率：\(lroundf((Float(winCount) / (Float(totalCount) + Float(escape))) * Float(100.0)))%"
          self?.runRate.text = "逃跑率 \(lroundf((Float(escape) / (Float(totalCount) + Float(escape))) * Float(100.0)))%"
        }
      }) { [weak self] (code, message) in
//        XBHHUD.showError(message)
        let winCount = 0;
        let loseCount = 0;
        let totalCount = winCount + loseCount
        self?.isFriend = true;
        self?.isTourist = true
        self?.levelLabel.text = "等级：\(1)"
        let total = "狼人杀\(totalCount)局"
        let win  = "胜\(winCount)局"
        let lose = "败\(loseCount)局"
        let final = total + " " + win + " " + lose
        let att = NSMutableAttributedString(string: final)
        att.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSMakeRange(0, total.length))
        att.addAttribute(NSForegroundColorAttributeName, value: UIColor.hexColor("5193EB"), range:NSMakeRange(total.length+1, win.length))
        att.addAttribute(NSForegroundColorAttributeName, value: UIColor.hexColor("EC3C1A"), range:NSMakeRange(total.length+1+win.length+1, lose.length))
        self?.winCountLabel.attributedText = att
        self?.layoutSubviews()
        
        self?.winPercentLabel.text = "胜率：0%"
        self?.runRate.text = "逃跑率：0%"

      }
    }
  }
  
  @IBAction func cancelClicked(_ sender: Any) {
    Utils.hideNoticeView()
  }
  
  
  class func view(type: PlayAlertType, playerView: PlayerView) -> PlayerAlertView {
    let view = PlayerAlertView.loadFromNib("PlayerAlertView") as! PlayerAlertView
    view.playerView = playerView
    view.type = type
    return view
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.iconImage.radius(iconImage.width * 0.5)
    self.width = Screen.width * 0.7
    
    let winLabelY = winCountLabel.frame.maxY + 16 + 44;

    if type == .normal {
      
      if CurrentUser.shareInstance.currentPosition == playerView.playerData["position"].intValue + 1 {
        // 是自己
        self.height = winLabelY + 48*1;  //winLabelY + 48*0;
      } else if isFriend {
        // 是好友
        self.height = winLabelY + 48*3; //winLabelY + 48*2;
      }else {
        // 非好友
        //自己是游客 
        if CurrentUser.shareInstance.isTourist {
          self.height = winLabelY + 48 * 3
        }else{
          self.height = winLabelY + 48*4; //winLabelY + 48*3;
        }
      }
    } else if type == .master {
      
      if isFriend {
        // 不显示好友添加请求按钮
        self.height = winLabelY + 48*5; //winLabelY + 48*4 + 4;
//        self.addFriendButtonHeight.constant = 0
        self.lockButtonPaddingProfile.constant = 4
      } else {
        //自己是游客
        if CurrentUser.shareInstance.isTourist {
          self.height = winLabelY + 48*5; //winLabelY + 48*4 + 4;
          self.lockButtonPaddingProfile.constant = 4
        }else{
          self.addFriendButton.isHidden = false
//        self.addFriendButtonHeight.constant = 44
          self.lockButtonPaddingProfile.constant = 52
          self.height = winLabelY + 48*6;
        }
      }
    }
    setCenter()
  }
  
  @IBAction func addFriendClicked(_ sender: Any) {
    
    Utils.hideAnimated()
    
    RequestManager().post(url: RequestUrls.friendAdd, parameters: ["friend_id":playerView.playerData["id"].stringValue], success: { (json) in
      XBHHUD.showTextOnly(text: "好友请求发送成功！")
    }) { (code, message) in
      if message == "" {
        XBHHUD.showTextOnly(text: "好友请求发送失败")
      } else {
        XBHHUD.showTextOnly(text: "\(message)")
      }
    }
  }
  
  @IBAction func kickOutButtonClicked(_ sender: Any) {
    messageDispatchManager.sendMessage(type: .kick_out, payLoad: ["position": playerView.tag - 1])
    Utils.hideNoticeView()
  }
  @IBAction func lockClicked(_ sender: Any) {
    if CurrentUser.shareInstance.currentRoomType == "simple" {
      messageDispatchManager.sendMessage(type: .kick_out, payLoad: ["position": playerView.tag - 1]);
      messageDispatchManager.sendMessage(type: .lock, payLoad: ["position": playerView.tag - 1]);
      Utils.hideNoticeView()
    } else {
      let view = NormalAlertView.view(type: .oneButton, title: "提示", detail: "该模式下不可开启或锁定座位")
      Utils.showAlertView(view: view)
    }
  }
    
    @IBAction func reportButtonClicked(_ sender: UIButton) {
        let actionSheet = UIActionSheet.init(title: "举报内容", delegate: self, cancelButtonTitle: "取消", destructiveButtonTitle: nil, otherButtonTitles: "广告", "色情","欺诈")
        actionSheet.show(in: UIApplication.shared.keyWindow!);
    }
    
    @IBAction func moveToBlackListButtonClicked(_ sender: UIButton) {
      Utils.hideNoticeView();
      XBHHUD.showLoading()
      if let ids =  playerView.playerData["id"].string {
        RequestManager().get(url: RequestUrls.block + "/\(ids)", success: { (json) in
          XBHHUD.hide()
          XBHHUD.showSuccess(NSLocalizedString("拉黑成功！", comment: ""))
        }) { (code, message) in
          XBHHUD.hide()
          XBHHUD.showError(NSLocalizedString("拉黑失败！", comment: ""))
          //TODO: 获取新的封禁列表并写入本地
        }
      }else{
        XBHHUD.showError(NSLocalizedString("拉黑失败！", comment: ""))
      }
    }
  
    //跳转到个人主页
    @IBAction func goToProfile(_ sender: Any) {
      let controller = UserInfoViewController()
      let myself = CurrentUser.shareInstance.currentPosition == playerView.playerData["position"].intValue + 1;
      controller.peerID = playerView.playerData["id"].stringValue
      controller.peerSex = playerView.playerData["sex"].stringValue
      controller.peerName = playerView.playerData["name"].stringValue
      controller.peerIcon = playerView.playerData["avatar"].stringValue
      controller.isFriend = self.isFriend
      controller.isTourist = self.isTourist
      controller.isself = myself
      Utils.hideNoticeView()
      let nav = RotationNavigationViewController.init(rootViewController: controller)
      ((Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)!) as UIViewController).present(nav, animated: true, completion: nil)
      
//      let presetend = Utils.getKeyWindow()?.rootViewController?.presentedViewController
//      presetend?.presentedViewController?.present(nav, animated: true, completion: nil)
//      UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
    }
  
  
  deinit {
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.radius(10)
    self.backgroundColor = UIColor.clear
  }
}

extension PlayerAlertView: UIActionSheetDelegate {
  // MARK: - actionSheetDelegate
  func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
    print("\(buttonIndex)")
    if buttonIndex != 0 {
      // 请求用户信息
      XBHHUD.showLoading()
      RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id":"590f2f51493d110007b8c75d"], success: { [weak self] (json) in
        XBHHUD.hide()
        XBHHUD.showSuccess("举报成功！")
      }) { (code, message) in
        XBHHUD.hide()
        XBHHUD.showError("请检查您的网络...")
      }
      Utils.hideNoticeView();
    }
  }
}
