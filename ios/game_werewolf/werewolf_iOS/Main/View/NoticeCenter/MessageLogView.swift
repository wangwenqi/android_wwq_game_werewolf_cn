//
//  MessageLogView.swift
//  game_werewolf
//
//  Created by QiaoYijie on 2017/3/31.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class MessageLogView: BaseNoticeView {
  
  @IBOutlet weak var contentView: UIView!
  
  enum MessageType: String {
    case send
    case receive
  }
  
  let messageController = GameRoomMessageViewController()
  
  class func view() -> MessageLogView {
    let view = MessageLogView.loadFromNib("MessageLogView") as! MessageLogView
    return view
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    contentView.addSubview(messageController.view)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    messageController.view.frame = contentView.bounds
    messageController.view.height -= 8
    height = Screen.height * 0.8
    setCenter()
  }
  
  func addConversation(type: MessageType, message: String) {
    messageController.addConversion(type: .player, messge: message, positionNumber: type.rawValue)
  }
  
  @IBAction func hideClicked(_ sender: Any) {
    Utils.hideNoticeView()
  }
  
  @IBAction func clearClicked(_ sender: Any) {
    messageController.clearAll()
  }
}
