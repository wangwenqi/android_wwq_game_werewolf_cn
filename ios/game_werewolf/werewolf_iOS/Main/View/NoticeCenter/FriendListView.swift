//
//  FriendListView.swift
//  game_werewolf
//
//  Created by QiaoYijie on 2017/5/24.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class FriendListView: BaseNoticeView {
  
  var modelArray = [FriendListModel]() {
    didSet {
      friendController.modelArray = modelArray
    }
  }
  
  @IBOutlet weak var contentView: UIView!
  /*
   // Only override draw() if you perform custom drawing.
   // An empty implementation adversely affects performance during animation.
   override func draw(_ rect: CGRect) {
   // Drawing code
   }
   */
  let friendController = GameRoomFriendListViewController()
  
  class func view(modelArray: [FriendListModel]) -> FriendListView {
    let view = FriendListView.loadFromNib("FriendListView") as! FriendListView
    view.modelArray = modelArray
    return view
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    contentView.addSubview(friendController.view)
    self.backgroundColor = UIColor.clear
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    friendController.view.frame = contentView.bounds
    friendController.view.height -= 8
    height = Screen.height * 0.8
    setCenter()
  }
  
  @IBAction func cancelButtonClicked(_ sender: Any) {
    Utils.hideNoticeView()
  }
  
  @IBAction func inviteButtonClicked(_ sender: Any) {
    friendController.inviteFriend()
  }
}
