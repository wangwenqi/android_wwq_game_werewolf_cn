//
//  LockAlertView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/25.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class LockAlertView: BaseNoticeView {

    @IBOutlet weak var iconImage: UIImageView!
    
    @IBOutlet weak var lockButton: XBHButton!
    
    
    enum LockType {
        case unLock
        case lock
    }
    
    var currentPosition = -1
    
    var type: LockType = .lock {
        didSet {
            if type == .lock {
                // 要锁
                lockButton.setTitle(NSLocalizedString("关闭座位", comment: ""), for: .normal)
                iconImage.image = UIImage.init(named: "popup_icon_locked")
            } else {
                lockButton.setTitle(NSLocalizedString("开启座位", comment: ""), for: .normal)
                   iconImage.image = UIImage.init(named: "popup_icon_unlocked")
            }
        }
    }
    
    class func view(type: LockType, position: Int) -> LockAlertView {
        let view = LockAlertView.loadFromNib("LockAlertView") as! LockAlertView
        view.type = type
        view.currentPosition = position
        return view
     }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        height = 185
        setCenter()
        self.width = Screen.width * 0.7
    }
    @IBAction func cancelClicked(_ sender: Any) {
        Utils.hideNoticeView()
    }
    
    @IBAction func lockClicked(_ sender: Any) {
        if type == .lock {
            // 要锁
          messageDispatchManager.sendMessage(type: .lock, payLoad: ["position": currentPosition - 1])
            Utils.hideNoticeView()
        } else {
          messageDispatchManager.sendMessage(type: .unlock, payLoad: ["position": currentPosition - 1])
            Utils.hideNoticeView()
        }
    }
    
}
