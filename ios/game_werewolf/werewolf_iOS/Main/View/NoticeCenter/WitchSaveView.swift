//
//  WitchSaveView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/23.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class WitchSaveView: BaseNoticeView {
  
  var saveBlock: (() -> Void)?
  
  var playerData = (0, JSON.init(parseJSON: "")) {
    didSet {
      let view = VotePlayerCellView.view(json: playerData)
      view.frame = playerView.bounds
      playerView.addSubview(view)
    }
  }
  

  @IBOutlet weak var playerView: UIView!
  @IBOutlet weak var titleLabel: LTMorphingLabel!
  @IBOutlet weak var detailLabel: UILabel!
  var can_save:Bool?
  
  
  @IBOutlet weak var confirm: UIButton!
  @IBOutlet weak var notSaveButton: UIButton!
  @IBOutlet weak var saveButton: UIButton!
  
  class func view(playerData: (Int, JSON),cansave:Bool) -> WitchSaveView {
    let view = WitchSaveView.loadFromNib("WitchSaveView") as! WitchSaveView
    view.playerData = playerData
    view.can_save = cansave
    if view.can_save! {
      view.saveButton.isHidden = false
      view.notSaveButton.isHidden = false
      view.confirm.isHidden = true
      let localWillKill = NSLocalizedString("今天晚上被杀的是", comment: "")
      let localIfSave = NSLocalizedString("号玩家，要救他吗？", comment: "")
      view.detailLabel.text = "\(localWillKill)［\(playerData.0)］\(localIfSave)"
      
    }else{
      view.saveButton.isHidden = true
      view.notSaveButton.isHidden = true
      view.confirm.isHidden = false
      let localSave = NSLocalizedString("本轮死亡的是您，无法自救", comment: "")
      view.detailLabel.text = "\(localSave)"
    }
    return view
  }
  
  override func timerAction(remainTime: Int) {
    if self.can_save != nil {
      if self.can_save! {
        let localSave = NSLocalizedString("选择是否救他", comment: "")
        titleLabel.text = "\(localSave)(\(remainTime)s)"
      }else{
        let localSave = NSLocalizedString("本轮死亡的是您，无法自救", comment: "")
        titleLabel.text = "\(localSave)(\(remainTime)s)"
      }
      if remainTime == 0 {
        Utils.hideNoticeView()
      }
    }
  }
  
  override func awakeFromNib() {
     self.backgroundColor = UIColor.clear
     self.height = 300
  }
  
  @IBAction func confirmNotSave(_ sender: Any) {
    Utils.hideNoticeView()
    messageDispatchManager.sendMessage(type: .save);
  }
  
  @IBAction func noSaveClicked(_ sender: Any) {
    Utils.hideNoticeView()
    messageDispatchManager.sendMessage(type: .save);
  }
  @IBAction func saveClicked(_ sender: Any) {
    Utils.hideNoticeView()
    messageDispatchManager.sendMessage(type: .save, payLoad: ["position": playerData.0 - 1])
    if saveBlock != nil {
      saveBlock!()
    }
  }
}
