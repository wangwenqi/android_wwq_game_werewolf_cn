//
//  shadowButton.swift
//  game_werewolf
//
//  Created by Hang on 2017/12/11.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class shadowButton: UIButton {

  override func awakeFromNib() {
    addshaow()
  }
  
  func addshaow() {
    layer.shadowColor = UIColor.darkGray.cgColor
    layer.shadowOffset = CGSize(width: 0, height: 2)
    layer.shadowOpacity = 0.8
    layer.shadowRadius = 6
    self.layer.cornerRadius = 3
    self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0)
    self.imageView?.contentMode = .scaleAspectFit
    if UIScreen.main.bounds.width > 320 {
       self.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        //日语
      if CurrentUser.shareInstance.currentLang == "ja" {
           self.titleLabel?.font = UIFont.systemFont(ofSize: 12)
      }else{
           self.titleLabel?.font = UIFont.systemFont(ofSize: 10)
      }
    } else {
      
      if CurrentUser.shareInstance.currentLang == "ja" {
        self.titleLabel?.font = UIFont.systemFont(ofSize: 11)
      }else{
        self.titleLabel?.font = UIFont.systemFont(ofSize: 9)
      }
     
    }
  }
}
