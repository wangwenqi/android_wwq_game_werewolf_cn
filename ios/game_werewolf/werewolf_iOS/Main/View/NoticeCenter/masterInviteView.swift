//
//  masterInviteView.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/23.
//  Copyright © 2017年 orangelab. All rights reserved.
// 召集令页面

import UIKit

class masterInviteView: BaseNoticeView {
    typealias callBack = () -> ()
    var confirmCallback:callBack?
    @IBOutlet weak var bg: UIView!
    @IBOutlet weak var cancleButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var inviteDes: UITextView!
    @IBOutlet weak var content: UIView!

  
    @IBOutlet weak var enButton: UIButton!
    @IBOutlet weak var languageButton: UIButton!
    @IBOutlet weak var heighLevel: UIButton!
    @IBOutlet weak var gameButton: UIButton!
    @IBOutlet weak var gameTypeTitle: UILabel!
    @IBOutlet weak var chooseTypeView: UIView!
    @IBOutlet weak var gameType: UIView!
    @IBOutlet weak var title: UILabel!
  
    @IBOutlet weak var masterNoticeLable: UILabel!
    
    @IBAction func cancleButtonClicked(_ sender: Any) {
       Utils.hideAnimated()
    }
   
    @IBAction func confirmButtonClicked(_ sender: Any) {
      //这里发送召集令
      let localTitle = NSLocalizedString("召集说明，例如：高端玩家速来", comment: "")
      if inviteDes.text != nil {
        if inviteDes.text == localTitle  {
            XBHHUD.showError(NSLocalizedString("召集说明不能为空", comment: ""))
            return
        }
        if current == 0 {
          XBHHUD.showError(NSLocalizedString("请选择召集类型", comment: ""))
          return
        }
        var chooseButton:UIButton?
        if let button = self.viewWithTag(lastSlected) as? UIButton {
          chooseButton = button
        }else{
          XBHHUD.showError(NSLocalizedString("请选择召集类型", comment: ""))
          return
        }
         let export = inviteDes.text.trimmingCharacters(in: .whitespaces)
         if export != "" {
            //可以发布
            if (export.characters.count) > 15 {
              XBHHUD.showError(NSLocalizedString("召集说明太长啦", comment: ""))
            }else{
              if (export.characters.count) < 4 {
                 XBHHUD.showError(NSLocalizedString("召集说明不能少于4个字", comment: ""))
                 return
              }
              if CurrentUser.shareInstance.currentRoomPassword != "" {
                XBHHUD.showError( NSLocalizedString("发全服召集令时不可设置房间密码", comment: ""))
                return
              }
              Utils.hideNoticeView()
              let type:String = (roomType.init(rawValue:(chooseButton?.titleLabel?.text!)!)?.des)!
              messageDispatchManager.sendMessage(type:.export, payLoad: ["type":type,"title":export,"position":CurrentUser.shareInstance.currentPosition])
//              XBHHUD.showSuccess(NSLocalizedString("召集令发布成功", comment: ""))
//              if self.confirmCallback != nil {
//                self.confirmCallback!()
//              }
            }
         }else{
          XBHHUD.showError(NSLocalizedString("召集说明不能为空", comment: ""))
        }
      }else{
        XBHHUD.showError(NSLocalizedString("召集说明不能为空", comment: ""))
      }
    }

    @IBAction func roomTypeChoosen(_ sender: Any) {
        
        if let button = sender as? UIButton {
            button.isSelected = !button.isSelected
          if button.isSelected {
            current = button.tag
          }else{
            current = 0
          }
          if lastSlected == 0 {
            lastSlected = button.tag
          }else{
            if lastSlected != button.tag {
              (self.viewWithTag(lastSlected) as! UIButton).isSelected = false
              lastSlected = button.tag
            }
          }
        }
    }
  
  func remove() {
    if self.confirmCallback != nil {
      self.confirmCallback!()
    }
  }
  
    lazy var gameTypes = [NSLocalizedString("游戏房", comment: ""),NSLocalizedString("高端房", comment: ""),NSLocalizedString("娱乐房", comment: ""),NSLocalizedString("语种房", comment: "")]
    var alertView:UIView = UIView()
    var baseHeight:CGFloat = 0
    var lastType = 0
    var lastSlected:Int = 0
    var current:Int = 0
    var config:[String:JSON]? {
      didSet{
        updateConfig()
      }
    }
    override func awakeFromNib() {
        self.gameButton.imageView?.contentMode = .scaleAspectFit
           self.heighLevel.imageView?.contentMode = .scaleAspectFit
           self.languageButton.imageView?.contentMode = .scaleAspectFit
           self.enButton.imageView?.contentMode = .scaleAspectFit
        self.gameButton.imageEdgeInsets = UIEdgeInsetsMake(4, 4, 4, 4   )
         self.heighLevel.imageEdgeInsets = UIEdgeInsetsMake(4, 4, 4, 4   )
          self.languageButton.imageEdgeInsets = UIEdgeInsetsMake(4, 4, 4, 4   )
          self.enButton.imageEdgeInsets = UIEdgeInsetsMake(4, 4, 4, 4   )
        self.backgroundColor = UIColor.clear
        self.inviteDes.layer.borderColor = UIColor.hexColor("7d64fd").cgColor
        self.inviteDes.layer.borderWidth = 1
        self.inviteDes.text = NSLocalizedString("召集说明，例如：高端玩家速来", comment: "")
        let str = NSLocalizedString("使用条件:玩家等级达到20级，需要消耗100金币", comment: "")
        let att = NSMutableAttributedString(string: str)
        let levelRange = NSRange(location: 11, length: 3)
        self.masterNoticeLable.textColor = UIColor.hexColor("7A33FF")
        let costRange = NSRange(location: str.length-5, length: 5)
        att.addAttributes([NSForegroundColorAttributeName:UIColor.hexColor("fd14a8")], range: levelRange)
        att.addAttributes([NSForegroundColorAttributeName:UIColor.hexColor("fd14a8")], range: costRange)
        self.masterNoticeLable.attributedText = att
 
        inviteDes.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //成功后隐藏
         NotificationCenter.default.addObserver(self, selector: #selector(remove), name: NSNotification.Name(rawValue: "MSTERINVITE_HIDDEN"), object: nil)
        adjustContentSize(tv: inviteDes)
        setCenter()
    }
  
    func updateConfig() {
      if let level = self.config?["level"]?.int,
        let cost = self.config?["cost"]?["value"].int,
        let type = self.config?["cost"]?["type"].string{
        let costType = getTypeBy(string: type)
          let localLevel = NSLocalizedString("级", comment: "")
          let localLevelLimit = NSLocalizedString("使用条件:玩家等级达到"
            , comment: "")
          let localCost = NSLocalizedString("，需要消耗"
          , comment: "")
          let localCostType = NSLocalizedString(costType, comment: "")
          let str = localLevelLimit + "\(level)\(localLevel)" + localCost + "\(cost)\(localCostType)"
          let att = NSMutableAttributedString(string: str)
          let levelRange = NSRange(location: 11, length: String(level).length+1)
          let costRange = NSRange(location: str.length-String(cost).length-2, length: String(cost).length+2)
          att.addAttributes([NSForegroundColorAttributeName:UIColor.hexColor("fd14a8")], range: levelRange)
          att.addAttributes([NSForegroundColorAttributeName:UIColor.hexColor("fd14a8")], range: costRange)
          self.masterNoticeLable.attributedText = att
      }
    }
  
    func getTypeBy(string:String) -> String {
      switch string {
      case "gold":
        return  NSLocalizedString("金币", comment: "")
      case "dim":
         return NSLocalizedString("钻石", comment: "")
      default:
         return NSLocalizedString("金币", comment: "")
      }
    }
    
    override func layoutSubviews() {
        let point = CGPoint(x: 0, y: inviteDes.maxY)
        let truePoint = self.content.convert(point, to: self)
        self.height = truePoint.y + 30 + 70
        baseHeight = self.height
    }
  
    @IBAction func chooseRoomType(_ sender: Any) {
        let copys = gameTypes
        let title = copys.filter {$0 != gameTypeTitle.text}
        generateAlertView(title: title)
    }
  
    func getTitle(_ type:String) -> String {
      switch type {
      case "game":
        return NSLocalizedString("游戏房", comment: "")
      case "high_level":
        return NSLocalizedString("高端房", comment: "")
      case "joy":
        return NSLocalizedString("娱乐房", comment: "")
      case "language":
        return NSLocalizedString("语种房", comment: "")
      default:
        return NSLocalizedString("游戏房", comment: "")
      }
    }
  
    func reSetTitle(title:String) {
      if let tag = roomType.init(rawValue: getTitle(title))?.tag {
        if let button = self.viewWithTag(tag) as? UIButton {
          button.isSelected = true
          current = tag
          lastSlected = tag
        }
      }
    }
  
    func generateAlertView(title:[String])  {
      if self.content.subviews.contains(alertView) == true {
        return
      }
      let begeinY = self.chooseTypeView.frame.maxY
      let width = self.gameType.width
      let beginX = self.chooseTypeView.frame.origin.x + width
      alertView.frame = CGRect(x: beginX, y: begeinY - 10, width: width, height: 10 + 75)
      for text in title {
        let btn = UIButton()
        btn.frame = CGRect(x: 0, y: 10 + title.index(of:text)! * 25, width: Int(width), height: 25)
        btn.setTitle(text, for: .normal)
        btn.setTitleColor(UIColor.clear, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        btn.addTarget(self, action: #selector(handleChangeType(type:)), for: .touchUpInside)
        btn.setTitleColor(UIColor.hexColor("ACACAC"), for: .normal)
        if title.index(of: text)! < 2 {
          let gap = UIView()
          gap.frame = CGRect(x: btn.minX + 5, y: btn.maxY, width: width - 10, height: 1)
          gap.backgroundColor = UIColor.hexColor("E9AE37")
          alertView.addSubview(gap)
        }
        alertView.addSubview(btn)
      }
      alertView.backgroundColor = UIColor.hexColor("FFFDEF")
      alertView.layer.cornerRadius = 5
      alertView.layer.borderColor = UIColor.hexColor("F3D190").cgColor
      alertView.layer.borderWidth = 1
      alertView.layer.masksToBounds = true
      self.content.addSubview(alertView)
      self.content.bringSubview(toFront: self.chooseTypeView)
    }
  
    func handleChangeType(type:UIButton) {
      if let text = type.titleLabel?.text {
          gameTypeTitle.text = text
      }
      for view in alertView.subviews {
        view.removeFromSuperview()
      }
      alertView.removeFromSuperview()
    }
  
    func keyBoardWillShow(notification: Notification) {
      let userInfo = notification.userInfo
      let keyBoardBounds = (userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
      let resultPoint = CGPoint(x: self.center.x, y: (Screen.height - keyBoardBounds.height - 44)/2)
      self.center = resultPoint
    }
  
    func keyBoardWillHide(notification: Notification) {
      setCenter()
    }
  
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("========召集令页面deinit")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        inviteDes.resignFirstResponder()
    }
  
    func adjustContentSize(tv: UITextView){
      let deadSpace = tv.bounds.size.height - tv.contentSize.height
      let inset = max(0, deadSpace/2.0)
      tv.contentInset = UIEdgeInsetsMake(inset, tv.contentInset.left, inset, tv.contentInset.right)
    }

}

extension masterInviteView: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
      let localTitle = NSLocalizedString("召集说明，例如：高端玩家速来", comment: "")
      if textView.text == localTitle {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.characters.count == 0 {
            textView.text = NSLocalizedString("召集说明，例如：高端玩家速来", comment: "")
            textView.textColor = UIColor.hexColor("ACACAC")
        }
    }
  
    func textViewDidChange(_ textView: UITextView) {
      let length = textView.text?.characters.count
      if length ?? 0 > 15 {
        XBHHUD.showError(NSLocalizedString("输入内容过长", comment: ""))
        if let text = textView.text {
          let index =  text.index(text.startIndex , offsetBy: 15)
          textView.text = text.substring(to: index)
          self.adjustContentSize(tv: textView)
        }
      }
    }

}

////"fVe-0i-UW3.normalTitle" = "游戏房";
//"fVe-0i-UW3.normalTitle" = "ゲームルーム";
//
//
////"yhb-59-Gl1.normalTitle" = "高端房";
//"yhb-59-Gl1.normalTitle" = "高級ルーム";
//
//
////"qwT-Xg-cmW.normalTitle" = "娱乐房";
//"qwT-Xg-cmW.normalTitle" = "リラックスルーム";
//
//
////"4bi-zt-z1h.normalTitle" = "语种房";
//"4bi-zt-z1h.normalTitle" = "言語ルーム";

extension masterInviteView {
  enum roomType:String {
    case 游戏房
    case 高端房
    case 娱乐房
    case 语种房
    case 遊戲房
    case 娛樂房
    case 語種房
    case ゲームルーム
    case 高級ルーム
    case リラックスルーム
    case 言語ルーム
    
    var des:String {
      switch self {
      case .游戏房:
        return "game"
      case .高端房:
        return "high_level"
      case .娱乐房:
        return "joy"
      case .语种房:
        return "language"
      case .娛樂房:
        return "joy"
      case .語種房:
        return "language"
      case .遊戲房:
        return "game"
      case .ゲームルーム:
        return "game"
      case .高級ルーム:
        return "high_level"
      case .リラックスルーム:
        return "joy"
      case .言語ルーム:
        return "language"
      }
    }
    
    var tag:Int {
      switch self {
      case .游戏房:
        return 1
      case .高端房:
        return 2
      case .娱乐房:
        return 3
      case .语种房:
        return 4
      case .娛樂房:
        return 3
      case .語種房:
        return 4
      case .遊戲房:
        return 1
      case .言語ルーム:
        return 4
      case .高級ルーム:
        return 2
      case .ゲームルーム:
        return 1
      case .リラックスルーム:
        return 3
      }
    }
  }
}



