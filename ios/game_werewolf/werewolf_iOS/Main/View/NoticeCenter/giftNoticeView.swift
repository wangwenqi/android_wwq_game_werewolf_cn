//
//  giftNoticeView.swift
//  game_werewolf
//
//  Created by Hang on 2017/11/9.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class giftNoticeView: BaseNoticeView {

    @IBOutlet weak var sentAva: UIImageView!
    @IBOutlet weak var sentNum: UIImageView!
    @IBOutlet weak var sentName: UILabel!
    @IBOutlet weak var giftType: UIImageView!
    @IBOutlet weak var giftNumber: SpringLabel!
    @IBOutlet weak var recAva: UIImageView!
    @IBOutlet weak var recNum: UIImageView!
    @IBOutlet weak var recName: UILabel!
    var oldCount = 0
    var image:UIImage?
    typealias compelePlay = (giftNoticeView)->()
    var compelete:compelePlay?
  
    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
        self.recAva.layer.borderColor = UIColor.hexColor("ffef00").cgColor
        self.recAva.layer.borderWidth = 2
        self.sentAva.layer.borderWidth = 2
        self.sentAva.layer.borderColor = UIColor.hexColor("ff00ff").cgColor
      if Screen.size.width > 375 {
        self.sentName.font = UIFont.systemFont(ofSize: 8)
        self.recName.font = UIFont.systemFont(ofSize: 8)
      }else if Screen.size.width == 375{
         self.sentName.font = UIFont.systemFont(ofSize: 7)
        self.recName.font = UIFont.systemFont(ofSize: 7)
      }else {
         self.sentName.font = UIFont.systemFont(ofSize: 6)
          self.recName.font = UIFont.systemFont(ofSize: 6)
      }
    }
    
    func config(data:giftNotices) {
 
      //得到图片
      if self.image != nil {
        self.giftType.image = image!
      }
      self.giftNumber.text = "\(data.giftCount)"
      self.giftNumber.text = "x\(data.giftCount)"
      if oldCount == 0 {
        //说明是第一次
        self.sentName.text = "号玩家\(data.from.nameLabel.text!)送"
        self.sentAva.image = data.from.iconImage.image
        self.sentNum.image = UIImage.init(named:"s\(data.from.tag)")
        self.recName.text = "号玩家\(data.to.nameLabel.text!)"
        self.recAva.image = data.to.iconImage.image
        self.recNum.image = UIImage.init(named:"\(data.to.tag)")
        self.tag = Int(data.tag)
        self.liveTime = 2
        self.giftNumber.animation = "wobble"
        self.giftNumber.animate()
        oldCount = data.giftCount
      }else{
        if oldCount == data.giftCount {
          
        }else{
          self.liveTime = 2
          self.giftNumber.animation = "wobble"
          self.giftNumber.animate()
          oldCount = data.giftCount
        }
      }
  }
  
  deinit {
    print("============= gift notice ===============")
  }
  
  override func timerAction(remainTime: Int) {
    if remainTime == 0 {
      //说明已经要从上边一处
      if compelete != nil {
        compelete!(self)
      }
    }
  }
}
