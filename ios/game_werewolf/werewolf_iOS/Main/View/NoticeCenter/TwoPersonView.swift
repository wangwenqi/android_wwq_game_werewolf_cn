//
//  TwoPersonView.swift
//  game_werewolf
//
//  Created by Hang on 2017/9/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class TwoPersonView: BaseNoticeView {

  @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: LTMorphingLabel!
    @IBOutlet weak var left: UIView!
    @IBOutlet weak var right: UIView!
    var playerCellView = VotePlayerCellView.view(json: (1, JSON.init(parseJSON: "")))
    var leftPlayerData = (0, JSON.init(parseJSON: "")) {
        didSet{
          let view = VotePlayerCellView.view(json: leftPlayerData)
          view.frame = left.bounds
          left.addSubview(view)
        }
    }
    
    var rightPlayerData = (0, JSON.init(parseJSON: "")) {
        didSet{
          let view = VotePlayerCellView.view(json: rightPlayerData)
          view.frame = right.bounds
          right.addSubview(view)
        }
    }
  
    static func view(left:(Int, JSON),right:(Int, JSON)) -> TwoPersonView {
      let view = Bundle.main.loadNibNamed("TwoPersonView", owner: nil, options: nil)?.last as! TwoPersonView
      view.leftPlayerData = left
      view.rightPlayerData = right
      return view
    }
  
    override func awakeFromNib() {
       titleLabel.morphingEffect = .evaporate
        self.backgroundColor = UIColor.clear
       setCenter()
    }
  
    override func layoutSubviews() {
      self.height = 300//self.contentView.maxY + 55
    }
  
    override func timerAction(remainTime: Int) {
      let localCheck = NSLocalizedString("请选择发言顺序", comment: "")
      titleLabel.text = "\(localCheck)(\(remainTime)s)"
      if remainTime == 0 {
        Utils.hideNoticeView()
      }
    }
  
    @IBAction func leftClick(_ sender: Any) {
      messageDispatchManager.sendMessage(type: .speech_direction, payLoad: ["direction":"left"]);
      Utils.hideNoticeView()
    }

    @IBAction func rightClick(_ sender: Any) {
      messageDispatchManager.sendMessage(type: .speech_direction, payLoad: ["direction":"right"]);
       Utils.hideNoticeView()
    }

}
