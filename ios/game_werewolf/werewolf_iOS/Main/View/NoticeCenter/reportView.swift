//
//  reportView.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/28.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

enum reportType:Int {
    case ad = 1
    case run = 2
    case x18 = 3
    case cheat = 4
    case gangup = 5
    case abuse = 6
    case nonemic = 7
  
    var report_name:String {
      switch self {
      case .ad:
        return "ad"
      case .abuse:
        return "abuse"
      case .x18:
        return "sex"
      case .run:
        return "escape"
      case .cheat:
        return "cheat"
      case .gangup:
        return "gang_up"
      case .nonemic:
        return "trouble"
      default:
        break
      }
    }
  
  init(numberic:Int) {
    self.init(rawValue: numberic)!
  }
}

enum formType {
  case GAME
  case PROFILE
}

class reportView: BaseNoticeView {


    @IBOutlet weak var reportInfo: UITextView!

  
    var peerId:String?

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var ad: UIButton!
    @IBOutlet weak var run: UIButton!
    @IBOutlet weak var x18: UIButton!
    @IBOutlet weak var cheat: UIButton!
    @IBOutlet weak var gangUp: UIButton!
    @IBOutlet weak var confirm: UIButton!
    @IBOutlet weak var reportName: UILabel!
    @IBOutlet weak var cancleButton: UIButton!
    @IBAction func selectType(_ sender: Any) {
        
        let button = sender as! UIButton
        button.isSelected = !button.isSelected
        if button.isSelected {
          current = button.tag
        }else{
          current = 0
        }
        if lastSlected == 0 {
            lastSlected = button.tag
        }else{
            if lastSlected != button.tag {
                (self.viewWithTag(lastSlected) as! UIButton).isSelected = false
                lastSlected = button.tag
            }
        }
    }
    
    @IBAction func cancleClicked(_ sender: Any) {
      if isKeyboardAnimation {
        self.endEditing(true)
      }else{
        if self.form == .GAME {
          self.animation = "fadeOut"
          for v in (self.superview?.subviews)! {
            if v.isKind(of: newPlayerAlertView.self) {
              (v as! newPlayerAlertView).animation = "fadeIn"
              (v as! newPlayerAlertView).animateNext {
                self.superview?.bringSubview(toFront: v)
              }
            }
          }
          self.animateNext {
            self.superview?.sendSubview(toBack: self)
          }
        }else{
          //查看个人信息里的举报
            Utils.hideNoticeView()
        }
      }
    }
  
    @IBAction func confrimClicked(_ sender: Any) {
        var content:String?
        if self.reportInfo.text != nil && self.reportInfo.text != "" {
            content = self.reportInfo.text.trimmingCharacters(in: .whitespaces)
        }
        
      if current == 0 {
        XBHHUD.showError(NSLocalizedString("请选择举报的类型", comment: ""))
        return
      }
      
      if content != nil {
        if (content?.length)! > 200 {
          XBHHUD.showError(NSLocalizedString("举报内容太长", comment: ""))
          return
        }
      }
      
      let type = reportType.init(numberic: current)
      var repostMessage:String?
      if self.reportInfo.text == nil || self.reportInfo.text.trimmingCharacters(in: .whitespaces) == "" {
        repostMessage == ""
      }else{
        repostMessage = self.reportInfo.text
      }
      let para = ["user_id":self.peerId,
                  "type":type.report_name,
                  "message":repostMessage]
      RequestManager().post(url: RequestUrls.report, parameters: para, success: { (json) in
        XBHHUD.hide()
        XBHHUD.showSuccess(NSLocalizedString("我们致力于维护一个良好的游戏环境，对您的举报我们会严肃处理。", comment: ""))
      }) { (code, message) in
        XBHHUD.hide()
        XBHHUD.showError(message)
      }
      
      Utils.hideNoticeView()
    }
    var lastSlected:Int = 0
    var current:Int = 0
    var isKeyboardAnimation = false
    var form:formType = .GAME
    var baseHeight:CGFloat = 0
  
    override func layoutSubviews() {
      self.height = confirm.maxY + 30
      if isKeyboardAnimation == false {
        setCenter()
      }
    }

    override func awakeFromNib() {
      self.backgroundColor = UIColor.hexColor("B9C4E4")
      var rect = self.frame
      rect.size.width = Screen.width * 0.9
      self.frame = rect
      reportInfo.delegate = self
      NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.baseHeight = self.frame.origin.y
      //日语适配
      if CurrentUser.shareInstance.currentLang == "ja" {
        self.gangUp.titleLabel?.font = UIFont.systemFont(ofSize: 10)
      }
    }
  
    func setup(_ name:String) {
        self.reportName.text = name
      
    }
    func hidden() {
      Utils.hideNoticeView()
    }

    func keyBoardWillShow(notification: Notification) {
      let userInfo = notification.userInfo
      let keyBoardBounds = (userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
      isKeyboardAnimation = true
      self.cancleButton.setTitle(NSLocalizedString("关闭键盘", comment: ""), for: .normal)
      self.frame.origin.y = self.baseHeight - keyBoardBounds.size.height
    }
  
    func keyBoardWillHide(notification: Notification) {
      isKeyboardAnimation = false
      self.cancleButton.setTitle(NSLocalizedString("取消", comment: ""), for: .normal)
      setCenter()
    }
  
    deinit {
      NotificationCenter.default.removeObserver(self)
    }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      self.endEditing(true)
  }
}

extension reportView:UITextViewDelegate {
    
  func textViewDidChange(_ textView: UITextView) {
    let length = textView.text?.characters.count
    if length ?? 0 > 200 {
      XBHHUD.showError(NSLocalizedString("输入内容过长", comment: ""))
      if let text = textView.text {
        let index =  text.index(text.startIndex , offsetBy: 200)
        textView.text = text.substring(to: index)
      }
    }
  }
}
