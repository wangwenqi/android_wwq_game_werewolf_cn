//
//  SendGiftListView.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/7/11.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation

class SendGiftListView: BaseNoticeView {
  
  var modelArray = [FriendListModel]() {
    didSet {
      sendGiftController.modelArray = modelArray
    }
  }
  
  let sendGiftController = GameRoomSendGiftListViewController();
  
  @IBOutlet weak var contentView: UIView!
  
  class func view(modelArray: [FriendListModel]) -> SendGiftListView {
    let view = SendGiftListView.loadFromNib("SendGiftListView") as! SendGiftListView
    view.modelArray = modelArray
    return view
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    contentView.addSubview(sendGiftController.view)
    self.backgroundColor = UIColor.clear
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    sendGiftController.view.frame = contentView.bounds
    sendGiftController.view.height -= 8
    height = Screen.height * 0.8
    setCenter()
  }
  
  @IBAction func cancelButtonClicked(_ sender: UIButton) {
    Utils.hideNoticeView()
  }
  
  deinit {
    print("========SendGiftListView deinit==========")
  }
  
}
