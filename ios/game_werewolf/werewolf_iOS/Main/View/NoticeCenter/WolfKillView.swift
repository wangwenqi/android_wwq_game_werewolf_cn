//
//  WolfKillView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/22.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class WolfKillView: BaseNoticeView {
  
  @IBOutlet weak var titleLabel: LTMorphingLabel!
  @IBOutlet weak var ruleLabel3: UILabel!
  @IBOutlet weak var ruleLabel1: UILabel!
  @IBOutlet weak var titleImage: UIImageView!
  
  var currentSelectedView: VotePlayerCellView?
  var playerViewArray = [VotePlayerCellView]()
  var wolfTeammate = [JSON]() {
    didSet {
      for view in playerViewArray {
        for teammateNumber in wolfTeammate {
          if teammateNumber.intValue + 1 == view.tag {
            view.wolfImage.isHidden = false
          }
        }
      }
    }
  }
  
  var demon_position:Int = -1 {
    didSet {
      if demon_position != -1 {
        //有恶魔
        for view in playerViewArray {
            if demon_position+1 == view.tag {
              view.wolfImage.isHidden = false
              view.RoleIdenfity.text = NSLocalizedString("恶", comment: "")
           }
        }
      }
    }
  }
  var werewolf_king_position:Int = -1 {
    didSet{
      if werewolf_king_position != -1 {
        //有狼王
        for view in playerViewArray {
          if werewolf_king_position+1 == view.tag {
            view.wolfImage.isHidden = false
            view.RoleIdenfity.text = NSLocalizedString("王", comment: "")
          }
        }
      }
    }
  }
  
  var playerDataArray = [(Int, JSON)](){
    didSet {
      
      for i in 0..<playerDataArray.count {
        
        let view = VotePlayerCellView.view(json: playerDataArray[i])
        addSubview(view)
        playerViewArray.append(view)
        
        view.addTapGesture(self, handler: #selector(WolfKillView.didTapped(tap:)))
      }
    }
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    let margin: CGFloat = 10
    let outMargin: CGFloat = 25
    let maxColumn = 4
    let viewW: CGFloat = (self.width - outMargin * 2 - margin * CGFloat(maxColumn + 1)) / CGFloat(maxColumn)
    let viewH: CGFloat = viewW * 1.5
    
    var finalH: CGFloat = 0
    
    for (i,view) in playerViewArray.enumerated(){
      
      let lineNumber = i / maxColumn
      let columnNumber = i % maxColumn
      
      view.frame = CGRect.init(x: margin + outMargin + (margin + viewW) * CGFloat(columnNumber), y: 3 + outMargin + (margin + viewH) * CGFloat(lineNumber) + titleImage.maxY, width: viewW, height: viewH + 20)
      
      print("view.frameview.frameview.frame:::\(view.frame)");
      
      finalH = view.maxY + 120
    }
    height = finalH
    setCenter()
  }
  
  func didTapped(tap: UIGestureRecognizer) {
    
    let view = tap.view as? VotePlayerCellView
    let playerNumber = view?.tag ?? -5
    messageDispatchManager.sendMessage(type: .kill, payLoad: ["position": playerNumber - 1]);
    currentSelectedView = view
  }
  
  override func timerAction(remainTime: Int) {
    let localKill = NSLocalizedString("选择要杀的玩家", comment: "")
    titleLabel.text = "\(localKill)(\(remainTime)s)"
    titleLabel.morphingEffect = .evaporate
    if remainTime == 0 {
      Utils.hideNoticeView()
    }
  }
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.clear
    // 狼人杀人
//    MessageManager.shareInstance.onMessageReceived(messageType: .kill, delegate: self, selector: #selector(WolfKillView.kill(notification:)))
//    messageDispatchManager.onMessageReceived(messageType: .kill, delegate: self, selector: #selector(WolfKillView.kill(notification:)))
    NotificationCenter.default.addObserver(self, selector:#selector(WolfKillView.kill(notification:)), name:NSNotification.Name(rawValue: "kill"), object: nil  )
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  // 收到杀人消息
  func kill(notification: Notification) {
    let jsonData = notification.object as! JSON
    
    let killDic = jsonData["kill_info"].dictionaryValue
    
    
    for view in playerViewArray {
      view.removeOldKiller()
    }
    
    for (key, value) in killDic {
      let killedPeople = value.intValue + 1
      
      var killerView = VotePlayerCellView()
      
      for view in playerViewArray {
        if view.tag == killedPeople {
          killerView = view
        }
      }
      if let n = Int(key) {
        killerView.addKiller(playerNumber: "\(n + 1)")
      }
    }
    
    if jsonData["finished"].bool == true {
      if CurrentUser.shareInstance.currentPosition  == demon_position+1 {
        //如果自己是恶魔就直接返回
        return
      }
      Utils.delay(3.0, closure: {
        Utils.hideNoticeView()
      })
    }
  }
  
  class func view(playerDataArray: [(Int, JSON)], teammate: [JSON]) -> WolfKillView {
    let view = WolfKillView.loadFromNib("WolfKillView") as! WolfKillView
    view.playerDataArray = playerDataArray
    view.wolfTeammate = teammate
    // 修复iOS狼人杀最后四人一排时候无法选择问题
    view.layoutIfNeeded()
    return view
  }
  
}
