//
//  unfinishedGame.swift
//  game_werewolf
//
//  Created by Hang on 2017/11/1.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class unfinishedGame: BaseNoticeView {
    @IBAction func cancleGame(_ sender: Any) {
      RequestManager().post(url: RequestUrls.leaveUnfinishedGame, success: { (json) in
        
      }) { (code, message) in
      }
      Utils.hideNoticeView()
    }
    @IBAction func confirm(_ sender: Any) {
      if CurrentUser.shareInstance.loadingGame {
        MobClick.event("MultipleAccess", label:CurrentUser.shareInstance.id)
        return
      }
      XBHHUD.showLoading()
      CurrentUser.shareInstance.loadingGame = true
      NotificationCenter.default.post(
        name: NotifyName.kEnterToGameRoom.name(),
        object: nil,
        userInfo:gameInfo ?? [:])
        Utils.hideNoticeView()
    }
  
    override func awakeFromNib() {
      self.backgroundColor = UIColor.hexColor("EDEDF3")
      setCenter()
    }
  
    static func view() -> unfinishedGame {
      let view = Bundle.main.loadNibNamed("unfinishedGame", owner: nil, options: nil)?.last as! unfinishedGame
      view.frame = CGRect(x: 0, y: 0, width: Screen.width*0.8, height: 200)
      view.setCenter()
      return view
    }
    var gameInfo:[String:String]?
}
