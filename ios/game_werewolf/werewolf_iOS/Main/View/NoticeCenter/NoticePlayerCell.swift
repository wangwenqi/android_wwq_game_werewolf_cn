//
//  NoticePlayerCell.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/15.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class NoticePlayerCell: XBHView {
    
    var model = PlayerModel() {
        didSet {
            numberLabel.text = " \(self.tag) "
            
            if self.tag == 2 {
            }
        }
    }
    
    @IBOutlet weak var iconImage: XBHImageView!
    @IBOutlet weak var numberLabel: XBHLabel!

    class func cell(frame: CGRect) -> NoticePlayerCell {
        let cell = Bundle.main.loadNibNamed("NoticePlayerCell", owner: nil, options: nil)?.last as! NoticePlayerCell
        cell.frame = frame
        return cell
    }
    
    override func awakeFromNib() {
    }
}
