//
//  VotePlayerCellView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/22.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class VotePlayerCellView: XBHView {
  
  var killerArray = [String]()
  
  var killerLabelArray = [UILabel]()
  
  var jsonData = JSON.init(parseJSON: "") {
    didSet {
      nameLabel.text = jsonData["name"].stringValue
      numberLabel.text = "\(tag)"
 
      numberLabel.backgroundColor = CustomColor.roomPink
      numberLabel.textColor = UIColor.white
      if tag == CurrentUser.shareInstance.currentPosition {
        numberLabel.backgroundColor = CustomColor.newRoomYellow
        numberLabel.textColor = UIColor.darkGray
      }
      if Utils.getCurrentDeviceType() == .iPhone5 {
         numberHeight.constant = 16
         numberLabel.layer.cornerRadius = 8
      } else {
        numberHeight.constant = 20
        numberLabel.layer.cornerRadius = 10
      }
     
      numberLabel.layer.masksToBounds = true
      
      iconImage.image = UIImage.init(named: "room_head_default")
      if let imageUrl = URL.init(string: jsonData["avatar"].stringValue) {
        iconImage.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named: "room_head_default"))
      }
    }
  }
  
  var didClickedView: ((_ playerNumber: Int) -> Void)?
  
  @IBOutlet weak var tapView: UIView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var numberLabel: XBHLabel!
  @IBOutlet weak var iconImage: UIImageView!
  @IBOutlet weak var checkImage: XBHImageView!
  @IBOutlet weak var loveImage: XBHImageView!
  @IBOutlet weak var sheriffImage: XBHImageView!
  @IBOutlet weak var wolfImage: XBHImageView!
  @IBOutlet weak var numberHeight: NSLayoutConstraint!
    @IBOutlet weak var shrrifeImage: UIImageView!
    var Hidenable:Bool = true {
    didSet{
      if Hidenable == false {
        self.checkImage.isHidden = false
        let shadow = UIView()
        shadow.frame = CGRect(x: 0, y: 0, width:iconImage.width , height: iconImage.height)
        shadow.layer.cornerRadius = iconImage.width/2
        shadow.layer.masksToBounds = true
        shadow.backgroundColor = CustomColor.roomPink.withAlphaComponent(0.7)
        self.iconImage.addSubview(shadow)
      }
    }
  }
  

  lazy var RoleIdenfity:RoleLable = {
    let role = RoleLable.generateForVote()
    role.text = NSLocalizedString("狼", comment:"")
    return role
  }()
  
  override func layoutSubviews() {
    super.layoutSubviews()
    //添加lable
    let werewolfFrame = CGRect.init(x: 0, y: 0, width: wolfImage.size.width, height: wolfImage.size.height)
    RoleIdenfity.frame = werewolfFrame
    RoleIdenfity.layer.cornerRadius = wolfImage.size.height/2
    iconImage.layer.cornerRadius = self.width * 0.5
    iconImage.clipsToBounds = true
    iconImage.layer.masksToBounds = true
    iconImage.layer.borderColor = UIColor.hexColor("6E2EFF").cgColor
    if tag == CurrentUser.shareInstance.currentPosition {
      iconImage.layer.borderColor = CustomColor.newRoomYellow.cgColor
    }
    if self.Hidenable == false {
      //被保护人的bordercolor
      iconImage.layer.borderColor = CustomColor.roomPink.cgColor
    }
    iconImage.layer.borderWidth = 2
    self.bringSubview(toFront: tapView);
  }
  
  func setSelected() {
    guard Hidenable else {
      return
    }
    iconImage.layer.borderWidth = 3
    iconImage.layer.borderColor = CustomColor.roomPink.cgColor//UIColor.hexColor("F62936").cgColor
    checkImage.isHidden = false
    checkImage.animation = "zoomIn"
    checkImage.curve = "easeOut"
    checkImage.duration = 0.45
    checkImage.damping = 0.5
    checkImage.animate()
  }
  
  func removeOldKiller() {
    for label in killerLabelArray {
      label.removeFromSuperview()
    }
    killerLabelArray.removeAll()
    killerArray.removeAll()
  }
  
  func addKiller(playerNumber: String) {
    
    killerArray.append(playerNumber)
    
    for (index, number) in killerArray.enumerated() {
      let label = UILabel()
      label.backgroundColor = UIColor.hexColor("7A40FB")//UIColor.red
      label.textAlignment = .center
      label.font = numberLabel.font
      label.textColor = numberLabel.textColor
      label.text = number
      label.clipsToBounds = true
      label.adjustsFontSizeToFitWidth = true
      label.layer.cornerRadius = (numberLabel.height - 3)/2
      label.frame = CGRect.init(x: (numberLabel.height - 3 + 2) * CGFloat(index), y: nameLabel.maxY, width: numberLabel.height - 3, height: numberLabel.height - 3)//CGRect.init(x: (numberLabel.width + 2) * CGFloat(index) - numberLabel.width/2, y: iconImage.maxY - (numberLabel.width) , width: numberLabel.width, height: numberLabel.width)
      addSubview(label)
      killerLabelArray.append(label)
    }
  }
  
  func setDeSelected() {
    guard Hidenable else {
      return
    }
    iconImage.layer.borderWidth = 2
    iconImage.layer.borderColor = UIColor.hexColor("6E2EFF").cgColor
    if tag == CurrentUser.shareInstance.currentPosition {
      iconImage.layer.borderColor = CustomColor.newRoomYellow.cgColor
    }
    checkImage.isHidden = true
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    wolfImage.addSubview(RoleIdenfity)
  }
  
  class func view(json: (Int, JSON)) -> VotePlayerCellView {
    let view = VotePlayerCellView.loadFromNib("VotePlayerCellView") as! VotePlayerCellView
    view.tag = json.0
    view.jsonData = json.1
    return view
  }
}
