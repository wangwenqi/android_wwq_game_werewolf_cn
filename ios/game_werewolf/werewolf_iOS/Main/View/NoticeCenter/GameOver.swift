//
//  GameOver.swift
//  game_werewolf
//
//  Created by Hang on 2017/12/7.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

enum LangeType:String {
 case Hant = "Hant"
 case Hans = "Hans"
}

class GameOver: SpringView {
  
    var isWin = false
    var winType = ""
    var peopleArr = [(String, JSON)]()
    var werewolfArr = [(String, JSON)]()
    var thirdPardArr = [(String, JSON)]()
    var currentAva:UIImage?
    var experience:[String:JSON]?
    var winArr = [(String, JSON)]()
    var loseArr1 = [(String, JSON)]()
    var loseArr2 = [(String, JSON)]()
    var loseName1 = ""
    var loseName2 = ""
    let winTag = 10010
    let loseTag1 = 10011
    let loseTag2 = 10012
    var lange:LangeType = .Hans
    var belongTo = ""
    //游戏分享
    var webObject: UMShareWebpageObject?
    var shareImage:UIImage?
    var maxNum = 8
    var itemWidth = ((Screen.width - 10) - 7 * 10)/8 //((Screen.width - 10) - (maxNum - 1) * 10)/maxNum

    @IBOutlet weak var titleImageHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var shareTitle: UILabel!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var expLable: UILabel!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var playViewHeight: NSLayoutConstraint!
  static  func view(people: [(String, JSON)], werewolf: [(String, JSON)], thirdPard: [(String, JSON)], selfIsWin: Bool, winType: String,userIcon:UIImage,exp:[String:JSON]?) -> GameOver
  {
    
     let view = GameOverView.loadFromNib("GameOver") as! GameOver
     view.isWin = selfIsWin
     view.winType = winType
     view.peopleArr = people
     view.werewolfArr = werewolf
     view.thirdPardArr = thirdPard
     if exp != nil {
       view.experience = exp
     }

    view.currentAva = userIcon
    if winType == "people" {
      view.winArr = people
      view.loseArr1 = werewolf
      view.loseArr2 = thirdPard
      view.loseName1 = "werewolf"
      view.loseName2 = "third_party"
      
    } else if winType == "werewolf" {
      view.winArr = werewolf
      view.loseArr1 = people
      view.loseArr2 = thirdPard
      view.loseName1 = "people"
      view.loseName2 = "third_party"
    } else {
      view.winArr = thirdPard
      view.loseArr1 = people
      view.loseArr2 = werewolf
      view.loseName1 = "people"
      view.loseName2 = "werewolf"
    }

    if view.loseArr1.count != 0 && view.loseArr2.count != 0 {
       view.titleImageHeight.constant = 100
       view.layoutIfNeeded()
      if Screen.width >= 375 {
        view.titleImageHeight.constant = 170
      }else if Screen.width > 320 {
        view.titleImageHeight.constant = 150
      }
    }
    
    //判断是简体还是繁体
    if let lange = Bundle.main.preferredLocalizations.first {
      if lange.contains("Hans") {
        //简体
        view.lange = .Hans
      }else if lange.contains("Hant") {
        //繁体
        view.lange = .Hant
      }else{
        view.lange = .Hans
      }
    }
    //自己是不是赢了
    if selfIsWin {
      view.belongTo = winType
      view.shareTitle.text = NSLocalizedString("炫耀一下", comment:"")
      if exp != nil {
        if let exps = exp?["win"]?.int  {
          view.expLable.text = NSLocalizedString("经验＋\(exps)", comment: "")
        }
      }
    }else{
      //如果自己输了，从失败队伍里找
      for (i,json) in view.loseArr1 {
        if CurrentUser.shareInstance.currentPosition == json["position"].intValue + 1 {
            view.belongTo = view.loseName1
        }
      }
      for (i,json) in view.loseArr2 {
        if CurrentUser.shareInstance.currentPosition == json["position"].intValue + 1 {
          view.belongTo = view.loseName2
        }
      }
      view.shareTitle.text = NSLocalizedString("喊人报仇", comment:"")
      if exp != nil {
        if let exps = exp?["lose"]?.int  {
          view.expLable.text = NSLocalizedString("经验＋\(exps)", comment: "")
        }
      }
    }
    //改变topimage
    let topImageName = view.belongTo + (selfIsWin ? "win" : "lose") + view.lange.rawValue
    let image = UIImage.init(named: topImageName)
    if image != nil {
       view.topImage.image = image
    }
    //生成分享图片
    let myava = view.currentAva
    let localSimple = NSLocalizedString("人简单局", comment: "")
    let share = Bundle.main.loadNibNamed("shareGame", owner: nil, options: nil)?.last as! shareGame
    share.setUpShare(with: selfIsWin ? RType.WIN : RType.LOSE, gametype: view.peopleArr.count + view.werewolfArr.count + view.thirdPardArr.count == 12 ? NSLocalizedString("12人标准局", comment: "") : "\(view.peopleArr.count + view.werewolfArr.count + view.thirdPardArr.count)\(localSimple)", ava: myava)
    view.shareImage = Utils.screenShot(view: share)
    //生成个人头像数据
    //生成胜利阵营
    var maxHeight:CGFloat = 0
      maxHeight = view.generateView(winName:winType, groupDataArr: view.winArr, isWin: true, minHeight: maxHeight, tag: view.winTag,needShowFailImage:true)
    if view.loseArr1.count == 0 {
      maxHeight = view.generateView(winName: view.loseName2, groupDataArr: view.loseArr2, isWin: false, minHeight: maxHeight, tag:view.loseTag2,needShowFailImage:true)
    } else if view.loseArr2.count == 0 {
      maxHeight = view.generateView(winName: view.loseName1, groupDataArr: view.loseArr1, isWin: false, minHeight: maxHeight, tag: view.loseTag1,needShowFailImage:true)
    } else {
      maxHeight = view.generateView(winName: view.loseName1, groupDataArr: view.loseArr1, isWin: false, minHeight: maxHeight, tag: view.loseTag1,needShowFailImage:true)
      maxHeight = view.generateView(winName: view.loseName2, groupDataArr: view.loseArr2, isWin: false, minHeight: maxHeight, tag: view.loseTag2,needShowFailImage:false)
    }
    view.layoutIfNeeded()
    view.playViewHeight.constant = maxHeight
    return view
  }
  
  //生成动画
  func generateView(winName:String, groupDataArr: [(String, JSON)], isWin: Bool, minHeight: CGFloat,tag:Int,needShowFailImage:Bool) -> CGFloat {
    var maxH = minHeight
    let defaultHeight:CGFloat = itemWidth * 2 + 5 + 20 + 5 // 100 + 100 高度 100 top 100
    var totalCount = 0
    totalCount = groupDataArr.count
    //整个view
    let totalView = UIView()
    self.playerView.addSubview(totalView)
    //autolayout
    totalView.snp.makeConstraints { (make) in
      make.left.equalToSuperview()
      make.right.equalToSuperview()
      make.top.equalToSuperview().offset(minHeight)
      make.height.equalTo(defaultHeight)
    }
    //阵营字符串
    let lable = UILabel()
    lable.font = UIFont.systemFont(ofSize: 15)
    lable.adjustsFontSizeToFitWidth = true
    lable.text = getTypeName(winName)
    lable.textAlignment = .center
    totalView.addSubview(lable)
    lable.snp.makeConstraints { (make) in
      make.centerX.equalTo(totalView.snp.centerX)
      if isWin {
        make.top.equalTo(totalView.snp.top).offset(15)
      }else{
        if needShowFailImage == false {
           make.top.equalTo(totalView.snp.top).offset(15)
        }else{
          make.top.equalTo(totalView.snp.top).offset(15)
        }
      }
      make.width.equalTo(100)
      make.height.equalTo(15)
    }
    //标志image
    if  needShowFailImage != false {
      let imageView = UIImageView()
      totalView.addSubview(imageView)
      imageView.snp.makeConstraints { (make) in
        make.centerY.equalTo(lable.snp.centerY)
        make.right.equalTo(lable.snp.left).offset(10)
        make.width.equalTo(itemWidth)   //47
        make.height.equalTo(itemWidth*1.27) //60
      }
      if isWin {
        lable.textColor = UIColor.hexColor("F92D5D")
        imageView.image = UIImage.init(named: "胜利-IOS")
      }else{
        lable.textColor = UIColor.hexColor("819AFF")
        imageView.image = UIImage.init(named: "失败-IOS")
      }
    }
    
    if isWin {
      lable.textColor = UIColor.hexColor("F92D5D")
    }else{
      lable.textColor = UIColor.hexColor("819AFF")
    }
    //uicollection
    let colleciton = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    colleciton.tag = tag
    colleciton.delegate = self
    colleciton.dataSource = self
    let nib = UINib(nibName: "GameOverCell", bundle: nil)
    colleciton.register(nib, forCellWithReuseIdentifier: "gameovercell")
    colleciton.backgroundColor = UIColor.clear
    totalView.addSubview(colleciton)
    colleciton.snp.makeConstraints { (make) in
      make.centerX.equalTo(totalView.snp.centerX)
      if isWin {
        make.top.equalTo(lable.snp.bottom).offset(15
        ) //100
      }else{
        if needShowFailImage == false {
          make.top.equalTo(lable.snp.bottom).offset(5)
        }else{
          make.top.equalTo(lable.snp.bottom).offset(15)
        }
      }
        make.width.equalTo(totalCount * Int(itemWidth) + (totalCount - 1) * 10) //(totalCount * 60 + (totalCount - 1) * 10)
        make.height.equalTo(itemWidth+20)
    }
    maxH = maxH + defaultHeight
    return maxH
  }
  
  func getTypeName(_ type:String) -> String {
    switch type {
    case "people":
      return NSLocalizedString("好人阵营", comment:"")
    case "werewolf":
       return NSLocalizedString("狼人阵营", comment:"")
    default:
      return NSLocalizedString("情侣阵营", comment:"")
    }
  }
  
    @IBAction func shareTo(_ sender: Any) {
      let button = sender as? UIButton
      var PlatformType:UMSocialPlatformType = .wechatSession
      switch button!.tag {
      case 10:
        PlatformType = .wechatSession
        break
      case 11:
        PlatformType = .wechatTimeLine
        break
      case 12:
        PlatformType = .QQ
        break
      case 13:
        PlatformType = .line
        break
      case 14:
        PlatformType = .facebook
        break
      case 15:
        PlatformType = .whatsapp
        break
      default:
        break
      }
      //生成分享图片
      let message = UMSocialMessageObject()
      if let webObject = webObject {
        message.shareObject = webObject
      } else {
        let shareContent = UMShareImageObject()
        shareContent.shareImage = shareImage
        message.shareObject = shareContent
      }
      UMSocialManager.default().share(to: PlatformType, messageObject:message , currentViewController: nil, completion: { (data,error) in
        //点击分享统计
        MobClick.event("Share_\(PlatformType.description)")
        if error != nil {
          if (error! as NSError).code == 2008 {
            XBHHUD.showError(NSLocalizedString("未安装该应用", comment: ""))
          }else if (error! as NSError).code == 2005 {
            XBHHUD.showError(NSLocalizedString("分享内容不支持", comment: ""))
          }else if (error! as NSError).code == 2009 {
            
          }else if (error! as NSError).code == 2002 {
            XBHHUD.showError(NSLocalizedString("授权失败", comment: ""))
          }else if (error! as NSError).code == 2003 {
            XBHHUD.showError(NSLocalizedString("分享失败", comment: ""))
          }
        }else{
          if let sendType = Utils.getTaskType(platform: PlatformType, type: .normal) {
            print(NSLocalizedString("----------发送数据:\(sendType);;;count:\(1)", comment: ""));
            RequestManager().post(
              url: RequestUrls.sendTaskData,
              parameters: ["type": sendType, "count": 1],
              success: { (response) in
            }, error: { (code, message) in
            });
          }
        }
      })
    }
    @IBAction func confirmClicked(_ sender: Any) {
     self.animation = "fadeOut"
     self.animate()
     self.removeFromSuperview()
   }
}

extension GameOver:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    switch collectionView.tag {
    case 10010:
      return winArr.count
    case 10011:
      return loseArr1.count
    case 10012:
      return loseArr2.count
    default:
      return 0
    }
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gameovercell", for: indexPath) as! GameOverCollectionViewCell
    var json:(String, JSON)?
    if collectionView.tag == 10010 {
      json = winArr[indexPath.row]
    }else if collectionView.tag == 10011 {
      json = loseArr1[indexPath.row]
    }else if collectionView.tag == 10012 {
      json = loseArr2[indexPath.row]
    }
    if json != nil {
      cell.config(json!)
    }
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
  }
  
  //layout
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width:itemWidth, height:itemWidth+20) //60,100 //itemWidth
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 7
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 10
  }
}
