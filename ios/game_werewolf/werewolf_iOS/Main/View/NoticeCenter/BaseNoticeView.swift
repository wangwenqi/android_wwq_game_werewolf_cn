//
//  BaseNoticeView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/15.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class BaseNoticeView: SpringView {

  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    self.endEditing(true)
  }
  
    var timer = Timer()
    
    private var remainTime = 0
    
    var liveTime = 0 {
        
        didSet {
            timer.invalidate()
            
            liveTime -= 1
            remainTime = liveTime
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(BaseNoticeView.tAction), userInfo: nil, repeats: true)
        }
    }
    
    final func tAction() {
        timerAction(remainTime: remainTime)
        
        if remainTime <= 0 {
            timer.invalidate()
        }
        
        remainTime -= 1
    }
    
    func timerAction(remainTime: Int) {
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initial()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initial()
    }

    func initial() {
        self.width = Screen.width * 0.85
        self.height = 400
        self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
        self.backgroundColor = #colorLiteral(red: 0.2399157584, green: 0.118033506, blue: 0.3918479681, alpha: 1)
        self.layer.cornerRadius = 6
    }
    
    func setCenter() {
        self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        timer.invalidate()
    }
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
        timer.invalidate()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
  

  
  
  func hiddenNotice() {
    Utils.hideNoticeView()
  }
}
