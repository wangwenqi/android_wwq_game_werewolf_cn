//
//  GrabRoleView.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/6/21.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation
import UIKit;

class GrabRoleView: BaseNoticeView {
  @IBOutlet weak var titleLabel: LTMorphingLabel!

    @IBOutlet weak var leftCardImageView: UIImageView!
    
    @IBOutlet weak var rightCardImageView: UIImageView!
  
  @IBOutlet weak var middleCardImageView: UIImageView!
  @IBOutlet weak var leftSelectRoleButton: UIButton!
  
  @IBOutlet weak var rightSelectRoleButton: UIButton!

  @IBOutlet weak var middleSelectRoleButton: UIButton!
  var rightFinished = false
  var leftFinished = false
  var middleFinished = false
  
  var jsonData:JSON = [:];
  
    @IBAction func leftButtonClicked(_ sender: Any) {
      messageDispatchManager.sendMessage(type: .apply_role, payLoad: ["role": jsonData["left"].string])
    }
    
   @IBAction func rightButtonClicked(_ sender: Any) {
       setButtonText(role: jsonData["right"].string!, buttonText: NSLocalizedString("抢", comment: ""));
      messageDispatchManager.sendMessage(type: .apply_role, payLoad: ["role": jsonData["right"].string])
   }
  
  @IBAction func middleButtonClicked(_ sender: Any) {
    setButtonText(role: jsonData["middle"].string!, buttonText: NSLocalizedString("抢", comment: ""));
    messageDispatchManager.sendMessage(type: .apply_role, payLoad: ["role": jsonData["middle"].string])
  }
  
  class func view(json: JSON) -> GrabRoleView {
    let roleView = UIView.loadFromNib("GrabRoleView") as? GrabRoleView;
    if let view = roleView,
      let left = json["left"].string,
      let right = json["right"].string {
      let middle = json["middle"].string
      view.jsonData = json
      let leftImage = GameRole.getCardImageName(name: left)
      let rightImage = GameRole.getCardImageName(name: right)
      let middleImage = GameRole.getCardImageName(name: middle!)
      view.leftCardImageView.image = leftImage
      view.rightCardImageView.image = rightImage
      view.middleCardImageView.image = middleImage
      
      view.height = 300
      view.checkButtonStatus(name: left, buttonText: NSLocalizedString("抢", comment: ""));
      view.checkButtonStatus(name: right, buttonText: NSLocalizedString("抢", comment: ""));
       view.checkButtonStatus(name: middle!, buttonText: NSLocalizedString("抢", comment: ""));
      return view;
    }
    
    return GrabRoleView();
  }
  
  func setLRFinished(role:String) {
    if let left = jsonData["left"].string,
      left == role {
      leftFinished = true
    } else if let right = jsonData["right"].string,
      right == role {
      rightFinished = true
    }else{
      middleFinished = true
    }
  }
  
  func setAllFinish() {
    rightFinished = true
    middleFinished = true
    leftFinished = true
    self.rightCardImageView.alpha = 0.5;
    self.rightSelectRoleButton.isEnabled = false;
    self.leftCardImageView.alpha = 0.5;
    self.leftSelectRoleButton.isEnabled = false;
    self.middleCardImageView.alpha = 0.5;
    self.middleSelectRoleButton.isEnabled = false;
  }
  
  func setButtonText(role:String, buttonText:String ) {
    if let left = jsonData["left"].string,
      left == role {
      if leftFinished {
        return
      }
      self.leftCardImageView.alpha = 0.5;
      self.leftSelectRoleButton.isEnabled = false;
      self.leftSelectRoleButton.setTitle(buttonText, for: .disabled);
    } else if let right = jsonData["right"].string,
      right == role{
      if rightFinished {
        return
      }
      self.rightCardImageView.alpha = 0.5;
      self.rightSelectRoleButton.isEnabled = false;
      self.rightSelectRoleButton.setTitle(buttonText, for: .disabled);
    }else{
      if middleFinished {
        return
      }
      self.middleCardImageView.alpha = 0.5;
      self.middleSelectRoleButton.isEnabled = false;
      self.middleSelectRoleButton.setTitle(buttonText, for: .disabled);
    }
  }
  
  func checkButtonStatus(name:String, buttonText:String = NSLocalizedString("已抢光", comment: "")) {
    if !checkIsAllGrabed(name: name) {
      print("\(buttonText)")
      setButtonText(role: name, buttonText: buttonText);
    }
  }
  
  fileprivate func checkIsAllGrabed(name:String) -> Bool {
    for data in CurrentUser.shareInstance.cachedCardData {
      // TODO 忽略时间
      if name == data["type"].string {
        return true;
      }
    }
    return false;
  }
  
  override func awakeFromNib() {
    self.backgroundColor = UIColor.clear
  }
  
  override func timerAction(remainTime: Int) {
    let localRole = NSLocalizedString("抢身份", comment: "")
    titleLabel.text = "\(localRole)(\(remainTime)s)"
    titleLabel.morphingEffect = .evaporate
    if remainTime == 0 {
      Utils.hideNoticeView()
    }
  }
  
}
