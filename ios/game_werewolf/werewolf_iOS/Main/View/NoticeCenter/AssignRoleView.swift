//
//  AssignRoleView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/20.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class AssignRoleView: BaseNoticeView {
    
    @IBOutlet weak var titleLabel: LTMorphingLabel!
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var detailLabel1: UILabel!
    @IBOutlet weak var detailLabel2: UILabel!
    
    @IBAction func affirmButtonClicked(_ sender: Any) {
        Utils.hideNoticeView()
    }

    class func view(json: JSON) -> AssignRoleView {
        let view = AssignRoleView.loadFromNib("AssignRoleView") as! AssignRoleView
        
        if json["duration"].intValue / 1000 != 0 {
            view.liveTime = json["duration"].intValue / 1000
        }
        
        switch GameRole.getRole(name: json["role"].stringValue) {
        case .werewolf:
            view.detailLabel1.text = NSLocalizedString("每个夜晚可以选择杀掉一人，白天隐匿身份陷害其他人", comment: "")
            view.detailLabel2.text = NSLocalizedString("杀掉除狼人以外的所有人", comment: "")
            view.titleImageView.image = UIImage.init(named: "狼人")
        case .seer:
            view.detailLabel1.text = NSLocalizedString("每个夜晚可以查验一位玩家的真实身份，查验结果为“狼人”或“好人”", comment: "")
            view.detailLabel2.text = NSLocalizedString("找出并杀掉所有狼人", comment: "")
            view.titleImageView.image = UIImage.init(named:"预言家")
        case .people:
            view.detailLabel1.text = NSLocalizedString("没有任何特殊技能，根据他人发言来判断其他玩家身份", comment: "")
            view.detailLabel2.text = NSLocalizedString("找出并杀掉所有狼人", comment: "")
            view.titleImageView.image = UIImage.init(named:"平民")
        case .witch:
            view.detailLabel1.text = NSLocalizedString("有两瓶药水。一瓶是解药，可以救活当天晚上被杀的玩家；另一瓶是毒药，可以在晚上毒死任意一位玩家。女巫在同一晚上不能同时使用两瓶药", comment: "")
            view.detailLabel2.text = NSLocalizedString("找出并杀掉所有狼人", comment: "")
            view.titleImageView.image = UIImage.init(named:"女巫")
        case .hunter:
            view.detailLabel1.text = NSLocalizedString("当“猎人”死亡时，可以开枪射杀一位玩家（俗称“带走”）。但死于女巫毒药时，不能开枪", comment: "")
            view.detailLabel2.text = NSLocalizedString("找出并杀掉所有狼人", comment: "")
            view.titleImageView.image = UIImage.init(named:"猎人")
        case .cupid:
          view.detailLabel1.text = NSLocalizedString("爱神可以在第一天指定任意两个（包括自己）成为情侣，情侣中有一人死去时，另一人将自动殉情", comment: "")
          view.detailLabel2.text = NSLocalizedString("如果射中的情侣是人人情侣或狼狼情侣，则爱神获胜方式和好人阵营相同；如果射中的情侣是人狼情侣，则爱神和情侣组成情侣阵营，需要除掉其他所有玩家", comment: "")
          view.titleImageView.image = UIImage.init(named:"爱神")
        case .iguard:
          view.detailLabel1.text = NSLocalizedString("每个夜晚可以守护一人不被狼人杀害；如果守护的人被女巫使用了解药，该玩家仍然会死亡", comment: "")
          view.detailLabel2.text = NSLocalizedString("找出并杀掉所有狼人", comment: "")
          view.titleImageView.image = UIImage.init(named:"守卫")
        case .werewolf_king:
          view.detailLabel1.text = NSLocalizedString("每个夜晚可以选择杀掉一人；白天的时候，可以在别人发言时，选择自爆，带走场上任意一名玩家。白狼王自爆后，游戏直接进入黑夜", comment: "")
          view.detailLabel2.text = NSLocalizedString("杀掉除狼人以外的所有人", comment: "")
          view.titleImageView.image = UIImage.init(named:"白狼王")
        case .magician:
          view.detailLabel1.text = NSLocalizedString("每晚可交换两名玩家的号码牌，使这两名玩家在夜间受到的技能（狼刀，预言家验人，女巫毒药，猎人夜间开的枪）互换；魔术师无法互换上一夜互换过的两张牌", comment: "")
          view.detailLabel2.text =  NSLocalizedString("找出并杀掉所有狼人", comment: "")
          view.titleImageView.image = UIImage.init(named: "魔术师卡")
        case .demon:
          view.detailLabel1.text = NSLocalizedString("恶魔每天在刀人之后，会单独验证一名玩家的身份，验证其是否为神职；恶魔不会死在夜间（女巫的毒，猎人在夜间开的枪），恶魔不能自爆", comment: "")
          view.detailLabel2.text =  NSLocalizedString("杀掉除狼人以外的所有人", comment: "")
          view.titleImageView.image = UIImage.init(named: "恶魔卡")
        default:
            break
        }
        
        return view
    }
    
    override func timerAction(remainTime: Int) {

        let show  = NSLocalizedString("你的身份", comment: "")
        let remain = "(\(remainTime)s)"
        titleLabel.text = show + remain
        if remainTime == 0 {
            Utils.hideNoticeView()
        }
    }
    
    override func awakeFromNib() {
        titleLabel.morphingEffect = .evaporate
         self.backgroundColor = UIColor.clear
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
         self.height = detailLabel2.maxY + 150
        setCenter()
    }
    
    deinit {
        timer.invalidate()
    }
}
