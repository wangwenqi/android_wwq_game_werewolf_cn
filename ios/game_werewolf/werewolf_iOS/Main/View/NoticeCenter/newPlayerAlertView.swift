
//
//  newPlayerAlertView.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import TTGTagCollectionView

protocol GiftSysDelegate:class {
  func showGift(with:PlayerView)
  func showSendGift(with:JSON)
}

extension GiftSysDelegate {
  func showSendGift(with:JSON){}
  func showGift(with:PlayerView) {}
}

class newPlayerAlertView: BaseNoticeView {
  
  enum viewType {
    case MASTER
    case NORMAL
    case GAMERECORD
  }

    @IBOutlet weak var ava: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var level: UILabel!
    @IBOutlet weak var sex: UIImageView!
    @IBOutlet weak var tagView: TTGTextTagCollectionView!
    @IBOutlet weak var addToBlackList: UIButton!
    @IBOutlet weak var report: UIButton!
    @IBOutlet weak var addFriend: UIButton!
    @IBOutlet weak var sendGift: UIButton!
    @IBOutlet weak var kickOut: UIButton!
    @IBOutlet weak var lock: UIButton!
    @IBOutlet weak var userID: UILabel!
    
    @IBOutlet weak var win: UILabel!
    @IBOutlet weak var lose: UILabel!
    @IBOutlet weak var winRate: UILabel!
    @IBOutlet weak var escapeRate: UILabel!
    @IBOutlet weak var contentView: UIView!
  
    @IBOutlet weak var familyCover: UIView!
    @IBOutlet weak var familyNotice: UILabel!
    @IBOutlet weak var userInfoButton: UIButton!
    @IBOutlet weak var familyNumber: UILabel!
    @IBOutlet weak var familyName: UILabel!
    @IBOutlet weak var familyImage: UIImageView!
    @IBOutlet weak var familyInfoButton: UIButton!
    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var clickbleView: UIView!
    
  @IBOutlet weak var sendGiftCenterX: NSLayoutConstraint!
  @IBOutlet weak var sendGiftPadding: NSLayoutConstraint!
    @IBOutlet weak var sendGiftWidthConstrant: NSLayoutConstraint!
    @IBOutlet weak var contentTopConstranit: NSLayoutConstraint!
    //人气值
    @IBOutlet weak var popularLable: UILabel!
    @IBAction func addFriendClicked(_ sender: Any) {
      Utils.hideAnimated()
      let user_id = playerView.playerData["id"].stringValue
      let position = playerView.playerData["position"].stringValue
      //在黑名单中的不能添加好友
      if let block = Utils.getCachedBLContent() {
        if user_id != nil {
          if block[user_id] != nil {
            XBHHUD.showError("此人已经被你拉黑")
            return
          }
        }else{
          return
        }
      }
      if CurrentUser.shareInstance.isTourist {
        XBHHUD.showError(NSLocalizedString("登录后加好友", comment: ""))
        return
      }
      RequestManager().post(url: RequestUrls.friendAdd, parameters: ["friend_id":playerView.playerData["id"].stringValue], success: { (json) in
         //发送socket请求
        messageDispatchManager.sendMessage(type:.add_friend, payLoad: ["user_id":user_id,"position":position])
        XBHHUD.showTextOnly(text: NSLocalizedString("好友请求发送成功！", comment: ""))
      }) { (code, message) in
        if message == "" {
          XBHHUD.showTextOnly(text: NSLocalizedString("好友请求发送失败", comment: ""))
        } else {
          XBHHUD.showTextOnly(text: "\(message)")
        }
      }
    }
  
    @IBAction func sendGiftClicked(_ sender: Any) {
      
      if CurrentUser.shareInstance.is_observer {
         XBHHUD.showError(NSLocalizedString("观战中不能赠送礼物", comment: ""))
         return
      }
      if type == .GAMERECORD {
        if CurrentUser.shareInstance.isTourist {
          XBHHUD.showError(NSLocalizedString("登录后赠送礼物", comment: ""))
          return
        }
        if self.isTourist {
          XBHHUD.showError(NSLocalizedString("对方登录后可赠送礼物", comment: ""))
          return
        }

      }else{
        
        if CurrentUser.shareInstance.isTourist {
          XBHHUD.showError(NSLocalizedString("登录后赠送礼物", comment: ""))
          return
        }
        if self.isTourist {
          XBHHUD.showError(NSLocalizedString("对方登录后可赠送礼物", comment: ""))
          return
        }
        if CurrentUser.shareInstance.isDeath {
          XBHHUD.showError(NSLocalizedString("您已经死亡，不能赠送礼物", comment: ""))
          return
        }
        if CurrentUser.shareInstance.isNight {
          XBHHUD.showError(NSLocalizedString("天黑了，不能赠送礼物", comment: ""))
          return
        }
      }
      var ids:String?
      if type == .GAMERECORD {
        ids =  gameRecordID!
      }else{
        ids =  playerView.playerData["id"].string
      }
      if let block = Utils.getCachedBLContent() {
        if ids != nil {
          if block[ids] != nil {
            XBHHUD.showError("此人已经被你拉黑")
            return
          }
        }else{
          return
        }
      }
      if type == .GAMERECORD {
        if let json = self.jsonData {
           self.delegate?.showSendGift(with:json)
        }
      }else{
         self.delegate?.showGift(with: playerView)
      }
      Utils.hideNoticeView()
     
    }
    
    @IBAction func kickOutClicked(_ sender: Any) {
      messageDispatchManager.sendMessage(type: .kick_out, payLoad: ["position": playerView.tag - 1])
      Utils.hideNoticeView()
    }
  
    @IBAction func lockClicked(_ sender: Any) {
      if CurrentUser.shareInstance.currentRoomType == "simple" {
        messageDispatchManager.sendMessage(type: .kick_out, payLoad: ["position": playerView.tag - 1])
        messageDispatchManager.sendMessage(type: .lock, payLoad: ["position": playerView.tag - 1])
        Utils.hideNoticeView()
      } else {
        let view = NormalAlertView.view(type: .oneButton, title: NSLocalizedString("提示", comment: ""), detail: NSLocalizedString("该模式下不可开启或锁定座位", comment: ""))
        Utils.showAlertView(view: view)
      }
    }
    @IBAction func profileClicked(_ sender: Any) {
      
      if type == .GAMERECORD {
        if jsonData == nil {
          XBHHUD.showError("无法查看游客的信息")
          return
        }
        let controller = UserInfoViewController()
        controller.peerID = (jsonData?["id"].stringValue)!
        controller.peerSex = sexStr == nil ? "1" : sexStr!//playerView.playerData["sex"].stringValue
        controller.peerName = (jsonData?["name"].stringValue)!
        controller.peerIcon = jsonData?["image"].string ?? ""
        controller.isFriend = self.isFriend
        controller.isTourist = self.isTourist
        controller.isself = self.isMyself
        Utils.hideNoticeView()
        let nav = RotationNavigationViewController.init(rootViewController: controller)
        ((Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)!) as UIViewController).present(nav, animated: true, completion: nil)
      
      }else{
        let controller = UserInfoViewController()
        let myself = CurrentUser.shareInstance.currentPosition == playerView.playerData["position"].intValue + 1;
        controller.peerID = playerView.playerData["id"].stringValue
        controller.peerSex = sexStr == nil ? "1" : sexStr!//playerView.playerData["sex"].stringValue
        controller.peerName = playerView.playerData["name"].stringValue
        controller.peerIcon = playerView.playerData["avatar"].stringValue
        controller.isFriend = self.isFriend
        controller.isTourist = self.isTourist
        controller.isself = myself
        Utils.hideNoticeView()
        let nav = RotationNavigationViewController.init(rootViewController: controller)
        ((Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)!) as UIViewController).present(nav, animated: true, completion: nil)
      }
    }
    @IBAction func addToBLClicked(_ sender: Any) {
      
      Utils.hideNoticeView();
      XBHHUD.showLoading()
      var ids:String?
      if type == .GAMERECORD {
        ids =  gameRecordID!
      }else{
        ids =  playerView.playerData["id"].string
      }
      if type == .GAMERECORD {
        ids =  gameRecordID!
      }else{
        ids =  playerView.playerData["id"].string
      }
      if let block = Utils.getCachedBLContent() {
        if ids != nil {
          if block[ids] != nil {
            XBHHUD.hide()
            XBHHUD.showError("此人已经被你拉黑")
            return
          }
        }else{
           XBHHUD.hide()
           return
        }
      }
      if let id = ids {
        RequestManager().get(url: RequestUrls.block + "/\(id)", success: { (json) in
          XBHHUD.hide()
          XBHHUD.showSuccess(NSLocalizedString("拉黑成功！", comment: ""))
          Utils.addToBL(id: id)
        }) { (code, message) in
          XBHHUD.hide()
          XBHHUD.showError(NSLocalizedString("拉黑失败！", comment: ""))
        }
      }else{
        XBHHUD.hide()
        XBHHUD.showError(NSLocalizedString("拉黑失败！", comment: ""))
      }
    }
    
    @IBAction func reportClicked(_ sender: Any) {
      
      let report = Bundle.main.loadNibNamed("reportView", owner: nil, options: nil)?.last as! reportView
      report.peerId = playerView.playerData["id"].stringValue
      report.setup(self.name.text!)
      self.superview?.addSubview(report)
      self.superview?.sendSubview(toBack: report)
      self.animation = "fadeOut"
      self.animateNext {
          self.superview?.sendSubview(toBack: self)
      }
    }
  
    var sexStr:String?
    weak var delegate:GiftSysDelegate?
    var gameRecordID:String? {
      didSet{
        if gameRecordID != nil && gameRecordID != "" {
            getUeserInfo()
        }
      }
    }
  
    var type:viewType = .NORMAL {
      didSet{
        if type == .NORMAL || type == .GAMERECORD {
          kickOut.isHidden = true
          lock.isHidden = true
          addFriend.isHidden = true
          report.isHidden = true
          addToBlackList.isHidden = true
          sendGift.isHidden = true
        }
      }
    }
  
    var isFriend = true {
      didSet{
        let isSelfInfo:Bool?
        if type == .GAMERECORD {
          isSelfInfo = self.isMyself
        }else{
          isSelfInfo = CurrentUser.shareInstance.currentPosition == playerView.playerData["position"].intValue + 1
        }
        // 如果不是自己
        if !isSelfInfo! {
          report.isHidden = false
          addToBlackList.isHidden = false
          sendGift.isHidden = false
        }
        
        if !isSelfInfo! && !isFriend {
          addFriend.isHidden = false
//          self.sendGiftPadding.isActive = false
        } else {
          addFriend.isHidden = true
           //不是好友也不是自己 此时送礼物button要重写layout
//            self.sendGiftWidthConstrant.isActive = false
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.3, options: UIViewAnimationOptions(rawValue: 0), animations: {
            self.sendGiftCenterX.constant = 0
            self.layoutIfNeeded()
            }, completion: {  finished in
          
            })
        
          
        }
        //现在能给自己发
        if isSelfInfo! {
          if type == .GAMERECORD {
            sendGift.isHidden = true
          }else{
            sendGift.isHidden = false
          }
          isMyself = true 
        }
        layoutSubviews()
      }
  
    }
  
    var playerView = PlayerView() {
      didSet{
        self.name.text = playerView.playerData["name"].stringValue
        self.level.text = playerView.playerData["level"].stringValue
        self.ava.image = UIImage.init(named: "room_head_default")
        if let iconUrl = URL.init(string: playerView.playerData["avatar"].stringValue) {
          ava.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
        }
        
        // 请求用户信息
        RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id":playerView.playerData["id"].stringValue], success: { [weak self] (json) in
            
          self?.userInfoButton.isEnabled = true
          let winCount = json["game"]["win"].intValue
          let loseCount = json["game"]["lose"].intValue
          let totalCount = winCount + loseCount
          self?.isFriend = json["is_friend"].boolValue
          
          let sex = json["sex"].stringValue
          self?.sexStr = sex
          if Int(sex) == 1 {
            self?.sex.image = UIImage.init(named: "ic_male")
          } else {
            self?.sex.image = UIImage.init(named: "ic_female")
          }
          //id
          if let userid = json["uid"].int {
              self?.userID.text = "ID:\(userid)"
              self?.userID.isHidden = false
            if Utils.getCurrentDeviceType() == .iPhone4 ||   Utils.getCurrentDeviceType() == .iPhone5 {
              self?.userID.font = UIFont.systemFont(ofSize: 9)
            }
          }else{
              self?.userID.isHidden = true
          }
          //人气值
          if let popular = json["popular"].int {
            self?.popularLable.text = "\(popular)"
          }
          //添加🏷️
          var tags:[String] = []
          let roleType = json["role"]["type"].intValue
          var defaultTag = ""
          switch roleType {
          case 1:
            defaultTag = "管理员"
          case 2:
            defaultTag = "老师"
          case 3:
            defaultTag = "管理员"
            tags.append("老师")
          default:
            defaultTag = ""
          }
          if defaultTag != "" {
            tags.append(defaultTag)
          }
          if let roleCustom = json["role"]["custom"].string {
            if roleCustom.isNotEmpty {
              tags.append(roleCustom)
            }
          }
          if tags.count != 0 {
            for str in tags {
              let index = tags.index(of: str)
              let config = TTGTextTagConfig()
              config.tagTextFont = UIFont.boldSystemFont(ofSize: 13)
              config.tagTextColor = UIColor.hexColor("2d1c4c")
              config.tagBorderWidth = 1
              config.tagBorderColor = UIColor.hexColor("7a64ff")
              config.tagExtraSpace = CGSize(width: 10, height: 10)
              config.tagCornerRadius = 10
              config.tagShadowColor = UIColor.clear
              let colorIndex = index! % 3
              if colorIndex == 0 {
                config.tagBackgroundColor = UIColor.hexColor("50d7ff")
                config.tagSelectedBackgroundColor = UIColor.hexColor("50d7ff")
              }else if colorIndex == 1 {
                config.tagBackgroundColor = UIColor.hexColor("fdff00")
                config.tagSelectedBackgroundColor = UIColor.hexColor("fdff00")
              }else if colorIndex == 2 {
                config.tagBackgroundColor = UIColor.hexColor("ff78ff")
                config.tagSelectedBackgroundColor = UIColor.hexColor("ff78ff")
              }
              self?.tagView.addTag(str, with: config)
              self?.tagView.scrollDirection = .horizontal
            }
          }
          //个人称号
          if let active = json["active"].dictionary {
            //添加点击事件
            let tap = UITapGestureRecognizer()
            tap.numberOfTapsRequired = 1
            tap.addTarget(self, action: #selector(self?.showBattleDes))
            self?.clickbleView.addGestureRecognizer(tap)
            var titleLevel = ""
            var levels = 0
            var types = ""
            if let title = active["title"]?.string {
              titleLevel = title
            }
            if let level = active["star"]?.int {
              levels = level
            }
            if let type = active["type"]?.string {
              types = type
            }
            if types.hasPrefix("villager") || types.hasPrefix("village_head") || types.hasPrefix("sheriff") {
              self?.titleLable.backgroundColor = UIColor.hexColor("ffe680")
              self?.titleLable.textColor = UIColor.hexColor("8c4600")
              self?.titleLable.layer.borderWidth = 1
              self?.titleLable.layer.borderColor = UIColor.hexColor("8c4600").cgColor
            }else if types.hasPrefix("guard") || types.hasPrefix("hunter") || types.hasPrefix("paladin") {
              self?.titleLable.backgroundColor = UIColor.hexColor("e8c9a1")
              self?.titleLable.textColor = UIColor.hexColor("621900")
              self?.titleLable.layer.borderWidth = 1
              self?.titleLable.layer.borderColor = UIColor.hexColor("621900").cgColor
            }else if types.hasPrefix("witch") || types.hasPrefix("seer") || types.hasPrefix("priest") {
              self?.titleLable.backgroundColor = UIColor.hexColor("d3e6ff")
              self?.titleLable.textColor = UIColor.hexColor("4c6390")
              self?.titleLable.layer.borderWidth = 1
              self?.titleLable.layer.borderColor = UIColor.hexColor("4c6390").cgColor
            }else if types.hasPrefix("bishop") || types.hasPrefix("red_bishop") || types.hasPrefix("pontiff") {
              self?.titleLable.backgroundColor = UIColor.hexColor("fff000")
              self?.titleLable.textColor = UIColor.hexColor("644000")
              self?.titleLable.layer.borderWidth = 1
              self?.titleLable.layer.borderColor = UIColor.hexColor("644000").cgColor
            }else if types.hasPrefix("angel") ||  types.hasPrefix("demon") || types.hasPrefix("cupid"){
              self?.titleLable.backgroundColor = UIColor.hexColor("ff2307")
              self?.titleLable.textColor = UIColor.hexColor("ffffb1")
              self?.titleLable.layer.borderWidth = 1
              self?.titleLable.layer.borderColor = UIColor.hexColor("660006").cgColor
            }else if types.hasPrefix("sun_god") || types.hasPrefix("pluto") || types.hasPrefix("zeus") {
              self?.titleLable.backgroundColor = UIColor.hexColor("550000")
              self?.titleLable.textColor = UIColor.hexColor("ffb91e")
              self?.titleLable.layer.borderWidth = 1
              self?.titleLable.layer.borderColor = UIColor.hexColor("ffb91e").cgColor
            }
            let levalImageTitle = types + "Level"
            let levelTitles = titleLevel + String(levels) + "星"
            if let image = UIImage.init(named: levalImageTitle) {
              self?.titleImage.image = image
            }
            self?.titleLable.text = " \(levelTitles) "
          }
          //家族信息
          if let family = json["group"].dictionary {
            self?.familyCover.isHidden = true
            if let number = family["gid"]?.int {
              self?.familyNumber.text = "\(number)"
            }
            if let name = family["short_name"]?.string {
                self?.familyName.text = "【" + "\(name)" + "】" + "家族"
            }
            if let levelImage = family["level_image"]?.string {
              let url = URL.init(string: levelImage)
              self?.familyImage.sd_setImage(with: url, placeholderImage: UIImage.init(named:"勋章初级图标"))
            }
            
            if let level_val = family["level_val"]?.int {
              let level  = level_val > 10 ? 10 : level_val
              self?.familyImage.image = UIImage.init(named: "level\(level)")
            }
            if let familyid = family["group_id"]?.string {
              self?.familyId = familyid
            }
            
          }else{
            if self?.isMyself == false {
             self?.familyNotice.text = NSLocalizedString("没有加入任何家族", comment: "")
            }
            self?.familyNotice.isHidden = false
          }
          self?.level.text = "LV.\(json["game"]["level"].intValue)"
          let total = "\(totalCount)"
          let win  = "\(winCount)"
          let lose = "\(loseCount)"
          let final = total + " " + win + " " + lose
          let att = NSMutableAttributedString(string: final)
          let escape = json["game"]["escape"].intValue
          att.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSMakeRange(0, total.length))
          att.addAttribute(NSForegroundColorAttributeName, value: UIColor.hexColor("5193EB"), range:NSMakeRange(total.length+1, win.length))
          att.addAttribute(NSForegroundColorAttributeName, value: UIColor.hexColor("EC3C1A"), range:NSMakeRange(total.length+1+win.length+1, lose.length))
          self?.win.text = win
          self?.lose.text = lose
          if winCount == 0 && loseCount == 0 {
            self?.winRate.text = "0%"
            self?.escapeRate.text = "0%"
          } else {
            self?.winRate.text = "\(lroundf((Float(winCount) / (Float(totalCount) + Float(escape))) * Float(100.0)))%"
            self?.escapeRate.text = "\(lroundf((Float(escape) / (Float(totalCount) + Float(escape))) * Float(100.0)))%"
          }
          self?.layoutSubviews()
        }) { [weak self] (code, message) in
          self?.userInfoButton.isEnabled = true
          let winCount = 0;
          let loseCount = 0;
          let totalCount = winCount + loseCount
          self?.isFriend = true;
          self?.isTourist = true
          self?.level.text = "LV.\(1)"
          let total = NSLocalizedString("狼人杀\(totalCount)局", comment: "")
          let win  = NSLocalizedString("胜\(winCount)局", comment: "")
          let lose = NSLocalizedString("败\(loseCount)局", comment: "")
          let final = total + " " + win + " " + lose
          self?.winRate.text = "0%"
          self?.escapeRate.text = "0%"
          self?.layoutSubviews()
        }
      }
    }
  
    var isTourist = false
    var isMyself = false
    var familyId = ""
    static func generatePlayerInfoView(type:viewType,player:PlayerView) -> newPlayerAlertView{
      
      let view = Bundle.main.loadNibNamed("newPlayerAlertView", owner: nil, options: nil)?.last as! newPlayerAlertView
      view.type = type
      view.playerView = player
      return view
    }
  
  func showBattleDes() {
    let web = StoreCenterViewController()
    web.type = .BattleLevel
    let nav = RotationNavigationViewController(rootViewController: web)
    Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)?.present(nav, animated: true, completion: nil)
//    UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
  }
  
  static func generatePlayerInfoView(type:viewType,gameRecordID:String) -> newPlayerAlertView {
    
    let view = Bundle.main.loadNibNamed("newPlayerAlertView", owner: nil, options: nil)?.last as! newPlayerAlertView
    view.type = type
    view.gameRecordID = gameRecordID
    view.isMyself = view.gameRecordID == CurrentUser.shareInstance.id
    return view
  }
  
  // 图片显示器
  
  var photoArr:[MWPhoto] = NSMutableArray() as! [MWPhoto]
  var borwser = MWPhotoBrowser()
  
  // 显示图片代理
  weak var showPhoto:showPhotoDelegate?
  
  var jsonData:JSON?
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.width = Screen.width-10
    self.backgroundColor = UIColor.clear//UIColor.hexColor("7A6AFB")
    let myself = CurrentUser.shareInstance.currentPosition == playerView.playerData["position"].intValue + 1;
    if type == .NORMAL || type == .GAMERECORD {
      self.height = contentView.maxY  + 40 + 20 + 20 + 30
    }else{
      self.height = contentView.maxY + 40 + 20 + 20 + 40 + 30 + 30
    }
    if type == .GAMERECORD {
      if isMyself {
        self.height = contentView.maxY  + 20 + 30 + 30
      }
    }else{
      if myself {
        self.height = contentView.maxY + 20 + 40 + 20 + 30
      }
    }
    setCenter()
  }

  override func awakeFromNib() {
//    self.radius(20)
    self.userInfoButton.isEnabled = false
    self.userInfoButton.imageView?.contentMode = .scaleAspectFit
    self.familyInfoButton.imageView?.contentMode = .scaleAspectFit
    self.ava.layer.borderColor = UIColor.hexColor("7A6AFB").cgColor
    self.ava.layer.borderWidth = 3
    self.addFriend.layer.shadowOffset = CGSize(width: 0, height: 5)
    self.addFriend.layer.shadowColor = UIColor.hexColor("C44211").cgColor
    self.sendGift.layer.shadowOffset = CGSize(width: 0, height: 8)
    self.sendGift.layer.shadowColor = UIColor.hexColor("299758").cgColor
    self.sendGift.layer.masksToBounds = false
    self.kickOut.layer.shadowOffset = CGSize(width: 0, height: 10)
    self.kickOut.layer.shadowColor = UIColor.hexColor("443371").cgColor
    self.lock.layer.shadowOffset = CGSize(width: 0, height: 12)
    self.lock.layer.shadowColor = UIColor.hexColor("443371").cgColor
    //举报按钮
    self.report.imageView?.contentMode = .scaleAspectFit
    self.report.imageEdgeInsets = UIEdgeInsetsMake(2, 2, 2, 2)
    
    //图片浏览
    borwser.displayActionButton = false
    borwser.displayNavArrows = false
    borwser.displayActionButton = false
    borwser.delegate = self
    self.width = Screen.width
    //添加点击事件
    self.ava.isUserInteractionEnabled = true
    let tap = UITapGestureRecognizer()
    tap.numberOfTouchesRequired = 1
    tap.numberOfTapsRequired = 1
    tap.addTarget(self, action: #selector(showPhoto(tap:)))
    self.ava.addGestureRecognizer(tap)
  }
  func showPhoto(tap:UITapGestureRecognizer) {
    if self.ava.image == nil {
      XBHHUD.showError(NSLocalizedString("头像为空", comment: ""))
      return
    }
    photoArr.append(MWPhoto(image: self.ava.image))
    let nav = RotationNavigationViewController(rootViewController: borwser)
    Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)?.present(nav, animated: true, completion: nil)
  }
  
  func hidden() {
    Utils.hideNoticeView()
  }
 
  @IBAction func hidden(_ sender: Any) {
    hidden()
  }
    
  @IBAction func familyInfo(_ sender: Any) {
    if CurrentUser.shareInstance.isTourist {
      XBHHUD.showError(NSLocalizedString("注册后才能查看家族信息", comment:""))
      return
    }
    XBHHUD.showLoading()
    let para = ["maxUser":"30"]
    RequestManager().get(url:RequestUrls.groupInfo + "\(familyId)",parameters:para, success: { (json) in
      XBHHUD.hide()
      let sb = UIStoryboard.init(name: "FamilyDetail", bundle: nil)
      let familyDetail = sb.instantiateViewController(withIdentifier: "FamilyDetail") as! FamilyDetailViewController
      familyDetail.datas = json
      let nav = RotationNavigationViewController.init(rootViewController: familyDetail)
      ((Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)!) as UIViewController).present(nav, animated: true, completion: nil)
    }) { (code, message) in
      XBHHUD.hide()
      XBHHUD.showError(message)
    }
  }
    
  func getUeserInfo() {
  
      RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id":gameRecordID!], success: { [weak self] (json) in
        self?.jsonData = json
        self?.userInfoButton.isEnabled = true
        let winCount = json["game"]["win"].intValue
        let loseCount = json["game"]["lose"].intValue
        let totalCount = winCount + loseCount
        self?.isFriend = json["is_friend"].boolValue
        if let name = json["name"].string {
            self?.name.text = name
        }
        self?.ava.image = UIImage.init(named: "room_head_default")
        if let iconUrl = URL.init(string: json["image"].stringValue) {
          self?.ava.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
        }
        //id
        if let userid = json["uid"].int {
            self?.userID.text = "ID:\(userid)"
            self?.userID.isHidden = false
        }else{
            self?.userID.isHidden = true
        }
        //人气值
        if let popular = json["popular"].int {
          self?.popularLable.text = "\(popular)"
        }
        let sex = json["sex"].stringValue
        self?.sexStr = sex
        if Int(sex) == 1 {
          self?.sex.image = UIImage.init(named: "ic_male")
        } else {
          self?.sex.image = UIImage.init(named: "ic_female")
        }
        //添加🏷️
        var tags:[String] = []
        let roleType = json["role"]["type"].intValue
        var defaultTag = ""
        switch roleType {
        case 1:
          defaultTag = "管理员"
        case 2:
          defaultTag = "老师"
        case 3:
          defaultTag = "管理员"
          tags.append("老师")
        default:
          defaultTag = ""
        }
        if defaultTag != "" {
          tags.append(defaultTag)
        }
        if let roleCustom = json["role"]["custom"].string {
          if roleCustom.isNotEmpty {
            tags.append(roleCustom)
          }
        }
        if tags.count != 0 {
          for str in tags {
            let index = tags.index(of: str)
            let config = TTGTextTagConfig()
            config.tagTextFont = UIFont.boldSystemFont(ofSize: 13)
            config.tagTextColor = UIColor.hexColor("2d1c4c")
            config.tagBorderWidth = 0
            config.tagBorderColor = UIColor.clear
            config.tagExtraSpace = CGSize(width: 5, height: 5)
            config.tagCornerRadius = 9
            config.tagShadowColor = UIColor.clear
            let colorIndex = index! % 3
            if colorIndex == 0 {
              config.tagBackgroundColor = UIColor.hexColor("ff9ec8")
              config.tagSelectedBackgroundColor = UIColor.hexColor("ff9ec8")
            }else if colorIndex == 1 {
              config.tagBackgroundColor = UIColor.hexColor("B5E4FD")
              config.tagSelectedBackgroundColor = UIColor.hexColor("B5E4FD")
            }else if colorIndex == 2 {
              config.tagBackgroundColor = UIColor.hexColor("FEEB35")
              config.tagSelectedBackgroundColor = UIColor.hexColor("FEEB35")
            }
            self?.tagView.addTag(str, with: config)
            self?.tagView.scrollDirection = .horizontal
          }
        }
        //个人称号
        if let active = json["active"].dictionary {
          let tap = UITapGestureRecognizer()
          tap.numberOfTapsRequired = 1
          tap.addTarget(self, action: #selector(self?.showBattleDes))
          self?.clickbleView.addGestureRecognizer(tap)
          var titleLevel = ""
          var levels = 0
          var types = ""
          if let title = active["title"]?.string {
            titleLevel = title
          }
          if let level = active["star"]?.int {
            levels = level
          }
          if let type = active["type"]?.string {
            types = type
          }
          if types.hasPrefix("villager") || types.hasPrefix("village_head") || types.hasPrefix("sheriff") {
            self?.titleLable.backgroundColor = UIColor.hexColor("ffe680")
            self?.titleLable.textColor = UIColor.hexColor("8c4600")
            self?.titleLable.layer.borderWidth = 1
            self?.titleLable.layer.borderColor = UIColor.hexColor("8c4600").cgColor
          }else if types.hasPrefix("guard") || types.hasPrefix("hunter") || types.hasPrefix("paladin") {
            self?.titleLable.backgroundColor = UIColor.hexColor("e8c9a1")
            self?.titleLable.textColor = UIColor.hexColor("621900")
            self?.titleLable.layer.borderWidth = 1
            self?.titleLable.layer.borderColor = UIColor.hexColor("621900").cgColor
          }else if types.hasPrefix("witch") || types.hasPrefix("seer") || types.hasPrefix("priest") {
            self?.titleLable.backgroundColor = UIColor.hexColor("d3e6ff")
            self?.titleLable.textColor = UIColor.hexColor("4c6390")
            self?.titleLable.layer.borderWidth = 1
            self?.titleLable.layer.borderColor = UIColor.hexColor("4c6390").cgColor
          }else if types.hasPrefix("bishop") || types.hasPrefix("red_bishop") || types.hasPrefix("pontiff") {
            self?.titleLable.backgroundColor = UIColor.hexColor("fff000")
            self?.titleLable.textColor = UIColor.hexColor("644000")
            self?.titleLable.layer.borderWidth = 1
            self?.titleLable.layer.borderColor = UIColor.hexColor("644000").cgColor
          }else if types.hasPrefix("angel") ||  types.hasPrefix("demon") || types.hasPrefix("cupid"){
            self?.titleLable.backgroundColor = UIColor.hexColor("ff2307")
            self?.titleLable.textColor = UIColor.hexColor("ffffb1")
            self?.titleLable.layer.borderWidth = 1
            self?.titleLable.layer.borderColor = UIColor.hexColor("660006").cgColor
          }else if types.hasPrefix("sun_god") || types.hasPrefix("pluto") || types.hasPrefix("zeus") {
            self?.titleLable.backgroundColor = UIColor.hexColor("550000")
            self?.titleLable.textColor = UIColor.hexColor("ffb91e")
            self?.titleLable.layer.borderWidth = 1
            self?.titleLable.layer.borderColor = UIColor.hexColor("ffb91e").cgColor
          }
          let levalImageTitle = types + "Level"
          let levelTitles = titleLevel + String(levels) + "星"
          if let image = UIImage.init(named: levalImageTitle) {
            self?.titleImage.image = image
          }
          self?.titleLable.text = " \(levelTitles) "
        }
        //家族信息
        if let family = json["group"].dictionary {
            self?.familyCover.isHidden = true
            if let number = family["gid"]?.int {
                self?.familyNumber.text = "\(number)"
            }
            if let name = family["short_name"]?.string {
                self?.familyName.text = "【" + "\(name)" + "】" + "家族"
            }
            if let levelImage = family["level_image"]?.string {
              let url = URL.init(string: levelImage)
              self?.familyImage.sd_setImage(with: url, placeholderImage: UIImage.init(named:"勋章初级图标"))
            }
            if let familyid = family["group_id"]?.string {
              self?.familyId = familyid
            }
        }else{
          if self?.isMyself == false {
            self?.familyNotice.text = NSLocalizedString("没有加入任何家族", comment: "")
          }
          self?.familyNotice.isHidden = false
        }

        self?.level.text = "LV.\(json["game"]["level"].intValue)"
        let total = "\(totalCount)"
        let win  = "\(winCount)"
        let lose = "\(loseCount)"
        let final = total + " " + win + " " + lose
        let att = NSMutableAttributedString(string: final)
        let escape = json["game"]["escape"].intValue
        att.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSMakeRange(0, total.length))
        att.addAttribute(NSForegroundColorAttributeName, value: UIColor.hexColor("5193EB"), range:NSMakeRange(total.length+1, win.length))
        att.addAttribute(NSForegroundColorAttributeName, value: UIColor.hexColor("EC3C1A"), range:NSMakeRange(total.length+1+win.length+1, lose.length))
        self?.win.text = win
        self?.lose.text = lose
        if winCount == 0 && loseCount == 0 {
          self?.winRate.text = "0%"
          self?.escapeRate.text = "0%"
        } else {
          self?.winRate.text = "\(lroundf((Float(winCount) / (Float(totalCount) + Float(escape))) * Float(100.0)))%"
          self?.escapeRate.text = "\(lroundf((Float(escape) / (Float(totalCount) + Float(escape))) * Float(100.0)))%"
        }
        self?.layoutSubviews()
      }) { [weak self] (code, message) in
        self?.userInfoButton.isEnabled = true
        let winCount = 0;
        let loseCount = 0;
        self?.ava.image = UIImage.init(named: "room_head_default")
        let totalCount = winCount + loseCount
        self?.isFriend = true;
        self?.isTourist = true
        self?.level.text = "LV.\(1)"
        let total = NSLocalizedString("狼人杀\(totalCount)局", comment: "")
        let win  = NSLocalizedString("胜\(winCount)局", comment: "")
        let lose = NSLocalizedString("败\(loseCount)局", comment: "")
        let final = total + " " + win + " " + lose
        self?.winRate.text = "0%"
        self?.escapeRate.text = "0%"
        self?.layoutSubviews()
      }
    }
    
    deinit {
        print("Player deinit")
    }
}

//MARK: photo browser

extension newPlayerAlertView :MWPhotoBrowserDelegate {
  
  func numberOfPhotos(in photoBrowser: MWPhotoBrowser!) -> UInt {
    return UInt(self.photoArr.count)
  }
  
  func photoBrowser(_ photoBrowser: MWPhotoBrowser!, photoAt index: UInt) -> MWPhotoProtocol! {
    if index < UInt(self.photoArr.count) {
      return self.photoArr[Int(index)] as! MWPhotoProtocol
    }else{
      return nil
    }
  }
  
  func photoBrowser(_ photoBrowser: MWPhotoBrowser!, didDisplayPhotoAt index: UInt) {
    
  }
  
}

