//
//  SeerCheckView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/23.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class SeerCheckView: BaseNoticeView {
  @IBOutlet weak var giveUpPoison: UIButton!
  
  @IBOutlet weak var conformButton: UIButton!
  @IBOutlet weak var poison: UIButton!
  @IBOutlet weak var titleLabel: LTMorphingLabel!
  
  var posionBlock: ((_ position: Int) -> Void)?
  
  var type: GameMessageType?
  
  var currentSelectedView: VotePlayerCellView?
  var currentSelectedViewArray = [VotePlayerCellView]()
  var lovers = [Int]()
  var sheriff = -5
  var teamMate = [JSON]()
  var lastProtect:Int?
  var lastExchange:[JSON]?
  var playerViewArray = [VotePlayerCellView]()
  var playerDataArray = [(Int, JSON)](){
    didSet {
      
      for i in 0..<playerDataArray.count {
        
        let view = VotePlayerCellView.view(json: playerDataArray[i])
        addSubview(view)
        
        for i in lovers {
          if view.tag == i {
            view.loveImage.isHidden = false
          }
        }
        
        if view.tag == sheriff {
          view.sheriffImage.isHidden = false
        }
        
        
        for wolf in teamMate {
          if view.tag == wolf.intValue + 1 {
            view.wolfImage.isHidden = false
          }
        } 
        
        playerViewArray.append(view)
        
        view.tapView.addTapGesture(self, handler: #selector(SeerCheckView.didTapped(tap:)))
      }
    }
  }
  
  var demon_position:Int = -1 {
    didSet {
      if demon_position != -1 {
        //有恶魔
        for view in playerViewArray {
          if demon_position + 1  == view.tag {
            view.wolfImage.isHidden = false
            view.RoleIdenfity.text = NSLocalizedString("恶", comment: "")
          }
        }
      }
    }
  }
  var werewolf_king_position:Int = -1 {
    didSet {
      if werewolf_king_position != -1 {
        //有狼王
        for view in playerViewArray {
          if werewolf_king_position + 1 == view.tag {
            view.wolfImage.isHidden = false
            view.RoleIdenfity.text = NSLocalizedString("王", comment: "")
          }
        }
      }
    }
  }
  
  func checkTitle() {
    
    if werewolf_king_position != -1 {
      //有狼王
      for view in playerViewArray {
        if werewolf_king_position == view.tag {
          view.wolfImage.isHidden = false
          view.RoleIdenfity.text = NSLocalizedString("王", comment: "")
        }
      }
    }
    if demon_position != -1 {
      //有恶魔
      for view in playerViewArray {
        if demon_position + 1  == view.tag {
          view.wolfImage.isHidden = false
          view.RoleIdenfity.text = NSLocalizedString("恶", comment: "")
        }
      }
    }
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    let margin: CGFloat = 10
    let outMargin: CGFloat = 25
    let maxColumn = 4
    let viewW: CGFloat = (self.width - outMargin * 2 - margin * CGFloat(maxColumn + 1)) / CGFloat(maxColumn)
    let viewH: CGFloat = viewW * 1.6
    
    var finalH: CGFloat = 0
    let t = playerDataArray
    
    for (i,view) in playerViewArray.enumerated() {
      let lineNumber = i / maxColumn
      let columnNumber = i % maxColumn

      view.frame = CGRect.init(x: margin + outMargin + (margin + viewW) * CGFloat(columnNumber), y: margin + outMargin + (viewH) * CGFloat(lineNumber) + titleLabel.maxY, width: viewW, height: viewH)

      print("view.frameview.frameview.frame:::\(view.frame)");
      
      finalH = view.maxY + 120
    }
    height = finalH
    print("heightheightheightheightheight:\(height)")
    setCenter()
    
  }
  
  override func timerAction(remainTime: Int) {
    var local = ""
    if type == .check {
      local = NSLocalizedString("选择要查验的玩家", comment: "")
//      titleLabel.text = "\(localCheck)(\(remainTime)s)"
    } else if type == .poison {
      local = NSLocalizedString("选择要投毒的玩家", comment: "")
//      titleLabel.text = "\(localPoison)(\(remainTime)s)"
    } else if type == .take_away {
      local = NSLocalizedString("选择要枪杀的玩家", comment: "")
//      titleLabel.text = "\(localKill)(\(remainTime)s)"
    } else if type == .link {
      local = NSLocalizedString("选择两名玩家成为情侣", comment: "")
//      titleLabel.text = "\(localLink)(\(remainTime)s)"
    } else if type == .vote {
      local = NSLocalizedString("请投票", comment: "")
//      titleLabel.text = "\(localVote)(\(remainTime)s)"
    } else if type == .hand_over {
      local = NSLocalizedString("择玩家接替警长", comment: "")
//      titleLabel.text = "\(localP)(\(remainTime)s)"
    }else if type == .protect {
      local = NSLocalizedString("选择你要守卫的玩家", comment: "")
//      titleLabel.text = "\(localP)(\(remainTime)s)"
    }else if type == .boom_away {
      local = NSLocalizedString("选择你杀的玩家", comment: "")
//      titleLabel.text = "\(localP)(\(remainTime)s)"
    }else if type == .exchange {
      local = NSLocalizedString("选择两名交换的玩家", comment:"")
//      titleLabel.text = "\(localDes)(\(remainTime)s)"
    }else if type == .demon_check {
      local = NSLocalizedString("选择要查验的玩家", comment: "")
//      titleLabel.text = "\(localCheck)(\(remainTime)s)"
    }
    let remain = "(\(remainTime)s)"
    let total = "\(local)\(remain)"
//    let str = NSAttributedString.init(string:total)
//    let showRange = NSRange(location: 0, length:local.length)
//    let remainRange = NSRange(location: local.length , length: remain.length)
//    str.attributes([NSFontAttributeName:UIFont.systemFont(ofSize: 14, weight: UIFontWeightThin)], range:remainRange)
//    str.attributes([NSFontAttributeName:UIFont.systemFont(ofSize: 14, weight: UIFontWeightHeavy)], range:showRange)
    titleLabel.text = total
    titleLabel.morphingEffect = .evaporate
    
    if remainTime == 0 {
      Utils.hideNoticeView()
    }
  }
  
  func didTapped(tap: UIGestureRecognizer) {
    if type == .link || type == .exchange {
      let view = tap.view?.superview as? VotePlayerCellView
      view?.setSelected()
      
      for v in currentSelectedViewArray {
        if v === view {
          return
        }
      }
      
      currentSelectedViewArray.append(view!)
      
      if currentSelectedViewArray.count > 2 {
        let view = currentSelectedViewArray.remove(at: 0)
        view.setDeSelected()
      }
      
    } else {
      
      let view = tap.view?.superview as? VotePlayerCellView
      //守卫不能选择上一个选中的
      if lastProtect != nil {
        if view?.tag == lastProtect! + 1 {
          return
        }
      }
      view?.setSelected()
      
      if currentSelectedView !== view {
        currentSelectedView?.setDeSelected()
      }
      
      currentSelectedView = view
    }
    
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.clear
    if Screen.width > 320 {
      titleLabel.font = UIFont.systemFont(ofSize: 14)
    }else{
      titleLabel.font = UIFont.systemFont(ofSize: 12)
    }
  }
  
  func lastGuard() {
    if lastProtect != nil {
      for view in playerViewArray {
        if view.tag == lastProtect! + 1 {
          view.Hidenable = false
        }
      }
    }
  }
  
  func lastExchangeView() {
    if lastExchange != nil {
      for view in playerViewArray {
        for last in lastExchange! {
          if let exchange = last.int {
            if view.tag == exchange + 1 {
              //设置为已经选择过
              view.Hidenable = false
            }
          }
        }
      }
    }
  }
  
  //守卫
  class func guardView(type: GameMessageType,lastProtect:Int, playerDataArray: [(Int, JSON)], lovers: [Int], sheriff: Int, wolfArr: [JSON]) -> SeerCheckView {
    
      let protectView = view(type: type, playerDataArray: playerDataArray, lovers: lovers, sheriff: sheriff, wolfArr: wolfArr)
      protectView.lastProtect = lastProtect
      return protectView
  }
  
  class func magicanView(type: GameMessageType,lastExchange:[JSON], playerDataArray: [(Int, JSON)], lovers: [Int], sheriff: Int, wolfArr: [JSON]) -> SeerCheckView {
    let magicanView = view(type: type, playerDataArray: playerDataArray, lovers: lovers, sheriff: sheriff, wolfArr: wolfArr)
    magicanView.lastExchange = lastExchange
    return magicanView
  }
  
  class func view(type: GameMessageType, playerDataArray: [(Int, JSON)], lovers: [Int], sheriff: Int, wolfArr: [JSON]) -> SeerCheckView {
    let view = SeerCheckView.loadFromNib("SeerCheckView") as! SeerCheckView
    view.lovers = lovers
    view.sheriff = sheriff
    view.teamMate = wolfArr
    view.playerDataArray = playerDataArray
    view.type = type
    
    if type == .check || type == .card_check || type == .demon_check {
      view.titleLabel.text = NSLocalizedString("选择要查验身份的玩家", comment: "")
    } else if type == .poison {
      view.titleLabel.text = NSLocalizedString("选择要投毒的玩家", comment: "")
      view.conformButton.isHidden = true
      view.poison.isHidden = false
      view.giveUpPoison.isHidden = false
    } else if type == .vote {
      view.titleLabel.text = NSLocalizedString("请投票", comment: "")
      view.conformButton.isHidden = true
      view.poison.isHidden = false
      view.giveUpPoison.isHidden = false
    } else if type == .take_away {
      view.titleLabel.text = NSLocalizedString("请选择要枪杀的玩家", comment: "")
      view.conformButton.isHidden = true
      view.poison.isHidden = false
      view.giveUpPoison.isHidden = false
    } else if type == .link {
      view.titleLabel.text = NSLocalizedString("选择两名玩家成为情侣", comment: "")
      view.conformButton.isHidden = false
      view.poison.isHidden = true
      view.giveUpPoison.isHidden = true
    } else if type == .hand_over {
      view.titleLabel.text = NSLocalizedString("选择玩家接替警长", comment: "")
      view.conformButton.isHidden = true
      view.poison.isHidden = false
      view.giveUpPoison.isHidden = false
    }else if type == .protect {
      view.titleLabel.text = NSLocalizedString("选择你要守卫的玩家", comment: "")
      view.conformButton.isHidden = true
      view.poison.isHidden = false
      view.giveUpPoison.isHidden = false
    }else if type == .boom_away {
      view.titleLabel.text = NSLocalizedString("请选择要杀的玩家", comment: "")
      view.conformButton.isHidden = true
      view.poison.isHidden = false
      view.giveUpPoison.isHidden = false
    }else if type == .exchange {
      view.titleLabel.text = NSLocalizedString("选择两名交换的玩家", comment: "")
      view.conformButton.isHidden = false
      view.poison.isHidden = true
      view.giveUpPoison.isHidden = true
    }

    // 修复iOS8无法点击待查验/待杀人头像
    view.layoutIfNeeded()
    return view
  }
  
  @IBAction func poisonClicked(_ sender: Any) {
    
    guard let playerNumber = currentSelectedView?.tag else { return }
    
    if type == .poison {
      messageDispatchManager.sendMessage(type: .poison, payLoad: ["position": playerNumber - 1])
      
      if posionBlock != nil {
        posionBlock!(playerNumber)
      }
    } else if type == .vote {
      messageDispatchManager.sendMessage(type: .vote, payLoad: ["position": playerNumber - 1])
    } else if type == .take_away {
      messageDispatchManager.sendMessage(type: .take_away, payLoad: ["position": playerNumber - 1])
    } else if type == .hand_over {
      messageDispatchManager.sendMessage(type: .hand_over, payLoad: ["position": playerNumber - 1])
    }else if type == .protect {
      messageDispatchManager.sendMessage(type: .protect, payLoad: ["position": playerNumber - 1])
    }else if type == .boom_away {
      messageDispatchManager.sendMessage(type: .boom_away, payLoad: ["position": playerNumber - 1])
    }
    Utils.hideNoticeView()
  }
  
  @IBAction func giveUpPoisonClicked(_ sender: Any) {
    if type == .poison {
      messageDispatchManager.sendMessage(type: .poison)
    } else if type == .vote {
      messageDispatchManager.sendMessage(type: .vote)
    } else if type == .take_away {
      messageDispatchManager.sendMessage(type: .take_away)
    }else if type == .boom_away {
       messageDispatchManager.sendMessage(type: .boom_away)
    }else if type == .exchange {
       messageDispatchManager.sendMessage(type: .exchange)
    }
    Utils.hideNoticeView()
  }
  
  @IBAction func conformButtonClicked(_ sender: Any) {
    
    if type == .link {
      guard currentSelectedViewArray.count >= 2 else { return }
      
      messageDispatchManager.sendMessage(type: .link, payLoad: ["positions": [currentSelectedViewArray[0].tag - 1, currentSelectedViewArray[1].tag - 1]]);
      
      Utils.hideNoticeView()
      // 弹出确认框
      let view = linkResultView.view(type: .conform, player1: (currentSelectedViewArray[0].tag, currentSelectedViewArray[0].jsonData), player2: (currentSelectedViewArray[1].tag, currentSelectedViewArray[1].jsonData))
      view.liveTime = 10
      Utils.showNoticeView(view: view);
    } else if type == .exchange {
      
      guard currentSelectedViewArray.count >= 2 else { return }
      messageDispatchManager.sendMessage(type: .exchange, payLoad: ["positions": [currentSelectedViewArray[0].tag - 1, currentSelectedViewArray[1].tag - 1]])
      Utils.hideNoticeView()
    
    }else if type == .demon_check{
      let playerNumber = currentSelectedView?.tag ?? -5
      if (playerNumber == -5) {
        return;
      }
     messageDispatchManager.sendMessage(type: .demon_check, payLoad: ["position": playerNumber - 1]);
      Utils.hideNoticeView()
    }else{
      var sendType:GameMessageType = .check;
      let playerNumber = currentSelectedView?.tag ?? -5
      if type == .card_check {
        sendType = .card_check;
        if (playerNumber == -5) {
          return;
        }
      }
      messageDispatchManager.sendMessage(type: sendType, payLoad: ["position": playerNumber - 1]);
      Utils.hideNoticeView()
    }
  }
}
