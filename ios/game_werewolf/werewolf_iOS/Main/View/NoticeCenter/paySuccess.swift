//
//  paySuccess.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/10.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class paySuccess: BaseNoticeView {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var paySuccess: UIImageView!
  
    override func awakeFromNib() {
      DispatchQueue.main.asyncAfter(deadline: .now() + 2) { 
        Utils.hideNoticeView()
      }
      let noti = Notification.Name(rawValue: "PAYSS")
      NotificationCenter.default.post(name: noti, object: nil)
    }
}
