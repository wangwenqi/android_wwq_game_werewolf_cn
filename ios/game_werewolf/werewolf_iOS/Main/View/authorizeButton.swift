//
//  authorizeButton.swift
//  game_werewolf
//
//  Created by Hang on 2017/7/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class authorizeButton: UIButton {

  var type:PermissionType?
  var status:PermissionStatus = .unknown  {
    didSet {
      if status == .authorized {
        self.isSelected = true
        
      }else if status == .unknown {
        
      }else {
         self.isEnabled = false
      }
    }
  }
}
