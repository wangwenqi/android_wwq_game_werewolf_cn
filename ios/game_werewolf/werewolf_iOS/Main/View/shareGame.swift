//
//  shareGame.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/12.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

enum RType  {
    case WIN
    case LOSE
}

class shareGame: UIView {

    @IBOutlet weak var bg: UIImageView!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var bottom: UIImageView!
    @IBOutlet weak var ava: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!

    
    override func awakeFromNib() {
        self.name.text = CurrentUser.shareInstance.name
        self.ava.layer.borderColor = UIColor.black.cgColor
        self.ava.layer.borderWidth = 1
        self.ava.layer.masksToBounds = true
        self.ava.layer.cornerRadius = self.ava.frame.width * 0.5
        self.frame = CGRect(x: 0, y: 0, width: 540, height: 761)
    }
    
  func setUpShare(with:RType,gametype:String,ava:UIImage?) {
      switch with {
        case .LOSE:
          //几人局
          self.title.text = gametype
        
        case .WIN:
          self.bg.image = UIImage(named: "share_bg2")
          self.topImage.image = UIImage(named: "share_head2")
          self.bottom.image = UIImage(named: "share_head22")
          self.title.text = gametype
          self.subTitle.text = NSLocalizedString("带领团队走向胜利", comment: "")
        default: break
        
      }
      self.ava.image = ava
    }
}
