//
//  FinishTaskView.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/21.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import ObjectMapper

enum ImageType: String {
  case seer           // 预言家
  case witch          // 女巫
  case werewolf       // 狼人
  case gold           // 金币
  case unKnown        // 未知类型
//  case people// 平民
  func image() -> UIImage? {
    switch self {
    case .seer:
      return UIImage(named: "message_gift_seer")
    case .witch:
      return UIImage(named: "message_gift_witch")
    case .werewolf:
      return UIImage(named: "message_gift_werewolf")
    case .gold:
      return UIImage(named: "gold")
    case .unKnown:
      return nil
    }
  }
}

class FinishTaskView: BaseNoticeView {

  let successImage = UIImageView(image: UIImage(named: "finish_task_top"))  // 成功图片
  let successImageLabel = UILabel()                                         // 领取成功/领取失败
  let whiteBackView = UIView.init(frame: CGRect.zero)                       // 白色背景
  let successLabel = UILabel()                                              // 您已领取成功提示
  let makeSureButton = UIButton(type: .system)                              // 确定按钮, 点击消失
  var viewArray = [TaskCardView]()                                          // 金币卡片 view数组
  var addArray = [UILabel]()                                                // + view数组
  var taskCardViewCenterY: CGFloat = 35                                     // 领取金币时候的位置
  // 新增轮盘部分
  let infoLabel = UILabel()
  
  var kWidth: CGFloat = 0                                                   // 金币400 + 女巫卡 + 狼人卡 宽度
  static let kScale: CGFloat = UIScreen.main.bounds.width / 640             // 暂时考虑按照比例缩放显示
  static let edgeLeftSpace: CGFloat = 30
  static let viewWidth = UIScreen.main.bounds.width * 0.85
  static let whiteWidth = viewWidth - edgeLeftSpace * 2
  static let imageHeight = viewWidth * 283 / 592                            // 领取金币图片比例 592 : 283
  static let iconHeifht = 56 * kScale                                       // 金币icon/女巫卡高度
  static let iconAndGold = 118 * kScale
  static let labelAddColor = UIColor(hex: "#5a595a")
  static let edgeItemSapce = 20 * kScale
  
  var dataSource: [[String: Any]]? {
    didSet {
      if let data = dataSource {
        self.modelArray = [FinishTaskViewModel]()
        for dic in data {
          if let model = Mapper<FinishTaskViewModel>().map(JSON: dic) {
            modelArray?.append(model)
          }
        }
      }
    }
  }
  var modelArray: [FinishTaskViewModel]?
  
  // 转盘抽奖的样子
  class func taskLottoView(_ dataSource: [[String: Any]]?, _ txt: String?, _ result: Bool, _ showAd: Bool, _ nativeAd: FBNativeAd?) -> FinishTaskView {
    let finishTaskView = FinishTaskView()
    finishTaskView.setViewFrames(showAd, nativeAd: nativeAd)
    finishTaskView.dataSource = dataSource
    finishTaskView.taskCardViewCenterY = 0
    
    if let array = finishTaskView.modelArray, !array.isEmpty {
      finishTaskView.kWidth = CGFloat((array.count - 1) * 10)
      for model in array {
        switch model.cardType {
        case .gold:
          finishTaskView.kWidth += 118
          let view = TaskCardView(model.cardType,
                                  model.count,
                                  CGRect(x: 0, y: 0, width: iconAndGold, height: iconHeifht))
          finishTaskView.addSubview(view)
          finishTaskView.viewArray.append(view)
        case .seer, .werewolf, .witch:
          finishTaskView.kWidth += 56
          let view = TaskCardView(model.cardType,
                                  CGRect(x: 0, y: 0, width: iconHeifht, height: iconHeifht))
          finishTaskView.addSubview(view)
          finishTaskView.viewArray.append(view)
        case .unKnown:
          finishTaskView.kWidth += 56
          let view = TaskCardView(model,
                                  CGRect(x: 0, y: 0, width: iconHeifht, height: iconHeifht))
          finishTaskView.addSubview(view)
          finishTaskView.viewArray.append(view)
        }
      }
    }
    
    finishTaskView.successImageLabel.text = result
      ? NSLocalizedString("恭喜中奖", comment: "")
      : NSLocalizedString("很遗憾没有中奖", comment: "")
    finishTaskView.infoLabel.text = result
      ? "手气不错, 获得\(txt ?? "")"
      : "很遗憾, 今天手气不佳, 明天继续"
    finishTaskView.infoLabel.textColor = result
      ? UIColor(hex: "#e0aa03")
      : UIColor(hex: "#828282")
    finishTaskView.setNeedsLayout()
    
    return finishTaskView
  }
  
  // 领金币的样子
  class func taskView(_ dataSource: [[String: Any]]?, _ showAd: Bool, _ nativeAd: FBNativeAd?) -> FinishTaskView {
    let finishTaskView = FinishTaskView()
    finishTaskView.setViewFrames(showAd, nativeAd: nativeAd)
    finishTaskView.dataSource = dataSource
    if let array = finishTaskView.modelArray, !array.isEmpty {
      finishTaskView.kWidth = CGFloat((array.count - 1) * 10)
      for model in array {
        switch model.cardType {
        case .gold:
          finishTaskView.kWidth += 118
          let view = TaskCardView(model.cardType,
                                  model.count,
                                  CGRect(x: 0, y: 0, width: iconAndGold, height: iconHeifht))
          finishTaskView.addSubview(view)
          finishTaskView.viewArray.append(view)
        case .seer, .werewolf, .witch:
          finishTaskView.kWidth += 56
          let view = TaskCardView(model.cardType,
                                  CGRect(x: 0, y: 0, width: iconHeifht, height: iconHeifht))
          finishTaskView.addSubview(view)
          finishTaskView.viewArray.append(view)
        case .unKnown:
          finishTaskView.kWidth += (model.count > 1) ? 118 : 56
          let view = TaskCardView(model,
                                  CGRect(x: 0, y: 0, width: iconHeifht, height: iconHeifht))
          finishTaskView.addSubview(view)
          finishTaskView.viewArray.append(view)
        }
      }
      for index in 0..<array.count {
        if index != 0 {
          let label = UILabel()
          label.text = "+"
          label.textColor = labelAddColor
          finishTaskView.kWidth += 20
          finishTaskView.addSubview(label)
          finishTaskView.addArray.append(label)
        }
      }
    }
    finishTaskView.successLabel.text = NSLocalizedString("恭喜您成功领取奖励", comment: "")
    finishTaskView.successImageLabel.text = NSLocalizedString("领取成功", comment: "")
    finishTaskView.setNeedsLayout()
    return finishTaskView
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    whiteBackView.backgroundColor = UIColor.hexColor("#fbfaf0")
    whiteBackView.layer.cornerRadius = 6
    successImageLabel.font = UIFont.boldSystemFont(ofSize: 32 * FinishTaskView.kScale)
    successImageLabel.textColor = UIColor(hex: "#5d301b")
    successLabel.font = UIFont.boldSystemFont(ofSize: 27 * FinishTaskView.kScale)
    successLabel.textColor = UIColor(hex: "#e0aa03")
    successLabel.textAlignment = .center
    infoLabel.font = UIFont.boldSystemFont(ofSize: 27 * FinishTaskView.kScale)
    infoLabel.textAlignment = .center
    self.addSubview(whiteBackView)
    self.addSubview(successImage)
    self.addSubview(successLabel)
    self.addSubview(successImageLabel)
    self.addSubview(infoLabel)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func initial() { }
  
  func setViewFrames(_ showAd: Bool, nativeAd: FBNativeAd?) {
    self.width = Screen.width * 0.85
    self.backgroundColor = UIColor.clear
    successImage.frame = CGRect(x: 0, y: 0, width: FinishTaskView.viewWidth , height: FinishTaskView.imageHeight)
    successLabel.frame = CGRect(x: 0, y: FinishTaskView.imageHeight, width: FinishTaskView.viewWidth, height: 20)
    if showAd {
      self.height = FinishTaskView.whiteWidth * 314 / 600 + 187.5 + FinishTaskView.imageHeight + FinishTaskView.iconHeifht
      let adView = AdCoinView.adStart(CGRect(x: 30, y: FinishTaskView.viewWidth * 283 / 592 + 40 + FinishTaskView.iconHeifht, width: FinishTaskView.whiteWidth, height: FinishTaskView.whiteWidth * 314 / 600 + 117.5), nativeAd)      //广告
      self.addSubview(adView)
      adView.layer.mask = self.bottomLayer(adView)
    } else {
      self.height = FinishTaskView.imageHeight + 105 +  FinishTaskView.iconHeifht
    }
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    whiteBackView.frame = CGRect(x: 30, y: FinishTaskView.imageHeight - 36, width: FinishTaskView.whiteWidth, height: self.height - FinishTaskView.imageHeight + 17.5)
    makeSureButton.frame = CGRect.init(x: 80, y: self.whiteBackView.maxY - 17.5, width: FinishTaskView.whiteWidth - 100, height: 35)
    makeSureButton.setTitle(NSLocalizedString("确定", comment:""), for: .normal)
    makeSureButton.setTitleColor(FinishTaskView.labelAddColor, for: .normal)
    makeSureButton.backgroundColor = UIColor(hex: "#fcc837")
    makeSureButton.layer.cornerRadius = 5
    makeSureButton.layer.masksToBounds = true
    self.addSubview(makeSureButton)
    makeSureButton.addTarget(self, action: #selector(hiddenNotice), for: .touchUpInside)
  }
  
  // 右下角和左下角圆角..
  func bottomLayer(_ layerView: AdCoinView) -> CAShapeLayer {
    let maskPath = UIBezierPath.init(roundedRect: layerView.bounds, byRoundingCorners: [UIRectCorner.bottomLeft, UIRectCorner.bottomRight], cornerRadii: CGSize(width: 6, height: 6))
    let tmpLayer = CAShapeLayer.init()
    tmpLayer.frame = layerView.bounds
    tmpLayer.path = maskPath.cgPath
    return tmpLayer
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    let width = UIScreen.main.bounds.width * 0.85
    let imageHeight = width * 283 / 592
    let edgeWidth = (self.width - kWidth * FinishTaskView.kScale) / 2
    var leftWidth = edgeWidth
    
    successImageLabel.sizeToFit()
    successImageLabel.center = CGPoint(x: FinishTaskView.viewWidth / 2, y: FinishTaskView.imageHeight * 185 / 285)
    infoLabel.sizeToFit()
    infoLabel.frame = CGRect(x: 0, y: imageHeight + taskCardViewCenterY + FinishTaskView.iconHeifht + 8, width: FinishTaskView.viewWidth, height: 30)
    for index in 0..<viewArray.count {
      let view = viewArray[index]
      if index != 0 {
        let label = addArray[index - 1]
        label.frame = CGRect(x: leftWidth, y: imageHeight + taskCardViewCenterY, width: FinishTaskView.edgeItemSapce, height: FinishTaskView.iconHeifht)
        leftWidth += FinishTaskView.edgeItemSapce
      }
      switch view.cardType! {
      case .gold:
        view.frame = CGRect(x: leftWidth, y: imageHeight + taskCardViewCenterY , width: FinishTaskView.iconAndGold , height: FinishTaskView.iconHeifht)
        leftWidth += FinishTaskView.iconAndGold
      case .seer, .werewolf, .witch, .unKnown:
        view.frame = CGRect(x: leftWidth, y: imageHeight + taskCardViewCenterY, width: 56 * FinishTaskView.kScale, height: FinishTaskView.iconHeifht)
        leftWidth += FinishTaskView.iconHeifht
      }
    }
  }
  

}

// 返回的model

class FinishTaskViewModel: Mappable {
  
  var type: String?
  var count: Int = 0
  var img: String?
  var cardType: ImageType = .unKnown
  
  required init?(map: Map) {}
  
  func mapping(map: Map) {
    type <- map["type"]
    count <- map["count"]
    img <- map["img"]
    getCardType()
  }
  
  func getCardType() -> Void {
    cardType = ImageType.init(rawValue: type ?? "unKnown") ?? .unKnown
  }
  
}

// 金币icon 400 // 女巫卡icon // 狼人卡icon 的view
class TaskCardView: UIView {
  
  let imageView = UIImageView()
  var label: UILabel?
  var cardType: ImageType?
  
  convenience init(_ type: ImageType, _ frame: CGRect) {
    self.init(frame: frame)
    cardType = type
    imageView.image = cardType?.image()
    imageView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
  }
  
  convenience init(_ model: FinishTaskViewModel, _ frame: CGRect) {
    self.init(frame: frame)
    if let img = model.img {
      imageView.sd_setImage(with: URL.init(string: img), placeholderImage: UIImage.init(named: "image_placeholder"))
    }
    cardType = .unKnown
    imageView.frame = CGRect(x: 0, y: 0, width: frame.height, height: frame.height)
    if model.count > 1 {
      cardType = .gold
      label = UILabel.init()
      label?.textColor = FinishTaskView.labelAddColor
      label?.text = "\(model.count)"
      label?.textAlignment = .center
      label?.font = UIFont.systemFont(ofSize: 15)
      label?.sizeToFit()
      label?.frame = CGRect(x: frame.height, y: 0, width: 62 * FinishTaskView.kScale, height: frame.height)
      self.addSubview(label!)
    }
  }
  
  convenience init(_ type: ImageType, _ iConNumber: Int, _ frame: CGRect) {
    self.init(frame: frame)
    cardType = type
    label = UILabel.init()
    label?.textColor = FinishTaskView.labelAddColor
    label?.text = "\(iConNumber)"
    label?.textAlignment = .center
    label?.font = UIFont.systemFont(ofSize: 15)
    label?.sizeToFit()
    self.addSubview(label!)
    imageView.image = cardType?.image()
    imageView.frame = CGRect(x: 0, y: 0, width: frame.height, height: frame.height)
    label?.frame = CGRect(x: frame.height, y: 0, width: frame.width - frame.height, height: frame.height)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    imageView.contentMode = .scaleAspectFit
    self.addSubview(imageView)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
