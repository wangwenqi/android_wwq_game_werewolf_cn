//
//  StoreCenterViewController.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/8.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import MJRefresh
import SwiftyStoreKit
import WebKit
import ChatKit

enum webType {
  case Store
  case Mission
  case Charge
  case FAQ
  case Recommend
  case Leaderboard
  case GameRecord
  case FamilyLevel
  case BattleLevel
}


class WeakScriptMessageDelegate :NSObject,WKScriptMessageHandler {

  weak var scriptDelegate:WKScriptMessageHandler?
  @available(iOS 8.0, *)
  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    self.scriptDelegate?.userContentController(userContentController, didReceive: message)
  }
  init(delegate:WKScriptMessageHandler){
    scriptDelegate = delegate
    super.init()
  }
}

class StoreCenterViewController: RotationViewController {
  
  var web:WKWebView?
  var type:webType?
  var fakeview:UIView?
  var weburl:String?
  var userid:String?
  var arr = Array<Any>()
  let nativeAd = FBNativeAd(placementID: KeyType.kFBAdNaviveKey.keys())         // 金币广告
  let roundBannerAd = FBAdView(placementID: KeyType.kFBAdRoundBannerKey.keys(), adSize: kFBAdSizeHeight50Banner, rootViewController: nil)                            // 横幅广告
  let roundGetAd = FBNativeAd(placementID: KeyType.kFBAdRoundGetKey.keys())     // 轮盘抽奖成功广告
  lazy var imClient:AVIMClient = LCChatKit.sharedInstance().client
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // 领取金币是否展示广告
    AdShowClass.isShowAd(.getGoldIconAd, { (isShow) in
      if isShow {
        self.nativeAd.delegate = self
        self.nativeAd.mediaCachePolicy = .all
        self.nativeAd.load()
      }
      adState.isAdGWGoldIconShow = isShow
    })
    
    AdShowClass.isShowAd(.roundBannerAd) { (isShow) in
      if isShow {
        self.roundBannerAd.delegate = self
        self.roundBannerAd.frame = CGRect(x: 0, y: Screen.height - 50, width: Screen.width, height: 50)
        self.roundBannerAd.loadAd()
      }
    }
    
    AdShowClass.isShowAd(.roundGetAd) { (isShow) in
      if isShow {
        self.roundGetAd.delegate = self
        self.roundGetAd.mediaCachePolicy = .all
        self.roundGetAd.load()
      }
    }
  
    navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "room_icon_leftarrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(gameAndMissionCenterViewController.leftClicked))
    getWeb()
    self.view.backgroundColor = UIColor.white
    let addHeaderToUrl = headerURLParams();
    if type != nil {
      if type! == .Store {
        weburl =  Config.urlHost + "/shop?access_token=" + "\(CurrentUser.shareInstance.token)&\(addHeaderToUrl)"
      }else if type! == .Mission{
        weburl =  Config.urlHost + "/task?access_token=" + "\(CurrentUser.shareInstance.token)&native=1&\(addHeaderToUrl)"
      }else if type! == .Charge {
        
        weburl = Config.urlHost + "/recharge?access_token=" + "\(CurrentUser.shareInstance.token)&\(addHeaderToUrl)"
      }else if type! == .FAQ {
        weburl =  Config.urlHost + "/pay_faq?\(addHeaderToUrl)"
      }else if type! == .Recommend {
        weburl =  Config.urlHost + "/recommend?access_token=" + "\(CurrentUser.shareInstance.token)&\(addHeaderToUrl)"
      }else if type! == .Leaderboard {
        weburl =  Config.urlHost + "/rank?\(addHeaderToUrl)"
      }else if type! == .GameRecord {
        weburl =  Config.urlHost + "/game_record/?access_token=" + "\(CurrentUser.shareInstance.token)&\(addHeaderToUrl)"
      }else if type! == .FamilyLevel {
        weburl =  Config.urlHost + "/family_grade/?access_token=" + "\(CurrentUser.shareInstance.token)&\(addHeaderToUrl)"
      }else if type! == .BattleLevel {
         weburl =  Config.urlHost + "/rank_instruction/?access_token=" + "\(CurrentUser.shareInstance.token)&\(addHeaderToUrl)"
      }
    }
    if (weburl == nil) {
      weburl = Config.urlHost;
    }
    let request = URLRequest(url: URL(string:weburl!)!)
    self.web?.load(request)
    self.view.addSubview(web!)
    self.web?.snp.makeConstraints({ (make) in
      make.top.equalToSuperview().offset(64)
      make.left.right.bottom.equalToSuperview()
    })
    self.automaticallyAdjustsScrollViewInsets = false
    self.web?.scrollView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [weak self] in
      self?.web?.reload()
    })
    let noti = Notification.Name(rawValue: "PAYSS")
    NotificationCenter.default.addObserver(self, selector:#selector(paySuccess), name: noti, object: nil)
  }
  
  func headerURLParams() -> String {
    return "pt=\(Config.platform)&v=\(Config.buildVersion)&b=\(Config.appStoreVersion)&sv=\(Config.serverVersion)&tz=\(Config.currentTimeZoneGMT)&lg=\(Config.systemLanage)&app=\(Config.app)&did=\(Utils.getUUID() ?? "")";
  }
  
  func getWeb() {
  
    if self.web == nil {
      let config = WKWebViewConfiguration()
      config.allowsInlineMediaPlayback = true
      if #available(iOS 9.0, *) {
        config.allowsAirPlayForMediaPlayback = true
      } else {
        // Fallback on earlier versions
      }
      if #available(iOS 9.0, *) {
        config.allowsPictureInPictureMediaPlayback = true
      } else {
        // Fallback on earlier versions
      }
      config.suppressesIncrementalRendering = true
      let content = WKUserContentController()
      content.add(WeakScriptMessageDelegate(delegate: self), name: "WereWolf")
      config.userContentController = content
      self.web = WKWebView(frame: .zero, configuration: config)
      self.web?.isOpaque = false
      self.web?.navigationDelegate = self
      self.web?.uiDelegate = self
      self.web?.sizeToFit()
    }
  }

  func leftClicked() {
    
    if (self.web?.canGoBack)! {
      self.roundBannerAd.removeFromSuperview()
      if (self.web?.isLoading)! {
        self.web?.stopLoading()
      }
      self.web?.goBack()
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {[weak self] in
        self?.web?.reload()
      })
    }else{
      RNMessageSender.emitEvent(name: "EVENT_USER_LEAVE_FROM_WEB")
      self.dismiss(animated: true, completion: nil)
    }
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func paySuccess() {
    DispatchQueue.main.async {
      self.web?.evaluateJavaScript(NSLocalizedString("payCallBack('0','支付成功')", comment: ""), completionHandler: nil)
    }
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
    self.web?.configuration.userContentController.removeScriptMessageHandler(forName: "WereWolf")
    self.web?.stopLoading()
    print("webView deinit")
  }
}

//MARK: delegate

extension StoreCenterViewController: WKNavigationDelegate {
  
  func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {

  }
  //开始加载
  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    XBHHUD.showLoading()
    XBHHUD.hidenSuccess = { [weak self] in
      self?.leftClicked()
    }
  }
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    XBHHUD.hide()
    self.navigationItem.title = webView.title
    if self.web?.scrollView.mj_header.state == .refreshing {
      self.web?.scrollView.mj_header.endRefreshing()
    }
  }
  
  func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    XBHHUD.hide()
    if self.web?.scrollView.mj_header.state == .refreshing {
      self.web?.scrollView.mj_header.endRefreshing()
    }
    errorView()
  }
  
  func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
    XBHHUD.hide()
    if self.web?.scrollView.mj_header.state == .refreshing {
      self.web?.scrollView.mj_header.endRefreshing()
    }
    errorView()
  }
  
//  func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
//  }
  
  func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
    
  
    if let response = navigationResponse.response as? HTTPURLResponse {
      if response.statusCode > 200 {
        errorView()
      }else{
        self.view.subviews.forEach { (view) in
          if view.tag == 404 {
              view.removeFromSuperview()
              fakeview = nil
          }
        }
      }
    }
    //允许跳转
    decisionHandler(.allow)
  }
  
  func errorView () {
    if fakeview == nil {
      fakeview = UIView(frame: self.view.frame)
      fakeview?.backgroundColor = UIColor.white
      fakeview?.tag = 404
      let image = UIImage(named: "出错了")
      let imagview = UIImageView.init(image: image)
      imagview.frame = CGRect(x: 0, y: 0, width: (image?.size.width)!/2, height: (image?.size.height)!/2)
      imagview.center = self.view.center
      fakeview?.addSubview(imagview)
      let tap = UITapGestureRecognizer()
      tap.addTarget(self, action: #selector(reload))
      self.fakeview?.addGestureRecognizer(tap)
      self.view.addSubview(fakeview!)
    }
  }
  
  func reload() {
    let request = URLRequest(url: URL(string:weburl!)!)
    self.web?.load(request)
  }


}

extension StoreCenterViewController : GiftSysDelegate,giftSend {
  func sendGift(typeString: giftType, to: PlayerView?) {
    
  }
  
  func sendGift(type: giftType, to: PlayerView?) {
    
  }
  
  func sendGiftWithRebate(type: String, json: JSON, rebate: JSON) {
    
    let name = json["name"].stringValue
    let id = json["id"].stringValue
    let image = json["image"].stringValue
    let sex = json["sex"].stringValue
    XBHHUD.showSuccess(NSLocalizedString("赠送礼物成功", comment: ""))
    // 发送消息
    
    if id == CurrentUser.shareInstance.id {
      return
    }
    
    var rebateValue = ""
    if let rebate = json["rebate"].dictionary {
      if let reValue = rebate["value"]?.int {
        rebateValue = "\(reValue)"
      }
    }
    
    imClient.createConversation(withName: name,
                                clientIds: [id],
                                attributes: nil,
                                options: AVIMConversationOption.unique,
                                callback: { (conversation, error) in
                                  
                                  let sendMessage = OLGiftMessage.init(attibutes: [
                                    "USER_SEX": "\(CurrentUser.shareInstance.sex)",
                                    "USER_ICON": CurrentUser.shareInstance.avatar,
                                    "USER_NAME": CurrentUser.shareInstance.name,
                                    "USER_ID": CurrentUser.shareInstance.id,
                                    GIFT_MSG_KEY_GITF_TYPE:type,
                                    "GIFT_REBATE":rebateValue,
                                    "APP": Config.app,
                                    "CHAT_TIME": Utils.getCurrentTimeStamp()
                                    ]);
                                  
                                  //此时也要发送通知，告诉RN更新
                                  conversation?.send(sendMessage, callback: { (isSucceeded, error) in
                                    if isSucceeded {
                                      //此时也要发送通知，告诉RN更新
                                      let dic = ["USER_NAME":name,"USER_SEX":sex,"USER_ICON":image,"MESSAGE_TYPE":"MESSAGE_TYPE_CHAT","USER_ID":id,"CHAT_TIME":String(Utils.getCurrentTimeStamp()),"APP": Config.app,"CHAT_MESSAGE":NSLocalizedString("🎁赠送成功", comment: ""),"READ":true] as [String : Any]
                                      NotificationCenter.default.post(name: NotifyName.kSendMessToRN.name(), object: dic)
                                    }
                                  });
                                  
    });
  }
  
  func showSendGift(with: JSON) {
    if let name = with["name"].string,
      let id = with["id"].string {
      let gift = giftView.initGiftViewWith(envtype:.GAMERECORD,People:name)
      gift.toID = id
      gift.jsonData = with
      gift.delegate = self
      Utils.getKeyWindow()?.addSubview(gift)
      gift.animation = "fadeIn"
      gift.animate()
      gift.getGiftFormRemote()
    }else{
      XBHHUD.showError("不能送礼物")
   }
  }
  
  func sendGift(type: String, json: JSON) {
    
  }

}

extension StoreCenterViewController : WKUIDelegate {
  
  func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
    let alert = UIAlertController(title: "Tip", message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) -> Void in
      // We must call back js
      completionHandler()
    }))
    self.present(alert, animated: true, completion: nil)
  }
  
  func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
    
    let alert = UIAlertController(title: "Tip", message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) -> Void in
      // We must call back js
      completionHandler(true)
    }))
    self.present(alert, animated: true, completion: nil)
  }
}

extension StoreCenterViewController: WKScriptMessageHandler {
  @available(iOS 8.0, *)
  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    if let dic = message.body as? NSDictionary,
      let functionName = dic["functionName"] as? String,
      let para = dic["parameter"]{
        if functionName == "buy" {
          //购买物品
          if let productid = para as? String {
            buy(productid)
          }
        }
      
      if functionName == "getPros" {
        //  获取价格
        if let productArr = para as? NSArray {
          getPros(productArr as! Array<String>)
        }
      }

      // 领取金币
      if functionName == "showSuccessDialog" {
        let info = para as! [[String: Any]]
        var isShow = false
        if adState.isAdGWGoldIconShow, adState.isAdFBGoldIconShow {
          isShow = true
        }
        let view = FinishTaskView.taskView(info, isShow, self.nativeAd)
        Utils.showAlertView(view: view)
        Utils.playSound(name: "ReceiveGoldGoldICon")
      }
      
      if functionName == "enterRoom" {
        if let roomInfo = para as? NSArray {
          print("\(roomInfo)")
          enterRoom(roomInfo: roomInfo)
        }
      }
      
      if functionName == "fortuneResult" {
        let tmpPara = para as! [String: Any]
        let info = tmpPara["gift"] as? [String: Any]
        var isShow = false
        if adState.isAdFBGoldIconShow {
          isShow = true
        }
        let params: [String : Any] = [
          "type":info?["type"] ?? "unKnown",
          "count":info?["count"] ?? "",
          "img":info?["img"] ?? ""
        ]
        let view = FinishTaskView.taskLottoView([params], info?["txt"] as? String, (tmpPara["result"] as? Bool) ?? false, adState.isAdFBRoundGetShow, self.roundGetAd)
        Utils.showAlertView(view: view)
        Utils.playSound(name: "ReceiveGoldGoldICon")
      }
      
      if functionName == "setAd" {
        self.view.addSubview(self.roundBannerAd)
      }
      //排行榜点击查看个人信息
      if functionName == "showUserInfo" {
        if let user_id = para as? String {
          self.userProfile(user_id: user_id)
        }
      }
      //使用新的JS通信
      if functionName == "sendJSCommend" {
        if let jsonStr = para as? String{
          let json = JSON.init(parseJSON: jsonStr)
          if let action = json["action"].string,
            let params = json["params"].dictionary {
            if action == "game_record_user_info" {
              //游戏记录里的用户信息
                //显示个人信息界面
                if let user_id = params["user_id"]?.string {
                  let view = newPlayerAlertView.generatePlayerInfoView(type: .GAMERECORD, gameRecordID: user_id)
                  view.delegate = self
                  Utils.showNoticeView(view: view)
              }
            }
            if action == "ACTION_SHOW_FAMILY_INFO" {
              if let group_id = params["group_id"]?.string {
                SwiftConverter.shareInstance.showFamilyInfo(familyId: group_id)
              }
            }
            if action == "enterAudioRoom" {
                SwiftConverter.shareInstance.enterAudioRoom(json: params)
            }
            //web调用本地分享
            if action == "openShareModal" {
              SwiftConverter.shareInstance.shareWithContent(params)
            }
          }
        }
      }
    }
  }
  
  func buy(_ productID: String){
    if CurrentUser.shareInstance.unCheckReceipt != nil {
      //说明还有未验证的购买
      DispatchQueue.main.async {
        let alert = UIAlertController(title: "", message: NSLocalizedString("您还有未验证的购买，您需要先验证以前的够买", comment: ""), preferredStyle: .alert)
        let action = UIAlertAction(title: NSLocalizedString("确定", comment: ""), style: .default, handler: { (action) in
          let receipt = CurrentUser.shareInstance.unCheckReceipt?.recept
          let purchase = CurrentUser.shareInstance.unCheckReceipt?.productid
          XBHHUD.showCanNotHidden(title:NSLocalizedString("正在验证购买，请耐心等待", comment: ""))
          RequestManager().post(url: RequestUrls.iap, parameters:["receipt":receipt!], success: { (json) in
            //把返回值刷成ReceiptInfo
            var sucess = false
            if json.dictionary != nil {
              let info = json.dictionaryValue["purchase"]?.arrayValue
              info?.forEach({ (item) in
                let receiptItem = item.dictionaryValue//ReceiptItem(receiptInfo: item)
                if receiptItem["productId"]?.stringValue == purchase?.productId {
                  if receiptItem["op_result"]?.intValue == 1000 ||  receiptItem["op_result"]?.intValue == 2000 {
                    XBHHUD.hide()
                    SwiftyStoreKit.finishTransaction((purchase?.transaction)!)
                    CurrentUser.shareInstance.unCheckReceipt = nil
                    sucess = true
                  }
                }
              })
              //显示成功
              if sucess {
                DispatchQueue.main.async {
                  XBHHUD.hide()
                  XBHHUD.showSuccess(NSLocalizedString("验证成功", comment: ""))
                  self.web?.evaluateJavaScript(NSLocalizedString("payCallBack('0','支付成功')", comment: ""), completionHandler: nil)
                }
              }else{
                XBHHUD.hide()
                DispatchQueue.main.async {
                  let payFail = Bundle.main.loadNibNamed("payFail", owner: nil, options: nil)?.last as! payFail
                  payFail.backgroundColor = UIColor.hexColor("E3DDEA")
                  payFail.textview.backgroundColor = UIColor.hexColor("E3DDEA")
                  Utils.showNoticeView(view: payFail)
                }
              }
            }
          }) { (code, message) in
            XBHHUD.hide()
            let localerror = NSLocalizedString("验证失败:", comment: "")
            let locallaterr = NSLocalizedString("请您稍后再试", comment: "")
            XBHHUD.showError("\(localerror)\(message)，\(locallaterr)")
          }
        })
        let cancle = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style:.cancel, handler: { (action) in
        })
        alert.addAction(action)
        alert.addAction(cancle)
        Utils.getCurrentVC(vc:(Utils.getKeyWindow()?.rootViewController)!)?.present(alert
          , animated: true, completion: nil)
      }
      
    }else{
      self.iap(productID)
    }
  }
  //购买
  func iap(_ productID:String) {
    XBHHUD.showCanNotHidden(title:NSLocalizedString("购买中请耐心等待", comment: ""))
    SwiftyStoreKit.purchaseProduct(productID, quantity: 1, atomically: false) { result in
      XBHHUD.hide() // 隐藏耐心等待
      switch result {
      case .success(let purchase):
        //客户端的验证器
        let wowValidator = WowValidator(service:.production)
        let product = productInfo(productId: purchase.productId, transaction: purchase.transaction, needsFinishTransaction: purchase.needsFinishTransaction)
        wowValidator.purchase = product
        XBHHUD.showCanNotHidden(title:NSLocalizedString("正在验证购买，请耐心等待", comment: ""))
        SwiftyStoreKit.verifyReceipt(using: wowValidator, password: nil) { result in
        }
      case .error(let error):
        switch error.code {
        case .unknown:
          XBHHUD.showError(NSLocalizedString("购买失败", comment: ""))
          print("Unknown error. Please contact support")
        case .clientInvalid:
          XBHHUD.showError(NSLocalizedString("您设置了不允许内购", comment: ""))
          print("Not allowed to make the payment")
        case .paymentCancelled: break
        case .paymentInvalid:
          XBHHUD.showError(NSLocalizedString("商品暂时不能购买", comment: ""))
          print("The purchase identifier was invalid")
        case .paymentNotAllowed: print("The device is not allowed to make the payment")
        case .storeProductNotAvailable:
          XBHHUD.showError(NSLocalizedString("该商品已经不在出售", comment: ""))
          print("The product is not available in the current storefront")
        case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
        case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
        case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
        }
      }
    }
  }
  //获取价格
  func getPros(_ productIDS: Array<String>) {
    let arrid = productIDS
    let iap:Set<String> = Set.init(productIDS)
    if arr.count == 0 || arr.isEmpty {
      SwiftyStoreKit.retrieveProductsInfo(iap) {[weak self] (results) in
        let products = results.retrievedProducts
        arrid.forEach({ (productid) in
          print("\(productid)")
          if let product = products.first(where: { $0.productIdentifier == productid}) {
            print("\(product.price)")
            var symbol:String
            if product.priceLocale.currencyCode == "TWD" {
              symbol = "NT$"
            }else if product.priceLocale.currencyCode == "HKD" {
              symbol = "HK$"
            }else if product.priceLocale.currencyCode == "SGD" {
              symbol = "S$"
            }else if product.priceLocale.currencyCode == "MYR" {
              symbol = "RM"
            }else{
              symbol = product.priceLocale.currencySymbol!
            }
            let dic = ["id":product.productIdentifier,"price":"\(product.price)","symbol":symbol] as [String : Any]
            self?.arr.append(dic)
          }
        })
    
        if arrid.count == self?.arr.count {
          self?.getProsCallBack((self?.arr)!)
        }else{
          //需要重新获取
          self?.arr.removeAll()
        }
      }
    }else{
      //检查值是否完整
      if arrid.count == arr.count {
        self.getProsCallBack(arr)
      }else{
        //需要重新获取
        arr.removeAll()
      }
    }
  }
  //设置价格
  func getProsCallBack(_ arr:Array<Any>) {
    if let objectData = try? JSONSerialization.data(withJSONObject: self.arr, options: JSONSerialization.WritingOptions(rawValue: 0)) {
      if let objectString = String(data: objectData, encoding: .utf8) {
        let finalJS = "getProsCallBack(" + "\(objectString)" + ")"
        web?.evaluateJavaScript(finalJS, completionHandler: nil)
      }
    }
  }
  //进入房间
  func enterRoom(roomInfo:NSArray) {
    //如果在游戏中就不进去

    if SwiftConverter.shareInstance.isGaming() {
      XBHHUD.showError(NSLocalizedString("您已经在一局游戏中,不能再进入游戏", comment: ""))
      return
    }
    if CurrentUser.shareInstance.loadingGame {
      MobClick.event("MultipleAccess", label:CurrentUser.shareInstance.id ?? "yk")
      return
    }
    CurrentUser.shareInstance.loadingGame = true
    CurrentUser.shareInstance.from = "export"
    if let roomId = roomInfo[0] as? String,
      let password = roomInfo[2] as? String,
      let type = roomInfo[1] as? String {
      NotificationCenter.default.post(
        name: NotifyName.kEnterToGameRoom.name(),
        object: nil,
        userInfo: [
          "password": password,
          "type": type,
          "roomId": roomId,
          "userName": CurrentUser.shareInstance.name,
          "userId": CurrentUser.shareInstance.id,
          "userImage": CurrentUser.shareInstance.avatar,
          "token": CurrentUser.shareInstance.token,
        ])
    }
  }

  //个人主页
  
  func userProfile(user_id:String) {
    XBHHUD.showLoading()
    RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id": user_id], success: {[weak self] (json) in
      XBHHUD.hide()
      let controller = UserInfoViewController()
      controller.peerID = json["id"].stringValue
      controller.peerSex = json["sex"].stringValue
      controller.peerName = json["name"].stringValue
      controller.peerIcon = json["image"].stringValue
      if let isFriend = json["is_friend"].string {
        controller.isFriend = isFriend == "false" ? false : true
      } else if let isFriend = json["is_friend"].bool {
        controller.isFriend = isFriend;
      }
      controller.isTourist = false
      controller.isself = json["id"].stringValue == CurrentUser.shareInstance.id ? true : false
      controller.comeFormRank = true
      let rootVC = RotationNavigationViewController.init(rootViewController: controller)
      self?.present(rootVC, animated: true, completion: nil)
      }, error:{ (code, message) in
        XBHHUD.showError(NSLocalizedString("请检查您的网络...", comment: ""))
    })
  }
  
}

