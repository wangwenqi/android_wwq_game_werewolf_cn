//
//  FBAdFinishTaskGoldIcon.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/22.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

let adState = AdState()

class AdState: NSObject {
  
  var isAdFBGoldIconShow: Bool = false      // 金币FB是否显示广告
  var isAdGWGoldIconShow: Bool = false      // 游戏后台是否显示广告
  var isAdFBGameOverShow: Bool = false      // FB游戏结束是否显示广告
  var isAdGWGameOverShow: Bool = false      // 后台游戏结束是否显示广告
  
  var isAdFBRoundBanShow: Bool = false      // 转盘横幅广告位置
  var isAdFBRoundGetShow: Bool = false      // 转盘领取弹出广告
}
// 领取金币的广告
extension StoreCenterViewController: FBNativeAdDelegate {
  
  func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
    if nativeAd == self.nativeAd {
      adState.isAdFBGoldIconShow = true
    }
    
    if nativeAd == self.roundGetAd {
      adState.isAdFBRoundGetShow = true
    }
  }
  
  func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error) {
    if nativeAd == self.nativeAd {
      adState.isAdFBGoldIconShow = false
    }
    if nativeAd == self.roundGetAd {
      adState.isAdFBRoundGetShow = false
    }
  }
  
  func nativeAdDidClick(_ nativeAd: FBNativeAd) {
    Utils.hideNoticeView()
  }
}

extension StoreCenterViewController: FBAdViewDelegate {
  
  func adViewDidLoad(_ adView: FBAdView) {
    if adView == self.roundBannerAd {
      adState.isAdFBRoundBanShow = true
    }
  }
  
  func adView(_ adView: FBAdView, didFailWithError error: Error) {
    if nativeAd == self.roundBannerAd {
      adState.isAdFBRoundBanShow = false
    }
  }
  
  func adViewDidClick(_ adView: FBAdView) {
    self.roundBannerAd.removeFromSuperview()
  }
  
}

extension GameRoomViewController: FBNativeAdDelegate {
  
  func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
    
    if nativeAd == self.nativeGameOverAd {
      adState.isAdFBGameOverShow = true
    }
//    // 进入房间广告
//    if nativeAd == self.nativeEnterGameAd {
//      AdShowClass.isShowAd(.enterGameAd, { [unowned self] (isShow) in
//        if isShow { self.showEnterGameAd() }
//      })
//    }
    
    if nativeAd == self.nativeDeadAd {
      AdShowClass.isShowAd(.gameDeathAd, { [unowned self] (isShow) in
        if isShow {
          self.showUserRoleBottomSpace.constant = 35
          self.showUserRoleButtonCenterX.constant = Screen.width / 2 - 30
          UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
          }
          self.showGameDeadAd()
        }
      })
    }
  }
  
  // 游戏结束
  func loadGameOverNativeAd() {
    if self.nativeGameOverAd.delegate == nil {
      nativeGameOverAd.delegate = self
      nativeGameOverAd.mediaCachePolicy = .all
      nativeGameOverAd.load()
      // 获取一次权限
      AdShowClass.isShowAd(.gameOverAd, { (isShow) in
        adState.isAdGWGameOverShow = isShow
      })
    }
  }
  
  // 游戏死亡
  func loadGameDeadNativeAd() {
    if self.nativeDeadAd.delegate == nil {
      nativeDeadAd.delegate = self
      nativeDeadAd.mediaCachePolicy = .all
      nativeDeadAd.load()
    }
  }
  
//    // 进入房间广告
//  func loadEnterGameNativeAd() {
//    nativeEnterGameAd.delegate = self
//    nativeEnterGameAd.mediaCachePolicy = .all
//    nativeEnterGameAd.load()
//  }
  
  
  
  func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error) {
    adState.isAdFBGameOverShow = false
  }
  
  // 进入房间广告
//  func showEnterGameAd() {
//    let enterAdView = AdEnterRoom.adStart(CGRect(x: 70 * Screen.width / 320, y: Screen.height - 220 * Screen.width / 320, width: Screen.width - 140 * Screen.width / 320, height: 120), nativeAd: self.nativeEnterGameAd)
//    self.view.addSubview(enterAdView)
//  }
  
  // 显示游戏结束广告
  func showGameDeadAd() {
    self.view.viewWithTag(19001)?.removeFromSuperview()
    let adGameDead = AdGameDead.adStart(CGRect(x: 0, y: Screen.height - 65, width: Screen.width - 80, height: 64), self.nativeDeadAd)
    adGameDead.tag = 19001
    self.view.addSubview(adGameDead)
    let button = UIButton(type: .system)
    button.setBackgroundImage(UIImage.init(named: "deleteAdButton"), for: .normal)
    button.addTarget(self, action: #selector(hideGameDeadAd), for: .touchUpInside)
    button.frame = CGRect.init(x: adGameDead.maxX, y: Screen.height - 32, width: 39, height: 30)
    button.tag = 19002
    self.view.addSubview(button)
  }
  // 隐藏游戏结束广告
  func hideGameDeadAd() {
    if self.nativeDeadAd.delegate != nil {
      let view = self.view.viewWithTag(19001)
      let button = self.view.viewWithTag(19002)
      view?.removeFromSuperview()
      button?.removeFromSuperview()
    }
    self.showUserRoleBottomSpace.constant = 8
    self.showUserRoleButtonCenterX.constant = 0
    UIView.animate(withDuration: 0.3) {
      self.view.layoutIfNeeded()
    }
    
  }
  
  // 点击广告
  func nativeAdDidClick(_ nativeAd: FBNativeAd) {
    
  }
  
  
  
}





















