//
//  PayNetwork.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/21.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class AdShowClass: NSObject {

  enum AdConfigType {
    case enterGameAd    //进入游戏广告展示
    case gameDeathAd    // 游戏内死亡后展示
    case getGoldIconAd  // 领取奖励后弹出
    case gameOverAd     // 游戏结束后广告
    case roundBannerAd  // 转盘横幅
    case roundGetAd     // 领奖广告
    
    func name() -> String {
      switch self {
      case .enterGameAd:
        return "1"
      case .gameDeathAd:
        return "2"
      case .getGoldIconAd:
        return "3"
      case .gameOverAd :
        return "4"
      case .roundBannerAd:
        return "5"
      case .roundGetAd:
        return "6"
      }
    }
  }
  
  class func isShowAd(_ type: AdConfigType ,_ show: @escaping (Bool) -> Void) {
    RequestManager().get(url: RequestUrls.showAdConfig, parameters: ["type": type.name()], success: { (response) in
      show(response["show"].boolValue)
    }) { (code, msg) in
      show(false)
    }
  }
}
