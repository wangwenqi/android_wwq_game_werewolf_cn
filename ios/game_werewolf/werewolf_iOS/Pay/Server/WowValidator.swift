//
//  WowValidator.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/1.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import Alamofire

struct productInfo {
  let productId: String
  let transaction: PaymentTransaction
  let needsFinishTransaction: Bool
}

class WowValidator: ReceiptValidator {
  
  enum VerifyReceiptType {
    case production
    case sandbox
  }
  
  public init(service: VerifyReceiptType = .production) {
    self.service = service
  }
  
  private let service: VerifyReceiptType
  var purchase:productInfo?
  var retryTime = 0
  public func validate(
    receipt: String,
    password autoRenewPassword: String? = nil,
    completion: @escaping (VerifyReceiptResult) -> Void) {
    //保存购买数据
    let receipts = unCheckReceipt(recept: receipt, productid: self.purchase!)
    CurrentUser.shareInstance.unCheckReceipt = receipts
    // 后台验证
    RequestManager().post(url: RequestUrls.iap, parameters:["receipt":receipt], success: { (json) in
      //把返回值刷成ReceiptInfo
      var shouldRetry = false
      if json.dictionary != nil {
        let info = json.dictionaryValue["purchase"]?.arrayValue
        info?.forEach({ (item) in
         let receiptItem = item.dictionaryValue//ReceiptItem(receiptInfo: item)
          if receiptItem["productId"]?.stringValue == self.purchase?.productId {
            if receiptItem["op_result"]?.intValue == 1000 ||  receiptItem["op_result"]?.intValue == 2000 {
                SwiftyStoreKit.finishTransaction((self.purchase?.transaction)!)
                CurrentUser.shareInstance.unCheckReceipt = nil
            }else{
              shouldRetry = true
            }
          }
        })
        if shouldRetry {
          self.retryTime += 1
          if self.retryTime <= 2 {
            self.validate(receipt: receipt, password:nil, completion: { (VerifyReceiptResult) in
            })
          }else{
            //重试次数过多
            DispatchQueue.main.async {
              XBHHUD.hide()
              let payFail = Bundle.main.loadNibNamed("payFail", owner: nil, options: nil)?.last as! payFail
              payFail.backgroundColor = UIColor.hexColor("E3DDEA")
              payFail.textview.backgroundColor = UIColor.hexColor("E3DDEA")
              Utils.showNoticeView(view: payFail)
            }
          }
        }else{
          //显示成功
          DispatchQueue.main.async {
            XBHHUD.hide()
            XBHHUD.showSuccess(NSLocalizedString("购买成功", comment: ""))
            let noti = Notification.Name(rawValue: "PAYSS")
            NotificationCenter.default.post(name: noti, object: nil)
          }
        }
      }
    }) { (code, message) in
      print("\(message)")
      DispatchQueue.main.async {
        XBHHUD.hide()
        let payFail = Bundle.main.loadNibNamed("payFail", owner: nil, options: nil)?.last as! payFail
        payFail.backgroundColor = UIColor.hexColor("E3DDEA")
        payFail.textview.backgroundColor = UIColor.hexColor("E3DDEA")
        Utils.showNoticeView(view: payFail)
      }
    }
  }
}




