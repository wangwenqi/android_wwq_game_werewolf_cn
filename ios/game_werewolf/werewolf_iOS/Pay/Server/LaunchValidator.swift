//
//  LaunchValidator.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/12.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import Alamofire

class LaunchValidator: ReceiptValidator {
  
  enum VerifyReceiptType {
    case production
    case sandbox
  }
  public init(service: VerifyReceiptType = .production) {
    self.service = service
  }
  private let service: VerifyReceiptType
  var purchase:productInfo?
  var retryTime = 0
  public func validate(
    receipt: String,
    password autoRenewPassword: String? = nil,
    completion: @escaping (VerifyReceiptResult) -> Void) {
    //保存购买数据
    let receipts = unCheckReceipt(recept: receipt, productid: self.purchase!)
    CurrentUser.shareInstance.unCheckReceipt = receipts
    // 后台验证
    RequestManager().post(url: RequestUrls.iap, parameters:["receipt":receipt], success: { (json) in
      //把返回值刷成ReceiptInfo
      var shouldRetry = false
      if json.dictionary != nil {
        let info = json.dictionaryValue["purchase"]?.arrayValue
        info?.forEach({ (item) in
          let receiptItem = item.dictionaryValue//ReceiptItem(receiptInfo: item)
          if receiptItem["productId"]?.stringValue == self.purchase?.productId {
            if receiptItem["op_result"]?.intValue == 1000 ||  receiptItem["op_result"]?.intValue == 2000 {
                  SwiftyStoreKit.finishTransaction((self.purchase!.transaction))
                  CurrentUser.shareInstance.unCheckReceipt = nil
            }else{
              shouldRetry = true
            }
          }
        })
        if shouldRetry {
          self.retryTime += 1
          if self.retryTime <= 2 {
            self.validate(receipt: receipt, password:nil, completion: { (VerifyReceiptResult) in
            })
          }else{
            //重试次数过多
            DispatchQueue.main.async {
              XBHHUD.hide()
              XBHHUD.showError(NSLocalizedString("验证失败，我们将在下次启动时重新验证", comment: ""))
            }
          }
        }else{
          //显示成功
          DispatchQueue.main.async {
            XBHHUD.hide()
            //告诉首页刷新
              RNMessageSender.emitEvent(name: "EVENT_USER_LEAVE_FROM_WEB")
            XBHHUD.showSuccess(NSLocalizedString("验证成功", comment: ""))

          }
        }
      }
    }) { (code, message) in
      print("\(message)")
      DispatchQueue.main.async {
        XBHHUD.hide()
         XBHHUD.showError(NSLocalizedString("验证失败，我们将在下次启动时重新验证", comment: ""))
      }
    }
  }
}
