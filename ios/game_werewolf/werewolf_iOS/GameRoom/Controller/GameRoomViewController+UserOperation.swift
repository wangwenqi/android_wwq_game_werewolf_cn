//
//  GameRoomViewController+UserOperation.swift
//  game_werewolf
//
//  Created by xiaozao on 2018/2/6.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

extension GameRoomViewController {
  
  func runDispatch(_ rawData:JSON) {
    let jsonData = rawData
    if let actionName = jsonData["type"].string {
      //说明实现了这个方法
      if CurrentUser.shareInstance.inRoomStatus != .inRoom {
        //还没有收到enter消息
        //把消息缓存
        if actionName != "connect_info" && actionName != "enter" {
          self.cachedMessage.append(rawData)
        }else{
          let selector = Selector("action_\(actionName):")
          if self.responds(to: selector) {
            self.perform(selector, with: jsonData["payload"].dictionaryObject)
          }
        }
      }else{
        //消息转发
        let selector = Selector("action_\(actionName):")
        if self.responds(to: selector) {
          self.perform(selector, with: jsonData["payload"].dictionaryObject)
        }
      }
    }
  }
  //NEW
  
  func handleSpeakAtNight() {
    Utils.getNoticeView().frame = Screen.bounds;
    self.bottomView.canUseKeyboard = true;
    self.hideSpeakBar();
    self.bottomView.speechTouchUp();
  }
  
  func decidePlaySunsetAudio(dic:NSDictionary) {
    sunsetAudio = "TianHei"
    if let _  = dic["seer"],
      let _  = dic["witch"],
      let _  = dic["guard"],
      let _  = dic["werewolf"]{
      sunsetAudio = "sunset1"
      return
    }
    if let _  = dic["seer"],
      let _  = dic["guard"],
      let _  = dic["werewolf"]{
      sunsetAudio = "sunset2"
      return
    }
    
    if let _  = dic["witch"],
      let _  = dic["werewolf"],
      let _  = dic["seer"]{
      sunsetAudio = "sunset4"
      return
    }
    
    if let _  = dic["seer"],
      let _  = dic["werewolf"]{
      sunsetAudio = "sunset3"
      return
    }
  }
  
  func clearGameRoomData() {
  // 清空上一局的游戏数据
  if speechingView != nil {
  speechingView = nil
  }
  liveTime = 0
  showSpeakBar()
  showBottomBar()
  bottomView.currrentSpeking = -1
  bottomView.setUnMute()
  CurrentUser.shareInstance.clearCurrentInformation()
  titleDeadImage.isHidden = true
  wolfTeammate = [JSON]()
  lovers.removeAll()
  currentSheriffPosition = -5
  //    self.sendGiftButton.isHidden = false
  self.demon_postion = -1
  self.werewolf_king_postion = -1
  showBottomForGameOver()
  hideGameDeadAd()
  currentUserRoleLabel.text = NSLocalizedString("当前角色：无", comment: "")
  setTitleView(title: NSLocalizedString("准备阶段", comment: ""), detail: " ", userDes: " ")
  showUserRoleButton.isHidden = true;
  //在非插麦模式要
  self.readyButton.isHidden = false
  inviteButton.setTitle(NSLocalizedString("邀请", comment: ""), for: .normal)
  readyButton.isSelected = false
  if CurrentUser.shareInstance.isMaster {
  readyButton.setTitle(NSLocalizedString("开局", comment: ""), for: .normal)
  } else {
  readyButton.setTitle(NSLocalizedString("准备", comment: ""), for: .normal)
  }
  if CurrentUser.shareInstance.isMaster {
  self.righeNavButton.isHidden = false
  self.rightInviteButton.isHidden = false
  self.navRightViewWidth.constant = 70//100
  self.statusRightPadding.constant = 45//67.5
  }else{
  self.rightChangeMasterButton.isHidden = false
  navRightViewWidth.constant = 33
  statusRightPadding.constant = 3
  }
  //新ui
  newPlayerModelUI()
  //隐藏自爆按钮
  self.boomButton.isHidden = true
  //删除所有游戏角色的提示
  self.titleBackViewRoleTotalDes.text = ""
  self.titleBackViewLine.isHidden = true
  for view in playerViewArray {
  view.setPrepare()
  }
  for view in leavedPlayerViewArray {
  view.setIconEmpty()
  }
  leavedPlayerViewArray.removeAll()
  //重新显示召集令
  for view in self.titleBackView.subviews {
  if view.isKind(of: exportNoticeView.self) {
  view.isHidden = true
  let e = view as! exportNoticeView
  setTitleView(title: NSLocalizedString("准备阶段", comment: ""), detail:e.title.text ?? "", userDes: "")
  }
  }
}

func setPassword() {
  
  if CurrentUser.shareInstance.currentRoomPassword != "" {
    // 有密码
    titleLockImage.isHidden = true
    if CurrentUser.shareInstance.isMaster {
      // 自己是房主
      titleLockImage.isHidden = false
      righeNavButton.isHidden = false
      rightChangeMasterButton.isHidden = true;
      rightChangeMasterButton.isEnabled = true
      rightInviteButton.isHidden = false
      navRightViewWidth.constant = 70//100
      statusRightPadding.constant = 45//67.5
    } else {
      // 自己不是房主
      rightInviteButton.isHidden = true
      titleLockImage.isHidden = false
      righeNavButton.isHidden = true
      rightChangeMasterButton.isHidden = false;
      rightChangeMasterButton.isEnabled = true
      navRightViewWidth.constant = 33
      statusRightPadding.constant = 3
    }
  } else {
    // 没有密码
    if CurrentUser.shareInstance.isMaster {
      // 自己是房主
      rightInviteButton.isHidden = false
      titleLockImage.isHidden = true
      righeNavButton.isHidden = false
      rightChangeMasterButton.isHidden = true;
      rightChangeMasterButton.isEnabled = true
      navRightViewWidth.constant = 70//100
      statusRightPadding.constant = 45//67.5
    } else {
      // 自己不是房主
      rightInviteButton.isHidden = true
      navRightViewWidth.constant = 33
      statusRightPadding.constant = 3
      titleLockImage.isHidden = true
      righeNavButton.isHidden = true
      rightChangeMasterButton.isHidden = false;
      rightChangeMasterButton.isEnabled = true
    }
  }
  //处理一下自己是观察者
  if CurrentUser.shareInstance.is_observer {
    rightInviteButton.isHidden = true
    righeNavButton.isHidden = true
    rightChangeMasterButton.isHidden = true
    navRightViewWidth.constant = 33
    statusRightPadding.constant = 3
  }
}

func setTitleView(title: String = "", detail: String = "", userDes: String = "", time: Int = -1) {
  if title != "" {
    titleViewTitleLabel.text = title
  }
  
  if detail != "" {
    titleViewDetailLabel.text = detail
  }
  
  if userDes != "" {
    titleViewUserLabel.text = userDes
  }
  
  guard time > 0 else {
    titleViewTimeLabel.text = ""
    return
  }
  liveTime = time
}

func hideBottombar() {
  controlBackView.isHidden = true
}

func hideBottombarForSpeeach() {
  controlBackView.isHidden = false
  readyButton.isHidden = true
  inviteButton.isHidden = true
  useDelayCardButton.isHidden = false
}

func showBottomForGameOver() {
  if CurrentUser.shareInstance.is_observer {
    return
  }
  controlBackView.isHidden = false
  readyButton.isHidden = false
  inviteButton.isHidden = false
  useDelayCardButton.isHidden = true
}

func showBottomForSpeeach() {
  if CurrentUser.shareInstance.is_observer {
    return
  }
  controlBackView.isHidden = false
  readyButton.isHidden = false
  inviteButton.isHidden = false
  useDelayCardButton.isHidden = false
}

func showBottomBar() {
  if CurrentUser.shareInstance.is_observer {
    return
  }
  readyButton.isSelected = false
  controlBackView.isHidden = false
  //需要重新渲染界面
  newPlayerModelUI()
}

func hideRoleBar() {
  rolebar.isHidden = true
  messageViewController.view.frame = CGRect.init(x: titleBackView.minX, y: titleBackView.maxY + 8 - 23, width: titleBackView.width, height: view.height - titleBackView.maxY + 23)
}

func showRoleBar() {
  rolebar.isHidden = false
  messageViewController.view.frame = CGRect.init(x: titleBackView.minX, y: titleBackView.maxY + 8, width: titleBackView.width, height: view.height - titleBackView.maxY)
}

// 发言结束到这里
func hideSpeakBar() {
  Utils.runInMainThread { [weak self] in
    self?.bottomView.dealWithLongTouch()
    //      self?.bottomView.setMute()
    self?.bottomView.isHidden = true
    if (CurrentUser.shareInstance.isDeath) {
      self?.showUserRoleButton.isHidden = false;
      //        self?.sendGiftButton.isHidden = true
      Utils.delay(2, closure: {
        if self?.bottomView.isHidden == true, self?.view.viewWithTag(190001) == nil {
          self?.loadGameDeadNativeAd()
        }
      })
    }
  }
  session?.changeSoundType(true);
  
  //处理隐藏送礼物
  //如果是观战必须隐藏
  if CurrentUser.shareInstance.is_observer == false {
    self.sendGiftButton.isHidden = false
  }
  messageDispatchManager.sendMessage(type: .unspeak)
}

func showSpeakBar() {
  if CurrentUser.shareInstance.is_observer {
    self.sendGiftButton.isHidden = true
    return
  }
  Utils.runInMainThread { [weak self] in
    self?.bottomView.isHidden = false
    self?.sendGiftButton.isHidden = true
    //      self?.bottomView.setUnMute()
    self?.showUserRoleButton.isHidden = true;
  }
}

func showApplybutton() {
  //自己是不是在观战
  if CurrentUser.shareInstance.is_observer {
    return
  }
  applyButton.isHidden = false
}

func hideApplyButton() {
  applyButton.isHidden = true
}

func setSelfDeath() {
  CurrentUser.shareInstance.isDeath = true
  titleDeadImage.isHidden = false
  bottomView.stopCountDown()
}

  //处理返现消息
  func dealWithRebate(rebate:[String:JSON],toView:PlayerView,gift_type:String) {
    var peer_id = ""
    var rebatevalu = 0
    if let peer = rebate["peer"]?.string {
      peer_id = peer
    }
    if let value = rebate["value"]?.int {
      rebatevalu = value
    }
    //发消息
    if CurrentUser.shareInstance.id != peer_id {
      sendGiftMessages(data: toView, type: gift_type, value: rebatevalu, valuetype:"")
    }
  }
  
  //显示延时卡
  func showDelayCard() {
    showBottomBar()
    if CurrentUser.shareInstance.is_observer {
      useDelayCardButton.isHidden = true
      return
    }
    controlBackView.isHidden = false
    useDelayCardButton.isHidden = false
  }
  
  
  func dismissGameVC() {
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "giftviewout"), object: nil)
    giftmanager.view = nil
    giftmanager.clear()
    readyTipView?.dismiss();
    readyTipView?.removeFromSuperview()
    readyTipView = nil
    let currentvc = Utils.getCurrentVC(vc:(Utils.getKeyWindow()?.rootViewController)!)
    if (currentvc?.isKind(of: GameRoomViewController.self))! {
      currentvc?.dismiss(animated: true, completion: {
        let lastVC = Utils.getCurrentVC(vc:(Utils.getKeyWindow()?.rootViewController)!)
        if (lastVC?.isKind(of: ChatViewController.self))! {
          lastVC?.dismiss(animated: true, completion:{
            
          })
        }
        //        XBHHUD.showCanNotHidden(title: NSLocalizedString("正在清理房间数据", comment: ""))
      })
    }else{
      self.dismiss(animated: true, completion: {
        self.dismissGameVC()
      })
    }
  }
  
  // 检查背景 是黑夜还是白天
  func checkBG(type:String,status:Int) {
    //status 0 是黑夜  1是白天
    if status == 0 {
      //黑夜
      if type.hasPrefix("pre_simple") {
        self.backImageView.image = UIImage.init(named: "game_sunset_pre_simple")
      }
      if type.hasPrefix("simple") {
        self.backImageView.image = UIImage.init(named: "bg_simple_sunset")
      }
      if type.hasPrefix("normal")  {
        self.backImageView.image = UIImage.init(named: "bg_normal_sunset")
      }
      if type.hasPrefix("high") {
        self.backImageView.image = UIImage.init(named: "bg_high_sunset")
      }
    }else{
      //白天
      if type.hasPrefix("simple") {
        self.backImageView.image = UIImage.init(named: "bg_simple_sunup")
      }
      if type.hasPrefix("normal")   {
        self.backImageView.image = UIImage.init(named: "bg_normal_sunup")
      }
      if type.hasPrefix("pre_simple")  {
        self.backImageView.image = UIImage.init(named: "game_sunup_pre_simple")
      }
      if type.hasPrefix("high") {
        self.backImageView.image = UIImage.init(named: "bg_high_sunup")
      }
    }
  }
  
  func checkNV(type:String,status:Int) {
    if status == 0 {
      if type.hasPrefix("simple") {
        self.navBar.image = UIImage(named: "bg_simple_title_sunset")
      }
      if type.hasPrefix("normal") {
        self.navBar.image = UIImage(named: "bg_normal_title_sunset")
      }
      if type.hasPrefix("pre_simple") {
        self.navBar.image = UIImage(named: "game_title_pre_simple_sunset")
      }
      if type.hasPrefix("high") {
        self.navBar.image = UIImage(named: "bg_high_title_sunset")
      }
    }else{
      if type.hasPrefix("simple") {
        self.navBar.image = UIImage(named: "bg_simple_title")
      }
      if type.hasPrefix("normal") {
        self.navBar.image = UIImage(named: "bg_normal_title")
      }
      if type.hasPrefix("pre_simple") {
        self.navBar.image = UIImage(named: "game_title_pre_simple")
      }
      if type.hasPrefix("high") {
        self.navBar.image = UIImage(named: "bg_high_title")
      }
    }
    //提示版
    if status == 0 {
      self.contentBackImageView.image = UIImage.init(named:"提示版-黑天")
    }else{
      self.contentBackImageView.image = UIImage.init(named:"提示版-白天")
    }
  }
  
  // 选择游戏模式
  func chooseGameModel() {
    let game = Bundle.main.loadNibNamed("GameModel", owner: "", options: nil)?.last as! GameModelView
    game.tag = 988
    var rect = CGRect()
    game.backgroundColor = UIColor.clear
    let baseW = inviteButton.width
    let gameW = baseW * 2 + 20
    let gameH = gameW * 157 / 138
    let origin = CGPoint(x:readyButton.minX , y: controlBackView.minY - gameH)
    let size = CGSize(width: gameW, height: gameH)
    rect.origin = origin
    rect.size = size
    game.frame = rect
    game.selectIndex = {[weak self] tag in
      //选择人数了
      var havePlayer = Array<PlayerView>()
      var locked = Array<PlayerView>()
      var empty = Array<PlayerView>()
      for playerView in (self?.playerViewArray)! {
        if playerView.currentPositionState == .havePlayer {
          havePlayer.append(playerView)
        }
        if playerView.currentPositionState == .locked {
          locked.append(playerView)
        }
        if playerView.currentPositionState == .empty {
          empty.append(playerView)
        }
      }
      if havePlayer.count > tag {
        // 进来的人已经超过要选的模式
        let errorStr = NSLocalizedString("当前人数超过所选模式", comment: "")
        XBHHUD.showError(errorStr)
      }else{
        let lock = 12 - tag
        if lock > locked.count {
          let needLock = lock - locked.count
          var num = 0
          for player in empty.reversed() {
            if num < needLock {
              messageDispatchManager.sendMessage(type: .lock, payLoad: ["position": player.tag-1])
              num = num + 1
            }
          }
        }else{
          let needUnLock = locked.count - lock
          var num = 0
          for player in locked {
            if num < needUnLock {
              messageDispatchManager.sendMessage(type: .unlock, payLoad: ["position": player.tag-1
                ])
              num = num + 1
            }
          }
        }
      }
    }
    game.animation = "slideUp"
    game.animate()
    self.view.addSubview(game)
  }
  
  func flashNotice() {
    if flash.isAnimating == false {
      var rect = CGRect()
      let type = Utils.getCurrentDeviceType()
      if type == .iPhone5 || type == .iPhone4 {
        rect.origin = CGPoint(x: readyButton.frame.origin.x-6, y: readyButton.frame.origin.y - 6)
        rect.size = CGSize(width: readyButton.frame.width + 12, height: readyButton.frame.height + 14)
      }else{
        rect.origin = CGPoint(x: readyButton.frame.origin.x-9, y: readyButton.frame.origin.y - 8)
        rect.size = CGSize(width: readyButton.frame.width + 18, height: readyButton.frame.height + 16)
      }
      flash.frame = rect
      if self.controlBackView.subviews.contains(flash) == false {
        self.controlBackView.addSubview(flash)
        self.controlBackView.sendSubview(toBack: flash)
      }
      let name = UIImage(named: "ios4")
      let name2 = UIImage(named: "ios5")
      let animations:[UIImage] = [name!,name2!]
      flash.contentMode = .scaleAspectFill
      flash.animationImages = animations
      flash.animationRepeatCount = 0
      flash.animationDuration = 1
      flash.startAnimating()
    }else{
      var rect = CGRect()
      let type = Utils.getCurrentDeviceType()
      if type == .iPhone5 || type == .iPhone4 {
        rect.origin = CGPoint(x: readyButton.frame.origin.x-6, y: readyButton.frame.origin.y - 6)
        rect.size = CGSize(width: readyButton.frame.width + 12, height: readyButton.frame.height + 14)
      }else{
        rect.origin = CGPoint(x: readyButton.frame.origin.x-9, y: readyButton.frame.origin.y - 8)
        rect.size = CGSize(width: readyButton.frame.width + 18, height: readyButton.frame.height + 16)
      }
      flash.frame = rect
    }
  }
  
  func directToUserProfile( _ jsonData:JSON) {
    XBHHUD.showLoading()
    RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id":jsonData["id"].stringValue], success: { [weak self] (json) in
      XBHHUD.hide()
      let controller = UserInfoViewController()
      controller.peerID = (json["id"].stringValue)
      controller.peerSex = json["sex"].stringValue ?? "0"
      controller.peerName = (json["name"].stringValue)
      controller.peerIcon = json["image"].string ?? ""
      controller.isFriend =  json["is_friend"].boolValue
      controller.isTourist = CurrentUser.shareInstance.isTourist
      let nav = UINavigationController.init(rootViewController: controller)
      self?.present(nav, animated: true, completion: nil)
    }) {  (code, message) in
      XBHHUD.hide()
      XBHHUD.showError("获取用户信息失败")
    }
  }
  
  
  func analysisData() {
    self.isNewPlayerModel = CurrentUser.shareInstance.currentRoomType == "pre_simple" ? true : false
    //看看有没有召集令
    if let title = data["room"]["title"].string {
      if title != "" {
        if let export = titleBackView.subviews.first(where: {$0.isKind(of: exportNoticeView.self)}) {
          let e = export as! exportNoticeView
          e.title.text = title
          if let type = data["room"]["export_type"].string {
            e.type = type
          }
          setTitleView(title: NSLocalizedString("准备阶段", comment: ""), detail:title, userDes: "")
          export.isHidden = true
        }else{
          var rect = titleViewDetailLabel.frame
          rect.origin = CGPoint(x:0, y: rect.origin.y - 10)
          rect.size = CGSize(width: titleBackView.width, height: rect.size.height + 20)
          let export = Bundle.main.loadNibNamed("exportNotice", owner: nil, options: nil)?.last as! exportNoticeView
          export.isHidden = true
          export.frame = rect
          export.title.text = title
          if let type = data["room"]["export_type"].string {
            export.type = type
          }
          setTitleView(title: NSLocalizedString("准备阶段", comment: ""), detail:title, userDes: "")
          titleBackView.addSubview(export)
        }
      }
    }
    //能不能插麦
    if let can_cut_speaker =  data["room"]["config"]["can_cut_speaker"].bool {
      self.freeSpeech = can_cut_speaker
    }
    //等级限制
    if let arr =  data["room"]["config"]["level_limitations"].array {
      self.levels = arr
    }
    //获取当前等级
    if let currentLevel = data["room"]["config"]["level"].int {
      self.level = currentLevel
    }
    //最低等级
    if let minLevel = data["room"]["config"]["min_level"].int {
      self.min_level = minLevel
    }
    //发召集令时候的限制
    if let  config = data["room"]["config"].dictionary {
      self.gameConfig = config
    }
    //需不需要显示等级限制view
    if self.level > self.min_level {
      self.levelLimitView.isHidden = false
      self.levelLimitLabel.text = "\(level)+"
    }else{
      self.levelLimitView.isHidden = true
    }
    let tempArr = data["room"]["locked_positions"].arrayValue
    for number in tempArr {
      let view = getViewWithID(number: number.stringValue)
      view?.setIconLocked()
    }
    var totalCurrentUser = 0
    //游戏玩家
    for (key, value) in data["room"]["users"].dictionaryValue {
      totalCurrentUser = totalCurrentUser + 1
      let view = getViewWithID(number: key)
      view?.currentPostion = data["position"].intValue
      view?.playerData = value
      view?.newPlayerModel = self.isNewPlayerModel
    }
    //观战者
    //游戏观战者
    for (key, value) in data["room"]["observers"].dictionaryValue {
      if let userid = value["id"].string {
        if CurrentUser.shareInstance.id == userid {
          //自己是不是观战
          CurrentUser.shareInstance.is_observer = true
          if let position = Int(key) {
            CurrentUser.shareInstance.currentPosition = position + 1
          }
          observerUI()
        }
      }
    }
    if let observer = data["room"]["observers"].dictionary {
      self.observers = observer
    }
    if totalCurrentUser <= 3 {
      StayTime = 0
    }else if totalCurrentUser >= 4 && totalCurrentUser <= 6 {
      self.StayTime = 3
    }else{
      self.StayTime = 5
    }
    //程序倒计时
    if isNewPlayerModel {
      StayTime = 10
    }
    if StayTime == 0 {
      self.backButton.isEnabled = true
    }else{
      miniStayTime = Timer(timeInterval: 1, target: self , selector: #selector(handleMiniStay), userInfo: nil, repeats: false)
      RunLoop.current.add(miniStayTime, forMode: .commonModes)
    }
    
    let num =  NSLocalizedString("房",comment: "")
    self.navTitle.text = "\(data["room"]["room_id"].stringValue)\(num)"
    
    if let mediaServerData = data["media_server"] as? JSON,
      let serverType = mediaServerData["server_type"].string,
      serverType == "orbit" {
      startConnect(data["room"]["room_id"].stringValue, config: data["media_server"]);
    } else {
      let roomId = data["room"]["room_id"].stringValue;
    }
    
    messageViewController.addConversion(type: .system, messge: NSLocalizedString("您已进入房间", comment: ""))
    
    // 房主设置
    if CurrentUser.shareInstance.isMaster {
      readyButton.setTitle(NSLocalizedString("开局", comment: ""), for: .normal);
    }
    
    for playerView in playerViewArray {
      playerView.didTappedBlock = { [weak self] in
        
        print("playerView - tag: \($0.tag)")
        
        if CurrentUser.shareInstance.is_observer {
          //如果自己是观战
          //直接进入个人主页
          self?.directToUserProfile(playerView.playerData)
          return
        }
        
        if CurrentUser.shareInstance.isMaster {
          
          if CurrentUser.shareInstance.currentPosition != $0.tag {
            
            if $0.currentPositionState == .havePlayer {
              
              if isPlaying == true {
                let view = newPlayerAlertView.generatePlayerInfoView(type: .NORMAL, player: $0) as! newPlayerAlertView
                view.delegate = self
                Utils.showNoticeView(view: view)
              } else {
                let view = newPlayerAlertView.generatePlayerInfoView(type: .MASTER, player: $0) as! newPlayerAlertView
                view.delegate = self
                Utils.showNoticeView(view: view)
              }
              
            } else if $0.currentPositionState == .locked {
              if CurrentUser.shareInstance.currentRoomType == "simple" {
                let view = LockAlertView.view(type: .unLock, position: $0.tag)
                Utils.showNoticeView(view: view)
              }
            } else if $0.currentPositionState == .empty {
              if CurrentUser.shareInstance.currentRoomType == "simple" {
                let view = LockAlertView.view(type: .lock, position: $0.tag)
                Utils.showNoticeView(view: view)
              }
            }
            
          } else {
            // 点击的是自己的头像
            let view = newPlayerAlertView.generatePlayerInfoView(type: .NORMAL, player: $0) as! newPlayerAlertView
            view.delegate = self
            Utils.showNoticeView(view: view)
          }
          
        } else {
          
          if $0.currentPositionState == .havePlayer {
            let view = newPlayerAlertView.generatePlayerInfoView(type: .NORMAL, player: $0) as! newPlayerAlertView
            view.delegate = self
            Utils.showNoticeView(view: view)
          }
        }
      }
    }
    
    setPassword()
    //设置新手UI
    newPlayerModelUI()
    //导航栏更换
    checkNV(type: CurrentUser.shareInstance.currentRoomType,status: 1)
    //背景更换
    checkBG(type: CurrentUser.shareInstance.currentRoomType, status: 1)
    
    // 游戏服务器的断开
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(reconnect(notification:)),
                                           name: Notification.Name.init(GameMessageType.reconnect.rawValue),
                                           object: nil);
    //有新的玩家进入
    MessageManager.shareInstance.onMessageReceived(messageType: .join, delegate: self, selector: #selector(GameRoomViewController.newUserComeIN(notification:)))
    //有玩家离开
    MessageManager.shareInstance.onMessageReceived(messageType: .leave, delegate: self, selector: #selector(GameRoomViewController.userleave(notification:)))
    // 玩家准备
    MessageManager.shareInstance.onMessageReceived(messageType: .prepare, delegate: self, selector: #selector(GameRoomViewController.playerPrepare(notification:)))
    // 玩家取消准备
    MessageManager.shareInstance.onMessageReceived(messageType: .unprepare, delegate: self, selector: #selector(GameRoomViewController.unPrepare(notification:)))
    // 房主变更
    MessageManager.shareInstance.onMessageReceived(messageType: .update_master, delegate: self, selector: #selector(GameRoomViewController.masterChange(notification:)))
    // 游戏开始+抢角色
    MessageManager.shareInstance.onMessageReceived(messageType: .start, delegate: self, selector: #selector(GameRoomViewController.gameStart(notification:)))
    
    // 抢角色结果
    MessageManager.shareInstance.onMessageReceived(messageType: .apply_role_result, delegate: self, selector: #selector(GameRoomViewController.applyRoleResult(notification:)))
    
    // 角色分配
    MessageManager.shareInstance.onMessageReceived(
      messageType: .assigned_role,
      delegate: self,
      selector: #selector(GameRoomViewController.assignedRole(notification:)))
    // 天黑
    MessageManager.shareInstance.onMessageReceived(messageType: .sunset, delegate: self, selector: #selector(GameRoomViewController.sunSet(notification:)))
    // 天亮
    MessageManager.shareInstance.onMessageReceived(messageType: .sunup, delegate: self, selector: #selector(GameRoomViewController.sunUp(notification:)))
    // 有玩家开始发言
    MessageManager.shareInstance.onMessageReceived(messageType: .speak, delegate: self, selector: #selector(GameRoomViewController.playerStartSpeech(notification:)))
    // 有玩家取消发言
    MessageManager.shareInstance.onMessageReceived(messageType: .unspeak, delegate: self, selector: #selector(GameRoomViewController.playerStopSpeech(notification:)))
    // 轮麦
    MessageManager.shareInstance.onMessageReceived(messageType: .speech, delegate: self, selector: #selector(GameRoomViewController.speech(notification:)))
    // 狼人准备杀人
    MessageManager.shareInstance.onMessageReceived(messageType: .wake_to_kill, delegate: self, selector: #selector(GameRoomViewController.wolfKill(notification:)))
    // 狼人杀人最终结果
    MessageManager.shareInstance.onMessageReceived(messageType: .kill_result, delegate: self, selector: #selector(GameRoomViewController.killResult(notification:)))
    // 预言家查人
    MessageManager.shareInstance.onMessageReceived(messageType: .check, delegate: self, selector: #selector(GameRoomViewController.seerCheck(notification:)))
    // 查人结果
    MessageManager.shareInstance.onMessageReceived(messageType: .check_result, delegate: self, selector: #selector(GameRoomViewController.seerCheckResult(notification:)))
    // 女巫救人
    MessageManager.shareInstance.onMessageReceived(messageType: .save, delegate: self, selector: #selector(GameRoomViewController.witchSave(notification:)))
    // 女巫毒人
    MessageManager.shareInstance.onMessageReceived(messageType: .poison, delegate: self, selector: #selector(GameRoomViewController.witchPoison(notification:)))
    // 死亡信息
    MessageManager.shareInstance.onMessageReceived(messageType: .death_info, delegate: self, selector: #selector(GameRoomViewController.deathInfo(notification:)))
    // 投票
    MessageManager.shareInstance.onMessageReceived(messageType: .vote, delegate: self, selector: #selector(GameRoomViewController.vote(notification:)))
    // 投票结果
    MessageManager.shareInstance.onMessageReceived(messageType: .vote_result, delegate: self, selector: #selector(GameRoomViewController.voteResult(notification:)))
    // 猎人带走
    MessageManager.shareInstance.onMessageReceived(messageType: .take_away, delegate: self, selector: #selector(GameRoomViewController.takeAway(notification:)))
    // 猎人带走结果
    MessageManager.shareInstance.onMessageReceived(messageType: .take_away_result, delegate: self, selector: #selector(GameRoomViewController.takeAwayResult(notification:)))
    //游戏结束
    MessageManager.shareInstance.onMessageReceived(messageType: .game_over, delegate: self, selector: #selector(GameRoomViewController.gameOver(notification:)))
    // 锁位置
    MessageManager.shareInstance.onMessageReceived(messageType: .lock, delegate: self, selector: #selector(GameRoomViewController.lock(notification:)))
    // 踢出房间
    MessageManager.shareInstance.onMessageReceived(messageType: .kick_out, delegate: self, selector: #selector(GameRoomViewController.kickOut(notification:)))
    // 禁言
    MessageManager.shareInstance.onMessageReceived(messageType: .mute_all, delegate: self, selector: #selector(GameRoomViewController.muteAll(notification:)))
    // 解除禁言
    MessageManager.shareInstance.onMessageReceived(messageType: .unmute_all, delegate: self, selector: #selector(GameRoomViewController.unMuteAll(notification:)))
    // 聊天
    MessageManager.shareInstance.onMessageReceived(messageType: .chat, delegate: self, selector: #selector(GameRoomViewController.chat(notification:)))
    // 爱神连接
    MessageManager.shareInstance.onMessageReceived(messageType: .link, delegate: self, selector: #selector(GameRoomViewController.link(notification:)))
    // 爱神连接结果
    MessageManager.shareInstance.onMessageReceived(messageType: .link_result, delegate: self, selector: #selector(GameRoomViewController.linkResult(notification:)))
    // 是否竞选警长
    MessageManager.shareInstance.onMessageReceived(messageType: .apply, delegate: self, selector: #selector(GameRoomViewController.apply(notification:)))
    // 竞选警长结果
    MessageManager.shareInstance.onMessageReceived(messageType: .apply_result, delegate: self, selector: #selector(GameRoomViewController.applyResult(notification:)))
    // 放弃竞选
    MessageManager.shareInstance.onMessageReceived(messageType: .give_up_apply, delegate: self, selector: #selector(GameRoomViewController.giveUpApply(notification:)))
    // 最后的警长
    MessageManager.shareInstance.onMessageReceived(messageType: .sheriff_result, delegate: self, selector: #selector(GameRoomViewController.sheriffResult(notification:)))
    // 警长交接
    MessageManager.shareInstance.onMessageReceived(messageType: .hand_over, delegate: self, selector: #selector(GameRoomViewController.handOver(notification:)))
    // 警长交接结果
    MessageManager.shareInstance.onMessageReceived(messageType: .hand_over_result, delegate: self, selector: #selector(GameRoomViewController.handOverResult(notification:)))
    // 重连
    MessageManager.shareInstance.onMessageReceived(messageType: .reconnect, delegate: self, selector: #selector(GameRoomViewController.reconnect(notification:)))
    // 重连后的房间信息
    MessageManager.shareInstance.onMessageReceived(messageType: .restore_room, delegate: self, selector: #selector(GameRoomViewController.restoreRoom(notification:)))
    // 长时间未操作
    MessageManager.shareInstance.onMessageReceived(messageType: .unactive_warning, delegate: self, selector: #selector(GameRoomViewController.unActiveWarning(notification:)))
    // 修改密码
    MessageManager.shareInstance.onMessageReceived(messageType: .change_password, delegate: self, selector: #selector(GameRoomViewController.changePassword(notification:)))
    
    // 系统消息，如:换房主的通知
    MessageManager.shareInstance.onMessageReceived(messageType: .system_msg, delegate: self, selector: #selector(GameRoomViewController.systemMsg(notification:)))
    
    // 使用延迟卡
    MessageManager.shareInstance.onMessageReceived(messageType: .append_time, delegate: self, selector: #selector(GameRoomViewController.appendTime(notification:)))
    
    // 使用延迟卡的结果
    MessageManager.shareInstance.onMessageReceived(messageType: .append_time_result, delegate: self, selector: #selector(GameRoomViewController.appendTimeResult(notification:)))
    
    // 使用查验卡的结果
    MessageManager.shareInstance.onMessageReceived(messageType: .card_check_result, delegate: self, selector: #selector(GameRoomViewController.cardCheckResult(notification:)))
    
    // 服务器更换了
    MessageManager.shareInstance.onMessageReceived(messageType: .olive_changed, delegate: self, selector: #selector(GameRoomViewController.oliveChanged(notification:)))
    
    // 收到添加好友的消息
    MessageManager.shareInstance.onMessageReceived(messageType: .add_friend, delegate: self, selector: #selector(GameRoomViewController.addRequest(notification:)))
    //守卫
    MessageManager.shareInstance.onMessageReceived(messageType: .protect, delegate: self, selector: #selector(GameRoomViewController.protect(notification:)))
    //守卫结果
    MessageManager.shareInstance.onMessageReceived(messageType: .protect_result, delegate: self, selector: #selector(GameRoomViewController.protectResult(notification:)))
    //收到自爆的消息
    MessageManager.shareInstance.onMessageReceived(messageType: .boom, delegate: self, selector: #selector(GameRoomViewController.boom(notification:)))
    //自爆带人
    MessageManager.shareInstance.onMessageReceived(messageType: .boom_away, delegate: self, selector: #selector(GameRoomViewController.boom_away(notification:)))
    //自爆带人结果
    MessageManager.shareInstance.onMessageReceived(messageType: .boom_away_result, delegate: self, selector: #selector(GameRoomViewController.boomResult(notification:)))
    // 是否可以自爆
    MessageManager.shareInstance.onMessageReceived(messageType: .boom_state, delegate: self, selector: #selector(GameRoomViewController.boomAble(notification:)))
    
    // 召集令消息
    MessageManager.shareInstance.onMessageReceived(messageType: .export, delegate: self, selector: #selector(GameRoomViewController.export(notification:)))
    
    //毒人结果
    MessageManager.shareInstance.onMessageReceived(messageType: .poison_result, delegate: self, selector: #selector(GameRoomViewController.poison_result(notification:)))
    
    //救人结果
    MessageManager.shareInstance.onMessageReceived(messageType: .save_result, delegate: self, selector: #selector(GameRoomViewController.save_result(notification:)))
    
    // 更新配置
    MessageManager.shareInstance.onMessageReceived(messageType: .update_config, delegate: self, selector: #selector(GameRoomViewController.update_config(notification:)))
    
    //发言顺序
    MessageManager.shareInstance.onMessageReceived(messageType: .speech_direction, delegate: self, selector: #selector(GameRoomViewController.speech_direction(notification:)))
    
    //发言顺序救人结果
    
    MessageManager.shareInstance.onMessageReceived(messageType: .speech_direction_result, delegate: self, selector: #selector(GameRoomViewController.speech_direction_result(notification:)))
    
    //请求更换房主
    
    MessageManager.shareInstance.onMessageReceived(messageType:.request_reset_master, delegate: self, selector: #selector(GameRoomViewController.request_reset_master(notification:)))
    
    //拒绝更换房主
    
    MessageManager.shareInstance.onMessageReceived(messageType:.reject_reset_master, delegate: self, selector: #selector(GameRoomViewController.reject_reset_master(notification:)))
    
    //发送召集令结果
    MessageManager.shareInstance.onMessageReceived(messageType:.export_pay, delegate: self, selector: #selector(GameRoomViewController.export_pay(notification:)))
    
    //赠送礼物
    MessageManager.shareInstance.onMessageReceived(messageType:.send_card, delegate: self, selector: #selector(GameRoomViewController.send_card(notification:)))
    MessageManager.shareInstance.onMessageReceived(messageType:.send_gift, delegate: self, selector: #selector(GameRoomViewController.send_gift(notification:)))
    
    //狼人夜间说话功能
    MessageManager.shareInstance.onMessageReceived(messageType:.can_speak, delegate: self, selector: #selector(GameRoomViewController.action_can_speak(notification:)))
    
    //魔术师
    
    MessageManager.shareInstance.onMessageReceived(messageType:.exchange, delegate: self, selector: #selector(GameRoomViewController.action_exchange(notification:)))
    
    MessageManager.shareInstance.onMessageReceived(messageType:.exchange_result, delegate: self, selector: #selector(GameRoomViewController.action_exchange_result(notification:)))
    
    //恶魔👿
    
    MessageManager.shareInstance.onMessageReceived(messageType:.demon_check, delegate: self, selector: #selector(GameRoomViewController.action_demo_check(notification:)))
    
    MessageManager.shareInstance.onMessageReceived(messageType:.demon_check_result, delegate: self, selector: #selector(GameRoomViewController.action_demon_check_result(notification:)))
    
    //给view赋值
    //给view赋值
    giftmanager.view = self.view
  }
  
  func reconnect(notification: Notification) {
    
    Utils.runInMainThread { [weak self] in
      let jsonData = notification.object as! JSON
      
      if jsonData.intValue != 1000 , CurrentUser.shareInstance.currentRoomID != ""{
        // 重连失败
        messageDispatchManager.viewController = nil
        self?.dismiss(animated: true, completion: nil)
        self?.leaveCurrentRoom()
        XBHHUD.showError(NSLocalizedString("你已经掉线，请检查您的网络", comment: ""))
      }
    }
  }


}
