//
//  GameRoomViewController+NewUserModel.swift
//  game_werewolf
//
//  Created by xiaozao on 2018/2/6.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

extension GameRoomViewController {

  func newPlayerModelUI () {
    //新手模式
    if isPlaying {
      //是不是正在游戏
      if isNewPlayerModel{
        inviteButton.isHidden = false
        //游戏中房主
        UIView.animate(withDuration: 0.3, animations: {
          self.readyButton.snp.removeConstraints()
          self.readyButton.layoutIfNeeded()
        })
        //停止动画
        flash.stopAnimating()
        readyButton.setTitle(NSLocalizedString("全体禁言", comment: ""), for: .normal)
        readyButton.setTitle(NSLocalizedString("取消禁言", comment: ""), for: .selected)
      }
    }else{
      if isNewPlayerModel {
        //隐藏右上角按钮
        //tips颜色
        rightInviteButton.isHidden = true
        titleLockImage.isHidden = true
        righeNavButton.isHidden = true
        navRightViewWidth.constant = 33
        statusRightPadding.constant = 3
        //layoutsubview
        self.controlBackView.layoutIfNeeded()
        self.readyButton.layoutIfNeeded()
        if CurrentUser.shareInstance.isMaster {
          rightChangeMasterButton.isHidden = true
          //邀请按钮改为设置游戏人数
          inviteButton.isHidden = false
          inviteButton.setTitle(NSLocalizedString("游戏模式", comment: ""), for: .normal)
          inviteButton.setTitle(NSLocalizedString("游戏模式", comment: ""), for: .selected)
          inviteButton.setBackgroundImage(UIImage.init(named:"yellow"), for: .normal)
          inviteButton.setTitleColor(UIColor.black, for: .normal)
          readyButton.setTitle(NSLocalizedString("开局", comment: ""), for: .normal)
          readyButton.setTitle(NSLocalizedString("开局", comment: ""), for: .selected)
          //移除添加的约束
          UIView.animate(withDuration: 0.3, animations: {
            self.readyButton.snp.removeConstraints()
            self.readyButton.layoutIfNeeded()
          }, completion: { (finish) in
            //闪动
            self.flashNotice()
          })
        }else{
          //隐藏邀请按钮
          inviteButton.isHidden = true
          flash.removeFromSuperview() //换房主是要删除原来的闪动效果
          rightChangeMasterButton.isHidden = false
          //默认准备
          dismissReadyButtonTip();
          readyButton.isSelected = true
          messageDispatchManager.sendMessage(type: .prepare)
          readyButton.setTitle(NSLocalizedString("取消准备", comment: ""), for: .selected)
          //准备按钮剧中
          readyButton.snp.makeConstraints({ (make) in
            make.centerX.equalTo(self.view.snp.centerX)
          })
        }
      }
    }
    //rightChangeMasterButton
    rightChangeMasterButton.setTitleColor(UIColor.hexColor("715C97"), for: .disabled)
  }

}
