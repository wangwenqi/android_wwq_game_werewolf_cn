//
//  MicroGameViewController+GameMessage.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/24.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import Foundation
//MARK: gameMessage
//收到Socket消息
//转发给web


extension MicroGameViewController:MicroGameDelegate {

  func runDispatch(_ rawData:JSON) {
    let jsonData = rawData
    if let actionName = jsonData["type"].string {
      //消息转发
      let selector = Selector("action_\(actionName):")
      if self.responds(to: selector) {
        self.perform(selector, with: jsonData["payload"].dictionaryObject)
      }
      //把所有收到的消息转发给Web，enter——server要换一个名字，enter_init,发给web，让她初始化数据
      var jsMessage:[String : Any] = [:]
      if actionName == "enter_server" {
        jsMessage = ["gameMessageType":"enter_init","gameData":jsonData["payload"].dictionaryObject] as [String : Any]
      }else{
        //别的信息直接转发
        jsMessage = ["gameMessageType":actionName,"gameData":jsonData["payload"].dictionaryObject] as [String : Any]
      }
      SendMessageToWeb(jsMessage)
    }
  }
  
  //连接
  @objc func action_connect_info(_ rawData: [String:Any]) {
    
    let data = JSON.init(rawData)
    
    if LocalSaveVar.connectID == "" {
      LocalSaveVar.connectID = data["connect_id"].stringValue
    }
    if(messageDispatchManager.isReconnecting()) {
      messageDispatchManager.sendMessage(type: .reconnect, payLoad: ["connect_id": LocalSaveVar.connectID]);
      messageDispatchManager.setReconnectingStatus(false);
    } else {
      
      if  CurrentUser.shareInstance.from != "" {
        messageDispatchManager.sendMessage(type: .enterRoom, payLoad: [
          "room_id": CurrentUser.shareInstance.currentRoomID,
          "password": CurrentUser.shareInstance.currentRoomPassword,
          "type"    : CurrentUser.shareInstance.currentRoomType,
          "token": CurrentUser.shareInstance.token,
          "extra" : [
            "pt": "\(Config.platform)",
            "v": "\(Config.buildVersion)",
            "b": "\(Config.appStoreVersion)",
            "sv": "\(Config.serverVersion)",
            "tz": "\(Config.currentTimeZoneGMT)",
            "lg": "\(Config.systemLanage)"
          ],
          "user": [
            "id": CurrentUser.shareInstance.id,
            "name": CurrentUser.shareInstance.name,
            "avatar": CurrentUser.shareInstance.avatar,
            "level": CurrentUser.shareInstance.level,
            "experience": CurrentUser.shareInstance.experience
          ],
          "from":"export"
          ])
        
      }else{
        messageDispatchManager.sendMessage(type: .enterRoom, payLoad: [
          "room_id":  CurrentUser.shareInstance.currentRoomID,
          "password": CurrentUser.shareInstance.currentRoomPassword,
          "type"    : CurrentUser.shareInstance.currentRoomType,
          "token": CurrentUser.shareInstance.token,
          "extra" : [
            "pt": "\(Config.platform)",
            "v": "\(Config.buildVersion)",
            "b": "\(Config.appStoreVersion)",
            "sv": "\(Config.serverVersion)",
            "tz": "\(Config.currentTimeZoneGMT)",
            "lg": "\(Config.systemLanage)"
          ],
          "user": [
            "id": CurrentUser.shareInstance.id,
            "name": CurrentUser.shareInstance.name,
            "avatar": CurrentUser.shareInstance.avatar,
            "level": CurrentUser.shareInstance.level,
            "experience": CurrentUser.shareInstance.experience
          ]
          ])
      }
    }
  }
  //连接socket成功
  @objc func action_enter_server(_ rawData: [String : Any]) {
    let data = JSON.init(rawData)
    Utils.runInMainThread {
      CurrentUser.shareInstance.inRoomStatus = .begin
      let rootViewController = UIApplication.shared.keyWindow?.rootViewController
      if CurrentUser.shareInstance.currentRoomType.contains("game") {
        if let controller = messageDispatchManager.viewController as? MicroGameViewController {
          self.loadingView.searchingPlayer()
          controller.data = data;
          self.loadingView.data = data
          //自己位置
          if let position = data["position"].int {
            CurrentUser.shareInstance.currentPosition = position + 1
          }
          //语音服务地址
          self.roomId = data["room"]["room_id"].stringValue
          if let mediaServerData = data["media_server"] as? JSON {
              self.mediaServer = mediaServerData
          }
          CurrentUser.shareInstance.currentRoomID = data["room"]["room_id"].stringValue
          CurrentUser.shareInstance.realCurrentRoomID = data["room"]["room_id"].stringValue
          // 真正进入房间后进行重连次数修改
          messageDispatchManager.socketManager.currentRetryTime = messageDispatchManager.MAX_RETRY_TIMES;
        } else {
          XBHHUD.showError("数据异常，请尝试重启APP并联系管理员(Error:002)");
        }
      }
    }
  }
  //加入
  func action_join_server(_ rawData: [String : Any]) {
    let jsonData = JSON.init(rawData)
    //有新的用户加入
    status = .searchSuccess
    loadingView.player2Join(jsonData)
    if let position = jsonData["position"].int {
      self.users["\(position)"] = jsonData["user"]
    }
  }
  //离开
  func action_leave_server(_ rawData: [String : Any]) {
    //有用户离开
      let jsonData = JSON.init(rawData)
      Utils.runInMainThread { [weak self] in
        if let position = jsonData["position"].int{
          if let userid = self?.users["\(position)"]?["id"].string {
            NotificationCenter.default.post(name:NSNotification.Name(rawValue:MICRO_GAME_USER_LEAVE), object: nil, userInfo: ["user_id":userid])
          }
          self?.leaveUsers["\(position)"] = self?.users["\(position)"]
          self?.users.removeValue(forKey:"\(position)")
        }
        //需不需要提示
        if self?.status != .play && self?.status != .game_over {
          self?.leaveGame()
          XBHHUD.showError("对手已经退出".localized)
        }
      }
  }
  //用户重新连接
  func action_user_return_server(_ rawData: [String : Any]){
    let jsonData = JSON.init(rawData)
    Utils.runInMainThread { [weak self] in
      //是不是自己
        if let user_id = jsonData["user_id"].string {
          let user = jsonData.dictionary?.first(where: { (postion,userInfo) -> Bool in
            if let id = userInfo["id"].string {
              if id == user_id {
                return true
              }else{
                return false
              }
            }else{
              return false
            }
          })
          if user != nil {
            //找到了回来的人
            if let value = user?.value,
              let key = user?.key {
                self?.users[key] = value
            }
          }
          NotificationCenter.default.post(name:NSNotification.Name(rawValue:MICRO_GAME_USER_RETURN), object: nil, userInfo: ["user_id":user_id])
        }
      }
  }
  //准备好游戏
  func action_ready_server(_ rawData: [String : Any]) {
    
  }
  //游戏开始
  func action_start_game_server(_ rawData: [String : Any]) {
    
  }
  //在玩一次
  func action_replay_server(_ rawData: [String : Any]) {
    let jsonData = JSON.init(rawData)
    if let replay = jsonData["replay"].bool,
      let user_id = jsonData["user_id"].string{
      //是不是自己
      if replay {
          //通知用户在玩一次消息
          NotificationCenter.default.post(name:NSNotification.Name(rawValue:MICRO_GAME_REPLAY), object: nil, userInfo: ["user_id":user_id])
      }
    }
  }

  //游戏结束
  func action_game_over_server(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)
      self?.gameOverData = jsonData
      //先交给web处理
      self?.status = .game_over
    }
  }
  
  func action_kick_out(_ rawData: [String : Any]) {
    let jsonData = JSON.init(rawData)
    if let position = jsonData["position"].int {
      //是不是自己
      if CurrentUser.shareInstance.currentPosition == position + 1 {
        self.leaveGame()
      }
    }
  }
}

extension MicroGameViewController: MicroGameAudioDelegate {
  func action_audio_connect_server(_ rawData: [String : Any]) {
    let jsonData = JSON.init(rawData)
    if let type =  jsonData["type"].string {
      switch type {
      case "be_required":
        print("default")
      case "orbit_enter":
        print("default")
      case "orbit_close_speaker":
        print("default")
      case "orbit_open_speaker":
        print("default")
      case "orbit_open_record":
        print("default")
      case "orbit_close_record":
        print("default")
      default:
        print("default")
      }
    }
  }
}


extension MicroGameViewController {
  
  func action_enter_error() {
    self.leaveGame()
  }
  
  func reMatch(_ gameType:String) {
    CurrentUser.shareInstance.loadingGame = true
      CurrentUser.shareInstance.loadingGame = true
      NotificationCenter.default.post(
        name: NotifyName.kEnterToGameRoom.name(),
        object: nil,
        userInfo: [
          "password":"",
          "type":gameType,
          "roomId": "",
          "userName":CurrentUser.shareInstance.name,
          "userId": CurrentUser.shareInstance.id,
          "userImage": CurrentUser.shareInstance.avatar,
          "token":CurrentUser.shareInstance.token
        ])
  }
  
  func handle_game_over(_ canReplay:Bool) {
    DispatchQueue.main.async { [weak self] in
      if CurrentUser.shareInstance.realCurrentRoomID.isEmpty {
          print("******** - 已经推出 - ******")
          return
      }
      let over = GameOverProfile.generateGameOverView()
      over.landScapeModel = (self?.landScapeModel)!
      over.isPvP = (self?.isPVP)!
      over.canReplay = canReplay
      over.currentUsers = self?.users
      over.data = self?.gameOverData
      over.reMatchBlock = {[weak self] in
        self?.rematch = true
        self?.leaveGame()
      }
      over.leaveBlock = { [weak self] in
        self?.leaveGame()
      }
      over.liveTime = 25
      if (self?.landScapeModel)! {
        if let bgView = self?.view.viewWithTag(9527) {
          bgView.removeFromSuperview()
        }
        let backGroundView = UIView()
        backGroundView.tag = 9527
        let max = Screen.height > Screen.width ? Screen.height : Screen.width
        backGroundView.frame = CGRect.init(x: 0, y: 0, width: max, height: max)
        backGroundView.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        self?.view.addSubview(backGroundView)
        backGroundView.addSubview(over)
      }else{
        if CurrentUser.shareInstance.realCurrentRoomID.isEmpty {
          print("******** - 已经销毁 - ******")
          return
        }else{
            Utils.showNoticeView(view: over)
        }
      }
    }
  }
}
