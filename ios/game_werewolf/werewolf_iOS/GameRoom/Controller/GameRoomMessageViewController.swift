//
//  GameRoomMessageViewController.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

enum MessageType {
  case system
  case player
  case judge
  case none
  case gift
  case voiceGift
  case voicePlayer
  case score
  case vote
  case dinting
}

class MessgeModel: NSObject {
  var type: MessageType = .none
  var message = ""
  var positionNumber = ""
  var vote:[String:Any] = [:]
  var messageType:GameMessageType = GameMessageType(rawValue: "chat")!
  // 礼物相关
  // 礼物发给谁
  var to = 0
  // 礼物类型
  var giftType:String = OLGiftType.flower_1.rawValue;
}

class GameRoomMessageViewController: UITableViewController {
  
  struct CellID {
    static let MessageNoticeCellID = "MessageNoticeCell"
    static let MessageUserCellID = "MessageUserCell"
    static let MessageGiftCellID = "MessageGiftCell"
  }
  
  var keyboardDissBlock: (() -> Void)?
  
  var lastTimeReloaded:TimeInterval = 0
  
  // 数据数组
  var modelArray = [MessgeModel]()
  var cachedArr = [MessgeModel]()
  //是不是在送礼物
  var isSendingGift = false
  override func viewDidLoad() {
    
    super.viewDidLoad()
    tableView.backgroundColor = UIColor.clear
    tableView.separatorColor = UIColor.clear
    tableView.showsVerticalScrollIndicator = false
//    tableView.allowsSelection = false
    tableView.estimatedRowHeight = 44
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 100, right: 0)
    tableView.register(UINib.init(nibName: "MessageNoticeCell", bundle: Bundle.main), forCellReuseIdentifier: CellID.MessageNoticeCellID)
    tableView.register(UINib.init(nibName: "MessageUserCell", bundle: Bundle.main), forCellReuseIdentifier: CellID.MessageUserCellID)
    tableView.register(UINib.init(nibName: "MessageGiftCell", bundle: Bundle.main), forCellReuseIdentifier: CellID.MessageGiftCellID)
    tableView.register(UINib.init(nibName: "MessageVoteCell", bundle: Bundle.main), forCellReuseIdentifier:"MessageVoteCell")
    tableView.addTapGesture(self, handler: #selector(GameRoomMessageViewController.tapped))
    
  }
  
  override func didReceiveMemoryWarning() {
    clearAll()
    super.didReceiveMemoryWarning()
  }
  
  func clearAll(){
    modelArray.removeAll()
    tableView.reloadData()
  }
  
  func tapped() {
    if keyboardDissBlock != nil {
      keyboardDissBlock!()
    }
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if keyboardDissBlock != nil {
      keyboardDissBlock!()
    }
  }
  
  func sendingGift() {
    isSendingGift = true
  }
  
  func endSending() {
    isSendingGift = false
  }
  
  func addConversion(type: MessageType, messge: String, positionNumber: String = "") {
    
    let model = MessgeModel()
    model.message = messge
    model.type = type
    model.positionNumber = positionNumber
    
    self.addModel(model: model);
  }
  
  func addVoteConversion(message:[String:Any]) {
    let model = MessgeModel()
    model.vote = message
    model.type = .vote
    self.addModel(model: model);
  }
  
  func addConversionWith(type: MessageType, messge: String,gameMessageType:GameMessageType) {
    let model = MessgeModel()
    model.message = messge
    model.messageType = gameMessageType
    model.type = type
    
    self.addModel(model: model);
  }
  
  func addGiftConversion(from: String, to: Int, giftType:String, messge: String) {
    let model = MessgeModel()
    model.message = messge
    model.type = .gift
    model.positionNumber = from;
    model.to = to;
    model.giftType = giftType;
    
    self.addModel(model: model);
  }
  
  func addVoiceConversion(playerMessge: String, positionNumber: String = "") {
    let model = MessgeModel()
    model.message = playerMessge
    model.type = .voicePlayer
    model.positionNumber = positionNumber
    
    self.addModel(model: model);
  }
  
  func addGiftClearConversion(from: String, to: Int, giftType:String, messge: String) {
    let model = MessgeModel()
    model.message = messge
    model.type = .voiceGift
    model.positionNumber = from;
    model.to = to;
    model.giftType = giftType;
    self.addModel(model: model);
  }
  
  
  private func addModel(model:MessgeModel) {
    DispatchQueue.main.async { [weak self] in
      if self?.modelArray.count ?? 0 >= 380 {
        self?.modelArray.remove(at: 0)
      }
      self?.modelArray.append(model)
      self?.tableView.reloadData()
      self?.tableView.scrollToRow(at: IndexPath.init(row: (self?.modelArray.count)! - 1, section: 0), at: .bottom, animated: false)
    }
  }
  

  // MARK: - tableViewDelegate
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return modelArray.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if indexPath.row < modelArray.count {
      let model = modelArray[indexPath.row]
      switch model.type {
      case .system:
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.MessageNoticeCellID) as! MessageNoticeCell
        cell.model = model
        return cell
      case .gift:
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.MessageGiftCellID) as! MessageGiftCell
        cell.model = model
        cell.giftRoom = .gameRoom
        return cell
      case .voiceGift:
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.MessageGiftCellID) as! MessageGiftCell
        cell.model = model
        cell.giftRoom = .voiceRoom
        return cell
      case .voicePlayer:
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.MessageUserCellID) as! MessageUserCell
        cell.model = model
        return cell
      case .vote:
        let cell = tableView.dequeueReusableCell(withIdentifier:"MessageVoteCell") as! MessageVoteTableViewCell
        cell.selectionStyle = .none
        cell.model = model
        return cell
      default:
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.MessageUserCellID) as! MessageUserCell
        cell.model = model
        return cell
      }
    }else{
      let cell = tableView.dequeueReusableCell(withIdentifier:"MessageNoticeCell") as! MessageUserCell
      return cell
    }
  }
}
