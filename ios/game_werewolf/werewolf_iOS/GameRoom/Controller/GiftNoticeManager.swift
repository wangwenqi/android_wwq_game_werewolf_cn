//
//  GiftNoticeManager.swift
//  game_werewolf
//
//  Created by Hang on 2017/11/9.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit


struct giftNotices:Hashable{

  var hashValue: Int {
    get {
      return (type.hashValue)
    }
  }
  
  static func ==(lhs: giftNotices, rhs: giftNotices) -> Bool {
    if lhs.from == rhs.from && lhs.to == rhs.to && lhs.type == rhs.type {
      return true
    }else{
      return false
    }
  }
  
  var from:PlayerView
  var to:PlayerView
  var type:String
  var expired:Int64
  var tag:Int64
  var updatedOnShow = false
  var giftCount:Int = 1
}


class GiftNoticeManager: UIStackView {

  var queue:[giftNotices] = []
  var showQueue:[giftNotices] = []
  var full = false
  var isRuning = false
  var totalCount = 10086
  var currentNoticeCount:Int = 0
  var cards:[giftType] = [.delayTime,.checkID,.Wolf,.Witch,.Prophet,.Civilian,.Lover,.Sheriff,.Hunter,.Werewolf_king,.Iguard,.Demon,.Magican]
  //礼物管理
  var giftData:JSON? = JSON.init(shareGift.shareInstance.giftInfo)
  
  func add(element:giftNotices) {
    if queue.count == 0 {
      var new = element
      new.tag = Int64(totalCount)
      totalCount = totalCount + 1
      queue.append(new)
      generater()
    }else{
     //如果已经存在
     var isTheSame = false
     var update = queue.first(where: { $0 == element})
     if update != nil {
      //如果有重复的
      isTheSame = true
      let indexs = queue.index(of: update!)
      update?.updatedOnShow = true
      update?.giftCount = update!.giftCount + 1
      print("重复的")
      //替换
      if indexs != nil {
        queue[indexs!] = update!
      }
    }
    if isTheSame == false {
      //不是相同的，就按新的加到礼物通知里
      var new = element
      new.tag = Int64(totalCount)
      totalCount = totalCount + 1
      queue.append(new)
    }
    generater()
   }
  }
  func pop() {
//    self.currentShow = false
    print("取出下一个")
    if queue.count != 0 {
      //取出下一個要顯示的view
      generater()
    }
  }
  
  
  func generater() {
    DispatchQueue.main.async {
      var noticeNum = 0
      self.queue.forEach({ (notice) in
        if noticeNum < 3 {
          var isExsit = false
          for subview in self.arrangedSubviews {
            //并且在游戏里已经显示了出来
            if subview.tag == notice.tag {
               print("有显示在屏幕上并且重复的")
              isExsit = true
            }
          }
          if isExsit {
              if let old = self.viewWithTag(Int(notice.tag))  as? giftNoticeView {
                old.config(data: notice)
              }
          }else{
            //生成新的
            let noticeView = Bundle.main.loadNibNamed("giftNoticeView", owner: "", options: nil)?.last as! giftNoticeView
            var rect = noticeView.frame
            //检查是不是卡片
            let cardData = self.cards.first(where: { (card) -> Bool in
              if card.buyName == notice.type {
                return true
              }else{
                return false
              }
            })
            if cardData != nil {
              noticeView.image = UIImage(named:(cardData!.image))
            }
            let paths = shareGift.shareInstance.getgetThumbnailImageOnly(type: notice.type)
            if let imagePath = paths["gift_image"] as? String {
              if imagePath != "" {
                 noticeView.image = UIImage(contentsOfFile: imagePath)
              }
            }
            rect.size.width = self.frame.width
            noticeView.config(data: notice)
            noticeView.compelete = { [weak self] (noticeviewfinish) in
               //倒计时到了的话，就告诉移除
              let update = self?.queue.first(where: { $0.tag == noticeviewfinish.tag})
              if update != nil {
              if let indexOfQueue = self?.queue.index(of: update!) {
                self?.queue.remove(at: indexOfQueue)
              }
              self?.removeArrangedSubview(noticeviewfinish)
              self?.pop()
              noticeviewfinish.removeFromSuperview()
               UIView.animate(withDuration: 0.25, animations: {
                   self?.layoutIfNeeded()
               })
              }
            }
            self.addArrangedSubview(noticeView)
            UIView.animate(withDuration: 0.25, animations: {
              self.layoutIfNeeded()
            })
          }
        }
        noticeNum = noticeNum + 1
      })
    }
  }
  
  deinit {
    print("============= gift manager ===============")
  }
  
  override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
    return nil
  }
  override func awakeFromNib() {
  }
}
