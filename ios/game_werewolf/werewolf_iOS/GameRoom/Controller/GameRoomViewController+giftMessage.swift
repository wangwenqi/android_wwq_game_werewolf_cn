//
//  GameRoomViewController+giftMessage.swift
//  game_werewolf
//
//  Created by Hang on 2017/12/19.
//  Copyright © 2017年 orangelab. All rights reserved.
//

extension GameRoomViewController {
  
  
  func sendGiftMessages(data:PlayerView,type:String,value:Int,valuetype:String) {
   let name = data.playerData["name"].stringValue
   let ava = data.playerData["avatar"].stringValue
   let rec_id = data.playerData["id"].stringValue
   let rec_sex = data.playerData["sex"].stringValue ?? "1"
   chat?.createConversation(withName: name,
                             clientIds: [rec_id],
                             attributes: nil,
                             options: AVIMConversationOption.unique,
                             callback: { (conversation, error) in
                              
                              let sendMessage = OLGiftMessage.init(attibutes: [
                                "USER_SEX": "\(CurrentUser.shareInstance.sex)",
                                "USER_ICON": CurrentUser.shareInstance.avatar,
                                "USER_NAME": CurrentUser.shareInstance.name,
                                "USER_ID": CurrentUser.shareInstance.id,
                                GIFT_MSG_KEY_GITF_TYPE:type,
                                "GIFT_REBATE":"\(value)",
                                "APP": Config.app,
                                "CHAT_TIME": Utils.getCurrentTimeStamp()
                                ]);
                              
                              //此时也要发送通知，告诉RN更新
                              conversation?.send(sendMessage, callback: { (isSucceeded, error) in
                                if isSucceeded {
                                  //此时也要发送通知，告诉RN更新
                                          let dic = ["USER_NAME":name,"APP": Config.app,"USER_SEX":rec_sex,"USER_ICON":ava,"MESSAGE_TYPE":"MESSAGE_TYPE_CHAT","USER_ID":rec_id,"CHAT_TIME":String(Utils.getCurrentTimeStamp()),"CHAT_MESSAGE":NSLocalizedString("🎁赠送成功", comment: ""),"READ":true] as [String : Any]
                                              NotificationCenter.default.post(name: NotifyName.kSendMessToRN.name(), object: dic)
                                }
                              });
    });
  }
  
}

