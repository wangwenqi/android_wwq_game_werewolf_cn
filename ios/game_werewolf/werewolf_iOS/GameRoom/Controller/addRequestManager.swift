//
//  addRequestManager.swift
//  game_werewolf
//
//  Created by Hang on 2017/8/22.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class addRequestManager: NSObject {

  var queue:[addFriendRequest] = []
  var currentShow = false //有沒有請求在顯示
  weak var view:UIView?
  var rect:CGRect?
  
  func add(element:addFriendRequest) {
    queue.append(element)
    if currentShow  == false {
      //正在顯示中
      generater()
    }
  }
  func pop() {
    self.currentShow = false
    queue.removeFirst()
    if queue.count != 0 {
      //取出下一個要顯示的view
      generater()
    }
  }
  func generater() {
    guard self.currentShow == false else {
      return
    }
    let request = queue.first
    let friend = Bundle.main.loadNibNamed("InGameAddFriend", owner: "", options: nil)?.last as! InGameAddFriend
    friend.frame = rect!
    friend.delegate = self
    friend.render(request: request!)
    friend.animation = "fadeInLeft"
    friend.animate()
    self.view?.addSubview(friend)
    self.currentShow = true
  }
  
}

extension addRequestManager : addFriendRequestDelegate {
  func remove() {
    pop()
  }
}
