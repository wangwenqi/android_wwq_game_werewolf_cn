//
//  GameRoomViewController.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/13.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import orangeLab_iOS
import AudioToolbox
import EasyTipView
import ChatKit

var isPlaying = false

class GameRoomViewController: BaseViewController, UIActionSheetDelegate, MessageBaseViewController {
  var canReconnectAudio = false
  // mic
  lazy  var micImage: UIImageView = {
    let image = UIImageView()
    image.animationImages = [UIImage.init(named: "room_icon_microphone_small")!,UIImage.init(named: "room_icon_microphone_medium")!,UIImage.init(named: "room_icon_microphone_large")!]
    image.animationDuration = 0.5
    image.contentMode = .scaleToFill
    image.layer.cornerRadius = 5
    image.clipsToBounds = true
    return image
  }()
  
  var werewolfSpeakAtNight:Timer?;
  
  var addFriendSet:Set<String> = [];
  
  // lastFiveCountDownLabel
  lazy var lastFiveCountDownLabel: LTMorphingLabel = {
    
    let W: CGFloat = 150
    let H: CGFloat = 130
    
    let label = LTMorphingLabel()
    label.textColor = UIColor.white
    label.text = "5"
    label.font = UIFont.boldSystemFont(ofSize: 50)
    label.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
    label.layer.cornerRadius = 6
    label.textAlignment = .center
    label.morphingEffect = .anvil
    label.layer.masksToBounds = true
    label.frame = CGRect.init(x: (Screen.width - W) * 0.5, y: (Screen.height - H) * 0.5, width: W, height: H)
    
    let titleLabel = UILabel.init(frame: CGRect.init(x: 0, y: label.height - 40, width: label.width, height: 40))
    titleLabel.textColor = UIColor.white
    titleLabel.text = NSLocalizedString("即将结束发言", comment: "")
    titleLabel.backgroundColor = label.backgroundColor
    titleLabel.textAlignment = .center
    titleLabel.font = UIFont.systemFont(ofSize: 12)
    label.addSubview(titleLabel)
    
    return label
  }()
  
  // titleView
  @IBOutlet weak var currentUserRoleLabel: UILabel!
  @IBOutlet weak var navTitle: UILabel!
  @IBOutlet weak var titleDescribe: UILabel!
    
  @IBOutlet weak var righeNavButton: UIButton! 
  @IBOutlet weak var rightChangeMasterButton: XBHButton!
    
  @IBOutlet weak var rightInviteButton: UIButton!
  @IBOutlet weak var statusButton: UIButton!

  @IBOutlet weak var delayCardPadding: NSLayoutConstraint!
  @IBOutlet weak var titleLockImage: UIButton!
  @IBOutlet weak var titleViewTitleLabel: UILabel!
  @IBOutlet weak var titleViewDetailLabel: UILabel!
  @IBOutlet weak var titleViewUserLabel: UILabel!
  @IBOutlet weak var titleViewTimeLabel: LTMorphingLabel!
  @IBOutlet weak var titleBackView: UIView!
  @IBOutlet weak var titleBackViewRoleTotalDes: UILabel!
  @IBOutlet weak var titleBackViewLine: UIView!
  @IBOutlet weak var characterView: XBHView!
  @IBOutlet weak var rolebar: XBHView!
  @IBOutlet weak var titleDeadImage: UIImageView!
  @IBOutlet weak var backButton: UIButton!
  @IBOutlet weak var useDelayCardButton: UIButton!
  @IBOutlet weak var showUserRoleBottomSpace: NSLayoutConstraint!
  @IBOutlet weak var showUserRoleButton: UIButton!
  @IBOutlet weak var showUserRoleButtonCenterX: NSLayoutConstraint!
  @IBOutlet weak var navBar: UIImageView!
  @IBOutlet weak var boomButton: UIButton!
  @IBOutlet weak var contentBackImageView: UIImageView!
  //levelLimitView
  @IBOutlet weak var levelLimitView: UIView!
  @IBOutlet weak var levelLimitLabel: UILabel!
    //右边导航栏
  @IBOutlet weak var navRightView: UIView!
  @IBOutlet weak var navRightViewWidth: NSLayoutConstraint! 
  @IBOutlet weak var statusRightPadding: NSLayoutConstraint!
    var isReady:Bool = false {
    didSet{
        if !CurrentUser.shareInstance.isMaster {
          if isReady {
            readyButton.setTitle(NSLocalizedString("取消准备", comment: ""), for: .normal)
          }else{
            readyButton.setTitle(NSLocalizedString("准备", comment: ""), for: .normal)
        }
      }
    }
  }
  
  @IBOutlet weak var sendGiftButton: UIButton!
  

  //playerViewArray
  var playerViewArray = [PlayerView]()
  // 中途离开的玩家
  var leavedPlayerViewArray = [PlayerView]()
  // 狼人的队友
  var wolfTeammate = [JSON]()
  // 情侣
  var lovers = [Int]()
  // 警长
  var currentSheriffPosition = 0
  // 当前玩家的个数
  var currentPlayerCount = 0

  // bottomView
  let bottomView = GameRoomBottomView.bottomView(frame: CGRect(x: 0, y: Screen.height - 44, width: Screen.width, height: 44))
  //观战view
  lazy var observiewView = { () -> ObserverView in 
    let view = ObserverView.observerView(frame: CGRect(x: 0, y: Screen.height - 100, width: Screen.width, height: 100))
    return view
  }()
  //观战者
  var observers:[String:JSON] = [:] {
    didSet{
//      var keys = Array(observers.keys)
      observerCount.text = "\(observers.keys.count)人"
    }
  }
  // 显示准备的按钮
  var readyTipView:EasyTipView? = nil;
  // 提示换房主
  var changeMasterTipView:EasyTipView? = nil;
  var changeMasterHideTimer:Timer? = nil;
  
  // bottomControlView
  @IBOutlet weak var controlBackView: SpringView!
  @IBOutlet weak var readyButton: XBHButton!
  @IBOutlet weak var inviteButton: XBHButton!
  @IBOutlet weak var applyButton: UIButton!
  @IBOutlet weak var toWatchButton: XBHButton!
  @IBOutlet weak var toPlayButton: XBHButton!
  @IBOutlet weak var inviteCenterXConst: NSLayoutConstraint!
    // playerView
  @IBOutlet weak var playerPosition1: PlayerView!
  @IBOutlet weak var playerPosition2: PlayerView!
  @IBOutlet weak var playerPosition3: PlayerView!
  @IBOutlet weak var playerPosition4: PlayerView!
  @IBOutlet weak var playerPosition5: PlayerView!
  @IBOutlet weak var playerPosition6: PlayerView!
  @IBOutlet weak var playerPosition7: PlayerView!
  @IBOutlet weak var playerPosition8: PlayerView!
  @IBOutlet weak var playerPosition9: PlayerView!
  @IBOutlet weak var playerPosition10: PlayerView!
  @IBOutlet weak var playerPosition11: PlayerView!
  @IBOutlet weak var playerPosition12: PlayerView!
  
  @IBOutlet weak var leftNavView: UIView!
  
    // other
  @IBOutlet weak var backImageView: UIImageView!
  @IBOutlet weak var userBackView: UIView!
  @IBOutlet weak var giftSendButtonPadding: NSLayoutConstraint!
  @IBOutlet weak var observerCount: UILabel!
    
  
  var data = JSON.init(parseJSON: "")
  var speechingView: PlayerView? {
    willSet {
      setTitleView(userDes: " ")
      speechingView?.unSpeeching()
    }
  }
  
  // 当前游戏状态
  var gameStatus: GameStatus = .prepare
  // 消息列表
  let messageViewController = GameRoomMessageViewController()
  // 当前是否正在竞选
  var isApplying = false
  var needShowGiveUp = false
  
  /* audio metting*/
  weak internal var publisher: OLPublisher?
  internal var session:OLSession?;
  
  var olConfig:[String: Any] = [:];
  var roomId:String = "";
  var connectUser:OLConnectorInfos = OLConnectorInfos();
  
  // timeOver
  var timeOver = false
  var StayTime = 5
  var miniStayTime = Timer()
  var giftmanager:giftManager = giftManager() //礼物管理
//  var giftMessages:[giftMessage] = Array()
  var addFriendRequestManager:[addFriendRequest] = [] //好友请求管理
  var requestManager:addRequestManager = addRequestManager()
  var giftNoticeManager:GiftNoticeManager = GiftNoticeManager()
  var nextInviteTime:TimeInterval = 0
  var cardDatas:[JSON] = [];
  var card:[giftType] = [.delayTime,.checkID,.Wolf,.Witch,.Prophet,.Civilian,.Lover,.Sheriff,.Hunter,.Werewolf_king,.Iguard,.Demon,.Magican]
  //游客最多玩三次
  var playGameTime = 0
  //是不是新手模式
  var isNewPlayerModel = false
  //闪动
  let flash = UIImageView()
  var nativeGameOverAd = FBNativeAd(placementID: KeyType.kFBAdGameOverKey.keys())       // 游戏结束广告
  var nativeDeadAd = FBNativeAd(placementID: KeyType.kFBAdGameDeadKey.keys())   // 游戏死亡广告
// var nativeEnterGameAd = FBNativeAd(placementID: KeyType.kFBAdEnterRoomKey.keys()) // 进入房间广告
 
  //能不能插麦
  var freeSpeech = true {
    didSet{
      self.bottomView.freeSpeach = freeSpeech
    }
  }
  //游戏等级限制
  var level = 0
  //最小等级
  var min_level = 0
  //后台返回的等级限制
  var levels:[JSON]=[]
  //晚上应该播放的语音
  var sunsetAudio = ""
  //游戏配置
  var gameConfig:[String:JSON]?
  //后台更新设置
  var remoteConfig:JSON?
  //发言顺序
  var speeachTurn:[Int] = []
  //白狼王位置
  var werewolf_king_postion = -1
  //恶魔位置
  var demon_postion = -1
  //发送消息
  lazy var chat = {
    return LCChatKit.sharedInstance().client
  }()
  //缓存消息
  var cachedMessage:Array<JSON> = Array()
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // 进入房间广告
//    loadEnterGameNativeAd()
    // 游戏结束广告
    loadGameOverNativeAd()
    backButton.imageView?.contentMode
        = .scaleAspectFit
    backButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    bottomView.delegate = self
    if self.view.frame.origin.y == 20 { //此时处于导航模式
      var rect = view.frame
      rect.origin.y = 0
      self.view.frame = rect
      var bottom = bottomView.frame
      bottom.origin.y = rect.height - 44
      bottomView.frame = bottom
      bottomView.didInNavagationModel = true
    }
    var bottom = bottomView.frame;
    bottom.origin.y -= kSafeAreaBottomHeight;
    bottomView.frame = bottom;
    self.transitioningDelegate = self;
    view.backgroundColor = UIColor.red
    playerViewArray = [playerPosition1,playerPosition2,playerPosition3,playerPosition4,playerPosition5,playerPosition6,playerPosition7,playerPosition8,playerPosition9,playerPosition10,playerPosition11,playerPosition12]
    syncCardDatas()
    timerActionBlock = { [weak self] in
      if $0 <= 0 {
        self?.titleViewTimeLabel.text = ""
      } else {
        if $0 <= 5 {
          self?.titleViewTimeLabel.textColor = UIColor.hexColor("470F8B")//CustomColor.buttonRedColor
          self?.titleViewTimeLabel.morphingEffect = .burn
          self?.titleViewTimeLabel.font = UIFont.boldSystemFont(ofSize: 10)
        } else {
          self?.titleViewTimeLabel.textColor = UIColor.hexColor("470F8B")
          self?.titleViewTimeLabel.morphingEffect = .evaporate
          self?.titleViewTimeLabel.font = UIFont.boldSystemFont(ofSize: 10)
          if Utils.getCurrentDeviceType() == deviceType.iPhone5 || Utils.getCurrentDeviceType() == deviceType.iPhone4 {
            self?.titleViewTimeLabel.font = UIFont.boldSystemFont(ofSize: 8)
          }
        }
        self?.titleViewTimeLabel.text = "\($0)s"
      }
    }
    
    // bottomView
    bottomView.startSpeechBlock = { [weak self] in
      // 开始说话
      self?.selfStartSpeak()
      self?.session?.changeSoundType(false);
    }
    
    bottomView.stopSpeechBlock = { [weak self] in
      // 结束说话
      self?.selfStopSpeak()
      self?.session?.changeSoundType(true)
      MobClick.event("Game_speak")
    }
    
    bottomView.startLastFiveCountDown = { [weak self] in
      if self?.lastFiveCountDownLabel.superview == nil {
        
        let W: CGFloat = 100
        let H: CGFloat = 100
        self?.lastFiveCountDownLabel.frame = CGRect.init(x: (Screen.width - W) * 0.5, y: (Screen.height - H) * 0.5, width: W, height: H)
        self?.view.addSubview(self!.lastFiveCountDownLabel)
      }
      self?.lastFiveCountDownLabel.text = "\($0)"
    }
    
    bottomView.stopLastFiveCountDown = { [weak self] in
      self?.lastFiveCountDownLabel.removeFromSuperview()
    }
    
    // 上线的时候不要增加logView
    if Config.devSetting != .release {
      titleBackView.addTapGesture(self, handler: #selector(GameRoomViewController.showLogView))
    }
  
    
    customUI()
    analysisData()
    NotificationCenter.default.addObserver(self, selector: #selector(errorNotification(notification:)), name: NSNotification.Name(rawValue: kMeetingMediaErrorNotification), object: nil);
    //程序即将到后台
    NotificationCenter.default.addObserver(self, selector: #selector(GameRoomViewController.onNotificication(notification:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil);//程序即将进入前台
    NotificationCenter.default.addObserver(self, selector: #selector(GameRoomViewController.onNotificication(notification:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil);
    
    //延时卡位置
    if Screen.size.width > 375 {
      delayCardPadding.constant = 35
      giftSendButtonPadding.constant = 35
    }else if Screen.size.width == 375{
      delayCardPadding.constant = 20
       giftSendButtonPadding.constant = 20
    }else {
      delayCardPadding.constant = 10
       giftSendButtonPadding.constant = 10
    }
    //标题
    switch CurrentUser.shareInstance.currentRoomType {
      case "pre_simple_new":
        self.titleDescribe.text = NSLocalizedString("6人猎人局", comment: "")
      case "pre_simple":
         self.titleDescribe.text = NSLocalizedString("6-10人局", comment: "")
      case "simple_6":
          self.titleDescribe.text = NSLocalizedString("6人守卫局", comment: "")
      case "simple_9":
         self.titleDescribe.text = NSLocalizedString("9人屠边局", comment: "")
      case "simple_10":
        self.titleDescribe.text = NSLocalizedString("10人屠城局", comment: "")
      case "normal_guard":
         self.titleDescribe.text = NSLocalizedString("12人守卫局", comment: "")
      case "normal":
        self.titleDescribe.text = NSLocalizedString("12人爱神局", comment: "")
      case "high_king":
        self.titleDescribe.text = NSLocalizedString("12人高阶模式", comment: "")
      default:
        self.titleDescribe.text = NSLocalizedString("游戏局", comment: "")
    }
    //处理观战
    observerUI()
    //延时卡图片本地化
    delayButtonLocalized()
    //把缓存的礼物信息清楚
    shareGift.shareInstance.cachedGift = nil
    //点击返回
    addTapBack()
    //检查是否需要下载新的礼物信息
    GiftSourceManager.checkVersion()
    //礼物通知管理
    setGiftNoticeManager()
    //礼物管理
    setGiftManager()
    //已经进入房间
    CurrentUser.shareInstance.inRoomStatus = .inRoom
    // 配置一下观战按钮
    configWatcherButtons()
  }
  
  func observerUI() {
    if CurrentUser.shareInstance.is_observer {
      self.rightChangeMasterButton.isHidden = true
      self.hideBottombar()
      self.hideSpeakBar()
      useDelayCardButton.isHidden = true
      if view.subviews.contains(observiewView) == false {
        view.addSubview(observiewView)
        view.bringSubview(toFront: observiewView)
      }
    }
  }
  
  func dispatchCachedMessage() {
      if self.cachedMessage != nil {
        if (self.cachedMessage.count) > 0 {
          for message in (self.cachedMessage) {
            //发送转发消息
            self.runDispatch(message)
          }
          //clear message
          self.cachedMessage.removeAll()
        }
      }
  }
  
  func delayButtonLocalized() {
    if let lange = Bundle.main.preferredLocalizations.first {
      if lange.contains("Hans") {
        //简体
        useDelayCardButton.setImage(UIImage.init(named:"delayHansUnpress"), for: .normal)
         useDelayCardButton.setImage(UIImage.init(named:"delayHansPress"), for: .selected)
      }else if lange.contains("Hant") {
        //繁体
        useDelayCardButton.setImage(UIImage.init(named:"delayHantUnpress"), for: .normal)
        useDelayCardButton.setImage(UIImage.init(named:"delayHantPress"), for: .selected)
      }
    }
  }
    
   func addTapBack() {
    let tap =  UITapGestureRecognizer()
    tap.addTarget(self, action:#selector(leftNavButtonClicked(_:)))
    leftNavView.addGestureRecognizer(tap)
   }
    
  
  func setGiftManager() {
    var rect = self.view.bounds
    giftmanager.frame = rect
    giftmanager.backgroundColor = UIColor.clear.withAlphaComponent(0)
    shareGift.shareInstance.updateGift()
    self.view.addSubview(giftmanager)
  }
  
  func setGiftNoticeManager() {
    var rect = CGRect()
    rect.origin = CGPoint(x:self.titleBackView.frame.origin.x, y: self.titleBackView.frame.origin.y + 40)
    rect.size = CGSize(width: self.titleBackView.size.width, height: 220)
    giftNoticeManager.frame = rect
    giftNoticeManager.backgroundColor = UIColor.clear.withAlphaComponent(0)
    self.view.addSubview(giftNoticeManager)
    self.view.bringSubview(toFront: giftNoticeManager)
    giftNoticeManager.axis = .vertical
    giftNoticeManager.alignment = .fill
    giftNoticeManager.distribution = .fillEqually
  }
  
  
  func onNotificication(notification: NSNotification) {
    messageDispatchManager.sendMessage(type: .unspeak);
    if bottomView != nil {
      bottomView.speechTouchUp()
    }
  }
  
  func showLogView() {
//    Utils.showNoticeView(view: MessageManager.shareInstance.logView)
  }
  
  func customUI() {
    titleViewTitleLabel.layer.cornerRadius = titleViewTitleLabel.height * 0.5
    titleViewTimeLabel.morphingEffect = .evaporate
    view.addSubview(bottomView)
    self.navigationController?.setNavigationBarHidden(true, animated: true)
    view.insertSubview(messageViewController.view, at: 3)
    
    view.backgroundColor = CustomColor.userBackColor
    setTitleView(title: NSLocalizedString("准备阶段", comment: ""), detail: NSLocalizedString("请等待其他玩家准备", comment: ""), userDes: "")
    
    UIApplication.shared.statusBarStyle = .lightContent
    characterView.addTapGesture(self, handler: #selector(GameRoomViewController.characterViewTapped))
    
    messageViewController.keyboardDissBlock = { [weak self] in
      self?.view.endEditing(true)
    }
  }
  
  func characterViewTapped() {
    
    guard CurrentUser.shareInstance.currentRole != .none else { return }
    let show = Bundle.main.loadNibNamed("ShowRole", owner: nil, options: nil)?.last as! ShowRoleView
    show.frame = CGRect(x: 0, y: 0, width: Screen.width * 0.9, height: Screen.height * 0.9)
    show.setCenter()
    Utils.showNoticeView(view: show)
  }
  
  override func viewWillLayoutSubviews() {
    
    if rolebar.isHidden == true {
      messageViewController.view.frame = CGRect.init(x: titleBackView.minX, y: titleBackView.maxY + 8 - 23, width: titleBackView.width, height: view.height - titleBackView.maxY + 23 - 105)
    } else {
      messageViewController.view.frame = CGRect.init(x: titleBackView.minX, y: titleBackView.maxY + 8, width: titleBackView.width, height: view.height - titleBackView.maxY - 105)
    }
    
    var rect = CGRect()
    rect.origin = CGPoint(x:self.titleBackView.frame.origin.x, y: self.titleBackView.frame.origin.y + 40)
    rect.size = CGSize(width: self.titleBackView.size.width, height: 220)
    giftNoticeManager.frame = rect
    
    for playerView in playerViewArray {
      playerView.layoutSubviews()
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    readyTipView?.dismiss();
    readyTipView?.removeFromSuperview()
    readyTipView = nil
    changeMasterTipView?.dismiss();
    changeMasterHideTimer?.invalidate();
    Utils.hideNoticeView()
  }
  
  func leaveCurrentRoom() {
    CurrentUser.shareInstance.currentPosition = -1
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "giftviewout"), object: nil)
    bottomView.isHidden = true
    Utils.stopPlaySound()
    //离开后把本用户的room设置为空
    giftmanager.view = nil
    giftmanager.clear()
    CurrentUser.shareInstance.currentRoomID = ""
    CurrentUser.shareInstance.realCurrentRoomID = ""
    CurrentUser.shareInstance.from = ""
    CurrentUser.shareInstance.inRoomStatus = .none
    messageDispatchManager.sendMessage(type: .leave);
    UIApplication.shared.isIdleTimerDisabled = false
    view.endEditing(true)
    timer.invalidate()
    bottomView.stopCountDown()
//    leaveGVVoiceDisconnect(data["room"]["room_id"].stringValue)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    UIApplication.shared.isIdleTimerDisabled = true
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //缓存消息转发
    dispatchCachedMessage()
    if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeAudio) != .authorized && AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeAudio) != .notDetermined {
      let view = NormalAlertView.view(type: .twoButton, title: NSLocalizedString("提示", comment: ""), detail: NSLocalizedString("需要获取您的麦克风权限才能正常进行游戏，是否现在去设置？", comment: ""))
      view.conformClickBlock = {
        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
      }
      view.cancelClickBlock = { [weak self] (timeOver: Bool) in
        self?.dismiss(animated: true, completion: nil);
      }
      Utils.showAlertView(view: view)
      return;
    }
    
    if !CurrentUser.shareInstance.isMaster {
      checkReadyButtonTip();
      checkChangeMasterButtonTip();
    }
  }
  
  // 设置状态栏为白色
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  // MARK: - 推出房间最短时间
  
  func handleMiniStay() {
    if isPlaying == false {
    StayTime = StayTime - 1
    backButton.setTitleColor(UIColor.white, for: .normal)
    if StayTime == 0 {
      miniStayTime.invalidate()
      DispatchQueue.main.async {
         self.titleViewTimeLabel.text = ""
        self.backButton.isEnabled = true
      }
    }else{
      DispatchQueue.main.async {
        self.miniStayTime.invalidate()
        self.titleViewTimeLabel.text = String(self.StayTime)
        self.backButton.isEnabled = false
        self.miniStayTime = Timer(timeInterval: 1, target: self, selector: #selector(self.handleMiniStay), userInfo: nil, repeats: false)
        RunLoop.current.add(self.miniStayTime, forMode: .commonModes)
      }
     }
    }
  }

  // MARK:- 点击事件

  @IBAction func leftNavButtonClicked(_ sender: Any) {
    var detail = NSLocalizedString("是否退出房间?", comment: "")
    if isPlaying == true {
      if CurrentUser.shareInstance.is_observer == false{
        if CurrentUser.shareInstance.isDeath == true {
          detail = NSLocalizedString("由于您已经死亡，现在退出游戏依旧可获得经验，是否确认退出？", comment: "")
        } else {
          detail = NSLocalizedString("在游戏中，逃跑会被系统法官判定为输，减5分，并且频繁退出会被计入黑名单，确认退出？", comment: "")
        }
      }
    }
    let view = NormalAlertView.view(type: .twoButton, title: NSLocalizedString("提示", comment: ""), detail: detail)
    if isPlaying {
       view.type = .delayTouchable
      if CurrentUser.shareInstance.is_observer == false{
          view.liveTime = 5
      }else{
         view.confirmButton.isEnabled = true
      }
    }
    
    view.conformClickBlock = { [weak self] in
      if isPlaying {
        MobClick.event("Game_escape")
      }
      self?.werewolfSpeakAtNight?.invalidate();
      self?.dismiss(animated: true, completion:{
      });
//      XBHHUD.showLoading(NSLocalizedString("正在清理房间数据", comment: ""))
      XBHHUD.showCanNotHidden(title: NSLocalizedString("正在清理房间数据", comment: ""))
      messageDispatchManager.viewController = nil
      self?.leaveCurrentRoom()
    
    }
    Utils.showAlertView(view: view)
  }
  
  @IBAction func rightNavButtonClicked(_ sender: Any) {
    if CurrentUser.shareInstance.isMaster {
      var needShow = false
      if CurrentUser.shareInstance.currentRoomType == "high_king" {
          needShow = true
      }
      let view = PasswordView.view(passWord: CurrentUser.shareInstance.currentRoomPassword,needShow:needShow,needWatch:false, configuration:remoteConfig)
      view.levelLimits = self.levels
      view.min_level = self.min_level
      view.setLevel(level: self.level,minLevel: self.min_level)
      Utils.showAlertView(view: view)
    }
  }
  
  
  // 准备按钮被点击
  @IBAction func readyButtonClicked(_ sender: Any) {
    
    if isPlaying {
      // 全体禁言
      readyButton.isSelected = !readyButton.isSelected
      
      
      if readyButton.isSelected {
        messageDispatchManager.sendMessage(type: .mute_all, payLoad: ["position":CurrentUser.shareInstance.currentPosition - 1])
        readyButton.setTitle(NSLocalizedString("取消禁言", comment: ""), for: .normal)
        
      } else {
        messageDispatchManager.sendMessage(type: .unmute_all, payLoad: ["position":CurrentUser.shareInstance.currentPosition - 1])
        readyButton.setTitle(NSLocalizedString("全体禁言", comment: ""), for: .normal)
      }
      
    } else {
      if CurrentUser.shareInstance.isMaster {
        if isNewPlayerModel {
          if let game = self.view.viewWithTag(988) {
            (game as! SpringView).animation = "fall"
            (game as! SpringView).animateNext(completion: {
              game.removeFromSuperview()
            })
          }
        }
        messageDispatchManager.sendMessage(type: .start);
      } else {
        readyButton.isSelected = !readyButton.isSelected
        dismissReadyButtonTip();
        if readyButton.isSelected {
          // 准备
          messageDispatchManager.sendMessage(type: .prepare);
          readyButton.setTitle(NSLocalizedString("取消准备", comment: ""), for: .normal)
          
        } else {
          messageDispatchManager.sendMessage(type: .unprepare);
          readyButton.setTitle(NSLocalizedString("准备", comment: ""), for: .normal)
        }
      }
    }
  }
  
  // 邀请按钮被点击
  @IBAction func inviteButtonClicked(_ sender: Any) {
    
    if isPlaying {
      // 结束发言
      messageDispatchManager.sendMessage(type: .end_speech);
      hideBottombar()
      
      speechingView = nil
      bottomView.stopCountDown()
      
    } else {
      if isNewPlayerModel {
        //弹出选择框
        if let game = self.view.viewWithTag(988) {
          (game as! SpringView).animation = "fall"
          (game as! SpringView).animateNext(completion: { 
               game.removeFromSuperview()
          })
        }else{
            self.chooseGameModel()
        }
      }else{
        let alert = UIAlertController.init(title: NSLocalizedString("邀请好友", comment: ""), message:"", preferredStyle:.actionSheet)
        let action = UIAlertAction.init(title:NSLocalizedString("QQ或微信好友", comment: ""), style: .default, handler: { (action) in
          self.inviteFormQQ()
        })
        let firend = UIAlertAction.init(title:NSLocalizedString("狼人杀好友", comment: ""), style: .default, handler: { (action) in
            self.inviteFormFriend()
        })
        let myFamily = UIAlertAction.init(title:NSLocalizedString("家族成员", comment: ""), style: .default, handler: { (action) in
            self.inviteFormFamily()
        })
        let cancle = UIAlertAction.init(title:NSLocalizedString("取消", comment: ""), style: .cancel, handler: { (action) in
          
        })
        
        if CurrentUser.shareInstance.isTourist  == false {
          XBHHUD.showLoading()
          CurrentUser.shareInstance.getFamilyInfo(sucess: { (family:Family?) in
            XBHHUD.hide()
            if let _ = family {
                alert.addAction(action)
                alert.addAction(firend)
                alert.addAction(myFamily)
                alert.addAction(cancle)
            }else{
              alert.addAction(action)
              alert.addAction(firend)
              alert.addAction(cancle)
            }
            DispatchQueue.main.async {
              self.present(alert, animated: true, completion: nil)
            }
          })
        }else{
          alert.addAction(action)
          alert.addAction(cancle)
          self.present(alert, animated: true, completion: nil)
        }
      }
    }
  }
  
    @IBAction func alaboom(_ sender: Any) {
      messageDispatchManager.sendMessage(type: .boom)
    }
    
  // 放弃竞选按钮
  @IBAction func applyClicked(_ sender: Any) {
    messageDispatchManager.sendMessage(type: .give_up_apply);
    isApplying = false
    hideApplyButton()
  }
  
  //增加更换房主的按钮点击事件
    @IBAction func changeMasterButtonClicked(_ sender: UIButton) {
      messageDispatchManager.sendMessage(type: .reset_master);
      self.rightChangeMasterButton.isEnabled = false
    }
  
  //房主召集令点击
    @IBAction func masterInvite(_ sender: Any) {
      //是不是可以邀请
      if CurrentUser.shareInstance.currentRoomPassword != "" {
        //有密码，不能发召集
        let notice = UIView()
        var rect = CGRect()
        rect.origin = self.titleBackView.frame.origin
        rect.size = CGSize(width: self.titleBackView.width, height: 60)
        notice.frame = rect
        notice.layer.cornerRadius = 10
        notice.layer.masksToBounds = true
        notice.layer.borderWidth = 5
        notice.layer.borderColor = UIColor.hexColor("ffaa00").cgColor
        notice.backgroundColor = UIColor.hexColor("FEE48B")
        let lable = UILabel()
        lable.textAlignment = .center
        lable.text = NSLocalizedString("发全服召集令时不可设置房间密码", comment: "")
        lable.font = UIFont.systemFont(ofSize: 13)
        let type = Utils.getCurrentDeviceType()
        if type == .iPhone4 || type == .iPhone5 {
           lable.font = UIFont.systemFont(ofSize: 11)
        }
        notice.addSubview(lable)
        lable.snp.makeConstraints({ (make) in
          make.edges.equalToSuperview().offset(3)
        })
        UIView.animate(withDuration: 3, animations: {
          notice.alpha = 0
        }, completion: { (results) in
          notice.removeFromSuperview()
        })
        self.view.addSubview(notice)
        return
      }
      //20级一下不能发
      //现在改为从后台获取
      var inviteLevel = 20
      if let levels = self.gameConfig?["export_request"]?.dictionary!["level"]?.int {
        inviteLevel = levels
      }
      let p = CurrentUser.shareInstance.currentPosition
      let view = self.getViewWithID(number: "\(p-1)")
      let noticeStr = "\(inviteLevel)" + NSLocalizedString("级以上玩家才可以发召集令", comment: "")
      if let level = view?.playerData["level"].int {
        if level <= inviteLevel {
          XBHHUD.showError(noticeStr)
          return
        }
      }else{
        XBHHUD.showError(noticeStr)
        return
      }
      //钱是不是足够
      XBHHUD.showLoading()
      RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id": view?.playerData["id"].string], success: { [weak self] (json) in
        XBHHUD.hide()
        self?.checkInviteable(data:json)
      }) { (code, message) in
        XBHHUD.hide()
        XBHHUD.showError(message)
      }
    }
  
    @IBAction func audioStatusButtonClicked(_ sender: Any) {
        if canReconnectAudio {
          noticeUser()
        }
    }
  
  func checkInviteable(data:JSON) {
    if let money = data["money"].dictionary {
      let gold = money["gold"]?.int ?? 0
      let dim = (money["dim"]?.int ?? 0) * 66
      let totalMoney = gold + dim
      let type = self.gameConfig?["export_request"]!["cost"]["type"].string ?? ""
      let cost = self.gameConfig?["export_request"]!["cost"]["value"].int ?? 0
      if totalMoney < getCost(title: type, count: cost) {
        XBHHUD.showError("您的余额不足，快去充值吧")
        return
      }
    }
    if Date.timeIntervalSinceReferenceDate - nextInviteTime  > 0 {
      if let _ = self.view.subviews.first(where: { $0.isKind(of: masterInviteView.self) }) {
        //已经存在就不再显示啦
      }else{
        let invite = Bundle.main.loadNibNamed("masterInviteView", owner: nil, options: nil)?.last as! masterInviteView
        invite.config = self.gameConfig?["export_request"]?.dictionary
        if let export = titleBackView.subviews.first(where: {$0.isKind(of: exportNoticeView.self)}) {
          let e = export as! exportNoticeView
          //说明不是第一次发召集令，需要用以前的内容填充一下
          invite.inviteDes.text = e.title.text
          invite.reSetTitle(title: e.type)
        }
//        self.view.addSubview(invite)
        Utils.showNoticeView(view: invite)
        invite.confirmCallback = { [weak self] in
          self?.nextInviteTime = Date.timeIntervalSinceReferenceDate + 180
        }
      }
    }else{
      XBHHUD.showError(NSLocalizedString("您刚刚已经发了召集令了，请稍后再发", comment: ""))
    }
  }
  
  func noticeUser() {
    let alter =  UIAlertController(title:"提示", message: "确定要重新连接吗", preferredStyle: .alert)
    let action = UIAlertAction(title: "确定", style: .default) { [weak self] (action) in
      self?.olSessionIOError(data:["fake"])
    }
    let cancleAction = UIAlertAction(title: "取消", style:.cancel) { (action) in
        print("取消操作")
    }
    alter.addAction(action)
    alter.addAction(cancleAction)
    Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)?.present(alter, animated: true, completion: nil)
  }
  
  func getCost(title:String,count:Int) -> Int {
    switch title {
    case "gold":
      return count
    case "dim":
      return count * 66
    default:
      return count
    }
  }
  
  deinit {
    Print.dlog("gameRoomViewController deinit")
    isPlaying = false
    disConnect()
    messageDispatchManager.close();
    // 退出房间后进行重连次数修改
    messageDispatchManager.socketManager.currentRetryTime = 1
    CurrentUser.shareInstance.inRoomStatus = .none
    CurrentUser.shareInstance.clearCurrentInformation()
    NotificationCenter.default.removeObserver(self, name: GameMessageType.prepare.notificationName, object: self)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kMeetingMediaErrorNotification), object: self);
    NotificationCenter.default.removeObserver(self)
    if timeOver {
      let view = NormalAlertView.view(type: .oneButton, title: NSLocalizedString("提示", comment: ""), detail: NSLocalizedString("由于您长时间未操作，已被请出游戏房间。", comment: ""))
      Utils.showAlertView(view: view)
    }
   
    RNMessageSender.emitEvent(name: "EVENT_USER_LEAVE");
    XBHHUD.hide()
  }
  
  func getViewWithID(number: String) -> PlayerView? {
    guard let position = Int(number) else {
      return nil
    }
    return getViewWithID(position: position);
  }
  
  func getViewWithID(position: Int) -> PlayerView? {
    let positionNumber = position + 1
    for view in playerViewArray {
      if view.tag == positionNumber {
        return view
      }
    }
    return nil;
  }
  
  // 自己开始说话
  func selfStartSpeak() {
    Utils.runInMainThread { [weak self] in
      
      let micImageW: CGFloat = 100
      let micimageH: CGFloat = 100
      self?.micImage.frame = CGRect.init(x: (Screen.width - micImageW) * 0.5, y: (Screen.height - micimageH) * 0.5, width: micImageW, height: micimageH)
      self?.view.addSubview(self!.micImage)
      self?.micImage.startAnimating()
    }
  }
  // 自己停止说话
  func selfStopSpeak() {
    Utils.runInMainThread { [weak self] in
      
      self?.micImage.removeFromSuperview()
      self?.micImage.stopAnimating()
    }
  }
  
  //随机产生邀请文字
  func getRandomInviteString() -> Dictionary<String,String> {
    let randomNum = arc4random_uniform(9)
    let random = randomNum % 2
    switch random {
    case 0:
      let dic = ["localinvate":NSLocalizedString("爱神，魔术师，恶魔，白狼王，女巫..百变角色", comment: ""),"localdetail":NSLocalizedString("房间号", comment: ""),"localdetail2":NSLocalizedString("爱神，魔术师，恶魔，白狼王，守卫陪伴你和萌妹男神一起嗨，上警，悍跳，盲毒，枪杀，嗨到根本停不下来...", comment: "")]
      return dic
    case 1:
      let dic = ["localinvate":NSLocalizedString("加入游戏家族，一起探索新世界", comment: ""),"localdetail":NSLocalizedString("房间号", comment: ""),"localdetail2":NSLocalizedString("加入家族与更多人探索美好，和家族里的萌妹帅哥不分昼夜一起相爱相杀...", comment: "")]
      return dic
    default:
      let dic = ["localinvate":NSLocalizedString("爱神，魔术师，恶魔，白狼王，女巫..百变角色", comment: ""),"localdetail":NSLocalizedString("房间号", comment: ""),"localdetail2":NSLocalizedString("爱神，魔术师，恶魔，白狼王，守卫陪伴你和萌妹男神一起嗨，上警，悍跳，盲毒，枪杀，嗨到根本停不下来...", comment: "")]
      return dic
    }
  }
  
  
  func inviteFormQQ() {
    // qq或微信好友
    let dic = getRandomInviteString()
    let localdetail = dic["localdetail"]!
    let localdetail2 = dic["localdetail2"]!
    let localinvate = dic["localinvate"]!
    var detailStr = ""
    if CurrentUser.shareInstance.currentRoomPassword == "" {
      detailStr = "\(localdetail)\(CurrentUser.shareInstance.currentRoomID)，\(localdetail2)"
    } else {
      let num = NSLocalizedString("密码为", comment: "")
      detailStr = "\(localdetail)\(CurrentUser.shareInstance.currentRoomID)（\(num)\(CurrentUser.shareInstance.currentRoomPassword)），\(localdetail2)"
    }
    // 邀请
    let image = UIImage(named: "shareIcon")
    Utils.UMShare("\(localinvate)", detail: detailStr, url: "\(Config.urlHost)/invite_kuaiwan?id=\(CurrentUser.shareInstance.currentRoomID)&inviter=\(CurrentUser.shareInstance.name.addingPercentEscapes(using: String.Encoding.utf8) ?? "https://werewolf.xiaobanhui.com/share")&password=\(CurrentUser.shareInstance.currentRoomPassword)",icon:image!,type:.Invite)
  }
  
  func inviteFormFriend() {
    //邀请统计
    MobClick.event("Invite_Friends")
    XBHHUD.showLoading()
    XBHHUD.hidenSuccess = {
      SwiftConverter.shareInstance.rnMessage = ["list":JSON.init([])]
    }
    //获取好友列表
    SwiftConverter.shareInstance.sendMessagesToRN(json: generaterFriendlistJson(), compeletion: { (message) in
      XBHHUD.hide()
      if let friend = message["list"]?.arrayValue {
        var modelArray = [FriendListModel]()
        for modelData in friend {
          let model = FriendListModel()
          model.userData = modelData
          model.isSelected = false
          modelArray.append(model)
        }
        DispatchQueue.main.async {
          let view = FriendListView.view(modelArray: modelArray)
          Utils.showNoticeView(view: view)
        }
      }
    })
  }
  
  func inviteFormFamily() {
    XBHHUD.showLoading()
    CurrentUser.shareInstance.getFamilyInfo { (family:Family?) in
      XBHHUD.hide()
      if let currentFamily = family {
        let lc_id = currentFamily.lc_id
        print("\(lc_id)")
        let conversation = LCChatKit.sharedInstance().client.conversation(forId: lc_id)
        let message = OLInviteMessage(attibutes: [
          INVITE_MSG_KEY_ROOM_ID: "\(CurrentUser.shareInstance.currentRoomID)",
          "GAME_TYPE": CurrentUser.shareInstance.currentRoomType,
          "child_type": SwiftConverter.shareInstance.roomTypeName,
          INVITE_MSG_KEY_PASSWORD: CurrentUser.shareInstance.currentRoomPassword,
          "USER_SEX": "\(CurrentUser.shareInstance.sex)",
          "USER_ICON": CurrentUser.shareInstance.avatar,
          "USER_NAME": CurrentUser.shareInstance.name,
          "USER_ID": CurrentUser.shareInstance.id,
          "MSG_TYPE": "FAMILY",
          "APP": Config.app,
          "MESSAGE_TYPE": "MESSAGE_TYPE_CHAT",
          "CHAT_TIME": Utils.getCurrentTimeStamp()
          ]);
        //此时也要发送通知，告诉RN更新
        conversation?.send(message, callback: { (isSucceeded, error) in
          if isSucceeded {
            XBHHUD.showTextOnly(text:NSLocalizedString("邀请成功", comment: ""))
            let dic =
              ["USER_NAME":CurrentUser.shareInstance.name,
               "USER_SEX":CurrentUser.shareInstance.sex,
               "USER_ICON":CurrentUser.shareInstance.avatar,
               "MESSAGE_TYPE":"MESSAGE_TYPE_CHAT",
               "USER_ID":CurrentUser.shareInstance.id,
               "MSG_TYPE": "FAMILY",
               "APP": Config.app,
               "CHAT_TIME":String(Utils.getCurrentTimeStamp()),
               "CONVERSATION_ID":lc_id,
               "CHAT_MESSAGE":"游戏邀请发送成功","READ":true] as [String : Any]
            NotificationCenter.default.post(name: NotifyName.kSendMessToRN.name(), object: dic)
          } else {
            XBHHUD.showTextOnly(text:NSLocalizedString("邀请失败", comment: ""))
          }
        })
      } else {
        XBHHUD.hide()
        XBHHUD.showError("家族信息异常")
      }
    }
  }
  
  func generaterFriendlistJson() -> JSON {
    let data:Dictionary<String,Any> = ["action":"ACTION_REQUEST_FRIENDS_LIST","options":["needcallback":true,"sync":true]]
    let json = JSON.init(data)
    return json
  }
  
  //MARK: - 延迟卡
  @IBAction func delayCardButtonClicked(_ sender: UIButton) {
    messageDispatchManager.sendMessage(type: .append_time);
  }
  
    //MARK: - 赠送礼物
    @IBAction func showSendGiftButtonClicked(_ sender: UIButton) {
      
      if CurrentUser.shareInstance.isDeath {
        XBHHUD.showError(NSLocalizedString("您已经死亡，不能赠送礼物", comment: ""))
        return
      }
      
      if CurrentUser.shareInstance.isNight {
        XBHHUD.showError(NSLocalizedString("天黑了，不能赠送礼物", comment: ""))
        return
      }
      var modelArray = [FriendListModel]()
      for i in 0...11 {
        if let view = self.getViewWithID(number: "\(i)"),
          view.playerData["id"].stringValue != ""{
          let model = FriendListModel()
          model.userData = view.playerData
          model.isSelected = false
          modelArray.append(model);
        }
      }
      
      let view = SendGiftListView.view(modelArray: modelArray)
      view.sendGiftController.gameRoomViewController = self;
      Utils.showNoticeView(view: view)
    }

    // MARK: - 查验卡
    @IBAction func showUserRoleButtonClicked(_ sender: UIButton) {
      
      var jsondataArray = [(Int, JSON)]();
      
      for i in 0...11 {
        if let view = self.getViewWithID(number: "\(i)"),
          // !view.isDeath,
          CurrentUser.shareInstance.currentPosition != i + 1,
          view.playerData["id"].stringValue != "" {
          jsondataArray.append((i + 1, view.playerData))
        }
      }
      
      let view = SeerCheckView.view(type: .card_check, playerDataArray: jsondataArray, lovers: [], sheriff: self.currentSheriffPosition, wolfArr: []);
      Utils.showNoticeView(view: view);
      
//      for cardData in CurrentUser.shareInstance.cachedCardData {
//        
//      }
    }
}
