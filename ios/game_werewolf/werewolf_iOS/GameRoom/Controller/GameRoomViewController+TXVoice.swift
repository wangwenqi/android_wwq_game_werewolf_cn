////
////  GameRoomViewController+TXVoice.swift
////  game_werewolf
////
////  Created by zhihanhe on 2017/7/13.
////  Copyright © 2017年 orangelab. All rights reserved.
////
//
//import Foundation
//
////var initedGVoice = false;
//
//extension GameRoomViewController:GVGCloudVoiceDelegate {
//  func useGVoiceConnect(_ roomId:String) {
//    if let instance = GVGCloudVoice.sharedInstance(),
//      let serverInfo = (Config.GVoiceHost as NSString).utf8String {
////      if !initedGVoice {
////        initedGVoice = true;
//      // gvoice是按openid算的，这边复用
//      print("------roomID：\(roomId)\(CurrentUser.shareInstance.currentPosition)----")
//      
//        instance.setAppInfo(Config.GVoiceKey, withKey: Config.GVoicePass, andOpenID: "\(CurrentUser.shareInstance.id)");
//        instance.initEngine();
//        instance.setServerInfo(serverInfo);
////      }
//      instance.enableLog(false);
//      instance.delegate = self;
//      instance.setMode(GCloudVoiceMode.init(4));
//      instance.joinTeamRoom(roomId, timeout: 18000);
//      txVoiceTimer?.invalidate();
//      txVoiceTimer = Timer.scheduledTimer(timeInterval: 1.000/15, target: self, selector: #selector(txPullVoiceData), userInfo: nil, repeats: true);
//    }
//  }
//  
//  func leaveGVVoiceDisconnect(_ roomId: String) {
//    if let instance = GVGCloudVoice.sharedInstance(),
//      let roomName = (roomId as NSString).utf8String {
//      txVoiceTimer?.invalidate();
//      instance.closeSpeaker()
//      instance.delegate = nil;
//      instance.quitRoom(roomName, timeout: 10000);
//    }
//  }
//  
//  func onJoinRoom(_ code: GCloudVoiceCompleteCode, withRoomName roomName: UnsafePointer<Int8>?, andMemberID memberID: Int32) {
//    
//    print("---------------------onJoinRoom:\(code.rawValue) ====== roomName:\(NSString.init(utf8String: roomName!)) ==== memberID::: ====\(memberID)")
//    
//    if code == GV_ON_JOINROOM_SUCC {
//      let result = GVGCloudVoice.sharedInstance()?.openSpeaker();
//      if result?.rawValue != 0,
//        let code = result?.rawValue {
//        print(NSLocalizedString("打开声音失败，错误代码:\(code)", comment: ""));
//      }
//      self.statusButton.isHidden = false
//      self.statusButton.isSelected = false
////      messageViewController.addConversion(type: .system, messge: NSLocalizedString("语音服务器连接成功", comment: ""));
//    } else {
//      self.statusButton.isHidden = false
//      self.statusButton.isSelected = true
////      messageViewController.addConversion(type: .system, messge: NSLocalizedString("语音服务器连接异常，尝试重连", comment: ""));
//      if let name = roomName,
//        let roomId = NSString.init(utf8String: name) as String? {
//        leaveGVVoiceDisconnect(roomId);
//        useGVoiceConnect(roomId)
//      }
//    }
//  }
//  
//  func onStatusUpdate(_ status: GCloudVoiceCompleteCode, withRoomName roomName: UnsafePointer<Int8>?, andMemberID memberID: Int32) {
//    print("---------------onStatusUpdate")
//  }
//  
//  func onQuitRoom(_ code: GCloudVoiceCompleteCode, withRoomName roomName: UnsafePointer<Int8>?) {
//    print(NSLocalizedString("------------------离开房间", comment: ""));
//    let result = GVGCloudVoice.sharedInstance()?.closeSpeaker()
//    if result?.rawValue != 0,
//      let code = result?.rawValue {
//      print(NSLocalizedString("关闭声音失败，错误代码:\(code)", comment: ""));
//    }
//  }
//  
//  func onMemberVoice(_ members: UnsafePointer<UInt32>?, withCount count: Int32) {
//    print("----------------onMemberVoice");
//  }
//  
//  func onUploadFile(_ code: GCloudVoiceCompleteCode, withFilePath filePath: UnsafePointer<Int8>?, andFileID fileID: UnsafePointer<Int8>?) {
//    print("----------------onUploadFile");
//  }
//  
//  func onDownloadFile(_ code: GCloudVoiceCompleteCode, withFilePath filePath: UnsafePointer<Int8>?, andFileID fileID: UnsafePointer<Int8>?) {
//    print("----------------onDownloadFile");
//  }
//  
//  func onPlayRecordedFile(_ code: GCloudVoiceCompleteCode, withFilePath filePath: UnsafePointer<Int8>?) {
//    print("----------------onPlayRecordedFile");
//  }
//  
//  func onSpeech(toText code: GCloudVoiceCompleteCode, withFileID fileID: UnsafePointer<Int8>?, andResult result: UnsafePointer<Int8>?) {
//    print("----------------onSpeech");
//  }
//  
//  func onRecording(_ pAudioData: UnsafePointer<UInt8>?, withLength nDataLength: UInt32) {
//    print("----------------onRecording");
//  }
//  
//  func onStreamSpeech(toText code: GCloudVoiceCompleteCode, withError error: Int32, andResult result: UnsafePointer<Int8>?) {
//    print("----------------onStreamSpeech");
//  }
//  
//  func onApplyMessageKey(_ code: GCloudVoiceCompleteCode) {
//    print("----------------onApplyMessageKey");
//  }
//}

