//
//  GameRoomViewController+card.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/7/10.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation

extension GameRoomViewController {
  func syncCardDatas() {
    CurrentUser.shareInstance.getUserCards(callback: nil);
  }
}
