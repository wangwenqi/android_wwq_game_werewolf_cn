//
//  ShowAnimatedTransitioning.swift
//  TestCustomTran
//
//  Created by zhihanhe on 2017/6/1.
//  Copyright © 2017年 hzh. All rights reserved.
//

import Foundation
import UIKit;

class WolfComeIn:NSObject, UIViewControllerAnimatedTransitioning {
    let playTime = TimeInterval(0.7);
  
  let animateTime = 0.45;
    
    var finalRadius = 140.0;
    var finalPoint = CGPoint.init(x: 187.5, y: 600);
    
    var aniContext:UIViewControllerContextTransitioning? = nil;
    var gToView:UIView? = nil;
    var gFromView:UIView? = nil;
    
    let KEY_CONTAINER = "containerView";
    let KEY_TO_VIEW = "toView";
    let KEY_CONTEXT = "k_transitionContext";
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        aniContext = transitionContext
//        let fromController = transitionContext.viewController(forKey: .from);
//        let toController = transitionContext.viewController(forKey: .to);
        let container = transitionContext.containerView;
        
        if let fromView = transitionContext.view(forKey: .from),
            let toView = transitionContext.view(forKey: .to) {
            
            gFromView = fromView;

            let animate = getBigToSmallAnimation(bindView: fromView);
            animate.setValue(container, forKey: KEY_CONTAINER);
            animate.setValue(toView, forKey: KEY_TO_VIEW);
            animate.setValue(transitionContext, forKey: KEY_CONTEXT);
            fromView.layer.mask?.add(animate, forKey: "animate");
          
          let imageView = UIImageView.init(
            frame: CGRect(
              origin: CGPoint.init(
                x: fromView.frame.origin.x,
                y: fromView.frame.origin.y + 64),
              size: CGSize.init(
                width: fromView.size.width,
                height: fromView.size.height - 64
            )));
          imageView.image = UIImage.init(named: "room_bg_day");
          imageView.contentMode = .scaleAspectFill;
          
          let oldBackColor = container.backgroundColor;
          container.addSubview(imageView);
          container.backgroundColor = UIColor.init(hex: "180836");
          container.bringSubview(toFront: fromView);
          
          UIView.animate(withDuration: animateTime, animations: {
            fromView.alpha = 0.3;
          }, completion: { (finished:Bool) in
            self.gToView = toView;

            toView.alpha = 0;
            container.addSubview(toView);
            
            self.gFromView?.alpha = 0;
            self.gFromView?.layer.mask?.removeAllAnimations();
            self.gFromView?.layer.mask = nil;

            // 不知道为什么时间短了会闪一闪
//            let animate = self.getSmallToBigAnimation(bindView: toView);
//            toView.layer.mask?.add(animate, forKey: "animate");
            
            UIView.animate(withDuration: self.animateTime, animations: {
              toView.alpha = 1;
            }, completion: { (flag:Bool) in
              self.gToView?.layer.mask?.removeAllAnimations();
              self.gToView?.layer.mask = nil;
              self.aniContext?.completeTransition(flag);
              self.gFromView?.alpha = 1;
              imageView.removeFromSuperview();
              container.backgroundColor = oldBackColor;
            })

          });
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return playTime;
    }
}

//extension WolfComeIn: CAAnimationDelegate {
//  
//    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
//        if let container = anim.value(forKey: KEY_CONTAINER) as? UIView,
//            let toView = anim.value(forKey: KEY_TO_VIEW) as? UIView {
//
//          
//          
//        } else {
//          
//        }
//    }
//}

extension WolfComeIn {
    func getBigToSmallAnimation(bindView:UIView) -> CAAnimation {
        let frame = bindView.frame;
        
        let startRadius = sqrt(frame.width*frame.width + frame.height*frame.height) * 2;
        
        let startReact = CGRect.init(
            x: Double(finalPoint.x) - Double(startRadius / 2.0),
            y: Double(finalPoint.y) - Double(startRadius / 2.0),
            width: Double(startRadius),
            height: Double(startRadius))
        
        let circleMaskPathInitial = UIBezierPath.init(ovalIn: startReact);
        
        let finalReact = CGRect.init(
            x: Double(finalPoint.x) - Double(finalRadius / 2.0),
            y: Double(finalPoint.y) - Double(finalRadius / 2.0),
            width: Double(finalRadius),
            height: Double(finalRadius));
        let circleMaskPathFinal = UIBezierPath.init(ovalIn: finalReact);
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = circleMaskPathFinal.cgPath;
        bindView.layer.mask = maskLayer;
        
        let maskLayerAnimation = CABasicAnimation.init(keyPath: "path");
        maskLayerAnimation.fromValue = circleMaskPathInitial.cgPath
        maskLayerAnimation.toValue = circleMaskPathFinal.cgPath
        maskLayerAnimation.duration = animateTime - 0.1;
//        maskLayerAnimation.delegate = self;
      
        return maskLayerAnimation;
    }
    
    func getSmallToBigAnimation(bindView:UIView) -> CAAnimation {
        let frame = bindView.frame;
        
        let startRadius = sqrt(frame.width*frame.width + frame.height*frame.height) * 2;
        
        let startReact = CGRect.init(
            x: Double(finalPoint.x) - Double(startRadius / 2.0),
            y: Double(finalPoint.y) - Double(startRadius / 2.0),
            width: Double(startRadius),
            height: Double(startRadius))
        
        let circleMaskPathInitial = UIBezierPath.init(ovalIn: startReact);
        
        let finalReact = CGRect.init(
            x: Double(finalPoint.x) - Double(finalRadius / 2.0),
            y: Double(finalPoint.y) - Double(finalRadius / 2.0),
            width: Double(finalRadius),
            height: Double(finalRadius));
        let circleMaskPathFinal = UIBezierPath.init(ovalIn: finalReact);
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = circleMaskPathFinal.cgPath;
        bindView.layer.mask = maskLayer;
        
        let maskLayerAnimation = CABasicAnimation.init(keyPath: "path");
        maskLayerAnimation.fromValue = circleMaskPathFinal.cgPath
        maskLayerAnimation.toValue = circleMaskPathInitial.cgPath
        maskLayerAnimation.duration = animateTime - 0.2;
//        maskLayerAnimation.delegate = self;
      
        return maskLayerAnimation;
    }
}

//extension UIImage {
//    func subImage(rect:CGRect) -> UIImage? {
//
//        var subImage:UIImage? = nil;
//        
//        let relRect = CGRect(x: rect.origin.x * 2,
//                             y: rect.origin.y * 2,
//                             width: rect.width * 2,
//                             height: rect.height * 2);
//        
//        if let srcCGImage = self.cgImage,
//            let subImageRef:CGImage = srcCGImage.cropping(to: relRect) {
//                let bound = CGRect(origin: .zero, size: relRect.size);
//                UIGraphicsBeginImageContext(bound.size);
//                if let context = UIGraphicsGetCurrentContext() {
//                    context.draw(subImageRef, in: bound);
//                    subImage = UIImage.init(cgImage: subImageRef);
//                }
//                UIGraphicsEndImageContext();
//        }
//
//        return subImage;
//    }
//}

// 截取当前屏幕
//extension UIView {
//    func capture() -> UIImage? {
//        UIGraphicsBeginImageContextWithOptions(self.frame.size, self.isOpaque, UIScreen.main.scale);
//        if let context = UIGraphicsGetCurrentContext() {
//            self.layer.render(in: context);
//            let image = UIGraphicsGetImageFromCurrentImageContext();
//            UIGraphicsEndImageContext();
//            return image;
//        }
//        return nil;
//    }
//}
