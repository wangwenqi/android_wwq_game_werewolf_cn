//
//  GameRoomViewController+gift.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/23.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation

struct giftMessage {
  var type:GameMessageType
  var payload:[String:Any]
}

extension GameRoomViewController:GiftSysDelegate {

  func showGift(with: PlayerView) {
    let gift = giftView.initGiftViewWith(envtype: .GAME,People:with.playerData["name"].stringValue)
    gift.to = with
//    gift.delegate = self as! giftSend
    Utils.getKeyWindow()?.addSubview(gift)
    gift.animation = "fadeIn"
    gift.animate()
    gift.getGiftFormRemote()
  }
}

extension GameRoomViewController : showFriendListTosend {
  func showFriendList() {
    if CurrentUser.shareInstance.isDeath {
      XBHHUD.showError(NSLocalizedString("您已经死亡，不能赠送礼物", comment: ""))
      return
    }
    
    if CurrentUser.shareInstance.isNight {
      XBHHUD.showError(NSLocalizedString("天黑了，不能赠送礼物", comment: ""))
      return
    }
    var modelArray = [FriendListModel]()
    for i in 0...11 {
      if let view = self.getViewWithID(number: "\(i)"),
        view.playerData["id"].stringValue != ""{
        let model = FriendListModel()
        model.userData = view.playerData
        model.isSelected = false
        modelArray.append(model);
      }
    }
    
    let view = SendGiftListView.view(modelArray: modelArray)
    view.sendGiftController.gameRoomViewController = self;
    Utils.showNoticeView(view: view)
  }
}

