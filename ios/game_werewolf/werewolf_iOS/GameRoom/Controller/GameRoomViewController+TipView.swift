//
//  GameRoomViewController+TipView.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/6/19.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation
import EasyTipView;

fileprivate let keySaveReadyTip = "tip_ready_showed_";
fileprivate let keySaveChangeMasterTip = "tip_change_master_showed_";

extension GameRoomViewController {
  private func getDefaultTipViewPrefrence() -> EasyTipView.Preferences {
    var preferences = EasyTipView.Preferences()
    if isNewPlayerModel {
       preferences.drawing.backgroundColor = UIColor.init(hex: "FDA929")
    }else{
       preferences.drawing.backgroundColor = UIColor.init(hex: "00A987E7")
    }
    preferences.positioning.maxWidth = self.view.bounds.width;
    preferences.animating.dismissTransform = CGAffineTransform(translationX: 0, y: -15)
    preferences.animating.showInitialTransform = CGAffineTransform(translationX: 0, y: -15)
    preferences.animating.showInitialAlpha = 0
    preferences.animating.showDuration = 0.3
    preferences.animating.dismissDuration = 0;
    return preferences;
  }
  
  func checkReadyButtonTip() {
    
    if CurrentUser.shareInstance.isMaster {
      return;
    }
    if CurrentUser.shareInstance.is_observer {
      return;
    }
    if Utils.defaultsReadObjectWithKey("\(keySaveReadyTip)\(CurrentUser.shareInstance.id)") != nil {
      return;
    }
    if isNewPlayerModel {
      return
    }
    if isPlaying {
      return
    }
    var preferences = getDefaultTipViewPrefrence();
    preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom;
    readyTipView = EasyTipView.init(text: NSLocalizedString("要点击“准备”才可以开始游戏哦~", comment: ""), preferences: preferences, delegate: nil);
    readyTipView?.show(forView: readyButton)
  }
  
  func checkChangeMasterButtonTip() {
    
    if CurrentUser.shareInstance.isMaster {
      return;
    }
    if CurrentUser.shareInstance.is_observer {
      return;
    }
    if Utils.defaultsReadObjectWithKey("\(keySaveChangeMasterTip)\(CurrentUser.shareInstance.id)") != nil {
      return;
    }
    if isPlaying {
      return
    }
    var preferences = getDefaultTipViewPrefrence();
    preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top;
    changeMasterTipView = EasyTipView.init(text: NSLocalizedString("房主长期不开，点击票数过半后更换房主。", comment: ""), preferences: preferences, delegate: nil);
    changeMasterTipView?.show(forView: rightChangeMasterButton)
    var tapView = self.view.viewWithTag(999);
    if tapView == nil {
      tapView = UIView.init(frame: self.view.frame);
    }
    tapView?.tag = 999;
    tapView?.isUserInteractionEnabled = true;
    if let tView = tapView {
      self.view.addSubview(tView);
      self.view.bringSubview(toFront: tView);
      tView.addTapGesture(self, handler: #selector(GameRoomViewController.dissmissChageMasterButtonTip));
    }
    changeMasterHideTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(GameRoomViewController.dissmissChageMasterButtonTip), userInfo: nil, repeats: false);
  }
  
  func dismissReadyButtonTip() {
    Utils.defaultsSaveObject(true, key: "\(keySaveReadyTip)\(CurrentUser.shareInstance.id)");
    readyTipView?.dismiss();
    readyTipView = nil;
  }
  
  @objc func dissmissChageMasterButtonTip() {
    Utils.defaultsSaveObject(true, key: "\(keySaveChangeMasterTip)\(CurrentUser.shareInstance.id)");
    if changeMasterTipView == nil {
      return;
    }
    changeMasterHideTimer?.invalidate();
    changeMasterTipView?.dismiss();
    changeMasterTipView = nil;
    if let tapview = self.view.viewWithTag(999) {
      tapview.removeFromSuperview()
    }
  }

}
