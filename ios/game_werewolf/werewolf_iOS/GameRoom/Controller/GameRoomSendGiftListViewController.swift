//
//  GameRoomSendGiftListViewController.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/7/12.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation

class GameRoomSendGiftListViewController: GameRoomFriendListViewController {
  
  var gameRoomViewController:GameRoomViewController?;
  
  override func viewDidLoad() {
    super.viewDidLoad();
    self.defaultView.tipView.text = NSLocalizedString("房间内还没人，赶快邀请一起相爱相杀", comment: "");
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    Utils.hideNoticeView();

    if CurrentUser.shareInstance.isTourist {
      XBHHUD.showError(NSLocalizedString("登录后赠送礼物", comment: ""))
      return;
    }
    
    let model = modelArray[indexPath.row];
    
    if let sendGiftName = model.userData["name"].string,
      let userId = model.userData["id"].string,
      let controller = gameRoomViewController,
      let pos = model.userData["position"].int,
      let toView = gameRoomViewController?.getViewWithID(position: pos) {
      
      RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id":userId], success: { (json) in
        let gift = giftView.initGiftViewWith(envtype: .GAME, People: sendGiftName);
        gift.to = toView;
//        gift.delegate = controller;
        Utils.getKeyWindow()?.addSubview(gift)
        gift.animation = "fadeIn"
        gift.animate()
        gift.getGiftFormRemote()
        
      }) { (code, message) in
        if code == 1001 {
          XBHHUD.showError(NSLocalizedString("对方登录后可赠送礼物", comment: ""));
          return;
        }
        XBHHUD.showError(message)
      }
    } else {
      XBHHUD.showError(NSLocalizedString("对方登录后可赠送礼物", comment: ""));
    }
  }
  
  deinit {
    print("==================GameRoomSendGiftListViewController deinit============================")
  }
  
}
