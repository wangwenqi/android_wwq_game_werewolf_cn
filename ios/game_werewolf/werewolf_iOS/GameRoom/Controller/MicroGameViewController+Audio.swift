//
//  MicroGameViewController+Audio.swift
//  game_werewolf
//
//  Created by Hang on 2018/2/2.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import Foundation
import orangeLab_iOS

extension MicroGameViewController: OLSessionDelegate  {
  
  // 对外开始进行连接
  internal func startConnect(_ roomId:String, config:JSON) {
    
    var sendConfig:[String:Any] = [
      "connect_room_type": "\(config["type"].int ?? 4)",
      "node_server_host": config["media_server"].stringValue,
      "turn_server_conf": [:]
    ];
    
    if let inTurnServers = config["turn_servers"].arrayObject,
      inTurnServers.count > 0,
      let inTurnServer = inTurnServers[0] as? [String:Any],
      var turnServerConf = sendConfig["turn_server_conf"] as? [String:Any] {
      turnServerConf.updateValue(inTurnServer, forKey: "turn_server");
      sendConfig.updateValue(turnServerConf, forKey: "turn_server_conf");
    }
    
    //sendConfig["turn_server_conf"] = false;
    
    let user = OLConnectorInfos();
    user.name = "\(CurrentUser.shareInstance.name)_\(Config.appStoreVersion)";
    user.uid  = "\(CurrentUser.shareInstance.id)";
    
    print("------sendConfig:\(sendConfig)");
    startConnect(roomId, withUser: user, config:sendConfig);
  }
  
  internal func disConnect() {
    OLSession.endSetUp()
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kMeetingMediaErrorNotification), object: nil);
    publisher?.clear();
    publisher = nil;
    session?.disConnect();
    session = nil;
    //退出语音通知web
    handleOrbitStatus("orbit_exited")
  }
  
  internal func muteAll() {
    session?.sendAction("muteAll", data: ["":"" as AnyObject]);
  }
  
  func setMute(mute: Bool) {
    session?.changeSoundType(mute);
  }
  
  private func startConnect(_ roomId:String,withUser user:OLConnectorInfos, config:[String:Any]) {
    
    self.olConfig = config;
    self.roomId = roomId;
    self.connectUser = user;
    
    OLSession.beginSetUp();
    NotificationCenter.default.addObserver(self, selector: #selector(GameRoomViewController.handleError(notifiction:)), name: NSNotification.Name(rawValue: kMeetingMediaErrorNotification), object: nil);
    
    session = OLSession(roomId: self.roomId, userInfo: self.connectUser, config: self.olConfig, withDelegate: self, embedToView: view);
    
    session?.startConnect([:]);
  }
  
  func handleError(notifiction:Notification) {
    if let info = notifiction.userInfo as? [String: Any],
      let orginError = info["orginError"] as? NSError {
      if (orginError.code == 111
        || orginError.code == 113
        || orginError.code == 114) {
        
        session?.restartSocketIO();
        
      }
    }
  }
  
  
  public func olSession(_ session: orangeLab_iOS.OLSession, publisherInited publisher: orangeLab_iOS.OLPublisher) {
    self.publisher = publisher;
  }
  
  //自己成功进入
  public func olSession(_ session: orangeLab_iOS.OLSession, userDidComeIn subscribes: [orangeLab_iOS.OLSubscribe]) {
    handleOrbitStatus("orbit_connect")
  }

  
  //有用户将要进入
  public func olSession(_ session: orangeLab_iOS.OLSession, userWillComming userInfo: orangeLab_iOS.OLConnectorInfos) {
    print("userInfo --------------------- \(userInfo.name) \(userInfo.uuid)");
  }
  
  //有用户离开
  public func olSessionConnectorLeave(_ connector: orangeLab_iOS.OLConnectorInfos) {
    
  }
  
  //当自己的状态发生改变时
  public func olSession(_ session: orangeLab_iOS.OLSession, statedChange changeData: [String : Any]) {
    if let mute = changeData["mute"] as? Bool {
      session.changeSoundType(mute)
      //改变自己的按钮状态
    
    }
  }
  
  //有用户的状态发生改变
  func olSessionUserStatusChanged(_ session: OLSession, subscriber: OLSubscribe, changeData: [String : Any]) {
    
  }
  
  func olSessionStatsReport(_ session: OLSession, report: String) {
    //    print(report)
  }
  
  func olSessionIOError(data: [Any]) {
    publisher?.clear();
    session?.disConnect();
    //告诉已经断开通知web
    handleOrbitStatus("orbit_disconnect")
    OLSession.beginSetUp();
    session = OLSession(roomId: self.roomId, userInfo: self.connectUser, config: self.olConfig, withDelegate: self, embedToView: view);
    session?.startConnect([:]);
  }
  
  internal func errorNotification(notification: Notification) {
    print("info = \(notification.userInfo)");
  }
}

