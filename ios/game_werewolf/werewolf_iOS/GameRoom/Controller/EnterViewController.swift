//
//  EnterViewController.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/17.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class EnterViewController: BaseViewController, UITextFieldDelegate {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    XBHHUD.showLoading()
    NotificationCenter.default.addObserver(forName: GameMessageType.socketDidOpen.notificationName, object: nil, queue: nil) { (notification) in
    }
    
    NotificationCenter.default.addObserver(forName: GameMessageType.enterRoom.notificationName, object: nil, queue: nil) { [weak self] (notification) in
      let jsonData = notification.object as! JSON
      let controller = UIViewController.loadFromStoryBoard("Main", identifier: "GameRoomViewController") as! GameRoomViewController
      controller.data = jsonData
      self?.present(controller, animated: true, completion: nil)
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBOutlet weak var roomTextField: UITextField!
  
  @IBAction func enter(_ sender: Any) {
    
//    let view = PasswordView.view(passWord: "1234")
//    Utils.showAlertView(view: view)
  }
}
