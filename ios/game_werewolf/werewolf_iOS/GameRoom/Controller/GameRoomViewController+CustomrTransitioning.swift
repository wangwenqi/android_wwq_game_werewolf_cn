//
//  GameRoomViewController+CustomrTransitioning.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/6/1.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation

// 自定义转场动画
extension GameRoomViewController: UIViewControllerTransitioningDelegate {
  
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    let animator = WolfComeIn();
    animator.finalRadius = 1;
    animator.finalPoint = CGPoint.init(x: self.view.center.x, y: self.view.height*0.9);
    return animator;
  }
}
