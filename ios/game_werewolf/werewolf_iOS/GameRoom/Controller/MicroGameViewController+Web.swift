//
//  MicroGameViewController+Web.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/29.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import Foundation
import WebKit

extension MicroGameViewController : WKUIDelegate {
  
  func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
    let alert = UIAlertController(title: "Tip", message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) -> Void in
      // We must call back js
      completionHandler()
    }))
    self.present(alert, animated: true, completion: nil)
  }
  
  func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
    
    let alert = UIAlertController(title: "Tip", message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) -> Void in
      // We must call back js
      completionHandler(true)
    }))
    self.present(alert, animated: true, completion: nil)
  }
}

//MARK: delegate
extension MicroGameViewController: WKNavigationDelegate {
  
  func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
    
  }
  //开始加载
  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    //开始加载
  }
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    //加载成功
  }
  
  func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    errorAction()
  }
  
  func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
    errorAction()
  }
  
  
  func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
    
    if let response = navigationResponse.response as? HTTPURLResponse {
      if response.statusCode > 200 {
        errorAction()
      }
    }
    //允许跳转
    decisionHandler(.allow)
  }

  func errorAction() {
    leaveGame()
    XBHHUD.showError("游戏初始化失败")
  }
  
  func reload() {
    let request = URLRequest(url: URL(string:gameurl)!)
    self.web?.load(request)
  }
}

//给Web发送消息
extension MicroGameViewController {
  func SendMessageToWeb(_ data:Dictionary<String, Any>) {
    if let gameMessageType = data["gameMessageType"] as? String,
      let gameData = data["gameData"] {
      if JSONSerialization.isValidJSONObject(gameData) == false {
        print("*********** - JS message ERROR - ***************************************************")
        print(data)
        print("*********** - JS message ERROR - ***************************************************")
        return
      }
      if let jsData = try? JSONSerialization.data(withJSONObject:gameData, options: JSONSerialization.WritingOptions(rawValue: 0)){
        if let jsDataString = String(data: jsData, encoding: .utf8){
          let jsStringWithUTF8 = "postJSDirectlyCommend(" + "'" + "\(gameMessageType)" + "'" + ","  + "'" + jsDataString + "'" + ")"
          print( "*********** did send JS message ***************************************************" )
          print(jsStringWithUTF8)
          web?.evaluateJavaScript(jsStringWithUTF8, completionHandler:nil)
          print(" *********** did send JS message *************************************************** ")
        }
      }
    }
  }
}

//接受Web消息
//把web消息回传给Server
extension MicroGameViewController: WKScriptMessageHandler {
  @available(iOS 8.0, *)
  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    if let dic = message.body as? NSDictionary,
      let functionName = dic["functionName"] as? String,
      let para = dic["parameter"]{
      //测试
      print("===================")
      print(dic)
      print("===================")
      if functionName == "postNativeDirectlyCommend" {
        if let arr = para as? Array<Any>,
          let type = arr.first as? String,
          let payloadString = arr.last as? String{
          if let payload = convertToDictionary(text: payloadString) {
            dispatchJSMessage(type: type, payload: payload)
          }
        }
      }
    }
  }
  //转发Web消息到Server
  func dispatchJSMessage(type:String,payload:[String:Any]) {
    //后台准备完毕
    if type == "enter_init" {
      //web加载成功
    }else if type == "dom_init" {
      connectMicroGameSocket()
    }else if type == "dom_show"{
        webShow = true
        handleShowWeb()
    }else if type == "ready" {
      //现在由web，告诉我们是不是要发送ready
      messageDispatchManager.sendMicroGameMessage(type:"ready", payLoad: payload)
    }else if type.hasSuffix("server") {
      //以server为结尾的不发送给server
      if type == "start_game_server" {
        webShow = true
        if let ready = payload["ready"] as? Bool {
          if ready {
              status = .play
              handleShowWeb()
          }
        }
      }
      if type == "game_over_server" {
        if let ready = payload["ready"] as? Bool {
          if ready {
            status = .game_over
            //游戏结束，并且用户点击返回，弹出这个
            var canReplay = true
            if let replay = payload["again"] as? Bool {
              canReplay = replay
            }
            handle_game_over(canReplay)
          }
        }
      }
    }else if type.hasPrefix("orbit"){
      //以obit开头的不发给server
      handleOrbitAction(type)
    }else if type == "native_destroy"{
      leaveGame()
      if let reason = payload["reason"] as? String {
         XBHHUD.showError(reason)
      }
    }else{
      //其余消息直接给server
      messageDispatchManager.sendMicroGameMessage(type: type, payLoad: payload)
    }
  }
  
  func handleShowWeb() {
    //收到这个消息时，要隐藏loading界面
    if loadingView.isHidden == false {
      loadingView.animation = "fadeOut"
      loadingView.animateNext {
        self.loadingView.isHidden = true
      }
      if self.landScapeModel {
        DispatchQueue.main.async {
          let value = UIInterfaceOrientation.landscapeLeft.rawValue
          UIDevice.current.setValue(value, forKey: "orientation")
        }
      }
    }
    //是不是在结束界面
    if Utils.getCurrentNoticeViews().count > 0 {
      //说明结束界面存在,需要隐藏
      Utils.hideNoticeView()
    }else{
      //如果有横屏的结束界面
      if let bgView = view.viewWithTag(9527) {
        bgView.removeFromSuperview()
      }
    }
  }
  
  //连接socket
  func connectMicroGameSocket() {
    if let url = socketConnect?["url"] as? URL,
      let roomid = socketConnect?["roomid"] as? String,
      let secureConfig = socketConnect?["secureConfig"] as? Bool {
      //加载成功以后，在连接socket
      self.status = .loadSuccess
      messageDispatchManager.socketManager.connectSocketIO(url:url, roomid:roomid, secureConfig:secureConfig)
    }
  }
  //处理orbit事件
  func handleOrbitAction(_ action:String) {
    switch action {
      case "orbit_exit":
        orbit_exit()
      case "orbit_enter":
          orbit_enter()
      case "orbit_close_speaker":
          orbit_close_speaker()
      case "orbit_open_speaker":
          orbit_open_speaker()
      case "orbit_open_record":
          orbit_open_record()
      case "orbit_close_record":
          orbit_close_record()
    default:
      print("default")
    }
  }
  
  //处理orbit状态
  func handleOrbitStatus( _ status:String) {
    switch status {
    case "orbit_exited":
      orbit_exited()
    case "orbit_disconnect":
      orbit_disconnect()
    case "orbit_connect":
      orbit_connect()
    default:
      print("default")
    }
  }
  
  func orbit_exited() {
    let jsMessage = ["gameMessageType":"orbit_exited","gameData":[:]] as [String : Any]
    SendMessageToWeb(jsMessage)
  }
  
  func orbit_disconnect() {
    let jsMessage = ["gameMessageType":"orbit_disconnect","gameData":[:]] as [String : Any]
    SendMessageToWeb(jsMessage)
  }
  
  func orbit_connect() {
    let jsMessage = ["gameMessageType":"orbit_connect","gameData":[:]] as [String : Any]
    SendMessageToWeb(jsMessage)
  }
  
  func orbit_enter() {
    if mediaServer != nil {
      startConnect(roomId, config: mediaServer!)
    }
  }
  
  func orbit_exit() {
    disConnect()
  }
  
  func orbit_close_speaker() {
    
  }
  
  func orbit_open_speaker() {
    
  }
  
  func orbit_open_record() {
    setMute(mute: false)
  }
  
  func orbit_close_record() {
      setMute(mute: true)
  }
  
}

// String -> Dictionary
extension MicroGameViewController {
  func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
      do {
        return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
      } catch {
        print(error.localizedDescription)
      }
    }
    return nil
  }
}
