//
//  GameRoomFriendListViewController.swift
//  game_werewolf
//
//  Created by QiaoYijie on 2017/5/24.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import ChatKit

class GameRoomFriendListViewController: UITableViewController {
  
  let gameRoomFriendListCellID = "GameRoomFriendListCell"
  var isCellBackClear = false {
    didSet {
      tableView.backgroundColor = UIColor(hex: "#e2dee8")
    }
  }
  
  let imClient = LCChatKit.sharedInstance().client
  
  let defaultView = DefaultView.view()
  
  // 数据数组
  var modelArray = [FriendListModel]()
  var selectedArray = [FriendListModel]()
  
  // 点击了邀请按钮
  func inviteFriend() {
    
    if modelArray.count == 0 {
      XBHHUD.showTextOnly(text: NSLocalizedString("暂时没有在线好友，试试其它邀请方式", comment: ""))
      return
    }
    var selectCount = 0;
    for model in modelArray {
      if model.isSelected {
        selectCount += 1;
        imClient?.createConversation(withName: model.userData["name"].stringValue,
                                     clientIds: [model.userData["id"].stringValue],
                                     attributes: nil,
                                     options: AVIMConversationOption.unique,
                                     callback: { (conversation, error) in
                                      
                                      let message = OLInviteMessage(attibutes: [
                                        INVITE_MSG_KEY_ROOM_ID: "\(CurrentUser.shareInstance.currentRoomID)",
                                        "GAME_TYPE": CurrentUser.shareInstance.currentRoomType,
                                        "child_type": SwiftConverter.shareInstance.roomTypeName,
                                        INVITE_MSG_KEY_PASSWORD: CurrentUser.shareInstance.currentRoomPassword,
                                        "USER_SEX": "\(CurrentUser.shareInstance.sex)",
                                        "USER_ICON": CurrentUser.shareInstance.avatar,
                                        "USER_NAME": CurrentUser.shareInstance.name,
                                        "USER_ID": CurrentUser.shareInstance.id,
                                        "MESSAGE_TYPE": "MESSAGE_TYPE_CHAT",
                                        "APP": Config.app,
                                        "CHAT_TIME": Utils.getCurrentTimeStamp()
                                        ]);
                                      //此时也要发送通知，告诉RN更新
                                      conversation?.send(message, callback: { (isSucceeded, error) in
                                        if isSucceeded {
                                          XBHHUD.showTextOnly(text: "邀请成功")
                                          let dic =
                                            ["USER_NAME":model.userData["name"].stringValue,
                                             "USER_SEX":model.userData["sex"].stringValue,
                                             "APP": Config.app,
                                             "USER_ICON":model.userData["image"].stringValue,
                                             "MESSAGE_TYPE":"MESSAGE_TYPE_CHAT",
                                             "USER_ID":model.userData["id"].stringValue,
                                             "CHAT_TIME":String(Utils.getCurrentTimeStamp()),
                                             "CHAT_MESSAGE":"游戏邀请发送成功","READ":true] as [String : Any]
                                          NotificationCenter.default.post(name: NotifyName.kSendMessToRN.name(), object: dic)
                                        } else {
                                          XBHHUD.showTextOnly(text: "邀请失败")
                                        }
                                        Utils.hideNoticeView()
                                      });
                                      
                                  });
                        }
    }
    
    if selectCount == 0 {
      XBHHUD.showTextOnly(text: NSLocalizedString("您没有选择任何好友", comment: ""))
    } else {
      if let sendType = Utils.getTaskType(platform: .userDefine_Begin, type: .Invite) {
        print(NSLocalizedString("----------发送数据:\(sendType);;;count:\(selectCount)", comment: ""));
        RequestManager().post(
          url: RequestUrls.sendTaskData,
          parameters: ["type": sendType, "count": selectCount],
          success: { (response) in
            
        }, error: { (code, message) in
        });
      }
    }
    Utils.hideNoticeView()
  }

  func sendMutiMessage(model:FriendListModel,done:@escaping AVBooleanResultBlock) {
    LCChatKit.sharedInstance().client.createConversation(withName: model.userData["name"].stringValue, clientIds: [model.userData["id"].stringValue], attributes: nil, options: AVIMConversationOption.unique) { (conversation, error) in
      usleep(5000)
      let message = OLInviteMessage(attibutes: [
        INVITE_MSG_KEY_ROOM_ID: "\(CurrentUser.shareInstance.currentRoomID)",
        "GAME_TYPE": CurrentUser.shareInstance.currentRoomType,
        "child_type": SwiftConverter.shareInstance.roomTypeName,
        INVITE_MSG_KEY_PASSWORD: CurrentUser.shareInstance.currentRoomPassword,
        "USER_SEX": "\(CurrentUser.shareInstance.sex)",
        "USER_ICON": CurrentUser.shareInstance.avatar,
        "USER_NAME": CurrentUser.shareInstance.name,
        "USER_ID": CurrentUser.shareInstance.id,
        "MESSAGE_TYPE": "MESSAGE_TYPE_CHAT",
        "APP": Config.app,
        "CHAT_TIME": Utils.getCurrentTimeStamp()
        ]);
        conversation?.send(message, callback: {(result,error) in
          sleep(1)
          done(result,error)
        })
    }
  }

//  func sendMessage() -> Void {
//    if selectedArray.count == 0 { return }
//    let model = selectedArray.last
//    sendMutiMessage(model: model!) { (restult, error) in
//
//    }
//  }
//
  func sendMessage() -> Void {
    if selectedArray.count == 0 { return }
    let model = selectedArray.last
    LCChatKit.sharedInstance().client.createConversation(withName: model!.userData["name"].stringValue,
                                                         clientIds: [model!.userData["id"].stringValue],
                                                         attributes: nil,
                                                         options: AVIMConversationOption.unique,
                                                         callback: { (conversation, error) in
                                                          
                                                          let message = OLInviteMessage(attibutes: [
                                                            INVITE_MSG_KEY_ROOM_ID: "\(CurrentUser.shareInstance.currentRoomID)",
                                                            "GAME_TYPE": CurrentUser.shareInstance.currentRoomType,
                                                            "child_type": SwiftConverter.shareInstance.roomTypeName,
                                                            INVITE_MSG_KEY_PASSWORD: CurrentUser.shareInstance.currentRoomPassword,
                                                            "USER_SEX": "\(CurrentUser.shareInstance.sex)",
                                                            "USER_ICON": CurrentUser.shareInstance.avatar,
                                                            "USER_NAME": CurrentUser.shareInstance.name,
                                                            "USER_ID": CurrentUser.shareInstance.id,
                                                            "MESSAGE_TYPE": "MESSAGE_TYPE_CHAT",
                                                            "APP": Config.app,
                                                            "CHAT_TIME": Utils.getCurrentTimeStamp()
                                                            ]);
                                                          //此时也要发送通知，告诉RN更新
                                                          conversation?.send(message, callback: {(_, _) in
                                                            self.selectedArray.removeLast()
                                                            self.sendMessage()
                                                          });
                                                          
    });
  }

  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    super.viewDidLoad()
    tableView.backgroundColor = UIColor.clear
    tableView.separatorColor = UIColor.clear
    tableView.showsVerticalScrollIndicator = false
    tableView.rowHeight = 76
    //    tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 100, right: 0)
    
    tableView.register(UINib.init(nibName: "GameRoomFriendListCell", bundle: Bundle.main), forCellReuseIdentifier: gameRoomFriendListCellID)
    view.addSubview(defaultView)
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    defaultView.frame = tableView.bounds
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Table view data source
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if modelArray.count == 0 {
      defaultView.isHidden = false
    } else {
      defaultView.isHidden = true
    }
    
    return modelArray.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let model = modelArray[indexPath.row]
    let cell = tableView.dequeueReusableCell(withIdentifier: gameRoomFriendListCellID) as! GameRoomFriendListCell
    if isCellBackClear {
      cell.backgroundColor = UIColor.clear
    }
    cell.model = model
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let model = modelArray[indexPath.row]
    let cell = tableView.cellForRow(at: indexPath) as! GameRoomFriendListCell
//    if cell.isOnLine {
        model.isSelected = !model.isSelected
        cell.selectedButton.isSelected = model.isSelected
//    }
  }
}
