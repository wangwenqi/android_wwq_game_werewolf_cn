//
//  GameRoomViewController+Watcher.swift
//  game_werewolf
//
//  Created by xiaozao on 2018/2/5.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

extension GameRoomViewController {

    // 观战几种状态
  // 游戏开始
  /*
   readyButton // 准备按钮
   inviteButton // 邀请按钮
   */
  
  func configWatcherButtons() -> Void {
//    toWatchButton.setBackgroundImage(UIImage.init(named:"yellow"), for: .normal)
//    toPlayButton.setBackgroundImage(UIImage.init(named:"yellow"), for: .normal)
    toWatchButton.isHidden = true
    toPlayButton.isHidden = true
    inviteCenterXConst.constant = 43
    
  }
  
  func showButtonsChange(_ isMaster: Bool) -> Void {
    
  }
  
  func hideButtonsChange(_ isMaster: Bool) -> Void {
    
  }
  
  func newUserModelShow(_ isMaster: Bool) -> Void {
    
  }
  
  func newUserModelHide(_ isMaster: Bool) -> Void {
    
  }
  // 点击去观战
  @IBAction func toWatchAction(_ sender: Any) {
//    CurrentUser.shareInstance.is_observer = true
//    messageDispatchManager.sendMessage(type: GameMessageType.down_seat, payLoad: ["position": CurrentUser.shareInstance.currentPosition - 1])
//    self.observerUI()
    
  }
  
  @IBAction func toPlayAction(_ sender: Any) {
//    CurrentUser.shareInstance.is_observer = false
//    messageDispatchManager.sendMessage(type: GameMessageType.up_seat, payLoad: ["position": CurrentUser.shareInstance.currentPosition - 1])
  }
  
  
  func watchGameStart() -> Void {
    // 隐藏掉抢座按钮, 不可发言状态
  }
  
  func playerStart() -> Void {
    // 隐藏掉观战 按钮
    // 开始进入游戏流程
  }
  
  // 游戏未开始
  // 玩家视角 // 非房主
  func playerBecomeWatcher() -> Void {
    // 玩家进入观战区域
    // 准备, 邀请, 观战按钮 消失
    // 抢座按钮出现
  }
  
  func watcherBecomePlayer() -> Void {
    // 观战者变为游戏者
    // 抢座按钮消失
    // 准备, 邀请, 观战按钮出现
  }

}
