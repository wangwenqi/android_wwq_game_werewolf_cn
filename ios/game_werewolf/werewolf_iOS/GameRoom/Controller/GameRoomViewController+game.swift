//
//  GameRoomViewController+Extension.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/24.
//  Copyright © 2017年 orangelab. All rights reserved.
//

enum gameType {
  case normal
  case simple
  case pre_simple
}
// 存放游戏房间的所有游戏逻辑

import Foundation

extension GameRoomViewController:EnterRoomMessage {
  
  func action_observer_join(_ rawData: [String : Any]) {
    if let jsonData = JSON.init(rawData) as? JSON {
      if let position = jsonData["position"].int {
        self.observers["\(position)"] = jsonData["user"]
        let local = NSLocalizedString("进入房间观战", comment: "")
        let message = jsonData["user"]["name"].stringValue
        self.messageViewController.addConversion(type: .system, messge:"\(message) \(local)")
      }
    }
  }
  
  func action_observer_leave(_ rawData: [String : Any]) {
    if let jsonData = JSON.init(rawData) as? JSON {
      Utils.runInMainThread { [weak self] in
        if let position = jsonData["position"].int{
          let local = NSLocalizedString("离开观战房间", comment: "")
          if let userData = self?.observers["\(position)"]  {
            if let name = userData["name"].string {
               self?.messageViewController.addConversion(type: .system, messge:"\(name) \(local)")
            }
          }
          self?.observers.removeValue(forKey:"\(position)")
        }
      }
    }
  }
  
  //OLD
  
  func action_export_pay(_ rawData: [String : Any]) {
    
    if let jsonData = JSON.init(rawData) as? JSON {
      Utils.runInMainThread { [weak self] in
        XBHHUD.showSuccess(NSLocalizedString("召集令发布成功", comment: ""))
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSTERINVITE_HIDDEN"), object: nil)
      }
    }
  }
  
  func action_send_card(_ rawData: [String : Any]) {
    if let jsonData = JSON.init(rawData) as? JSON {
      Utils.runInMainThread { [weak self] in
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SENDCARD"), object: nil)
      }
    }
  }
  
  func action_send_gift(_ rawData: [String : Any]) {
    
  }
  
  func action_can_speak(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON,
        let duration = jsonData["duration"].int {
        
        if let strongSelf = self {
          strongSelf.werewolfSpeakAtNight?.invalidate();
          
          Utils.stopPlaySound();
          
          let noticeView = Utils.getNoticeView();
          let scBounds = Screen.bounds;
          noticeView.frame = CGRect.init(x: scBounds.origin.x,
                                         y: scBounds.origin.y,
                                         width: scBounds.width,
                                         height: scBounds.height - 50);
          strongSelf.bottomView.canUseKeyboard = false;
          strongSelf.bottomView.setUnMute();
          strongSelf.showSpeakBar();
          strongSelf.werewolfSpeakAtNight = Timer.scheduledTimer(timeInterval: TimeInterval(duration / 1000),
                                                                 target: strongSelf,
                                                                 selector: #selector(strongSelf.handleSpeakAtNight),
                                                                 userInfo: nil,
                                                                 repeats: false);
        }
      }
    }
    
  }
  
  func action_exchange(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData) as! JSON
      guard CurrentUser.shareInstance.currentRole == .magician else { return }
      let aliveArray = jsonData["alives"].arrayValue
      var jsondataArray = [(Int, JSON)]()
      for json in aliveArray {
        let playerNumber = json.intValue
        let data = self?.getViewWithID(number: "\(playerNumber)")?.playerData ?? JSON.init(parseJSON: "")
        jsondataArray.append((playerNumber + 1, data))
      }
      if let lastChange = jsonData["last_exchanged"].array {
        let view = SeerCheckView.magicanView(type: .exchange, lastExchange: lastChange, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
        view.lastExchangeView()
        Utils.showNoticeView(view: view)
        view.liveTime = jsonData["duration"].intValue / 1000
      }else{
        let view = SeerCheckView.view(type: .exchange, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
        Utils.showNoticeView(view: view)
        view.liveTime = jsonData["duration"].intValue / 1000
      }
    }
  }
  
  func action_exchange_result(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData) as! JSON
      guard CurrentUser.shareInstance.currentRole == .magician else { return }
      var sec = ""
      var first = ""
      if let positions = jsonData["positions"].array {
        if let firstValue =  positions[0].int {
          first = String(firstValue+1)
        }
        if let secValue =  positions[1].int {
          sec = String(secValue+1)
        }
      }
      if first != "" && sec != "" {
        let local = NSLocalizedString("成功交换", comment: "")
        let number = NSLocalizedString("号", comment:"")
        let and = NSLocalizedString("和", comment: "")
        self?.messageViewController.addConversionWith(type: .judge, messge: "\(local)[\(first)]\(number)\(and)[\(sec)]\(number)", gameMessageType:GameMessageType.exchange_result)
      }
    }
  }
  
  func action_demon_check(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      //恶魔查验不能说话
      self?.hideSpeakBar()
      self?.hideBottombar()
      let jsonData = JSON.init(rawData) as! JSON
      let aliveArray = jsonData["alives"].arrayValue
      var jsondataArray = [(Int, JSON)]()
      
      for json in aliveArray {
        let playerNumber = json.intValue
        let data = self?.getViewWithID(number: "\(playerNumber)")?.playerData ?? JSON.init(parseJSON: "")
        jsondataArray.append((playerNumber + 1, data))
      }
      let view = SeerCheckView.view(type: .demon_check, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
      //狼王，恶魔显示
      view.demon_position = (self?.demon_postion)!
      Utils.showNoticeView(view: view)
      view.liveTime = jsonData["duration"].intValue / 1000
    }
  }
  
  func action_demon_check_result(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      
      let jsonData = JSON.init(rawData) as? JSON as! JSON
      let role = GameRole.getRole(name: jsonData["role"].stringValue)
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      var isGod = false
      var roleString = "是神"
      if let is_god = jsonData["is_god"].bool {
        if is_god {
          roleString = NSLocalizedString("是神", comment: "")
        }else{
          roleString = NSLocalizedString("不是神", comment: "")
        }
        isGod = is_god
      }
      if view?.playerData != nil {
        let noticeView = OnePersonView.view(isgod: isGod, playerData: (jsonData["position"].intValue + 1, view!.playerData))
        Utils.showNoticeView(view: noticeView)
        noticeView.liveTime = jsonData["duration"].intValue / 1000
      }
      let localposition = jsonData["position"].intValue
      let localCheck = NSLocalizedString("本轮查验的", comment: "")
      let localRole = NSLocalizedString("号身份", comment: "")
      self?.messageViewController.addConversionWith(type: .judge, messge: "\(localCheck)[\(localposition + 1)]\(localRole)\(roleString)",gameMessageType: GameMessageType.demon_check_result)
    }
    
  }
  
  func action_system_msg(_ rawData: [String : Any]) {
    if let jsonData = JSON.init(rawData) as? JSON,
      let content = jsonData["content"].string {
      if let type = jsonData["type"].string {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
          switch type {
          case "dialog":
            let view = systemDialog.view(title: content)
            view.liveTime = jsonData["duration"].intValue / 1000
            Utils.showNoticeView(view: view)
          case "toast":
            XBHHUD.showSuccess(content)
          case "text":
            self.messageViewController.addConversion(type: .judge, messge: content)
          default:
            self.messageViewController.addConversion(type: .system, messge: content)
          }
        })
      }else{
        self.messageViewController.addConversion(type: .system, messge: content)
      }
    }
  }
  
  func action_join(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      view?.newUserComeIn(jsonData: jsonData["user"])
      let local = NSLocalizedString("进入房间", comment: "")
      self?.messageViewController.addConversion(type: .system, messge: "\(jsonData["user"]["name"].stringValue) \(local)")
    }
  }
  
  func action_leave(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      let playerName = view?.playerData["name"].stringValue ?? ""
      view?.userLeave(jsonData: jsonData["remove"])
      
      if jsonData["remove"].boolValue == false && view != nil {
        self?.leavedPlayerViewArray.append(view!)
        if view?.isDeath == false {
          let local = NSLocalizedString("号玩家中途离开，被系统判定为死亡", comment: "")
          self?.messageViewController.addConversion(type: .judge, messge: "[\(jsonData["position"].intValue + 1)]\(local)")
          if let role = jsonData["role"].string {
            view?.showRole(role)
          }
        }
      } else {
        if let position = jsonData["position"].int {
          if position < 12 {
            let local = NSLocalizedString("离开房间", comment: "")
            self?.messageViewController.addConversion(type: .system, messge:"\(playerName) \(local)")
            view?.userLeave(jsonData: jsonData["remove"])
          }
        }
       
      }
    }
  }
  
  func action_update_master(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON,
        let viewArray = self?.playerViewArray,
        let updatePosition = jsonData["position"].int {
        
        for (index, view) in viewArray.enumerated() {
          
          if (index == updatePosition) {
            if !isPlaying {
              view.setMaster();
            }
            view.isMaster = true;
          } else {
            if view.isMaster && !isPlaying {
              view.unSetMaster();
            }
            view.isMaster = false;
          }
        }
        //游戏中不改变
        if isPlaying == false {
          if CurrentUser.shareInstance.currentPosition == (updatePosition + 1) {
            CurrentUser.shareInstance.isMaster = true
            self?.readyButton.setTitle(NSLocalizedString("开局", comment: ""), for: .normal)
          } else {
            if CurrentUser.shareInstance.isMaster {
              self?.readyButton.setTitle(NSLocalizedString("准备", comment: ""), for: .normal)
            }
            CurrentUser.shareInstance.isMaster = false
          }
        }
        if isPlaying == false {
          self?.setPassword()
        }
        if !isPlaying {
          self?.checkChangeMasterButtonTip();
          self?.checkReadyButtonTip();
        }
        //重新刷新UI
        self?.newPlayerModelUI()
        if CurrentUser.shareInstance.isMaster {
          self?.readyTipView?.dismiss()
        }
        let local = NSLocalizedString("房主变更为", comment: "")
        let local2 = NSLocalizedString("号玩家", comment: "")
        self?.messageViewController.addConversion(type: .system, messge: "\(local)\(jsonData["position"].intValue + 1)\(local2)")
      }
    }
  }
  
  func action_prepare(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      if jsonData["position"].intValue + 1 == self?.currentPlayerCount {
        self?.isReady = true
      }
      view?.prepare()
    }
  }
  
  func action_unprepare(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      if jsonData["position"].intValue + 1 == self?.currentPlayerCount {
        self?.isReady = false
      }
      view?.unPrepare()
    }
  }
  
  func action_chat(_ rawData: [String : Any]) {
    DispatchQueue.global().async {
      let jsonData = JSON.init(rawData)
      let position = jsonData["position"].intValue
      let message = jsonData["message"].stringValue;
      if let giftData = jsonData["gift"].dictionary,
        let from = giftData["from"]?.intValue,
        let to = giftData["to"]?.intValue,
        let typeString = giftData["type"]?.stringValue,
        let fromView = self.getViewWithID(position: from),
        let toView = self.getViewWithID(position: to),
        let _ = self.view {
        //有没有返利
        var rebateValue = -1
        if let rebate = giftData["rebate"]?.dictionary {
          //是自己送的再发送
          if from == CurrentUser.shareInstance.currentPosition - 1 {
            self.dealWithRebate(rebate: rebate,toView: toView,gift_type:typeString)
          }
          if let value = rebate["value"]?.int {
            rebateValue = value
          }
        }
        //看看是不是卡片
        let isCard = self.card.first(where: { (cardtype) -> Bool in
          if cardtype.buyName == typeString {
            return true
          }else{
            return false
          }
        })
        //如果不是卡片
        if isCard == nil {
          let gift = Gifts(fromView: fromView, toView: toView, typeString: typeString, rebate:rebateValue)
          self.giftmanager.add(gift)
          //礼物通知管理
          let giftNotice = giftNotices(from: fromView, to: toView, type:typeString, expired:Utils.getCurrentTimeStamp(), tag:0, updatedOnShow:false, giftCount: 1)
          self.giftNoticeManager.add(element: giftNotice)
        }
        DispatchQueue.global().async { [weak self] in
          self?.messageViewController.addGiftConversion(from: "\(from + 1)", to: to, giftType: typeString, messge: message);
        }
      }else{
        DispatchQueue.global().async { [weak self] in
          self?.messageViewController.addConversion(type: .player, messge: message, positionNumber: "\(position + 1)")
        }
      }
    }
  }
  
  func action_kick_out(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      let localKickOut = NSLocalizedString("被房主踢出房间", comment: "")
      self?.messageViewController.addConversion(type: .system, messge: "\(view?.playerData["name"] ?? "")\(localKickOut)")
      view?.setIconEmpty()
      if CurrentUser.shareInstance.currentPosition == jsonData["position"].intValue + 1 {
        //        XBHHUD.showError(NSLocalizedString("你被踢出了房间", comment: ""))
        XBHHUD.showCanNotHidden(title: NSLocalizedString("正在清理房间数据", comment: ""))
        messageDispatchManager.viewController = nil
        self?.dismissGameVC()
        self?.leaveCurrentRoom()
      }
    }
  }
  
  func action_start(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      if CurrentUser.shareInstance.currentLang == "ja" {
        Utils.playSound(name: "YouXiJiJiangKaiShi_JP")
      }else{
        Utils.playSound(name: "YouXiJiJiangKaiShi")
      }
      self?.syncCardDatas();
      isPlaying = true
      self?.dismissReadyButtonTip();
      self?.dissmissChageMasterButtonTip();
      self?.rightChangeMasterButton.isHidden = true;
      //游戏开始隐藏所有功能按钮
      self?.rightInviteButton.isHidden = true
      self?.righeNavButton.isHidden = true
      self?.navRightViewWidth.constant = 33
      self?.statusRightPadding.constant = 5
      //告诉RN游戏开始
      if self?.playGameTime == 0 {
        if CurrentUser.shareInstance.isTourist {
          //告诉已经玩过一次
          RNMessageSender.emitEvent(name: "EVENT_USER_START", andPayload: ["GAME_TYPE":CurrentUser.shareInstance.currentRoomType])
        }
      }
      //有召集令的隐藏召集令，游戏结束后重新显示
      for view in (self?.titleBackView.subviews)! {
        if view.isKind(of: exportNoticeView.self) {
          view.isHidden = true
        }
      }
      self?.playGameTime = (self?.playGameTime)! + 1
      self?.setTitleView(title: NSLocalizedString("第1天 天亮了", comment: ""), detail: NSLocalizedString("游戏即将开始", comment: ""), userDes: " ")
      self?.messageViewController.clearAll()
      self?.speechingView = nil
      
      for view in self!.playerViewArray {
        view.gameStarted()
      }
      self?.hideBottombar()
      if self?.freeSpeech == false {
        //游戏开始时就隐藏发言
        self?.hideSpeakBar()
      }
      let jsonData = JSON.init(rawData)
      var titleViewRoleTotal = ""
      var finalStr = NSLocalizedString("游戏开始，本轮共有：\n", comment: "")
      for (key, value) in jsonData["role_map"].dictionaryValue {
        if value.intValue == 0 {
          continue
        }
        let count = value
        let role = GameRole.getRole(name: key).gameDescribe
        finalStr.append("\(count)个\(role)\n")
        if count == 1 {
          titleViewRoleTotal = titleViewRoleTotal + "\(role) "
        }else{
          titleViewRoleTotal = titleViewRoleTotal + "\(role)X\(count) "
        }
        self?.currentPlayerCount += value.intValue
      }
      //字体设置
      if (self?.currentPlayerCount)! >= 10 {
        if Screen.width > 320 {
          self?.titleBackViewRoleTotalDes.font = UIFont.systemFont(ofSize:11)
        }else{
          self?.titleBackViewRoleTotalDes.font = UIFont.systemFont(ofSize:8)
        }
      }else{
        self?.titleBackViewRoleTotalDes.font = UIFont.systemFont(ofSize:9)
      }
      //设置titleview
      //防止被覆盖
      if titleViewRoleTotal != "" {
        self?.titleBackViewRoleTotalDes.text = titleViewRoleTotal
        self?.titleBackViewLine.isHidden = false
      }
      if jsonData["role_map"].dictionaryValue.count == 0 {
        return
      }
      
      self?.decidePlaySunsetAudio(dic:jsonData["role_map"].dictionaryValue as NSDictionary)
      Utils.delay(1.0) { [weak self] in
        self?.messageViewController.addConversion(type: .judge, messge: finalStr)
      }
      
      if let optional = jsonData["optional"].array,
        optional.count >= 2,
        let leftKey = optional[0].string,
        let rightKey = optional[1].string{
        self?.speechingView = nil
        var middleKey:String? = nil
        if optional.count > 2 {
          middleKey = optional[2].string
        }
        let view = GrabRoleView.view(json: [
          "left": leftKey,
          "right": rightKey,
          "middle":middleKey ?? ""]);
        view.liveTime = jsonData["duration"].intValue / 1000
        Utils.showNoticeView(view: view);
      }
    }
  }
  
  func action_apply_role_result(_ rawData: [String : Any]) {
    if let jsonData = JSON.init(rawData) as? JSON,
      let type = jsonData["type"].string {
      switch type {
      case "success":
        Utils.runInMainThread { [weak self] in
          for view in Utils.getCurrentNoticeViews() {
            if view.isKind(of: GrabRoleView.self) {
              let v = view as! GrabRoleView
              if let role = jsonData["card_type"].string {
                print("print 角色成功光:\(jsonData)");
                v.setButtonText(role: role,buttonText:NSLocalizedString("已抢", comment: ""))
                v.setLRFinished(role: role)
                v.setAllFinish()
                self?.messageViewController.addConversion(type: .system, messge: "成功抢到角色")
              }
            }
          }
        }
      // 角色被抢光了
      case "none":
        print("已抢光")
        //         Utils.runInMainThread {
        //          for view in Utils.getCurrentNoticeViews() {
        //            if let v = view as? GrabRoleView,
        //              let role = jsonData["card_type"].string {
        //              v.setButtonText(role: role,buttonText:NSLocalizedString("已抢光", comment: ""));
        //              v.setLRFinished(role: role)
        //            }
        //          }
      //        };
      case "not_enough_roles":
        Utils.runInMainThread { [weak self] in
          for view in Utils.getCurrentNoticeViews() {
            if let v = view as? GrabRoleView,
              let role = jsonData["card_type"].string {
              v.setButtonText(role: role,buttonText:NSLocalizedString("未抢到", comment: ""));
              v.setLRFinished(role: role)
              self?.messageViewController.addConversion(type: .system, messge: "抢角色失败,卡片已经返还")
            }
          }
        }
      case "not_enough_card":
        Utils.runInMainThread { [weak self] in
          for view in Utils.getCurrentNoticeViews() {
            if let v = view as? GrabRoleView,
              let role = jsonData["card_type"].string {
              v.setButtonText(role: role,buttonText: NSLocalizedString("抢", comment: ""))
              v.setLRFinished(role: role)
              self?.messageViewController.addConversion(type: .system, messge: "抢角色失败，卡片已经返还")
            }
          }
        };
      default:
        print(NSLocalizedString("------未知的错误返回:\(type)--------", comment: ""));
        self.messageViewController.addConversion(type: .system, messge: "抢角色失败，卡片已经返还")
      }
      
    }
  }
  
  func action_assigned_role(_ rawData: [String : Any]) {
    CurrentUser.shareInstance.isDeath = false
//    guard CurrentUser.shareInstance.is_observer == false else {
//      return
//    }
    Utils.runInMainThread { [weak self] in
      if CurrentUser.shareInstance.currentLang == "ja" {
        Utils.playSound(name: "QingDaJiaChaKanShenFen_JP")
      }else{
        Utils.playSound(name: "QingDaJiaChaKanShenFen")
      }
      self?.speechingView = nil
      
      let jsonData = JSON.init(rawData)
      CurrentUser.shareInstance.currentRole = GameRole.getRole(name: jsonData["role"].stringValue)
      let myself = self?.getViewWithID(number:"\(CurrentUser.shareInstance.currentPosition - 1)")
      myself?.role = jsonData["role"].stringValue
      self?.showRoleBar()
      let local = NSLocalizedString("当前角色:", comment: "")
      self?.currentUserRoleLabel.text = NSLocalizedString("\(local) \(CurrentUser.shareInstance.currentRole.descriptionString)", comment: "")
      
      if CurrentUser.shareInstance.currentRole == .werewolf || CurrentUser.shareInstance.currentRole == .werewolf_king || CurrentUser.shareInstance.currentRole == .demon {
        let wolfArr = jsonData["teammates"].arrayValue
        self?.wolfTeammate = wolfArr
        for i in wolfArr {
          let view = self?.getViewWithID(number: "\(i.intValue)")
          view?.setWolf()
        }
        //设置白狼王
        if let king = jsonData["king"].int {
          let view = self?.getViewWithID(number: "\(king)")
          view?.setWolfKing()
          self?.werewolf_king_postion = king
        }
        if let demon = jsonData["demon"].int {
          let view = self?.getViewWithID(number: "\(demon)")
          view?.setDemon()
          self?.demon_postion = demon
        }
      }else{
        //自己不是狼，需要把自己设置为黄色
        let view = self?.getViewWithID(number:"\(CurrentUser.shareInstance.currentPosition - 1)")
        view?.setGoodPerson()
      }
      
      let view = AssignRoleView.view(json: jsonData)
      Utils.showNoticeView(view: view)
      
      if CurrentUser.shareInstance.currentRoomType == "simple" || CurrentUser.shareInstance.currentRoomType == "pre_simple" {
        self?.setTitleView(detail: NSLocalizedString("即将进入天黑", comment: ""), userDes: "", time: jsonData["duration"].intValue / 1000)
      } else {
        self?.setTitleView(detail: NSLocalizedString("等待其他玩家确认身份", comment: ""), userDes: "", time: jsonData["duration"].intValue / 1000)
      }
    }
  }
  
  func action_sunset(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)
      if let _ = jsonData["fake"].string {
        //说明是重连消息
        
      }else{
        self?.speechingView = nil
        //播放语音
        if CurrentUser.shareInstance.currentLang == "ja" {
          Utils.playSound(name:(self?.sunsetAudio)! + "_JP")
        }else{
          Utils.playSound(name:(self?.sunsetAudio)!)
        }
        //Utils.playSound(name:(self?.sunsetAudio)!)
        let local = NSLocalizedString("天黑请闭眼", comment: "")
        let local1 = NSLocalizedString("天夜晚开始，大家出来活动吧", comment: "")
        self?.messageViewController.addConversion(type: .judge, messge: "\(local)，第\(jsonData["day_index"].intValue)\(local1)")
      }
      //新UI
      //避免重连时发言顺序
      self?.speeachTurn = Array()
      for view in (self?.playerViewArray)! {
        if view.playerData != nil {
          view.removeWaitForSpeeach()
          view.stopCutDown()
        }
      }
      self?.titleDeadImage.image = UIImage.init(named: "death_n")
      //发消息告诉礼物界面消失
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "giftviewout"), object: nil)
      self?.liveTime = Int(jsonData["duration"].doubleValue * 0.001)
      self?.checkBG(type: CurrentUser.shareInstance.currentRoomType, status: 0)
      self?.checkNV(type: CurrentUser.shareInstance.currentRoomType, status: 0)
      CurrentUser.shareInstance.isNight = true
      self?.hideSpeakBar()
      self?.hideBottombar()
      self?.setTitleView(title: "第\(jsonData["day_index"].intValue)夜 天黑了", detail: NSLocalizedString("请等待其他玩家操作", comment: ""), userDes: " ")
      self?.bottomView.speechTouchUp();
    }
  }
  
  func action_sunup(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)
      var show_speakbar = true
      if let fake = jsonData["fake"].string {
        //说明是重连消息
        if fake == "yh_no_speech" {
          show_speakbar = false
        }
      }else{
        if CurrentUser.shareInstance.currentLang == "ja" {
          Utils.playSound(name: "TianLiangLe_JP")
        }else{
          Utils.playSound(name: "TianLiangLe")
        }
      }
      self?.titleDeadImage.image = UIImage.init(named: "death_w")
      self?.titleViewTitleLabel.text = "第\(jsonData["day_index"].intValue)天 天亮了"
      self?.liveTime = Int(jsonData["duration"].doubleValue * 0.001)
      self?.titleViewDetailLabel.text = NSLocalizedString("请等待其他玩家操作", comment: "")
      CurrentUser.shareInstance.isNight = false
      self?.checkBG(type:CurrentUser.shareInstance.currentRoomType, status: 1)
      self?.checkNV(type: CurrentUser.shareInstance.currentRoomType, status: 1)
      self?.liveTime = jsonData["duration"].intValue / 1000
      if !CurrentUser.shareInstance.isDeath && (self?.freeSpeech)! && show_speakbar {
        self?.showSpeakBar()
      }
      self?.setTitleView(title: "第\(jsonData["day_index"].intValue)天 天亮了")
    }
  }
  
  func action_speak(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
    
      let jsonData = JSON.init(rawData)
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      view?.startSpeak()
    }
  }
  
  func action_unspeak(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      view?.stopSpeak()
    }
  }
  
  func action_speech(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      self?.bottomView.setUnMute()
      //self?.useDelayCardButton.isHidden = true;
      self?.showBottomForSpeeach()
      let jsonData = JSON.init(rawData)
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      view?.newPlayerModel = (self?.isNewPlayerModel)!
      self?.speechingView = view
      view?.speeching()
      //发言的不是自己就先取消发言图标
      for view in (self?.playerViewArray)! {
        view.stopCutDown()
      }
      //设置说话动画
      view?.startCutDown(jsonData["duration"].intValue / 1000)
      //新UI，增加等待说话
      var needShowPoint = true
      //正在说话的人
      //改变title
      self?.setTitleView(detail:NSLocalizedString("发言阶段", comment: ""))
      self?.bottomView.currrentSpeking = (self?.speechingView?.tag)!
      if jsonData["dead"].boolValue == false {
        let local = NSLocalizedString("号玩家开始发言", comment: "")
        self?.setTitleView(detail: "\(jsonData["position"].intValue + 1)\(local)")
      } else {
        let local = NSLocalizedString("号玩家请留遗言", comment: "")
        self?.setTitleView(detail: "\(jsonData["position"].intValue + 1)\(local)")
        needShowPoint = false
      }
      if let leftToSpeeach = jsonData["lefts"].arrayObject {
        if let arr = leftToSpeeach as? [Int] {
          self?.speeachTurn = arr
        }
      }
      //新UI，增加等待说话
      if needShowPoint {
        for view in (self?.playerViewArray)! {
          if (self?.speeachTurn.contains(view.tag-1))! {
            view.waitForSpeeach()
          }
        }
      }
      //取消正在说话
      self?.speechingView?.removeWaitForSpeeach()
      self?.liveTime = jsonData["duration"].intValue / 1000
      if CurrentUser.shareInstance.currentPosition == self?.speechingView?.tag {
        //把语音按钮打开
        self?.bottomView.setUnMute()
        if self?.freeSpeech == false {
          self?.bottomView.itIsMyTurn = true
          //如果不能自由发言
          self?.readyButton.isHidden = true
          //把结束发言居中
          self?.inviteButton.snp.removeConstraints()
          self?.inviteButton.layoutIfNeeded()
          self?.controlBackView.layoutIfNeeded()
          self?.inviteButton.snp.makeConstraints({ (make) in
            make.centerX.equalTo((self?.view.snp.centerX)!)
          })
        }else{
          self?.readyButton.isHidden = false
        }
        self?.readyButton.setTitle(NSLocalizedString("全体禁言", comment: ""), for: .normal)
        self?.inviteButton.setTitle(NSLocalizedString("结束发言", comment: ""), for: .normal)
        self?.showBottomBar()
        self?.showSpeakBar()
        self?.useDelayCardButton.isHidden = false;
        Utils.playSound(name: "startSpeech")
        self?.bottomView.startCountDown()
        //竞选的时候如果轮到自己说话，不能显示
        self?.hideApplyButton()
        
      } else {
        // 停止倒计时
        self?.bottomView.dealWithLongTouch()
        self?.bottomView.stopCountDown()
        // 如果自己没死
        if !CurrentUser.shareInstance.isDeath {
          self?.showSpeakBar()
        } else {
          self?.hideSpeakBar()
        }
        if self?.freeSpeech == false {
          //居中显示
          self?.inviteButton.layoutIfNeeded()
          self?.controlBackView.layoutIfNeeded()
          UIView.animate(withDuration: 0.3, animations: {
            self?.inviteButton.snp.removeConstraints()
          })
        }
        self?.readyButton.setTitle(NSLocalizedString("准备", comment: ""), for: .normal)
        self?.inviteButton.setTitle(NSLocalizedString("邀请", comment: ""), for: .normal)
        // 不是自己发言的时候能不能轮麦
        //        self?.hideBottombar()
        //不是自己说话
        self?.hideBottombarForSpeeach()
        if self?.freeSpeech == false {
          self?.bottomView.itIsMyTurn = false
          self?.bottomView.dealWithLongTouch()
          self?.bottomView.setCanNotFreeTalk()
        }else{
          //不是自己发言，就处理
          self?.bottomView.dealWithLongTouch()
        }
        if self!.isApplying && (self?.needShowGiveUp)! {
          //竞选的时候如果轮到比人说话，需要显示
          self?.showApplybutton()
        }
        self?.observerUI()
      }
    }
  }
  
  func action_wake_to_kill(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      
      let jsonData = JSON.init(rawData) as? JSON as! JSON
      
      let aliveArray = jsonData["alives"].arrayValue
      
      var jsondataArray = [(Int, JSON)]()
      
      for json in aliveArray {
        let playerNumber = json.intValue
        let data = self?.getViewWithID(number: "\(playerNumber)")?.playerData ?? JSON.init(parseJSON: "")
        jsondataArray.append((playerNumber + 1, data))
      }
      
      let view = WolfKillView.view(playerDataArray: jsondataArray,teammate: self!.wolfTeammate)
      view.werewolf_king_position = (self?.werewolf_king_postion)!
      view.demon_position = (self?.demon_postion)!
      Utils.showNoticeView(view: view)
      view.liveTime = jsonData["duration"].intValue / 1000
    }
  }
  
  func action_kill(_ rawData: [String : Any]) {
    let jsonData = JSON.init(rawData)
    NotificationCenter.default.post(name: Notification.Name.init("kill"), object:jsonData)
  }
  
  func action_kill_result(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData) as? JSON as! JSON
      let positon = jsonData["position"].intValue
      let local = NSLocalizedString("本轮你们选择杀掉", comment: "")
      let num = NSLocalizedString("号玩家", comment: "")
      self?.messageViewController.addConversionWith(type: .judge, messge: "\(local)[\(positon + 1)]\(num)",gameMessageType: GameMessageType.kill_result)
    }
  }
  
  func action_check(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      
      let jsonData = JSON.init(rawData) as? JSON as! JSON
      let aliveArray = jsonData["alives"].arrayValue
      
      var jsondataArray = [(Int, JSON)]()
      
      for json in aliveArray {
        let playerNumber = json.intValue
        let data = self?.getViewWithID(number: "\(playerNumber)")?.playerData ?? JSON.init(parseJSON: "")
        jsondataArray.append((playerNumber + 1, data))
      }
      
      let view = SeerCheckView.view(type: .check, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
      Utils.showNoticeView(view: view)
      view.liveTime = jsonData["duration"].intValue / 1000
    }
  }
  
  func action_check_result(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      
      let jsonData = JSON.init(rawData) as? JSON as! JSON
      
      let role = GameRole.getRole(name: jsonData["role"].stringValue)
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      
      if view?.playerData != nil {
        let noticeView = OnePersonView.view(role: role, playerData: (jsonData["position"].intValue + 1, view!.playerData))
        Utils.showNoticeView(view: noticeView)
        noticeView.liveTime = jsonData["duration"].intValue / 1000
      }
      
      var roleString = ""
      
      if role == .werewolf || role == .werewolf_king || role == .demon {
        roleString = NSLocalizedString("狼人", comment: "")
      } else {
        roleString = NSLocalizedString("好人", comment: "")
      }
      let localposition = jsonData["position"].intValue
      let localCheck = NSLocalizedString("本轮查验的", comment: "")
      let localRole = NSLocalizedString("号身份为", comment: "")
      self?.messageViewController.addConversionWith(type: .judge, messge: "\(localCheck)[\(localposition + 1)]\(localRole)\(roleString)",gameMessageType: GameMessageType.check_result)
    }
  }
  
  func action_save(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      
      let jsonData = JSON.init(rawData) as? JSON as! JSON
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      if view?.playerData != nil {
        var savebal = true
        if let save = jsonData["can_save"].bool {
          savebal = save
        }
        let noticeView = WitchSaveView.view(playerData: (jsonData["position"].intValue + 1, view!.playerData),cansave:savebal)
        Utils.showNoticeView(view: noticeView)
        noticeView.liveTime = jsonData["duration"].intValue / 1000
      }
    }
  }
  
  func action_poison(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      
      let jsonData = JSON.init(rawData)  as! JSON
      let aliveArray = jsonData["alives"].arrayValue
      
      var jsondataArray = [(Int, JSON)]()
      
      for json in aliveArray {
        let playerNumber = json.intValue
        let data = self?.getViewWithID(number: "\(playerNumber)")?.playerData ?? JSON.init(parseJSON: "")
        jsondataArray.append((playerNumber + 1, data))
      }
      
      let view = SeerCheckView.view(type: .poison, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
      Utils.showNoticeView(view: view)
      view.liveTime = jsonData["duration"].intValue / 1000
    }
  }
  
  func action_death_info(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      
      let arr = jsonData["death_info"].arrayValue
      var titleStr = NSLocalizedString("天亮了，昨晚", comment: "")
      
      for dic in arr {
        
        titleStr.append("[\(dic["killed"].intValue + 1)]")
        let view = self?.getViewWithID(number: dic["killed"].stringValue)
        view?.setDeath()
        //明牌
        if let role = dic["role"].string {
          view?.showRole(role)
        }
        
        // 判断自己是否已经死亡
        if CurrentUser.shareInstance.currentPosition == dic["killed"].intValue + 1 {
          self?.hideSpeakBar()
          self?.setSelfDeath()
        }
      }
      titleStr.append(NSLocalizedString("号玩家死亡", comment: ""))
      
      for dic in arr {
        if GameRole.getRole(name: dic["killed_role"].stringValue) == .hunter {
          let local = NSLocalizedString("号玩家触发了猎人技能", comment: "")
          self?.messageViewController.addConversion(type: .judge, messge: "[\(dic["killed"].intValue + 1)]\(local)")
        }
      }
      
      if jsonData["death_info"].arrayValue.count == 0 {
        self?.messageViewController.addConversionWith(type: .judge, messge: NSLocalizedString("天亮了，昨晚是平安夜", comment: ""),gameMessageType: GameMessageType.death_info)
        self?.setTitleView(detail: NSLocalizedString("昨晚是平安夜", comment: ""))
      } else {
        self?.messageViewController.addConversion(type: .judge, messge: titleStr)
        self?.setTitleView(detail: titleStr)
      }
      
    }
  }
  
  func action_vote(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      //避免重连时发言顺序
      self?.speeachTurn = Array()
      for view in (self?.playerViewArray)! {
        if view.playerData != nil {
          view.removeWaitForSpeeach()
          view.stopCutDown()
        }
      }
      self?.speechingView = nil
      //投票时隐藏
      if self?.freeSpeech == false {
        self?.hideBottombar()
        self?.hideSpeakBar()
      }
      // 隐藏竞选者图标
      for view in self!.playerViewArray {
        view.setUnCandidate()
      }
      let jsonData = JSON.init(rawData)  as! JSON
      //投票时隐藏放弃竞选按钮
      self?.hideApplyButton()
      self?.setTitleView(detail: NSLocalizedString("等待其他玩家投票", comment: ""), userDes: " ", time: jsonData["duration"].intValue / 1000)
      
      let aliveArray = jsonData["alives"].arrayValue
      var jsondataArray = [(Int, JSON)]()
      for json in aliveArray {
        let playerNumber = json.intValue
        let data = self?.getViewWithID(number: "\(playerNumber)")?.playerData ?? JSON.init(parseJSON: "")
        jsondataArray.append((playerNumber + 1, data))
      }
      guard jsondataArray.count != 0 else {
        if CurrentUser.shareInstance.is_observer || CurrentUser.shareInstance.isDeath {
          if CurrentUser.shareInstance.currentLang == "ja" {
            Utils.playSound(name: "KaiShiTouPiao_JP")
          }else{
            Utils.playSound(name: "KaiShiTouPiao")
          }
        }
        return
      }
      if CurrentUser.shareInstance.currentLang == "ja" {
        Utils.playSound(name: "KaiShiTouPiao_JP")
      }else{
        Utils.playSound(name: "KaiShiTouPiao")
      }
      let view = SeerCheckView.view(type: .vote, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
      //狼王，恶魔显示
      view.demon_position = (self?.demon_postion)!
      view.werewolf_king_position = (self?.werewolf_king_postion)!
      Utils.showNoticeView(view: view)
      
      view.liveTime = jsonData["duration"].intValue / 1000
      
      self?.hideBottombar()
    }
  }
  
  func action_vote_result(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      
      let jsonData = JSON.init(rawData)  as! JSON
      var needPk = false
      self?.speechingView = nil
      let localvote = NSLocalizedString("号投给了", comment: "")
      let localvoted = NSLocalizedString("号玩家\n", comment: "")
      let localbePloce = NSLocalizedString("号玩家当选为警长", comment: "")
      let localnum = NSLocalizedString("号玩家", comment: "")
      var voteInfo:[String:Any] = [:]
      // 法官消息
      var messageStr = ""
      for (key, value) in jsonData["vote_info"].dictionaryValue {
        if key != "-1" {
          let voted = "\(Int.init(key)! + 1)"
          for index in value.arrayValue {
            messageStr += "[\(index.intValue + 1)]"
          }
          messageStr += NSLocalizedString("\(localvote)[\(voted)]\(localvoted)", comment: "")
        } else {
          for index in value.arrayValue {
            messageStr += "[\(index.intValue + 1)]"
          }
          messageStr += NSLocalizedString("号玩家弃票\n", comment: "")
        }
        voteInfo[key] = value
      }
      
      if jsonData["type"] == "apply" { // 竞选警长的结果
        //是否需要PK
        voteInfo["type"] = "apply"
        //显示投票信息
        self?.messageViewController.addVoteConversion(message: voteInfo)
        if let need_Pk = jsonData["need_pk"].bool {
          if need_Pk {
            messageStr.append(NSLocalizedString("得票相同，开始PK", comment: ""))
            self?.messageViewController.addConversionWith(type: .judge, messge: messageStr,gameMessageType:GameMessageType.vote_result)
            needPk = need_Pk
          }
        }
        if needPk {
          //判断自己还有没有机会竞选
          if let pk = jsonData["positions"].arrayObject {
            if  pk.contains(where: { $0 as! Int + 1 == CurrentUser.shareInstance.currentPosition }) {
              //自己需要PK
            }else{
              //自己不需要PK
              self?.isApplying = false
              self?.hideApplyButton()
            }
            //把不PK设置为不参加
            for playerView in self!.playerViewArray {
              if pk.contains(where: { $0 as! Int + 1 == playerView.tag }) == false {
                playerView.setUnCandidate()
              }
            }
          }
          return
        }else{
          self?.isApplying = false
          self?.hideApplyButton()
        }
        //把所有人设置为不在竞选
        for playerView in self!.playerViewArray {
          playerView.setUnCandidate()
        }
        if jsonData["positions"].arrayValue.count == 0 {
          // 没有警长
          self?.setTitleView(detail: NSLocalizedString("没有玩家竞选，本轮没有警长", comment: ""))
          messageStr.append(NSLocalizedString("没有玩家竞选，本轮没有警长", comment: ""))
          self?.messageViewController.addConversionWith(type: .judge, messge: messageStr,gameMessageType: GameMessageType.vote_result)
        } else {
          self?.setTitleView(detail: "\(jsonData["positions"][0].intValue + 1)\(localbePloce)")
          messageStr.append("\(jsonData["positions"][0].intValue + 1)\(localbePloce)")
          self?.messageViewController.addConversion(type: .judge, messge: messageStr)
          let view = self?.getViewWithID(number: jsonData["positions"][0].stringValue)
          view?.setSheriff()
          self?.currentSheriffPosition = jsonData["positions"][0].intValue + 1
          
          let noticeView = OnePersonView.viewSheriff(type: .sheriff, data: (jsonData["positions"][0].intValue + 1, view?.playerData ?? JSON.init(parseJSON: "")))
          Utils.showNoticeView(view: noticeView)
          noticeView.liveTime = jsonData["duration"].intValue / 1000
        }
        
        
      } else if jsonData["type"] == "death" {
        
        voteInfo["type"] = "death"
        self?.messageViewController.addVoteConversion(message: voteInfo)
        
        var titleStr = ""
        for value in jsonData["death_info"].arrayValue {
          
          let view = self?.getViewWithID(number: value["killed"].stringValue)
          view?.setDeath()
          messageStr.append("\(value["killed"].intValue + 1)\(localnum)\(DeathWay.getWay(name: value["reason"].stringValue).descriptionString)\n")
          titleStr.append("\(value["killed"].intValue + 1)\(localnum)\(DeathWay.getWay(name: value["reason"].stringValue).descriptionString),")
          // 判断自己是否已经死亡
          if CurrentUser.shareInstance.currentPosition == value["killed"].intValue + 1 {
            self?.hideSpeakBar()
            self?.hideBottombar()
            self?.setSelfDeath()
          }
          //死后明牌
          if let role = value["role"].string {
            view?.showRole(role)
          }
        }
        self?.setTitleView(detail: titleStr)
        self?.messageViewController.addConversion(type: .judge, messge: messageStr)
        
      } else if jsonData["type"] == "equal" {
        voteInfo["type"] = "death"
        self?.messageViewController.addVoteConversion(message: voteInfo)
        var finalStr = ""
        for index in jsonData["positions"].arrayValue {
          finalStr += "[\(index.intValue + 1)]"
        }
        finalStr += NSLocalizedString("号平票\n", comment: "")
        messageStr.append(finalStr)
        self?.setTitleView(detail: finalStr)
        self?.messageViewController.addConversion(type: .judge, messge: messageStr)
      } else if jsonData["type"] == "no_death" {
        self?.setTitleView(detail: NSLocalizedString("所有人弃票", comment: ""))
        messageStr.append(NSLocalizedString("所有人弃票", comment: ""))
        self?.messageViewController.addConversion(type: .judge, messge: messageStr)
      }
      
      self?.liveTime = jsonData["duration"].intValue / 1000
    }
  }
  
  func action_take_away(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      
      self?.setTitleView(detail: NSLocalizedString("猎人正在使用猎人技能", comment: ""), time: jsonData["duration"].intValue / 1000)
      
      if CurrentUser.shareInstance.currentRole == .hunter {
        let aliveArray = jsonData["alives"].arrayValue
        
        var jsondataArray = [(Int, JSON)]()
        
        for json in aliveArray {
          
          let playerNumber = json.intValue
          let data = self?.getViewWithID(number: "\(playerNumber)")?.playerData ?? JSON.init(parseJSON: "")
          jsondataArray.append((playerNumber + 1, data))
        }
        
        let view = SeerCheckView.view(type: .take_away, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
        Utils.showNoticeView(view: view)
        view.liveTime = jsonData["duration"].intValue / 1000
      }
    }
    
  }
  
  func action_take_away_result(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      var local_real_dead = ""
      let localKill = NSLocalizedString("你触发了猎人技能，射杀了", comment: "")
      let localWord = NSLocalizedString("号玩家请留遗言。", comment: "")
      let num = NSLocalizedString("号", comment: "")
      for dic in jsonData["death_info"].arrayValue {
        let view = self?.getViewWithID(number: dic["killed"].stringValue)
        //是不是死了,恶魔晚上不会死
        if let dead = dic["dead"].bool {
          if dead {
            view?.setDeath()
            if let role = dic["role"].string {
              view?.showRole(role)
            }
          }else{
            //不是真的死了
            if let real_dead_int = dic["real_dead"].int {
              //把真的死了就设置为死亡
              let real_dead = self?.getViewWithID(number: "\(real_dead_int)")
              real_dead?.setDeath()
              if let role = dic["role"].string {
                real_dead?.showRole(role)
              }
              let real_dead_string = NSLocalizedString("号玩家死亡", comment: "")
              local_real_dead = ",\(real_dead_int + 1)\(real_dead_string)"
            }
          }
        }
        if DeathWay.getWay(name: dic["reason"].stringValue) == .token_away {
          if CurrentUser.shareInstance.currentRole == .hunter  {
            self?.setTitleView(detail: "\(localKill)[\(dic["killed"].intValue + 1)]\(num) [\(dic["killed"].intValue + 1)]\(localWord)", userDes:"")
            self?.messageViewController.addConversionWith(type: .judge, messge: "\(localKill)[\(dic["killed"].intValue + 1)]\(num)\(local_real_dead)",gameMessageType: GameMessageType.take_away_result)
          } else {
            let locolKill1 = NSLocalizedString("号触发了猎人技能，射杀了", comment: "")
            self?.setTitleView(detail: "[\(dic["killer"].intValue + 1)]\(locolKill1)[\(dic["killed"].intValue + 1)]\(num) [\(dic["killed"].intValue + 1)]\(localWord)", userDes:"")
            self?.messageViewController.addConversionWith(type: .judge, messge: "[\(dic["killer"].intValue + 1)]\(locolKill1)[\(dic["killed"].intValue + 1)]\(num)\(local_real_dead)",gameMessageType: GameMessageType.take_away_result)
          }
        } else {
          if DeathWay.getWay(name: dic["reason"].stringValue) == .linked {
            let localdie = NSLocalizedString("号玩家殉情", comment: "")
            self?.messageViewController.addConversionWith(type: .judge, messge: "[\(dic["killed"].intValue + 1)]\(localdie)",gameMessageType: GameMessageType.take_away_result)
          }
        }
        
        // 判断带走的是不是自己，是不是真的死了
        if CurrentUser.shareInstance.currentPosition == dic["killed"].intValue + 1 {
          if let dead = dic["dead"].bool {
            if dead {
              self?.hideBottombar()
              self?.hideSpeakBar()
              self?.setSelfDeath()
            }
          }
        }
        //谁真的死了
        if let real = dic["real_dead"].int {
          if CurrentUser.shareInstance.currentPosition == real + 1 {
            self?.hideBottombar()
            self?.hideSpeakBar()
            self?.setSelfDeath()
          }
        }
      }
    }
  }
  
  func action_game_over(_ rawData: [String : Any]) {
    //发送统计
    MobClick.event("Game_gameover")
    Utils.runInMainThread { [weak self] in
      guard let _ = self else { return }
      isPlaying = false
      // 隐藏角色栏
      self?.hideRoleBar()
      // 设置房主
      for view in self!.playerViewArray {
        if view.isMaster {
          view.setMaster()
        }
        //把狼人设置为普通颜色
        view.setNormalBG()
      }
      //把正在说话的处理掉
      self?.speeachTurn = Array()
      for view in (self?.playerViewArray)! {
        if view.playerData != nil {
          view.removeWaitForSpeeach()
          view.stopCutDown()
        }
      }
      //如果是不能自由发言，就恢复一下
      if self?.freeSpeech == false {
        //居中显示
        self?.inviteButton.layoutIfNeeded()
        self?.controlBackView.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, animations: {
          self?.inviteButton.snp.removeConstraints()
        })
      }
      var messageStr = NSLocalizedString("游戏结束：", comment: "")
      // 游戏结束控件
      let jsonData = JSON.init(rawData)  as! JSON
      let winType = jsonData["win_type"].stringValue
      if winType == "people" {
        messageStr.append(NSLocalizedString("好人胜利\n", comment: ""))
        if CurrentUser.shareInstance.currentLang == "ja" {
          Utils.playSound(name: "HaoRenShengLi_JP")
        }else{
          Utils.playSound(name: "HaoRenShengLi")
        }
        //        Utils.playSound(name: "HaoRenShengLi")
      } else if winType == "werewolf" {
        if CurrentUser.shareInstance.currentLang == "ja" {
          Utils.playSound(name: "LangRenShengLi_JP")
        }else{
          Utils.playSound(name: "LangRenShengLi")
        }
        messageStr.append(NSLocalizedString("狼人胜利\n", comment: ""))
        //        Utils.playSound(name: "LangRenShengLi")
      } else {
        if CurrentUser.shareInstance.currentLang == "ja" {
          Utils.playSound(name: "QingLvShengLi_JP")
        }else{
          Utils.playSound(name: "QingLvShengLi")
        }
        messageStr.append(NSLocalizedString("情侣阵营胜利\n", comment: ""))
        //        Utils.playSound(name: "QingLvShengLi")
      }
      var selfIsWin = false
      for index in jsonData["winners"].arrayValue {
        if CurrentUser.shareInstance.currentPosition == index.intValue + 1 {
          selfIsWin = true
          break
        }
      }
      var peopleDataArr = [(String, JSON)]()
      var wolfDataArr = [(String, JSON)]()
      var thirdDataArr = [(String, JSON)]()
      if jsonData["roles"]["people"].arrayValue.count != 0 {
        messageStr.append(NSLocalizedString("\n好人阵营\n", comment: ""))
      }
      
      for jsonDic in jsonData["roles"]["people"].arrayValue {
        messageStr.append("[\(jsonDic["position"].intValue + 1)]号: \(GameRole.getRole(name: jsonDic["role"].stringValue).descriptionString)(\(DeathWay.getWay(name: jsonDic["status"].stringValue).descriptionString))\n")
        let view = self?.getViewWithID(number: jsonDic["position"].stringValue)
        if view?.playerData != nil {
          peopleDataArr.append((jsonDic["role"].stringValue,view!.playerData))
        }
      }
      
      if jsonData["roles"]["werewolf"].arrayValue.count != 0 {
        messageStr.append(NSLocalizedString("\n狼人阵营\n", comment: ""))
      }
      for jsonDic in jsonData["roles"]["werewolf"].arrayValue {
        messageStr.append("[\(jsonDic["position"].intValue + 1)]号: \(GameRole.getRole(name: jsonDic["role"].stringValue).descriptionString)(\(DeathWay.getWay(name: jsonDic["status"].stringValue).descriptionString))\n")
        let view = self?.getViewWithID(number: jsonDic["position"].stringValue)
        if view?.playerData != nil {
          wolfDataArr.append((jsonDic["role"].stringValue,view!.playerData))
        }
      }
      
      if jsonData["roles"]["third_party"].arrayValue.count != 0 {
        messageStr.append(NSLocalizedString("\n情侣阵营\n", comment: ""))
      }
      for jsonDic in jsonData["roles"]["third_party"].arrayValue {
        messageStr.append("[\(jsonDic["position"].intValue + 1)]号: \(GameRole.getRole(name: jsonDic["role"].stringValue).descriptionString)(\(DeathWay.getWay(name: jsonDic["status"].stringValue).descriptionString))\n")
        let view = self?.getViewWithID(number: jsonDic["position"].stringValue)
        if view?.playerData != nil {
          thirdDataArr.append((jsonDic["role"].stringValue,view!.playerData))
        }
      }
      self?.messageViewController.addConversion(type: .judge, messge: messageStr)
      let currentp = self?.getViewWithID(position: CurrentUser.shareInstance.currentPosition - 1)
      var currentAva = currentp?.iconImage.image
      if currentAva == nil {
        currentAva = UIImage(named: "appIcon")
      }
      var view: GameOver?
      AdShowClass.isShowAd(.gameOverAd, { (isShow) in
        adState.isAdGWGameOverShow = isShow
      })
      var show = false
      if adState.isAdFBGameOverShow, adState.isAdGWGameOverShow {
        show = true
      }
      //观战模式不显示
      if CurrentUser.shareInstance.is_observer == false {
        if let exp = jsonData["experience"].dictionary {
          view = GameOver.view(people: peopleDataArr, werewolf: wolfDataArr, thirdPard: thirdDataArr, selfIsWin: selfIsWin, winType: winType,userIcon: currentAva!,exp:exp)
        }else{
          view = GameOver.view(people: peopleDataArr, werewolf: wolfDataArr, thirdPard: thirdDataArr, selfIsWin: selfIsWin, winType: winType,userIcon: currentAva!,exp:nil)
        }
        DispatchQueue.main.async {
          self?.bottomView.stopCountDown()
        }
        self?.view.addSubview(view!)
        view?.snp.makeConstraints({ (make) in
          make.edges.equalToSuperview()
        })
      }
      self?.clearGameRoomData()
      self?.setPassword()
      if !CurrentUser.shareInstance.isMaster {
        self?.rightChangeMasterButton.isHidden = false;
      }
      //CHONGZHI_OBSERVER
      self?.observerUI()
    }
  }
  
  func action_lock(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      for view in self!.playerViewArray {
        if view.currentPositionState != .havePlayer {
          view.setIconEmpty()
        }
      }
  
      let jsonData = JSON.init(rawData)  as! JSON
      for positon in jsonData["locked_positions"].arrayValue {
        let view = self!.getViewWithID(number: positon.stringValue)
        view?.setIconLocked()
      }
    }
    
  }
  
  func action_mute_all(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      
      if CurrentUser.shareInstance.isDeath {
        return
      }
      for view in self!.playerViewArray {
        view.stopSpeak()
      }
      let jsonData = JSON.init(rawData)  as! JSON
      guard CurrentUser.shareInstance.currentPosition != jsonData["position"].intValue + 1 else { return }
      XBHHUD.showError(NSLocalizedString("你已被禁言", comment: ""))
      self?.bottomView.dealWithLongTouch()
      self?.bottomView.setMute()
    }
  }
  
  func action_unmute_all(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      
      if CurrentUser.shareInstance.isDeath {
        return
      }
      let jsonData = JSON.init(rawData)  as! JSON
      guard CurrentUser.shareInstance.currentPosition != jsonData["position"].intValue + 1 else { return }
      let localSpeak = NSLocalizedString("号玩家解除全体禁言", comment: "")
      XBHHUD.showSuccess("\(jsonData["position"].intValue + 1)\(localSpeak)")
      self?.bottomView.setUnMute()
    }

  }
  
  func action_link(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      if CurrentUser.shareInstance.currentLang == "ja" {
        Utils.playSound(name: "AiShenQingLianJieQingLv_JP")
      }else{
        Utils.playSound(name: "AiShenQingLianJieQingLv")
      }
      let jsonData = JSON.init(rawData)  as! JSON
      
      self?.setTitleView(detail: NSLocalizedString("爱神正在选择情侣", comment: ""), userDes: " ", time: jsonData["duration"].intValue / 1000)
      
      guard CurrentUser.shareInstance.currentRole == .cupid else { return }
      
      let aliveArray = jsonData["alives"].arrayValue
      var jsondataArray = [(Int, JSON)]()
      
      for json in aliveArray {
        let playerNumber = json.intValue
        let data = self?.getViewWithID(number: "\(playerNumber)")?.playerData ?? JSON.init(parseJSON: "")
        jsondataArray.append((playerNumber + 1, data))
      }
      
      let view = SeerCheckView.view(type: .link, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
      Utils.showNoticeView(view: view)
      view.liveTime = jsonData["duration"].intValue / 1000
    }
  }
  
  func action_link_result(_ rawData: [String : Any]) {
   
    Utils.runInMainThread { [weak self] in
      
      let jsonData = JSON.init(rawData)  as! JSON
      self?.setTitleView(detail: NSLocalizedString("情侣彼此确认身份，即将进入天黑", comment: ""), userDes: " ", time: jsonData["duration"].intValue / 1000)
      
      var viewArray = [PlayerView]()
      
      for (key, value) in jsonData["lovers"].dictionaryValue {
        self?.lovers.append(Int(key)! + 1)
        let view = self?.getViewWithID(number: key) ?? PlayerView()
        view.setLover()
        viewArray.append(view)
        if value.boolValue && CurrentUser.shareInstance.currentRole != .cupid {
          view.setWolf()
        }
      }
      
      guard viewArray.count >= 2 && CurrentUser.shareInstance.currentRole != .cupid else { return }
      
      if CurrentUser.shareInstance.currentPosition == viewArray[0].tag || CurrentUser.shareInstance.currentPosition == viewArray[1].tag {
        if CurrentUser.shareInstance.is_observer == false {
          let view = linkResultView.view(type: .notice, player1: (viewArray[0].tag, viewArray[0].playerData), player2: (viewArray[1].tag, viewArray[1].playerData))
          Utils.showNoticeView(view: view)
          view.liveTime = jsonData["duration"].intValue / 1000
        }
      }
      let localChose = NSLocalizedString("本轮你选择", comment: "")
      let localZ = NSLocalizedString("号成为情侣", comment: "")
      let localL = NSLocalizedString("号和", comment: "")
      let localNotice = NSLocalizedString("本轮你和", comment: "")
      let localNotice1 = NSLocalizedString("号被选为情侣", comment: "")
      if CurrentUser.shareInstance.currentRole == .cupid {
        self?.messageViewController.addConversionWith(type: .judge, messge:"\(localChose)[\(viewArray[0].tag)]\(localL)[\(viewArray[1].tag)]\(localZ)",gameMessageType: GameMessageType.link_result)
      }
      
      if CurrentUser.shareInstance.currentPosition == viewArray[0].tag {
        self?.messageViewController.addConversionWith(type: .judge, messge:"\(localNotice)[\(viewArray[1].tag)]\(localNotice1)",gameMessageType: GameMessageType.link_result)
      }
      if CurrentUser.shareInstance.currentPosition == viewArray[1].tag {
        self?.messageViewController.addConversionWith(type: .judge, messge: "\(localNotice)([\(viewArray[0].tag)]\(localNotice1)",gameMessageType: GameMessageType.link_result)
      }
    }
    
  }
  
  func action_apply(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      if self?.freeSpeech == false {
        self?.hideSpeakBar()
        self?.hideBottombar()
      }
      Utils.playSound(name: "XiaMianJinRuJingZhangJingXuan")
      let jsonData = JSON.init(rawData)  as! JSON
      if CurrentUser.shareInstance.is_observer == false {
        let view = SheriffView.view()
        Utils.showNoticeView(view: view)
        view.liveTime = jsonData["duration"].intValue / 1000
      }
      self?.setTitleView(detail: NSLocalizedString("其他玩家正在考虑是否参选", comment: ""),time: jsonData["duration"].intValue / 1000)
      self?.messageViewController.addConversion(type: .judge, messge: NSLocalizedString("下面进入警长竞选环节", comment: ""))
    }
  }
  
  func action_apply_result(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      self?.isApplying = true
      for view in self!.playerViewArray {
        view.setUnCandidate()
      }
      let jsonData = JSON.init(rawData)  as! JSON
      let aliveArray = jsonData["positions"].arrayValue
      
      for position in aliveArray {
        let view = self?.getViewWithID(number: position.stringValue)
        view?.setCandidate()
        //参与竞选不能退出
        if CurrentUser.shareInstance.currentPosition == position.intValue + 1 {
          //设置需要显示竞选按钮
          self?.needShowGiveUp = true
        }
      }
    }
  }
  
  func action_give_up_apply(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      
      let jsonData = JSON.init(rawData)  as! JSON
      let view = self?.getViewWithID(number: jsonData["position"].stringValue)
      view?.setUnCandidate()
      let position = jsonData["position"].intValue
      let local = NSLocalizedString("号玩家放弃竞选", comment: "")
      self?.messageViewController.addConversion(type: .judge, messge: "[\(position + 1)]\(local)")
    }
  }
  
  func action_sheriff_result(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      
      self?.isApplying = false
      self?.needShowGiveUp = false
      //如果正在说话，就要隐藏
      if self?.freeSpeech == false {
        self?.speechingView = nil
        self?.hideSpeakBar()
        self?.hideBottombar()
      }
      for playerView in self!.playerViewArray {
        playerView.setUnCandidate()
      }
      self?.hideApplyButton()
      let jsonData = JSON.init(rawData)  as! JSON
      
      if jsonData["position"].int != nil {
        // 有警长
        let position = jsonData["position"].int
        let view = self?.getViewWithID(number: jsonData["position"].stringValue)
        view?.setSheriff()
        self?.currentSheriffPosition = jsonData["position"].intValue + 1
        if CurrentUser.shareInstance.is_observer == false {
          let noticeView = OnePersonView.viewSheriff(type: .sheriff, data: (jsonData["position"].intValue + 1, view?.playerData ?? JSON.init(parseJSON: "")))
          Utils.showNoticeView(view: noticeView)
          noticeView.liveTime = jsonData["duration"].intValue / 1000
        }
        let localP = NSLocalizedString("号玩家当选为警长", comment: "")
        self?.setTitleView(detail: "\(position! + 1)\(localP)")
        self?.messageViewController.addConversionWith(type: .judge, messge: "\(position! + 1)\(localP)",gameMessageType: GameMessageType.sheriff_result)
      } else {
        // 没有警长
        self?.setTitleView(detail: NSLocalizedString("没有玩家竞选，本轮没有警长", comment: ""))
        self?.messageViewController.addConversionWith(type: .judge, messge: NSLocalizedString("没有玩家竞选，本轮没有警长", comment: ""),gameMessageType: GameMessageType.sheriff_result)
      }
      self?.liveTime = jsonData["duration"].intValue / 1000
    }
    
  }
  
  func action_hand_over(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      
      self?.setTitleView(detail: NSLocalizedString("警长选择玩家交替职位", comment: ""), time: jsonData["duration"].intValue / 1000)
      
      if jsonData["alives"].arrayValue.count != 0 {
        var jsondataArray = [(Int, JSON)]()
        
        for json in jsonData["alives"].arrayValue {
          let playerNumber = json.intValue
          let data = self?.getViewWithID(number: "\(playerNumber)")?.playerData ?? JSON.init(parseJSON: "")
          jsondataArray.append((playerNumber + 1, data))
        }
        
        let view = SeerCheckView.view(type: .hand_over, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
        //狼王，恶魔显示
        view.demon_position = (self?.demon_postion)!
        view.werewolf_king_position = (self?.werewolf_king_postion)!
        Utils.showNoticeView(view: view)
        view.liveTime = jsonData["duration"].intValue / 1000
      }
    }
  }
  
  func action_hand_over_result(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      
      let toView = self?.getViewWithID(number: jsonData["to"].stringValue)
      let fromView = self?.getViewWithID(number: jsonData["from"].stringValue)
      
      fromView?.setDeSheriff()
      
      if jsonData["to"].int != nil {
        // 交接警长
        let form = jsonData["from"].intValue
        let to = jsonData["to"].intValue
        let view = OnePersonView.viewSheriff(type: .handOver, data: (jsonData["from"].intValue + 1, fromView?.playerData ?? JSON.init(parseJSON: "")), data2: (jsonData["to"].intValue + 1, toView?.playerData ?? JSON.init(parseJSON: "")))
        Utils.showNoticeView(view: view)
        view.liveTime = 3
        
        Utils.delay(3, closure: {
          let resultView = OnePersonView.viewSheriff(type: .sheriff, data: (jsonData["to"].intValue + 1, toView?.playerData ?? JSON.init(parseJSON: "")))
          Utils.showNoticeView(view: resultView)
          resultView.liveTime = 2
        })
        let localOldP = NSLocalizedString("号玩家选择", comment: "")
        let localNewP = NSLocalizedString("号玩家继承警长", comment: "")
        self?.setTitleView(detail: "[\(form + 1)]\(localOldP)[\(to + 1)]\(localNewP)")
        self?.messageViewController.addConversion(type: .judge, messge: "[\(form + 1)]\(localOldP)[\(to + 1)]\(localNewP)")
        let localP = NSLocalizedString("号玩家当选为警长", comment: "")
        self?.messageViewController.addConversionWith(type: .judge, messge: "[\(to + 1)]\(localP)",gameMessageType: GameMessageType.hand_over_result)
        toView?.setSheriff()
        self?.currentSheriffPosition = jsonData["to"].intValue + 1
        
      } else {
        // 弃掉警徽
        self?.setTitleView(detail: NSLocalizedString("警长撕掉了警徽，未让任何人继承", comment: ""))
        self?.messageViewController.addConversionWith(type: .judge, messge: NSLocalizedString("警长撕掉了警徽，未让任何人继承", comment: ""),gameMessageType: GameMessageType.hand_over_result)
      }
    }
  }
  
  func action_reconnect(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      
      if jsonData.intValue != 1000 , CurrentUser.shareInstance.currentRoomID != ""{
        // 重连失败
        messageDispatchManager.viewController = nil
        self?.dismiss(animated: true, completion: nil)
        self?.leaveCurrentRoom()
        XBHHUD.showError(NSLocalizedString("你已经掉线，请检查您的网络", comment: ""))
      }
    }
    
  }
  
  func action_restore_room(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      self?.wolfTeammate = [JSON]()
      self?.lovers.removeAll()
      
      for view in self!.playerViewArray {
        view.setIconEmpty()
      }
      var noSppechInfo = false
      var gametypes = ""
      let jsonData = JSON.init(rawData)  as! JSON
      
      isPlaying = jsonData["room_info"]["isPlaying"].boolValue
      
      if let gameType = jsonData["room_info"]["type"].string {
        if gameType == "pre_simple" {
          self?.isNewPlayerModel = true
          gametypes = gameType
        }
      }
      
      if !isPlaying {
        // 这里会让取消准备按钮变为准备
        self?.clearGameRoomData()
        self?.checkBG(type: gametypes, status: 1)
        self?.checkNV(type: CurrentUser.shareInstance.currentRoomType, status: 1)
        self?.newPlayerModelUI()
      }
      
      if isPlaying {
        //把button按钮改变
        self?.readyButton.setTitle(NSLocalizedString("全体禁言", comment: ""), for: .normal)
        self?.inviteButton.setTitle(NSLocalizedString("结束发言", comment: ""), for: .normal)
        self?.rightChangeMasterButton.isHidden = true
        self?.righeNavButton.isHidden = true
        self?.rightInviteButton.isHidden = true
        self?.navRightViewWidth.constant = 33
        self?.statusRightPadding.constant = 5
      }
      
      let tempArr = jsonData["room_info"]["locked_positions"].arrayValue
      for number in tempArr {
        let view = self?.getViewWithID(number: number.stringValue);
        view?.setIconLocked()
      }
      //游戏玩家
      for (key, value) in jsonData["room_info"]["users"].dictionaryValue {
        let view = self?.getViewWithID(number: key)
        view?.playerData = value
        view?.newPlayerModel = (self?.isNewPlayerModel)!
        // 这里重新进行一次判断保证 准备 和 取消准备
        if let id = value["id"].string,
          id == CurrentUser.shareInstance.id,
          let prepare = value["prepared"].int,
          prepare == 1 {
          self?.readyButton.setTitle(NSLocalizedString("取消准备", comment: ""), for: .normal);
          self?.readyButton.isSelected = true;
        }
      }
      //游戏观战者
      for (key, value) in jsonData["room_info"]["observers"].dictionaryValue {
        if let userid = value["id"].string {
          if CurrentUser.shareInstance.id == userid {
            //自己是不是观战
            CurrentUser.shareInstance.is_observer = true
            CurrentUser.shareInstance.isMaster = false
            self?.observerUI()
          }
        }
      }
      //角色列表
       var titleViewRoleTotal = ""
      if let config = jsonData["room_info"]["config"].dictionary {
        if let map = config["role_map"]?.dictionary {
          for (key, value) in (config["role_map"]?.dictionaryValue)! {
            if value.intValue == 0 {
              continue
            }
            let count = value
            let role = GameRole.getRole(name: key).gameDescribe
            if count == 1 {
              titleViewRoleTotal = titleViewRoleTotal + "\(role) "
            }else{
              titleViewRoleTotal = titleViewRoleTotal + "\(role)X\(count) "
            }
            self?.currentPlayerCount += value.intValue
          }
          if titleViewRoleTotal != "" {
            self?.titleBackViewRoleTotalDes.text = titleViewRoleTotal
            self?.titleBackViewLine.isHidden = false
          }
          self?.decidePlaySunsetAudio(dic:config["role_map"]?.dictionaryValue as! NSDictionary)
        }
      }
      if let observer = jsonData["room_info"]["observers"].dictionary {
        self?.observers = observer
      }
      // 房主设置
      if CurrentUser.shareInstance.isMaster {
        self?.readyButton.setTitle(NSLocalizedString("开局", comment: ""), for: .normal)
      }
      
      CurrentUser.shareInstance.currentRole = GameRole.getRole(name: jsonData["game_info"]["role"].stringValue)
      let localRole = NSLocalizedString("当前角色",comment:"")
      self?.currentUserRoleLabel.text = "\(localRole)：\(CurrentUser.shareInstance.currentRole.descriptionString)"
      //新UI
      let myview = self?.getViewWithID(number:"\(CurrentUser.shareInstance.currentPosition - 1)")
      if isPlaying {
        //正在游戏中的话，显示当前角色,如果自己没有观战
        if CurrentUser.shareInstance.is_observer == false{
          self?.showRoleBar()
          myview?.setGoodPerson()
          myview?.role = jsonData["game_info"]["role"].stringValue
        }
      }
      
      self?.wolfTeammate = jsonData["game_info"]["teammates"].arrayValue
      for index in jsonData["game_info"]["teammates"].arrayValue {
        let view = self?.getViewWithID(number: index.stringValue)
        view?.setWolf()
      }
      //特殊处理自己是恶魔的情况
      if let isDemon = jsonData["game_info"]["role"].string {
        if isDemon == "demon" {
          myview?.setDemon()
          self?.demon_postion = CurrentUser.shareInstance.currentPosition - 1
        }
      }
      //白狼王
      if let king = jsonData["game_info"]["king"].int {
        let view = self?.getViewWithID(number:"\(king)")
        view?.setWolfKing()
        self?.werewolf_king_postion = king
      }
      
      let view = self?.getViewWithID(number: jsonData["game_info"]["sheriff"].stringValue)
      view?.setSheriff()
      if jsonData["game_info"]["sheriff"].int != nil {
        self?.currentSheriffPosition = jsonData["game_info"]["sheriff"].intValue + 1
      }
      for (key, value) in jsonData["game_info"]["linked"].dictionaryValue {
        self?.lovers.append(Int(key)! + 1)
        let view = self?.getViewWithID(number: key)
        view?.setLover()
        if value.boolValue {
          view?.setWolf()
        }
      }
      
      //死亡信息
      for index in jsonData["game_info"]["death"].arrayValue {
        let view = self?.getViewWithID(number: index.stringValue)
        view?.setDeath()
      }
      for info in jsonData["game_info"]["death_info"].arrayValue {
        if let position = info["position"].int {
          let view = self?.getViewWithID(number:"\(position)")
          view?.setDeath()
          if let role = info["role"].string {
            view?.showRole(role)
          }
          if position + 1 == CurrentUser.shareInstance.currentPosition {
            CurrentUser.shareInstance.isDeath = true
            self?.hideSpeakBar()
            self?.hideBottombar()
            self?.setSelfDeath()
          }
        }
      }
      //轮到谁发言
      if let turnToSay = jsonData["game_info"]["speech_info"].dictionary {
        let current = turnToSay["current"]?.dictionary
        let position = current?["position"]?.intValue
        let duration = turnToSay["duration"]?.intValue
        let dead = current?["dead"]?.boolValue
        let speech:Dictionary<String,Any> = ["position":position,"duration":duration,"dead":dead,"fake":"yh"]
        let fakeSpeech =  JSON.init(speech)//JSON.init(dictionaryLitersppecal: speech)
        NotificationCenter.default.post(name: Notification.Name.init("speech"), object:fakeSpeech)
        noSppechInfo = false
      }else{
        //没有人说话，隐藏发言
        noSppechInfo = true
        if isPlaying {
          //正在游戏中就先隐藏
          self?.hideBottombar()
          if self?.freeSpeech == false {
            self?.bottomView.itIsMyTurn = false
            self?.bottomView.dealWithLongTouch()
            self?.bottomView.setCanNotFreeTalk()
          }else{
            self?.hideSpeakBar()
          }
        }
      }
      //天亮天黑
      if let dayinfo = jsonData["game_info"]["day_info"].dictionary {
        let night = dayinfo["is_night"]?.boolValue
        let index = dayinfo["day_index"]?.intValue
        let duration = dayinfo["duration"]?.intValue
        if night!{
          //晚上
          let speech:Dictionary<String,Any> = ["day_index":index,"duration":duration,"fake":"yh"]
          let fakeDay = JSON.init(speech)
          NotificationCenter.default.post(name: Notification.Name.init("sunset"), object:fakeDay)
        }else{
          //白天
          var speech:Dictionary<String,Any>?
          if noSppechInfo {
            if isPlaying {
              //正在游戏中就先隐藏
              speech = ["day_index":index,"duration":duration,"fake":"yh_no_speech"]
            }else{
              //不在游戏中不隐藏
              speech = ["day_index":index,"duration":duration,"fake":"yh"]
            }
          }else{
              speech = ["day_index":index,"duration":duration,"fake":"yh"]
          }
          let fakeDay = JSON.init(speech)
          NotificationCenter.default.post(name: Notification.Name.init("sunup"), object:fakeDay)
        }
        if let canSpeak = dayinfo["can_speak"]?.bool,
          canSpeak,
          let duration = dayinfo["can_speak_duration"]?.int {
          let canSpeakData:Dictionary<String,Any> = ["duration":duration];
          let fakeDay = JSON.init(canSpeakData);
          NotificationCenter.default.post(name: Notification.Name.init(GameMessageType.can_speak.rawValue),
                                          object:fakeDay);
        }
      }

      //是否可以自爆
      if let boom = jsonData["game_info"]["boomable"].bool {
        self?.boomButton.isHidden = !boom
      }
    }
    
  }
  
  func action_unactive_warning(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      let view = NormalAlertView.view(type: .unActiveAlert, title: NSLocalizedString("提示", comment: ""), detail: NSLocalizedString("您已经长时间未进行任何操作，是否继续留在房间？", comment: ""))
      view.liveTime = jsonData["duration"].intValue / 1000
      view.cancelClickBlock = { [weak self] in
        self?.timeOver = $0
        XBHHUD.showCanNotHidden(title: NSLocalizedString("正在清理房间数据", comment: ""))
        messageDispatchManager.viewController = nil
        self?.dismissGameVC()
        self?.leaveCurrentRoom()
      }
      view.conformClickBlock = {
        messageDispatchManager.sendMessage(type: .continue)
      }
      Utils.showAlertView(view: view)
    }
    
  }
  
  func action_change_password(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      
      let jsonData = JSON.init(rawData)  as! JSON
      CurrentUser.shareInstance.currentRoomPassword = jsonData["password"].stringValue
      self?.setPassword()
    }
  }
  
  func action_append_time(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON,
        let leftTime = jsonData["left_time"].int,
        let positon = jsonData["position"].int,
        let addTime = jsonData["append_duration"].int {
        self?.liveTime = leftTime / 1000
        self?.bottomView.remainTime = 35
        let local = NSLocalizedString("号玩家使用延时卡，发言时间延长", comment: "")
        let view = self?.getViewWithID(number: jsonData["position"].stringValue)
        view?.startCutDown(Int(leftTime / 1000))
        
        self?.messageViewController.addConversionWith(type: .system, messge: NSLocalizedString("\(positon + 1)\(local)\(addTime / 1000)s", comment: ""),gameMessageType: GameMessageType.append_time_result);
      }
    }
  }
  
  func action_append_time_result(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON,
        let type = jsonData["type"].string {
        switch type {
        case "not_enough_card":
          XBHHUD.showError(NSLocalizedString("没有足够的延时卡", comment: ""));
        case "failed":
          XBHHUD.showError(NSLocalizedString("使用延时卡失败", comment: ""));
        case "illegal_state":
          XBHHUD.showError(NSLocalizedString("使用延时卡失败", comment: ""));
        case "only_once":
          XBHHUD.showError(NSLocalizedString("使用延时卡失败", comment: ""));
        default:
          // TODO 发消息
          print(NSLocalizedString("使用延时卡成功", comment: ""));
        }
      }
    }
  }
  
  func action_card_check_result(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON,
        let type = jsonData["type"].string {
        
        switch type {
        case "success":
          if let roleString = jsonData["role"].string,
            let position = jsonData["checked_position"].int,
            let view = self?.getViewWithID(position: position) {
            let role = GameRole.getRole(name: roleString)
            let noticeView = OnePersonView.view(
              role: role,
              playerData: (position + 1, view.playerData),
              detailRole: true);
            Utils.showNoticeView(view: noticeView)
            
          }
        case "not_enough_card":
          XBHHUD.showError(NSLocalizedString("没有足够的查验卡查询", comment: ""));
        case "failed":
          XBHHUD.showError(NSLocalizedString("查验卡使用失败", comment: ""));
        default:
          XBHHUD.showError(NSLocalizedString("未知错误:\(type)", comment: ""));
        }
      }
    }
  }
  
  func action_olive_changed(_ rawData: [String : Any]) {
    if let jsonData = JSON.init(rawData) as? JSON,
      let config = jsonData["media_server"].dictionary,
      let type = config["type"]?.int,
      let media_server = config["media_server"]?.string {
      
      var sendConfig:[String:Any] = [
        "connect_room_type": "\(type)",
        "node_server_host": media_server,
        "turn_server_conf": [:]
      ];
      
      // 不去做检测
      //    if let standbyRelayConf = config["standby_relay"].dictionaryObject,
      //      var turnServerConf = sendConfig["turn_server_conf"] as? [String:Any] {
      //      turnServerConf.updateValue(standbyRelayConf, forKey: "standby_relay");
      //      sendConfig.updateValue(turnServerConf, forKey: "turn_server_conf");
      //    }
      
      if let inTurnServers = config["turn_servers"]?.arrayObject,
        inTurnServers.count > 0,
        let inTurnServer = inTurnServers[0] as? [String:Any],
        var turnServerConf = sendConfig["turn_server_conf"] as? [String:Any] {
        turnServerConf.updateValue(inTurnServer, forKey: "turn_server");
        sendConfig.updateValue(turnServerConf, forKey: "turn_server_conf");
      }
      
      self.olConfig = sendConfig;
      
      olSessionIOError(data: []);
      
    }
  }
  
  func action_add_friend(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      //添加好友請求
      if let jsonData = JSON.init(rawData) as? JSON,
        let user_id = jsonData["user_id"].string,
        let name = jsonData["name"].string{
        var rect:CGRect = CGRect()
        var avater = jsonData["avatar"].string
        if avater == nil {
          avater = ""
        }
        
        if let strongSelf = self {
          if strongSelf.addFriendSet.contains(user_id) {
            return;
          }
          strongSelf.addFriendSet.insert(user_id);
          rect.origin = CGPoint(x: strongSelf.titleBackView.frame.origin.x - 10, y: strongSelf.titleBackView.frame.origin.y + 40)
          rect.size = CGSize(width: strongSelf.titleBackView.width, height: 90)
          let request = addFriendRequest(id:user_id, avaImage:avater!, name:name)
          strongSelf.requestManager.rect = rect
          strongSelf.requestManager.view = strongSelf.view
          strongSelf.requestManager.add(element: request)
        }
      }
    }
  }
  
  func action_protect(_ rawData: [String : Any]) {
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      guard CurrentUser.shareInstance.currentRole == .iguard else { return }
      let aliveArray = jsonData["alives"].arrayValue
      var jsondataArray = [(Int, JSON)]()
      for json in aliveArray {
        let playerNumber = json.intValue
        let data = self?.getViewWithID(number: "\(playerNumber)")?.playerData ?? JSON.init(parseJSON: "")
        jsondataArray.append((playerNumber + 1, data))
      }
      if let lastProtect = jsonData["last_protected"].int {
        let view = SeerCheckView.guardView(type: .protect, lastProtect: lastProtect, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
        view.lastGuard()
        Utils.showNoticeView(view: view)
        view.liveTime = jsonData["duration"].intValue / 1000
      }else{
        let view = SeerCheckView.view(type: .protect, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
        Utils.showNoticeView(view: view)
        view.liveTime = jsonData["duration"].intValue / 1000
      }
    }
  }
  
  func action_protect_result(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      if let guardPeople = jsonData["position"].int {
        let localChose = NSLocalizedString("本轮你选择守卫", comment: "")
        let localNotice1 = NSLocalizedString("号玩家", comment: "")
        if CurrentUser.shareInstance.currentRole == .iguard {
          self?.messageViewController.addConversionWith(type: .judge, messge:"\(localChose)[\(guardPeople + 1)]\(localNotice1)",gameMessageType: GameMessageType.protect_result)
        }
      }
    }
  }
  
  func action_boom(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      //把这个人设置为死亡,如果是自己死，就隐藏
      if CurrentUser.shareInstance.currentLang == "ja" {
        Utils.playSound(name: "boom_JP")
      }else{
        Utils.playSound(name: "boom")
      }
      //停止发言倒计时
      self?.hideBottombar()
      self?.speechingView = nil
      self?.bottomView.stopCountDown()
      //正在说话的处理掉
      self?.speeachTurn = Array()
      for view in (self?.playerViewArray)! {
        if view.playerData != nil {
          view.removeWaitForSpeeach()
          view.stopCutDown()
        }
      }
      if let position = jsonData["position"].int {
        let view = self?.getViewWithID(number: "\(position)")
        view?.setDeath()
        if position + 1 == CurrentUser.shareInstance.currentPosition {
          CurrentUser.shareInstance.isDeath = true
          self?.hideSpeakBar()
          self?.setSelfDeath()
          //隐藏禁言按钮
          self?.hideBottombar()
        }
        var king = false
        if let isKing = jsonData["is_werewolf_king"].bool {
          if isKing {
            self?.setTitleView(detail: NSLocalizedString("白狼王自爆，正在使用技能", comment: ""),userDes:"  ",time: jsonData["duration"].intValue / 1000)
            //白狼王添加法官信息
            king = true
            self?.messageViewController.addConversion(type: .judge, messge:"[\(position + 1)]号玩家自爆")
          }else{
            self?.messageViewController.addConversion(type: .judge, messge:"[\(position + 1)]号玩家自爆")
          }
        }
        //提示框
        let noticeView = OnePersonView.viewBoom(type: .boom, isKing:king, data: (position + 1, view?.playerData ?? JSON.init(parseJSON: "")))
        Utils.showNoticeView(view: noticeView)
        noticeView.liveTime = jsonData["duration"].intValue / 1000
      }
    }
    
  }
  
  func action_boom_away(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      if  CurrentUser.shareInstance.currentRole == .werewolf_king  {
        let aliveArray = jsonData["alives"].arrayValue
        var jsondataArray = [(Int, JSON)]()
        for json in aliveArray {
          let playerNumber = json.intValue
          let data = self?.getViewWithID(number: "\(playerNumber)")?.playerData ?? JSON.init(parseJSON: "")
          jsondataArray.append((playerNumber + 1, data))
        }
        let view = SeerCheckView.view(type: .boom_away, playerDataArray: jsondataArray, lovers: self!.lovers, sheriff: self!.currentSheriffPosition, wolfArr: self!.wolfTeammate)
        Utils.showNoticeView(view: view)
        view.liveTime = jsonData["duration"].intValue / 1000
      }
    }
    
  }
  
  func action_boom_away_result(_ rawData: [String : Any]) {
    
    Utils.runInMainThread { [weak self] in
      let jsonData = JSON.init(rawData)  as! JSON
      self?.setTitleView(detail: " ", userDes:" ")
      for dic in jsonData["death_info"].arrayValue {
        let view = self?.getViewWithID(number: dic["killed"].stringValue)
        view?.setDeath()
        if let postion = dic["killed"].int,
          let killer = dic["killer"].int{
          if CurrentUser.shareInstance.currentRole == .werewolf_king {
            let localKill = NSLocalizedString("你发动了白狼王技能，杀了", comment: "")
            let num = NSLocalizedString("号", comment: "")
            self?.setTitleView(detail: "\(localKill)[\(postion + 1)]\(num)", userDes:" ")
            self?.messageViewController.addConversionWith(type: .judge, messge:"\(localKill)[\(postion + 1)]\(num)", gameMessageType: GameMessageType.boom_away_result)
          }else{
            let localKill = NSLocalizedString("号玩家自爆，杀了", comment: "")
            let num = NSLocalizedString("号", comment: "")
            self?.setTitleView(detail: "[\(killer + 1)]\(localKill)[\(postion + 1)]\(num)", userDes:" ")
            self?.messageViewController.addConversionWith(type: .judge, messge:"[\(killer + 1)]\(localKill)[\(postion + 1)]\(num)", gameMessageType: GameMessageType.boom_away_result)
          }
          // 判断带走的是不是自己
          if CurrentUser.shareInstance.currentPosition == dic["killed"].intValue + 1 {
            self?.hideBottombar()
            self?.hideSpeakBar()
            self?.setSelfDeath()
          }
        }
      }
    }
    
  }
  
  func action_boom_state(_ rawData: [String : Any]) {
    
    let jsonData = JSON.init(rawData)  as! JSON
    Utils.runInMainThread { [weak self] in
      if let boomalbe = jsonData["can_boom"].bool {
        self?.boomButton.isHidden = !boomalbe
      }
    }
    
  }
  
  func action_export(_ rawData: [String : Any]) {
    
    if let jsonData = JSON.init(rawData) as? JSON,
      let title = jsonData["title"].string,
      let type = jsonData["type"].string{
      if let export = self.titleViewDetailLabel.superview?.subviews.first(where: {$0.isKind(of: exportNoticeView.self)}) {
        let e = export as! exportNoticeView
        e.title.text = title
        e.type = type
        export.isHidden = true
        setTitleView(title: NSLocalizedString("准备阶段", comment: ""), detail:title, userDes: "")
      }else{
        var rect = titleViewDetailLabel.frame
        rect.origin = CGPoint(x:0, y: rect.origin.y - 10)
        rect.size = CGSize(width: (titleViewDetailLabel.superview?.width)!, height: rect.size.height + 20)
        let export = Bundle.main.loadNibNamed("exportNotice", owner: nil, options: nil)?.last as! exportNoticeView
        export.frame = rect
        export.title.text = title
        export.type = type
        export.isHidden = true
        setTitleView(title: NSLocalizedString("准备阶段", comment: ""), detail:title, userDes: "")
        self.titleViewDetailLabel.superview?.addSubview(export)
      }
    }
    
  }
  
  func action_poison_result(_ rawData: [String : Any]) {
    
    let jsonData = JSON.init(rawData)  as! JSON
    Utils.runInMainThread { [weak self] in
      if let poison = jsonData["position"].int {
        let local = NSLocalizedString("本轮使用毒药毒杀了", comment: "")
        let num = NSLocalizedString("号玩家", comment: "")
        self?.messageViewController.addConversionWith(type: .judge, messge: "\(local)[\(poison + 1)]\(num)", gameMessageType:GameMessageType.poison_result)
      }
    }

  }
  
  func action_save_result(_ rawData: [String : Any]) {
    
    let jsonData = JSON.init(rawData)  as! JSON
    Utils.runInMainThread { [weak self] in
      if let save = jsonData["position"].int {
        let localSave = NSLocalizedString("本轮使用灵药救了", comment: "")
        let num = NSLocalizedString("号", comment: "")
        self?.messageViewController.addConversionWith(type: .judge, messge: "\(localSave)[\(save + 1)]\(num)", gameMessageType:GameMessageType.save_result)
      }
    }

  }
  
  func action_update_config(_ rawData: [String : Any]) {
  
    if let jsonData = JSON.init(rawData) as? JSON,
      let config = jsonData["config"].dictionary {
      self.remoteConfig = jsonData
      if let level = config["level"]?.stringValue{
        self.level = Int(level)!
        if let min = config["min_level"]?.stringValue {
          if Int(level)! > Int(min)! {
            self.levelLimitView.isHidden = false
            self.levelLimitLabel.text = "\(level)+"
          }else{
            self.levelLimitView.isHidden = true
          }
        }
      }
      if let can_cut_speaker = config["can_cut_speaker"]?.bool {
        self.freeSpeech = can_cut_speaker
      }
    }
    
  }
  
  func action_speech_direction(_ rawData: [String : Any]) {

    if let jsonData = JSON.init(rawData) as? JSON {
      Utils.runInMainThread { [weak self] in
        self?.bottomView.stopCountDown()
        //把所有人的发言打断
        self?.setTitleView(detail: NSLocalizedString("等待警长选择发言顺序", comment: ""), time: jsonData["duration"].intValue / 1000)
        if let jsonData = JSON.init(rawData) as? JSON,
          let left = jsonData["left"].int,
          let right = jsonData["right"].int{
          let leftData = self?.getViewWithID(number:"\(left)")
          let rightData = self?.getViewWithID(number: "\(right)")
          let views = TwoPersonView.view(left: (left + 1,(leftData?.playerData)!), right: (right + 1,(rightData?.playerData)!))
          Utils.showNoticeView(view: views)
          views.liveTime =  jsonData["duration"].intValue / 1000
        }
      }
    }

    
  }
  
  func action_speech_direction_result(_ rawData: [String : Any]) {
    
    if let jsonData = JSON.init(rawData) as? JSON {
      Utils.runInMainThread { [weak self] in
        self?.bottomView.stopCountDown()
        //把所有人的发言打断
        self?.setTitleView(detail:"  ", time: jsonData["duration"].intValue / 1000)
        if let jsonData = JSON.init(rawData) as? JSON,
          let left = jsonData["left"].int,
          let right = jsonData["right"].int{
          let leftData = self?.getViewWithID(number:"\(left)")
          let rightData = self?.getViewWithID(number: "\(right)")
          let views = TwoPersonView.view(left: (left + 1,(leftData?.playerData)!), right: (right + 1,(rightData?.playerData)!))
          Utils.showNoticeView(view: views)
          views.liveTime =  jsonData["duration"].intValue / 1000
        }
      }
    }


  }
  
  func action_request_reset_master(_ rawData: [String : Any]) {
    
    if let jsonData = JSON.init(rawData) as? JSON {
      Utils.runInMainThread { [weak self] in
        let view = changeMaster.view()
        view.liveTime = jsonData["duration"].intValue / 1000
        Utils.showNoticeView(view: view)
      }
    }

  }
  
  func action_reject_reset_master(_ rawData: [String : Any]) {
    
    if let jsonData = JSON.init(rawData) as? JSON {
      let delayTime = jsonData["duration"].intValue
      DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delayTime), execute: { [weak self] in
        self?.rightChangeMasterButton.isEnabled = true
      })
    }

  }
  
  
  @objc func action_connect_info(_ rawData: [String:Any]) {
    
    let data = JSON.init(rawData)
    
    if LocalSaveVar.connectID == "" {
      LocalSaveVar.connectID = data["connect_id"].stringValue
    }
    if(messageDispatchManager.isReconnecting()) {
      messageDispatchManager.sendMessage(type: .reconnect, payLoad: ["connect_id": LocalSaveVar.connectID]);
      messageDispatchManager.setReconnectingStatus(false);
    } else {
      
      if  CurrentUser.shareInstance.from != "" {
        messageDispatchManager.sendMessage(type: .enterRoom, payLoad: [
          "room_id": CurrentUser.shareInstance.currentRoomID,
          "password": CurrentUser.shareInstance.currentRoomPassword,
          "type"    : CurrentUser.shareInstance.currentRoomType,
          "token": CurrentUser.shareInstance.token,
          "extra" : [
            "pt": "\(Config.platform)",
            "v": "\(Config.buildVersion)",
            "b": "\(Config.appStoreVersion)",
            "sv": "\(Config.serverVersion)",
            "tz": "\(Config.currentTimeZoneGMT)",
            "lg": "\(Config.systemLanage)"
          ],
          "user": [
            "id": CurrentUser.shareInstance.id,
            "name": CurrentUser.shareInstance.name,
            "avatar": CurrentUser.shareInstance.avatar,
            "level": CurrentUser.shareInstance.level,
            "experience": CurrentUser.shareInstance.experience
          ],
          "from":"export"
          ])
      }else{
        messageDispatchManager.sendMessage(type: .enterRoom, payLoad: [
          "room_id":  CurrentUser.shareInstance.currentRoomID,
          "password": CurrentUser.shareInstance.currentRoomPassword,
          "type"    : CurrentUser.shareInstance.currentRoomType,
          "token": CurrentUser.shareInstance.token,
          "extra" : [
            "pt": "\(Config.platform)",
            "v": "\(Config.buildVersion)",
            "b": "\(Config.appStoreVersion)",
            "sv": "\(Config.serverVersion)",
            "tz": "\(Config.currentTimeZoneGMT)",
            "lg": "\(Config.systemLanage)"
          ],
          "user": [
            "id": CurrentUser.shareInstance.id,
            "name": CurrentUser.shareInstance.name,
            "avatar": CurrentUser.shareInstance.avatar,
            "level": CurrentUser.shareInstance.level,
            "experience": CurrentUser.shareInstance.experience
          ]
          ])
      }
    }
  }
  
  @objc func action_enter(_ rawData: [String : Any]) {
    
    let data = JSON.init(rawData)
    
    Utils.runInMainThread {
//      XBHHUD.hide()
      CurrentUser.shareInstance.inRoomStatus = .begin
      let rootViewController = UIApplication.shared.keyWindow?.rootViewController
      
      if CurrentUser.shareInstance.currentRoomType == "audio" {
        // ChatVoiceRoomViewController // 语音房间
        if let controller = messageDispatchManager.viewController as? ChatVoiceRoomViewController {
          controller.data = data;
          rootViewController?.present(controller, animated: true, completion: nil);
        } else {
          XBHHUD.showError("数据异常，请尝试重启APP并联系管理员(Error:001)");
        }
      } else {
        if let controller = messageDispatchManager.viewController as? GameRoomViewController {
          controller.data = data;
          CurrentUser.shareInstance.currentRoomID = data["room"]["room_id"].stringValue
          CurrentUser.shareInstance.realCurrentRoomID = data["room"]["room_id"].stringValue
          // 真正进入房间后进行重连次数修改
          messageDispatchManager.socketManager.currentRetryTime = messageDispatchManager.MAX_RETRY_TIMES
          // 链接而不进入房间 语音房间
          // XBHHUD.showLoading()
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.7, execute: {
            Utils.getCurrentVC(vc: (UIApplication.shared.keyWindow?.rootViewController)!)?.present(controller, animated: true, completion: {
                XBHHUD.hide()
            })
          })
        } else {
          XBHHUD.showError("数据异常，请尝试重启APP并联系管理员(Error:002)");
        }
      
      }
    }
  }
}




