//
//  MicroGameViewController.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/23.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit
import WebKit
import orangeLab_iOS

enum MicroGameLoadingStatus {
  case none
  case loadSuccess
  case searchSuccess
  case play
  case game_over
}
class MicroGameViewController: UIViewController {
  
    var web:WKWebView?
    //缓存消息
    var cachedMessage:Array<JSON> = Array()
    //进入房间的内容
    var data = JSON.init(parseJSON: "")  {
      didSet{
        //加入用户组
        if let allUsers = data["room"]["users"].dictionary {
          self.users = allUsers
        }
      }
    }
    //加载view
    let loadingView = Bundle.main.loadNibNamed("MicroGameLoading", owner: nil, options: nil)?.last as! MicroGameLoading
    //游戏URL
    var gameurl:String = ""
    //连接sockeet
    var socketConnect:[String:Any]?
    //返回Button
    var backButton:UIButton = UIButton()
    //游戏设置
    var config:JSON?
    //离开的用户
    var leaveUsers:[String:JSON] = [:]
    //用户列表
    var users:[String:JSON] = [:]
    //加载游戏状态
    var status:MicroGameLoadingStatus = .none {
      didSet{
        loadingView.status = status
      }
    }
    //随机用户头像
    var random_userImage:[JSON]?
    //是不是对战
    var isPVP = false {
      didSet{
        if isPVP {
          if let url = CurrentUser.shareInstance.microGame?["url"]?.string {
            self.gameurl = url
          }
          if let landscape = CurrentUser.shareInstance.microGame?["landscape"]?.bool {
            self.landScapeModel = landscape
          }
        }
      }
    }
    //对战模式下对手信息
    var oppoInfo:[String:JSON]?
    //头部
    @IBOutlet weak var nativeHeader: UIView!
    //横屏模式
    var landScapeModel = false {
      didSet{
        if landScapeModel == false {
          oritentionFinished = true 
        }
      }
    }
    //web显示出来啦
    var webShow = false
    //屏幕设置成功
    var oritentionFinished = false
    //重新匹配
    var rematch = false
    //游戏类型
    var gameType:String = ""
    //游戏结束数据
    var gameOverData:JSON?
    //serverget 信息
    //游戏信息
    var gameInfo:JSON? {
      didSet{
        //随机用户
        if let user_images = gameInfo?["user_images"].array{
          self.random_userImage = user_images
        }
        if let url = gameInfo?["mini_game"]["url"].string{
          self.gameurl = url
        }
        if let landscape = gameInfo?["mini_game"]["landscape"].bool {
          self.landScapeModel = landscape
        }
      }
    }
    /* audio metting*/
    weak internal var publisher: OLPublisher?
    internal var session:OLSession?;
    var olConfig:[String: Any] = [:];
    var mediaServer:JSON?
    var roomId:String = "";
    var connectUser:OLConnectorInfos = OLConnectorInfos();
    var statusBarHidden:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
      // 游戏服务器的断开
        addNotifcation()
        getWeb()
        handleMicroGame()
        view.addSubview(web!)
        if gameurl.isEmpty {
          leaveGame()
          XBHHUD.showError("游戏初始化失败")
        }
        let addHeaderToUrl = headerURLParams()
        if gameurl.contains("?") {
          gameurl = gameurl + "&access_token=" + "\(CurrentUser.shareInstance.token)&\(addHeaderToUrl)"
        }else{
          gameurl = gameurl + "?access_token=" + "\(CurrentUser.shareInstance.token)&\(addHeaderToUrl)"
        }
        print("+++++++ \(gameurl) ======")
        let request = URLRequest(url: URL(string:gameurl)!)
        self.web?.load(request)
        self.web?.snp.makeConstraints({ (make) in
          make.top.equalToSuperview().offset(0)
          make.left.right.bottom.equalToSuperview()
        })
        setOrientation(UIInterfaceOrientation.portrait)
        view.bringSubview(toFront: nativeHeader)
        self.automaticallyAdjustsScrollViewInsets = false
        //添加正在加载动画
        loadingView.isPVP = isPVP
        loadingView.gameInfo = gameInfo
        if oppoInfo != nil {
          loadingView.loadOppo(oppoInfo!)
        }
        loadingView.random_iamge = random_userImage
        loadingView.goBack = { [weak self] in
          self?.leaveGame()
        }
        view.addSubview(loadingView)
        loadingView.snp.makeConstraints { (make) in
          make.top.equalToSuperview().offset(0)
          make.left.equalToSuperview()
          make.right.equalToSuperview()
          make.bottom.equalToSuperview()
        }
        web?.scrollView.isScrollEnabled = false
        isPlaying = true
        //已经进入房间
    }
  
    func headerURLParams()  -> String  {
      return "pt=\(Config.platform)&v=\(Config.buildVersion)&b=\(Config.appStoreVersion)&sv=\(Config.serverVersion)&tz=\(Config.currentTimeZoneGMT)&lg=\(Config.systemLanage)&app=\(Config.app)&did=\(Utils.getUUID() ?? "")";
    }
  
    func addNotifcation() {
      NotificationCenter.default.addObserver(self,
      selector: #selector(reconnect(notification:)),
      name:  Notification.Name.init(GameMessageType.reconnect.rawValue),
      object: nil)
      
      NotificationCenter.default.addObserver(self, selector: #selector(server_disconnect(notification:)), name: NSNotification.Name(rawValue:MICRO_GAME_DISCONNECT), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(server_connect(notification:)), name: NSNotification.Name(rawValue:MICRO_GAME_CONNECT), object: nil)
    }
  
    override func viewWillAppear(_ animated: Bool) {
      UIApplication.shared.isStatusBarHidden = true
      setNeedsStatusBarAppearanceUpdate()
    }
  
    override func viewWillDisappear(_ animated: Bool) {
      UIApplication.shared.isStatusBarHidden = false
      statusBarHidden = false
      setNeedsStatusBarAppearanceUpdate()
    }
  
    override var prefersStatusBarHidden: Bool {
      return statusBarHidden
    }
  
    func handleMicroGame() {
      gameType = CurrentUser.shareInstance.currentRoomType
    }
  
    @IBAction func goBackButtonClicked(_ sender: Any) {
      let detail = NSLocalizedString("是否退出游戏?", comment: "")
      let view = NormalAlertView.view(type: .twoButton, title: NSLocalizedString("提示", comment: ""), detail: detail)
      view.type = .delayTouchable
      view.confirmButton.isEnabled = true
      view.conformClickBlock = { [weak self] in
        self?.leaveGame()
      }
      view.cancelClickBlock = { [weak self] (finished)  in
        if Utils.getCurrentNoticeViews().count > 0 {
          //说明结束界面存在,需要隐藏
          Utils.hideNoticeView()
        }else{
          //如果有横屏的结束界面
          if let bgView = self?.view.viewWithTag(9527) {
            bgView.removeFromSuperview()
          }
        }
      }
      if (self.landScapeModel) {
        if let bgView = self.view.viewWithTag(9527) {
          bgView.removeFromSuperview()
        }
        let backGroundView = UIView()
        backGroundView.tag = 9527
        let max = Screen.height > Screen.width ? Screen.height : Screen.width
        backGroundView.frame = CGRect.init(x: 0, y: 0, width: max, height: max)
        backGroundView.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        self.view.addSubview(backGroundView)
        backGroundView.addSubview(view)
      }else{
        Utils.showNoticeView(view: view)
      }
    }
    func getBackButton() {
      backButton.setImage(UIImage.init(named: ""), for: .normal)
      backButton.addTarget(self, action: #selector(leaveGame), for: .touchUpInside)
      backButton.frame = CGRect.init(x: 20, y: 20, width: 30, height: 30)
      view.addSubview(backButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //告诉web已经断开链接
    func server_disconnect(notification: Notification) {
      let jsMessage = ["gameMessageType":"server_disconnect","gameData":[:]] as [String : Any]
      SendMessageToWeb(jsMessage)
    }
    //告诉web在连接
    func server_connect(notification: Notification) {
      let jsMessage = ["gameMessageType":"server_connect","gameData":[:]] as [String : Any]
      SendMessageToWeb(jsMessage)
    }
  
    func dispatchCachedMessage() {
        if (self.cachedMessage.count) > 0 {
          for message in (self.cachedMessage) {
            //发送转发消息
            self.runDispatch(message)
          }
          //clear message
          self.cachedMessage.removeAll()
        }
    }
  
    // reconnect是否成功
    func reconnect(notification: Notification) {
      Utils.runInMainThread { [weak self] in
        let jsonData = notification.object as! JSON
        if jsonData.intValue != 1000 {
          // 重连失败
          Utils.hideNoticeView()
          if jsonData.intValue != 9999 {
            XBHHUD.showError("你已经掉线，请检查您的网络".localized)
          }
          self?.leaveGame()
        }
      }
    }
  
    func leftClicked() {
      if (self.web?.canGoBack)! {
        if (self.web?.isLoading)! {
          self.web?.stopLoading()
        }
        self.web?.goBack()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {[weak self] in
          self?.web?.reload()
        })
      }else{
        RNMessageSender.emitEvent(name: "EVENT_USER_LEAVE_FROM_WEB")
        self.leaveGame()
      }
    }
  
    func getWeb() {
      if self.web == nil {
        let config = WKWebViewConfiguration()
        config.allowsInlineMediaPlayback = true
        if #available(iOS 9.0, *) {
          config.allowsAirPlayForMediaPlayback = true
        } else {
          // Fallback on earlier versions
        }
        if #available(iOS 9.0, *) {
          config.allowsPictureInPictureMediaPlayback = true
        } else {
          // Fallback on earlier versions
        }
        config.suppressesIncrementalRendering = true
        let content = WKUserContentController()
        content.add(WeakScriptMessageDelegate(delegate: self), name: "WereWolf")
        config.userContentController = content
        self.web = WKWebView(frame: .zero, configuration: config)
        self.web?.isOpaque = false
        self.web?.navigationDelegate = self
        self.web?.backgroundColor = UIColor.gray
        self.web?.uiDelegate = self
        self.web?.sizeToFit()
      }
    }
  
    func leaveGame() {
      self.dismiss(animated: true, completion:{
      });
      Utils.hideNoticeView()
      messageDispatchManager.sendMicroGameMessage(type:"leave")
      let value = UIInterfaceOrientation.portrait.rawValue
      UIDevice.current.setValue(value, forKey: "orientation")
      XBHHUD.showCanNotHidden(title: NSLocalizedString("正在清理房间数据", comment: ""))
      messageDispatchManager.viewController = nil
      CurrentUser.shareInstance.currentRoomID = ""
      CurrentUser.shareInstance.realCurrentRoomID = ""
      CurrentUser.shareInstance.from = ""
      CurrentUser.shareInstance.inRoomStatus = .none
      //清空数据
      messageDispatchManager.microExtraData = ""
    }
  
    override var shouldAutorotate: Bool {
      if webShow && oritentionFinished == false {
        oritentionFinished = true
        print("允许横屏允许横屏允许横屏允许横屏允许横屏允许横屏允许横屏允许横屏允许横屏")
        return true
      }
      print("不允许不允许不允许不允许不允许不允许不允许不允许不允许不允许")
      return false
    }
  
    func setOrientation(_ orientatio:UIInterfaceOrientation) {
      let value = orientatio.rawValue
      UIDevice.current.setValue(value, forKey: "orientation")
    }

    deinit {
      Print.dlog("====== MicroGameViewController deinit =====")
      isPlaying = false
      disConnect()
      Utils.hideNoticeView()
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "microGameOVer"), object: nil)
      messageDispatchManager.close();
      messageDispatchManager.socketManager.currentRetryTime = 1
      CurrentUser.shareInstance.inRoomStatus = .none
      CurrentUser.shareInstance.clearCurrentInformation()
      NotificationCenter.default.removeObserver(self)
      RNMessageSender.emitEvent(name: "EVENT_USER_LEAVE");
      XBHHUD.hide()
      self.web?.configuration.userContentController.removeScriptMessageHandler(forName: "WereWolf")
      self.web?.stopLoading()
      print("microgame deinit")
      //如果是横屏的化，需要处理一下啊
      if landScapeModel {
        setOrientation(.portrait)
        Utils.backGroundView.frame = CGRect.init(x: 0, y: 0, width: Screen.width, height: Screen.height)
      }
      //这个时候，查看需不需要rematch
      if rematch {
        reMatch(gameType)
      }
    }
}


