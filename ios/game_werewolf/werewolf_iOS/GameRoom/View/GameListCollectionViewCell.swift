//
//  GameListCollectionViewCell.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/19.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

class GameListCollectionViewCell: UICollectionViewCell {
    
  @IBOutlet weak var gameImage: UIImageView!
  @IBOutlet weak var gameName: UILabel!
  var gameType:String?
  var data:JSON?
  func config(_ json:JSON) {
    self.data = json
    if let name = json["name"].string {
       self.gameName.text = name
    }
    if let icon = json["icon"].string {
      gameImage.sd_setImage(with: URL.init(string: icon))
    }
    if let type = json["type"].string {
      gameType = type
    }
  }
  override func awakeFromNib() {
//    self.layer.borderWidth = 2
//    self.layer.borderColor = UIColor.hexColor("8161E1").cgColor
    self.layer.cornerRadius = 6
  }
}
