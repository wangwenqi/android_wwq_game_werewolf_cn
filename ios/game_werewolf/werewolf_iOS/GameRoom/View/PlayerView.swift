//
//  PlayerView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

extension PlayerView:SRCountdownTimerDelegate {
  func timerDidEnd() {
    stopCutDown()
  }
}

class RoleLable:UILabel {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  static func generate(frame:CGRect) -> RoleLable{
    let role = RoleLable()
    role.frame = frame
    role.textColor = UIColor.white
    role.backgroundColor = UIColor.hexColor("6B77E5")
    role.textAlignment = .center
    role.layer.cornerRadius = 3
    role.layer.masksToBounds = true
    role.font = UIFont.boldSystemFont(ofSize: 10)
    return role
  }
  static func generate() -> RoleLable{
    let role = RoleLable()
    role.textColor = UIColor.white
    role.backgroundColor = UIColor.hexColor("FC139E")
    role.textAlignment = .center
    role.layer.cornerRadius = 3
    role.layer.masksToBounds = true
    role.font = UIFont.boldSystemFont(ofSize: 10)
    return role
  }
  static func generateForVote() -> RoleLable {
    let role = RoleLable()
    role.textColor = UIColor.hexColor("1F0862")
    role.backgroundColor = CustomColor.newRoomYellow//UIColor.hexColor("FC139E")
    role.textAlignment = .center
    role.layer.cornerRadius = 3
    role.layer.masksToBounds = true
    if Screen.width > 320 {
      role.font = UIFont.systemFont(ofSize: 10)
    }else{
      role.font = UIFont.systemFont(ofSize: 7)
    }
    return role
  }
}

class RoleAfterDeathLable:UILabel {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  static func generate() -> RoleAfterDeathLable{
    let view = RoleAfterDeathLable()
    view.textColor = UIColor.white
    view.backgroundColor = CustomColor.roomPink
    if Screen.width > 320 {
      view.font = UIFont.systemFont(ofSize: 10)
    }else{
      view.font = UIFont.systemFont(ofSize: 7)
    }
    view.textColor = UIColor.white
    view.textAlignment = .center
    view.layer.cornerRadius = 3
    view.layer.masksToBounds = true
    return view
  }
}

class PlayerView: XBHView {

  //自己的身份
  var role = "" {
    didSet{
       if let _ = self.subviews.first(where: { $0.isKind(of:RoleLable.self)  }) {
        self.bringSubview(toFront: RoleIdenfity)
       }else{
         RoleIdenfity.text = GameRole.getRole(name: role).showRole
         //根据自己身份显示不同颜色
          if role == "werewolf" || role == "werewolf_king" || role == "demon" {
            RoleIdenfity.backgroundColor = CustomColor.newRoomYellow
            RoleIdenfity.textColor = UIColor.hexColor("1F0064")
          }else{
            RoleIdenfity.backgroundColor = UIColor.hexColor("4a0082")
          }
         addSubview(RoleIdenfity)
         self.bringSubview(toFront: RoleIdenfity)
       }
    }
  }
  // 玩家的游戏状态
  var isMaster = false
  // 玩家是否死亡
  var isDeath = false
  //是不是新手模式
  var newPlayerModel = false
  //gameType
  var game = ""
  // 常量
  struct consts {
    static let cornerRadio: CGFloat = 3.0
    static let numberScale: CGFloat = 0.35
  }
  
  // 当前座位的状态
  enum PositionStatus {
    case havePlayer
    case empty
    case locked
  }
  
  var currentPositionState: PositionStatus = .empty
  
  var didTappedBlock: ((_ playerView: PlayerView) -> Void)?
  
  var currentPostion = -1
  
  var myTurnToSay = false {
    didSet{
      if myTurnToSay == true {
        speakView.isHidden = false
        cutDown.isHidden = false
      }else{
        speakView.isHidden = true
        cutDown.isHidden = true
      }
    }
  }
  
  // 玩家信息模型
  var playerData = JSON.init(parseJSON: "") {
    didSet {
      
      if playerData.debugDescription == "null" {
        return
      }
      if UIScreen.main.bounds.width > 320 {
        numberLabel.font = UIFont.boldSystemFont(ofSize: 12)
        nameLabel.font = UIFont.boldSystemFont(ofSize: 10)
        numberLabel.verticalAlignment = .Middle
      } else {
        numberLabel.font = UIFont.boldSystemFont(ofSize: 10)
        numberLabel.verticalAlignment = .Top
        nameLabel.font = UIFont.boldSystemFont(ofSize: 8)
      }
      // 保存当前玩家自己的房间信息
      if  playerData["position"].intValue == currentPostion {
        CurrentUser.shareInstance.currentPosition = playerData["position"].intValue + 1
        CurrentUser.shareInstance.isMaster = playerData["is_master"].boolValue
      }
  
      isMaster = playerData["is_master"].boolValue
  
      iconImage.image = UIImage.init(named: "room_head_default")
      if let iconUrl = URL.init(string: playerData["avatar"].stringValue) {
        iconImage.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
      }
      //边框颜色
      iconImage.layer.borderColor = UIColor.hexColor("FC56FB").cgColor
      iconImage.layer.borderWidth = 2
      //更新背景颜色
      avaBg.backgroundColor = UIColor.hexColor("7c3cf5")//UIColor.hexColor("9D78FB")
      self.superview?.addSubview(numberBg)

      if !isPlaying {
        if playerData["prepared"].boolValue {
          prepare()
        } else {
          unPrepare()
        }
      }
      
      if playerData["is_master"].bool == true && isPlaying == false {
        setMaster()
      }
          
      if playerData["speaking"].boolValue  {
        startSpeak()
      }
      
      if playerData["is_leaved"].boolValue {
        
      }
      // 名字
      nameLabel.text = playerData["name"].stringValue
      addSubview(nameLabel)
      currentPositionState = .havePlayer
    }
  }
  //显示身份
  
  lazy var RoleIdenfity:RoleLable = {
      let role = RoleLable.generate()
      return role
  }()
  
  
  //游戏中标志
  lazy var gameSimbol:UIStackView = {
    let stack = UIStackView()
    stack.axis = .horizontal
    stack.alignment = .fill
    stack.distribution = .fillEqually
    stack.backgroundColor = UIColor.brown
    return stack
  }()
  
  //头像背景
  
 lazy var avaBg: UIView = {
    let view = UIView()
    view.layer.cornerRadius = 3
    view.backgroundColor = UIColor.hexColor("7c3cf5")
    return view
  }()
  
  // 头像
  lazy var iconImage: UIImageView = {
    let imageView = UIImageView.init(image: UIImage.init(named: "room_bg_vacant"))
    imageView.contentMode = .scaleAspectFill
    imageView.clipsToBounds = true
    imageView.layer.cornerRadius = consts.cornerRadio
    return imageView
  }()
  
  // 名字
  lazy var nameLabel: UILabel = {
    let label = UILabel.init()
    label.textColor = UIColor.white
    label.textAlignment = .center
    label.font = UIFont.systemFont(ofSize: 10)
    label.sizeToFit()
    return label
  }()
  
  //编号背景
  
  lazy var numberBg:UIView = {
    let view = UIView()
    view.backgroundColor = UIColor.darkGray
    view.layer.shadowColor = UIColor.darkGray.cgColor
    view.layer.shadowOffset = CGSize(width: 0, height: 4)
    view.layer.shadowOpacity = 0.8
    return view
  }()
  
  // 编号
  lazy var numberLabel: verticalAlignmentLable = {
    let label  = verticalAlignmentLable()
    label.verticalAlignment = .Middle
    label.clipsToBounds = true
    label.layer.cornerRadius = 2
    label.textColor = UIColor.white
    label.layer.borderColor = UIColor.hexColor("3d0086").cgColor
    label.layer.borderWidth = 1
    label.textAlignment = .center
    label.font = UIFont.boldSystemFont(ofSize: 16)
    label.sizeToFit()
    label.text = "\(self.tag)"
    label.backgroundColor = UIColor.hexColor("FC139E")//CustomColor.roomGreen
    return label
  }()
  
  // 状态
  lazy var statusLabel: SpringLabel = {
    let label = SpringLabel()
    label.textColor = UIColor.darkGray
    label.backgroundColor = CustomColor.newRoomYellow//UIColor.hexColor("2DFFFF")
    label.textAlignment = .center
    if UIScreen.main.bounds.width > 320 {
      label.font = UIFont.systemFont(ofSize: 9, weight:UIFontWeightHeavy)
    } else {
      label.font = UIFont.systemFont(ofSize: 6, weight:UIFontWeightHeavy)
    }
    label.text = NSLocalizedString("已准备", comment: "")
    label.layer.cornerRadius = 5
    label.clipsToBounds = true
    return label
    
  }()
  
  // 房主
  lazy var masterLabel: UILabel = {
    let label = UILabel()
    label.textColor = UIColor.darkGray
    label.textAlignment = .center
    if UIScreen.main.bounds.width > 320 {
      label.font = UIFont.systemFont(ofSize: 9, weight:UIFontWeightHeavy)
    } else {
      label.font = UIFont.systemFont(ofSize: 6, weight:UIFontWeightHeavy)
    }

    label.text = NSLocalizedString("房主", comment: "")
    label.backgroundColor = UIColor.hexColor("2DFFFF")// #colorLiteral(red: 0.7487837672, green: 0.09917544574, blue: 0.004318060819, alpha: 1)
    label.layer.cornerRadius = 4
    label.clipsToBounds = true
    return label
    
  }()
  
  // wolf
  lazy var wolfImageView: UIImageView = {
    let image = UIImageView.init(image: UIImage.init(named: "room_icon_wolf"))
    image.contentMode = .scaleAspectFit
    image.clipsToBounds = true
    return image
  }()
  
  //wolfking
  lazy var wolfKingImageview:UIImageView = {
    let image = UIImageView.init(image: UIImage.init(named: "wolfKing"))
    image.contentMode = .scaleAspectFit
    image.clipsToBounds = true
    return image
  }()
  
  // 竞选者
  lazy var candidateImageView: UIImageView = {
    let image = UIImageView.init(image: UIImage.init(named: "room_icon_handsup"))
    image.contentMode = .scaleAspectFit
    image.clipsToBounds = true
    return image
  }()
  
  // 警长
  lazy var sheriffImageView: UIImageView = {
    let image = UIImageView.init(image: UIImage.init(named: "room_icon_badge"))
    image.contentMode = .scaleAspectFill
    image.clipsToBounds = true
    return image
  }()
  
  // 情侣
  lazy var loveImageView: UIImageView = {
    let image = UIImageView.init(image: UIImage.init(named: "room_icon_heart"))
    image.contentMode = .scaleAspectFill
    image.clipsToBounds = true
    return image
  }()
  
  
  // 死亡
  lazy  var deadImage: UIView = {
    let view = UIView()
    let image = UIImageView.init(image: UIImage.init(named: "room_icon_dead"))
    view.addSubview(image)
    image.contentMode = .scaleAspectFit
    view.backgroundColor = UIColor.black
    view.alpha = 0.5
    return view
  }()
  
  
  // 锁
  lazy  var lockImage: UIImageView = {
    let image = UIImageView.init(image: UIImage.init(named: "锁座位-ios"))
    image.contentMode = .scaleAspectFill
    image.clipsToBounds = true
    image.layer.cornerRadius = 3
    return image
  }()
  
  // mic
  lazy  var micImage: UIImageView = {
    let image = UIImageView()
    image.animationImages = [UIImage.init(named: "话筒1")!,UIImage.init(named: "话筒2")!,UIImage.init(named: "话筒3")!,UIImage.init(named: "话筒4")!] //[UIImage.init(named: "room_icon_microphone_small")!,UIImage.init(named: "room_icon_microphone_medium")!,UIImage.init(named: "room_icon_microphone_large")!]
    image.animationDuration = 0.5
    image.contentMode = .scaleAspectFill
    return image
  }()
  
  //倒计时
  lazy var cutDown:SRCountdownTimer = {
    let countdownTimer = SRCountdownTimer()
    countdownTimer.isLabelHidden = true
    countdownTimer.lineWidth = 2
    countdownTimer.lineColor = UIColor.white
    countdownTimer.delegate = self
    countdownTimer.trailLineColor = UIColor.hexColor("8C8C8C")
    countdownTimer.backgroundColor = UIColor.clear
    return countdownTimer
  }()
  
  //发言图标
  lazy var speakImage:UIImageView = {
    let image = UIImageView()
    image.image = UIImage.init(named:"话筒1")
    return image
  }()
  
  //发言倒计时动画
  lazy var speakView:UIView = {
    let view = UIView()
    view.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
    view.backgroundColor = UIColor.black.withAlphaComponent(0.65)
    view.addSubview(self.speakImage)
    view.addSubview(self.micImage)
    view.addSubview(self.cutDown)
    view.layer.cornerRadius = 3
    view.layer.masksToBounds = true
    return view
  }()
  //死后显示自己身份
  lazy var showRoleAfterDeath:UILabel = {
    let view = RoleAfterDeathLable.generate()
    return view
  }()
  
  //等待发言标志
  lazy var waitForSpeeachPoint:UIView  = {
    let view = UIView()
    view.backgroundColor = UIColor.hexColor("64f0f0")
    view.layer.cornerRadius = 3
    return view
  }()
  
  //个人等级图标
  lazy var levelImage:UIImageView = {
    let image = UIImageView()
    return image
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  func showSelfRole() {
    
  }

  func setIconLocked() {
    for view in subviews {
      view.removeFromSuperview()
    }
    addSubview(lockImage)
    currentPositionState = .locked
    clearUserData()
    unSpeeching()
  }
  
  func setIconEmpty() {
    iconImage.sd_cancelCurrentImageLoad()
    for view in subviews {
      view.removeFromSuperview()
    }
    addSubview(iconImage)
    iconImage.image = UIImage.init(named: "room_bg_vacant")
    iconImage.layer.borderWidth = 0
    currentPositionState = .empty
    clearUserData()
    unSpeeching()
    //添加默认的UI
    nameLabel.text = ""
    //改回默认颜色
    avaBg.backgroundColor = UIColor.hexColor("7c3cf5")
    addSubview(avaBg)
    addSubview(nameLabel)
    addSubview(speakView)
  
    //删除编号
    numberBg.removeFromSuperview()
  }
  
  func setNormalBG() {
     avaBg.backgroundColor = UIColor.hexColor("7c3cf5")
  }
  
  func clearUserData() {
    playerData = JSON.init(parseJSON: "")
  }
  
  // MARK: - 游戏逻辑
  // 新玩家进入
  func newUserComeIn(jsonData: JSON) {
    self.playerData = jsonData
  }
  
  //不是遗言的轮麦
  func waitForSpeeach() {
    if isDeath == false{
      self.superview?.addSubview(waitForSpeeachPoint)
    }
  }
  
  //移除view
  func removeWaitForSpeeach() {
    waitForSpeeachPoint.removeFromSuperview()
  }
  
  // 玩家离开
  func userLeave(jsonData: JSON) {
    if jsonData.boolValue == false {
      self.addSubview(deadImage)
    } else {
      setIconEmpty()
    }
  }
  
  // 设置房主
  func setMaster() {
    unPrepare()
    addSubview(masterLabel)
  }
  
  // 取消房主
  func unSetMaster() {
    unPrepare()
    masterLabel.removeFromSuperview();
  }
  
  // 准备
  func prepare() {
    addSubview(statusLabel)
    statusLabel.animation = "zoomIn"
    statusLabel.curve = "easeOut"
    statusLabel.duration = 0.45
    statusLabel.damping = 0.5
    statusLabel.animate()
  }
  
  // 设置为狼人
  func setWolf() {
    RoleIdenfity.removeFromSuperview()
    RoleIdenfity.text = GameRole.getRole(name:"werewolf").showRole
    RoleIdenfity.backgroundColor = CustomColor.newRoomYellow
    RoleIdenfity.textColor = UIColor.black
    addSubview(RoleIdenfity)
    self.bringSubview(toFront: RoleIdenfity)
    //改变背景色
    avaBg.backgroundColor = UIColor.hexColor("E529AC")
    
  }
  
  func setDemon() {
    RoleIdenfity.removeFromSuperview()
    RoleIdenfity.text = GameRole.getRole(name:"demon").showRole
    addSubview(RoleIdenfity)
    self.bringSubview(toFront: RoleIdenfity)
    avaBg.backgroundColor = UIColor.hexColor("E529AC")
  }
  
  func setGoodPerson() {
    avaBg.backgroundColor = UIColor.hexColor("F6F535")
  }
  
  func setWolfKing() {
    RoleIdenfity.removeFromSuperview()
    RoleIdenfity.text = GameRole.getRole(name:"werewolf_king").showRole
    RoleIdenfity.backgroundColor = CustomColor.newRoomYellow
    RoleIdenfity.textColor = UIColor.black
    addSubview(RoleIdenfity)
    self.bringSubview(toFront: RoleIdenfity)
//    addSubview(wolfKingImageview)
  }
  
  // 取消准备
  func unPrepare() {
    statusLabel.removeFromSuperview()
  }
  
  // 游戏开始
  func gameStarted() {
    statusLabel.animation = "fadeOut"
    statusLabel.curve = "easeOut"
    statusLabel.duration = 0.4
    statusLabel.damping = 1.0
    statusLabel.velocity = 0.7
    statusLabel.animateNext { [weak self] in
      self?.statusLabel.removeFromSuperview()
      self?.masterLabel.removeFromSuperview()
    }
  }
  
  // 设置为死亡
  func setDeath() {
    addSubview(deadImage)
//    self.bringSubview(toFront: numberBg)
    removeWaitForSpeeach()
    isDeath = true
  }
  
  // 竞选者
  func setCandidate() {
    self.superview?.addSubview(candidateImageView)
//    addSubview(candidateImageView)
  }
  
  // 取消竞选者
  func setUnCandidate() {
    candidateImageView.removeFromSuperview()
  }
  
  // 设置为警长
  func setSheriff() {
    addSubview(sheriffImageView)
  }
  
  // 取消设置为警长
  func setDeSheriff() {
    sheriffImageView.removeFromSuperview()
  }
  
  // 轮麦
  func speeching() {
    if CurrentUser.shareInstance.currentPosition == tag {
      layer.borderColor = CustomColor.roomYellow.cgColor
    } else {
      if newPlayerModel {
        layer.borderColor = UIColor.hexColor("d8adfc").cgColor
      }else{
        layer.borderColor = UIColor.hexColor("d8adfc").cgColor//CustomColor.speechingColor.cgColor
      }
    }
    Utils.runInMainThread { [weak self] in
      self?.layer.borderWidth = 2
    }
  }
  
  func unSpeeching() {
    Utils.runInMainThread { [weak self] in
      self?.layer.borderWidth = 1
      self?.layer.borderColor = UIColor.hexColor("3d0086").cgColor
    }
  }
  
  //明牌
  func showRole(_ roles:String) {
    let string = GameRole.getRole(name: roles).descriptionString
    showRoleAfterDeath.text = string
    addSubview(showRoleAfterDeath)
    showRoleAfterDeath.isHidden = false
    self.bringSubview(toFront: showRoleAfterDeath)
  }
  
  // 设置为情侣
  func setLover() {
    addSubview(loveImageView)
  }
  
  // 开始说话
  func startSpeak() {
    DispatchQueue.main.async {
      self.speakView.isHidden = false
      self.micImage.isHidden = false
      self.speakImage.isHidden = true
      self.cutDown.isHidden = true
      self.speakView.bringSubview(toFront: self.micImage)
      self.micImage.startAnimating()
    }
  }
  // 停止说话
  func stopSpeak() {
    DispatchQueue.main.async {
      self.micImage.isHidden = true
      if self.myTurnToSay {
        //需要显示倒计时
        self.speakImage.isHidden = false
        self.cutDown.isHidden = false
      }else{
        self.speakView.isHidden = true
      }
      self.micImage.stopAnimating()
    }
  }
  
  override func awakeFromNib() {
    backgroundColor = CustomColor.userBackColor
    addTapGesture(self, handler: #selector(PlayerView.didTaped))
    layer.borderWidth = 1
    layer.borderColor = UIColor.hexColor("3d0086").cgColor
    self.layer.cornerRadius = 3
    addSubview(avaBg)
    addSubview(nameLabel)
    addSubview(iconImage)
    addSubview(speakView)
    speakView.isHidden = true
    bringSubview(toFront: speakView)
  }
  //开始倒计时动画
  func startCutDown( _ time:Int) {
    myTurnToSay = true
    cutDown.start(beginingValue: time)
  }
  
  func stopCutDown() {
    myTurnToSay = false
    stopSpeak()
  }
  
  func didTaped() {
    if didTappedBlock != nil {
      
      if CurrentUser.shareInstance.currentRoomType == "simple" || CurrentUser.shareInstance.currentRoomType == "pre_simple" {
        print("\(self.tag)   \(self.tag != 11)")
        if self.tag != 11 && self.tag != 12 {
          didTappedBlock!(self)
        }
      } else {
        didTappedBlock!(self)
      }
    }
  }
  
  func setPrepare() {
    isDeath = false
    //删除死亡明牌
    for view in self.subviews {
      if view.isKind(of: RoleLable.self) {
        view.removeFromSuperview()
      }
      if view.isKind(of: RoleAfterDeathLable.self) {
        view.removeFromSuperview()
      }
    }
    deadImage.removeFromSuperview()
    wolfImageView.removeFromSuperview()
    wolfKingImageview.removeFromSuperview()
    loveImageView.removeFromSuperview()
    candidateImageView.removeFromSuperview()
    sheriffImageView.removeFromSuperview()
  }
  
  override func layoutSubviews() {
    
    let iconFrame = CGRect.init(x: 4, y: 4, width: self.frame.size.width - 8, height: self.frame.size.width - 8)
    let numberLabelW = iconFrame.width * consts.numberScale
    let numberLabelH = numberLabelW//numberLabelW * 1.3

    // 头像
    iconImage.frame = iconFrame
    iconImage.layer.cornerRadius = iconImage.width * 0.5
 
    //头像背景
    let avaFrame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height:self.frame.size.height)//iconImage.maxY+4
    avaBg.frame = avaFrame
    self.sendSubview(toBack: avaBg)
    
    //编号背景
    let playerPoint = CGRect.init(x:-4, y:-4, width: numberLabelW, height: numberLabelH)
    let superRect = self.convert(playerPoint, to: self.superview)
    numberBg.frame = superRect
    numberBg.layer.cornerRadius = numberLabelW/2
    // 编号
    numberLabel.frame = CGRect.init(x:0, y:0, width: numberLabelW, height: numberLabelH)
    numberLabel.adjustsFontSizeToFitWidth = true
    numberBg.addSubview(numberLabel)
    numberLabel.layer.cornerRadius = numberLabelW/2
    //准备状态，根据编号显示的位置不同
    let statusLabelOffset = 6.0
    var statusLabelH = 10.0
    if UIScreen.main.bounds.width > 320 {
        statusLabelH = 12.0
    } else {
        statusLabelH = 10.0
    }
    var statusLabelFrame = CGRect()
    var masterLabelFrame = CGRect()
    print("\(playerData["position"].intValue)")
    let masterlableWidth = 25.0
    if playerData["position"].intValue < 6 {
        statusLabelFrame = CGRect.init(x: Double(self.width)  - (Double(iconFrame.width) - statusLabelOffset * 2 - 7), y: Double(iconImage.maxY) - Double(statusLabelH), width: Double(iconFrame.width) - statusLabelOffset * 2 - 7, height: statusLabelH)
        masterLabelFrame = CGRect.init(x:Double(self.width)  - (masterlableWidth), y: Double(iconImage.maxY) - Double(statusLabelH), width: masterlableWidth, height: statusLabelH)
    }else{
         statusLabelFrame = CGRect.init(x:0, y: Double(iconImage.maxY) - Double(statusLabelH), width: Double(iconFrame.width) - statusLabelOffset * 2 - 7, height: statusLabelH)
         masterLabelFrame = CGRect.init(x:0, y: Double(iconImage.maxY) - Double(statusLabelH), width: masterlableWidth, height: statusLabelH)
    }
    //等待发言
    var waitForSpeeachRect = CGRect()
    waitForSpeeachRect.origin = CGPoint(x: -3, y: iconImage.center.y)
    waitForSpeeachRect.size = CGSize(width: 6, height: 6)
    let waitForSpeeachRectSuper = self.convert(waitForSpeeachRect, to: self.superview)
    waitForSpeeachPoint.frame = waitForSpeeachRectSuper
    if Screen.width > 320 {
      // 状态
      statusLabel.frame = statusLabelFrame
      // 房主
      masterLabel.frame = masterLabelFrame
    }else{
      statusLabel.frame = statusLabelFrame
      // 房主
      masterLabel.frame = statusLabelFrame
      
    }
    let bedgeWH = self.width / 3.0 - 2
    let badgeY = iconImage.maxY - bedgeWH + 2
    
    //显示自己身份
    let rect = CGRect(x:2, y:badgeY, width: bedgeWH, height: bedgeWH)
    RoleIdenfity.frame = rect
    RoleIdenfity.layer.cornerRadius = 3
    //死后显示身份
    if showRoleAfterDeath.text != nil {
      if (showRoleAfterDeath.text?.length)! > 2 {
        showRoleAfterDeath.frame = statusLabelFrame//CGRect.init(x: 0, y: 0, width: 0, height: 0)
      }else{
        showRoleAfterDeath.frame = masterLabelFrame//CGRect.init(x: 0, y: 0, width: 0, height: 0)
      }
    }
    // 情侣
    loveImageView.frame = CGRect.init(x: RoleIdenfity.maxX + 2 , y: badgeY, width: bedgeWH, height: bedgeWH)
    
    let candidate = CGRect.init(x: loveImageView.maxX + 2, y: iconImage.maxY - 1.5*bedgeWH + 2, width: 1.5*bedgeWH, height: 1.5*bedgeWH)
    let candidateRectSuper = self.convert(candidate, to: self.superview)
    // 竞选者
    candidateImageView.frame = candidateRectSuper
    
    let sheriffFrame = CGRect.init(x: loveImageView.maxX + 2, y: badgeY, width: bedgeWH, height: bedgeWH)
    // 警长
    sheriffImageView.frame = sheriffFrame
    // 死亡
    deadImage.frame = bounds
    for v in deadImage.subviews {
      if v.isKind(of:UIImageView.self) {
        v.frame = CGRect.init(x: 0, y: 0, width:iconFrame.width - 3, height: iconFrame.height - 3)
        v.center = deadImage.center
      }
    }
    // 锁
    lockImage.frame = bounds
    
    // 名字
    //倒计时动画
    speakView.frame = CGRect.init(x: 0, y: 0, width:self.frame.size.width, height: self.frame.size.height)
    speakImage.frame = CGRect.init(x: 0, y: 0, width: iconImage.width - 5, height:iconImage.height - 5)
    cutDown.frame =  CGRect.init(x: 0, y: 0, width: iconImage.width - 5, height:iconImage.height - 5)
    // mic
    micImage.frame = CGRect.init(x: 0, y: 0, width: iconImage.width, height: iconImage.height)
    micImage.center = iconImage.center
    speakImage.center = iconImage.center
    cutDown.center = iconImage.center
    nameLabel.frame = CGRect.init(x: 0, y: iconImage.maxY + 4, width: width, height: height - iconImage.maxY - 4)
    nameLabel.backgroundColor = UIColor.hexColor("9e73ff")
    let maskpath = UIBezierPath(roundedRect: nameLabel.bounds, byRoundingCorners:[.bottomLeft,.bottomRight], cornerRadii:CGSize(width: 3, height: 3))
    let shaplayer = CAShapeLayer()
    shaplayer.frame = nameLabel.bounds
    shaplayer.path = maskpath.cgPath
    nameLabel.layer.mask = shaplayer
    bringSubview(toFront: speakView)
  }
}
