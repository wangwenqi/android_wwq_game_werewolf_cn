//
//  rebateAnimationView.swift
//  game_werewolf
//
//  Created by Hang on 2017/12/20.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import YYText

class rebateAnimationView: SpringView {
    @IBOutlet weak var firstNumber: UIImageView!
    @IBOutlet weak var secNumber: UIImageView!
    @IBOutlet weak var titleView: UIView!
    
  var text:String = "" {
        didSet{
            let attText = NSMutableAttributedString.init(string: text)
            let newlabel = UILabel()
            newlabel.frame = CGRect.init(x: 0, y: 0, width:titleView.size.width, height:25)
            newlabel.textColor = UIColor.white
            newlabel.textAlignment = .left
            let attributes = [NSStrokeWidthAttributeName: -4.0,
                            NSStrokeColorAttributeName: UIColor.hexColor("7C46F1"),
                            NSForegroundColorAttributeName:UIColor.white,
                            NSFontAttributeName:UIFont.init(name: "Futura", size: 20)] as [String : Any]
            attText.addAttributes(attributes, range:NSRange.init(location: 0, length: text.length))
            newlabel.attributedText = attText
            titleView.addSubview(newlabel)
      }
    }
    func animateView() {
      animation = "slideDown"
      animateNext {
        self.removeFromSuperview()
      }
    }
  
  deinit {
    print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
  }
}
