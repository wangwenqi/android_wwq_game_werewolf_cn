//
//  GameRoomBottomView.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/13.
//  Copyright © 2017年 orangelab. All rights reserved.
//

protocol showFriendListTosend:class{
  func showFriendList()
}

import UIKit

class GameRoomBottomView: SpringView {
  
  var timer = Timer()
  var remainTime = 35
  
  var canUseKeyboard = true;
  
  var startLastFiveCountDown: ((Int) -> Void)?
  var stopLastFiveCountDown: (() -> Void)?
  var didInNavagationModel = false
  
  @IBOutlet weak var controlButton: UIButton!
  @IBOutlet weak var speechButton: UIButton!
  @IBOutlet weak var speechTextField: UITextField!
    @IBOutlet weak var giftPaddingRight: NSLayoutConstraint!
    
    @IBOutlet weak var keyboardPaddingLeft: NSLayoutConstraint!
    var startSpeechBlock: (() -> Void)?
  var stopSpeechBlock: (() -> Void)?
  var currrentSpeking:Int = -1
  var itIsMyTurn = false
  var freeSpeach = true
  weak var delegate:showFriendListTosend?
  class func bottomView(frame: CGRect) -> GameRoomBottomView {
    let view = Bundle.main.loadNibNamed("GameRoomBottomView", owner: nil, options: nil)?.last as! GameRoomBottomView
    view.frame = frame
    return view
  }
  
  @IBAction func controlButtonClicked(_ sender: Any) {
    
    if(!canUseKeyboard) {
      return;
    }
    
    controlButton.isSelected = !controlButton.isSelected
    speechButton.isHidden = controlButton.isSelected
    speechTextField.isHidden = !controlButton.isSelected
    
    if controlButton.isSelected {
      // 打字
      controlButton.imageView?.contentMode = .scaleAspectFit
      speechTextField.becomeFirstResponder()
    } else {
      // 语音
      speechTextField.resignFirstResponder()
    }
  }
    @IBAction func sendGift(_ sender: Any) {
        self.delegate?.showFriendList()
    }
    
  override func awakeFromNib() {
    super.awakeFromNib()
    NotificationCenter.default.addObserver(self, selector: #selector(GameRoomBottomView.keyBoardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(GameRoomBottomView.keyBoardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(GameRoomBottomView.enterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil);
    
    speechButton.imageView?.contentMode = .scaleAspectFit
    speechButton.imageEdgeInsets = UIEdgeInsetsMake(10, 0, 10,0)
    speechButton.titleEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
    
    speechButton.addTarget(self, action: #selector(GameRoomBottomView.speechTouchDown), for: .touchDown)
    speechButton.addTarget(self, action: #selector(GameRoomBottomView.speechTouchUp), for: .touchUpInside)
    speechButton.addTarget(self, action: #selector(GameRoomBottomView.speechTouchUp), for: .touchUpOutside)
    speechButton.addTarget(self, action: #selector(GameRoomBottomView.speechTouchUp), for: .touchDragExit)
    speechButton.addTarget(self, action: #selector(GameRoomBottomView.speechTouchUp), for: .touchCancel)
    
    speechTextField.delegate = self
    speechTextField.returnKeyType = .send
    
    let str = NSLocalizedString("输入你要说的话", comment: "")
    
    let attStr = str.highLightedStringWith(str, color:UIColor.hexColor("9e73ff"))
    speechTextField.attributedPlaceholder = attStr
    speechTextField.backgroundColor = UIColor.hexColor("3e1a5b").withAlphaComponent(0.80)
    //padding left
    if Screen.size.width > 375 {
       keyboardPaddingLeft.constant = 25
        giftPaddingRight.constant = 35
    }else if Screen.size.width == 375{
       keyboardPaddingLeft.constant = 20
       giftPaddingRight.constant = 20
    }else {
       keyboardPaddingLeft.constant = 10
        giftPaddingRight.constant = 10
    }
  }
  
  // 开始倒计时
  func startCountDown() {
    
    self.timer.invalidate()
    
    self.timer = Timer.init(timeInterval: 1.0, target: self, selector: #selector(GameRoomBottomView.tAction), userInfo: nil, repeats: true)
    RunLoop.current.add(self.timer, forMode: RunLoopMode.commonModes)
    
  }
  
  final func tAction() {
    if remainTime <= 5 && remainTime > 0 {
      if startLastFiveCountDown != nil {
        startLastFiveCountDown!(remainTime)
      }
    } else if remainTime <= 0 {
      timer.invalidate()
      remainTime = 36
      // 结束发言
      if isPlaying {
        if itIsMyTurn {
          XBHHUD.showError(NSLocalizedString("由于您长时间未说话，已经自动过麦", comment: ""))
          messageDispatchManager.sendMessage(type: .end_speech);
          if stopLastFiveCountDown != nil {
            stopLastFiveCountDown!()
          }
        }else{
          stopCountDown()
        }
        if freeSpeach == true {
          XBHHUD.showError(NSLocalizedString("由于您长时间未说话，已经自动过麦", comment: ""))
          messageDispatchManager.sendMessage(type: .end_speech);
          if stopLastFiveCountDown != nil {
            stopLastFiveCountDown!()
          }
        }
      }
    }
    remainTime -= 1
    print(" timer: \(remainTime)")
  }
  
  // 停止倒计时
  func stopCountDown() {
    remainTime = 35
    timer.invalidate()
    if stopLastFiveCountDown != nil {
      stopLastFiveCountDown!()
    }
  }
  
  func setCanNotFreeTalk() {
    speechButton.isUserInteractionEnabled = false
    speechButton.isEnabled = false
    speechButton.setTitle(NSLocalizedString("   还没轮到你说话", comment: ""), for: .disabled)
//    speechButton.addCustomHighlightLayer();
  }
  
  func setMute() {
    speechButton.isUserInteractionEnabled = false
    speechButton.isEnabled = false
    speechButton.setTitle(NSLocalizedString("   你已被禁言", comment: ""), for: .disabled)
//    speechButton.addCustomHighlightLayer();
  }
  
  func setUnMute() {
    speechButton.isUserInteractionEnabled = true
    speechButton.isEnabled = true
    speechButton.setTitle(NSLocalizedString("   按住说话", comment: ""), for: .normal)
//    speechButton.removeCustomHighlightLayer();
  }
  
  func speechTouchDown() {
    //如果在观战位置上不能说话
    if CurrentUser.shareInstance.currentPosition >= 13 || CurrentUser.shareInstance.currentPosition == -1  {
      return
    }
    remainTime = 35
    if stopLastFiveCountDown != nil {
      stopLastFiveCountDown!()
    }
    messageDispatchManager.sendMessage(type: .speak);
    if startSpeechBlock != nil {
      startSpeechBlock!()
    }
    stopCountDown()
//    speechButton.setButtonSoftShadowColor(color: UIColor.clear);
//    speechButton.addCustomHighlightLayer();
  }
  
  func speechTouchUp() {
    if CurrentUser.shareInstance.currentPosition >= 13 || CurrentUser.shareInstance.currentPosition == -1  {
      return
    }
    //不是自己不响应按下
    messageDispatchManager.sendMessage(type: .unspeak);
    if stopSpeechBlock != nil {
      stopSpeechBlock!()
    }
//    speechButton.setButtonSoftShadowColor(color: speechButton.buttonShdowColor);
    if self.freeSpeach == false {
      if isPlaying && itIsMyTurn == false{
        return
      }
    }
//    speechButton.removeCustomHighlightLayer();
  }
  
  func dealWithLongTouch() {
    messageDispatchManager.sendMessage(type: .unspeak);
    if stopSpeechBlock != nil {
      stopSpeechBlock!()
    }
  }
  
  func enterBackground() {
    self.speechTouchUp();
  }
  
  func keyBoardWillShow(notification: Notification) {
    let userInfo = notification.userInfo
    let keyBoardBounds = (userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    if didInNavagationModel {
      self.frame.origin.y = keyBoardBounds.origin.y - self.frame.height - 20
    }else{
      self.frame.origin.y = keyBoardBounds.origin.y - self.frame.height
    }
  }
  
  func keyBoardWillHide(notification: Notification) {
    let userInfo = notification.userInfo
    let keyBoardBounds = (userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    if didInNavagationModel {
      self.frame.origin.y = keyBoardBounds.origin.y - self.frame.height - 20 - kSafeAreaBottomHeight
    }else{
      self.frame.origin.y = keyBoardBounds.origin.y - self.frame.height - kSafeAreaBottomHeight
    }
    controlButton.isSelected = false;
    speechButton.isHidden = false;
    speechTextField.isHidden = true;
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self);
    timer.invalidate()
  }
}


extension GameRoomBottomView: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    if textField.text?.isEmpty ?? true {
      return true
    }
    //发送字数限制
    if let count = textField.text?.length {
      if count > 200 {
        XBHHUD.showError("超过最大字数限制")
        return true
      }
    }
    
    remainTime = 35
    
    if stopLastFiveCountDown != nil {
      stopLastFiveCountDown!()
    }
    MobClick.event("Game_sendText")
    messageDispatchManager.sendMessage(type: .chat, payLoad: ["position": CurrentUser.shareInstance.currentPosition - 1, "message": textField.text ?? ""]);
    textField.text = ""
    self.controlButtonClicked(controlButton);
    return true
  }
  
}
