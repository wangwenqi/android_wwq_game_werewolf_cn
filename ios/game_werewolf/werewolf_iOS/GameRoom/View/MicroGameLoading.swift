//
//  MicroGameLoading.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/24.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

class MicroGameLoading: SpringView {
    
    @IBOutlet weak var vsImage: UIImageView!
    @IBOutlet weak var playView2: GamePlayerView!
    @IBOutlet weak var playView1: GamePlayerView!
    @IBOutlet weak var timer: UILabel!
    @IBOutlet weak var gameDescribe: UILabel!
    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var goBackButton: UIButton!
    @IBOutlet weak var searchingImage: UIImageView!
    @IBOutlet weak var searchingTitle: UILabel!
    @IBOutlet weak var thirdCircleImageView: UIImageView!
    @IBOutlet weak var secondCircleImageView: UIImageView!
    @IBOutlet weak var firstCircleImageView: UIImageView!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var coutDownBg: UIView!
    @IBOutlet weak var randomUsers: UIImageView!
    
    var clockTimer:Timer?
    var currentTime = 0
    var couldAnimationTime = 0
    var isPVP = false {
      didSet{
        setUpGameData()
      }
    }
    var webLoaded = false
    var canNotLeave = false {
     didSet{
       if canNotLeave {
         goBackButton.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 6, execute: { [weak self] in
          self?.canNotLeave = false
        })
       }else{
         goBackButton.isHidden = false
        }
      }
     }
    var goBack:(()-> Void)?
    @IBAction func goBack(_ sender: Any) {
       messageDispatchManager.sendMicroGameMessage(type:"leave", payLoad: ["leave":true])
      if goBack != nil {
        goBack!()
      }
    }
    //进入房间的内容
    var data = JSON.init(parseJSON: "") {
      didSet{
        dataAnalyse()
      }
    }
   //随即头像
   var random_iamge:[JSON]?
  
  //加载游戏状态
    var status:MicroGameLoadingStatus = .none {
      didSet{
        if status == .loadSuccess {
          webViewdidFinishLoad()
        }
      }
    }
  
    //游戏信息
    var gameInfo:JSON? {
      didSet{
        if let icon = gameInfo?["mini_game"]["icon"].string {
            gameImage.sd_setImage(with: URL.init(string:icon))
            DispatchQueue.main.async {
                self.startCircleAnimation()
            }
        }
      }
    }
  
    override func awakeFromNib() {
      //开启计时器
      currentTime = 0
      clockTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
      goBackButton.imageView?.contentMode = .scaleAspectFit
      coutDownBg.backgroundColor = UIColor.white.withAlphaComponent(0.6)
      coutDownBg.layer.cornerRadius = Screen.width * 0.09
      randomUsers.layer.cornerRadius = Screen.width * 0.105
      randomUsers.layer.masksToBounds = true
      randomUsers.contentMode = .scaleAspectFill
      //自己的view
      player1()
    }
    
    func startCircleAnimation() {
        rotate(imageView: thirdCircleImageView, aCircleTime: 9)
        rotate(imageView: secondCircleImageView, aCircleTime: 8)
        rotate(imageView: firstCircleImageView, aCircleTime: 7)
    }
  
    func setUpGameData() {
      if let icon = CurrentUser.shareInstance.microGame?["icon"]?.string{
        gameImage.sd_setImage(with: URL.init(string:icon))
        DispatchQueue.main.async {
          self.startCircleAnimation()
        }
      }
    }

    func updateTime() {
      //web加载完，并且不是pvp，就显示匹配动画
      if webLoaded && isPVP == false && status == .loadSuccess   {
        if let view = self.viewWithTag(3) as? UIImageView {
          if couldAnimationTime == 0 {
            couldAnimationTime = currentTime
          }
          if (currentTime-couldAnimationTime)%4 == 0 {
            let total = self.random_iamge?.count ?? 0
            if total != 0 {
              let index = ((currentTime-couldAnimationTime)/4)%total
              if let image = self.random_iamge?[index].string {
                if let iconUrl = URL.init(string:image) {
                    view.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
                }
              }
            }
            let newCenter = randomPointOnCircle(radius: Float(Screen.width*0.39), center: circleView.center)
            view.center = newCenter
            animationShow()
          }
        }
      }
      currentTime = currentTime + 1
      timer.text = "\(currentTime)s"
    }
    //显示
    func animationShow() {
      if let view = self.viewWithTag(3) as? UIImageView {
          UIView.animate(withDuration: 2, animations: {
              view.alpha = 1
          }, completion: { (finished) in
              if finished {
                  self.animationHiden()
              }
          })
      }
    }
    //隐藏
    func animationHiden() {
      if status == .searchSuccess {
        return
      }
      if let view = self.viewWithTag(3) as? UIImageView {
          UIView.animate(withDuration: 2, animations: {
              view.alpha = 0
          }, completion: { (finished) in
    
          })
      }
    }
    //显示自己的位置
    func showOpopWithAnimation() {
      if let view:GamePlayerView = self.viewWithTag(2) as? GamePlayerView {
        view.alpha = 1
      }
    }
    //获取随机位置
    func randomPointOnCircle(radius:Float, center:CGPoint) -> CGPoint {
        // Random angle in [0, 2*pi]
        let theta = Float(arc4random_uniform(UInt32.max))/Float(UInt32.max-1) * Float.pi * 2.0
        // Convert polar to cartesian
        let x = radius * cos(theta)
        let y = radius * sin(theta)
        let finalPoint = CGPoint(x: CGFloat(x)+center.x, y: CGFloat(y)+center.y)
        let width = Screen.width * 0.21
        let origin = playView1.frame.origin
        let newOrigin = CGPoint(x: origin.x - (width/2), y: origin.y - (width/2))
        let saftRect = CGRect.init(origin:newOrigin, size:CGSize(width:width*1.5, height: width*1.5))
        if saftRect.contains(finalPoint) {
            return randomPointOnCircle(radius: radius, center: center)
        }
        return finalPoint
    }
    //转动动画
    func rotate(imageView: UIImageView, aCircleTime: Double) { //CABasicAnimation
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = -Double.pi * 2 //Minus can be Direction
        rotationAnimation.duration = aCircleTime
        rotationAnimation.repeatCount = .infinity
        imageView.layer.add(rotationAnimation, forKey: nil)
    }
    // 进入socket时，获取的数据
    func dataAnalyse() {
      //游戏玩家
      for (key, value) in data["room"]["users"].dictionaryValue {
        if let posion = Int(key) {
          if posion <= 1 {
            if let userid = value["id"].string {
              if CurrentUser.shareInstance.id == userid {
                let view:GamePlayerView = self.viewWithTag(1)! as! GamePlayerView
                view.userData = value
              }else{
                let view:GamePlayerView = self.viewWithTag(2)! as! GamePlayerView
                view.userData = value
              }
            }
          }
        }
      }
      //如果是2人接进入游戏
     if data["room"]["users"].dictionary?.keys.count == 2 {
        if isPVP {
           playerAnimation()
        }else{
           status = .searchSuccess
        }
        //游戏名字
        canNotLeave = true
        gameDescribe.text = NSLocalizedString("即将进入", comment: "") + (CurrentUser.shareInstance.microGame?["name"]?.stringValue)!
      }
    }
    //初始化自己的view
    func player1() {
      let view:GamePlayerView = self.viewWithTag(1)! as! GamePlayerView
      view.userImage.image = UIImage.init(named: "room_head_default")
      if let iconUrl = URL.init(string:CurrentUser.shareInstance.avatar) {
        view.userImage.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
      }
    }
    //对战模式下加载对手信息
    func loadOppo(_ data:[String:JSON]) {
      let view:GamePlayerView = self.viewWithTag(2)! as! GamePlayerView
      view.userImage.image = UIImage.init(named: "room_head_default")
      //先显示对手的头像
      if let urlString = data["avatar"]?.string {
        if let iconUrl = URL.init(string:urlString) {
          view.userImage.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
        }
      }
      showOpopWithAnimation()
    }
    //第二个对手找到
    func player2Join(_ data:JSON) {
      canNotLeave = true
      status = .searchSuccess
      if let position = data["position"].int {
        if position == 1 {
          let view:GamePlayerView = self.viewWithTag(2) as! GamePlayerView
          view.userData = data
          //即将进入游戏
          gameDescribe.text = NSLocalizedString("即将进入", comment: "") + (CurrentUser.shareInstance.microGame?["name"]?.stringValue)!
          //然后做动画,只有pvp模式下才会有动画
          if isPVP {
             playerAnimation()
          }else{
            //这个时候，真正的人是真正的对手
            randomUsers.isHidden = true
            showOpopWithAnimation()
          }
        }
      }
    }
    //web加载成功
    func webViewdidFinishLoad() {
      webLoaded = true
      //如果是在PVP模式下，不要随机匹配动画
      if isPVP == false {
        if let view:GamePlayerView = self.viewWithTag(2) as? GamePlayerView {
          if let image = random_iamge?[0].string {
            view.changeAva(image)
          }
        }
      }
    }
    //游戏匹配中
    func searchingPlayer() {
      self.searchingTitle.text = NSLocalizedString("速配中", comment: "")
      if isPVP {
          self.gameDescribe.text = NSLocalizedString("等待对手加入", comment: "")
      }else{
          self.gameDescribe.text = NSLocalizedString("速配中", comment: "")
      }
    }
    //游戏匹配完成动画
    func playerAnimation() {
    }
}
