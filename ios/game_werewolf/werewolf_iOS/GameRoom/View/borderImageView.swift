//
//  borderImageView.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/20.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

class borderImageView: UIImageView {
  
  let borderWidth: CGFloat = 2
  
  let startAngle = CGFloat(0)
  let middleAngle = CGFloat(M_PI)
  let endAngle = CGFloat(2 * M_PI)
  
  var primaryColor = UIColor.red
  var secondaryColor = UIColor.blue
  var currentStrokeValue = CGFloat(0)
  
  private weak var borderLayer: CAShapeLayer?
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    var center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
    var radius = CGFloat(self.frame.width / 2 - 1)
    // create path
    let width = min(bounds.width, bounds.height)
    let path = UIBezierPath(arcCenter: CGPoint(x: bounds.midX, y: bounds.midY), radius: width / 2, startAngle: 0, endAngle: .pi * 2, clockwise: true)
    
    // update mask and save for future reference
    
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    layer.mask = mask
    
    // create border layer

    let path1 = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle, endAngle: middleAngle, clockwise: true)
    
    let path2 = UIBezierPath(arcCenter: center, radius: radius, startAngle: middleAngle, endAngle: endAngle, clockwise: true)
    
    let frameLayer = CAShapeLayer()
    frameLayer.path = path1.cgPath
    frameLayer.lineWidth = 3.0
    frameLayer.strokeColor = UIColor.hexColor("FD9026").cgColor
    frameLayer.fillColor = nil
    
    let frameLayer1 = CAShapeLayer()
    frameLayer1.path = path2.cgPath
    frameLayer1.lineWidth = 3.0
    frameLayer1.strokeColor = UIColor.white.cgColor
    frameLayer1.fillColor = nil
    
    // if we had previous border remove it, add new one, and save reference to new one
    
    borderLayer?.removeFromSuperlayer()
    layer.addSublayer(frameLayer)
    layer.addSublayer(frameLayer1)
    borderLayer = frameLayer
  }
}
