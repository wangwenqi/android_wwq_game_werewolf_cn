//
//  giftManager.swift
//  game_werewolf
//
//  Created by Hang on 2017/7/13.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

struct Gifts:Hashable {
  
  static func ==(lhs: Gifts, rhs: Gifts) -> Bool {
    return lhs.hashValue == rhs.hashValue
  }
  var hashValue: Int {
    get {
      return (typeString.hashValue)
    }
  }
 var fromView:UIView?
 var toView:UIView?
 var typeString:String
 var rebate:Int = -1
  
  func animationOn(view:UIView,config:NSDictionary,originview:UIView?) -> gift {
    if rebate != -1 {
      //说明有返利，要播放动画
      let to = toView!.superview?.convert(toView!.center, to: originview) ?? CGPoint(x: 0, y: 0)
      let rebateView = Bundle.main.loadNibNamed("rebateAnimation", owner: nil, options: nil)?.last as? rebateAnimationView
      rebateView?.frame  = CGRect.init(x: 0, y: 0, width: 35, height: 64)
      rebateView?.center = to
      rebateView?.text = "\(rebate)"
      originview?.addSubview(rebateView!)
      rebateView?.animateView()
    }
    let gg = gift.getAnimationImage(form: self.fromView!, to: self.toView!, type: self.typeString, view:view ,config: config)
    return gg
  }
}

enum currentRoomType {
  case GAME
  case AUDIO
}

class giftManager: UIView {
  
  var gifts:[Gifts] = [] {
    didSet{
      if gifts.count != 0 {
        if oldValue.count < gifts.count {
          if gifts.count > 0 {
            if isRuning == false {
              checkIfHasGifts()
            }
          }
        }
      }
    }
  }
//  weak var view:UIView?
  var view:UIView?
  var isAnimation:Bool = false
  var toName = ""
  var exstsCout = 0
  var current:gift?
  var isRuning = false
  var maxShow = 2
  var type:currentRoomType = .GAME
  var maxCheckTime = 0
  var giftData:JSON? = JSON.init(GiftSourceManager.getTotalConfig())

  override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
    return nil
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    NotificationCenter.default.addObserver(self, selector: #selector(updateGift), name:  NSNotification.Name(rawValue: "UPDATEGIFT"), object: nil)
    self.maxShowNum()
  }

  func maxShowNum() {
    let name = UIDevice.current.model
    let nameModel = UIDevice.current.modelName
    if nameModel.hasPrefix("iPhone") {
      //如果是真机
      let deviceName = nameModel.split(",").first
      let deviceNum = (deviceName as? NSString)?.substring(with: NSMakeRange(6,(deviceName?.length)! - 6)) as! String
      if let num = Int(deviceNum) {
        if num <= 7 {
          maxShow = 1
        }else{
          maxShow = 2
        }
      }else{
        maxShow = 1
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func willRemoveSubview(_ subview: UIView) {
    if subview.isKind(of:YYAnimatedImageView.self) {
      //要删除view,检查
      checkIfHasGifts()
    }
  }
  
  func updateGift() {
    giftData = JSON.init(GiftSourceManager.getTotalConfig())
  }

  deinit {
     NotificationCenter.default.removeObserver(self)
    print("======================gift manager deinit======================")
  }
  
  func add(_ gift:Gifts) {
    lock(obj: gifts as AnyObject, blk: {
      gifts.append(gift)
    })
  }
  
  func checkNum() -> Int {
    return self.subviews.count
  }
  
  func pop() {
     var probliem = false
     let t = checkNum()
     if t > maxShow {
      print("++++++\(t)展示的动画太多，先等一在重试一次")
      return
    }
    let cur = gifts.first
    if let giftArr = giftData?["gifts"].array {
      let finalGift = giftArr.first(where: { (gift) -> Bool in
        if let type = gift["gift_type"].string {
          if type == cur?.typeString {
            return true
          }else{
            return false
          }
        }else{
          return false
        }
      })
      if let animationConfig =  finalGift?["animate"].dictionaryObject {
         DispatchQueue.main.async {
          let gg = cur?.animationOn(view:self, config: animationConfig as NSDictionary,originview:self.view)
           self.addSubview(gg!)
           DispatchQueue.global().async {
              gg?.parserConfig(type: self.type)
           }
        }
      }else{
        //这个时候就是说明有问题，没有动画
        //检测一次吧
        if self.maxCheckTime <= 2 {
         GiftSourceManager.checkVersionWithoutLocalVersion()
          self.maxCheckTime = self.maxCheckTime + 1
        }
        probliem = true
      }
    }else{
      //这个时候就是说明有问题,没有找到对应的类型
        if self.maxCheckTime <= 2 {
          GiftSourceManager.checkVersionWithoutLocalVersion()
          self.maxCheckTime = self.maxCheckTime + 1
        }
        probliem = true
    }
    lock(obj: gifts as AnyObject, blk: {
      if gifts.count > 0{
         gifts.removeFirst()
      }
    })
    if probliem {
      //有问题，这个不会掉用最新的
      checkIfHasGifts()
    }
  }
  
  func clear() {
    lock(obj: gifts as AnyObject, blk: {
      gifts.removeAll()
    })
  }
  
  func lock(obj: AnyObject, blk:() -> ()) {
    objc_sync_enter(obj)
    blk()
    objc_sync_exit(obj)
  }
  
  func checkIfHasGifts() {
    print(NSLocalizedString("====================调用检查", comment: ""))
    if gifts.count > 0 {
        pop()
    }
  }
}

extension UIDevice {
  var modelName: String {
    var systemInfo = utsname()
    uname(&systemInfo)
    let machineMirror = Mirror(reflecting: systemInfo.machine)
    let identifier = machineMirror.children.reduce("") { identifier, element in
      guard let value = element.value as? Int8, value != 0 else { return identifier }
      return identifier + String(UnicodeScalar(UInt8(value)))
    }
    return identifier
  }
}
