//
//  giftCollectionViewCell.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/22.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class giftCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var flView: UIView!
    @IBOutlet weak var giftImage: UIImageView!
    @IBOutlet weak var giftName: UILabel!
    @IBOutlet weak var giftCost: UIButton!
    @IBOutlet weak var giftHot: UILabel!
    @IBOutlet weak var discountButton: UIButton!
    @IBOutlet weak var bg: UIImageView!
    var type:giftType?
    var typeString:String?
    var costType:String = "gold"
  
    func config() {
      //corner
      let maskpath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners:.allCorners, cornerRadii:CGSize(width: 10, height: 10))
      let shaplayer = CAShapeLayer()
      shaplayer.frame = self.bounds
      shaplayer.path = maskpath.cgPath
      self.layer.mask = shaplayer
      //border
      let borderLayer = CAShapeLayer()
      borderLayer.frame = self.bounds
      borderLayer.lineWidth = 1.5
      borderLayer.path =  maskpath.cgPath
      borderLayer.strokeColor = UIColor.hexColor("ff00ff").cgColor
      borderLayer.fillColor = UIColor.clear.cgColor
      
      self.layer.addSublayer(borderLayer)
    }
  
    func configWith(data:NSDictionary) {
      if let giftType = data["gift_type"] as? String,
        let url = data["image"] as? String,
        let price = (data["price"] as? NSArray)?.firstObject as? NSDictionary,
        let popular = data["popular"] as? Int{
        let paths = shareGift.shareInstance.getgetThumbnailImageOnly(type: giftType)
        if let imagePath = paths["gift_image"] as? String {
          if imagePath != "" {
             self.giftImage.image = UIImage(contentsOfFile: imagePath)
          }
        }
        if let name = paths["gift_name"] as? String {
          if name != "" {
            self.giftName.text = name
          }
        }
        self.typeString = data["gift_type"] as? String
      
        if let costType =  price["type"] as? String {
          if costType == "dim" {
            self.giftCost.setImage(UIImage(named:"room_gift_icon_diamond"), for: .normal)
            self.giftCost.setTitleColor(UIColor.hexColor("55AAF3"), for: .normal)
            self.costType = "dim"
          }else if costType == "gold" {
            self.giftCost.setImage(UIImage(named:"gold"), for: .normal)
            self.giftCost.setTitleColor(UIColor.hexColor("D77D4A"), for: .normal)
            self.costType = "gold"
          }
          //价格
          if let count = price["count"] as? Int {
             self.giftCost.setTitle("\(count)", for: .normal)
          }
        }
        if popular > 0 {
          self.giftHot.text = NSLocalizedString("人气", comment: "") + "+\(popular)"
        }else{
          self.giftHot.text = NSLocalizedString("人气", comment: "") + "\(popular)"
        }
        //返回
        if let rebateMsg = data["rebateMsg"] as? String {
          self.discountButton.setTitle(rebateMsg, for: .normal)
          self.flView.isHidden = false
        }else{
          self.flView.isHidden = true
        }
      }
    }
  
    override func awakeFromNib() {
      self.discountButton.imageView?.contentMode = .scaleAspectFit
      self.bg.image = UIImage.init(named: "giftview_bg")
    }
}
