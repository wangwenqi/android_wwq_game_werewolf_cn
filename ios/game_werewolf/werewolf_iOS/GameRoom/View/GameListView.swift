//
//  GameListView.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/19.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

protocol SendGameInviteProtocol:class {
  func gameInvite(data:JSON)
}

class GameListView: SpringView {

   @IBOutlet weak var collectionView: UICollectionView!
   @IBOutlet weak var gameImageView: UIView!
   @IBOutlet weak var pangeControl: UIPageControl!
   @IBOutlet weak var gameListHeight: NSLayoutConstraint!
    
  @IBOutlet weak var shaowView: UIView!
  //MARK: variable
  weak var delegate:SendGameInviteProtocol?
  var data:[JSON]?
  var line:UIView = UIView()
  
  override func awakeFromNib() {
    self.collectionView.dataSource = self
    self.collectionView.delegate = self
    let newLayout = FlyHorizontalFlowLauyout()
    newLayout.minimumLineSpacing = 20
    newLayout.minimumInteritemSpacing = 20
    self.collectionView.collectionViewLayout = newLayout
//    if Utils.getCurrentDeviceType() == .iPhone4 || Utils.getCurrentDeviceType() == .iPhone5 {
//      self.gameListHeight.setMultiplier(multiplier: 0.7)
//    }
    let nib = UINib(nibName: "GameListCell", bundle: nil)
    self.collectionView.register(nib, forCellWithReuseIdentifier: "gameCell")
    //点击事件
    let tap = UITapGestureRecognizer()
    tap.addTarget(self, action: #selector(hiddenView))
    shaowView.addGestureRecognizer(tap)
    //gamelist添加划线
    line.backgroundColor = UIColor.hexColor("8161E1")
    line.frame = CGRect.init(x: 0, y: 0, width:Screen.width,height: 2)
    collectionView.addSubview(line)
    //pageconrol
    pangeControl.pageIndicatorTintColor = UIColor.hexColor("555555")
    
    NotificationCenter.default.addObserver(self, selector: #selector(handleOut), name: NSNotification.Name(rawValue: "giftviewout"), object: nil)
  }
  
  static func initGameView() -> GameListView{
    let game = Bundle.main.loadNibNamed("GameListView", owner: nil, options: nil)?.last as! GameListView
    game.frame = UIScreen.main.bounds
    return game
  }
  //获取游戏
  func getGame() {
    XBHHUD.showInView(collectionView)
    RequestManager().get(url: RequestUrls.gameList, success: {[weak self] (response) in
      //隐藏遮挡
      XBHHUD.hide()
      //处理数据，得到游戏类型
      self?.data = nil
      if let datas = response.dictionary {
        self?.data = datas["games"]?.array
        if self?.data != nil {
          let page = (Double((self?.data?.count)!)/Double(4))
          if page.rounded(.up) == 1 {
            self?.pangeControl.isHidden = true
          }else{
            self?.pangeControl.isHidden = false
            self?.pangeControl.numberOfPages = Int(page.rounded(.up))
            self?.line.frame = CGRect.init(x: 0, y: 0, width:Screen.width*CGFloat(page.rounded(.up)),height: 2)
          }
        }else{
          self?.pangeControl.isHidden = true
        }
      }else{
        self?.pangeControl.isHidden = true
      }
      DispatchQueue.main.async {
        self?.collectionView.reloadData()
      }
    }) {  [weak self] (code, message) in
      //出现错误，直接隐藏
      self?.hiddenView()
      XBHHUD.showError(message)
     }
  }
  
  //隐藏view
  func hiddenView() {
    self.animation = "fadeOut"
    self.animateNext {
      self.removeFromSuperview()
    }
  }
  
  func handleOut() {
    self.animation = "fadeOut"
    self.animateNext {
      self.removeFromSuperview()
    }
  }
  
  deinit {
    print(NSLocalizedString("小游戏列表界面deinit", comment: ""))
    NotificationCenter.default.removeObserver(self)
  }
}


//MARK: - collection

extension GameListView:UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if self.data == nil {
      return 0
    }
    return  (self.data?.count)!
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
      if let data = self.data?[indexPath.row] {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gameCell", for: indexPath) as! GameListCollectionViewCell
        cell.config(data)
        return cell
      }
       return  collectionView.dequeueReusableCell(withReuseIdentifier: "gameCell", for: indexPath) as! GameListCollectionViewCell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     let targetCell = collectionView.cellForItem(at: indexPath) as? GameListCollectionViewCell
     if let gameData = targetCell?.data {
      hiddenView()
      self.delegate?.gameInvite(data:gameData)
     }
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width:(Screen.width-103)/4, height:(self.collectionView.height-60))
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 40.0, left: 20.0, bottom: 10.0, right: 20.0)
  }
  
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    pangeControl.currentPage = Int(ceilf((Float(scrollView.contentOffset.x / Screen.width))))
  }
}

