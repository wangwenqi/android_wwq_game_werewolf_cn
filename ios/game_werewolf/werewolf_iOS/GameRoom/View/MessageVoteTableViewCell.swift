//
//  MessageVoteTableViewCell.swift
//  game_werewolf
//
//  Created by Hang on 2017/12/5.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
//import UITableView_FDTemplateLayoutCell

class MessageVoteTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  
  @IBOutlet weak var bg: UIView!
  @IBOutlet weak var voteDescribe: UILabel!
  @IBOutlet weak var title: UILabel!
  @IBOutlet weak var titleWidth: NSLayoutConstraint!
  
  var model = MessgeModel() {
    didSet {
        //对投票结果进行，特出处理
        if model.vote.isEmpty  == false {
          generateVoteResult(model.vote as! [String : Any])
        }
    }
  }
  
  func generateVoteResult(_ dic:[String:Any]) {
    for i in self.bg.subviews {
      i.removeFromSuperview()
    }
    var realLine = 0
    var lines = 0
    let total = dic.count
    var baseHeight:Double = 0
    let maxNumInOneLine = 4
    for (keys,values) in dic {
      if keys != "type" {
        let line = UIView()
        self.bg.addSubview(line)
        if realLine == total - 1 {
          //最后一个
          var lineNum:Double = 0
          if let arr = values as? JSON {
            lineNum = ceil(Double(arr.count)/Double(maxNumInOneLine))
          }
          line.snp.makeConstraints({ (make) in
            make.left.equalTo(self.bg).offset(10)
            make.top.equalTo(self.bg.snp.top).offset(10+baseHeight)
            make.width.equalTo(self.bg.snp.width)
            make.height.equalTo(30.0+25.0*(lineNum - 1))
            make.bottom.equalTo(self.bg.snp.bottom)
          })
          baseHeight = baseHeight + 30.0+20.0*(lineNum - 1)+5*(lineNum)

        }else {
          //判断是不是太多了，超过最大宽度
          var lineNum:Double = 0
          if let arr = values as? JSON {
            lineNum = ceil(Double(arr.count)/Double(4))
          }
          line.snp.makeConstraints({ (make) in
            make.left.equalTo(self.bg).offset(10)
            make.top.equalTo(self.bg.snp.top).offset(10+baseHeight+5) //lines*25
            make.width.equalTo(self.bg.snp.width)
            make.height.equalTo(20.0+25.0*(lineNum-1))
          })
          baseHeight = baseHeight + 30.0+20.0*(lineNum - 1)+5*(lineNum)
          let underLine = UIView()
          self.bg.addSubview(underLine)
          underLine.backgroundColor = UIColor.hexColor("5145FF")
          underLine.snp.makeConstraints({ (make) in
            make.left.equalTo(bg.snp.left).offset(4)
            make.right.equalTo(bg.snp.right).offset(2)
            make.centerX.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(line.snp.bottom).offset(5)
          })
        }
        let voted = UIButton()
        line.addSubview(voted)
        voted.snp.makeConstraints({ (make) in
          make.left.equalTo(0)
          make.top.equalTo(0)
          make.width.equalTo(20)
          make.height.equalTo(20)
        })
        if keys == "-1" {
          voted.setImage(UIImage.init(named: "弃票"), for: .normal)
        }else{
          voted.backgroundColor = CustomColor.roomPink
          voted.layer.cornerRadius = 10
          voted.layer.masksToBounds = true
          voted.titleLabel?.font = UIFont.systemFont(ofSize: 10)
          voted.setTitle("\(Int(keys)! + 1)", for: .normal)
        }
        //投票按钮
        let voteImage = UIImageView()
        line.addSubview(voteImage)
        voteImage.snp.makeConstraints({ (make) in
          make.left.equalTo(voted.snp.right).offset(5)
          make.top.equalTo(voted.snp.top).offset(0)
          make.width.equalTo(15)
          make.height.equalTo(15)
        })
        layoutIfNeeded()
        voteImage.image = UIImage.init(named:"消息投票小箭头")
        //投票人
        var peopleCount = 1
        if let arr = values as? JSON {
          for people in arr.arrayValue {
            let votedPeople = UILabel()
            line.addSubview(votedPeople)
            let xValue = (peopleCount-1)%4
            let lineNum:Double = ceil(Double(peopleCount)/Double(4))
            votedPeople.snp.makeConstraints({ (make) in
              make.left.equalTo(voteImage.snp.right).offset(5 + xValue * 25)
              make.top.equalTo(line.snp.top).offset((CGFloat(0.0+25.0*(lineNum-1))))//0.0+25.0*(lineNum-1)
              make.width.equalTo(20)
              make.height.equalTo(20)
            })
            peopleCount = peopleCount + 1
            let text = people.intValue + 1
            votedPeople.text = "\(text)"
            votedPeople.layer.cornerRadius = 10
            votedPeople.layer.masksToBounds = true
            votedPeople.backgroundColor = CustomColor.roomPink
            votedPeople.textAlignment = .center
            votedPeople.textColor = UIColor.white
            votedPeople.font = UIFont.systemFont(ofSize: 10)
          }
        }
        lines = lines + 1
      }else{
        print("******\(values)")
        if values as? String == "apply" {
          self.voteDescribe.text = NSLocalizedString("竞选警长", comment: "")
        }else{
          self.voteDescribe.text = NSLocalizedString("投票", comment:"")
        }
        if CurrentUser.shareInstance.isNight {
          self.voteDescribe.textColor = UIColor.white
        }else{
          self.voteDescribe.textColor = UIColor.hexColor("D5ABFF")
        }
      }
      realLine = realLine + 1
    }
  }
  
}
