//
//  giftCollectionViewHorizontalFlowLayout.swift
//  game_werewolf
//
//  Created by Hang on 2017/7/3.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class giftCollectionViewHorizontalFlowLayout: UICollectionViewFlowLayout {
  

  private var cellCount = 0
  private var boundsSize = CGSize(width: 0, height: 0)
  
  override func prepare() {
    super.prepare()
    cellCount = self.collectionView!.numberOfItems(inSection: 0)
    boundsSize = self.collectionView!.bounds.size
  }
  
  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    var allAttributes = [UICollectionViewLayoutAttributes]()
    for i in 0...cellCount - 1{
      let indexPath = NSIndexPath(row: i, section: 0)
    let attr = self.computeLayoutAttributesForCellAtIndexPath(indexPath: indexPath)
     allAttributes.append(attr)
    }
    return allAttributes
  }
  
  override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    return true
  }
  
  override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
      return self.computeLayoutAttributesForCellAtIndexPath(indexPath: indexPath as NSIndexPath)
  }
  

  
  
  func computeLayoutAttributesForCellAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes {
    let row = indexPath.row
    let bounds = self.collectionView!.bounds
    
    let verticalItemsCount = Int(floor(boundsSize.height / itemSize.height))
    let horizontalItemsCount = Int(floor(boundsSize.width / itemSize.width))
    let itemsPerPage = verticalItemsCount * horizontalItemsCount
    let columnPosition = row % horizontalItemsCount
    let rowPosition = (row/horizontalItemsCount)%verticalItemsCount
    let itemPage = Int(floor(Double(row)/Double(itemsPerPage)))
    
   let attr = UICollectionViewLayoutAttributes(forCellWith:indexPath as IndexPath)
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    frame.origin.x = CGFloat(itemPage) * bounds.size.width + CGFloat(columnPosition) * itemSize.width
    frame.origin.y = CGFloat(rowPosition) * itemSize.height
    frame.size = itemSize
    attr.frame = frame
    
    return attr
  }

}
