//
//  giftType2CollectionViewCell.swift
//  game_werewolf
//
//  Created by Hang on 2017/7/3.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class giftType2CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var num: levelLable!
  
    var type:giftType?
  
  func config() {
    //corner
    let maskpath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners:.allCorners, cornerRadii:CGSize(width: 10, height: 10))
    let shaplayer = CAShapeLayer()
    shaplayer.frame = self.bounds
    shaplayer.path = maskpath.cgPath
    self.layer.mask = shaplayer
    //border
    let borderLayer = CAShapeLayer()
    borderLayer.frame = self.bounds
    borderLayer.lineWidth = 1.5
    borderLayer.path =  maskpath.cgPath
    borderLayer.strokeColor = UIColor.hexColor("ff00ff").cgColor
    borderLayer.fillColor = UIColor.clear.cgColor
    
    self.layer.addSublayer(borderLayer)
  }
}
