//
//  GamePlayerView.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/24.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

class GamePlayerView: SpringView {
  var userImage:UIImageView = UIImageView()
  var currentPostion = -1
  var userData:JSON? {
    didSet{
      CurrentUser.shareInstance.isMaster = userData!["is_master"].boolValue
      userImage.image = UIImage.init(named: "room_head_default")
      if let iconUrl = URL.init(string: userData!["avatar"].stringValue) {
        userImage.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
      }else{
        //加入的人
        if let iconUrl = URL.init(string: userData!["user"]["avatar"].stringValue) {
          userImage.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
        }
      }
    }
  }
  
  override func awakeFromNib() {
    addSubview(userImage)
    self.layer.cornerRadius = Screen.width * 0.105
    self.layer.masksToBounds = true
    userImage.contentMode = .scaleAspectFill
  }
  
  func changeAva(_ url:String) {
    if userData == nil {
      if let iconUrl = URL.init(string:url) {
        userImage.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
      }
    }
  }
  
  override func layoutSubviews() {
    userImage.snp.makeConstraints { (make) in
      make.edges.equalTo(self)  
    }
  }
}
