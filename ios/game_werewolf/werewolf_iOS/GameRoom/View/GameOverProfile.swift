//
//  GameOverProfile.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/31.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

class GameOverProfile: BaseNoticeView {
  
  @IBOutlet weak var rematchTitle: UILabel!
  @IBOutlet weak var addFriendTitle: UILabel!
  @IBOutlet weak var reMatchButton: UIButton!
  @IBOutlet weak var addFriendButton: UIButton!
  @IBOutlet weak var avaImage: UIImageView!
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var describe: UILabel!
  @IBOutlet weak var reMatchView: UIView!
  @IBOutlet weak var addFriendView: UIView!
  @IBOutlet weak var rePlayView: UIView!
  @IBOutlet weak var rePlayButton: UIButton!
  @IBOutlet weak var rePlayTitle: UILabel!
  @IBAction func addFriendClicked(_ sender: Any) {
    if CurrentUser.shareInstance.realCurrentRoomID.isEmpty  {
      return
    }
    var userid = ""
    if let block = Utils.getCachedBLContent() {
        if let uid = userData?["id"].string  {
            userid = uid
            if block[userid] != nil {
                XBHHUD.showError("此人已经被你拉黑")
                return
            }
        }else{
            return
        }
    }
    RequestManager().post(url: RequestUrls.friendAdd, parameters: ["friend_id":userid], success: { (json) in
        XBHHUD.showTextOnly(text: NSLocalizedString("好友请求发送成功！", comment: ""))
    }) { (code, message) in
        if message == "" {
            XBHHUD.showTextOnly(text: NSLocalizedString("好友请求发送失败", comment: ""))
        } else {
            XBHHUD.showTextOnly(text: "\(message)")
        }
    }
  }
    
  @IBAction func reMatchClicked(_ sender: Any) {
    if CurrentUser.shareInstance.realCurrentRoomID.isEmpty  {
      Utils.hideNoticeView()
      return
    }
    if reMatchBlock != nil {
      if landScapeModel == false {
          Utils.hideNoticeView()
      }else{
          self.superview?.removeFromSuperview()
      }
      reMatchBlock!()
    }
  }
  
  @IBAction func rePlayClicked(_ sender: Any) {
    if CurrentUser.shareInstance.realCurrentRoomID.isEmpty  {
      return
    }
    let payload = ["replay":true]
    messageDispatchManager.sendMicroGameMessage(type:"replay", payLoad:payload)
  }
  
  @IBAction func closeView(_ sender: Any) {
    if landScapeModel == false {
      if leaveBlock != nil {
          Utils.hideNoticeView()
          leaveBlock!()
      }
    }else{
      if leaveBlock != nil {
        self.superview?.removeFromSuperview()
        leaveBlock!()
      }
    }
  }
  var leaveBlock:(()->Void)?
  var reMatchBlock:(()->Void)?
  var userData:JSON?
  //后台告诉我是不是可以
  var canReplay = true {
    didSet{
      if canReplay == false {
        rePlayButton.isEnabled = false
        rePlayTitle.textColor = UIColor.gray
      }
    }
  }
  //replay过期啦
  var isReplyInvilide = false
  //对手是不是离开了
  var isLeaved = false
  //自己已经发送再来一句
  var isSendReplay = false
  //自己是不是收到replay消息
  var isReciveReplay = false
  var userId = ""
  var isPvP = false
  var data:JSON? {
    didSet{
      var userid = ""
      if let users = data?["users"].dictionary {
        if users.count == 2 {
          if CurrentUser.shareInstance.currentPosition == 2 {
            if let id = users["0"]?["id"].string {
              userid = id
              userId = userid
              if currentUsers != nil {
                if currentUsers?["0"] == nil {
                  isLeaved = true
                  setUnReplay()
                }
              }
            }
          }else{
            if let id = users["1"]?["id"].string {
              userid = id
              userId = userid
              if currentUsers != nil {
                if currentUsers?["1"] == nil {
                  isLeaved = true
                  setUnReplay()
                }
              }
            }
          }
        }
      }
      if  userid != "" {
        //获取胜利人信息
        RequestManager().post(url: RequestUrls.userInfo,parameters:["user_id":userid], success: { [weak self] (json) in
          self?.addFriendButton.isEnabled = true
          self?.userData = json
          self?.name.text = json["name"].stringValue
          if let iconUrl = URL.init(string: json["image"].stringValue) {
            self?.avaImage.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
          }
          if let isFriend = json["is_friend"].string {
            self?.addFriendView.isHidden = isFriend == "false" ? false : true
          } else if let isFriend = json["is_friend"].bool {
            self?.addFriendView.isHidden = isFriend
          }
          //个性签名
          self?.describe.text = json["signature"].stringValue
          if json["signature"].stringValue.isEmpty || json["signature"].stringValue == "" {
            self?.describe.text = NSLocalizedString(NSLocalizedString("Ta很懒，没有留下签名", comment: ""), comment: "")
            self?.describe.textColor = UIColor.gray
          }
        }, error: { (code, message) in
          XBHHUD.showError(message)
        })
      }
    }
  }
  //当前所有用户
  var currentUsers:[String:JSON]?
  var landScapeModel = false {
    didSet{
      if landScapeModel {
        self.height = Screen.height*0.95
      }else{
        self.width = Screen.width*0.95
      }
      setCenter()
    }
  }
  
  static func generateGameOverView() -> GameOverProfile {
    let view = Bundle.main.loadNibNamed("GameOverProfile", owner: nil, options: nil)?.last as! GameOverProfile
    return view
  }
  override func awakeFromNib() {
    self.addFriendButton.isEnabled = false
    self.backgroundColor = UIColor.clear
    //设置文字大小
    if Utils.getCurrentDeviceType() == .iPhone5 || Utils.getCurrentDeviceType() == .iPhone4 {
      rePlayTitle.font = UIFont.systemFont(ofSize: 10)
      rematchTitle.font = UIFont.systemFont(ofSize: 10)
      addFriendTitle.font = UIFont.systemFont(ofSize: 10)
    }
    NotificationCenter.default.addObserver(self, selector: #selector(userLeave(notification:)), name: NSNotification.Name(rawValue:MICRO_GAME_USER_LEAVE), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(userReturn(notification:)), name: NSNotification.Name(rawValue:MICRO_GAME_USER_RETURN), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(userReplay(notification:)), name: NSNotification.Name(rawValue:MICRO_GAME_REPLAY), object: nil)
    setCenter()
  }
  
  func userReplay(notification:NSNotification) {
    if let dic = notification.userInfo as? [String: Any] {
      if let uid = dic["user_id"] as? String {
        if uid == CurrentUser.shareInstance.id {
          if isReciveReplay == false {
            //自己没收到在玩一次的邀请
            isSendReplay = true
            setSendReplay()
          }else{
            //都同意了
          }
        }else{
          //不是自己，说明是别人已经发了在玩一次
          if isSendReplay  == false {
             isReciveReplay = true
             setReciveReplay()
          }else{
            //都同意了
          }
        }
      }
    }
  }
  
  func userLeave(notification:NSNotification) {
    if let dic = notification.userInfo as? [String: Any] {
      if let uid = dic["user_id"] as? String {
        if uid == userData?["id"].stringValue {
            isLeaved = true
            setUnReplay()
        }
      }
    }
  }
  
  func userReturn(notification:NSNotification) {
    if let dic = notification.userInfo as? [String: Any] {
      if let uid = dic["user_id"] as? String {
        if uid == userData?["id"].stringValue {
          setReplay()
        }
      }
    }
  }

  override func layoutSubviews() {
    self.avaImage.layer.cornerRadius = avaImage.size.width/2
    self.avaImage.layer.masksToBounds = true
    if isPvP {
      reMatchView.isHidden = true
    }
  }
  
  override func timerAction(remainTime: Int) {
    if canReplay  {
      if isLeaved == false {
        if isSendReplay == false  && isReciveReplay == false {
          let local = NSLocalizedString("再来一局", comment:"")
          let remain = "(\(remainTime)s)"
          let total = "\(local)\(remain)"
          rePlayTitle.text = total
          if remainTime == 0 {
            isReplyInvilide = true
            setUnReplay()
          }
        }
      }
    }
  }
  
  func setUnReplay() {
    if canReplay == true {
      if isReplyInvilide == false {
         rePlayTitle.text = NSLocalizedString("已离线", comment:"")
      }else{
        if isLeaved {
          rePlayTitle.text = NSLocalizedString("已离线", comment:"")
        }else{
          rePlayTitle.text = NSLocalizedString("再来一局", comment:"")
        }
      }
      rePlayButton.isEnabled = false
      rePlayTitle.textColor = UIColor.gray
    }
  }
  
  func setSendReplay() {
    rePlayButton.isEnabled = true
    rePlayTitle.text = NSLocalizedString("请求已发送", comment:"")
    rePlayTitle.textColor = UIColor.black
  }
  
  func setReciveReplay() {
    rePlayButton.isEnabled = true
    rePlayTitle.text = NSLocalizedString("对方已准备", comment:"")
    rePlayTitle.textColor = UIColor.black
  }
  
  func setReplay() {
    if canReplay == true {
      if isReplyInvilide == false {
        rePlayButton.isEnabled = true
        rePlayTitle.text = NSLocalizedString("再来一局", comment:"")
        rePlayTitle.textColor = UIColor.black
      }
    }
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
}
