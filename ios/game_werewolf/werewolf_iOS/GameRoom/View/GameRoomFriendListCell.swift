//
//  GameRoomFriendListCell.swift
//  game_werewolf
//
//  Created by QiaoYijie on 2017/5/24.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class GameRoomFriendListCell: UITableViewCell {
  
  var model = FriendListModel() {
    didSet {
      if let level = model.userData["level"].int {
        levelLabel.text = "LV.\(level)"
      } else {
        levelLabel.text = "LV.\(model.userData["game"]["level"])"
      }
      nameLabel.text = "\(model.userData["name"])"
      if let imageurl = model.userData["avatar"].string {
        let url = URL(string: imageurl)
        iconImage.sd_setImage(with: url, placeholderImage: UIImage.init(named: "room_head_default"))
      } else {
         let url = URL(string: model.userData["image"].stringValue)
        iconImage.sd_setImage(with: url, placeholderImage: UIImage.init(named: "room_head_default"))
      }
      //border
      iconImage.layer.borderColor = UIColor.hexColor("6F2FFF").cgColor
      iconImage.layer.borderWidth = 1
      //显示seleted
      selectedButton.isHidden = false
      
      if model.userData["sex"].intValue == 1 {
        // 男
        sexImage.image = UIImage.init(named: "ic_male")
      } else {
        // 女
        sexImage.image = UIImage.init(named: "ic_female")
      }
      //用户状态
      self.roomNum.isHidden = true
      self.statusLable.isHidden = true
      self.roomIcon.isHidden = true
      self.status.isHidden = true
      if let status = model.userData["status"].string?.split(":").first {
//        game_status：0/1/2/3
//        0: 用户离线
//        1: 用户在线
//        2: 用户在线，并在012345房间，游戏未开始
//        3: 用户在线，并在012345房间，游戏进行中
//        4: 用户观战中，并在012345房间 TODO
        self.roomNum.isHidden = false
        self.statusLable.isHidden = false
        self.roomIcon.isHidden = false
        self.status.isHidden = false
        if let game_status = Int(status) {
          switch game_status {
          case 0:
            notOnline()
          case 1:
            online()
          case 2:
            onlineReady()
          case 3:
            onlineGame()
          default:
            online()
          }
        }else{
          notOnline()
        }
        //房间号
        if model.userData["status"].string!.split(":").count >= 3 {
          if let roomid = model.userData["status"].string?.split(":")[2] as? String{
            if roomid != "" {
              self.roomNum.text = roomid
            }
          }
        }
      }
      selectedButton.isSelected = model.isSelected
    }
  }
  
  @IBOutlet weak var iconImage: UIImageView!
  @IBOutlet weak var roomNum: UILabel!
  @IBOutlet weak var statusLable: UILabel!
  @IBOutlet weak var roomIcon: UIImageView!
  @IBOutlet weak var status: UIImageView!
  @IBOutlet weak var selectedButton: UIButton!
  @IBOutlet weak var levelLabel: XBHLabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var sexImage: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.iconImage.layer.cornerRadius = iconImage.frame.width * 0.5
    self.iconImage.clipsToBounds = true
    self.levelLabel.morphingEffect = .evaporate
  }
  
  var isOnLine:Bool = false

  func notOnline() {
    status.isHidden = true
    roomIcon.isHidden = true
    roomNum.isHidden = true
    statusLable.isHidden = true
//    selectedButton.isHidden = true
    isOnLine = false
  }
  
  func online() {
    notOnline()
    isOnLine = true
    selectedButton.isHidden = false
    status.isHidden = false
    statusLable.isHidden = false
    statusLable.text = "在线"
  }
  
  func onlineReady() {
    isOnLine = true
    status.isHidden = false
    roomIcon.isHidden = false
    roomNum.isHidden = false
    statusLable.isHidden = false
    statusLable.text = "准备"
  }
  
  func onlineGame() {
    isOnLine = true
    onlineReady()
    statusLable.text = "游戏中"
  }
  
}
