//
//  MessageNoticeCell.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class MessageNoticeCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var model = MessgeModel() {
        didSet {
            self.titleLabel.text = model.message
        }
    }
  

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      if Screen.width > 320 {
        self.titleLabel.font = UIFont.systemFont(ofSize: 12)
      }else{
        self.titleLabel.font = UIFont.systemFont(ofSize: 10)
      }
  
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func cell() -> MessageNoticeCell {
        let cell = Bundle.main.loadNibNamed("MessageNoticeCell", owner: nil, options: nil)?.last as! MessageNoticeCell
        return cell
    }
    
}
