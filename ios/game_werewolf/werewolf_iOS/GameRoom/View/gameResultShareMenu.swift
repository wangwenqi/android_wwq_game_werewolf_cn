//
//  gameResultShareMenu.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/13.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class gameResultShareMenu: UIView {
  
  var selectBlock: (() -> Void)?
  var webObject: UMShareWebpageObject?
  
  @IBOutlet weak var buttonWidth: NSLayoutConstraint!
  @IBOutlet weak var buttonHeight: NSLayoutConstraint!
  @IBOutlet weak var paddingLeft: NSLayoutConstraint!
  @IBOutlet weak var padding1: NSLayoutConstraint!
  @IBOutlet weak var padding2: NSLayoutConstraint!
  @IBOutlet weak var padding3: NSLayoutConstraint!
  @IBOutlet weak var padding4: NSLayoutConstraint!
  @IBOutlet weak var padding5: NSLayoutConstraint!
    
  @IBOutlet weak var wechat: UIButton!
  @IBOutlet weak var discover: UIButton!
  @IBOutlet weak var line: UIButton!
  @IBOutlet weak var qq: UIButton!
  @IBOutlet weak var facebook: UIButton!
  @IBOutlet weak var whatsapp: UIButton!
  
  var shareImage:UIImage?
  var bigShareImage = true
  
  
  func setUp() {
    wechat.addTarget(self, action: #selector(handleTouch(Button:)), for: .touchUpInside)
    discover.addTarget(self, action: #selector(handleTouch(Button:)), for: .touchUpInside)
    qq.addTarget(self, action: #selector(handleTouch(Button:)), for: .touchUpInside)
    line.addTarget(self, action: #selector(handleTouch(Button:)), for: .touchUpInside)
    facebook.addTarget(self, action: #selector(handleTouch(Button:)), for: .touchUpInside)
    whatsapp.addTarget(self, action: #selector(handleTouch(Button:)), for: .touchUpInside)
  }
  
  func handleTouch(Button:UIButton) {
    selectBlock?()
    let button = Button
    var PlatformType:UMSocialPlatformType = .wechatSession
    switch button.tag {
    case 1:
      PlatformType = .wechatSession
     break
    case 2:
      PlatformType = .wechatTimeLine
      break
    case 3:
      PlatformType = .QQ
      break
    case 4:
      PlatformType = .line
      break
    case 5:
      PlatformType = .facebook
      break
    case 6:
      PlatformType = .whatsapp
      break
      
    default:
      break
    }
    
    //生成分享图片
    let message = UMSocialMessageObject()
    
    if let webObject = webObject {
      message.shareObject = webObject
    } else {
      let shareContent = UMShareImageObject()
      shareContent.shareImage = shareImage
      message.shareObject = shareContent
    }
    
    UMSocialManager.default().share(to: PlatformType, messageObject:message , currentViewController: nil, completion: { (data,error) in
      //点击分享统计
      MobClick.event("Share_\(PlatformType.description)")
      if error != nil {
        if (error! as NSError).code == 2008 {
          XBHHUD.showError(NSLocalizedString("未安装该应用", comment: ""))
        }else if (error! as NSError).code == 2005 {
            XBHHUD.showError(NSLocalizedString("分享内容不支持", comment: ""))
        }else if (error! as NSError).code == 2009 {
          
        }else if (error! as NSError).code == 2002 {
            XBHHUD.showError(NSLocalizedString("授权失败", comment: ""))
        }else if (error! as NSError).code == 2003 {
            XBHHUD.showError(NSLocalizedString("分享失败", comment: ""))
        }
      }else{
        if let sendType = Utils.getTaskType(platform: PlatformType, type: .normal) {
          print(NSLocalizedString("----------发送数据:\(sendType);;;count:\(1)", comment: ""));
          RequestManager().post(
            url: RequestUrls.sendTaskData,
            parameters: ["type": sendType, "count": 1],
            success: { (response) in
              
          }, error: { (code, message) in
          });
        }
      }
    })
  }

  override func layoutSubviews() {
    var padding:CGFloat
    if bigShareImage {
      padding = (self.frame.width - 210) / 5 //(self.frame.width - 175) / 4 //
    }else{
      padding = (self.frame.width - 180) / 5 //(self.frame.width - 150) / 4 //
    }
    padding1.constant = padding
    padding2.constant = padding
    padding3.constant = padding
    padding4.constant = padding
    padding5.constant = padding
//    padding5.constant = padding
//    paddiing6.constant = padding
    
  }
  
  override func awakeFromNib() {
  
    let length = UIScreen.main.bounds.size.height
    
    if length == 568 {
      //iphone5
      bigShareImage = false
    }else if length == 667 {
      //iphone6 
      buttonWidth.constant =  35
      buttonHeight.constant  = 35

    }else if length == 736 {
      buttonWidth.constant =  35
      buttonHeight.constant = 35
    }else if length > 736 {
      buttonWidth.constant =  35
      buttonHeight.constant = 35
  }else{
    buttonWidth.constant =  30
    buttonHeight.constant  = 30
  }
  
  }
  
}
