//
//  MessageGiftCell.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/7/5.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation

enum GiftRoomState {
  case voiceRoom
  case gameRoom
}

class MessageGiftCell:UITableViewCell {
  
  @IBOutlet weak var detailBackView: UIView!
  @IBOutlet weak var messageSenderLabel: UILabel!
  @IBOutlet weak var messageContentLabel: UILabel!
  @IBOutlet weak var giftImageView: UIImageView!
  @IBOutlet weak var detailView: XBHView!
  @IBOutlet weak var titleView: XBHView!
  
  var giftRoom: GiftRoomState = .gameRoom
  
  var model = MessgeModel() {
    didSet {
      messageSenderLabel.text = model.positionNumber
      messageContentLabel.text = model.message

      titleView.backgroundColor = CustomColor.roomGreen
      switch giftRoom {
      case .gameRoom:
        titleView.backgroundColor = CustomColor.roomPink
        titleView.layer.cornerRadius = 10
        detailBackView.isHidden = true
        detailView.backgroundColor = CustomColor.userMessageTextColor
      case .voiceRoom:
        detailView.backgroundColor = UIColor.clear
        detailBackView.isHidden = false
      }
      
      if let type = model.giftType as? String {
        let paths = shareGift.shareInstance.getgetThumbnailImageOnly(type: type)
        if let imagePath = paths["gift_image"] as? String{
          if imagePath != "" {
            giftImageView.image = UIImage(contentsOfFile: imagePath)
          }else{
            if let image =  OLGiftType.getCardPic(type: type) {
              giftImageView.image = image
            }else{
              giftImageView.image = OLGiftType.getGiftPic(type: "");
            }
          }
        }
      } else {
        giftImageView.image = OLGiftType.getGiftPic(type: "");
      }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    detailBackView.layer.cornerRadius = 5
  }
}


