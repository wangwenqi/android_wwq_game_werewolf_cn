//
//  GameProfileView.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/20.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit
import SDCycleScrollView

class GameProfileView: SpringView {
    var goBackBlock:(()-> Void)?
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userDes: UILabel!
    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var addFriendView: UIView!
    @IBOutlet weak var playAgainView: UIView!
    @IBOutlet weak var cycleImageView: SDCycleScrollView!
    @IBOutlet weak var goBack: UIButton!
    //用户信息
    var data:JSON? {
    didSet{
      if let winPeople = data?["game_data"]["gameOver"]["winUserId"].string {
        //获取胜利人信息
        RequestManager().post(url: RequestUrls.userInfo,parameters:["user_id":winPeople], success: { (json) in
          var photoArr = [String]()
          if let photos = json["photos"].array {
            for item in photos {
              var dictionary = item.dictionary
              if let urlString = dictionary?["url"]?.string {
                photoArr.append(urlString)
              }
            }
          }
          self.cycleImageView.delegate = self
          self.cycleImageView.imageURLStringsGroup = photoArr
          self.cycleImageView.showPageControl = true
          self.cycleImageView.autoScroll = true
          self.cycleImageView.infiniteLoop = true
          self.cycleImageView.currentPageDotColor = UIColor.hexColor("FD9026")
          self.userName.text = json["name"].stringValue
          if let iconUrl = URL.init(string: json["image"].stringValue) {
            self.userIcon.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
          }
          //个性签名
          self.userDes.text = json["signature"].stringValue
          if json["signature"].stringValue.isEmpty || json["signature"].stringValue == "" {
              self.userDes.text = NSLocalizedString(NSLocalizedString("Ta很懒，没有留下签名", comment: ""), comment: "")
              self.userDes.textColor = UIColor.gray
          }
        }, error: { (code, message) in
          XBHHUD.showError(message)
        })
        }
      }
    }
    override func awakeFromNib() {
      let tap = UITapGestureRecognizer()
      tap.addTarget(self, action: #selector(playAgin))
      playAgainView.addGestureRecognizer(tap)
//    let tap = UITapGestureRecognizer()
//    tap.addTarget(self, action: #selector(playAgin))
//    playAgainView.addGestureRecognizer(tap)
    }
    
    func playAgin() {
      let payload = ["replay":true]
      messageDispatchManager.sendMicroGameMessage(type:"replay", payLoad: payload)
    }

    @IBAction func goBackClicked(_ sender: Any) {
        if goBackBlock != nil {
            goBackBlock!()
        }
    }
    override func layoutSubviews() {
      for view in cycleImageView.subviews {
        if view.isKind(of: UIPageControl.self) {
          cycleImageView.bringSubview(toFront: view)
        }
      }
    }
}



extension GameProfileView: SDCycleScrollViewDelegate {
  func cycleScrollView(_ cycleScrollView: SDCycleScrollView!, didScrollTo index: Int) {
    print("1")
  }
  func cycleScrollView(_ cycleScrollView: SDCycleScrollView!, didSelectItemAt index: Int) {
    print("2")
  }
}
