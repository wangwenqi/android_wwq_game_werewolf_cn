//
//  MessageUserCell.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class MessageUserCell: UITableViewCell {
  
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet weak var titleView: XBHView!
    @IBOutlet weak var detailBackView: UIView!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var detailView: XBHView!
  
    var model = MessgeModel() {
        didSet {
            titleLabel.text = model.positionNumber
            detailLabel.text = model.message
            if model.type == .judge {
                titleView.backgroundColor = CustomColor.newRoomYellow
                titleLabel.textColor = CustomColor.userMessageTextDark
                detailView.backgroundColor = CustomColor.judgeMessageTextColor
                titleLabel.text = NSLocalizedString("法官", comment: "")
                titleView.layer.cornerRadius = 5
                if model.messageType != GameMessageType.chat {
                  detailView.backgroundColor = CustomColor.judgeResult
                }
            } else if model.type == .player {
                titleView.backgroundColor = CustomColor.roomPink
                detailView.backgroundColor = CustomColor.userMessageTextColor
                titleLabel.textColor = CustomColor.userMessageTextLight
                titleView.layer.cornerRadius = 10
                detailBackView.isHidden = true
            } else if model.type == .voicePlayer {
                titleView.backgroundColor = CustomColor.roomGreen
                detailView.backgroundColor = UIColor.clear
                detailBackView.isHidden = false
          }
      }
  }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      detailBackView.layer.cornerRadius = 5
      detailBackView.layer.masksToBounds = true
      if Screen.width > 320 {
        self.detailLabel.font = UIFont.systemFont(ofSize: 12)
        self.titleLabel.font = UIFont.systemFont(ofSize: 12)

      }else{
        self.detailLabel.font = UIFont.systemFont(ofSize: 10)
        self.titleLabel.font = UIFont.systemFont(ofSize: 10)
      }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
  
  
}
