//
//  ObserverView.swift
//  game_werewolf
//
//  Created by Hang on 2018/1/22.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

class ObserverView: UIView {

  override func awakeFromNib() {
    
  }
  static func observerView(frame: CGRect) -> ObserverView {
    let view = Bundle.main.loadNibNamed("ObserverView", owner: nil, options: nil)?.last as! ObserverView
    view.frame = frame
    return view
  }
}
