//
//  giftView.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/21.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import Alamofire

protocol giftSend:class {
  func sendGift(type:giftType,to:PlayerView?)
  func sendGift(typeString:String,to:PlayerView?)
  func sendGift(type:giftType,form:Int,to:Int)
  func sendGift(typeString:String,form:Int,to:Int)
  func sendGift(type:giftType,json:JSON)
  func sendGift(type:String,json:JSON)
  func sendGiftWithRebate(type:String,to:PlayerView?,json:JSON)
  func sendGiftWithRebate(type:String,json:JSON,rebate:JSON)
}

extension giftSend {
  func sendGift(type:giftType,form:Int,to:Int) {
      print("optinal")
  }
  func sendGift(type:giftType,json:JSON){}
  //MARK:TODO
  func sendGift(typeString:String,to:PlayerView?){}
  func sendGift(typeString:String,form:Int,to:Int){}
  func sendGift(type:giftType,to:PlayerView?){}
  func sendGift(type:String,json:JSON) {}
  func sendGiftWithRebate(type:String,to:PlayerView?,json:JSON){}
  func sendGiftWithRebate(type:String,json:JSON,rebate:JSON){}
}

enum EnvType {
  case GAME
  case PROFILE
  case CHAT
  case AUDIO
  case GAMERECORD
}

enum noticeType {
  case dead
  case money
  case diamond
}

class giftView: SpringView {

  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var pageControl: UIPageControl!
  @IBOutlet weak var shadowView: UIView!
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var shaodwHeight: NSLayoutConstraint!
  @IBOutlet weak var titleView: UIView!
    
  var sendGiftTo:String = ""
  var envType:EnvType = .GAME
  weak var delegate:giftSend?
  var to:PlayerView?
  var toID:String?
  var jsonData:JSON?
  var audioForm:Int?
  var audioTo:Int?
  var effectView:UIVisualEffectView?
  var cards:[JSON]?
  var card:[giftType] = [.delayTime,.checkID,.Wolf,.Witch,.Prophet,.Civilian,.Lover,.Sheriff,.Hunter,.Werewolf_king,.Iguard,.Demon,.Magican]
  var valueArr:NSMutableArray?
  var giftData:NSMutableDictionary? = shareGift.shareInstance.giftInfo
  lazy var charge = Bundle.main.loadNibNamed("giftNotice", owner: nil, options: nil)?.last as! giftNotice
  typealias chatGift = (String)->()
  var chatSendGift:chatGift?
  var requestSuccess:Bool = false
  
  override func awakeFromNib() {
    self.backgroundColor = UIColor.clear//UIColor.black.withAlphaComponent(0.5)
    self.shadowView.backgroundColor = UIColor.clear
    self.contentView.backgroundColor = UIColor.hexColor("140f19").withAlphaComponent(0.8)//UIColor.black.withAlphaComponent(0.65)
    self.collectionView.backgroundColor = UIColor.clear
    self.collectionView.dataSource = self
    self.collectionView.delegate = self
    let newLayout = FlyHorizontalFlowLauyout()
    newLayout.minimumLineSpacing = 6
    newLayout.minimumInteritemSpacing = 6
    self.collectionView.collectionViewLayout = newLayout
    let nib = UINib(nibName: "giftCollectionCell", bundle: nil)
    let type2 = UINib(nibName: "giftCollectionType2Cell", bundle: nil)
    self.collectionView.register(type2, forCellWithReuseIdentifier: "giftype2")
    self.collectionView.register(nib, forCellWithReuseIdentifier: "gift")
    let tap = UITapGestureRecognizer()
    tap.addTarget(self, action: #selector(hiddenView))
    self.shadowView.addGestureRecognizer(tap)
    if Utils.getCurrentDeviceType() == .iPhone5 || Utils.getCurrentDeviceType() == .iPhone4 {
        let _ = self.shaodwHeight.setMultiplier(multiplier: 0.5)
    }
    NotificationCenter.default.addObserver(self, selector: #selector(handleOut), name: NSNotification.Name(rawValue: "giftviewout"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name(rawValue: "SENDCARD"), object: nil)
    
    //监听礼物变化
    NotificationCenter.default.addObserver(self, selector: #selector(updateGift), name:  NSNotification.Name(rawValue: "UPDATEGIFT"), object: nil)
  }
  
  func configUI() {
    //title mask
    layoutIfNeeded()
    self.bringSubview(toFront: self.titleView)
  }
  
  static func initGiftViewWith(envtype:EnvType,People:String) -> giftView{
    let gift = Bundle.main.loadNibNamed("giftView", owner: nil, options: nil)?.last as! giftView
    gift.frame = UIScreen.main.bounds
    let giftTitle = NSLocalizedString("赠送礼物给", comment: "")
    gift.name.text = "\(giftTitle) \(People)"//NSLocalizedString("赠送礼物给  \(People)", comment: "")
    gift.envType = envtype
    XBHHUD.showInView(gift.contentView)
    return gift
  }
  
  func updateGift() {
    giftData = shareGift.shareInstance.giftInfo
    getGiftFormRemote()
  }

  func handleOut() {
    self.hiddenView()
  }

  func reloadData() {
    getCardOnly()
  }
  
  func getCardOnly() {
    CurrentUser.shareInstance.getUserCards { (data: [JSON]) in
      //删除所有的卡牌
      //这个时候一种类型的卡牌可能已经没了
      let deleted = self.valueArr?.filter({ (card) -> Bool in
        if let _ = card as? NSDictionary {
          return true
        }else{
          return false
        }
      })
      self.valueArr = NSMutableArray.init(array: deleted!)
      self.cards = data
      if self.cards != nil {
        for card in self.cards! {
          if let typename = card["type"].string {
            let g = self.card.first(where: {$0.buyName == typename})
            if g != nil {
              self.valueArr?.add(card)
            }
          }
        }
      }
      if self.valueArr != nil {
        let page = (Double((self.valueArr?.count)!)/Double(8))
        self.pageControl.numberOfPages = Int(page.rounded(.up)) //Int(ceil(Double((self.valueArr?.count)!/8)))
        self.collectionView.reloadData()
      }
    }
  }
  
  func generate() {
    if shareGift.shareInstance.cachedGift == nil {
        if let total = giftData {
          if let arr = total["gifts"] as? NSArray {
            let target = NSMutableArray(array: arr)
            let result = target.filter({ (gift) -> Bool in
              if let type = (gift as? NSDictionary)?.object(forKey: "gift_type") as? String {
                let rep = shareGift.shareInstance.getThumbnailImagePathBy(type: type)
                if rep["gift_image"] != "" &&  rep["gift_image"] != "local" {
                  //并且有gif
                  if let _ = shareGift.shareInstance.checkGifByType(str: type) {
                    return true
                  }else{
                    return false
                  }
                }else{
                  return false
                }
              }else{
                return false
              }
            })
            self.valueArr = NSMutableArray(array: result)
          }
        }
        //过滤本地没有图片的
        //过滤所有不在线的
        let fliteArr = self.valueArr?.filter({ (arr) -> Bool in
          if let status = (arr as? NSDictionary)?.object(forKey: "status") as? Int {
            if status == 1 {
              return true
            }else{
              return false
            }
          }else{
            return false
          }
        })
        if fliteArr != nil {
          self.valueArr = NSMutableArray(array: fliteArr!)
        }
        if self.valueArr == nil {
          self.valueArr = NSMutableArray()
        }
        //此时缓存一下礼物信息
        if self.valueArr != nil {
          shareGift.shareInstance.cachedGift = self.valueArr!.mutableCopy() as! NSMutableArray
        }
    }else{
      self.valueArr = shareGift.shareInstance.cachedGift!.mutableCopy() as! NSMutableArray
    }
    DispatchQueue.main.async {
      CurrentUser.shareInstance.getUserCards { (data: [JSON]) in
        XBHHUD.hide()
        self.cards = data;
        if self.cards != nil {
          for card in self.cards! {
            if let typename = card["type"].string {
              let g = self.card.first(where: {$0.buyName == typename})
              if g != nil {
                 self.valueArr?.add(card)
              }
            }
          }
        }
        if self.valueArr != nil {
          let page = (Double((self.valueArr?.count)!)/Double(8))
          self.pageControl.numberOfPages = Int(page.rounded(.up)) //Int(ceil(Double((self.valueArr?.count)!/8)))
          self.collectionView.reloadData()
        }
      }
    }
  }
  
  func getGiftFormRemote(){
    DispatchQueue.global().async {
      self.generate()
    }
  }
  
  func hiddenView() {
    self.animation = "fadeOut"
    self.animateNext {
        self.removeFromSuperview()
    }
  }
  
  
  deinit {
    print(NSLocalizedString("送礼物界面deinit", comment: ""))
    NotificationCenter.default.removeObserver(self)
  }
  
  func handleRefresh() {
    self.effectView?.removeFromSuperview()
    let sessionManager = Alamofire.SessionManager.default
    sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
      dataTasks.forEach { $0.cancel() }
      uploadTasks.forEach { $0.cancel() }
      downloadTasks.forEach { $0.cancel() }
    }
    getGiftFormRemote()
  }
  
  func showNotice(type:noticeType) {
   //遮罩层
    let effect = UIBlurEffect.init(style: .light)
    effectView = UIVisualEffectView.init(effect: effect)
    self.contentView.addSubview(effectView!)
    effectView?.snp.makeConstraints({ (make) in
      make.top.equalTo(self.contentView.snp.top).offset(40)
      make.width.equalTo(self.contentView.snp.width)
      make.bottom.equalTo(self.contentView.snp.bottom).offset(-30)
    })
    //弹出充值
    let rect = CGRect(x: 0, y: 0, width: Screen.width * 0.8, height: 40)
    let lable = levelLable()
    if type == .money {
      lable.text = NSLocalizedString("金币不足无法赠送,快去做任务吧～", comment: "")
      lable.backgroundColor = UIColor.hexColor("0E5F71")
      lable.layer.cornerRadius = 20
      lable.textColor = UIColor.white
      lable.frame = rect
      lable.center = self.collectionView.center
      self.contentView.addSubview(lable)
    }
    if type == .diamond {
      lable.text = NSLocalizedString("钻石不足无法赠送,快去充值吧～", comment: "")
      lable.backgroundColor = UIColor.hexColor("0E5F71")
      lable.layer.cornerRadius = 20
      lable.textColor = UIColor.white
      lable.frame = rect
      lable.center = self.collectionView.center
      self.contentView.addSubview(lable)
    }
  }
}

//MARK: - collection

extension giftView:UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if self.valueArr == nil {
      return 0
    }
    return  (self.valueArr?.count)!
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    if indexPath.row < (self.valueArr?.count)! {
      if let data = self.valueArr?[indexPath.row] as? NSDictionary {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gift", for: indexPath) as! giftCollectionViewCell
          cell.configWith(data: data)
          
          return cell
      }else{
        //json 是卡牌
        if let json = self.valueArr?[indexPath.row] as? JSON {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "giftype2", for: indexPath) as! giftType2CollectionViewCell
          if let cardType = json["type"].string {
            let type = card.first(where: {$0.buyName == cardType})!
            cell.image.image = UIImage(named: type.rawValue)
            cell.backgroundColor = UIColor.clear//UIColor.black.withAlphaComponent(0.5)
            cell.type = type
            if let number = json[cardType]["count"].int {
              if  number > 99 {
                cell.num.text = "x99+"
              }else{
                cell.num.text = "x\(number)"
              }
            }
            return cell
          }
        }
      }
    }
    return  collectionView.dequeueReusableCell(withReuseIdentifier: "gift", for: indexPath) as! giftCollectionViewCell
  }
  
  func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
    let targetCell = collectionView.cellForItem(at: indexPath)
    if (targetCell?.isKind(of: giftType2CollectionViewCell.self))! {
      //点击卡片
      let cell:giftType2CollectionViewCell = targetCell as! giftType2CollectionViewCell
    }else if (targetCell?.isKind(of:giftCollectionViewCell.self))! {
      let cell:giftCollectionViewCell = targetCell as! giftCollectionViewCell
      cell.bg.image = UIImage.init(named:"giftview_bg")
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
    let targetCell = collectionView.cellForItem(at: indexPath)
    if (targetCell?.isKind(of: giftType2CollectionViewCell.self))! {
      //点击卡片
      let cell:giftType2CollectionViewCell = targetCell as! giftType2CollectionViewCell
    }else if (targetCell?.isKind(of:giftCollectionViewCell.self))! {
      let cell:giftCollectionViewCell = targetCell as! giftCollectionViewCell
      cell.bg.image = UIImage.init(named:"giftview_bg_1")
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    //购买礼物
    //购买成功以后在送出🎁
    if CurrentUser.shareInstance.isNight {
      XBHHUD.showError(NSLocalizedString("晚上不能送礼物", comment: ""))
      return
    }
    let targetCell = collectionView.cellForItem(at: indexPath)
    if (targetCell?.isKind(of: giftType2CollectionViewCell.self))! {
      //点击卡片
      let cell:giftType2CollectionViewCell = targetCell as! giftType2CollectionViewCell
      var params:[String:String]
      if envType == .GAME || envType == .AUDIO {
        if envType == .AUDIO {
          if toID == nil || toID == "" {
            XBHHUD.showError("用户已经离开房间")
            return
          }
          params = ["peer_id":toID!,"card_type":(cell.type?.buyName)!]
        }else{
          if to?.playerData.dictionaryObject == nil {
            XBHHUD.showError("用户已经离开房间")
            return
          }
          params = ["peer_id":(to?.playerData["id"].stringValue)!,"card_type":(cell.type?.buyName)!]
        }
        messageDispatchManager.sendMessage(type:.send_card, payLoad:params)
      }else{
        if toID == nil || toID == "" {
          XBHHUD.showError("用户已经离开房间")
          return
        }
        params = ["access_token":CurrentUser.shareInstance.token,"peer":toID!,"type":(cell.type?.buyName)!]
        let _ = XBHHUD.showInView(self.contentView)
        sentCard(params: params, cell: cell)
      }

    }else if (targetCell?.isKind(of:giftCollectionViewCell.self))! {
      let cell:giftCollectionViewCell = targetCell as! giftCollectionViewCell
      var params:[String:String]
      if envType == .GAME || envType == .AUDIO{
        if envType == .AUDIO {
          if toID == nil || toID == "" {
            XBHHUD.showError("用户已经离开房间")
            return
          }
          params = ["peer_id":toID!,"gift_type":(cell.typeString)!]
        }else{
          if to?.playerData.dictionaryObject == nil {
            XBHHUD.showError("用户已经离开房间")
            return
          }
          params = ["peer_id":(to?.playerData["id"].stringValue)!,"gift_type":(cell.typeString)!]
        }
        messageDispatchManager.sendMessage(type:.send_gift, payLoad:params)
        
      }else{
        if toID == nil || toID == "" {
           XBHHUD.showError("用户已经离开房间")
            return
        }
         params = ["access_token":CurrentUser.shareInstance.token,"peer":toID!,"type":(cell.typeString)!]
         let _ = XBHHUD.showInView(self.contentView)
         buyGiftFromRemote(params: params,cell: cell)
      }
    }
  }
  
  func sentCard(params:[String:String],cell:giftType2CollectionViewCell) {
    RequestManager().post(url: RequestUrls.sendCard, parameters:params, success: {[weak self] (response) in
      //隐藏遮挡
      XBHHUD.hide()
      //隐藏礼物界面
      self?.hiddenView()
      CurrentUser.shareInstance.ticket = response["ticket"].stringValue
      //更新cell
      self?.getGiftFormRemote()
      if self?.envType == .PROFILE{
        self?.delegate?.sendGift(typeString: cell.type!.buyName, to: self?.to)
      }
      if self?.envType == .GAMERECORD {
        if let json = self?.jsonData {
          self?.delegate?.sendGift(type: (cell.type?.buyName)!, json: json)
        }
      }
      
    }) {  [weak self] (code, message) in
      //出现错误，直接隐藏
      self?.hiddenView()
      if code == 1008 {
        XBHHUD.showError(NSLocalizedString("赠送卡牌失败", comment: ""))
      }else{
        XBHHUD.showError(message)
      }
    }
  }
  
  func buyGiftFromRemote(params:[String:String],cell:giftCollectionViewCell) {
    RequestManager().post(url: RequestUrls.buy, parameters:params, success: {[weak self] (response) in
      //隐藏遮挡
      XBHHUD.hide()
      CurrentUser.shareInstance.ticket = response["ticket"].stringValue
      //隐藏礼物界面
      if  self?.envType == .PROFILE{
        self?.delegate?.sendGiftWithRebate(type:cell.typeString!, to: self?.to, json: response)
      }
      if self?.envType == .AUDIO {
        // 语音房送礼物成功
        if let form = self?.audioForm,let to = self?.audioTo {
          //self?.delegate?.sendGift(type: cell.type!, form:form , to: to)
          self?.delegate?.sendGift(typeString: cell.typeString!, form: form, to: to)
        }
      }
      if self?.envType == .GAMERECORD {
        if let json = self?.jsonData {
          self?.delegate?.sendGiftWithRebate(type: cell.typeString!, json: json, rebate: response)
        }
      }
      
    }) {  [weak self] (code, message) in
      //出现错误，直接隐藏
      //自动充值
      if code == 1008 {
        if cell.costType == "gold" {
          self?.showNotice(type: .money)
        }else{
          self?.showNotice(type: .diamond)
        }
      }else{
        self?.hiddenView()
        XBHHUD.showError(message)
      }
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width:(Screen.width-38)/4, height:(self.collectionView.height-6)/2)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
  }
  

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
      pageControl.currentPage = Int(ceilf((Float(scrollView.contentOffset.x / Screen.width))))
  }
}

extension NSLayoutConstraint {
  /**
   Change multiplier constraint
   
   - parameter multiplier: CGFloat
   - returns: NSLayoutConstraint
   */
  func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
    
    NSLayoutConstraint.deactivate([self])
    
    let newConstraint = NSLayoutConstraint(
      item: firstItem,
      attribute: firstAttribute,
      relatedBy: relation,
      toItem: secondItem,
      attribute: secondAttribute,
      multiplier: multiplier,
      constant: constant)
    
    newConstraint.priority = priority
    newConstraint.shouldBeArchived = self.shouldBeArchived
    newConstraint.identifier = self.identifier
    
    NSLayoutConstraint.activate([newConstraint])
    return newConstraint
  }
}

/*code is far away from bug with the animal protecting
 *  ┏┓　　　┏┓
 *┏┛┻━━━┛┻┓
 *┃　　　　　　　┃
 *┃　　　━　　　┃
 *┃　┳┛　┗┳　┃
 *┃　　　　　　　┃
 *┃　　　┻　　　┃
 *┃　　　　　　　┃
 *┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　　┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
 *
 */


/**
 *　　　　　　　　┏┓　　　┏┓+ +
 *　　　　　　　┏┛┻━━━┛┻┓ + +
 *　　　　　　　┃　　　　　　　┃
 *　　　　　　　┃　　　━　　　┃ ++ + + +
 *　　　　　　 ████━████ ┃+
 *　　　　　　　┃　　　　　　　┃ +
 *　　　　　　　┃　　　┻　　　┃
 *　　　　　　　┃　　　　　　　┃ + +
 *　　　　　　　┗━┓　　　┏━┛
 *　　　　　　　　　┃　　　┃
 *　　　　　　　　　┃　　　┃ + + + +
 *　　　　　　　　　┃　　　┃　　　　Code is far away from bug with the animal protecting
 *　　　　　　　　　┃　　　┃ + 　　　　神兽保佑,代码无bug
 *　　　　　　　　　┃　　　┃
 *　　　　　　　　　┃　　　┃　　+
 *　　　　　　　　　┃　 　　┗━━━┓ + +
 *　　　　　　　　　┃ 　　　　　　　┣┓
 *　　　　　　　　　┃ 　　　　　　　┏┛
 *　　　　　　　　　┗┓┓┏━┳┓┏┛ + + + +
 *　　　　　　　　　　┃┫┫　┃┫┫
 *　　　　　　　　　　┗┻┛　┗┻┛+ + + +
 */


