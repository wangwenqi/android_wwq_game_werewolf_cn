//
//  userInfoGiftCollectionViewCell.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class userInfoGiftCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var totalNum: levelLable!
  
  
  override func awakeFromNib() {
    let type = Utils.getCurrentDeviceType()
    if type == .iPhone6 {
      self.number.font = UIFont.systemFont(ofSize: 11)
    }else if type == .iPhone6P {
      self.number.font = UIFont.systemFont(ofSize: 12)
    }else if type == .iPhone5 || type == .iPhone4 {
      self.number.font = UIFont.systemFont(ofSize: 10)
    }
  }
    
}
