//
//  levelLable.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class levelLable: UILabel {
  
  let padding: UIEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
  
  // Create a new PaddingLabel instance programamtically with the desired insets
  // Create a new PaddingLabel instance programamtically with default insets
  override init(frame: CGRect) {
    // set desired insets value according to your needs
    super.init(frame: frame)
    self.layer.cornerRadius = 4
    self.layer.masksToBounds = true
  }
  
  // Create a new PaddingLabel instance from Storyboard with default insets
  required init?(coder aDecoder: NSCoder) {
    // set desired insets value according to your needs
    super.init(coder: aDecoder)
  }
  
  override func drawText(in rect: CGRect) {
    super.drawText(in: UIEdgeInsetsInsetRect(rect, padding))
  }
  
  // Override `intrinsicContentSize` property for Auto layout code
  override var intrinsicContentSize: CGSize {
    let superContentSize = super.intrinsicContentSize
    let width = superContentSize.width + padding.left + padding.right
    let heigth = superContentSize.height + padding.top + padding.bottom
    return CGSize(width: width, height: heigth)
  }
  
  // Override `sizeThatFits(_:)` method for Springs & Struts code
  override func sizeThatFits(_ size: CGSize) -> CGSize {
    let superSizeThatFits = super.sizeThatFits(size)
    let width = superSizeThatFits.width + padding.left + padding.right
    let heigth = superSizeThatFits.height + padding.top + padding.bottom
    return CGSize(width: width, height: heigth)
  }

}
