//
//  usreInfoPhotoWallView.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

protocol showPhotoDelegate: class {
  func showPhoto(_ view:MWPhotoBrowser)
}

class usreInfoPhotoWallView: UIView {

    @IBOutlet weak var photoWall: UICollectionView!
    @IBOutlet weak var noticeLable: UILabel!
    
    var photoArr:[MWPhoto] = NSMutableArray() as! [MWPhoto]
    var photo = NSMutableArray()
    var borwser = MWPhotoBrowser()
    var pagecontoller = UIPageControl()
    weak var delegate:showPhotoDelegate?
    override func awakeFromNib() {
        self.photoWall.dataSource = self
        self.photoWall.delegate = self
        let nib = UINib(nibName: "PhotoWallCell", bundle: nil)
        self.photoWall.register(nib, forCellWithReuseIdentifier: "photo")
        pagecontoller.frame = CGRect(x: 0, y: Screen.height - 40, width: Screen.width, height: 30)
        borwser.displayActionButton = false
        borwser.displayNavArrows = false
        borwser.displayActionButton = false
        borwser.delegate = self
        if photo.count > 0 {
            noticeLable.isHidden = true
        }else{
            noticeLable.isHidden = false
        }
    }
}

//MARK: photo browser

extension usreInfoPhotoWallView :MWPhotoBrowserDelegate {
  
  func numberOfPhotos(in photoBrowser: MWPhotoBrowser!) -> UInt {
    return UInt(self.photoArr.count)
  }
  
  func photoBrowser(_ photoBrowser: MWPhotoBrowser!, photoAt index: UInt) -> MWPhotoProtocol! {
    if index < UInt(self.photoArr.count) {
      return self.photoArr[Int(index)] as! MWPhotoProtocol
    }else{
      return nil
    }
  }
  
  func photoBrowser(_ photoBrowser: MWPhotoBrowser!, didDisplayPhotoAt index: UInt) {
        pagecontoller.currentPage = Int(index)
  }
  
}

// MARK: Collection delegate

extension usreInfoPhotoWallView :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return photo.count
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photo", for: indexPath) as! photoWallCollectionViewCell
    photoArr.append(MWPhoto(url: URL(string:photo[indexPath.row] as! String)!))
    cell.photo.image = UIImage(named: "me_photo_default")
    cell.photo.setImage(urlString: photo[indexPath.row] as! String, placeholderImage: UIImage(named: "me_photo_default"))
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
   
    borwser.setCurrentPhotoIndex(UInt(indexPath.row))
    if !borwser.view.subviews.contains(pagecontoller) {
        borwser.view.addSubview(pagecontoller)
    }
    pagecontoller.numberOfPages = self.photoArr.count
    pagecontoller.currentPage = indexPath.row
    delegate?.showPhoto(borwser)
  }
  
  //layout 
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //35为左边距
    return CGSize(width:  (Screen.width - 30 - 35)/3, height:  (Screen.width - 30 - 35)/3) //(Screen.width - 80 - 30)/3)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
      return UIEdgeInsets(top: 0.0, left: 10.0, bottom: 10.0, right: 10.0)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
      return 5
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      return 5
  }
}
