//
//  userBlockListViewController.swift
//  game_werewolf
//
//  Created by Hang on 2017/9/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import MJRefresh

class userBlockListViewController: RotationViewController {
    var currentPage = 0
    let tableView = UITableView()
    var data:[JSON]?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
          make.edges.equalToSuperview()
      }
      navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "room_bg_navbar"), for: .default)
      navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "room_icon_leftarrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(userBlockListViewController.leftClicked))
      self.navigationItem.title = NSLocalizedString("黑名单", comment: "")
      self.view.backgroundColor = UIColor.white
      //注册table
      let register = UINib(nibName: "blockCell", bundle: nil)
      tableView.register(register , forCellReuseIdentifier: "blockCell")
      tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
      tableView.delegate = self
      tableView.dataSource = self
      getData()
      Utils.downloadBL()
    }
  
    func leftClicked() {
        self.dismiss(animated: true, completion: nil)
    }
  
    func getData() {
        XBHHUD.showLoading()
        let params = ["limit":"20","skip":String(currentPage * 20)] as [String : Any]
        RequestManager().get(url: RequestUrls.blockList,parameters: params , success: {[weak self] (response) in
          //隐藏遮挡
          XBHHUD.hide()
          if self?.currentPage == 0 {
            //有数据
            //过滤掉用户
            var user:[JSON]?
            if let users = response["users"].array {
              user = users.filter({ $0["name"].string != nil && $0["name"].string != ""})
            }
            self?.data = user
            if self?.data != nil {
              if  (self?.data?.count)! >= 15 {
                self?.tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: {
                  self?.currentPage = (self?.currentPage)! + 1
                  self?.getData()
                })
              }
            }
            self?.tableView.reloadData()
          }else{
            if self?.data != nil {
              if let _ = response["users"].array {
                if response["users"].arrayValue.count == 0 {
                  self?.tableView.mj_footer.endRefreshingWithNoMoreData()
                }else{
                  var user:[JSON]?
                  if let users = response["users"].array {
                    user = users.filter({ $0["name"].string != nil && $0["name"].string != ""})
                  }
                  if  user != nil && user?.count != 0 {
                    self?.data?.append(contentsOf: user!)
                  }
                  self?.tableView.mj_footer.endRefreshing()
                }
                self?.tableView.reloadData()
              }
            }
          }
        }) { (code, message) in
          XBHHUD.showError(message)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  

}

extension userBlockListViewController: UITableViewDataSource,UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return data == nil ? 0 : (data?.count)!
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
      let cell = tableView.dequeueReusableCell(withIdentifier: "blockCell", for: indexPath) as! blockCellTableViewCell
      let item = data?[indexPath.row]
      cell.config(json: item!)
      return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 80
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
        let item = data?[indexPath.row]
        if let id = item?["id"].string {
          XBHHUD.showLoading()
          RequestManager().get(url:RequestUrls.blockRmove + "/\(id)" , success: { [weak self] (json) in
            XBHHUD.hide()
            self?.getData()
            Utils.remove(id: id)
          }, error: { (code, message) in
               XBHHUD.hide()
              XBHHUD.showError("删除黑名单失败")
          })
        }
    }
  }
  
  func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
    return .delete
  }
  
  func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
    return NSLocalizedString("移除", comment: "")
  }
}
