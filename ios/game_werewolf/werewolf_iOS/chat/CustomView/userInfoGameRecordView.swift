//
//  userInfoGameRecordView.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class userInfoGameRecordView: UIView {
    
    @IBOutlet weak var winRate: UIButton!
    @IBOutlet weak var runRate: UIButton!
    @IBOutlet weak var totalGame: UILabel!
    @IBOutlet weak var winGame: UILabel!
    @IBOutlet weak var loseGame: UILabel!
    @IBOutlet weak var winRateConstant: NSLayoutConstraint!
    @IBOutlet weak var gameHistory: UIButton!
    @IBOutlet weak var winProgress: UIView!
    @IBOutlet weak var escapeProgress: UIView!
    @IBOutlet weak var winWidth: NSLayoutConstraint!
    @IBOutlet weak var loseWidth: NSLayoutConstraint!
    @IBOutlet weak var seeGameHistory: UIButton!
    @IBOutlet weak var levelPoint: UILabel!
    @IBOutlet weak var levelTitle: UILabel!
    @IBOutlet weak var levelImage: UIImageView!
    var userid:String = ""
    typealias ToBattlelevelDesBlock = () -> ()
    var goToDes:ToBattlelevelDesBlock?
    @IBOutlet weak var topView: UIView!
    override func awakeFromNib() {
        self.winProgress.layer.borderWidth = 1
        self.winProgress.layer.borderColor = UIColor.hexColor("f47103").cgColor
        self.escapeProgress.layer.borderWidth = 1
        self.escapeProgress.layer.borderColor = UIColor.hexColor("9b92ff").cgColor
    }

    @IBAction func seeBattle(_ sender: Any) {
      DispatchQueue.main.async {
        let web = StoreCenterViewController()
        web.type = .GameRecord
        web.userid = self.userid
        let nav = RotationNavigationViewController(rootViewController: web)
        Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)?.present(nav, animated: true, completion: { 
          
        })
      }
    }
  
    func tapGestureAction() {
      if goToDes != nil {
          goToDes!()
        }
    }
  
    func setDefaultInfo() {
      UIView.animate(withDuration: 0.5) {
        let _ = self.winWidth.setMultiplier(multiplier:0)
        let _ = self.loseWidth.setMultiplier(multiplier:0)
        self.setNeedsLayout()
        self.layoutIfNeeded()
      }
    }
  
    func setUpWith(json:JSON){
      let winCount = json["game"]["win"].intValue
      let loseCount = json["game"]["lose"].intValue
      let escape = json["game"]["escape"].intValue
      let totalCount = winCount + loseCount
      self.totalGame.text = "\(totalCount)局"
      self.winGame.text = "\(winCount)局"
      self.loseGame.text = "\(loseCount)局"
      let local = NSLocalizedString("胜率", comment: "勝率")
      if winCount == 0 && loseCount == 0 {
         self.winRate.setTitle("\(local) 0%", for: .normal)
         self.runRate.setTitle("逃跑率 0%", for: .normal)
      } else {
        self.winRate.setTitle("\(local) \(lroundf((Float(winCount) / (Float(totalCount) + Float(escape))) * Float(100.0)))%", for: .normal)
        self.runRate.setTitle("逃跑率 \(lroundf((Float(escape) / (Float(totalCount) + Float(escape))) * Float(100.0)))%", for: .normal)
      }
      if let active = json["active"].dictionary {
        var titleLevel = ""
        var star = 0
        var types = ""
        if let title = active["title"]?.string {
            titleLevel = title
        }
        if let level = active["star"]?.int {
            star = level
        }
        if let type = active["type"]?.string {
          types = type
        }
        let levalImageTitle = types + "Level"
        let levelTitles = titleLevel + String(star) + "星"
        if let image = UIImage.init(named: levalImageTitle) {
            self.levelImage.image = image
        }
        self.levelTitle.text = levelTitles
        if let exp = active["experience"]?.int {
          self.levelPoint.text = NSLocalizedString("战力:", comment: "") + "\(exp)"
        }
      }
       //添加说明页面
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(tapGestureAction))
        self.topView.addGestureRecognizer(tap)
        
      let stringWidth = self.winGame.text?.stringSize(CGSize(width: Screen.width, height:20))
      self.winRateConstant.constant = -(((stringWidth?.width)! + 5 + 25)/2 - 12)
      if totalCount + escape == 0 {
        if self.winWidth != nil {
          UIView.animate(withDuration: 0.5) {
            let _ = self.winWidth.setMultiplier(multiplier:0)
            let _ = self.loseWidth.setMultiplier(multiplier:0)
            self.setNeedsLayout()
            self.layoutIfNeeded()
          }
        }
      }else{
        let win = Float(Float(winCount) / (Float(totalCount) + Float(escape)))
        let es = Float(Float(escape) / (Float(totalCount) + Float(escape)))
        if self.winWidth != nil {
          UIView.animate(withDuration: 0.5) {
            let _ = self.winWidth.setMultiplier(multiplier:CGFloat(win))
            let _ = self.loseWidth.setMultiplier(multiplier:CGFloat(es))
            self.setNeedsLayout()
            self.layoutIfNeeded()
            }
          }
      }
    }
}

//"active": {
//  "title" : "村民",
//  "level" : 1,
//  "type" : "villager",
//  "experience" : 0,
//  "star" : 1
//}

enum levelTitle:String {
 
  case villager
  case village_head
  case sheriff
  case guard1 = "guard"
  case hunter
  case paladin
  case witch
  case seer
  case priest
  case bishop
  case red_bishop
  case pontiff
  case angel
  case demon
  case cupid
  case sun_god
  case pluto
  case zeus
  

  case minister
  case minister1
  case minister2
  case minister3
  
  case paladin1
  case paladin2
  case paladin3
  
  case pontiff1
  case pontiff2
  case pontiff3
  
  case cupid1
  case cupid2
  case cupid3
  
  case big_cupid1
  case big_cupid2
  case big_cupid3
  
  case wise_cupid1
  case wise_cupid2
  case wise_cupid3
  
  case seraph1
  case seraph2
  case seraph3
  
  case luna1
  case luna2
  case luna3
  
  case thor1
  case thor2
  case thor3
  
  case poseidon1
  case poseidon2
  case poseidon3
  
  case sun_god1
  case sun_god2
  case sun_god3
  
  case pluto1
  case pluto2
  case pluto3
  
  case zeus1
  case zeus2
  case zeus3
}

