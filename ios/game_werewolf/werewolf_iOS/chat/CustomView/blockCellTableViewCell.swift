//
//  blockCellTableViewCell.swift
//  game_werewolf
//
//  Created by Hang on 2017/9/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class blockCellTableViewCell: UITableViewCell {

 
    @IBOutlet weak var avaImage: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var blockTime: UILabel!
    
    @IBOutlet weak var sex: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  
   func config(json:JSON) {
    
      if let name = json["name"].string {
        self.name.text = name
      }
    
      if let time = json["created_at"].int {
        //时间转换
        let date = Date(timeIntervalSince1970: TimeInterval(time/1000))
        let formater = DateFormatter()
        formater.dateFormat = "YY年MM月dd日 HH:mm"
        self.blockTime.text = formater.string(from: date)
      }
    
      if json["sex"].intValue == 1 {
        // 男
        self.sex.image = UIImage.init(named: "ic_male")
      } else {
        // 女
        self.sex.image = UIImage.init(named: "ic_female")
      }

      if let url = json["image"].string {
        self.avaImage.sd_setImage(with: URL(string:(url)), placeholderImage: UIImage(named:"room_head_default"))
      }else{
        self.avaImage.image = UIImage(named:"room_head_default")
      }
      //选中状态为空
      self.selectionStyle = .none
    }
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
