//
//  userInfoHeaderView.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import TTGTagCollectionView

protocol userInfoHeaderDelegate :class {
    func addFriend()
    func sendMessage()
}

class userInfoHeaderView: UIView {

  @IBOutlet weak var ava: UIImageView!
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var level: levelLable!
  @IBOutlet weak var sendMessage: UIButton!
  @IBOutlet weak var addFriend: UIButton!
  @IBOutlet weak var sex: UIImageView!
  @IBOutlet weak var location: UILabel!
  
  @IBOutlet weak var avaWitdth: NSLayoutConstraint!
  @IBOutlet weak var avaHeighr: NSLayoutConstraint!
  @IBOutlet weak var popularLable: UILabel!
 
  @IBOutlet weak var tagView: TTGTextTagCollectionView!
  @IBOutlet weak var signTtitle: UILabel!
  @IBOutlet weak var signContent: UILabel!
  @IBOutlet weak var userID: UILabel!
    
  weak var delegate:userInfoHeaderDelegate?
  
  var photoArr:[MWPhoto] = NSMutableArray() as! [MWPhoto]
  var borwser = MWPhotoBrowser()
  weak var showDelegate:showPhotoDelegate?
  override func awakeFromNib() {
    
//    self.level.layer.borderColor = UIColor.black.cgColor
//    self.level.layer.borderWidth = 1
//    self.level.layer.cornerRadius = 3
//    self.level.layer.masksToBounds = true

    self.ava.layer.borderColor = UIColor.white.cgColor
    self.ava.layer.borderWidth = 4
    
    self.location.layer.borderColor = UIColor.black.cgColor
    self.location.layer.borderWidth = 1
    self.location.layer.cornerRadius = 3
    self.location.layer.masksToBounds = true
  }
  
  func showPhoto(tap:UITapGestureRecognizer) {
      borwser.displayActionButton = false
      borwser.displayNavArrows = false
      borwser.displayActionButton = false
      borwser.delegate = self
      self.showDelegate?.showPhoto(borwser)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
  }
  
  
  func setUpWith(json:JSON){
    self.name.text = json["name"].stringValue
    if let iconUrl = URL.init(string: json["image"].stringValue) {
      self.ava.sd_setImage(with: iconUrl, placeholderImage: UIImage.init(named: "room_head_default"))
    }
    //先清除所有的tag
    tagView.removeAllTags()
    //添加🏷️
    var tags:[String] = []
    let roleType = json["role"]["type"].intValue
    var defaultTag = ""
    switch roleType {
    case 1:
      defaultTag = "管理员"
    case 2:
      defaultTag = "老师"
    case 3:
      defaultTag = "管理员"
      tags.append("老师")
    default:
      defaultTag = ""
    }
    if defaultTag != "" {
      tags.append(defaultTag)
    }
    if let roleCustom = json["role"]["custom"].string {
      if roleCustom.isNotEmpty {
         tags.append(roleCustom)
      }
    }
    if tags.count != 0 {
    for str in tags {
      let index = tags.index(of: str)
      let config = TTGTextTagConfig()
      config.tagTextFont = UIFont.boldSystemFont(ofSize: 13)
      config.tagTextColor = UIColor.hexColor("2d1c4c")
      config.tagBorderWidth = 1
      config.tagBorderColor = UIColor.hexColor("7a64ff")
      config.tagExtraSpace = CGSize(width: 10, height: 10)
      config.tagCornerRadius = 10
      config.tagShadowColor = UIColor.clear
      let colorIndex = index! % 3
      if colorIndex == 0 {
        config.tagBackgroundColor = UIColor.hexColor("50d7ff")
        config.tagSelectedBackgroundColor = UIColor.hexColor("50d7ff")
      }else if colorIndex == 1 {
        config.tagBackgroundColor = UIColor.hexColor("fdff00")
        config.tagSelectedBackgroundColor = UIColor.hexColor("fdff00")
      }else if colorIndex == 2 {
        config.tagBackgroundColor = UIColor.hexColor("ff78ff")
        config.tagSelectedBackgroundColor = UIColor.hexColor("ff78ff")
      }
      tagView.addTag(str, with: config)
//      tagView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
    }
    }
    //添加到图片库
    photoArr.append(MWPhoto(url:URL.init(string: json["image"].stringValue)))
    if json["sex"].intValue == 1 {
      // 男
      self.sex.image = UIImage.init(named: "ic_male")
    } else {
      // 女
      self.sex.image = UIImage.init(named: "ic_female")
    }
    self.level.text = "Lv.\(json["game"]["level"].intValue)"
    //人气值
    if let popurlar = json["popular"].int {
        self.popularLable.text = "\(popurlar)"
    }
    //uid
    if let uid = json["uid"].int {
        self.userID.text = "ID:\(uid)"
        self.userID.isHidden = false
    }

    //添加点击事件
    self.ava.isUserInteractionEnabled = true
    let tap = UITapGestureRecognizer()
    tap.numberOfTouchesRequired = 1
    tap.numberOfTapsRequired = 1
    tap.addTarget(self, action: #selector(showPhoto(tap:)))
    self.ava.addGestureRecognizer(tap)
  
  }
    @IBAction func addFriend(_ sender: Any) {
      delegate?.addFriend()
    }
    @IBAction func sendMessage(_ sender: Any) {
      delegate?.sendMessage()
    }
}

//MARK: photo browser

extension userInfoHeaderView :MWPhotoBrowserDelegate {
  
  func numberOfPhotos(in photoBrowser: MWPhotoBrowser!) -> UInt {
    return UInt(self.photoArr.count)
  }
  
  func photoBrowser(_ photoBrowser: MWPhotoBrowser!, photoAt index: UInt) -> MWPhotoProtocol! {
    if index < UInt(self.photoArr.count) {
      return self.photoArr[Int(index)] as! MWPhotoProtocol
    }else{
      return nil
    }
  }
  
  func photoBrowser(_ photoBrowser: MWPhotoBrowser!, didDisplayPhotoAt index: UInt) {
   
  }
  
}
