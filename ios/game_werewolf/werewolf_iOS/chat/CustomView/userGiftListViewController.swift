//
//  userGiftListViewController.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import MJRefresh

enum From {
  case NATIVE
  case REACT
}

class userGiftListViewController: RotationViewController {
  
    var tableView:UITableView?
    var userid:String?
    var currentPage = 0
    var data:[JSON]?
    var users:[String:JSON]?
    var form:From = .NATIVE
  
    lazy var allgifts:[giftType] = [.Rose,.Rose3,.Rose99,.Kiss,.Vegetable,.Egg,.Clap,.Chocolate,.Cake,.Weddingdress,.Lollipop,.Boat,.Teddy,.Heart,.Ring,.Crown,.Car,.Airplane,.Rocket,.delayTime,.checkID,.Sheriff,.Wolf,.Lover,.Hunter,.Civilian,.Witch,.Prophet,.Demon,.Magican]
  
    override func viewDidLoad() {
        super.viewDidLoad()
      navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "room_bg_navbar"), for: .default)
      navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "room_icon_leftarrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(userGiftListViewController.leftClicked))
      self.navigationItem.title = NSLocalizedString("收到的礼物", comment: "")
      self.view.backgroundColor = UIColor.white
      if form == .NATIVE {
        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: Screen.width, height: Screen.height - 64), style: .plain)
      }else{
        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: Screen.width, height: Screen.height-20), style: .plain)
      }
      tableView?.dataSource = self
      tableView?.delegate = self
      let register = UINib(nibName: "userGiftListCell", bundle: nil)
      tableView?.register(register, forCellReuseIdentifier: "giftlist")
      tableView?.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
      self.view.addSubview(tableView!)
      getData()
    }
  
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    func leftClicked() {
      if self.form == .REACT {
        self.dismiss(animated: true, completion: nil)
      }else{
        self.navigationController?.popViewController(animated: true)
      }
    }
  
  func fliter() {
    data = data?.filter({ (item) -> Bool in
      if let type = item["type"].string {
        let rep = shareGift.shareInstance.getThumbnailImagePathBy(type: type)
        if rep["gift_image"] != "" &&  rep["gift_image"] != "local" {
          return true
        }else{
          return false
        }
      }else{
        return false
      }
    })
    self.tableView?.reloadData()
  }
  
  func getData() {
    XBHHUD.showLoading() //2f026f8499d109aec95349a76a9b2ae44d18b7ee CurrentUser.shareInstance.token
    let params = ["access_token":CurrentUser.shareInstance.token,"id":userid!,"page":String(currentPage)]
    RequestManager().post(url: RequestUrls.giftHistory, parameters:params, success: { (response) in
      //隐藏遮挡
      XBHHUD.hide()
      if self.currentPage == 0 {
        //有数据
        let tempData = response["histories"].arrayValue
        let tempUser = response["users"].dictionaryValue
        //过滤不存在的item
        self.data = tempData.filter{
          tempUser[$0["from"].stringValue] != nil
        }
        self.users = response["users"].dictionaryValue
        if  CGFloat((self.data?.count)! * 90) > Screen.height {
          self.tableView?.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: {
            self.currentPage = self.currentPage + 1
            self.getData()
          })
        }
        //过滤礼物type
        self.fliter()
      }else{
        if self.data != nil {
            if response["histories"].arrayValue.count == 0 {
              self.tableView?.mj_footer.endRefreshingWithNoMoreData()
            }else{
              for key in response["users"].dictionaryValue {
                self.users?.updateValue(key.value, forKey: key.key)
              }
              let tempData = response["histories"].arrayValue
              let validate = tempData.filter{
                self.users?[$0["from"].stringValue] != nil
              }
              self.data?.append(contentsOf: validate)
              self.tableView?.mj_footer.endRefreshing()
          }
          self.fliter()
        }
      }
    }) { (code, message) in
      XBHHUD.showError(message)
    }
  }
}

extension userGiftListViewController: UITableViewDelegate,UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return data == nil ? 0 : (data?.count)!
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 90
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let item = data?[indexPath.row]
    var cell = tableView.dequeueReusableCell(withIdentifier: "giftlist", for: indexPath) as! userGiftListTableViewCell
    let typeName = item?["type"].stringValue
    let type = allgifts.first { $0.buyName == typeName}
    let userkey = item?["from"].stringValue
    let json = users?[userkey!]
    let time = (item?["time"].doubleValue)!/1000 as! TimeInterval
    //时间转换
    let date = Date(timeIntervalSince1970: time)
    let formater = DateFormatter()
    formater.dateFormat = "YY年MM月dd日 HH:mm"
    //配置
    let paths = shareGift.shareInstance.getThumbnailImagePathBy(type: typeName!)
    if let imagePath = paths["gift_image"] as? String {
      if imagePath != "" {
        cell.giftImage.image = UIImage(contentsOfFile: imagePath)
      }
    }
   cell.title.text = json?["name"].stringValue
    if let name = paths["gift_name"] as? String {
      if name != "" && name != "local" {
        cell.giftName.text = name
      }
    }
    cell.ava.sd_setImage(with: URL(string:(json?["image"].stringValue)!), placeholderImage: UIImage(named:"room_head_default"))
    cell.sendTime.text = formater.string(from: date)
    cell.selectionStyle = .none
    cell.avaClickAction = {
      //去这个人的主页
      RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id": userkey], success: { [weak self] (json) in
        XBHHUD.hide()
        let controller = UserInfoViewController()
        controller.peerID = json["id"].stringValue
        controller.peerSex = json["sex"].stringValue
        controller.peerName = json["name"].stringValue
        controller.peerIcon = json["image"].stringValue
        if let isFriend = json["is_friend"].string {
          controller.isFriend = isFriend == "false" ? false : true
        } else if let isFriend = json["is_friend"].bool {
          controller.isFriend = isFriend;
        }
        controller.isTourist = false
        controller.isself = json["id"].stringValue == CurrentUser.shareInstance.id ? true : false
        Utils.hideNoticeView()
        let nav = RotationNavigationViewController.init(rootViewController: controller)
        ((Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)!) as UIViewController).present(nav, animated: true, completion: nil)
      }) { (code, message) in
        XBHHUD.showError(message)
      }
    }
    return cell
  }

}
