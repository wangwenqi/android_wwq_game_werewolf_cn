//
//  userInfoFamilyView.swift
//  game_werewolf
//
//  Created by Hang on 2017/10/25.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class userInfoFamilyView: UIView {
  
    var familyID = ""
    var isMyself = false
    @IBOutlet weak var familyImage: UIImageView!
    @IBOutlet weak var familyName: UILabel!
    @IBOutlet weak var familyPeople: levelLable!
    @IBOutlet weak var familyNumber: UILabel!
    @IBOutlet weak var noFamilyNoticeView: UIView!
    @IBOutlet weak var familyInfo: UIButton!
    @IBOutlet weak var notice: UILabel!
    @IBOutlet weak var levelImage: UIImageView!
    
    @IBAction func familyInfoCliked(_ sender: Any) {
      if CurrentUser.shareInstance.isTourist {
        XBHHUD.showError(NSLocalizedString("注册后才能查看家族信息", comment:""))
        return
      }
      XBHHUD.showLoading()
      let para = ["maxUser":"30"]
      RequestManager().get(url:RequestUrls.groupInfo + "\(familyID)",parameters:para, success: { (json) in
        XBHHUD.hide()
        let sb = UIStoryboard.init(name: "FamilyDetail", bundle: nil)
        let familyDetail = sb.instantiateViewController(withIdentifier: "FamilyDetail") as! FamilyDetailViewController
        familyDetail.datas = json
        let nav = RotationNavigationViewController.init(rootViewController: familyDetail)
        ((Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)!) as UIViewController).present(nav, animated: true, completion: nil)
      }) { (code, message) in
        XBHHUD.hide()
        XBHHUD.showError(message)
      }
    }
  
    func setTuroseModel() {
      self.notice.text = NSLocalizedString("还没有加入任何家族", comment: "")
      self.height = 90
      self.noFamilyNoticeView.isHidden = false
    }
    
    override func awakeFromNib() {
      self.familyPeople.layer.cornerRadius = 10
      self.familyPeople.layer.masksToBounds = true
      self.familyImage.isHidden = true
      self.familyPeople.isHidden = true
      self.familyName.isHidden = true
      self.familyNumber.isHidden = true
    }
  
    func setUpWith(json:JSON){
    //没有的话，就直接在上边覆盖
    if let family = json["group"].dictionary {
      
      self.familyImage.isHidden = false
      self.familyPeople.isHidden = false
      self.familyName.isHidden = false
      self.familyNumber.isHidden = false
      
      if let number = family["gid"]?.int {
        self.familyNumber.text = "\(number)"
      }
      if let name = family["name"]?.string {
        self.familyName.text = name
      }
      if let image = family["image"]?.string {
        let url = URL.init(string: image)
        self.familyImage.sd_setImage(with: url, placeholderImage: UIImage.init(named: "ico_family_default"))
      }
      if let count = family["member_count"]?.int {
        self.familyPeople.text = "\(count)人"
      }
      if let groupID = family["group_id"]?.string {
        self.familyID = groupID
      }
      if let levelImage = family["level_image"]?.string {
        let url = URL.init(string: levelImage)
        self.levelImage.sd_setImage(with: url, placeholderImage: UIImage.init(named:"勋章初级图标"))
      }
      if let level_val = family["level_val"]?.int {
        let level  = level_val > 10 ? 10 : level_val
        self.levelImage.image = UIImage.init(named: "level\(level)")
      }
  
    }else{
      if isMyself == false {
        self.notice.text = NSLocalizedString("还没有加入任何家族", comment: "")
      }
      self.height = 90
      self.noFamilyNoticeView.isHidden = false
    }
  }
}
