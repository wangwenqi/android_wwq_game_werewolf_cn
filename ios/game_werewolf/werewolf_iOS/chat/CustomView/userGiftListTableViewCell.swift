//
//  userGiftListTableViewCell.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class userGiftListTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var ava: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var giftImage: UIImageView!
    @IBOutlet weak var giftName: UILabel!
    @IBOutlet weak var sendTime: UILabel!
    typealias avaClick = () -> ()
    var avaClickAction:avaClick?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action:#selector(handle))
        ava.addGestureRecognizer(tap)
      
    }
  
    func handle() {
      avaClickAction!()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
