//
//  userInfoGiftView.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

protocol goToGiftList :class {
  func goToGiftList()
}

class userInfoGiftView: UIView {

    @IBOutlet weak var narrowButton: UIButton!
    @IBOutlet weak var gift: UICollectionView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var noticeLable: UILabel!
    
    var totalGifts:Int = 0 {
        didSet{
          if totalGifts != 0 {
            let local = NSLocalizedString("收到的礼物", comment: "")
            self.title.text = "\(local)(\(totalGifts))"
          }
        }
    }
    
    weak var delegate:goToGiftList?
    var gifts:[JSON]?
    lazy var allgifts:[giftType] = [.Rose,.Rose3,.Rose99,.Kiss,.Vegetable,.Egg,.Clap,.Chocolate,.Cake,.Weddingdress,.Lollipop,.Boat,.Teddy,.Heart,.Ring,.Crown,.Car,.Airplane,.Rocket,.delayTime,.checkID,.Wolf,.Witch,.Prophet,.Civilian,.Lover,.Sheriff,.Hunter,.Demon,.Magican]
    var card:[giftType] = [.delayTime,.checkID,.Wolf,.Witch,.Prophet,.Civilian,.Lover,.Sheriff,.Hunter,.Demon,.Magican]
    override func awakeFromNib() {
      let nib = UINib(nibName: "userInfoGiftCell", bundle: nil)
      let card = UINib(nibName: "userInfoGiftCardTypeCell", bundle: nil)
      self.gift.register(nib, forCellWithReuseIdentifier: "userInfoGift")
      self.gift.register(card, forCellWithReuseIdentifier: "userInfoGiftCardTypeCell")
      self.gift.dataSource = self
      self.gift.delegate = self
      self.narrowButton.isHidden = true
    }
  
  func setup(withJson:JSON) {
    if let json = withJson.array {
      //解析json
     gifts = json.filter({ (gift) -> Bool in
        if let type = gift["type"].string {
         let rep = shareGift.shareInstance.getThumbnailImagePathBy(type: type)
         if rep["gift_image"] != "" &&  rep["gift_image"] != "local" {
              return true
            }else{
              return false
            }
        }else{
          return false
        }
      })
    }
    if gifts != nil {
       if (gifts!.count) > 0 {
            self.narrowButton.isHidden = false
            self.gift.reloadData()
            let GiftRow = ceilf((Float((gifts!.count)) / Float(4)))
            self.height = CGFloat(80 * GiftRow + 60 + 10)
            self.noticeLable.isHidden = true
       }else{
        self.height = 80
        self.noticeLable.isHidden = false
      }
    }else{
      self.height = 80
    }
  }
    @IBAction func goToProfile(_ sender: Any) {
      self.delegate?.goToGiftList()
    }

}

extension userInfoGiftView :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return gifts == nil ? 0 : (gifts?.count)!
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let typeName = gifts?[indexPath.row].dictionaryValue["type"]?.stringValue
    if let type = card.first(where: {$0.buyName == typeName}) {
      if card.contains(type) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userInfoGiftCardTypeCell", for: indexPath) as! userInfoGiftCardCollectionViewCell
        cell.image.image = UIImage(named: (type.image))
        return cell
      }
    }else{
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userInfoGift", for: indexPath) as! userInfoGiftCollectionViewCell
      let num = gifts?[indexPath.row].dictionaryValue["count"]?.stringValue
      let paths = shareGift.shareInstance.getThumbnailImagePathBy(type: typeName!)
      let imagePath = paths["gift_image"] as? String
      if imagePath != "" {
        
        cell.image.image = UIImage(contentsOfFile: imagePath!)
      }
      let name = paths["gift_name"] as? String
      cell.number.text = name
      totalGifts += Int(num!)!
      if num == "0" {
        cell.totalNum.isHidden = true
      }else{
        cell.totalNum.isHidden = false
        if Int(num!)! > 999 {
          cell.totalNum.text = "x999+"
        }else{
          cell.totalNum.text = "x\(num!)"
        }
      }
      return cell
    }
    return  collectionView.dequeueReusableCell(withReuseIdentifier: "userInfoGift", for: indexPath) as! userInfoGiftCollectionViewCell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    self.delegate?.goToGiftList()
  }
  
  //layout
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //40右边距，40四个间距为10的，35左间距
    return CGSize(width:(Screen.width - 40 - 40 - 35)/4 , height:80)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 10.0, right: 0.0)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 10
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
}
