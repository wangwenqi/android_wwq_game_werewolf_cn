//
//  AddFriendTipInChatView.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/6/7.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation

class CallbackData {
  // 错误状态 0 是表示没有问题
  var code:Int = 0;
  // 实际需要返回的错误
  var data:[String:Any] = [:];
  // 错误消息内容
  var message:String = "";
}
typealias goProfile = (UserInfoViewController) -> ()
typealias AddFriendButtonClickedCallbackData = CallbackData;
typealias AddFriendButtonClickedCallback = ((_ data:AddFriendButtonClickedCallbackData) -> Void);
typealias addToBL = () -> Void
class AddFriendTipInChatView:UIView {
  
    var addFriendUid:String = "";
    var goto:goProfile?
    @IBOutlet weak var ava: UIImageView!
    @IBOutlet weak var sex: UIImageView!
    @IBOutlet weak var addFriend: UIButton!
    @IBOutlet weak var addToBL: UIButton!
    
    var clickCallback:AddFriendButtonClickedCallback? = nil;
    var addBlCallback:addToBL? = nil
    override func awakeFromNib() {
        self.addFriend.layer.borderColor = UIColor.hexColor("6FCB00").cgColor
        self.addFriend.layer.borderWidth = 1
        self.addFriend.layer.cornerRadius = 2
        self.addFriend.layer.masksToBounds = true
        //添加点击事件
        self.ava.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.ava.addGestureRecognizer(tap)
        //点击拉黑
        self.addToBL.layer.borderColor = UIColor.hexColor("BE7E7A").cgColor
        self.addToBL.layer.borderWidth = 1
        self.addToBL.layer.cornerRadius = 2
        self.addToBL.layer.masksToBounds = true
    }
  
  func handleTap() {
    
    XBHHUD.showLoading()
    RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id": addFriendUid], success: {[weak self] (json) in
      XBHHUD.hide()
      let controller = UserInfoViewController()
      controller.peerID = json["id"].stringValue
      controller.peerSex = json["sex"].stringValue
      controller.peerName = json["name"].stringValue
      controller.peerIcon = json["image"].stringValue
      if let isFriend = json["is_friend"].string {
        controller.isFriend = isFriend == "false" ? false : true
      } else if let isFriend = json["is_friend"].bool {
        controller.isFriend = isFriend;
      }
      controller.isTourist = false
      controller.isself = json["id"].stringValue == CurrentUser.shareInstance.id ? true : false
      if (self?.goto != nil) {
        self?.goto!(controller)
      }
      }, error:{ (code, message) in
         XBHHUD.showError(NSLocalizedString("请检查您的网络...", comment: ""))
      })
    }
  
    @IBAction func HandleAddFriendButtonClicked(_ sender: UIButton) {
      let data = AddFriendButtonClickedCallbackData();
      if(addFriendUid == "") {
        data.code = -1;
        data.message = "未获取到对方的数据";
        clickCallback?(data);
        return;
      }
      //在黑名单中的不能添加好友
      if let block = Utils.getCachedBLContent() {
        if addFriendUid != nil {
          if block[addFriendUid] != nil {
            XBHHUD.showError("此人已经被你拉黑")
            return
          }
        }else{
          return
        }
      }
      XBHHUD.showLoading();
      RequestManager().post(
        url: RequestUrls.friendAdd,
        parameters: ["friend_id": addFriendUid],
        success: { [weak self] (response) in
          XBHHUD.hide()
          self?.clickCallback?(data);
        }, error: { [weak self] (code, message) in
          data.code = code;
          data.message = message;
          XBHHUD.hide();
          self?.clickCallback?(data);
//          XBHHUD.hide()
//          self?.clickCallback?(data);
        });
    }

  @IBAction func addToBLClicked(_ sender: Any) {
    
    if let block = Utils.getCachedBLContent() {
      if addFriendUid != nil {
        if block[addFriendUid] != nil {
          XBHHUD.showError("此人已经被你拉黑")
          return
        }
      }else{
        return
      }
    }
    XBHHUD.showLoading()
    RequestManager().get(url: RequestUrls.block + "/\(addFriendUid)", success: {[weak self] (json) in
      XBHHUD.hide()
      XBHHUD.showSuccess(NSLocalizedString("拉黑成功！", comment: ""))
      Utils.addToBL(id:(self?.addFriendUid)!)
      if self?.addBlCallback != nil {
        self?.addBlCallback!()
      }
    }) { (code, message) in
      XBHHUD.hide()
      XBHHUD.showError(NSLocalizedString("拉黑失败！", comment: ""))
    }
  }
}

extension AddFriendTipInChatView {
  func handleClickAddFriendButtonCallback(_ callback:@escaping AddFriendButtonClickedCallback) {
    self.clickCallback = callback;
  }
}
