//
//  ChatViewController.swift
//  game_werewolf
//
//  Created by QiaoYijie on 2017/4/26.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import ChatKit

class ChatViewController: BaseChatViewController {

  var tipView:AddFriendTipInChatView? = nil;
  
  // MSG_TYPE: FAMILY | SINGLE | noplay
  
  fileprivate let addFriendTipHeight = 64.0;
  
  lazy var giftButton:SpringButton = {
    var button = SpringButton();
    if let giftImage = UIImage(named: "送礼物图标") {
      button.frame = CGRect(x: Screen.width - 60, y: Screen.height * 0.7, width: 60, height: 60);
      button.setImage(giftImage, for: .normal)
      button.addTarget(self, action: #selector(showSendGift), for:.touchUpInside)
    }
    return button;
  }();
  var gameButton:UIButton = UIButton()
  override func viewDidLoad() {
    super.viewDidLoad()
    checkControlSocket()
    messageType = "SINGLE";
    self.viewDidAppearBlock = {(controller:LCCKBaseViewController?, bool:Bool)  in
      Utils.runInMainThread { [weak self] in
        if let titleVIew = self?.navigationItem.titleView as? LCCKConversationNavigationTitleView,
          let title = self?.peerName {
          titleVIew.conversationNameView.text = title;
          if #available(iOS 11, *) {
              titleVIew.snp.makeConstraints({ (make) in
                make.left.equalTo((Screen.width - titleVIew.width)/2)
              })
              for view in titleVIew.subviews {
                if view.isKind(of: UIStackView.self) {
                  view.snp.makeConstraints({ (make) in
                    make.edges.equalToSuperview()
                  })
                }
              }
          }
        }
      }
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    checkFriendship(uid: self.peerID)
    addSendGiftButton()
    addGameButton()
  }
  
  override var shouldAutorotate: Bool {
    return false
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated);
    removeSendGiftButton();
    removeGameButton()
  }
  
  override func leftClicked() {
    hideAddFriendView()
    super.leftClicked();
    self.viewDidAppearBlock = nil;
    if self.conversationId != nil {
      RNMessageSender.emitEvent(name: "LEAVE_EVENT_CHAT", andPayload: [
        "USER_ID":self.peerID,
        "CONVERSATION_ID":self.conversationId,
        "CONVERSATION_TYPE":self.messageType
        ])
    }
    NotificationCenter.default.removeObserver(self)
  }
  
  func checkControlSocket() {
      SwiftConverter.shareInstance.connectControlSocket()
  }
    
  func checkFriendship(uid:String) {
    
    if uid.lowercased() == Config.customCareUserID {
      return;
    }
    
    SwiftConverter.shareInstance.getUserInformation(userId: uid, success: { dic in
      if let friendship = dic["is_friend"] as? Bool,
        friendship == false {
        self.showAddFriendView()
      }
    })

  }
  
  deinit {
    //离开就发送取消游戏
    let payload = ["type":"cancel_invite"]
    messageDispatchManager.sendControlMessage(type:ControlMessage.play_mini_game.rawValue, payLoad: payload)
  }
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
}

extension ChatViewController {
  fileprivate func showAddFriendView() {
    if tipView == nil,
      let navFrame = navigationController?.view.frame {
      tipView = UIView.loadFromNib("AddFriendTipInChatView") as? AddFriendTipInChatView;
      tipView?.addFriendUid = self.peerID
      if self.peerIcon != nil,
        let imageUrl = URL(string: self.peerIcon) {
        tipView?.ava.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named: "room_head_default"))
      } else {
        tipView?.ava.image = UIImage.init(named: "room_head_default");
      }
      if self.peerSex != nil {
        if Int(self.peerSex) == 1 {
          tipView?.sex.image = UIImage.init(named: "ic_male")
        } else {
          tipView?.sex.image = UIImage.init(named: "ic_female")
        }
      }
      tipView?.goto = { [weak self] userinfo in
        self?.tipView?.removeFromSuperview()
        self?.navigationController?.pushViewController(userinfo, animated: true)
      }
      tipView?.handleClickAddFriendButtonCallback({ [weak self] (data:AddFriendButtonClickedCallbackData) in
        if data.code != 0 && data.code != 1005 {
          let localError = NSLocalizedString("添加好友消息发送失败", comment: "")
          XBHHUD.showError("\(localError):\(data.message)");
          return;
        }
        
        XBHHUD.showSuccess(NSLocalizedString("添加好友消息发送成功", comment: ""))
        self?.hideAddFriendView();
        
      });
      //点击黑名单
      tipView?.addBlCallback = { [weak self] in
        if (Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)?.isKind(of: ChatViewController.self))! {
          //如果还在当前页面就退出
          self?.dismiss(animated: true, completion: nil)
        }
      }
      tipView?.frame = CGRect.init(
        x: 0.0,
        y: addFriendTipHeight,
        width: Double(navFrame.width),
        height: addFriendTipHeight);
      self.tableView.contentInset.top += CGFloat(addFriendTipHeight);
      if let newView = tipView {
        navigationController?.view.addSubview(newView);
      }
    }
  }
  
  fileprivate func hideAddFriendView() {
    if tipView == nil {
      return;
    }
    tipView?.removeFromSuperview();
    self.tableView.contentInset.top -= CGFloat(addFriendTipHeight);
    tipView = nil;
  }
  
}

//小游戏邀请

extension ChatViewController:SendGameInviteProtocol {
  
  func gameInvite(data: JSON) {
   //发送游戏邀请
   
   if let gameType = data["type"].string,
    let gameName = data["name"].string,
    let icon = data["icon"].string {
        //发送邀请
        let payload = ["type":"invite","game_type":gameType,"target_user_id":self.peerID]
        messageDispatchManager.sendControlMessage(type:ControlMessage.play_mini_game.rawValue , payLoad:payload)
        let sendMessage = OLGameInviteMessage.init(attibutes: [
          "USER_SEX": "\(CurrentUser.shareInstance.sex)",
          "USER_ICON": CurrentUser.shareInstance.avatar,
          "USER_NAME": CurrentUser.shareInstance.name,
          "USER_ID": CurrentUser.shareInstance.id,
          "APP": Config.app,
          "MINI_GAME_TYPE": gameType,
          "MINI_GAME_NAME":gameName,
          "MINI_COUNTDOWN_TIME":30, //单位秒
          "MINI_GAME_ICON":icon,
          "CHAT_TIME": Utils.getCurrentTimeStamp() - Int64(messageDispatchManager.serverTime ?? 0) 
          ]);
        self.sendCustomMessage(sendMessage);
      let dic = ["USER_NAME":self.peerName,"USER_SEX":self.peerSex,"USER_ICON":self.peerIcon,"MESSAGE_TYPE":"MESSAGE_TYPE_CHAT","USER_ID":self.peerID,"CHAT_TIME":String(Utils.getCurrentTimeStamp()),"CHAT_MESSAGE":"邀请发送成功","READ":true,"APP": Config.app] as [String : Any]
      NotificationCenter.default.post(name:NotifyName.kSendMessToRN.name(), object:dic)
    }
  }
}

// 把送礼物外提
extension ChatViewController: giftSend {

  func  addSendGiftButton() {
    if let keyWindow = Utils.getKeyWindow() {
      keyWindow.addSubview(giftButton);
      keyWindow.bringSubview(toFront: giftButton);
    }
  }
  
  func addGameButton() {
    if let keyWindow = Utils.getKeyWindow() {
      let giftImage = UIImage(named: "小游戏小按钮")
      gameButton.frame = CGRect(x: Screen.width - 60, y: Screen.height * 0.7 - 70, width: 55, height: 55);
      gameButton.setImage(giftImage, for: .normal)
      gameButton.addTarget(self, action: #selector(showGame), for:.touchUpInside)
      gameButton.imageView?.contentMode = .scaleAspectFit
      keyWindow.addSubview(gameButton);
      keyWindow.bringSubview(toFront: gameButton);
    }
  }
  
  func removeSendGiftButton() {
    giftButton.removeFromSuperview();
  }
  
  func removeGameButton() {
    gameButton.removeFromSuperview()
  }
  
  func showGame() {
    if SwiftConverter.shareInstance.isGaming() {
      XBHHUD.showError(NSLocalizedString("您已经在一局游戏中", comment: ""))
      return
    }
    let game = GameListView.initGameView()
    Utils.getKeyWindow()?.addSubview(game)
    game.delegate =  self
    game.animation = "fadeIn"
    game.animate()
    game.getGame()
  }
  
  func showSendGift() {
    let gift = giftView.initGiftViewWith(envtype: .PROFILE, People:peerName)
    gift.toID = peerID
    gift.delegate = self
    Utils.getKeyWindow()?.addSubview(gift)
    gift.animation = "fadeIn"
    gift.animate()
    gift.getGiftFormRemote()
  }
  
  func sendGift(type: giftType, to: PlayerView?) {
    let gift = OLGiftType.changeGiftTypeToOLGiftType(type: type);
    let sendMessage = OLGiftMessage.init(attibutes: [
      "USER_SEX": "\(CurrentUser.shareInstance.sex)",
      "USER_ICON": CurrentUser.shareInstance.avatar,
      "USER_NAME": CurrentUser.shareInstance.name,
      "USER_ID": CurrentUser.shareInstance.id,
      "APP": Config.app,
      GIFT_MSG_KEY_GITF_TYPE: gift.rawValue,
      "CHAT_TIME": Utils.getCurrentTimeStamp()
      ]);
    self.sendCustomMessage(sendMessage);
    
    //此时也要发送通知，告诉RN更新
    XBHHUD.showTextOnly(text: "礼物赠送成功")
    let dic = ["USER_NAME":self.peerName,"USER_SEX":self.peerSex,"USER_ICON":self.peerIcon,"MESSAGE_TYPE":"MESSAGE_TYPE_CHAT","USER_ID":self.peerID,"CHAT_TIME":String(Utils.getCurrentTimeStamp()),"CHAT_MESSAGE":"🎁赠送成功","READ":true,"APP": Config.app] as [String : Any]
    NotificationCenter.default.post(name:NotifyName.kSendMessToRN.name(), object:dic)
  }
  
  func sendGiftWithRebate(type: String, to: PlayerView?, json: JSON) {
    
    var rebateValue = ""
    if let rebate = json["rebate"].dictionary {
      if let reValue = rebate["value"]?.int {
        rebateValue = "\(reValue)"
      }
    }
    let sendMessage = OLGiftMessage.init(attibutes: [
      "USER_SEX": "\(CurrentUser.shareInstance.sex)",
      "USER_ICON": CurrentUser.shareInstance.avatar,
      "USER_NAME": CurrentUser.shareInstance.name,
      "USER_ID": CurrentUser.shareInstance.id,
      GIFT_MSG_KEY_GITF_TYPE: type,
      "GIFT_REBATE":rebateValue,
      "APP": self.messageFromApp,
      "CHAT_TIME": Utils.getCurrentTimeStamp()
      ]);
    self.sendCustomMessage(sendMessage);
    
    //此时也要发送通知，告诉RN更新
    XBHHUD.showTextOnly(text: "礼物赠送成功")
    let dic = ["USER_NAME":self.peerName,"USER_SEX":self.peerSex,"USER_ICON":self.peerIcon,"MESSAGE_TYPE":"MESSAGE_TYPE_CHAT","USER_ID":self.peerID,"CHAT_TIME":String(Utils.getCurrentTimeStamp()),"CHAT_MESSAGE":"🎁赠送成功","READ":true,"APP": Config.app] as [String : Any]
    NotificationCenter.default.post(name:NotifyName.kSendMessToRN.name(), object:dic)

  }

  func sendGift(typeString: String, to: PlayerView?) {
    let sendMessage = OLGiftMessage.init(attibutes: [
      "USER_SEX": "\(CurrentUser.shareInstance.sex)",
      "USER_ICON": CurrentUser.shareInstance.avatar,
      "USER_NAME": CurrentUser.shareInstance.name,
      "USER_ID": CurrentUser.shareInstance.id,
      GIFT_MSG_KEY_GITF_TYPE: typeString,
      "APP": Config.app,
      "CHAT_TIME": Utils.getCurrentTimeStamp()
      ]);
    self.sendCustomMessage(sendMessage);
    
    //此时也要发送通知，告诉RN更新
      XBHHUD.showTextOnly(text: "礼物赠送成功")
      let dic = ["USER_NAME":self.peerName,"USER_SEX":self.peerSex,"USER_ICON":self.peerIcon,"MESSAGE_TYPE":"MESSAGE_TYPE_CHAT","USER_ID":self.peerID,"CHAT_TIME":String(Utils.getCurrentTimeStamp()),"CHAT_MESSAGE":"🎁赠送成功","READ":true,"APP": Config.app] as [String : Any]
      NotificationCenter.default.post(name:NotifyName.kSendMessToRN.name(), object:dic)
  }
}
