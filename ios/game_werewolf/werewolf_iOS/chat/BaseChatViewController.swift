//
//  BaseChatViewController.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/11/1.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import ChatKit

class BaseChatViewController: LCCKConversationViewController {
  
  // MSG_TYPE: FAMILY | SINGLE | noplay
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.messageFromApp = Config.app;
    messageType = "SINGLE";
    navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "room_bg_navbar"), for: .default)
    
    chatBar.startVoiceWhenGaming = {
      let local = NSLocalizedString("正在游戏中，不能发送语音", comment: "")
      XBHHUD.showError("\(local)")
    }
    chatBar.isGaming = SwiftConverter.shareInstance.isGaming()
    
    navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "room_icon_leftarrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(BaseChatViewController.leftClicked))
    
    NotificationCenter.default.addObserver(self, selector: #selector(BaseChatViewController.chatKitLogOut), name: NotifyName.kChatKitLogInOut.name(), object: nil)
    
    if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeAudio) != .authorized && AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeAudio) != .notDetermined {
      let localVocie = NSLocalizedString("需要获取您的麦克风权限才能发送语音消息，是否现在去设置？", comment: "")
      let view = NormalAlertView.view(type: .twoButton, title: "提示", detail:"\(localVocie)")
      view.conformClickBlock = {
        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
      }
      Utils.showAlertView(view: view)
    }
    LCCKUserSystemService.sharedInstance()
  }
  
  func leftClicked() {
    if (self.navigationController?.childViewControllers.count == 1) {
      dismiss(animated: true, completion: nil)
    }else{
      self.navigationController?.popViewController(animated: true)
    }
  }
  
  func chatKitLogOut() {
    if (self.navigationController != nil) {
      self.navigationController?.popViewController(animated: true)
    }else{
      dismiss(animated: true, completion: nil)
    }
  }
}

