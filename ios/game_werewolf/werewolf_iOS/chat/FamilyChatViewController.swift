//
//  FamilyChatViewController.swift
//  game_werewolf
//
//  Created by zhihanhe on 2017/10/25.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation
import ChatKit

class FamilyChatViewController: BaseChatViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad();
    messageType = "FAMILY";
    self.viewDidAppearBlock = {(controller:LCCKBaseViewController?, bool:Bool)  in
      Utils.runInMainThread { [weak self] in
        if let titleVIew = self?.navigationItem.titleView as? LCCKConversationNavigationTitleView {
          titleVIew.conversationNameView.text = "家族群聊";
          if #available(iOS 11, *) {
            titleVIew.snp.makeConstraints({ (make) in
              make.left.equalTo((Screen.width - titleVIew.width)/2)
            })
            for view in titleVIew.subviews {
              if view.isKind(of: UIStackView.self) {
                view.snp.makeConstraints({ (make) in
                  make.edges.equalToSuperview()
                })
              }
            }
          }
        }
        self?.customerRightButtono(hasNew: false)
        self?.newMem()
      }
    }
  }
  
  func customerRightButtono(hasNew:Bool){
    let moreButton = UIButton()
    if hasNew {
      moreButton.setImage(UIImage(named:"小红点-iOS"), for: .normal)
    }else{
      moreButton.setImage(UIImage(named:"成员按钮-iOS"), for: .normal)
    }
    if #available(iOS 11, *) {
      moreButton.snp.makeConstraints { (make) in
        make.width.equalTo(27)
        make.height.equalTo(19)
      }
    } else {
      moreButton.frame = CGRect(x: 0, y: 0, width: 27, height: 19)
    }
    moreButton.addTarget(self, action: #selector(FamilyChatViewController.test), for: .touchUpInside)
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: moreButton)
  }
  
  func newMem() {
    CurrentUser.shareInstance.getFamilyInfo { (family:Family?) in
      if let currentFamily = family {
          RequestManager().get(url:RequestUrls.groupInfo + currentFamily.group_id, success: {[weak self] (json) in
            XBHHUD.hide()
            if let new = json["group"]["require_count"].int{
              if new > 0  {
                self?.customerRightButtono(hasNew: true)
              }
            }
          }) { (code, message) in
          }
      }
    }
  }
  
  override func leftClicked() {
    super.leftClicked();
    self.viewDidAppearBlock = nil;
    if self.conversationId != nil {
      RNMessageSender.emitEvent(name: "LEAVE_EVENT_CHAT", andPayload: [
        "CONVERSATION_ID":self.conversationId,
        "CONVERSATION_TYPE":self.messageType
        ])
    }
    NotificationCenter.default.removeObserver(self);
  }
  
  func test() {
    XBHHUD.showLoading()
    CurrentUser.shareInstance.getFamilyInfo { (family:Family?) in
      if let currentFamily = family {
        let para = ["maxUser":"30"]
        RequestManager().get(url:RequestUrls.groupInfo + currentFamily.group_id,parameters: para, success: { (json) in
          DispatchQueue.main.async {
            let sb = UIStoryboard.init(name: "FamilyDetail", bundle: nil)
            let familyDetail = sb.instantiateViewController(withIdentifier: "FamilyDetail") as! FamilyDetailViewController
            familyDetail.datas = json
            let nav = RotationNavigationViewController.init(rootViewController: familyDetail)
            XBHHUD.hide()
            ((Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)!) as UIViewController).present(nav, animated: true, completion: nil)
          }
        }) { (code, message) in
          XBHHUD.showError(message)
        }
      } else {
        XBHHUD.showError("家族信息异常")
      }
    }
  }
}
