//
//  ChatRoomUserView+UserOperation.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/30.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class MyLabel: UILabel {
  var outLineColor = UIColor.white
  
  override func drawText(in rect: CGRect) {
    let textColor = self.textColor
    let c = UIGraphicsGetCurrentContext()
    c?.setLineWidth(0.5)
    c?.setLineJoin(CGLineJoin.round)
    c?.setTextDrawingMode(CGTextDrawingMode.stroke)
    self.textColor = self.outLineColor
    super.drawText(in: rect)
    
    c?.setTextDrawingMode(.fill)
    self.textColor = textColor
    self.shadowOffset = CGSize(width: 0, height: 0)
    super.drawText(in: rect)
    
    self.shadowOffset = shadowOffset
  }
}

extension ChatRoomUserView {
  
  enum GameUserState {
    case readyState
    case leaveState
    case hideState
  }
  
  
  func setGameUserState(_ state: GameUserState) -> Void {
    switch state {
    case .readyState:
      rightTopLabel.isHidden = false
      rightTopLabel.text = "准备中".localized
      rightTopLabel.backgroundColor = UIColor.yellow
      rightTopLabel.textColor = UIColor.textPurpleDarkColor()
    case .leaveState:
      rightTopLabel.isHidden = false
      rightTopLabel.text = "离线中".localized
      rightTopLabel.backgroundColor = UIColor(hexInt: 0xb2b2b2)
      rightTopLabel.textColor = UIColor.textPurpleDarkColor()
    default:
      rightTopLabel.isHidden = true
    }
  }
    
  func speakAnimation(_ isSpeak: Bool) -> Void {
    if isSpeak {
      speakImage.startAnimating()
    } else {
      speakImage.stopAnimating()
    }
  }
  
  func appendModel(_ model: ChatGifModel) -> Void {
    animationArray.append(model)
    if self.observerCount == 0 {
      playAnimations()
    }
  }
  
  func canSendGif(_ isCan: Bool) -> Void {
    if Int(number) == CurrentUser.shareInstance.currentPosition {
      NotificationCenter.default.post(name: NotifyName.kToolBarGifCanSelected.name(), object: isCan)
    }
  }
  
  func setLabelWithNumber(_ frame: CGRect, _ number: Int) -> Void {
    let label = UILabel()
    label.backgroundColor = UIColor.pinkColor()
    label.textColor = UIColor.white
    label.text = "\(number)"
    label.font = UIFont.systemFont(ofSize: 11)
    label.layer.cornerRadius = 7.5
    label.textAlignment = .center
    label.layer.masksToBounds = true
    label.frame = frame
    votesView.addSubview(label)
  }
  
  func setGameOpLabel(_ myLabel: MyLabel, _ textColor: UIColor, _ outLineColor: UIColor) -> Void {
    myLabel.textAlignment = .center
    myLabel.textColor = textColor
    myLabel.font = UIFont.boldSystemFont(ofSize: 13.5)
    myLabel.outLineColor = outLineColor
    self.setBordView(myLabel)
  }
  
  func setBordView(_ label: UILabel) -> Void {
    label.textAlignment = .center
    addSubview(label)
  }
  
  // 动画一次就停吧
  func playAnimations() -> Void {
    if self.observerCount != 0 { return }
    if let model = self.animationArray.first {
      currentGifModel = model
      self.animationArray.removeFirst()
      if model.mark == "applause" {
        Utils.playSound(name: "Gif_applause")
      }
      let gifImage = ChatVoiceRoomViewController.GifFileNameURL(rawValue: "Gif_" + model.mark) ?? .UnKnown
      if gifImage == .UnKnown {        // 本地没有
        if  let url = URL.init(string: model.url) {
          self.animationView.sd_setImage(with: url, completed: { [weak self] (_, _, _, _) in
            self?.animationView.isHidden = false
            self?.observerCount = 0
          })
          canSendGif(false)
        }
        self.searchDBSource(model.mark)     // 确认一下数据库是否有此文件
      } else {                              // 如果本地有图片
        self.animationView.image = gifImage.GifFile() ?? YYImage(named: "Gif_流汗_dynamic")!
        self.animationView.isHidden = false
        self.observerCount = 0
        canSendGif(false)
      }
    }
  }
  
  func searchDBSource(_ mark: String) -> Void {
    DispatchQueue.global().async {
      guard let _ = GifDBManager.queryDB(mark) else {
        RequestManager().get(url: RequestUrls.audioEmot, success: { (success) in
          GifDBManager.removeDBSource()
          let _ = GifDBManager.openDB()
          for model in success.arrayValue {
            let url = model["url"].stringValue
            let mark = model["mark"].stringValue
            let thum = model["thumbnail"].stringValue
            let _ = GifDBManager.insertToDB(ChatGifModel(url: url, mark: mark, thumbnail: thum, res_url: "", res: "", sound: "", stay_time: 0))
          }
          GifDBManager.closeDB()
        }, error: { (code, message) in
          
        })
        return
      }
    }
  }
  
  func onceDelayGameOver(_ time: Int) -> Void {
    self.currentGifModel = nil
    DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + TimeInterval(time), execute: { [weak self] in
      DispatchQueue.main.async {
        self?.staticImageView.isHidden = true
        self?.animationView.isHidden = true
        self?.canSendGif(true)
        self?.observerCount = 0
        self?.setGameLabel("")
        self?.playAnimations()
      }
    })
  }
  
  func setGameLabel(_ res: String) -> Void {
    switch res.length {
    case 0:
      self.numberLabels.forEach({ (label) in
        label.text = ""
      })
    case 1:
      self.numberLabels[1].text = "0"
      self.numberLabels[2].text = "0"
      self.numberLabels[3].text = res
    case 2:
      self.numberLabels[1].text = "0"
      self.numberLabels[2].text = res.slicing(from: 0, length: 1)
      self.numberLabels[3].text = res.slicing(from: 1, length: 1)
    default:
      self.numberLabels[1].text = res.slicing(from: 0, length: 1)
      self.numberLabels[2].text = res.slicing(from: 1, length: 1)
      self.numberLabels[3].text = res.slicing(from: 2, length: 1)
    }
  }
  
  func setRightBottomImage(_ state: String) -> Void {
    switch state {
    case "free":
      rightBottomImage.isHidden = false
      rightBottomImage.image = UIImage.init(named: "chatRoom_User_rightFree")
    case "limit":
      rightBottomImage.isHidden = false
      rightBottomImage.image = UIImage.init(named: "chatRoom_userState_Limit")
    default:
      rightBottomImage.isHidden = true
    }
  }
  
  
}

extension ChatRoomUserView: SRCountdownTimerDelegate {
  func timerDidEnd() {
    cutDown.isHidden = true
    endSpeechButton.isHidden = true
  }
  func timerDidStart() {
    cutDown.isHidden = false
    if "\(CurrentUser.shareInstance.currentPosition)" == number {
      endSpeechButton.isHidden = false
    }
  }
}
