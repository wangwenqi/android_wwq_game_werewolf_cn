//
//  ChatRoomOnLineLabel.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/10.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomOnLineLabel: UIView {
  
  var onLine = 1 {
    didSet {
      let number = NSMutableAttributedString(string: "\(onLine) ", attributes:[NSForegroundColorAttributeName: UIColor(hex: "#fd6b97"),
                                                                              NSFontAttributeName: UIFont.systemFont(ofSize: 12)])
      number.append(onLineHelper)
      onLineNumberLabel.attributedText = number
    }
  }
  var roomId = "" {
    didSet {
      let number = NSAttributedString(string: ": \(roomId)", attributes: [NSForegroundColorAttributeName: UIColor.white,
                                                                          NSFontAttributeName: UIFont.systemFont(ofSize: 12)])
      let roomNumberHelper = NSMutableAttributedString(string: "房间号".localized,
                                                       attributes: [NSForegroundColorAttributeName: UIColor(hex: "#fcbf3e"),
                                                                    NSFontAttributeName: UIFont.systemFont(ofSize: 12)])
      roomNumberHelper.append(number)
      roomNumberLabel.attributedText = roomNumberHelper
    }
  }
  let roomNumberLabel = UILabel()
  let onLineNumberLabel = UILabel()
  let onLineHelper = NSAttributedString(string: "人同时在线",
                                                       attributes: [NSForegroundColorAttributeName: UIColor.white,
                                                                    NSFontAttributeName: UIFont.systemFont(ofSize: 13)])
  
  fileprivate let rightImage = UIImageView.init(image: UIImage(named: "chatRoom_smile_Icon"))
  let roomImage = UIImageView(image: UIImage(named: "chatRoom_userIcon_Icon"))
  fileprivate let backBottomView = UIView()
  fileprivate let backTopView = UIView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(backBottomView)
    addSubview(backTopView)
    addSubview(onLineNumberLabel)
    addSubview(rightImage)
    addSubview(roomNumberLabel)
    addSubview(roomImage)
    rightImage.contentMode = .scaleAspectFit
    roomImage.contentMode = .scaleAspectFit
    onLine = 1
    backBottomView.backgroundColor = UIColor.black
    backBottomView.alpha = 0.2
    backTopView.backgroundColor = UIColor.black
    backTopView.alpha = 0.2
    self.backgroundColor = UIColor.clear
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    onLineNumberLabel.frame = CGRect(x: 8, y: 28, width: 100, height: 28)
    rightImage.frame = CGRect(x: self.width - 32, y: 30, width: 26, height: 24)
    backBottomView.frame = CGRect(x: 0, y: 28, width: self.width, height: 28)
    setbackLayer(backBottomView)
    backTopView.frame = CGRect(x: 34, y: 2, width: self.width - 34, height: 25)
    setbackLayer(backTopView)
    roomImage.frame = CGRect.init(x: 10, y: 0, width: 21, height: 26)
    roomNumberLabel.frame = CGRect(x: 40, y: 2, width: self.width - 40, height: 25)
  }
  
  func setbackLayer(_ view: UIView) -> Void {
    let tmpLayer = CAShapeLayer.init()
    let bezierPath = UIBezierPath.init(roundedRect: view.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: 15, height: 15))
    tmpLayer.frame = view.bounds
    tmpLayer.path = bezierPath.cgPath
    view.layer.mask = tmpLayer
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("failInit")
  }
  
}
