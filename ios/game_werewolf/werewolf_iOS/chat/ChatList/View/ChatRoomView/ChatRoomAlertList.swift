//
//  ChatRoomAlertList.swift
//  game_werewolf
//
//  Created by Tony on 2017/9/7.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomAlertList: BaseNoticeView {
  
    @IBOutlet weak var tableViewBackView: UIView!
    @IBOutlet weak var titleLabel: LTMorphingLabel!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var tableViewArea: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    
    weak var chatListVC: ChatListViewController?
  
    override func initial() {
      self.width = Screen.width * 0.85
      self.height = Screen.height * 0.7
      self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
      self.backgroundColor = UIColor(hex: "#e2dee8")
      self.layer.cornerRadius = 6
    }

    override func awakeFromNib() {
      super.awakeFromNib()
      tableViewArea.layer.cornerRadius = 6
      tableViewArea.layer.masksToBounds = true
      cancelButton.layer.cornerRadius = 5
      cancelButton.layer.masksToBounds = true
    }
  
    override func layoutSubviews() {
      super.layoutSubviews()
    }
  
  class func showSendGiftListView(_ chatList: ChatListViewController, _ currentIndex: Int) -> ChatRoomAlertList {
      let chatListView = Bundle.main.loadNibNamed(ChatRoomAlertList.name(), owner: nil, options: nil)?.last as! ChatRoomAlertList
      chatListView.chatListVC = chatList
      chatListView.chatListVC?.currentIndex = currentIndex
      if let _ = chatListView.chatListVC {
        chatListView.tableViewBackView.addSubview(chatListView.chatListVC!.view)
        chatListView.chatListVC!.view.snp.makeConstraints { (make) in
          make.edges.equalToSuperview()
        }
      }
      return chatListView
    }
  
  
    @IBAction func cancelButtonAction(_ sender: Any) {
      Utils.hideNoticeView()
    }
  
}
