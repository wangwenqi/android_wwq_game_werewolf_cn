//
//  ChatBlackCell.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/10.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatBlackCell: UITableViewCell {

    @IBOutlet weak var headImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    var model: ChatBlackModel? {
      didSet {
        if let model = model {
          headImageView.setImage(urlString: model.image, placeholderImage: UIImage.init(named: "room_head_default"))
          nameLabel.text = model.name
        }
      }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      headImageView.layer.cornerRadius = 25
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        headImageView.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
