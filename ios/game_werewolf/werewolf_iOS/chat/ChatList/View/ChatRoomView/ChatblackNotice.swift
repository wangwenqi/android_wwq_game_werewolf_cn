//
//  ChatblackNotice.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/9.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatblackNotice: BaseNoticeView {

    var selectedBlock: ((Bool) -> Void)?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sureButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func initial() {
      self.width = Screen.width * 0.85
      self.height = 180
      self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
      self.layer.cornerRadius = 6
      self.layer.masksToBounds = true
    }
  
    class func showChatblackNotice(_ info: String) -> ChatblackNotice {
      let view = Bundle.main.loadNibNamed(ChatblackNotice.name(), owner: nil, options: nil)?.last as! ChatblackNotice
      view.infoLabel.text = info
      return view
    }
  
    @IBAction func sureAction(_ sender: Any) {
      selectedBlock?(true)
      Utils.hideNoticeView()
    }
    @IBAction func cancelAction(_ sender: Any) {
      selectedBlock?(false)
      Utils.hideNoticeView()
    }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    cancelButton.layer.cornerRadius = 5
    cancelButton.layer.masksToBounds = true
    sureButton.layer.cornerRadius = 5
    sureButton.layer.masksToBounds = true
    titleLabel.text = "提 示".localized
    cancelButton.setTitle("取 消", for: .normal)
    sureButton.setTitle("确 认".localized, for: .normal)
  }

    
}
