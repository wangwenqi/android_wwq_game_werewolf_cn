//
//  ChatNoticeView.swift
//  game_werewolf
//
//  Created by Tony on 2017/9/13.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatNoticeView: UIControl {

  enum ChatRoomUserState {
    case leaveRoom
    case enterRoom
    case upSeat
    case downSeat
    case forceSeat
    case kickout
    case freeModel
    case hide
  }
  
  var noticeUpdate: ((CGFloat, CGFloat, ChatRoomUserState) -> Void)?
  var useTime: Timer?
  
  func notice(name: String?, state: ChatRoomUserState) -> Void {
    useTime?.fireDate = Date.init(timeIntervalSinceNow: 3)
    if let name = name {
      let rect = name.sizeWithGivenSize(CGSize(width: 200, height: 25), font: UIFont.systemFont(ofSize: 12))
      let cnWidth: CGFloat = rect.width > 100 ? 80.0 : rect.width
      let cnHeight: CGFloat = rect.width > 100 ? 40 : 25
      nameLabel.frame = CGRect(x: 10, y: 0, width: cnWidth, height: cnHeight)
      infoLabel.frame = CGRect(x: 10 + cnWidth + 5, y: 0, width: 50, height: cnHeight)
      clearBack.frame = CGRect(x: 0, y: 0, width: 80 + cnWidth, height: cnHeight)
      nameLabel.text = name
      switch state {
      case .leaveRoom:
        infoLabel.text = "离开房间".localized
      case .enterRoom:
        infoLabel.text = "进入房间".localized
      case .upSeat:
        infoLabel.text = "已上麦".localized
      case .downSeat:
        infoLabel.text = "已下麦".localized
      case .forceSeat:
        infoLabel.text = "被抱上麦".localized
      case .kickout:
        infoLabel.text = "被房主踢出房间".localized
      default:
        nameLabel.text = nil
        infoLabel.text = nil
        break
      }
      noticeUpdate?(70 + cnWidth, cnHeight, state)
    }
  }
  
  let nameLabel = UILabel()
  let infoLabel = UILabel()
  let clearBack = UIView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    clearBack.backgroundColor = UIColor.black
    clearBack.alpha = 0.2
    clearBack.layer.cornerRadius = 10
    self.addSubview(clearBack)
    nameLabel.font = UIFont.systemFont(ofSize: 12)
    nameLabel.numberOfLines = 2
    nameLabel.textColor = UIColor(hex: "#4877fc")
    infoLabel.font = UIFont.systemFont(ofSize: 12)
    infoLabel.textColor = UIColor(hex: "#e6e6e6")
    self.addSubview(nameLabel)
    self.addSubview(infoLabel)
    useTime = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(ChatNoticeView.hideView), userInfo: nil, repeats: true)
  }
  
  func hideView() {
    noticeUpdate?(0, 25, .hide)
    useTime?.fireDate = Date.distantFuture
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    nameLabel.adjustsFontSizeToFitWidth = true
  }
  
  deinit {
    useTime?.invalidate()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}
