//
//  ChatRoomTitleView.swift
//  game_werewolf
//
//  Created by Tony on 2017/9/4.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomTitleView: UIView {

  var endEditBlock: ((CGFloat, String?)->Void)?
  var titleText: String? {
    didSet {
      self.titleTextField.text = titleText
      numberLabel.text = "\(titleTextField.text?.length ?? 0)/15"
      isEditState = false
    }
  }
  
  var isEditState = false {
    didSet {
      self.setNeedsLayout()
    }
  }
  
  let editImageView = UIImageView.init(image: UIImage(named: "chatRoom_Edit_Icon"))
  private let loudImageView = UIImageView.init(image: UIImage(named: "chatRoom_loudOpen_Icon"))
  let titleTextField = UITextField()
  private let saveButton = UIButton(type: .system)
  private let lightBackView = UIView.init()
  let numberLabel = UILabel()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(lightBackView)
    addSubview(loudImageView)
    addSubview(titleTextField)
    addSubview(editImageView)
    addSubview(numberLabel)
    addSubview(saveButton)
    editImageView.contentMode = .center
    loudImageView.contentMode = .center
    
    titleTextField.font = UIFont.systemFont(ofSize: 12)
    titleTextField.textColor = UIColor.white
    
    titleTextField.attributedPlaceholder = NSAttributedString(string: "输入聊天主题".localized, attributes: [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 12)])
    
    numberLabel.text = "0/15"
    titleTextField.textAlignment = .center
    titleTextField.delegate = self
    titleTextField.returnKeyType = .send
    numberLabel.textColor = UIColor(hex: "#9d9ead")
    numberLabel.font = UIFont.systemFont(ofSize: 12)
    numberLabel.textAlignment = .center
    
    saveButton.backgroundColor = UIColor(hex: "#d949ad")
    saveButton.setTitleColor(UIColor.white, for: .normal)
    saveButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
    saveButton.setTitle("保存", for: .normal)
    saveButton.layer.cornerRadius = 5
    saveButton.layer.masksToBounds = true
    saveButton.addTarget(self, action: #selector(saveTitleAction), for: .touchUpInside)
    lightBackView.backgroundColor = UIColor.white
    lightBackView.layer.cornerRadius = 5
    lightBackView.layer.masksToBounds = true
    lightBackView.alpha = 0.2
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func saveTitleAction() -> Void {
    isEditState = false
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    changeEditState()
  }
  
  func changeEditState() -> Void {
    saveButton.isHidden = isEditState ? false : true
    numberLabel.isHidden = isEditState ? false : true
    titleTextField.isUserInteractionEnabled = isEditState ? true : false
    loudImageView.isHidden = isEditState ? true : false
    editImageView.isHidden = isEditState ? true : false
    lightBackView.backgroundColor = isEditState ? UIColor.white : UIColor.clear
    
    if let text = self.titleTextField.text, !isEditState {
      if let text = titleTextField.text, !text.isEmpty, text.length >= 4 {
      } else {
        if text.containsEmoji, text.length >= 2 { }
        else {
          titleTextField.text = self.titleText ?? "设置聊天内容".localized
//          XBHHUD.showError("主题要大于4个字哦~".localized)
        }
      }
      
      let rect = titleTextField.text!.sizeWithGivenSize(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 18), font: UIFont.systemFont(ofSize: 12), paragraph: nil)
      UIView.animate(withDuration: 0.2, animations: { [unowned self] _ in
        self.loudImageView.frame = CGRect(x: 0, y: 0, width: 18, height: 18)
        self.titleTextField.frame = CGRect(x: self.loudImageView.maxX + 5, y: 0, width: rect.width, height: rect.height)
        self.editImageView.frame = CGRect(x: self.titleTextField.maxX + 5, y: 0, width: 18, height: 18)
      })
      endEditBlock?(rect.width + 10 + 36, titleTextField.text)
    } else {
      UIView.animate(withDuration: 0.2, animations: { [unowned self] _ in
        self.titleTextField.frame = CGRect(x: 40, y: 0, width: Screen.width - 135 - 10 - 80, height: 18)
        self.numberLabel.frame = CGRect(x: self.titleTextField.maxX + 5, y: 0, width: 35, height: 18)
        self.saveButton.frame = CGRect(x: self.numberLabel.maxX + 5, y: 0, width: 100, height: 18)
      })
      endEditBlock?(Screen.width, titleTextField.text)
    }
    UIView.animate(withDuration: 0.2) {
      self.lightBackView.frame = self.titleTextField.frame
    }
  }
  
}

extension ChatRoomTitleView: UITextFieldDelegate {
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let textLength = textField.text?.length ?? 0
    let newLength = textLength + string.length - range.length
    if newLength >= 15 {
    } else {
      numberLabel.text = "\(newLength)/15"
    }
    return newLength <= 15
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    isEditState = false
    self.endEditing(true)
    return true
  }
  
}
