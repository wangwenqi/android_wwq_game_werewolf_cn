//
//  ChatRoomAlertSheet.swift
//  game_werewolf
//
//  Created by Tony on 2017/9/12.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit


class ChatEmptyBackView: UIViewController {
  
  let CEback = UIControl()
  let CEalertView = UIView()
  var CEHeight: CGFloat = 0
  init(height: CGFloat, title: String?) {
    super.init(nibName: nil, bundle: nil)
    CEHeight = height
    self.modalPresentationStyle = .overCurrentContext
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor.clear
    view.addSubview(CEback)
    view.addSubview(CEalertView)
    
    CEback.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
    
    CEback.snp.makeConstraints { (make) in
      make.edges.equalToSuperview()
    }
    
    CEalertView.backgroundColor = UIColor(hex: "#2E1251")
    CEalertView.snp.makeConstraints { (make) in
      make.left.right.bottom.equalToSuperview()
      make.height.equalTo(CEHeight)
    }
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(true)
    CEback.backgroundColor = UIColor.black
    self.CEback.alpha = 0.05
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    self.CEback.alpha = 0.0
  }
  
  func dismissView() -> Void {
    self.dismiss(animated: true, completion: nil)
  }
}
