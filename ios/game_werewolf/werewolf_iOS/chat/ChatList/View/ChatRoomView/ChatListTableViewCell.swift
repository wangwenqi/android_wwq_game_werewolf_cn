//
//  ChatListTableViewCell.swift
//  game_werewolf
//
//  Created by Tony on 2017/9/6.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import SDWebImage

class ChatListTableViewCell: UITableViewCell {

    @IBOutlet weak var kickOut: UIButton!
    var model: ChatRoomUserModel? {
      didSet {
        if let model = model {
            if !model.avatar.isEmpty {
              headerImageView.sd_setImage(with: URL(string: model.avatar), placeholderImage: UIImage(named: "room_head_default"))
            }
          nameLabel.text = model.name
          signatureLabel.text = model.signature
          switch model.userRoomState {
          case .roomOfMaster:
            roomStateLabel.isHidden = false
            roomStateLabel.text = "房主"
          case .roomOfOnMicro:
            roomStateLabel.isHidden = false
            roomStateLabel.text = "上麦".localized
          default:
            roomStateLabel.isHidden = true
          }
          
        }
      }
    }
  
    @IBOutlet weak var signatureLabel: UILabel!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roomStateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      roomStateLabel.layer.cornerRadius = 5
      roomStateLabel.layer.masksToBounds = true
      headerImageView.layer.masksToBounds = true
      kickOut.layer.cornerRadius = 5
      kickOut.layer.masksToBounds = true
    }
  
    @IBAction func kickoutAction(_ sender: Any) {
      if !CurrentUser.shareInstance.messageShow,
        (model?.userRoomState == .roomOfMaster || model?.userRoomState == .roomOfOnMicro) {
        XBHHUD.showError("游戏中不可拉黑麦上的人".localized)
        return
      }
      if model?.id == CurrentUser.shareInstance.roomOwnerId,
        CurrentUser.shareInstance.ownerType == "purchased" {
        XBHHUD.showError("不可拉黑房契拥有者".localized)
        return
      }
      let noticeString = "确定要拉黑并踢出" + (model?.name ?? "") + "么?"
      let noticeView = ChatblackNotice.showChatblackNotice(noticeString)
      noticeView.selectedBlock = { [weak self] isChoose in
        if isChoose {
          messageDispatchManager.sendMessage(type: .kick_out, payLoad: ["uid": self?.model?.id ?? "", "block": 1])
        }
      }
      Utils.showAlertView(view: noticeView)
    }
    
    override func layoutSubviews() {
    super.layoutSubviews()
    headerImageView.layer.cornerRadius = headerImageView.width / 2
  }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
