//
//  ChatRoomVoiceToolBar.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/30.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomVoiceToolBar: UIView {
  
    var speechBlock: ((Bool) -> Void)?
    var inputTextBlock: ((Bool, CGFloat) -> Void)?
    var sendMessageBlock: ((String) -> Void)?
    var sendGiftBlock: (() -> Void)?
    var sendGifImageBlock: ((Bool) -> Void)?
    var micState = false
    let gifView = ChatRoomShowGif(frame: .zero, type: .faceGif)
    var changeUserState = "" {
      didSet {
        switch changeUserState {
        case "free":
          self.controlButton.setImage(UIImage(named: "chatRoom_FreeMic_State"), for: .normal)
          isOnMir = false
          micState = true
          speechBlock?(true)
        case "limit":
          self.controlButton.setImage(UIImage(named: "chatRoom_Free_closeMic"), for: .normal)
          isOnMir = false
          micState = false
          speechBlock?(false)
        default:
          self.controlButton.setImage(UIImage(named: "chatRoom_toolBar_changeKeyboard"), for: .normal)
          isOnMir = true
          speechBlock?(false)
        }
      }
    }
    var isOnMir = false {
      didSet {
        if changeUserState == "" {
          sendGifButton.isHighlighted = !isOnMir
          sendGifButton.isUserInteractionEnabled = isOnMir
          speechButton.isHidden = !isOnMir;
          speechTextField.isHidden = isOnMir
        } else {
          sendGifButton.isHighlighted = isOnMir
          sendGifButton.isUserInteractionEnabled = !isOnMir
          speechButton.isHidden = !isOnMir;
          speechTextField.isHidden = isOnMir
        }
      }
    }

    @IBOutlet weak var giftButton: UIButton!
    @IBOutlet weak var sendGifButton: UIButton!
    @IBOutlet weak var controlButton: UIButton!
    @IBOutlet weak var speechTextField: UITextField!
    @IBOutlet weak var speechButton: XBHButton!
    @IBOutlet weak var rightSpace: NSLayoutConstraint!

  
    override func layoutSubviews() {
      super.layoutSubviews()
      gifView.frame = CGRect(x: 0, y: 50, width: width, height: (Screen.width - 45) / 3 + 35)
    }
  
      @IBAction func giftButtonAction(_ sender: Any) {
        sendGiftBlock?()
      }
  
      @IBAction func sendGifAction(_ sender: Any) {
        self.endEditing(true)
        sendGifImageBlock?(false)
//        if self.gifView.isHidden == true {
//          self.gifView.isHidden = false
//          self.gameView.isHidden = true
//        }
      }
    
//    func showGameGif() -> Void {
//      self.endEditing(true)
//      sendGifImageBlock?(false)
////      if self.gameView.isHidden == true {
////        self.gifView.isHidden = true
////        self.gameView.isHidden = false
////      }
//    }
  
    override func awakeFromNib() {
      super.awakeFromNib()
      NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(enterBackGround), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(showGifChooseView(notification:)), name: NotifyName.kToolBarGifCanSelected.name(), object: nil)
      
      speechButton.addTarget(self, action: #selector(speechTouchDown), for: .touchDown)
      speechButton.addTarget(self, action: #selector(speechTouchUp), for: .touchUpInside)
      speechButton.addTarget(self, action: #selector(speechTouchUp), for: .touchUpOutside)
      
      speechTextField.delegate = self
      speechTextField.returnKeyType = .send
      speechButton.layer.cornerRadius = 5
      speechButton.layer.masksToBounds = true
      speechButton.setTitleShadowColor(UIColor.clear, for: .normal)
      speechTextField.attributedPlaceholder = "输入你要说的话".localized.highLightedStringWith("输入你要说的话".localized, color: CustomColor.roomYellow)
      addSubview(gifView)
    }
  
  func showGifChooseView(notification: NSNotification) -> Void {
    if let show = notification.object as? Bool, show {
      canChooseGif(true)
    } else {
      canChooseGif(false)
    }
  }
  
  func canChooseGif(_ show: Bool) -> Void {
    self.gifView.alpha = show ? 1 : 0.5
    self.gifView.isUserInteractionEnabled = show
  }
  
    func keyBoardWillShow(notification: Notification) {
      let userInfo = notification.userInfo
      if let bounds = userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
        let keyBoardBounds = bounds.cgRectValue
        inputTextBlock?(true, (Screen.height - keyBoardBounds.origin.y))
      }
    }
    
    func keyBoardWillHide(notification: Notification) {
      if isOnMir {
        controlButton.isSelected = false;
        speechButton.isHidden = false;
        speechTextField.isHidden = true;
      } else {
        speechButton.isHidden = true;
        speechTextField.isHidden = false;
      }
      if let _ = inputTextBlock {
        inputTextBlock?(false, 0)
      }
    }
    
    deinit {
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
      NotificationCenter.default.removeObserver(self, name: NotifyName.kToolBarGifCanSelected.name(), object: nil)
    }

    func speechTouchDown() {
      DispatchQueue.global().async { [weak self] _ in
        DispatchQueue.main.async {
          self?.speechButton.addCustomHighlightLayer()
        }
        self?.speechBlock?(true)
      }
    }
  
    func enterBackGround() -> () {
      self.endEditing(true)
      if changeUserState != "free" {
        speechTouchUp()
      }
      inputTextBlock?(false, 0)
    }
  
    func speechTouchUp() {
      DispatchQueue.global().async { [weak self] _ in
        DispatchQueue.main.async {
          guard let weakSelf = self else { return }
          weakSelf.speechButton.removeCustomHighlightLayer();
        }
        self?.speechBlock?(false)
      }
    }
  
    @IBAction func controlButtonAction(_ sender: Any) {
      if changeUserState == "free" {
        self.speechBlock?(micState)
        if micState {
          self.controlButton.setImage(UIImage(named: "chatRoom_FreeMic_State"), for: .normal)
          micState = false
        } else {
          self.controlButton.setImage(UIImage(named: "chatRoom_Free_closeMic"), for: .normal)
          micState = true
        }
      } else if changeUserState == "limit" {
        XBHHUD.showError("您被房主禁言啦~".localized)
      } else {
        if isOnMir {
         speakStart()
        } else {
          let state = CurrentUser.shareInstance.isGameing
          if state != .isNotAGame, state != .isGameOverStates, state != .isUpdateConfig  {
            if CurrentUser.shareInstance.can_cut_speaker {
              speakStart()
            } else {
              XBHHUD.showError("还没有轮到你发言".localized)
            }
          } else {
            XBHHUD.showError("上麦后才可以发语音哦~".localized)
          }
        }
    }
  }
  
  func speakStart() -> Void {
    controlButton.isSelected = !controlButton.isSelected
    speechButton.isHidden = controlButton.isSelected
    speechTextField.isHidden = !controlButton.isSelected
    if controlButton.isSelected {
      // 打字
      speechTextField.becomeFirstResponder()
    } else {
      // 语音
      speechTextField.resignFirstResponder()
    }
  }
}

extension ChatRoomVoiceToolBar: UITextFieldDelegate {
  
    func textFieldDidBeginEditing(_ textField: UITextField) {
      sendGifImageBlock?(true)
    }
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      if textField.text?.isEmpty ?? true {
        return true
      }
      sendMessageBlock?(textField.text ?? "")
      textField.text = ""
      speechTextField.resignFirstResponder()
      return true
    }
  
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      let textLength = textField.text?.length ?? 0
      let newLength = textLength + string.length - range.length
      return newLength <= 500
    }
  
}


