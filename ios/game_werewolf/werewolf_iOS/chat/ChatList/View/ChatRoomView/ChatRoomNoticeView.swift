//
//  ChatRoomNoticeView.swift
//  game_werewolf
//
//  Created by Tony on 2017/9/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomNoticeView: BaseNoticeView {

  var leaveRoomBlock: (() -> Void)?
  let titleLabel = UILabel()
  let infoLabel = UILabel()
  let cancel = UIButton()
  let makeSure = UIButton()

  override func initial() {
    self.width = 280
    self.height = 175
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.backgroundColor = #colorLiteral(red: 0.2399157584, green: 0.118033506, blue: 0.3918479681, alpha: 1)
    self.layer.cornerRadius = 6
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    configView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func configView() -> Void {
    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
    addSubview(titleLabel)
    titleLabel.frame = CGRect(x: 0, y: 0, width: width, height: 50)
    titleLabel.textAlignment = .center
    
    infoLabel.textColor = UIColor.white
    infoLabel.frame = CGRect(x: 40, y: 50, width: 200, height: 80);
    infoLabel.textAlignment = .center
    infoLabel.font = UIFont.systemFont(ofSize: 14)
    addSubview(infoLabel)
    
    cancel.setTitle(NSLocalizedString("取消", comment: ""), for: .normal)
    cancel.frame = CGRect.init(x: 0, y: 130, width: 140, height: 45)
    cancel.addTarget(self, action: #selector(hiddenNotice), for: .touchUpInside)
    cancel.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
    addSubview(cancel)
    
    makeSure.setTitle(NSLocalizedString("确定", comment: ""), for: .normal)
    makeSure.frame = CGRect.init(x: 140, y: 130, width: 140, height: 45)
    makeSure.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
    makeSure.addTarget(self, action: #selector(makeSurePassword), for: .touchUpInside)
    addSubview(makeSure)
    
    for index in 0...1 {
      let whiteView = UIView(frame: CGRect(x: 2.0, y: 50 + Double(index) * 80.0, width: 276.0, height: 0.5))
      whiteView.backgroundColor = UIColor.white
      whiteView.alpha = 0.5
      addSubview(whiteView)
    }
    
    let whiteView = UIView(frame: CGRect(x: 140, y: 130, width: 0.5, height: 43))
    whiteView.backgroundColor = UIColor.white
    whiteView.alpha = 0.5
    addSubview(whiteView)
    
  }
  
  func makeSurePassword() -> Void {
    self.leaveRoomBlock?()
  }
  
  

}
