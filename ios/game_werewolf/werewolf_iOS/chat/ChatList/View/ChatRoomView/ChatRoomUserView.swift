//
//  ChatRoomUserView.swift
//  game_werewolf
//
//  Created by Tony on 2017/10/9.
//  Copyright © 2017年 orangelab. All rights reserved.
//

// 语音房 用户的头像cell
import UIKit
import SDWebImage

enum GameVoteState {
  case votesShow
  case readyShow
  case hideButton
}

class ChatRoomUserView: UIControl {
  
  let addImage = UIImage(named: "chatRoom_add_Icon")
  let closeImage =  UIImage(named: "chatRoom_itemClose_Icon")
  let headImage = UIImage.init(named: "room_head_default")
  // 是否在玩游戏中
  var isGameing = false
  var voteState: GameVoteState = .hideButton {  // 三种状态负责选择下方按钮
    didSet {
      if number == "\(CurrentUser.shareInstance.currentPosition)" {
        titleImage.layer.borderColor = voteState == .readyShow ? UIColor.yellow.cgColor : UIColor.white.cgColor
        readyButton.isHidden = !(voteState == .readyShow)
      } else {
        readyButton.isHidden = true
      }
      if CurrentUser.shareInstance.currentPosition > 0, CurrentUser.shareInstance.currentPosition <= chatRoomOnLineNumber {
        voteButton.isHidden = !(voteState == .votesShow)
      } else {
        voteButton.isHidden = true
      }
    }
  }
  func setVotes(votes: [Int]) -> Void {
    votesView.subviews.forEach { (numberView) in
      numberView.removeFromSuperview()
    }
    votesView.isHidden = false
    switch votes.count {
    case 1, 2, 3, 4:
      let vWidth = CGFloat(17 * votes.count)
      votesView.frame = CGRect(x: (width - vWidth + 2) / 2, y: width - 15, width: vWidth - 2, height: 15)
      for index in 0..<votes.count {
        setLabelWithNumber(CGRect(x: 0 + 17 * CGFloat(index), y: 0, width: 15, height: 15), votes[index])
      }
    case 5, 6:
      let vWidth = CGFloat(17 * 3)
      votesView.frame = CGRect(x: (width - vWidth + 2) / 2, y: width - 33, width: vWidth - 2, height: 33)
      for index in 0..<votes.count {
        setLabelWithNumber(CGRect(x: 0 + 17 * CGFloat(index >= 3 ? index - 3 : index), y: 0 + (index >= 3 ? 17 : 0), width: 15, height: 15), votes[index])
      }
    case 7, 8:
      let vWidth = CGFloat(17 * 4)
      votesView.frame = CGRect(x: (width - vWidth + 2) / 2, y: width - 33, width: vWidth - 2, height: 33)
      for index in 0..<votes.count {
        setLabelWithNumber(CGRect(x: 0 + 17 * CGFloat(index >= 4 ? index - 4 : index), y: 0 + (index >= 4 ? 17 : 0), width: 15, height: 15), votes[index])
      }
    case 9, 10, 11, 12:
      let vWidth = CGFloat(17 * 4)
      votesView.frame = CGRect(x: (width - vWidth + 2) / 2, y: width - 50, width: vWidth - 2, height: 33)
      for index in 0..<votes.count {
        let vHeight = index % 4 == 0 ? (index / 5) * 17 : (index / 4) * 17
        setLabelWithNumber(CGRect(x: Int(0 + 17 * CGFloat(index >= 4 ? index - 4 : index)), y: vHeight, width: 15, height: 15), votes[index])
      }
    default:
      votesView.isHidden = true
    }
  }
  var animationArray = [ChatGifModel]()       // 现在一次一张Gif, 也支持多个队列
  var observerCount = 0                       // Gif的帧数, 用来停止和隐藏的
  var currentGifModel: ChatGifModel?          // 当前的git信息
  let animationView = YYAnimatedImageView()   // Gif图片
  let staticImageView = UIImageView()         // gif结束的最后一张图片
  var numberLabels = [MyLabel(), MyLabel(), MyLabel(), MyLabel()] //游戏的数字, 章鱼机抽麦序
  let rightBottomImage = UIImageView()    // 自由模式与禁麦图片
  let speakImage = UIImageView()          // 麦克风
  let titleImage = UIImageView()          // 头像
  let nameLabel = UILabel()               // 姓名
  let numberImage = UIImageView()         // 编号 1,2,3
  let backClearColor = UIView()           // 半透明紫色
  let rightTopLabel = UILabel()           // 准备中, 离线中
  let readyButton = UIButton(type: .system)  // 准备按钮
  let voteButton = UIButton(type: .system)   // 投票按钮
  let endSpeechButton = UIButton(type: .system)  // 结束发言按钮
  let outImageView = UIImageView()        // out出局
  let votesView = UIView()                // 投票的背景透明图片
  let masterReady = UILabel()             // 准备
  
  lazy var cutDown:SRCountdownTimer = {
    let countdownTimer = SRCountdownTimer()
    countdownTimer.isLabelHidden = true
    countdownTimer.lineWidth = 2
    countdownTimer.lineColor = UIColor.white
    countdownTimer.trailLineColor = UIColor.pinkColor()
    countdownTimer.backgroundColor = UIColor.clear
    countdownTimer.delegate = self
    return countdownTimer
  }()
  
  func setProgressLayer(time: Int) -> Void {
    insertSubview(cutDown, aboveSubview: titleImage)
    cutDown.frame = CGRect.init(x: 0, y: 0, width: titleImage.width, height: titleImage.width)
    cutDown.start(beginingValue: time)
  }
  
  var number = "0" { // 设置编号
    didSet {
      numberImage.image = UIImage(named: "chatPosition_\(number)");
    }
  }
  var tmpId = ""
  var model: ChatRoomUserModel = ChatRoomUserModel() {
    didSet {
      if !model.id.isEmpty {
        if tmpId != model.id {
          if let iconUrl = URL.init(string: model.avatar) {
            titleImage.sd_setImage(with: iconUrl, placeholderImage: headImage)
          } else {
            titleImage.image = headImage
          }
          tmpId = model.id
          nameLabel.text = model.name
          model.userRoomState = .roomOfOnMicro
          canSendGif(true)
          setUserModelChange(model)
        } else {
          setUserModelChange(model)
        }
      } else {
        speakImage.stopAnimating()
        titleImage.layer.borderColor = UIColor.white.cgColor
        setGameUserState(.hideState)
        voteState = .hideButton
        readyButton.isHidden = true
        rightBottomImage.isHidden = true
        endSpeechButton.isHidden = true
        animationArray.removeAll()
        nameLabel.text = ""
        outImageView.isHidden = true
        setGameUserState(.hideState)
        votesView.isHidden = true
        tmpId = ""
        switch model.userRoomState {
        case .roomOfClose:
          titleImage.image = closeImage
        default:
          titleImage.image = addImage
        }
      }
    }
  }
  
  func setUserModelChange(_ model: ChatRoomUserModel) -> Void {
    self.speakAnimation(model.speaking)
    self.setRightBottomImage(model.state)
    if CurrentUser.shareInstance.isGameing != .isNotAGame {
      if CurrentUser.shareInstance.isGameing == .isGameOverStates || CurrentUser.shareInstance.isGameing == .isUpdateConfig {
        if number == "0" {
          masterReady.text = model.masterReady ? "配置完成".localized : "配置中".localized
          masterReady.isHidden = false
          return
        }
        if CurrentUser.shareInstance.messageShow {
          voteState = model.prepared ? .hideButton : .readyShow
        } else {
          voteState = .hideButton
        }
        setGameUserState(model.prepared ? .readyState : .hideState)
        outImageView.isHidden = true
        votesView.isHidden = true
        deathState(true)
      } else {
        masterHidden()
        masterReady.text = ""
        deathState(!model.isDeath)
        voteState =  model.isVoted ? .votesShow : .hideButton
        setGameUserState(model.is_leaved ? .leaveState : .hideState)
        if !model.votes.isEmpty {
          setVotes(votes: model.votes)
        } else {
          votesView.isHidden = true
        }
      }
    } else {
      masterReady.isHidden = true
      readyButton.isHidden = true
      masterReady.isHidden = true
      rightTopLabel.isHighlighted = true
      endSpeechButton.isHidden = true
      titleImage.layer.borderColor = UIColor.white.cgColor
      setGameUserState(ChatRoomUserView.GameUserState.hideState)
    }
  }
  
  func deathState(_ unDeath: Bool) -> Void {
    outImageView.isHidden = unDeath
    titleImage.alpha = unDeath ? 1 : 0.5
  }
  
  func masterHidden() -> Void {
    if number == "0" || number == "" {
      voteState = .hideButton
      setGameUserState(.hideState)
      votesView.isHidden = true
      outImageView.isHidden = true
      masterReady.isHidden = true
    }
  }
  
  convenience init(frame: CGRect, _ model: ChatRoomUserModel = ChatRoomUserModel()) {
    self.init()
    addSubview(backClearColor)
    self.frame = frame
    self.model = model
    addSubview(titleImage)
    addSubview(nameLabel)
    addSubview(numberImage)
    addSubview(rightBottomImage)
    titleImage.addSubview(speakImage)
    addSubview(animationView)
    addSubview(staticImageView)
    addSubview(rightTopLabel)
    addSubview(endSpeechButton)
    addSubview(masterReady)
    makeShadowButton(button: readyButton, colors: [UIColor(hexInt: 0xffa000), UIColor(hexInt: 0xffcd00), UIColor(hexInt: 0xffff00)],
                     title: "准备".localized, selector: #selector(readyAction))
    makeShadowButton(button: voteButton, colors: [UIColor(hexInt: 0xff33ff), UIColor(hexInt: 0xff35c9), UIColor(hexInt: 0xff378c)],
                     title: "投票".localized, selector: #selector(votesAction))
    makeShadowButton(button: endSpeechButton, colors: [UIColor(hexInt: 0xffa000), UIColor(hexInt: 0xffcd00), UIColor(hexInt: 0xffff00)],
                     title: "结束发言".localized, selector: #selector(endSpeech))
    endSpeechButton.titleLabel?.font = UIFont.systemFont(ofSize: 10)
    addSubview(votesView)
    addSubview(outImageView)
    setGameOpLabel(self.numberLabels[0], UIColor.red, UIColor.hexColor("#0d4f71"))
    setGameOpLabel(self.numberLabels[1], UIColor(hexInt: 0xe03820), UIColor(hexInt: 0x765407))
    setGameOpLabel(self.numberLabels[2], UIColor(hexInt: 0x1ed2ff), UIColor(hexInt: 0x0d4f71))
    setGameOpLabel(self.numberLabels[3], UIColor(hexInt: 0xe03820), UIColor(hexInt: 0x765407))
    addObserver(self, forKeyPath: "animationView.currentAnimatedImageIndex", options: .new, context: nil)
  }
  
  func makeShadowButton(button: UIButton, colors: [UIColor], title: String, selector: Selector) -> Void {
    button.frame = CGRect(x: 10, y: width - 20, width: width - 20, height: 20)
    button.layer.cornerRadius = 10
    button.layer.masksToBounds = true
    button.shadowTopDown(colors: colors, locations: [0, 0.5, 1])
    button.setTitle(title, for: .normal)
    button.setTitleColor(UIColor.white, for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
    button.isHidden = true
    button.addTarget(self, action: selector, for: .touchUpInside)
    addSubview(button)
  }
  
  func endSpeech() -> Void {
    cutDown.end()
    messageDispatchManager.sendMessage(type: GameMessageType.end_speech)
  }
  
  func readyAction() -> Void {
    messageDispatchManager.sendMessage(type: GameMessageType.prepare)
  }

  func votesAction() -> Void {
    CurrentUser.shareInstance.isGameing = .isVotedState
    messageDispatchManager.sendMessage(type: GameMessageType.vote, payLoad: ["position": number])
  }

  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    guard self.animationView.isHidden == false, self.currentGifModel != nil else { return }
    guard let myCount = change?[.newKey] as? Int, observerCount > myCount else {   observerCount += 1; return }
    if currentGifModel?.stay_time != 0 {
      if let time = currentGifModel?.stay_time {
        if let mark = GamEmotice(rawValue: currentGifModel?.mark ?? "unKnown") {
          switch mark {
          case .dice, .points, .octopus, .coins, .mic, .finger_guessing:
            self.animationView.isHidden = true
            self.staticImageView.isHidden = false
          case .light, .hand_up, .gifts, .applause:
            self.animationView.isHidden = false
            self.staticImageView.isHidden = true
          default:
            self.animationView.isHidden = false
            self.staticImageView.isHidden = true
          }
          if let res = currentGifModel?.res {
            switch mark {
            case .dice: // 骰子
              self.staticImageView.image = UIImage(named: "chatVoiceGame_points_\(res)")
            case .finger_guessing: // 猜拳
              self.animationView.isHidden = true
              self.staticImageView.isHidden = false
              self.staticImageView.image = UIImage(named: "chatViewGame_guessing_\(res)")
            case .points:  // 打分
              self.staticImageView.image = UIImage(named: "chatViewGame_ points_\(res)")
            case .octopus:  // 章鱼机
              self.staticImageView.image = UIImage(named: "chatViewGame_octopus_1")
              self.setGameLabel(res)
            case .coins:  // 抛硬币
              self.staticImageView.image = UIImage(named: "chatViewGame_coins_\(res)")
            case .mic:    // 抽麦序
                self.staticImageView.image = UIImage(named: "chatViewGame_mic_1")
                self.numberLabels[0].text = res
            default:
              debugPrint("无法识别的动画 ChatRoomUserView Line186")
            }
          }
        }
        onceDelayGameOver(time)
      }
    } else {
      onceDelayGameOver(0)
    }
  }
  
  deinit {
    self.removeObserver(self, forKeyPath: "animationView.currentAnimatedImageIndex")
  }
  
  
  
  init() {
    super.init(frame: CGRect.zero)
    speakImage.animationImages = [UIImage.init(named: "user_speak1")!,UIImage.init(named: "user_speak2")!,UIImage.init(named: "user_speak3")!]
    speakImage.animationDuration = 0.5
    speakImage.layer.cornerRadius = 5
    speakImage.clipsToBounds = true
    speakImage.contentMode = .scaleToFill
    nameLabel.layer.cornerRadius = 5
    nameLabel.layer.masksToBounds = true
    nameLabel.backgroundColor = UIColor(hex: "#741f78")
    nameLabel.font = UIFont.systemFont(ofSize: 11)
    nameLabel.textAlignment = .center
    nameLabel.textColor = UIColor.white
    titleImage.backgroundColor = UIColor.clear
    titleImage.layer.borderColor = UIColor.white.cgColor
    titleImage.contentMode = .scaleAspectFill
    titleImage.image = addImage
    titleImage.layer.masksToBounds = true
    titleImage.layer.borderColor = UIColor.white.cgColor
    titleImage.layer.borderWidth = 2
    backClearColor.layer.masksToBounds = true
    backClearColor.backgroundColor = UIColor(hex: "#8460E3")
    backClearColor.alpha = 0.5
    backClearColor.isUserInteractionEnabled = false
    numberImage.layer.cornerRadius = 7.5
    numberImage.layer.masksToBounds = true
    numberImage.image = UIImage(named: "chatPosition_0")
    rightTopLabel.layer.cornerRadius = 6
    rightTopLabel.layer.masksToBounds = true
    rightTopLabel.font = UIFont.systemFont(ofSize: 9)
    rightTopLabel.textAlignment = .center
    masterReady.layer.cornerRadius = 6
    masterReady.layer.masksToBounds = true
    masterReady.font = UIFont.systemFont(ofSize: 9)
    masterReady.textAlignment = .center
    masterReady.backgroundColor = UIColor.yellow
    masterReady.textColor = UIColor.textPurpleDarkColor()
    masterReady.isHidden = true
    setGameUserState(.hideState)
    votesView.backgroundColor = UIColor.clear
    outImageView.isHidden = true
    outImageView.image = UIImage.init(named: "chatDeath")
    animationView.autoPlayAnimatedImage = true
    animationView.animationRepeatCount = 1
    canSendGif(true)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    titleImage.frame = CGRect(x: 0, y: 0, width: self.width, height: self.width)
    animationView.frame = CGRect(x: 0, y: 0, width: self.width, height: self.width)
    staticImageView.frame = CGRect(x: 0, y: 0, width: self.width, height: self.width)
    let space = (self.height - self.width - 12) / 2
    nameLabel.frame = CGRect(x: 0, y: self.width + space, width: self.width, height: 12)
    let tmpX = sqrt(self.width * self.width / 32)
    numberImage.frame = CGRect(x: tmpX - 8, y: tmpX - 8, width: 15, height: 15)
    rightBottomImage.frame = CGRect(x: width - tmpX - 8, y: width - tmpX - 8, width: 15, height: 15)
    titleImage.layer.cornerRadius = self.frame.width / 2
    rightTopLabel.frame = CGRect(x: width - tmpX - 14, y: tmpX - 4, width: 40, height: 12)
    masterReady.frame = CGRect(x: width - tmpX - 14, y: tmpX - 4, width: 50, height: 12)
    speakImage.frame = CGRect(x: 0, y: 0, width: self.width, height: self.width);
    backClearColor.frame = titleImage.frame
    backClearColor.layer.cornerRadius = self.frame.width / 2
    outImageView.frame = CGRect(x: width - 35, y: width - 40, width: 50, height: 35)
    let micLay = self.width / 167
    self.numberLabels[0].frame = CGRect(x: 58 * micLay, y: 42 * micLay, width: 41 * micLay, height: 44 * micLay)
    for index in 1...3 {
      self.numberLabels[index].frame = CGRect(x: CGFloat(14 + 27 * index) * micLay, y: 44 * micLay, width: 27 * micLay, height: 32 * micLay)
    }
  }
}



