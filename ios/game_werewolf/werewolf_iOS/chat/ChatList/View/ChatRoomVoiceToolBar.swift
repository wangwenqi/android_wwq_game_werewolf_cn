//
//  ChatRoomVoiceToolBar.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/30.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomVoiceToolBar: UIView {
  
    var speechBlock: ((Bool) -> Void)?
    var inputTextBlock: ((Bool, CGFloat) -> Void)?
    var sendMessageBlock: ((String) -> Void)?
    var sendGiftBlock: (() -> Void)?
    var isOnMir = false {
      didSet {
        if isOnMir {
          controlButton.isSelected = false;
          speechButton.isHidden = false;
          speechTextField.isHidden = true;
        } else {
          speechButton.isHidden = true;
          speechTextField.isHidden = false;
          controlButton.isSelected = true;
        }
      }
    }

    @IBOutlet weak var giftButton: UIButton!
    @IBOutlet weak var controlButton: UIButton!
    @IBOutlet weak var speechTextField: UITextField!
    @IBOutlet weak var speechButton: XBHButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func giftButtonAction(_ sender: Any) {
      sendGiftBlock?()
    }
    override func awakeFromNib() {
      super.awakeFromNib()
      NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(enterBackGround), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
      
      speechButton.addTarget(self, action: #selector(speechTouchDown), for: .touchDown)
      speechButton.addTarget(self, action: #selector(speechTouchUp), for: .touchUpInside)
      speechButton.addTarget(self, action: #selector(speechTouchUp), for: .touchUpOutside)
      
      speechTextField.delegate = self
      speechTextField.returnKeyType = .send
      
      let str = NSLocalizedString("输入你要说的话", comment: "")
      let attStr = str.highLightedStringWith(str, color: CustomColor.roomYellow)
      speechTextField.attributedPlaceholder = attStr
    }
  
    func keyBoardWillShow(notification: Notification) {
      let userInfo = notification.userInfo
      if let bounds = userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
        let keyBoardBounds = bounds.cgRectValue
        inputTextBlock?(true, (Screen.height - keyBoardBounds.origin.y))
      }
    }
    
    func keyBoardWillHide(notification: Notification) {
      if isOnMir {
        controlButton.isSelected = false;
        speechButton.isHidden = false;
        speechTextField.isHidden = true;
      } else {
        speechButton.isHidden = true;
        speechTextField.isHidden = false;
      }
      inputTextBlock?(false, 0)
    }
    
    deinit {
      NotificationCenter.default.removeObserver(self);
    }

    func speechTouchDown() {
      DispatchQueue.global().async { [weak self] _ in
        DispatchQueue.main.async {
          self?.speechButton.setButtonSoftShadowColor(color: UIColor.clear);
          self?.speechButton.addCustomHighlightLayer();
        }
        messageDispatchManager.sendMessage(type: .speak);
//        MessageManager.shareInstance.sendMessage(type: .speak)
        self?.speechBlock?(true)
      }
    }
  
    func enterBackGround() -> () {
      self.endEditing(true)
      inputTextBlock?(false, 0)
      speechTouchUp()
    }
  
    func speechTouchUp() {
      DispatchQueue.global().async { [weak self] _ in
        DispatchQueue.main.async {
          guard let weakSelf = self else { return }
          weakSelf.speechButton.setButtonSoftShadowColor(color: weakSelf.speechButton.buttonShdowColor);
          weakSelf.speechButton.removeCustomHighlightLayer();
        }
        messageDispatchManager.sendMessage(type: .unspeak);
//        MessageManager.shareInstance.sendMessage(type: .unspeak)
        self?.speechBlock?(false)
      }
    }
  
    @IBAction func controlButtonAction(_ sender: Any) {
      if isOnMir {
        controlButton.isSelected = !controlButton.isSelected
        speechButton.isHidden = controlButton.isSelected
        speechTextField.isHidden = !controlButton.isSelected
      } else {
        XBHHUD.showError(NSLocalizedString("上麦后才可以发语音哦~", comment: ""))
      }
      
      if controlButton.isSelected {
        // 打字
        speechTextField.becomeFirstResponder()
      } else {
        // 语音
        speechTextField.resignFirstResponder()
      }
    }
}

extension ChatRoomVoiceToolBar: UITextFieldDelegate {
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      if textField.text?.isEmpty ?? true {
        return true
      }
      sendMessageBlock?(textField.text ?? "")
      textField.text = ""
      speechTextField.resignFirstResponder()
      return true
    }
  
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      let textLength = textField.text?.length ?? 0
      let newLength = textLength + string.length - range.length
      return newLength <= 500
    }
  
}


