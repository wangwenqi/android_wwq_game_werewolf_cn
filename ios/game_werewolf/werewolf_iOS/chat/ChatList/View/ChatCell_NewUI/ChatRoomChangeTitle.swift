//
//  ChatRoomChangeTitle.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/12.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomChangeTitle: BaseNoticeView {

    var selectBlock:((String, Int) -> Void)?
    @IBOutlet weak var roomTitleLabel: UILabel!
    @IBOutlet weak var changeRoomTitleLabel: UILabel!
    @IBOutlet weak var chooseRoomTypeLabel: UILabel!
    @IBOutlet weak var roomTitleTextField: UITextField!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sureButton: UIButton!
    var isEditState = false {
      didSet {
        self.setNeedsLayout()
      }
    }
    var roomTypes = [SelectUserView]()
    var selectTypeIndex = 0
    var selectTypeStr = "" {
      didSet {
        for i in 0..<SwiftConverter.shareInstance.setAudio.types.count {
          let model = SwiftConverter.shareInstance.setAudio.types[i]
          if model.type == selectTypeStr {
            selectTypeIndex = i
          }
        }
      }
    }
  
    @IBAction func cancelButtonAction(_ sender: Any) {
      Utils.hideNoticeView()
    }
    @IBAction func sureButtonAction(_ sender: Any) {
      if let text = roomTitleTextField.text, text.length >= 4 {
        selectBlock?(text, selectTypeIndex)
        Utils.hideNoticeView()
      } else {
        XBHHUD.showError("房间话题需要4~15个字符".localized)
      }
    }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        roomTitleTextField.layer.borderColor = UIColor(hex: "#8460e3").cgColor
        roomTitleTextField.layer.cornerRadius = 2
        roomTitleTextField.layer.borderWidth = 1
        roomTitleTextField.delegate = self
        cancelButton.layer.cornerRadius = 5
        cancelButton.layer.masksToBounds = true
        
        self.sureButton.layer.cornerRadius = 5
        self.sureButton.layer.masksToBounds = true
        self.sureButton.setTitle("确认".localized, for: .normal)
        self.roomTitleLabel.text = "修改房间主题".localized
        self.roomTitleTextField.placeholder = "输入房间话题(4-15个字符)".localized
        self.infoLabel.text = "   提示: 官方严厉打击色情, 广告, 辱骂等内容, 一经发现永久封号.".localized
        print(SwiftConverter.shareInstance.setAudio.types)
        for i in 0..<SwiftConverter.shareInstance.setAudio.types.count {
          let model = SwiftConverter.shareInstance.setAudio.types[i]
          if let name = model.name, let _ = model.image {
            let tmpTypeView = SelectUserView(title: name)
            tmpTypeView.markNumber = i
            roomTypes.append(tmpTypeView)
            tmpTypeView.addTarget(self, action: #selector(chooseReason(control:)), for: .touchUpInside)
            addSubview(tmpTypeView)
          }
        }
    }
  
    func chooseReason(control: SelectUserView) -> Void {
      print(control.markNumber)
      for i in 0..<roomTypes.count {
        let reasonView = roomTypes[i]
        reasonView.selectState = false
      }
      control.selectState = true
      selectTypeIndex = control.markNumber
    }
    
    class func showChatRoomChangeTitle() -> ChatRoomChangeTitle {
        let view = Bundle.main.loadNibNamed(ChatRoomChangeTitle.name(), owner: nil, options: nil)?.last as! ChatRoomChangeTitle
        return view
    }
  
    override func layoutSubviews() {
      super.layoutSubviews()
      
      let myWidth = self.frame.width - 60
      for i in 0..<roomTypes.count {
        let selectView = roomTypes[i]
        if selectView.markNumber == selectTypeIndex {
          selectView.selectState = true
        }
        let x: CGFloat = myWidth / 3 * CGFloat(i % 3)
        let line: Int = i / 3
        let y: CGFloat = CGFloat(line * 40) + 82
        selectView.frame = CGRect(x: x + 20, y: y, width: myWidth / 3, height: 20)
      }
      if roomTypes.isEmpty {
        self.height = CGFloat(228)
        self.chooseRoomTypeLabel.isHidden = true
      } else {
        self.height = CGFloat(250 + 40 * roomTypes.count / 2)
        self.chooseRoomTypeLabel.isHidden = false
      }
      let myHeight = Screen.height * 0.5 - (isEditState ? 100 : 0)
      UIView.animate(withDuration: 0.2) {
        self.center = CGPoint(x: Screen.width / 2, y: myHeight)
      }
    }
  
    override func initial() {
      self.width = Screen.width * 0.85
      self.height = CGFloat(200 + 40 * roomTypes.count / 3)
      self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
      self.backgroundColor = UIColor(hex: "#e2dee8")
      self.layer.cornerRadius = 6
    }

}

extension ChatRoomChangeTitle: UITextFieldDelegate {
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    isEditState = true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let textLength = textField.text?.length ?? 0
    let newLength = textLength + string.length - range.length
    return newLength <= 15
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    self.endEditing(true)
    isEditState = false
    return true
  }
  
}
