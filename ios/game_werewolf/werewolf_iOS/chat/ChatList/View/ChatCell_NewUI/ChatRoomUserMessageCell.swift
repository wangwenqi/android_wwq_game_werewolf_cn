//
//  ChatGiftMessageCell.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/11.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomUserMessageCell: UITableViewCell {

  var model = ChatRoomChatList.ChatMessgeModel() {
    didSet {
      switch model.positionNumber {
      case "0", "1", "2", "3", "4", "5", "6", "7", "8":
        headerImage.image = UIImage(named: "chatPosition_\(model.positionNumber)")
      default:
        headerImage.image = UIImage(named: "chatPosition_x")
      }
      nameLabbel.text = model.name
      messageLabel.text = model.message
    }
  }
  
  fileprivate let headerImage = UIImageView()
  fileprivate let nameLabbel = UILabel()
  fileprivate let messageLabel = UILabel()
  // 辅助背景
  fileprivate let nameBackView = UIView()
  fileprivate let messageBackView = UIView()
  
  func copyInfo(gesture: UIGestureRecognizer) -> Void {
    guard gesture.state == .began else { return }
    let paste = UIPasteboard.general
    paste.string = messageLabel.text
    XBHHUD.showTextOnly(text: "已复制到剪切板".localized)
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    let longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(copyInfo(gesture:)))
    
    self.addGestureRecognizer(longPress)
    self.backgroundColor = UIColor.clear
    
    nameLabbel.textColor = UIColor.white
    nameLabbel.font = UIFont.systemFont(ofSize: 12)
    
    messageLabel.textColor = UIColor.white
    messageLabel.font = UIFont.systemFont(ofSize: 12)
    
    messageLabel.numberOfLines = 0
    
    addSubview(nameBackView)
    addSubview(messageBackView)
    
    addSubview(headerImage)
    addSubview(nameLabbel)
    addSubview(messageLabel)
    
    messageLabel.numberOfLines = 0
    
    headerImage.snp.makeConstraints { (make) in
      make.left.equalToSuperview().offset(15)
      make.top.equalToSuperview().offset(3)
      make.height.width.equalTo(20)
    }
    
    nameLabbel.snp.makeConstraints { (make) in
      make.left.equalTo(headerImage.snp.right).offset(5)
      make.top.equalToSuperview().offset(3)
      make.height.equalTo(20)
    }
    
    messageLabel.snp.makeConstraints { (make) in
      make.left.equalToSuperview().offset(40)
      make.top.equalTo(nameLabbel.snp.bottom).offset(3 + 5)
      make.right.lessThanOrEqualTo(self).offset(-40)
      make.bottom.equalToSuperview().offset(-8)
    }
    
    nameBackView.backgroundColor = UIColor(hex: "#f19201")
    nameBackView.layer.cornerRadius = 5
    nameBackView.layer.masksToBounds = true
    nameBackView.snp.makeConstraints { (make) in
      make.left.equalTo(headerImage.snp.right)
      make.right.equalTo(nameLabbel.snp.right).offset(5)
      make.top.equalToSuperview().offset(3)
      make.height.equalTo(20)
    }
    
    messageBackView.backgroundColor = UIColor.white
    messageBackView.alpha = 0.2
    messageBackView.layer.cornerRadius = 5
    messageBackView.layer.masksToBounds = true
    messageBackView.snp.makeConstraints { (make) in
      make.left.equalToSuperview().offset(35)
      make.top.equalTo(nameLabbel.snp.bottom).offset(3)
      make.right.equalTo(messageLabel.snp.right).offset(5)
      make.bottom.equalTo(messageLabel.snp.bottom).offset(3)
    }
    selectionStyle = .none
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }

}
