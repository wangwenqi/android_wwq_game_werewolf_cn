//
//  ChatRoomShowGif.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/21.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

enum GamEmotice: String {
  case dice                  // 骰子
  case light                 // 亮灯
  case hand_up               // 举手
  case finger_guessing       // 猜拳
  case points                // 打分
  case octopus               // 章鱼机
  case coins                 // 抛硬币
  case mic                   // 抽麦序
  case gifts                 // 求礼物
  case applause              // 鼓掌
  case unKnown
}

class ChatRoomShowGif: UIView {

  var collectionView: UICollectionView?
  var giftURLArray = [ChatGifModel]()
  var selectGifBlock: ((ChatGifModel, Int) -> Void)?
  var openGifNames = [String]()
  enum GifType {
    case gameGif
    case faceGif
  }
  
  convenience init(frame: CGRect, type: GifType) {
    self.init(frame: frame)
    switch type {
    case .faceGif:
      // 查询Gif表情
//      DispatchQueue.global().async {
        guard GifDBManager.openDB() else { return }
        if let models = GifDBManager.queryDBAll(), !models.isEmpty {
          self.setDataSource(models)
        } else {
          RequestManager().get(url: RequestUrls.audioEmot, success: { (success) in
            for model in success.arrayValue {
              let url = model["url"].stringValue
              let mark = model["mark"].stringValue
              let thum = model["thumbnail"].stringValue
              let _ = GifDBManager.insertToDB(ChatGifModel(url: url, mark: mark, thumbnail: thum, res_url: "", res: "", sound: "", stay_time: 0))
            }
            if let all = GifDBManager.queryDBAll() {
              self.setDataSource(all)
            }
          }, error: { (code, message) in
            
          })
        }
//      }
    case .gameGif:
      // 请求数据库拿到数据
      var all = [ChatGifModel]()
      let gameArray = [GamEmotice.dice, GamEmotice.light, GamEmotice.hand_up, GamEmotice.finger_guessing, GamEmotice.points, GamEmotice.octopus, GamEmotice.coins, GamEmotice.mic, GamEmotice.gifts, GamEmotice.applause]
      gameArray.forEach({ (type) in
        let model = ChatGifModel(url: "", mark: type.rawValue, thumbnail: "", res_url: "", res: "", sound: "", stay_time: 0)
        all.append(model)
      })
      self.setDataSource(all)
      }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    let layout = UICollectionViewFlowLayout.init()
    let myWidth = (Screen.width - 110) / 6
//    layout.scrollDirection = .horizontal
    layout.itemSize = CGSize(width: myWidth, height: myWidth + 15)
    layout.minimumLineSpacing = 13
    layout.minimumInteritemSpacing = 8
    collectionView = UICollectionView.init(frame: CGRect(x: 20, y: 0, width: Screen.width - 40, height: myWidth * 2 + 55), collectionViewLayout: layout)
    collectionView?.register(ChatRoomGifCell.self, forCellWithReuseIdentifier: ChatRoomGifCell.name())
    collectionView?.delegate = self
    collectionView?.dataSource = self
    collectionView?.isPagingEnabled = true
    collectionView?.backgroundColor = UIColor.clear
    addSubview(collectionView!)
  }
  
  func setDataSource(_ models: [ChatGifModel]) -> Void {
    for model in models {
      openGifNames.append(model.mark)
      giftURLArray.append(ChatGifModel(url: model.url, mark: model.mark, thumbnail: model.thumbnail, res_url: "", res: "", sound: "", stay_time: 0))
    }
    GifDBManager.closeDB()
    DispatchQueue.main.async {[weak self] in
      self?.collectionView?.reloadData()
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    selectGifBlock?(giftURLArray[indexPath.item], indexPath.item)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}

extension ChatRoomShowGif: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.giftURLArray.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChatRoomGifCell.name(), for: indexPath) as! ChatRoomGifCell
    cell.model = giftURLArray[indexPath.row]
    return cell
  }
  
}
