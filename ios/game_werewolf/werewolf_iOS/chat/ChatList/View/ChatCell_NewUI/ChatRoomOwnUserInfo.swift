//
//  ChatRoomOwnUserInfo.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/12.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomOwnUserInfo: UIControl {

  var json: JSON? {
    didSet {
      var winInt = 0
      var loseInt = 0
      nameLabel.text = json?["name"].stringValue
      if let headUrl = json?["image"].stringValue, !headUrl.isEmpty {
        headImage.sd_setImage(with: URL.init(string: headUrl))
      }
      if let popular = json?["popular"].stringValue, !popular.isEmpty {
        levelLabel.text = "LV." + (json?["game"]["level"].stringValue ?? "0") + "      " + "人气值: " +  popular
      } else {
        levelLabel.text = "LV." + (json?["game"]["level"].stringValue ?? "0")
      }
      if let win = json?["game"]["win"].stringValue {
        winInt = Int(win) ?? 0
        winNum.text = win
      }
      if let lose = json?["game"]["lose"].stringValue {
        loseInt = Int(lose) ?? 0
        loseNum.text = lose
      }
      let roleType = json?["role"]["type"].intValue ?? 0
      switch roleType {
      case 1:
        medalLabel.text = "管理员".localized
      case 2:
        medalLabel.text = "老师".localized
      default:
        medalLabel.text = json?["role"]["custom"].stringValue
      }
      if let escape = json?["game"]["escape"].stringValue {
        if winInt + loseInt + (Int(escape) ?? 0) == 0 {
          excapeScape.text = "0%"
        } else if winInt + loseInt == 0, (Int(escape) ?? 0) != 0 {
          excapeScape.text = "100%"
        } else {
          let myAll = Float(winInt + loseInt) + (Float(escape) ?? 0)
          let excapeNum = Float(escape) ?? 0
          let scale = lroundf(excapeNum / myAll * Float(100.0))
          excapeScape.text = "\(scale)%"
        }
      }
      if loseInt == 0, winInt != 0 {
        winScale.text = "100%"
      } else if loseInt == 0, winInt == 0 {
        winScale.text = "0%"
      } else {
        let escape = json?["game"]["escape"].intValue ?? 0
        let scale = lroundf(Float(winInt) / (Float(winInt + loseInt + escape)) * Float(100.0))
        winScale.text = "\(scale)%"
      }
      setNeedsLayout()
    }
  }

  
  
  
  let headImage = UIImageView()       // 头像
  let nameLabel = UILabel()           // 姓名
  let medalLabel = UILabel()          // 勋章
  let levelLabel = UILabel()          // 等级 人气
  let rightImage = UIImageView()      // 右侧Image
  
  let winNum = UILabel()              // 胜利
  let loseNum = UILabel()             // 失败
  let winScale = UILabel()            // 胜率
  let excapeScape = UILabel()         // 逃跑率
  
  
  let headHeight: CGFloat = 60.0
  let leftRight: CGFloat = 30.0
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    addSubview(headImage)
    addSubview(nameLabel)
    addSubview(levelLabel)
    addSubview(rightImage)
  
    addSubview(winNum)
    addSubview(loseNum)
    addSubview(winScale)
    addSubview(excapeScape)
    addSubview(medalLabel)
    medalLabel.backgroundColor = UIColor.yellow
    medalLabel.layer.masksToBounds = true
    medalLabel.textAlignment = .center
    medalLabel.font = UIFont.systemFont(ofSize: 12)
    medalLabel.layer.cornerRadius = 5
    
    headImage.frame = CGRect(x: leftRight, y: 0, width: headHeight, height: headHeight)
    headImage.layer.cornerRadius = headHeight / 2
    headImage.layer.masksToBounds = true
    headImage.layer.borderColor = UIColor.white.cgColor
    headImage.layer.borderWidth = 1
    headImage.image = UIImage(named: "room_head_default")
    // 总高度 121 = 50 + 15 + 56
    rightImage.image = UIImage(named: "chatRoom_right_addFrend")
    rightImage.contentMode = .scaleAspectFit
    
    levelLabel.textColor = UIColor(hex: "#F19201")
    levelLabel.font = UIFont.systemFont(ofSize: 11)
    
    winNum.textAlignment = .center
    winNum.textColor = UIColor(hex: "#F19201")
    loseNum.textAlignment = .center
    loseNum.textColor = UIColor(hex: "#F19201")
    winScale.textAlignment = .center
    winScale.textColor = UIColor(hex: "#F19201")
    excapeScape.textAlignment = .center
    excapeScape.textColor = UIColor(hex: "#43B184")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    let nameLeft = headHeight + leftRight + 8
    nameLabel.frame = CGRect(x: nameLeft, y: 0, width: width - nameLeft - leftRight - 10, height: headHeight / 3)
    nameLabel.font = UIFont.systemFont(ofSize: 14)
    
    let medalSize = medalLabel.text?.sizeWithGivenSize(CGSize(width: 200, height: 300), font: UIFont.systemFont(ofSize: 12))
    medalLabel.frame = CGRect(x: nameLeft, y: headHeight / 3, width: (medalSize?.width ?? 0) + 8, height: headHeight / 3)
    medalLabel.isHidden = medalLabel.frame.width <= 8
    levelLabel.frame = CGRect(x: nameLeft, y: headHeight / 3 * 2, width: width - nameLeft - leftRight, height: headHeight / 3)
    rightImage.frame = CGRect(x: width - 20 - leftRight, y: headHeight / 2 - 7.5, width: 15, height: 15)
    let LineTop: CGFloat = headHeight + 15
    let resultHeight: CGFloat = 56
    let baseLine1 = UIImageView(frame: CGRect(x: leftRight, y: LineTop, width: width - 2 * leftRight, height: 1))
    let baseLine2 = UIImageView(frame: CGRect(x: leftRight, y: LineTop + resultHeight, width: width - 2 * leftRight, height: 1))
    baseLine1.image = UIImage(named: "dashline")
    addSubview(baseLine1)
    baseLine2.image = UIImage(named: "dashline")
    addSubview(baseLine2)
    let names = ["胜利".localized, "失败".localized, "胜率".localized, "逃跑率".localized]
    let resultWidth = (width - 2 * leftRight - 20) / 4
    for index in 0...3 {
      let label = UILabel()
      label.text = names[index]
      label.textColor = UIColor(hex: "#3C1B69")
      label.font = UIFont.systemFont(ofSize: 13)
      label.textAlignment = .center
      label.frame = CGRect(x: 40 + CGFloat(index) * resultWidth, y: LineTop + 10, width: resultWidth, height: resultHeight / 2 - 10)
      addSubview(label)
    }
    winNum.frame = CGRect(x: leftRight + 10, y: LineTop + resultHeight / 2, width: resultWidth, height: resultHeight / 2 - 10)
    loseNum.frame = CGRect(x: leftRight + 10 + resultWidth, y: LineTop + resultHeight / 2, width: resultWidth, height: resultHeight / 2 - 10)
    winScale.frame = CGRect(x: leftRight + 10 + 2 * resultWidth, y: LineTop + resultHeight / 2, width: resultWidth, height: resultHeight / 2 - 10)
    excapeScape.frame = CGRect(x: leftRight + 10 + 3 * resultWidth, y: LineTop + resultHeight / 2, width: resultWidth, height: resultHeight / 2 - 10)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
