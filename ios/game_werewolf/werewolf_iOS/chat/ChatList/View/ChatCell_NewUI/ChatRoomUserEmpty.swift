//
//  ChatRoomUserEmpty.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/11.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

// 房主点击空白位置
class ChatRoomUserEmpty: BaseNoticeView {
  
    
  @IBOutlet weak var closeThisMicLabel: UILabel!
  @IBOutlet weak var cancelButton: UIButton!
  
  var selectBlock:((ButtonAction) -> Void)?
  enum ButtonAction {
    case closeMic
  }
  override func initial() {
    self.width = Screen.width * 0.85
    self.height = 175
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.backgroundColor = UIColor(hex: "#e2dee8")
    self.layer.cornerRadius = 6
  }
  
    @IBAction func closeMicAction(_ sender: Any) {
        // 封麦
        Utils.hideNoticeView()
        selectBlock?(.closeMic)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        Utils.hideNoticeView()
    }
  
  
    override func awakeFromNib() {
      super.awakeFromNib()
      cancelButton.layer.cornerRadius = 5
      cancelButton.layer.masksToBounds = true
      closeThisMicLabel.text = "解锁此麦位".localized
    }
  
  class func showChatRoomEmptyUser() -> ChatRoomUserEmpty {
    let chatListView = Bundle.main.loadNibNamed(ChatRoomUserEmpty.name(), owner: nil, options: nil)?.last as! ChatRoomUserEmpty
    return chatListView
  }

}
