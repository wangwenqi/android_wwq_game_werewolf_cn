//
//  ChatRoomChangePassword.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/12.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomChangePassword: BaseNoticeView {
  
  var selectPassword:((String) -> Void)?
  
  @IBOutlet weak var openPasswordLable: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var firstLabel: UILabel!
  @IBOutlet weak var secondLabel: UILabel!
  @IBOutlet weak var thirdLabel: UILabel!
  @IBOutlet weak var fourLabel: UILabel!
  @IBOutlet weak var infoLabel: UILabel!
  @IBOutlet weak var cancelButton: UIButton!
  @IBOutlet weak var sureButton: UIButton!
  @IBOutlet weak var inputControl: UIControl!
  @IBOutlet weak var OpenPassword: UIControl!
  @IBOutlet weak var switchImageView: UIImageView!
  
  let textField = UITextField()
  
  override func initial() {
    self.width = Screen.width * 0.85
    self.height = 251 // 146
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.backgroundColor = UIColor(hex: "#e2dee8")
    self.layer.cornerRadius = 6
  }
    
  @IBAction func openPasswordAction(_ sender: Any) {
    if self.textField.isEditing {
        switchImageView.image = UIImage.init(named: "chatRoom_switch_close")
        self.textField.resignFirstResponder()
        self.textField.text = ""
        self.height = 146
        self.inputControl.isHidden = true
        self.infoLabel.isHidden = true
        firstLabel.text = ""
        secondLabel.text = ""
        thirdLabel.text = ""
        fourLabel.text = ""
    } else {
        switchImageView.image = UIImage.init(named: "chatRoom_switch_open")
        self.textField.becomeFirstResponder()
        self.height = 251
        self.inputControl.isHidden = false
        self.infoLabel.isHidden = false
    }
  }
    
  @IBAction func cancelButtonAction(_ sender: Any) {
    Utils.hideNoticeView()
  }
  
  @IBAction func sureButtonAction(_ sender: Any) {
    if let text = textField.text, text.length == 4  {
      Utils.hideNoticeView()
      selectPassword?(text)
    } else if textField.text == "" {
      Utils.hideNoticeView()
      selectPassword?("")
    } else {
      XBHHUD.showError("请输入4位密码")
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.addSubview(textField)
    textField.delegate = self
    textField.keyboardType = .numberPad
    firstLabel.layer.cornerRadius = 5
    firstLabel.layer.masksToBounds = true
    secondLabel.layer.cornerRadius = 5
    secondLabel.layer.masksToBounds = true
    thirdLabel.layer.cornerRadius = 5
    thirdLabel.layer.masksToBounds = true
    fourLabel.layer.cornerRadius = 5
    fourLabel.layer.masksToBounds = true
    sureButton.layer.cornerRadius = 5
    cancelButton.layer.cornerRadius = 5
    self.titleLabel.text = "修改房间密码".localized
    self.infoLabel.text = "请输入房间密码".localized
    self.openPasswordLable.text = "打开房间密码".localized
    self.sureButton.setTitle("确认".localized, for: .normal)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
  }
  
  class func showChatRoomChangePassword() -> ChatRoomChangePassword {
    let view = Bundle.main.loadNibNamed(ChatRoomChangePassword.name(), owner: nil, options: nil)?.last as! ChatRoomChangePassword
    view.textField.becomeFirstResponder()
    let password = CurrentUser.shareInstance.currentRoomPassword
    if password.length == 4 {
      view.textField.text = password
      view.firstLabel.text = password.slicing(from: 0, length: 1)
      view.secondLabel.text = password.slicing(from: 1, length: 1)
      view.thirdLabel.text = password.slicing(from: 2, length: 1)
      view.fourLabel.text = password.slicing(from: 3, length: 1)
    }
    return view
  }
  
}

extension ChatRoomChangePassword: UITextFieldDelegate {
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    switch range.location {
    case 0:
      firstLabel.text = string
    case 1:
      secondLabel.text = string
    case 2:
      thirdLabel.text = string
    case 3:
      fourLabel.text = string
    default:
      break
    }
    let textLength = textField.text?.length ?? 0
    if range.length + range.location > textLength {
      return false
    }
    let newLength = textLength + string.length - range.length
    return newLength <= 4
  }
  
}
