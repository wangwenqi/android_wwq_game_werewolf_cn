//
//  ChatRoomDintingCell.swift
//  xiaoyu
//
//  Created by xiaozao on 2017/12/18.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit


class ChatRoomDintingCell: UITableViewCell {

  enum DintingState {
    case gameOver
    case voteResult
    case deathInfo
  }

  var model = ChatRoomChatList.ChatMessgeModel() {
    didSet {
      titleLabel.text = model.position
      infoLabel.text = model.message
      senderLabel.text = model.name
    }
  }
  
  let titleLabel = UILabel()
  let infoLabel = UILabel()
  let senderLabel = UILabel()
  let backGroundView = UIView()
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    self.backgroundColor = UIColor.clear
    
    addSubview(backGroundView)
    addSubview(titleLabel)
    addSubview(infoLabel)
    addSubview(senderLabel)
    
    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont.systemFont(ofSize: 12)
    titleLabel.textAlignment = .center

    senderLabel.textColor = UIColor.white
    senderLabel.font = UIFont.systemFont(ofSize: 12)
    senderLabel.backgroundColor =  UIColor.pinkColor()
    senderLabel.textAlignment = .center
    senderLabel.layer.cornerRadius = 5
    senderLabel.layer.masksToBounds = true
    
    infoLabel.textColor = UIColor.white
    infoLabel.font = UIFont.systemFont(ofSize: 12)
    infoLabel.numberOfLines = 0
    infoLabel.textAlignment = .center
    
    senderLabel.snp.makeConstraints { (make) in
      make.left.equalToSuperview().offset(15)
      make.top.equalToSuperview().offset(3)
      make.height.equalTo(20)
      make.width.equalTo(60)
    }
    
    titleLabel.snp.makeConstraints { (make) in
      make.left.equalToSuperview().offset(35)
      make.right.equalToSuperview().offset(-35)
      make.top.equalTo(senderLabel.snp.bottom).offset(5)
      make.height.equalTo(25)
    }
    
    infoLabel.snp.makeConstraints { (make) in
      make.left.equalToSuperview().offset(35)
      make.right.equalToSuperview().offset(-35)
      make.top.equalTo(titleLabel.snp.bottom).offset(3)
      make.bottom.equalToSuperview().offset(-5)
    }
    
    backGroundView.backgroundColor = UIColor(hex: "#96157d")
    backGroundView.layer.cornerRadius = 5
    backGroundView.layer.masksToBounds = true
    backGroundView.alpha = 0.8
    backGroundView.snp.makeConstraints { (make) in
      make.left.equalToSuperview().offset(30)
      make.right.equalToSuperview().offset(-30)
      make.top.equalTo(titleLabel.snp.top).offset(-3)
      make.bottom.equalToSuperview()
    }
    

    selectionStyle = .none
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }

}
