//
//  ChatRoomChooseUser.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/13.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomChooseUser: BaseNoticeView {

  @IBOutlet weak var cancelButton: UIButton!
  @IBOutlet weak var setDownSeatLabel: UILabel! // 设为旁听
  @IBOutlet weak var closeSomeOneControl: UIControl!
  @IBOutlet weak var closeSomeOneLabel: UILabel!
  @IBOutlet weak var closeSomeOneImageView: UIImageView!
  @IBOutlet weak var leaveRoomLabel: UILabel!   // 踢出房间
  @IBOutlet weak var setFreeLabel: UILabel!
  @IBOutlet weak var setFreeImageView: UIImageView!
  @IBOutlet weak var closeSeatLabel: UILabel!   // 封闭此麦位
  @IBOutlet weak var setFreeControl: UIControl!
  @IBOutlet weak var addBlackList: UILabel!
  @IBOutlet weak var reportLabel: UILabel!
  @IBOutlet weak var addFrendControl: UIControl!
  @IBOutlet weak var addFrendLabel: UILabel!
  @IBOutlet weak var sendGiftControl: UIControl!
  @IBOutlet weak var sendGiftLabel: UILabel!
  @IBOutlet weak var sendGiftWidth: NSLayoutConstraint!
  @IBOutlet weak var addFrendWidth: NSLayoutConstraint!
  @IBOutlet weak var mangerHeight: NSLayoutConstraint! //?? 82 ?? 0
  @IBOutlet weak var leftControl: UIControl!
  @IBOutlet weak var centerControl: UIControl!
  @IBOutlet weak var rightControl: UIControl!
  var position = 0
    
  let userInfoView = ChatRoomOwnUserInfo(frame: CGRect.zero)
  var isManager = false {
    didSet {
      self.height = isManager ? 470 : 309
      self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
      self.mangerHeight.constant = isManager ? 82 : 0
      self.leftControl.isHidden = !isManager
      self.centerControl.isHidden = !isManager
      self.rightControl.isHidden = !isManager
    }
  }
  enum ButtonAction {
    case setDownSeat
    case setLeaveRoom
    case setCloseSeat
    case sendGift
    case checkUserInfo
    case addBlackList
    case reportSomeOne
    case closeSomeOne
    case freeModel
  }
  var selectBlock: ((ButtonAction) -> Void)?
  var model: ChatRoomUserModel? {
    didSet {
      if let id = model?.id, let state = model?.state {
        changeUserState(state)
        XBHHUD.showLoading()
        RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id": id], success: { [weak self] (json) in
          XBHHUD.hide()
          self?.userInfoView.json = json
          if json["is_friend"].boolValue {
            self?.addFrendControl.isHidden = true
            self?.sendGiftWidth.constant = 0.85 * Screen.width - 60
          } else {
            self?.sendGiftControl.isHidden = false
            self?.sendGiftWidth.constant = 0.425 * Screen.width - 40
            self?.addFrendWidth.constant = 0.425 * Screen.width - 40
          }
        }) { (code, message) in
          XBHHUD.hide()
          XBHHUD.showError(message)
        }
      }
    }
  }
  
  func changeUserState(_ state: String) -> Void {
    switch state {
    case "free":
      closeSomeOneLabel.text = "禁言此人".localized
      closeSomeOneImageView.image = UIImage.init(named: "chatRoom_Close_SomeOne")
      setFreeLabel.text = "关自由模式".localized
      setFreeImageView.image = UIImage.init(named: "chatRoom_closeFree_Model")
    case "limit":
      closeSomeOneLabel.text = "解除禁言".localized
      closeSomeOneImageView.image = UIImage.init(named: "chatRoom_UnClose_SomeOne")
      setFreeLabel.text = "开自由模式".localized
      setFreeImageView.image = UIImage.init(named: "chatRoom_OpenFree_Model")
    default:
      closeSomeOneLabel.text = "禁言此人".localized
      closeSomeOneImageView.image = UIImage.init(named: "chatRoom_Close_SomeOne")
      setFreeLabel.text = "开自由模式".localized
      setFreeImageView.image = UIImage.init(named: "chatRoom_OpenFree_Model")
    }
  }
  
  override func initial() {
    self.width = Screen.width * 0.85
    self.height = 391
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.backgroundColor = UIColor(hex: "#e2dee8")
    self.layer.cornerRadius = 6
  }
  
  @IBAction func setDownSeatAction(_ sender: Any) {
    Utils.hideNoticeView()
    selectBlock?(.setDownSeat)
  }
  
  @IBAction func setLeaveRoom(_ sender: Any) {
    Utils.hideNoticeView()
    selectBlock?(.setLeaveRoom)
  }
  
  @IBAction func setCloseSeatAction(_ sender: Any) {
    Utils.hideNoticeView()
    selectBlock?(.setCloseSeat)
  }
  
  @IBAction func sendGiftAction(_ sender: Any) {
    Utils.hideNoticeView()
    selectBlock?(.sendGift)
  }
  @IBAction func cancelAction(_ sender: Any) {
    Utils.hideNoticeView()
  }
    
  @IBAction func closeSomeOne(_ sender: Any) {
    Utils.hideNoticeView()
    selectBlock?(.closeSomeOne)
  }
    
  @IBAction func freeAction(_ sender: Any) {
    Utils.hideNoticeView()
    selectBlock?(.freeModel)
  }
    
  func checkUserInfo() -> Void {
    Utils.hideNoticeView()
    selectBlock?(.checkUserInfo)
  }
  // 黑名单
  func addBlackListAction() -> Void {
    Utils.hideNoticeView()
    if let ids =  model?.id {
      RequestManager().get(url: RequestUrls.block + "/\(ids)", success: { (json) in
        XBHHUD.hide()
        XBHHUD.showSuccess("拉黑成功！".localized)
        Utils.addToBL(id:ids)
      }) { (code, message) in
        XBHHUD.hide()
        XBHHUD.showError("拉黑失败！".localized)
    
      }
    } else {
      XBHHUD.showError("拉黑成功！".localized)
    }
//    selectBlock?(.addBlackList)
  }
  // 举报
  func reportAction() -> Void {
    Utils.hideNoticeView()
    if let id = model?.id {
      ChatRoomReportUser.showChatRoomReportUser(uid: id)
    }
//    selectBlock?(.reportSomeOne)
  }
  
  @IBAction func addFrendAction(_ sender: Any) {
    if let user_id = model?.id {
      if let block = Utils.getCachedBLContent() {
        if !user_id.isEmpty {
          if block[user_id] != nil {
            XBHHUD.showError("此人已经被你拉黑")
            return
          }
        }else{
          return
        }
      }
      messageDispatchManager.sendMessage(type:.add_friend, payLoad: ["user_id":user_id,"position": position])
      RequestManager().post(url: RequestUrls.friendAdd, parameters: ["friend_id": user_id], success: { [weak self] (json) in
        //发送socket请求
        XBHHUD.showTextOnly(text: NSLocalizedString("好友请求发送成功！", comment: ""))
      }) { (code, message) in
        if message == "" {
          XBHHUD.showTextOnly(text: NSLocalizedString("好友请求发送失败", comment: ""))
        } else {
          XBHHUD.showTextOnly(text: "\(message)")
        }
      }
    }
    Utils.hideNoticeView()
  }
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.leftControl.isHidden = true
    self.centerControl.isHidden = true
    self.rightControl.isHidden = true

    addSubview(userInfoView)
    cancelButton.layer.cornerRadius = 5
    cancelButton.layer.masksToBounds = true
    setDownSeatLabel.text = "设为旁听".localized
    leaveRoomLabel.text = "踢出房间".localized
    closeSeatLabel.text = "封闭此麦位".localized
    closeSomeOneLabel.text = "禁言此人".localized
    setFreeLabel.text = "关自由模式".localized
    addFrendControl.layer.cornerRadius = 5
    addFrendControl.layer.masksToBounds = true
    sendGiftControl.layer.cornerRadius = 5
    sendGiftControl.layer.masksToBounds = true
    addBlackList.layer.cornerRadius = 10
    addBlackList.layer.masksToBounds = true
    reportLabel.layer.cornerRadius = 10
    reportLabel.layer.masksToBounds = true
    closeSomeOneControl.layer.masksToBounds = true
    setFreeControl.layer.masksToBounds = true
    userInfoView.addTarget(self, action: #selector(checkUserInfo), for: .touchUpInside)
    
    let addBlackTap = UITapGestureRecognizer.init(target: self, action: #selector(addBlackListAction))
    addBlackList.isUserInteractionEnabled = true
    addBlackList.addGestureRecognizer(addBlackTap)
    let reportTap = UITapGestureRecognizer.init(target: self, action: #selector(reportAction))
    reportLabel.isUserInteractionEnabled = true
    reportLabel.addGestureRecognizer(reportTap)
    
    sendGiftLabel.text = "送礼物".localized
    addFrendLabel.text = "加好友"
    addBlackList.text = "加入黑名单".localized
    reportLabel.text = "         举报".localized
    
  }
  
  class func showChatRoomChooseUser(_ isManger: Bool, _ model: ChatRoomUserModel?, _ position: Int) -> ChatRoomChooseUser {
    let view = Bundle.main.loadNibNamed(ChatRoomChooseUser.name(), owner: nil, options: nil)?.last as! ChatRoomChooseUser
    view.position = position
    view.model = model
    view.isManager = isManger
    return view
  }
  
  
  override func layoutSubviews() {
    super.layoutSubviews()
    userInfoView.frame = CGRect(x: 0, y: 50, width: width, height: 121)
    
  }
  
}
