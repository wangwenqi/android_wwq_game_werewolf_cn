//
//  ChatRoomShareOnLine.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/16.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomShareOnLine: BaseNoticeView {

    @IBOutlet weak var titleLabel: LTMorphingLabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sureButton: UIButton!
    var modelArray = [FriendListModel]() 
    override func initial() {
      self.width = Screen.width * 0.85
      self.height = Screen.height * 0.75
      self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
      self.backgroundColor = UIColor(hex: "#e2dee8")
      self.layer.cornerRadius = 6
    }
  
    let friendController = GameRoomFriendListViewController()
  
    class func showChatRoomShareOnLine(modelArray: [FriendListModel]) -> ChatRoomShareOnLine {
      let view = Bundle.main.loadNibNamed(ChatRoomShareOnLine.name(), owner: nil, options: nil)?.last as! ChatRoomShareOnLine
      view.friendController.modelArray = modelArray
      view.friendController.isCellBackClear = true
      return view
    }
  
    override func layoutSubviews() {
      super.layoutSubviews()
      friendController.view.frame = CGRect(x: 10, y: 50, width: width - 20, height: Screen.height * 0.75 - 120)
    }
  
    override func awakeFromNib() {
      super.awakeFromNib()
      cancelButton.layer.cornerRadius = 5
      sureButton.layer.cornerRadius = 5
      cancelButton.layer.masksToBounds = true
      sureButton.layer.masksToBounds = true
      addSubview(friendController.view)
      self.titleLabel.text = "选择邀请好友".localized
      sureButton.setTitle("确认".localized, for: .normal)
    }

    @IBAction func cancelAction(_ sender: Any) {
      Utils.hideNoticeView()
    }
  
    @IBAction func sureAction(_ sender: Any) {
      friendController.inviteFriend()
      Utils.hideNoticeView()
    }
}
