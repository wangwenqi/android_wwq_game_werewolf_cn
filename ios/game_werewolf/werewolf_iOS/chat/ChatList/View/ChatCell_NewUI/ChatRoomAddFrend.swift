//
//  ChatRoomAddFrend.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/12.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

struct ChatRoomAddFriendRequest {
  var id = ""
  var avaImage = ""
  var name = ""
  var level = ""
  var sex = ""
}

class ChatRoomAddFrend: SpringView {

  enum ButtonAction {
    case checkUserInfo
    case refuseAction
    case agreeAction
    case closeAction
  }
  var selectedBlock: ((ButtonAction) -> Void)?
  var model = ChatRoomAddFriendRequest() {
    didSet {
      nameLabel.text = model.name
      levelLabel.text = model.level
      id = model.id
      if !model.avaImage.isEmpty {
        headerImageView.sd_setImage(with: URL.init(string: model.avaImage), placeholderImage: UIImage(named: "room_head_default"))
      }
    }
  }
  @IBOutlet weak var refustLabel: UILabel!
  @IBOutlet weak var levelLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var headerImageView: UIImageView!
  @IBOutlet weak var checkUserInfoControl: UIControl!
  @IBOutlet weak var refuseControl: UIControl!
  @IBOutlet weak var agreeControl: UIControl!
    
  @IBAction func cancelButton_Action(_ sender: Any) {
    selectedBlock?(.closeAction)
  }

  var id = "" {
    didSet {
      RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id": id], success: { [weak self] (json) in
        XBHHUD.hide()
        let popular = json["popular"].stringValue
        if !popular.isEmpty {
          self?.levelLabel.text = "LV." + json["game"]["level"].stringValue + "      " + "人气值: " +  popular
        } else {
          self?.levelLabel.text = "LV." + json["game"]["level"].stringValue
        }
      }) { (code, message) in
        XBHHUD.hide()
        XBHHUD.showError(message)
      }
    }
  }
  
  func initial() {
    self.width = Screen.width * 0.85
    self.height = 175
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height - 175)
    self.backgroundColor = UIColor(hex: "#e2dee8")
    self.layer.cornerRadius = 7
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    self.initial()
    checkUserInfoControl.layer.cornerRadius = 6
    checkUserInfoControl.layer.masksToBounds = true
    
    refuseControl.layer.cornerRadius = 5
    refuseControl.layer.masksToBounds = true
    
    agreeControl.layer.cornerRadius = 5
    agreeControl.layer.masksToBounds = true
    
    headerImageView.layer.masksToBounds = true
    refustLabel.text = "拒绝".localized
  }
  
  @IBAction func checkUserAction(_ sender: Any) {
    selectedBlock?(.checkUserInfo)
  }
  
  @IBAction func refuseAction(_ sender: Any) {
    selectedBlock?(.refuseAction)
    RequestManager().post(url: RequestUrls.rejectFriend, parameters: ["friend_id":id], success: { (json) in
      print("=====\(json)")
      XBHHUD.showTextOnly(text: "拒绝好友请求".localized)
    }) { (code, message) in }
  }
  
  @IBAction func agreeAction(_ sender: Any) {
    selectedBlock?(.agreeAction)
    RequestManager().post(url: RequestUrls.acceptFriend, parameters: ["friend_id":id], success: { [weak self] (json) in
      XBHHUD.showTextOnly(text: "好友添加成功".localized)
      self?.tellRN(id:(self?.id)!)
    }) { (code, message) in
    }
  }
  
  func tellRN(id:String) {
    let data:[String: Any] = ["action":"ACTION_ADD_FRIEND","params":["user_id":id],"options":["needcallback":false,"sync":true]]
    let json = JSON.init(data)
    SwiftConverter.shareInstance.sendMessagesToRN(json: json)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    headerImageView.layer.cornerRadius = headerImageView.width / 2
  }
  
  class func showChatRoomAddFrend(_ request: ChatRoomAddFriendRequest) -> ChatRoomAddFrend {
    let chatRoomAddFrend = Bundle.main.loadNibNamed(ChatRoomAddFrend.name(), owner: nil, options: nil)?.last as! ChatRoomAddFrend
    chatRoomAddFrend.model = request
    return chatRoomAddFrend
  }
  
}



