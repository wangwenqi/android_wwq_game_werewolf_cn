//
//  ChatRoomLeaveRoom.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/12.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomLeaveRoom: BaseNoticeView {
  
    enum ButtonAction {
      case cancelButton
      case makeSureButton
    }
  
    var selectBlock: ((ButtonAction) -> Void)?
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sureButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
  @IBAction func cancelButtonAction(_ sender: Any) {
      Utils.hideNoticeView()
    }
    @IBAction func sureButtonAction(_ sender: Any) {
      selectBlock?(.makeSureButton)
      Utils.hideNoticeView()
    }
  
  override func initial() {
    self.width = Screen.width * 0.85
    self.height = 175
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.backgroundColor = UIColor(hex: "#e2dee8")
    self.layer.cornerRadius = 6
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    self.cancelButton.layer.cornerRadius = 5
    self.cancelButton.layer.masksToBounds = true
    
    self.sureButton.layer.cornerRadius = 5
    self.sureButton.layer.masksToBounds = true
    
    self.infoLabel.text = "是否离开房间?".localized
    self.titleLabel.text = "提 示"
    sureButton.setTitle("确认".localized, for: .normal)
  }
  
  class func showChatRoomLeaveRoom() -> ChatRoomLeaveRoom {
    let view = Bundle.main.loadNibNamed(ChatRoomLeaveRoom.name(), owner: nil, options: nil)?.last as! ChatRoomLeaveRoom
    return view
  }
  
}
