//
//  ChatRoomShareFrends.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/16.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import ChatKit

class ChatRoomShareFrends: BaseNoticeView {

    let tableView = UITableView()
    var roomId = ""
    var nameArray = ["游戏好友".localized, "家族好友", "微信好友", "朋友圈", "QQ好友"]
    var imageArray = ["chatRoom_lineFrend_Icon","chatRoom_familyInvite", "icon_wechat", "icon_discover", "icon_qq"]
    var undercover = false
    var roomTitle = ""
    var shareView: ChatRoomCertificate?
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var invoteLabels: UILabel!
    @IBAction func cancelAction(_ sender: Any) {
        Utils.hideNoticeView()
    }
  
  override func initial() {
    self.width = Screen.width * 0.85
    self.backgroundColor = UIColor(hex: "#e2dee8")
    self.layer.cornerRadius = 6
  }

  class func showChatRoomShareFrends(roomId: String, _ undercover: String, _ title: String?) -> ChatRoomShareFrends {
    let tmpView = Bundle.main.loadNibNamed(ChatRoomShareFrends.name(), owner: nil, options: nil)?.last as! ChatRoomShareFrends
    tmpView.roomId = roomId
    tmpView.undercover = undercover == "undercover"
    tmpView.roomTitle = title ?? ""
    if let groupID = CurrentUser.shareInstance.groupID, !groupID.isEmpty {
      tmpView.nameArray = ["游戏好友".localized, "家族好友", "微信好友", "朋友圈", "QQ好友"]
      tmpView.imageArray = ["chatRoom_lineFrend_Icon","chatRoom_familyInvite", "icon_wechat", "icon_discover", "icon_qq"]
    } else {
      tmpView.nameArray = ["游戏好友".localized, "微信好友", "朋友圈", "QQ好友"]
      tmpView.imageArray = ["chatRoom_lineFrend_Icon", "icon_wechat", "icon_discover", "icon_qq"]
    }
    tmpView.height = CGFloat(tmpView.nameArray.count * 60 + 118) //
    tmpView.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    return tmpView
  }
  
  class func showRoomOwner(_ mView: ChatRoomCertificate) -> ChatRoomShareFrends {
    let tmpView = Bundle.main.loadNibNamed(ChatRoomShareFrends.name(), owner: nil, options: nil)?.last as! ChatRoomShareFrends
    tmpView.shareView = mView
    tmpView.nameArray = ["微信好友", "朋友圈", "QQ好友"]
    tmpView.imageArray = ["icon_wechat", "icon_discover", "icon_qq"]
    tmpView.height = CGFloat(tmpView.nameArray.count * 60 + 118) //
    tmpView.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    return tmpView
  }
  
  override func awakeFromNib() {
      super.awakeFromNib()
      tableView.register(UINib(nibName: ChatRoonShareCell.name(), bundle: nil), forCellReuseIdentifier: "reuse")
      tableView.delegate = self
      tableView.dataSource = self
      tableView.rowHeight = 60
      tableView.backgroundColor = UIColor.clear
      tableView.separatorStyle = .none
    
      addSubview(tableView)
      self.cancelButton.layer.cornerRadius = 5
      self.cancelButton.layer.masksToBounds = true
      self.invoteLabels.text = "邀请好友".localized
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    tableView.frame = CGRect(x: CGFloat(30.0), y: CGFloat(50.0), width: CGFloat(width - 60), height: CGFloat(nameArray.count * 60))
  }
  
  func createImage(_ mView: ChatRoomCertificate) -> UIImage? {
    let size = mView.bounds.size
    UIGraphicsBeginImageContextWithOptions(size, false, 1)
    mView.layer.render(in: UIGraphicsGetCurrentContext()!)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
  }
  
  func shareSocialChatRoom(PlatformType: UMSocialPlatformType) -> Void {
    guard !roomId.isEmpty else {
      if let shareView = shareView {
       let tmpImage = createImage(shareView) ?? UIImage.init(named: "room_head_default")!
       let message = UMShareImageObject.shareObject(withTitle: "", descr: "", thumImage: tmpImage)
        message?.shareImage = tmpImage
        let sharesMessage = UMSocialMessageObject.init()
        sharesMessage.shareObject = message
        self.shareUM(sharesMessage, PlatformType)
      }
      return
    }
    let password = CurrentUser.shareInstance.currentRoomPassword.isEmpty ? "" : "密码\(CurrentUser.shareInstance.currentRoomPassword), "
    let info = self.undercover ? "与".localized + CurrentUser.shareInstance.name + "一起K歌, 嗨麦, 闲聊, 相亲, 算命, 情感咨询, 真心话大冒险, 掷骰子, 章鱼机, 玩小游戏..".localized : "认识萌妹帅哥百玩不厌的互动游戏, 跟附近趣味相投的好友一起寻找可爱的卧底..".localized
    let roomNum = NSLocalizedString("房间号", comment: "")
    let title = self.undercover ? "快玩小游戏吧外, 与萌妹一起玩谁是卧底吧".localized : roomTitle
    let message = UMShareWebpageObject.shareObject(withTitle: title, descr: "\(roomNum)\(roomId), \(password),\(info)", thumImage: UIImage(named: "shareIcon"))
    message?.webpageUrl = Config.urlHost
      + "/share_audio_kuaiwan?"
      + "roomid=\(roomId)&"
      + "type=audio&"
      + "password=\(CurrentUser.shareInstance.currentRoomPassword)"
    
    let sharesMessage = UMSocialMessageObject.init()
    sharesMessage.shareObject = message
    self.shareUM(sharesMessage, PlatformType)
  }
  
  func shareUM(_ sharesMessage: UMSocialMessageObject, _ PlatformType: UMSocialPlatformType) -> Void {
    UMSocialManager.default().share(to: PlatformType, messageObject:sharesMessage , currentViewController: nil, completion: { (data,error) in
      //点击分享统计
      MobClick.event("Share_\(PlatformType.description)")
      if error != nil {
        if (error! as NSError).code == 2008 {
          XBHHUD.showError(NSLocalizedString("未安装该应用", comment: ""))
        }else if (error! as NSError).code == 2005 {
          XBHHUD.showError(NSLocalizedString("分享内容不支持", comment: ""))
        }else if (error! as NSError).code == 2009 {
          
        }else if (error! as NSError).code == 2002 {
          XBHHUD.showError(NSLocalizedString("授权失败", comment: ""))
        }else if (error! as NSError).code == 2003 {
          XBHHUD.showError(NSLocalizedString("分享失败", comment: ""))
        }
      }else{
        if let sendType = Utils.getTaskType(platform: PlatformType, type: .normal) {
          print(NSLocalizedString("----------发送数据:\(sendType);;;count:\(1)", comment: ""));
          RequestManager().post(
            url: RequestUrls.sendTaskData,
            parameters: ["type": sendType, "count": 1],
            success: { (response) in
              
          }, error: { (code, message) in
          });
        }
      }
    })
  }
  
}




extension ChatRoomShareFrends: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: true)
      guard !roomId.isEmpty else {
        switch indexPath.row {
        case 0:
          self.shareSocialChatRoom(PlatformType: .wechatSession)
        case 1:
          self.shareSocialChatRoom(PlatformType: .wechatTimeLine)
        case 2:
          self.shareSocialChatRoom(PlatformType: .QQ)
        case 3:
          self.shareSocialChatRoom(PlatformType: .facebook)
        case 4:
          self.shareSocialChatRoom(PlatformType: .line)
        default:
          break
        }
        return
      }
      if let groupID = CurrentUser.shareInstance.groupID, !groupID.isEmpty {
        switch indexPath.row {
        case 0:
          self.inviteFrend()
        case 1:
          self.inviteFormFamily()
        case 2:
          self.shareSocialChatRoom(PlatformType: .wechatSession)
        case 3:
          self.shareSocialChatRoom(PlatformType: .wechatTimeLine)
        case 4:
          self.shareSocialChatRoom(PlatformType: .QQ)
        case 5:
          self.shareSocialChatRoom(PlatformType: .facebook)
        case 6:
          self.shareSocialChatRoom(PlatformType: .line)
        default:
          break
        }
      } else {
        switch indexPath.row {
        case 0:
          self.inviteFrend()
        case 1:
          self.shareSocialChatRoom(PlatformType: .wechatSession)
        case 2:
          self.shareSocialChatRoom(PlatformType: .wechatTimeLine)
        case 3:
          self.shareSocialChatRoom(PlatformType: .QQ)
        case 4:
          self.shareSocialChatRoom(PlatformType: .facebook)
        case 5:
          self.shareSocialChatRoom(PlatformType: .line)
        default:
          break
        }
      }
    }
  func generaterFriendlistJson() -> JSON {
    let data: [String:Any] = ["action":"ACTION_REQUEST_FRIENDS_LIST","options":["needcallback":true,"sync":true]]
    let json = JSON.init(data)
    return json
  }
  
  func inviteFrend() -> Void {
    // 游戏内好友
    MobClick.event("Invite_Friends")
    XBHHUD.showLoading()
    XBHHUD.hidenSuccess = {
      SwiftConverter.shareInstance.rnMessage = ["list":JSON.init([])]
    }
    //获取好友列表
    SwiftConverter.shareInstance.sendMessagesToRN(json: generaterFriendlistJson(), compeletion: { (message) in
      XBHHUD.hide()
      if let friend = message["list"]?.arrayValue {
        var modelArray = [FriendListModel]()
        for modelData in friend {
          let model = FriendListModel()
          model.userData = modelData
          model.isSelected = false
          modelArray.append(model)
        }
        DispatchQueue.main.async {
          let view = ChatRoomShareOnLine.showChatRoomShareOnLine(modelArray: modelArray)
          Utils.showNoticeView(view: view)
        }
      }
    })
  }
  
  func inviteFormFamily() {
    XBHHUD.showLoading()
    CurrentUser.shareInstance.getFamilyInfo { (family:Family?) in
      XBHHUD.hide()
      if let currentFamily = family {
        let lc_id = currentFamily.lc_id
        print("\(lc_id)")
        let conversation = LCChatKit.sharedInstance().client.conversation(forId: lc_id)
        let message = OLInviteMessage(attibutes: [
          INVITE_MSG_KEY_ROOM_ID: "\(CurrentUser.shareInstance.currentRoomID)",
          "GAME_TYPE": CurrentUser.shareInstance.currentRoomType,
          "child_type": SwiftConverter.shareInstance.roomTypeName,
          INVITE_MSG_KEY_PASSWORD: CurrentUser.shareInstance.currentRoomPassword,
          "USER_SEX": "\(CurrentUser.shareInstance.sex)",
          "USER_ICON": CurrentUser.shareInstance.avatar,
          "USER_NAME": CurrentUser.shareInstance.name,
          "USER_ID": CurrentUser.shareInstance.id,
          "MSG_TYPE": "FAMILY",
          "APP": Config.app,
          "MESSAGE_TYPE": "MESSAGE_TYPE_CHAT",
          "CHAT_TIME": Utils.getCurrentTimeStamp()
          ]);
        //此时也要发送通知，告诉RN更新
        conversation?.send(message, callback: { (isSucceeded, error) in
          if isSucceeded {
            XBHHUD.showTextOnly(text:NSLocalizedString("邀请成功", comment: ""))
            let dic =
              ["USER_NAME":CurrentUser.shareInstance.name,
               "USER_SEX":CurrentUser.shareInstance.sex,
               "USER_ICON":CurrentUser.shareInstance.avatar,
               "MESSAGE_TYPE":"MESSAGE_TYPE_CHAT",
               "USER_ID":CurrentUser.shareInstance.id,
               "MSG_TYPE": "FAMILY",
               "APP": Config.app,
               "CHAT_TIME":String(Utils.getCurrentTimeStamp()),
               "CHAT_MESSAGE":"游戏邀请发送成功","READ":true] as [String : Any]
            NotificationCenter.default.post(name: NotifyName.kSendMessToRN.name(), object: dic)
          } else {
            XBHHUD.showTextOnly(text:NSLocalizedString("邀请失败", comment: ""))
          }
        })
      } else {
        XBHHUD.hide()
        XBHHUD.showError("家族信息异常")
      }
    }
  }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuse") as! ChatRoonShareCell
        cell.headerImageView.image = UIImage(named: imageArray[indexPath.row])
        cell.headerLabel.text = nameArray[indexPath.row]
        return cell
    }
}
