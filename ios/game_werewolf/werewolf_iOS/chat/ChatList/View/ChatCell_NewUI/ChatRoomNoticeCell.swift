//
//  ChatRoomNoticeCell.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/11.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomNoticeCell: UITableViewCell {

  var model = ChatRoomChatList.ChatMessgeModel() {
    didSet {
      self.messageLabel.text = model.message
      switch model.colorType {
      case .leaveRoom, .enterRoom:
        messageLabel.textColor = UIColor(hex: "#7b87d2")
      case .upSeat, .downSeat, .forceSeat:
        messageLabel.textColor = UIColor(hex: "#d09a61")
      case .freeModel:
        messageLabel.textColor = UIColor(hex: "#55c7ca")
      case .hide:
        messageLabel.textColor = UIColor.white
      default:
        messageLabel.textColor = UIColor(hex: "#908993")
      }
    }
  }

  fileprivate let messageLabel = UILabel()
  fileprivate let messageBackGoundView = UIView()

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    self.backgroundColor = UIColor.clear
    messageLabel.font = UIFont.systemFont(ofSize: 12)
    messageLabel.textColor = UIColor.white
    messageLabel.textAlignment = .center
    messageLabel.numberOfLines = 0
    
    messageBackGoundView.layer.cornerRadius = 5
    messageBackGoundView.layer.masksToBounds = true
    messageBackGoundView.backgroundColor = UIColor.white
    messageBackGoundView.alpha = 0.2
    
    addSubview(messageBackGoundView)
    addSubview(messageLabel)
    
    messageBackGoundView.snp.makeConstraints { (make) in
      make.top.equalToSuperview().offset(3)
      make.bottom.equalToSuperview().offset(-3)
      make.left.equalToSuperview().offset(35)
      make.right.equalToSuperview().offset(-35)
    }
    
    messageLabel.snp.makeConstraints { (make) in
      make.top.equalToSuperview().offset(8)
      make.bottom.equalToSuperview().offset(-8)
      make.left.equalToSuperview().offset(40)
      make.right.equalToSuperview().offset(-40)
    }
    
    selectionStyle = .none
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)
  }

}
