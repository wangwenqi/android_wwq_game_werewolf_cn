//
//  ChatRoomForceSeat.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/12.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomForceSeat: BaseNoticeView {

  override func initial() {
    self.width = Screen.width * 0.7
    self.height = Screen.width * 0.7
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.backgroundColor = UIColor(hex: "#e2dee8")
    self.layer.cornerRadius = 6
  }
  
    @IBOutlet weak var makeSureButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBAction func makeSureAction(_ sender: Any) {
        Utils.hideNoticeView()
    }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    makeSureButton.layer.cornerRadius = 5
    makeSureButton.layer.masksToBounds = true
    
    infoLabel.text = "房主把我抱上麦".localized
    titleLabel.text = "提示"
    makeSureButton.setTitle("确认".localized, for: .normal)
  }
  
  class func showChatRoomForceSeat() -> Void {
    let chatRoomAddFrend = Bundle.main.loadNibNamed(ChatRoomForceSeat.name(), owner: nil, options: nil)?.last as! ChatRoomForceSeat
    Utils.showNoticeView(view: chatRoomAddFrend)
  }
  
}
