//
//  ChatRoomReportUser.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomReportUser: BaseNoticeView {
  
    var uid = ""
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sureButton: UIButton!
    var textViewIsEdited = false
    var reportNumber = -1
    let reasonArray = ["广告".localized, "逃跑", "色情".localized, "欺诈".localized, "开黑".localized, "骂人".localized, "黑麦".localized]
    var reportReasonArray = [SelectUserView]()
  
    override func initial() {
      self.width = Screen.width * 0.85
      self.height = CGFloat(173 + 96 + reasonArray.count / 2 * 40) //
      self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
      self.backgroundColor = UIColor(hex: "#e2dee8")
      self.layer.cornerRadius = 6
    }
  
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      super.touchesBegan(touches, with: event)
      inputTextView.resignFirstResponder()
    }
  
    override func awakeFromNib() {
      super.awakeFromNib()
      cancelButton.layer.cornerRadius = 5
      cancelButton.layer.masksToBounds = true
      sureButton.layer.cornerRadius = 5
      sureButton.layer.masksToBounds = true
      inputTextView.layer.cornerRadius = 5
      inputTextView.layer.masksToBounds = true
      inputTextView.layer.borderColor = UIColor(hex: "#8460E3").cgColor
      inputTextView.layer.borderWidth = 1
      inputTextView.text = "输入举报理由, 运营人员会优先处理...".localized
      sureButton.setTitle("确认".localized, for: .normal)
      for i in 0..<reasonArray.count {
        let tmpView = SelectUserView(title: reasonArray[i])
        tmpView.markNumber = i
        tmpView.addTarget(self, action: #selector(chooseReason(control:)), for: .touchUpInside)
        addSubview(tmpView)
        reportReasonArray.append(tmpView)
      }
      
      inputTextView.returnKeyType = .done
      inputTextView.delegate = self
    }
  
    func dismissKeyboard() -> Void {
      self.endEditing(true)
    }
  
    func chooseReason(control: SelectUserView) -> Void {
      print(control.markNumber)
      reportNumber = -1
      for i in 0..<reportReasonArray.count {
        let reasonView = reportReasonArray[i]
        reasonView.selectState = false
      }
      control.selectState = true
      reportNumber = control.markNumber
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
      Utils.hideNoticeView()
    }
  
    @IBAction func sureButtonAction(_ sender: Any) {
      var reportType = ""
      switch reportNumber {
      case 0:
        reportType = "ad"       //  广告
      case 1:
        reportType = "escape"   // 逃跑
      case 2:
        reportType = "sex"      // 色情
      case 3:
        reportType = "cheat"   // 欺诈
      case 4:
        reportType = "gang_up"    // 开黑
      case 5:
        reportType = "abuse"    // 骂人
      case 6:
        reportType = "trouble"  // 黑麦
      default:
        reportType = ""
      }
      
      if reportType == "" {
        XBHHUD.showError(NSLocalizedString("请选择举报的类型", comment: ""))
        return
      }
      
      var reportMessage = ""
      if textViewIsEdited {
        reportMessage = inputTextView.text
        if inputTextView.text.length > 200 {
          XBHHUD.showError(NSLocalizedString("举报内容太长", comment: ""))
          return
        }
      }
      
      let para = ["user_id": uid,
                  "type": reportType,
                  "message": reportMessage]
      RequestManager().post(url: RequestUrls.report, parameters: para, success: { _ in
        Utils.hideNoticeView()
        XBHHUD.hide()
        XBHHUD.showSuccess(NSLocalizedString("我们致力于维护一个良好的游戏环境，对您的举报我们会严肃处理。", comment: ""))
      }) { (code, message) in
        XBHHUD.hide()
        XBHHUD.showError(message)
      }

    }
  
   class func showChatRoomReportUser(uid: String) -> Void {
    let showView = Bundle.main.loadNibNamed(ChatRoomReportUser.name(), owner: nil, options: nil)?.last as! ChatRoomReportUser
    showView.uid = uid
    Utils.showAlertView(view: showView)
    }
  
    override func layoutSubviews() {
      super.layoutSubviews()
      
      for i in 0..<reportReasonArray.count {
        let selectView = reportReasonArray[i]
        let x: CGFloat = 0 == i % 2  ? 0 : self.frame.width / 2
        let line: Int = i / 2
        let y: CGFloat = CGFloat(line * 40) + 51
        selectView.frame = CGRect(x: x, y: y, width: self.frame.width / 2, height: 20)
      }
    }
  
}
extension ChatRoomReportUser: UIGestureRecognizerDelegate {
  
}

extension ChatRoomReportUser: UITextViewDelegate {
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    if !textViewIsEdited {
      textView.text = ""
      textViewIsEdited = true
    }
  }
  
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    if text == "\n" {
      textView.resignFirstResponder()
      return false
    }
    return true
  }
  
}

class SelectUserView: UIControl {
  
  let selectImage = UIImageView()
  let label = UILabel()
  var markNumber = 0
  var selectState = false {
    didSet {
      if selectState {
        selectImage.image = UIImage.init(named: "charRoom_reportSelected")
      } else {
        selectImage.image = UIImage.init(named: "charRoom_reportUnSelected")
      }
    }
  }
  
  convenience init(title: String) {
    self.init(frame: CGRect(x: 0, y: 0, width: 100, height: 20))
    selectImage.image = UIImage.init(named: "charRoom_reportUnSelected")
    label.text = title
    addSubview(selectImage)
    addSubview(label)
    label.textColor = UIColor(hex: "#3C1B69")
    label.font = UIFont.systemFont(ofSize: 14)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    selectImage.frame = CGRect(x: self.frame.width / 2 - 25, y: 0, width: 20, height: 20)
    label.frame = CGRect(x: self.frame.width / 2, y: 0, width: 60, height: 20)
  }
  
}


