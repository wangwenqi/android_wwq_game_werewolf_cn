//
//  ChatRoomScoreCell.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/2.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomScoreCell: UITableViewCell {

  var model = ChatRoomChatList.ChatMessgeModel() {
    didSet {
      switch model.positionNumber {
      case "0", "1", "2", "3", "4", "5", "6", "7", "8":
        headerImage.image = UIImage(named: "chatPosition_\(model.positionNumber)")
      default:
        headerImage.image = UIImage(named: "chatPosition_x")
      }
      nameLabbel.text = model.name
      scoreImage.image = UIImage.init(named: "chatViewGame_ points_\(model.score)")
    }
  }
  
  fileprivate let headerImage = UIImageView()
  fileprivate let scoreImage = UIImageView()
  fileprivate let nameLabbel = UILabel()
  // 辅助背景
  fileprivate let nameBackView = UIView()
  fileprivate let messageBackView = UIView()
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    self.backgroundColor = UIColor.clear
    
    nameLabbel.textColor = UIColor.white
    nameLabbel.font = UIFont.systemFont(ofSize: 12)
    
    scoreImage.contentMode = .scaleAspectFit
    
    addSubview(nameBackView)
    addSubview(messageBackView)
    
    addSubview(headerImage)
    addSubview(nameLabbel)
    addSubview(scoreImage)
    
    headerImage.snp.makeConstraints { (make) in
      make.left.equalToSuperview().offset(15)
      make.top.equalToSuperview().offset(3)
      make.height.width.equalTo(20)
    }
    
    nameLabbel.snp.makeConstraints { (make) in
      make.left.equalTo(headerImage.snp.right).offset(5)
      make.top.equalToSuperview().offset(3)
      make.height.equalTo(20)
    }
    
    nameBackView.backgroundColor = UIColor(hex: "#f19201")
    nameBackView.layer.cornerRadius = 5
    nameBackView.layer.masksToBounds = true
    nameBackView.snp.makeConstraints { (make) in
      make.left.equalTo(headerImage.snp.right)
      make.right.equalTo(nameLabbel.snp.right).offset(5)
      make.top.equalToSuperview().offset(3)
      make.height.equalTo(20)
    }
    
    messageBackView.backgroundColor = UIColor.white
    messageBackView.alpha = 0.2
    messageBackView.layer.cornerRadius = 5
    messageBackView.layer.masksToBounds = true
    messageBackView.snp.makeConstraints { (make) in
      make.left.equalToSuperview().offset(35)
      make.top.equalTo(nameLabbel.snp.bottom).offset(3)
      make.width.equalTo(80)
      make.bottom.equalToSuperview().offset(-5)
    }
    
    scoreImage.snp.makeConstraints { (make) in
      make.top.equalTo(nameLabbel.snp.bottom)
      make.left.equalToSuperview().offset(35)
      make.height.equalTo(60)
      make.width.equalTo(80)
      make.bottom.equalToSuperview().offset(-5)
    }
    
    selectionStyle = .none
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
