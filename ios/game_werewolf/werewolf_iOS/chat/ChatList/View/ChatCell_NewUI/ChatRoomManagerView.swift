
//
//  ChatRoomManagerView.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/11.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomManagerView: BaseNoticeView {

    @IBOutlet weak var roomPassword: UILabel!
    @IBOutlet weak var roomTitle: UILabel!
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var blackListControl: UIControl!
    @IBOutlet weak var blackListWitdh: NSLayoutConstraint! // 利用这个复用三个或者两个icon
    @IBOutlet weak var blackLabel: UILabel!
    
    var seletdBlock: ((ButtonAction) -> Void)?
    enum ButtonAction {
      case leftChoose
      case rightChoose
      case blackChoose
    }
  
    enum PageState {
      case chooseEmpty
      case changeRoomInfo
      case chooseLock
    }
    var pageState: PageState = .chooseEmpty {
      didSet {
        switch pageState {
        case .chooseEmpty:
          roomTitle.text = "抱用户上麦克".localized
          roomPassword.text = "封闭此麦位".localized
          blackListWitdh.constant = 0
          leftImageView.image = UIImage(named: "chatRoom_EmptyUser_user")
          rightImageView.image = UIImage(named: "chatRoom_EmptyUser_closeMic")
        case .changeRoomInfo:
          roomTitle.text = "修改房间主题".localized
          roomPassword.text = "修改房间密码"
          blackListWitdh.constant = (Screen.width * 0.85 - 50) / 3
          leftImageView.image = UIImage(named: "chatRoom_managerUserTitle")
          rightImageView.image = UIImage(named: "chatRoom_managerUserPassword")
        case.chooseLock:
          roomTitle.text = "抱用户上麦克".localized
          roomPassword.text = "解锁此麦位".localized
          blackListWitdh.constant = 0
          leftImageView.image = UIImage(named: "chatRoom_EmptyUser_user")
          rightImageView.image = UIImage(named: "chatRoom_unLock")
        }
      }
    }
    override func initial() {
    self.width = Screen.width * 0.85
    self.height = 175
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.backgroundColor = UIColor(hex: "#e2dee8")
    self.layer.cornerRadius = 6
  }

    @IBAction func cancelAction(_ sender: Any) {
        Utils.hideNoticeView()
    }
  
    @IBAction func changeRoomTitle(_ sender: Any) {
        Utils.hideNoticeView()
        seletdBlock?(.leftChoose)
    }
  
    @IBAction func changeRoomPassword(_ sender: Any) {
        Utils.hideNoticeView()
        seletdBlock?(.rightChoose)
    }
  
    @IBAction func blackAction(_ sender: Any) {
      Utils.hideNoticeView()
      seletdBlock?(.blackChoose)
    }
  
    override func awakeFromNib() {
      super.awakeFromNib()
      cancelButton.layer.cornerRadius = 5
      cancelButton.layer.masksToBounds = true
      blackListControl.layer.masksToBounds = true
      blackLabel.text = "房间黑名单".localized
    }
  
  override func layoutSubviews() {
    super.layoutSubviews()
  }
  
  class func showChatRoomManagerView(_ state: PageState) -> ChatRoomManagerView {
      let chatRoomManagerView = Bundle.main.loadNibNamed(ChatRoomManagerView.name(), owner: nil, options: nil)?.last as! ChatRoomManagerView
      chatRoomManagerView.pageState = state
      return chatRoomManagerView
   }
  
  
}
