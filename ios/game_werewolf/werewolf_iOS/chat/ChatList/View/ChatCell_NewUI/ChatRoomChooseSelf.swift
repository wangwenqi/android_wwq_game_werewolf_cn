//
//  ChatRoomChooseSelf.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/13.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomChooseSelf: BaseNoticeView {

    var isManager = false {
      didSet {
        if isManager {
          if model?.state == "free" {
            downSeatButton.setTitle("关自由模式".localized, for: UIControlState.normal)
          } else {
            downSeatButton.setTitle("开自由模式".localized, for: UIControlState.normal)
          }
        } else {
          downSeatButton.setTitle("下麦旁听".localized, for: UIControlState.normal)
        }
      }
    }
    var model: ChatRoomUserModel? {
      didSet {
        if let id = model?.id {
          XBHHUD.showLoading()
          RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id": id], success: { [weak self] (json) in
            XBHHUD.hide()
            self?.userInfoView.json = json
          }) { (code, message) in
            XBHHUD.hide()
            XBHHUD.showError(message)
          }
        }
      }
    }
    enum ButtonAction {
      case downSeat
      case checkUserInfo
      case sendGift
    }
    @IBOutlet weak var sendGiftLabel: UILabel!
    @IBOutlet weak var sendGiftButton: UIControl!
    var selectBlock: ((ButtonAction) -> Void)?
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var downSeatButton: UIButton!
    let userInfoView = ChatRoomOwnUserInfo(frame: CGRect.zero)
  
    @IBAction func sendGiftAction(_ sender: Any) {
        selectBlock?(.sendGift)
        Utils.hideNoticeView()
    }
    @IBAction func cancelAction(_ sender: Any) {
      Utils.hideNoticeView()
    }
    @IBAction func downAction(_ sender: Any) {
      selectBlock?(.downSeat)
      Utils.hideNoticeView()
    }
  
  func checSelfInfo() -> Void {
    Utils.hideNoticeView()
    selectBlock?(.checkUserInfo)
  }
  
  override func initial() {
    self.width = Screen.width * 0.85
    self.height = 264
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.backgroundColor = UIColor(hex: "#e2dee8")
    self.layer.cornerRadius = 6
  }
  
  class func showChatRoomChooseSelf(_ isManger: Bool, _ model: ChatRoomUserModel?) -> ChatRoomChooseSelf {
    let view = Bundle.main.loadNibNamed(ChatRoomChooseSelf.name(), owner: nil, options: nil)?.last as! ChatRoomChooseSelf
    view.model = model
    view.isManager = isManger
    return view
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    addSubview(userInfoView)
    cancelButton.layer.cornerRadius = 5
    cancelButton.layer.masksToBounds = true
    
    downSeatButton.layer.cornerRadius = 5
    downSeatButton.layer.masksToBounds = true
    
    sendGiftButton.layer.cornerRadius = 5
    sendGiftButton.layer.masksToBounds = true
    
    userInfoView.addTarget(self, action: #selector(checSelfInfo), for: .touchUpInside)
    self.downSeatButton.setTitle("下麦旁听".localized, for: .normal)
    self.sendGiftLabel.text = "送礼物".localized
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    userInfoView.frame = CGRect(x: 0, y: 15, width: width, height: 121)
  }
}
