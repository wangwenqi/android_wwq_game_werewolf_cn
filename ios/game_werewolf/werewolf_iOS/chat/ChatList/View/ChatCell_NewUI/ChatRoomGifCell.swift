//
//  ChatRoomGifCell.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/21.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomGifCell: UICollectionViewCell {

  var model: ChatGifModel? {
    didSet {
      if let model = model {
        let type = ChatVoiceRoomViewController.GifFileNameURL(rawValue: ("Gif_" + model.mark))
        giftLabel.text = type?.textName()
        if let image = type?.imageFile() {
          gifImageView.image = image
        } else {
          if let url = URL.init(string: model.thumbnail) {
            gifImageView.sd_setImage(with: url, placeholderImage: placeHolder)
          }
        }
      }
    }
  }
  let gifImageView = UIImageView()
  let giftLabel = UILabel()
  let placeHolder = UIImage(named: "chatGif_placeholder")
  override init(frame: CGRect) {
    super.init(frame: frame)
    giftLabel.font = UIFont.systemFont(ofSize: 11)
    giftLabel.textColor = UIColor(hex: "#C0C0C0")
    giftLabel.textAlignment = .center
    addSubview(giftLabel)
    gifImageView.image = placeHolder
    gifImageView.contentMode = .scaleAspectFit
    addSubview(gifImageView)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    gifImageView.frame = CGRect(x: 0, y: 0, width: width, height: width)
    giftLabel.frame = CGRect(x: 0, y: width, width: width, height: 15)
  }
  
}

