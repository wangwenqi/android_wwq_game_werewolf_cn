//
//  ChatMessageUserInfo.swift
//  game_werewolf
//
//  Created by xiaozao on 2018/1/9.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ChatMessageUserInfo: BaseNoticeView {
  
  enum buttonAction: Int {
    case addToBlock
    case sendGift
  }
  
  var selectBlock: ((buttonAction) -> Void)?
  var model: ChatRoomUserModel? {
    didSet {
      if let model = model {
        if let url = URL.init(string: model.avatar) {
          headerImage.sd_setImage(with: url, placeholderImage: UIImage.init(named: "room_head_default"))
        } else {
          headerImage.image = UIImage.init(named: "room_head_default")
        }
        nameLabel.text = model.name
        levelLabel.text = "LV: " + "\(model.level)"
      }
    }
  }
  
  override func initial() {
    self.width = Screen.width * 0.85
    self.height = 190
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.7)
    self.layer.cornerRadius = 6
    self.backgroundColor = UIColor.noticeBackGroundColor()
  }
  
  override init(frame: CGRect) {
    super.init(frame: .zero)
    addSubview(headerImage)
    headerImage.layer.cornerRadius = 30
    headerImage.layer.masksToBounds = true
    addSubview(nameLabel)
    nameLabel.font = UIFont.systemFont(ofSize: 14)
    addSubview(levelLabel)
    levelLabel.font = UIFont.systemFont(ofSize: 12)
    levelLabel.textColor = UIColor.oragenBack()
    addSubview(rightImage)
    rightImage.contentMode = .scaleAspectFit
    addSubview(kickout)
    addSubview(sendGift)
    addSubview(cancel)
    kickout.setTitle("拉黑并踢出".localized, for: .normal)
    kickout.layer.cornerRadius = 5
    kickout.layer.masksToBounds = true
    kickout.setTitleColor(UIColor.white, for: .normal)
    kickout.backgroundColor = UIColor.purpleBackColor()
    let attch = NSTextAttachment.init()
    attch.image = UIImage.init(named: "chatRoom_gift_backicon")
    attch.bounds = CGRect.init(x: 0, y: -1, width: 15, height: 15)
    let icoAttribute = NSAttributedString.init(attachment: attch)
    let tmpStr = NSMutableAttributedString.init(attributedString: icoAttribute)
    let sendGiftStr = NSAttributedString.init(string: " " + "送礼物".localized, attributes: [NSForegroundColorAttributeName: UIColor.white])
    tmpStr.append(sendGiftStr)
    sendGift.setAttributedTitle(tmpStr, for: UIControlState.normal)
    sendGift.backgroundColor = UIColor.oragenBack()
    sendGift.layer.cornerRadius = 5
    sendGift.layer.masksToBounds = true
    cancel.setTitle("取消".localized, for: .normal)
    cancel.backgroundColor = UIColor.white
    cancel.layer.cornerRadius = 5
    cancel.layer.masksToBounds = true
    cancel.setTitleColor(UIColor.textPurpleDarkColor(), for: .normal)
    rightImage.image = UIImage.init(named: "chatRoom_right_addFrend")
    
    kickout.rx.tap.subscribe(onNext: { [weak self] (_) in
      self?.selectBlock?(.addToBlock)
    }).disposed(by: disposeBag)
    
    sendGift.rx.tap.subscribe(onNext: { [weak self] (_) in
      self?.selectBlock?(.sendGift)
    }).disposed(by: disposeBag)
    
    cancel.rx.tap.subscribe(onNext: { (_) in
      Utils.hideAnimated()
    }).disposed(by: disposeBag)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  let headerImage = UIImageView()
  let nameLabel = UILabel()
  let levelLabel = UILabel()
  let rightImage = UIImageView()
  let kickout = UIButton.init(type: UIButtonType.system)
  let sendGift = UIButton.init(type: UIButtonType.system)
  let cancel = UIButton.init(type: UIButtonType.system)
  let disposeBag = DisposeBag.init()
  
  override func layoutSubviews() {
    super.layoutSubviews()
    headerImage.frame = CGRect.init(x: 30, y: 15, width: 60, height: 60)
    nameLabel.frame = CGRect.init(x: 100, y: 15, width: width - 155, height: 30)
    levelLabel.frame = CGRect.init(x: 100, y: 45, width: nameLabel.width, height: nameLabel.height)
    rightImage.frame = CGRect(x: width - 40, y: 40, width: 10, height: 10)
    kickout.frame = CGRect.init(x: 30, y: 90, width: (width - 80) / 2, height: 38)
    sendGift.frame = CGRect.init(x: (width - 80) / 2 + 50, y: 90, width: (width - 80) / 2, height: 38)
    cancel.frame = CGRect.init(x: 30, y: sendGift.maxY + 10, width: width - 60, height: 38)
  }
}
