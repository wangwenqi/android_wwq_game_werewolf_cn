//
//  ChatRoomCertificateVC.swift
//  game_werewolf
//
//  Created by xiaozao on 2018/2/2.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomCertificate: UIView {

  let bigBackImage1 = UIImageView()
  let bigBackImage2 = UIImageView()
  let codeImage = UIImageView()
  let headerImage = UIImageView()
  let iconImage = UIImageView()
  let markIcon = UIImageView()
  
  let titleLabel = UILabel()
  let nameLabel = UILabel()
  let roomNumber = UILabel()
  let roomID = UILabel()
  let infoLabel = UILabel()
  let roomOwner = UILabel()
  let timeLabel = UILabel()
  
  let leftSpace: CGFloat = 60
  let topSpace: CGFloat = 60
  let iconSpace: CGFloat = 80
  
  convenience init(_ frame: CGRect, _ data: JSON) {
    self.init(frame: .zero)
    XBHHUD.showLoading("正在生成图片".localized)
    let name = data["name"].stringValue
    let roomid = data["room_id"].stringValue
    let img = data["image"].stringValue
    let uid = data["uid"].stringValue
    let applyTime = data["apply_time"].intValue / 1000
    
    titleLabel.text = "快玩小游戏吧房产证".localized
    nameLabel.text = "昵称".localized + ": " + name
    roomNumber.text = "房间号" + ": " + roomid
    roomID.text = "ID: " + uid
    print(img)
    print(data)
    let timeFormater = DateFormatter.init(withFormat: "yyyy年MM月dd日hh:mm", locale: "cn")
    let time = Date.init(timeIntervalSince1970: TimeInterval.init(applyTime))
    let timeStr = timeFormater.string(from: time)
    infoLabel.text = "    " + "从".localized + timeStr + "开始".localized + name + "正式成为".localized + roomid + "房间所有者, 您可以在快玩小游戏吧中寻找到心中的另一半, 共同铸造爱的小屋, 或者和家族好友一起构建温馨的家园.  \n    特此证明"
    infoLabel.numberOfLines = 0
    roomOwner.text = roomid + "房间所有者".localized
    let shortTime = DateFormatter.init(withFormat: "yyyy-MM-dd hh:mm", locale: "cn")
    timeLabel.text = shortTime.string(from: time)
    headerImage.image = UIImage.init(named: "room_head_default")
    if let imageUrl  = URL.init(string: img) {
      headerImage.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named: "room_head_default"))
    }
    bigBackImage1.image = UIImage.init(named: "shareCert_back1")
    bigBackImage2.image = UIImage.init(named: "shareCert_back2")
    codeImage.image = UIImage.init(named: "shareCert_code")
    markIcon.image = UIImage.init(named: "shareCert_sign")
    XBHHUD.hide()
  }
  
  override init(frame: CGRect) {
    super.init(frame: CGRect(x: 0, y: 0, width: 800, height: 500))
    addSubview(bigBackImage1)
    addSubview(bigBackImage2)
    addSubview(titleLabel)
    addSubview(roomNumber)
    addSubview(roomID)
    addSubview(infoLabel)
    addSubview(roomOwner)
    addSubview(timeLabel)
    addSubview(codeImage)
    addSubview(markIcon)
    addSubview(iconImage)
    addSubview(headerImage)
    addSubview(nameLabel)
    bigBackImage1.frame = CGRect(x: 0, y: 0, width: 800, height: 500)
    bigBackImage2.frame = CGRect(x: 30, y: 15, width: 800 - 60, height: 470)
    codeImage.frame = CGRect(x: 613, y: 0, width: 107, height: 127)
    iconImage.frame = CGRect(x: leftSpace + 10, y: topSpace - 10, width: iconSpace, height: iconSpace)
    iconImage.image = UIImage.init(named: "AppIcon")
    titleLabel.frame = CGRect(x: 0, y: topSpace, width: 800, height: 50)
    titleLabel.font = UIFont.systemFont(ofSize: 30)
    titleLabel.textColor = UIColor.rgb(63 / 255, 41 / 255, 39 / 255)
    titleLabel.textAlignment = .center
    headerImage.frame = CGRect(x: 365, y: topSpace + 50, width: 70, height: 70)
    headerImage.layer.cornerRadius = 35
    headerImage.layer.masksToBounds = true
    nameLabel.frame = CGRect(x: 0, y: 180, width: 800, height: 30)
    nameLabel.textAlignment = .center
    roomNumber.frame = CGRect(x: 0, y: 210, width: 800, height: 30)
    roomNumber.textAlignment = .center
    roomID.frame = CGRect(x: 0, y: 240, width: 800, height: 30)
    roomID.textAlignment = .center
    infoLabel.frame = CGRect(x: leftSpace + 40, y: 270, width: 800 - 2 * leftSpace - 80, height: 100)
    markIcon.frame = CGRect(x: 640, y: 340, width: 80, height: 80)
    roomOwner.frame = CGRect(x: 0, y: 380, width: 800 - 80, height: 20)
    roomOwner.textAlignment = .right
    timeLabel.frame = CGRect(x: 0, y: 400, width: 800 - 80, height: 20)
    timeLabel.textAlignment = .right
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
