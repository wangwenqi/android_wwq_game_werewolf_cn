//
//  ChatRoomDintingSetting.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomDintingSetting: BaseNoticeView {

  let titleLabel = UILabel()      // 卧底设置
  let subTitleLabel = UILabel()   // 可点击下框, 自行输入卧底和好人的命题
  let desLabel = UILabel()        // 说明 [3-5人: 1个卧底]
  let textField1 = UITextField()  // 好人词
  let textField2 = UITextField()  // 卧底词
  let systemWordButton = UIButton.init(type: .system)   // 系统词库
  let userWoldButton = UIButton.init(type: .system)     // 玩家词库
  let cancelButton = UIButton.init(type: .system)       // 取消
  let sureButton = UIButton.init(type: .system)         // 确认
  let addDintingControl = ChatRoomDintingView("增加一个卧底(8人可使用)".localized)    // 是否增加一个卧底
  let setGhostControl = ChatRoomDintingView("设置卧底为幽灵".localized)      // 是否设置卧底为白板
  let insertMicControl = ChatRoomDintingView("是否可插麦".localized)     // 是否可以插麦
  let canGuessControl = ChatRoomDintingView("卧底是否可猜词".localized)      // 卧底是否可以猜词
  
  override func initial() {
    self.width = Screen.width * 0.85
    self.height = 455
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.layer.cornerRadius = 6
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.initial()
    self.backgroundColor = UIColor.chatNoticeBack()
    titleLabel.text = "卧底设置".localized
    desLabel.text = "说明: [3-5人: 1个卧底]  [6-8人: 2个卧底]".localized
    subTitleLabel.text = "可点击下框, 自行输入卧底和好人的词语".localized
    userWoldButton.setTitle("用户词库".localized, for: .normal)
    systemWordButton.setTitle("系统词库".localized, for: .normal)
    cancelButton.setTitle("取消".localized, for: .normal)
    sureButton.setTitle("确定".localized, for: .normal)
    setLabelAttribution(label: titleLabel, alignment: .center, font: 17)
    setLabelAttribution(label: subTitleLabel, alignment: .center, font: 14)
    setLabelAttribution(label: desLabel, alignment: .center, font: 12)
    setTextFieldAttributon(textField: textField1)
    textField1.placeholder = "请输入第一个词语".localized
    textField2.placeholder = "请输入第二个词语".localized
    textField1.delegate = self
    textField2.delegate = self
    setTextFieldAttributon(textField: textField2)
    setButtonAttribution()
    [titleLabel, subTitleLabel, desLabel, textField1, textField2, systemWordButton, userWoldButton, cancelButton, sureButton, addDintingControl, setGhostControl, insertMicControl, canGuessControl, cancelButton, sureButton].forEach { (tmpView) in
      addSubview(tmpView)
    }
    chatRoomColor()
    systemWordButton.addTarget(self, action: #selector(systemRequest), for: .touchUpInside)
    userWoldButton.addTarget(self, action: #selector(userRequest), for: .touchUpInside)
    cancelButton.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
    sureButton.addTarget(self, action: #selector(sureAction), for: .touchUpInside)
    
    addDintingControl.addTarget(self, action: #selector(chooseControl(control:)), for: .touchUpInside)
    setGhostControl.addTarget(self, action: #selector(chooseControl(control:)), for: UIControlEvents.touchUpInside)
    insertMicControl.addTarget(self, action: #selector(chooseControl(control:)), for: UIControlEvents.touchUpInside)
    canGuessControl.addTarget(self, action: #selector(chooseControl(control:)), for: UIControlEvents.touchUpInside)
  }
  
  func chooseControl(control: ChatRoomDintingView) -> Void {
    control.isChoose = !control.isChoose
  }
  
  func systemRequest() -> Void {
    messageDispatchManager.sendMessage(type: GameMessageType.uc_get_words, payLoad: ["type": "system"])
  }
  
  func userRequest() -> Void {
    messageDispatchManager.sendMessage(type: GameMessageType.uc_get_words, payLoad: ["type": "custom"])
  }
  
  func cancelAction() -> Void {
    Utils.hideAnimated()
  }
  
  func sureAction() -> Void {
    guard let key1 = textField1.text, !key1.isEmpty else {
      XBHHUD.showError("词语不能为空".localized)
      return
    }
    guard let key2 = textField2.text, !key2.isEmpty else {
      XBHHUD.showError("词语不能为空".localized)
      return
    }
    messageDispatchManager.sendMessage(type: GameMessageType.uc_stop_config)
    messageDispatchManager.sendMessage(type: GameMessageType.uc_update_config,
                                       payLoad: ["has_ghost":setGhostControl.isChoose,
                                                 "can_cut_speaker":insertMicControl.isChoose,
                                                 "can_guess":canGuessControl.isChoose,
                                                 "first":key1,
                                                 "second":key2,
                                                 "more_undercover":addDintingControl.isChoose,])
    CurrentUser.shareInstance.key1 = key1
    CurrentUser.shareInstance.key2 = key2
    Utils.hideNoticeView()
    XBHHUD.showTextOnly(text: "设置完成".localized)
    CurrentUser.shareInstance.isGameing = .isUpdateConfig
  }
  
  func setLabelAttribution(label: UILabel, alignment: NSTextAlignment, font: CGFloat) -> Void {
    label.textAlignment = alignment
    label.font = UIFont.boldSystemFont(ofSize: font)
  }
  
  func setTextFieldAttributon(textField: UITextField) -> Void {
    textField.textAlignment = .center
    textField.font = UIFont.systemFont(ofSize: 14)
    textField.textColor = UIColor.textPurpleLightColor()
    textField.layer.cornerRadius = 5
    textField.layer.borderWidth = 0.5
  }
  
  func setButtonAttribution() -> Void {
    systemWordButton.layer.cornerRadius = 5
    systemWordButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
    systemWordButton.layer.masksToBounds = true
    userWoldButton.layer.cornerRadius = 5
    userWoldButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
    userWoldButton.layer.masksToBounds = true
    cancelButton.layer.cornerRadius = 5
    cancelButton.layer.masksToBounds = true
    sureButton.layer.cornerRadius = 5
    sureButton.layer.masksToBounds = true
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    titleLabel.frame = CGRect(x: 0, y: 15, width: width, height: 20)
    subTitleLabel.frame = CGRect(x: 0, y: titleLabel.maxY + 15, width: width, height: 20)
    desLabel.frame = CGRect(x: 0, y: subTitleLabel.maxY + 5, width: width, height: 16)
    textField1.frame = CGRect(x: 35, y: desLabel.maxY + 20, width: width - 70, height: 30)
    textField2.frame = CGRect(x: 35, y: textField1.maxY + 8, width: width - 70, height: 30)
    systemWordButton.frame = CGRect(x: width / 2 - 20 - 85, y: textField2.maxY + 15, width: 85, height: 28)
    userWoldButton.frame = CGRect(x: width / 2 + 20, y: textField2.maxY + 15, width: 85, height: 28)
    addDintingControl.frame = CGRect(x: 30, y: systemWordButton.maxY + 15, width: width - 60, height: 30)
    setGhostControl.frame = CGRect(x: 30, y: addDintingControl.maxY + 8, width: width - 60, height: 30)
    insertMicControl.frame = CGRect(x: 30, y: setGhostControl.maxY + 8, width: width - 60, height: 30)
    canGuessControl.frame = CGRect(x: 30, y: insertMicControl.maxY + 8, width: width - 60, height: 30)
    cancelButton.frame = CGRect(x: 30, y: canGuessControl.maxY + 20, width: width / 2 - 45, height: 38)
    sureButton.frame = CGRect(x: cancelButton.maxX + 30, y: canGuessControl.maxY + 20, width: width / 2 - 45, height: 38)
  }
  
  func chatRoomColor() -> Void {
    titleLabel.textColor = UIColor.textPurpleDarkColor()
    subTitleLabel.textColor = UIColor.textPurpleDarkColor()
    desLabel.textColor = UIColor.textPurpleLightColor()
    textField1.layer.borderColor = UIColor.textPurpleDarkColor().cgColor
    textField2.layer.borderColor = UIColor.textPurpleDarkColor().cgColor
    userWoldButton.backgroundColor = UIColor.white
    userWoldButton.setTitleColor(UIColor.textPurpleLightColor(), for: .normal)
    systemWordButton.backgroundColor = UIColor.oragenBack()
    systemWordButton.setTitleColor(.white, for: .normal)
    cancelButton.setTitleColor(UIColor.textPurpleLightColor(), for: .normal)
    sureButton.setTitleColor(UIColor.white, for: .normal)
    cancelButton.backgroundColor = UIColor.white
    sureButton.backgroundColor = UIColor.textPurpleLightColor()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}


class ChatRoomDintingView: UIControl {
  
  var isChoose = false {
    didSet {
      imageView.image = isChoose ? isChooseImage : unChooseImage
    }
  }
  convenience init(_ title: String) {
    self.init(frame: .zero)
    titleLabel.text = title
  }
  
  let isChooseImage = UIImage.init(named: "chatRoom_switch_open")
  let unChooseImage = UIImage.init(named: "chatRoom_switch_close")
  let titleLabel = UILabel()
  let isYESNO = UILabel()
  let imageView = UIImageView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    titleLabel.textColor = UIColor.textPurpleDarkColor()
    isYESNO.textColor = UIColor.textPurpleDarkColor()
    isYESNO.text = "否            是".localized
    imageView.image = unChooseImage
    titleLabel.font = UIFont.systemFont(ofSize: 14)
    isYESNO.font = UIFont.systemFont(ofSize: 14)
    isYESNO.textAlignment = .right
    addSubview(titleLabel)
    addSubview(isYESNO)
    addSubview(imageView)
    imageView.contentMode = .scaleAspectFit
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    let yesNoWidth: CGFloat = 80
    titleLabel.frame = CGRect.init(x: 0, y: 0, width: self.width - yesNoWidth, height: self.height)
    isYESNO.frame = CGRect.init(x: self.width - yesNoWidth, y: 0, width: yesNoWidth, height: self.height)
    imageView.frame = CGRect.init(x: self.width - yesNoWidth + 23, y: 5, width: yesNoWidth - 40, height: 20)
  }
}

extension ChatRoomDintingSetting: UITextFieldDelegate {
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if self.isInputRuleAndBlank(str: string) || string == "" {
      let enc = CFStringConvertEncodingToNSStringEncoding(UInt32(CFStringEncodings.GB_18030_2000.rawValue))
      let dataTmp = textField.text?.data(using: String.Encoding(rawValue: enc))
      let dataNow = string.data(using: String.Encoding(rawValue: enc))
      if let tmpCount = dataTmp?.count, let nowCount = dataNow?.count {
        return tmpCount + nowCount <= 14
      }
      return true
    }
    return false
  }
  
  func isInputRuleAndBlank(str:String)->Bool{
    let pattern = "^[a-zA-Z\\u4E00-\\u9FA5\\d\\s]*$"
    let pred = NSPredicate(format: "SELF MATCHES %@", pattern)
    let isMatch = pred.evaluate(with: str)
    return isMatch
  }
  
}

