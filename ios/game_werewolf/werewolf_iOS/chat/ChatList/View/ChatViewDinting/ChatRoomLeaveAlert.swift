//
//  ChatRoomLeaveAlert.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/12/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

struct ChatActionStruct {
  var image: UIImage?
  var title: String?
  var tag = 0
}

class ChatRoomLeaveAlert: BaseNoticeView {

  var selectBlock: ((Int) -> Void)?
  var titleLabel = UILabel()
  var icons: [ChatActionStruct]?
  let disposeBag = DisposeBag()
  let itemTopSpace: CGFloat = 60
  let itemHeight: CGFloat = 90
  
  
  convenience init(height: CGFloat, title: String, icons: [ChatActionStruct]) {
    self.init()
    self.width = Screen.width * 0.85
    self.height = height
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.layer.cornerRadius = 6
    addSubview(titleLabel)
    titleLabel.frame = CGRect(x: 0, y: 0, width: self.width, height: 60)
    titleLabel.text = title
    titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
    titleLabel.textAlignment = .center
    titleLabel.textColor = UIColor.init(hexInt: 0x8460E3)
    self.icons = icons
    setiCons()
    self.backgroundColor = UIColor.init(hexInt: 0xe3dce9)
    let cancelButton = UIButton()
    cancelButton.setTitle("取 消".localized, for: UIControlState.normal)
    cancelButton.backgroundColor = UIColor.white
    cancelButton.setTitleColor(UIColor.init(hexInt: 0x8460E3), for: UIControlState.normal)
    cancelButton.layer.cornerRadius = 5
    cancelButton.layer.masksToBounds = true
    cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
    cancelButton.frame = CGRect.init(x: 30, y: height - 38 - 22, width: self.width - 60, height: 38)
    addSubview(cancelButton)
    cancelButton.rx.tap.subscribe(onNext: {[weak self] (_) in
      Utils.hideAnimated()
      self?.selectBlock?(1000)
    }).disposed(by: disposeBag)
  }
  
  func setiCons() -> Void {
    if let icons = icons, !icons.isEmpty {
      let itemWidth = (Screen.width * 0.85 - 60) / CGFloat(icons.count)
      for i in 0..<icons.count {
        let actionView = ChatActionView.init(frame: CGRect.init(x: 30 + CGFloat(i) * itemWidth, y: itemTopSpace, width: itemWidth, height: itemHeight))
        let info = icons[i]
        actionView.imageView.image = info.image
        actionView.titleLabel.text = info.title
        actionView.rx.controlEvent(UIControlEvents.touchUpInside).subscribe(onNext: { [weak self] (_) in
          self?.selectBlock?(i)
        }).disposed(by: disposeBag)
        addSubview(actionView)
      }
    }
  }
}

class ChatActionView: UIControl {
  
  let imageView = UIImageView.init()
  let titleLabel = UILabel()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    imageView.contentMode = .scaleAspectFill
    imageView.layer.masksToBounds = true
    imageView.layer.cornerRadius = 33
    titleLabel.font = UIFont.systemFont(ofSize: 13)
    titleLabel.textColor = UIColor.init(hexInt: 0x8460E3)
    titleLabel.textAlignment = .center
    addSubview(imageView)
    addSubview(titleLabel)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    imageView.frame = CGRect.init(x: self.width / 2 - 33, y: 0, width: 66, height: 66)
    titleLabel.frame = CGRect.init(x: 0, y: 70, width: self.width, height: 20)
  }
}












