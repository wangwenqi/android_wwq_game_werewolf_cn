//
//  ChatDintingAlert.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/12/4.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatDintingAlert: BaseNoticeView {
  
  var timeInterValue = 0
  var titleString = ""
  let timeLabel = UILabel()
  fileprivate let titleLabel = UILabel()
  let subTitleLabel = UILabel()
  var cTime: Timer?
  
  override func initial() {
    self.width = Screen.width * 0.85
    self.height = 180
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.layer.cornerRadius = 6
  }
  
  convenience init(frame: CGRect, time: Int, title: String, subTitle: String) {
    self.init(frame: .zero)
    cTime = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timeChange), userInfo: nil, repeats: true)
    timeInterValue = time / 1000
    titleLabel.text = title
    subTitleLabel.text = subTitle
    cTime?.fire()
  }

  override init(frame: CGRect) {
    super.init(frame: .zero)
    self.backgroundColor = UIColor.noticeBackGroundColor()
    addSubview(titleLabel)
    addSubview(subTitleLabel)
    addSubview(timeLabel)
    titleLabel.textAlignment = .center
    titleLabel.font = UIFont.systemFont(ofSize: 15)
    titleLabel.textColor = UIColor.textPurpleDarkColor()
    timeLabel.textAlignment = .center
    timeLabel.textColor = UIColor.textPurpleDarkColor()
    timeLabel.font = UIFont.systemFont(ofSize: 11)
    subTitleLabel.numberOfLines = 0
    subTitleLabel.textColor = UIColor.textPurpleDarkColor()
    subTitleLabel.font = UIFont.systemFont(ofSize: 14)
    subTitleLabel.textAlignment = .center
  }
  
  func timeChange() -> Void {
    if timeInterValue == 0 {
      cTime?.invalidate()
      Utils.hideAnimated()
    }
    timeLabel.text = "(" + "\(timeInterValue)s" + ")"
    timeInterValue -= 1
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    titleLabel.frame = CGRect.init(x: 0, y: 12, width: width, height: 30)
    subTitleLabel.frame = CGRect.init(x: 60, y: titleLabel.maxY + 20, width: width - 120, height: 50)
    timeLabel.frame = CGRect(x: width - 60, y: 12, width: 50, height: 30)
  }
  
  deinit {
    cTime?.invalidate()
  }
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
