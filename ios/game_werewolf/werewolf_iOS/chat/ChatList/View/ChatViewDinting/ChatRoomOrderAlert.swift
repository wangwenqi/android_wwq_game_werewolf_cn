//
//  ChatRoomOrderAlert.swift
//  xiaoyu
//
//  Created by xiaozao on 2018/1/22.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ChatRoomOrderAlert: BaseNoticeView {
  
  var selectBlock: ((Int) -> Void)?
  var icons: [ChatActionStruct]?
  let disposeBag = DisposeBag()
  let itemTopSpace: CGFloat = 22
  let itemHeight: CGFloat = 75
  
  
  convenience init(height: CGFloat, title: String, icons: [ChatActionStruct]) {
    self.init()
    self.width = Screen.width * 0.85
    self.height = height
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.layer.cornerRadius = 6
    self.icons = icons
    setiCons()
    self.backgroundColor = UIColor.init(hexInt: 0xe3dce9)
    let cancelButton = UIButton()
    cancelButton.setTitle("取 消".localized, for: UIControlState.normal)
    cancelButton.backgroundColor = UIColor.white
    cancelButton.setTitleColor(UIColor.init(hexInt: 0x8460E3), for: UIControlState.normal)
    cancelButton.layer.cornerRadius = 5
    cancelButton.layer.masksToBounds = true
    cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
    cancelButton.frame = CGRect.init(x: 30, y: height - 38 - 22, width: self.width - 60, height: 38)
    addSubview(cancelButton)
    cancelButton.rx.tap.subscribe(onNext: { (_) in
      Utils.hideAnimated()
    }).disposed(by: disposeBag)
  }
  
  func setiCons() -> Void {
    if let icons = icons, !icons.isEmpty {
      let itemWidth = (Screen.width * 0.85 - 40) / CGFloat(icons.count)
      for i in 0..<icons.count {
        let actionView = ChatActionViewOrder.init(frame: CGRect.init(x: 20 + CGFloat(i) * itemWidth, y: itemTopSpace, width: itemWidth, height: itemHeight))
        let info = icons[i]
        actionView.imageView.image = info.image
        actionView.titleLabel.text = info.title
        actionView.rx.controlEvent(UIControlEvents.touchUpInside).subscribe(onNext: { [weak self] (_) in
          self?.selectBlock?(i)
        }).disposed(by: disposeBag)
        addSubview(actionView)
      }
    }
  }
}

class ChatActionViewOrder: UIControl {
  
  let imageView = UIImageView.init()
  let titleLabel = UILabel()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    imageView.contentMode = .scaleAspectFill
    imageView.layer.masksToBounds = true
    imageView.layer.cornerRadius = 25
    titleLabel.font = UIFont.systemFont(ofSize: 13)
    titleLabel.textColor = UIColor.init(hexInt: 0x8460E3)
    titleLabel.textAlignment = .center
    addSubview(imageView)
    addSubview(titleLabel)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    imageView.frame = CGRect.init(x: self.width / 2 - 25, y: 0, width: 50, height: 50)
    titleLabel.frame = CGRect.init(x: 0, y: 55, width: self.width, height: 20)
  }
}
