//
//  ChatDintingGuessWord.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/12/1.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatDintingGuessWord: BaseNoticeView {

  var showTime: Int = 0
  var showTimer: Timer?
  let timeLabel = UILabel()
  let titleLabel = UILabel()
  let subTitleLabel = UILabel()
  let keyWorld = UITextField()
  let sureButton = UIButton(type: .system)
  
  override func initial() {
    self.width = Screen.width * 0.85
    self.height = 200
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.layer.cornerRadius = 6
  }
  
  convenience init(frame: CGRect, time: Int) {
    self.init(frame: frame)
    showTime = time
    showTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timeOut), userInfo: nil, repeats: true)
    showTimer?.fire()
  }
  
  func timeOut() -> Void {
    if showTime == 0 {
      Utils.hideAnimated()
      showTime -= 1
      return
    }
    showTime -= 1
    timeLabel.text = "(" + "\(showTime)s" + ")"
  }

  override init(frame: CGRect) {
    super.init(frame: .zero)
    self.backgroundColor = UIColor.noticeBackGroundColor()
    addSubview(titleLabel)
    addSubview(subTitleLabel)
    addSubview(keyWorld)
    addSubview(sureButton)
    addSubview(timeLabel)
    titleLabel.textColor = UIColor.textPurpleDarkColor()
    titleLabel.text = "卧底爆词".localized
    titleLabel.textAlignment = .center
    titleLabel.font = UIFont.systemFont(ofSize: 16)
    timeLabel.textAlignment = .center
    timeLabel.font = UIFont.systemFont(ofSize: 11)
    timeLabel.textColor = UIColor.textPurpleDarkColor()
    subTitleLabel.textColor = UIColor.textPurpleDarkColor()
    subTitleLabel.text = "猜猜好人的词是什么".localized
    subTitleLabel.font = UIFont.systemFont(ofSize: 15)
    subTitleLabel.textAlignment = .center
    keyWorld.textColor = UIColor.textPurpleLightColor()
    keyWorld.layer.cornerRadius = 5
    keyWorld.textAlignment = .center
    keyWorld.layer.borderColor = UIColor.textPurpleLightColor().cgColor
    keyWorld.placeholder = "请输入你猜测的好人词".localized
    keyWorld.font = UIFont.systemFont(ofSize: 14)
    keyWorld.layer.borderWidth = 0.5
    keyWorld.layer.masksToBounds = true
    sureButton.layer.cornerRadius = 5
    sureButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
    sureButton.backgroundColor = UIColor.textPurpleLightColor()
    sureButton.setTitleColor(UIColor.white, for: .normal)
    sureButton.setTitle("确定".localized, for: .normal)
    sureButton.addTarget(self, action: #selector(guessWords), for: .touchUpInside)
  }
  
  func guessWords() -> Void {
    // 猜词
    messageDispatchManager.sendMessage(type: GameMessageType.uc_guess_word, payLoad: ["word": keyWorld.text ?? ""])
    Utils.hideAnimated()
    showTimer?.fireDate = Date.distantFuture
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    titleLabel.frame = CGRect(x: 0, y: 12, width: width, height: 30)
    subTitleLabel.frame = CGRect(x: 0, y: titleLabel.maxY + 20, width: width, height: 20)
    keyWorld.frame = CGRect(x: 30, y: subTitleLabel.maxY + 15, width: width - 60, height: 35)
    sureButton.frame = CGRect(x: width / 2 - 50, y: keyWorld.maxY + 15, width: 100, height: 35)
    timeLabel.frame = CGRect(x: width - 60, y: 12, width: 50, height: 30)
  }
  
}











