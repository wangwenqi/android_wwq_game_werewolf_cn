//
//  ChatViewReadyView.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatViewReadyView: UIView {
  
  struct GameSetting {
    let word1 = ""            // 好人词
    let wold2 = ""            // 卧底词
    let addDinting = false    // 是否增加一个卧底
    let setGhost = false      // 是否设置卧底为白板
    let insertMic = false     // 是否可以插麦
    let canGuess = false      // 卧底是否可以猜词
  }
  
  var gameInfoBlock: ((GameSetting) -> Void)?
  lazy var setButton: UIButton = {
    let button = UIButton(type: .system)
    button.addTarget(self, action: #selector(setButtonAction), for: .touchUpInside)
    button.layer.masksToBounds = true
    button.layer.cornerRadius = 12.5
    button.frame = CGRect.init(x: 0, y: 0, width: 65, height: 25)
    button.shadowTopDown(colors: [UIColor(hexInt: 0xffff00), UIColor(hexInt: 0xffcd00), UIColor(hexInt: 0xffa000)], locations: [0, 0.5, 1])
    button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
    button.setTitle("设置".localized, for: .normal)
    return button
  }()
  
  let startButton: UIButton = {
    let button = UIButton(type: .system)
    button.addTarget(self, action: #selector(startGames), for: .touchUpInside)
    button.layer.masksToBounds = true
    button.layer.cornerRadius = 12.5
    button.frame = CGRect.init(x: 70, y: 0, width: 65, height: 25)
    button.shadowTopDown(colors: [UIColor(hexInt: 0xff33ff), UIColor(hexInt: 0xff35c9), UIColor(hexInt: 0xff378c)], locations: [0, 0.5, 1])
    button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
    button.setTitle("开始游戏".localized, for: .normal)
    return button
  }()
  var isGameing = false
  lazy var dintingSetView: ChatRoomDintingSetting = {
     ChatRoomDintingSetting.init(frame: .zero)
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    addSubview(setButton)
    addSubview(startButton)
  }
  
  func setButtonAction() -> Void {
    messageDispatchManager.sendMessage(type: GameMessageType.uc_start_config)
    Utils.showNoticeView(view: dintingSetView)
  }
  
  func startGames() -> Void {
    if CurrentUser.shareInstance.isGameing != .isUpdateConfig {
      XBHHUD.showError("请先设置卧底词语".localized)
      return
    }
    messageDispatchManager.sendMessage(type: GameMessageType.start)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
