//
//  ChatRoomOpenMic.swift
//  xiaoyu
//
//  Created by xiaozao on 2018/1/10.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ChatRoomOpenMic: BaseNoticeView {

  var selectBlock: ((Bool) -> Void)?
  var isShow = false {
    didSet {
      if isShow == false {
        timerDevice?.fireDate = NSDate.distantFuture
      }
    }
  }
  
  override func initial() {
    self.width = Screen.width * 0.85
    self.height = 173
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
    self.layer.cornerRadius = 6
    self.backgroundColor = UIColor.noticeBackGroundColor()
  }
  
  var timerDevice:Timer?
  var timeCount = 20
  let infoLabel = UILabel()
  let refuse = UIButton.init(type: UIButtonType.system)
  let agreeOpen = UIButton.init(type: UIButtonType.system)
  let disposeBag = DisposeBag()
  
  
  func toSetTimer(_ time: Int) -> Void {
    isShow = true
    timeCount = time
    timerDevice?.fireDate = Date()
  }
  
  override init(frame: CGRect) {
    super.init(frame: .zero)
    addSubview(infoLabel)
    infoLabel.textAlignment = .center
    infoLabel.textColor = UIColor.textPurpleDarkColor()
    addSubview(refuse)
    addSubview(agreeOpen)
    self.setAttributeImageTitle("拒 绝".localized, "chatRoom_wrong", refuse)
    refuse.backgroundColor = UIColor.purpleBackColor()
    self.setAttributeImageTitle("开 启".localized, "chatRoom_right", agreeOpen)
    agreeOpen.backgroundColor = UIColor.oragenBack()
    infoLabel.text = "房主即将开启你的自由模式".localized
    timerDevice = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerChange), userInfo: nil, repeats: true)
    timerDevice?.fireDate = NSDate.distantFuture
    refuse.rx.tap.subscribe(onNext: { [weak self] (_) in
      self?.selectBlock?(false)
      self?.isShow = false
      Utils.hideAnimated()
    }).disposed(by: disposeBag)
    
    agreeOpen.rx.tap.subscribe(onNext: { [weak self] (_) in
      self?.selectBlock?(true)
      self?.isShow = false
      Utils.hideAnimated()
    }).disposed(by: disposeBag)
  }
  
  func setAttributeImageTitle(_ title: String, _ imageName: String, _ button: UIButton) -> Void {
    let attch = NSTextAttachment.init()
    attch.image = UIImage.init(named: imageName)
    attch.bounds = CGRect.init(x: 0, y: -2, width: 15, height: 15)
    let icoAttribute = NSAttributedString.init(attachment: attch)
    let tmpStr = NSMutableAttributedString.init(attributedString: icoAttribute)
    let sendGiftStr = NSAttributedString.init(string: " " + title, attributes: [NSForegroundColorAttributeName: UIColor.white])
    button.layer.cornerRadius = 5
    button.layer.masksToBounds = true
    tmpStr.append(sendGiftStr)
    button.setAttributedTitle(tmpStr, for: UIControlState.normal)
  }
  
  override func removeFromSuperview() {
    super.removeFromSuperview()
    isShow = false
  }
  
  func timerChange() -> Void {
    if timeCount <= 1 {
      timerDevice?.fireDate = NSDate.distantFuture
      Utils.hideAnimated()
    }
    timeCount -= 1
    changeTimeLabel(timeCount)
  }
  
  func changeTimeLabel(_ count: Int) -> Void {
    infoLabel.text = "房主即将开启你的自由模式".localized + "(" + "\(count)" + "s" + ")"
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    infoLabel.frame = CGRect.init(x: 0, y: 0, width: width, height: 120)
    refuse.frame = CGRect.init(x: 30, y: infoLabel.maxY, width: (width - 80) / 2, height: 38)
    agreeOpen.frame = CGRect.init(x: refuse.width + 50, y: infoLabel.maxY, width: refuse.width, height: refuse.height)
  }
  
  deinit {
    timerDevice?.invalidate()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  

}
