//
//  ChatRoomSpeakView.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomSpeakView: UIView {

  var progress: Double = 0 
  
  override func draw(_ rect: CGRect) {
    let ctx = UIGraphicsGetCurrentContext()
    let startA = -Double.pi / 2
    let endA = -Double.pi / 2 + (Double.pi / 2) * progress
    let path = UIBezierPath.init(arcCenter: self.center, radius: width / 2, startAngle: CGFloat(startA), endAngle: CGFloat(endA), clockwise: true)
    ctx?.setLineWidth(10)
    UIColor.blue.setStroke()
    ctx?.addPath(path.cgPath)
    ctx?.strokePath()
  }
  
}
