//
//  ChatRoomApplyCert.swift
//  game_werewolf
//
//  Created by xiaozao on 2018/1/22.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ChatRoomApplyCert: BaseNoticeView {

  var buyAction: ((Bool) -> Void)?
  let disposeBag = DisposeBag()
  let titleLabel = UILabel()
  let infoLabel = UILabel()
  let moneyLabel = UILabel()
  let moreinfoLabel = UILabel()
  let cancelButton = UIButton(type: .system)
  let buyButton = UIButton(type: .system)
  
  let chooseLabel = UILabel()
  let chooseButton = UserNameAndIcon()
  var model: ChatRoomUserModel?
  
  func setInfoMessage(_ roomid: String, _ type: String, _ isExchange: Bool = false) -> NSMutableAttributedString {
    let paraph = NSMutableParagraphStyle()
    paraph.lineSpacing = 3
    let strTmp = isExchange ? "房间的房契之后, 新房主将拥有此房间的所有权".localized : "房间的房契之后, 房间的归属权为房契所有者".localized
    let str1 = NSMutableAttributedString.init(attributedString:NSAttributedString.init(string: "    " + type + roomid + strTmp, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15), NSForegroundColorAttributeName:  UIColor.textPurpleDarkColor(), NSParagraphStyleAttributeName: paraph]))
    let str2 = NSAttributedString.init(string: "(房间无人挂房的24小时内房间不会消失)".localized, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15), NSForegroundColorAttributeName: UIColor.red, NSParagraphStyleAttributeName: paraph])
    let str3 = NSAttributedString.init(string: "房主被其他人抢走的时候, 可以进入房间拿回房主, 房主可以转让房契, 转让房契后房间的归属权为新的房契所有者所有.".localized, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15), NSForegroundColorAttributeName:  UIColor.textPurpleDarkColor(), NSParagraphStyleAttributeName: paraph])
    str1.append(str2)
    str1.append(str3)
    return str1
  }
  
  func moneyText(_ money: String) -> NSMutableAttributedString {
    let attch = NSTextAttachment.init()
    attch.image = UIImage.init(named: "chatRoom_purdamond")
    attch.bounds = CGRect.init(x: 0, y: -4, width: 22, height: 19)
    let icoAttribute = NSAttributedString.init(attachment: attch)
    let sendGiftStr = NSAttributedString.init(string: "花费" + " " + money + "钻石".localized + " ", attributes: [NSForegroundColorAttributeName: UIColor.red])
    let tmpStr = NSMutableAttributedString.init(attributedString: sendGiftStr)
    tmpStr.append(icoAttribute)
    return tmpStr
  }
  
  convenience init(height: CGFloat, title: String, money: Int, roomid: String, model: ChatRoomUserModel?) {
    self.init(height: height, title: title, money: money, roomid: roomid)
    addSubview(chooseLabel)
    addSubview(chooseButton)
    buyButton.setTitle("转 让".localized, for: UIControlState.normal)
    self.model = model ?? ChatRoomUserModel()
    chooseLabel.textColor = UIColor.textPurpleDarkColor()
    chooseLabel.textAlignment = .center
    chooseLabel.font = UIFont.systemFont(ofSize: 12)
    chooseLabel.text = "选择移交房契者".localized
    infoLabel.attributedText = setInfoMessage(roomid, title.slicing(from: 0, length: 2)!, true)
    if let model = model {
      if let url = URL.init(string: model.avatar) {
        chooseButton.imageView.sd_setImage(with: url)
        chooseButton.name.text = model.name
        chooseButton.idLabel.text = " ID:" + model.uid
      }
    }
    chooseButton.addTarget(self, action: #selector(choosePerson), for: .touchUpInside)
  }
  
  func choosePerson() -> Void {
    self.buyAction?(false)
  }
  
  convenience init(height: CGFloat, title: String, money: Int, roomid: String) {
    self.init()
    buyButton.setTitle("购 买".localized, for: UIControlState.normal)
    self.width = Screen.width * 0.85
    self.height = height
    self.layer.cornerRadius = 6
    addSubview(titleLabel)
    addSubview(infoLabel)
    addSubview(moneyLabel)
    addSubview(moreinfoLabel)
    addSubview(cancelButton)
    addSubview(buyButton)
    infoLabel.attributedText = setInfoMessage(roomid, title.slicing(from: 0, length: 2)!, false)
    moneyLabel.attributedText = moneyText("\(money)")
    moneyLabel.textAlignment = .center
    infoLabel.numberOfLines = 0
    titleLabel.text = title
    titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
    titleLabel.textAlignment = .center
    titleLabel.textColor = UIColor.textPurpleDarkColor()
    self.backgroundColor = UIColor.init(hexInt: 0xe3dce9)
    
    cancelButton.setTitle("取 消".localized, for: UIControlState.normal)
    cancelButton.backgroundColor = UIColor.white
    cancelButton.setTitleColor(UIColor.textPurpleLightColor(), for: UIControlState.normal)
    cancelButton.layer.cornerRadius = 5
    cancelButton.layer.masksToBounds = true
    buyButton.backgroundColor = UIColor.purpleBackColor()
    buyButton.setTitleColor(UIColor.white, for: UIControlState.normal)
    buyButton.layer.cornerRadius = 5
    buyButton.layer.masksToBounds = true
    moreinfoLabel.text = "备注: 可移交房契者为当前房间的在线玩家".localized
    moreinfoLabel.font = UIFont.systemFont(ofSize: 12)
    moreinfoLabel.textColor = UIColor.textPurpleLightColor()

    cancelButton.rx.tap.subscribe(onNext: { (_) in
      Utils.hideAnimated()
    }).disposed(by: disposeBag)
    buyButton.rx.tap.subscribe(onNext: { [weak self] (_) in
      self?.buyAction?(true)
    }).disposed(by: disposeBag)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    titleLabel.frame = CGRect(x: 0, y: 0, width: self.width, height: 70)
    infoLabel.frame = CGRect(x: 20, y: titleLabel.maxY, width: self.width - 40, height: 200)
    infoLabel.sizeToFit()
    moneyLabel.frame = CGRect(x: 0, y: infoLabel.maxY, width: self.width, height: 60)
    if let _ = model {
      chooseLabel.frame = CGRect.init(x: 3, y: moneyLabel.maxY + 5, width: 90, height: 33)
      chooseButton.frame = CGRect.init(x: chooseLabel.maxX, y: moneyLabel.maxY, width: self.width - 20 - chooseLabel.frame.width, height: 38)
      cancelButton.frame = CGRect(x: 30, y: chooseButton.maxY + 5, width: self.width / 2 - 40, height: 38)
      buyButton.frame = CGRect(x: self.width / 2 + 10, y: chooseButton.maxY + 5, width: self.width / 2 - 40, height: 38)
    } else {
      cancelButton.frame = CGRect(x: 30, y: moneyLabel.maxY, width: self.width / 2 - 40, height: 38)
      buyButton.frame = CGRect(x: self.width / 2 + 10, y: moneyLabel.maxY, width: self.width / 2 - 40, height: 38)
    }
    moreinfoLabel.frame = CGRect(x: 20, y: cancelButton.maxY, width: self.width, height: 50)
    self.height = moreinfoLabel.maxY
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
  }
  
}


class UserNameAndIcon: UIControl {
  
  let imageView = UIImageView()
  let name = UILabel()
  let idLabel = UILabel()
  let righticon = UIImageView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(imageView)
    imageView.image = UIImage.init(named: "room_head_default")
    addSubview(name)
    addSubview(idLabel)
    addSubview(righticon)
    name.text = "请选择要移交的人".localized
    name.textColor = UIColor.textPurpleLightColor()
    name.font = UIFont.systemFont(ofSize: 12)
    idLabel.textColor = UIColor.textPurpleLightColor()
    idLabel.font = UIFont.systemFont(ofSize: 12)
    idLabel.text = ""
    righticon.image = UIImage.init(named: "chatRoom_right_addFrend")
    righticon.contentMode = .scaleAspectFit
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    imageView.frame = CGRect(x: 3, y: 5, width: self.height - 10, height: self.height - 10)
    imageView.layer.cornerRadius = imageView.frame.width / 2
    imageView.layer.masksToBounds = true
    if idLabel.text?.isEmpty == true {
      name.frame = CGRect(x: imageView.maxX + 5, y: 0, width: width - 50, height: self.height)
    } else {
      name.frame = CGRect(x: imageView.maxX + 5, y: 0, width: width - 50 - 80, height: self.height)
      idLabel.frame = CGRect(x: name.frame.maxX , y: 0, width: 80, height: self.height)
    }
    righticon.frame = CGRect(x: self.width - 10, y: self.height / 2 - 6, width: 12, height: 12)
  }
}
















