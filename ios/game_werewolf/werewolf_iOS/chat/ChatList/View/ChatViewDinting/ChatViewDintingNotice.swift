//
//  ChatViewDintingNotice.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/28.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatViewDintingNotice: UIView {
  
  var showTimer: Timer?
  var showTime = 0
  let timeLabel = UILabel()
  let helperLabel = UILabel()
  let clickImageView = UIImageView()
  func setDuring(_ duration: Int?) -> Void {
    showTime = (duration ?? 0) / 1000
    showTimer?.fireDate = Date.init(timeIntervalSinceNow: 0)
    clickImageView.isHidden = false
  }
  
  enum NoticeTitleType: String {
    case desState
    case voteState
    case interState
    case gameOver
    case unKnown
    
    func text() -> String {
      switch self {
      case .desState:
        return "描述阶段".localized
      case .voteState:
        return "投票阶段".localized
      case .interState:
        return "过度阶段".localized
      case .gameOver:
        return "游戏结束".localized
      default:
        return "谁是卧底".localized
      }
    }
  }
  
  enum UserStatus {
    case master // 房主
    case player // 1-8玩家
    case watcher // 观众
  }
  
  func setHelpWorld(text: String) -> Void {
    helperLabel.text = text
  }
  
  func setUserType(type: UserStatus) -> Void {        // 成为玩家/房主/下麦时候调用
    if type == .master {
      userType = type
    }
    guard userType == type else {
      userType = type
      return
    }
  }
  
  func setNoticeType(type: NoticeTitleType, text: String) -> Void {  // 游戏进行不同阶段切换
    switch userType {
    case .master:
      return
    case .player:
      if type == .desState {
        keyWord1.text = text; desInfo.text = "你的词".localized
      } else {
        desInfo.text = text
      }
    case .watcher:
      if type == .gameOver {
        desInfo.text = text
      } else {
        desInfo.text = "游戏正在进行".localized
      }
    }
    guard titleType == type else {
      titleType = type
      self.setNeedsLayout()
      return
    }
  }
  
  var titleType: NoticeTitleType = .unKnown {
    didSet {
      titleLabel.text = titleType.text()
      switch titleType {
      case .desState:
        keyWord1.isHidden = false
      case .interState:  // 过度阶段
        keyWord1.isHidden = true
      case .voteState:
        if CurrentUser.shareInstance.currentPosition != 0 {
          keyWord1.isHidden = true
        }
      case .gameOver:
        keyWord1.isHidden = true
      default:
        keyWord1.isHidden = true
      }
    }
  }
  
  var userType: UserStatus = .watcher {
    didSet {
      switch userType {
      case .master:
        setNoticeType(type: .unKnown, text: "")
        keyWord1.text = CurrentUser.shareInstance.key1
        keyWord2.text = CurrentUser.shareInstance.key2
        desInfo.text = "词汇一".localized
        desInfo2.text = "词汇二".localized
        desInfo.textColor = UIColor.pinkColor()
        desInfo.font = UIFont.boldSystemFont(ofSize: 11)
        desInfo2.isHidden = false
        keyWord2.isHidden = false
        keyWord1.isHidden = false
        timeLabel.isHidden = false
        helperLabel.isHidden = false
        clickImageView.isHidden = true
        desInfo.font = UIFont.boldSystemFont(ofSize: 11)
        desInfo2.font = UIFont.boldSystemFont(ofSize: 11)
        self.setNeedsLayout() // 布局为卧底形式
      case .player:
        desInfo.font = UIFont.systemFont(ofSize: 11)
        desInfo.textColor = UIColor.textPurpleDarkColor()
        desInfo2.isHidden = true
        keyWord2.isHidden = true
        timeLabel.isHidden = false
        helperLabel.isHidden = false
        clickImageView.isHidden = false
        self.setNeedsLayout() // 布局为玩家视角
      default:
        desInfo.font = UIFont.systemFont(ofSize: 11)
        desInfo.textColor = UIColor.textPurpleDarkColor()
        desInfo2.isHidden = true
        keyWord2.isHidden = true
        timeLabel.isHidden = false
        helperLabel.isHidden = false
        clickImageView.isHidden = false
        self.setNeedsLayout() // 布局为观众视角
      }
    }
  }
  
  let titleLabel = UILabel()        // 标题
  private let whiteBack = UIView()  // 白色背景
  private let purpleBack = UIView() // 紫色背景
  var desInfo = UILabel()           // 卧底正在暴词等提示文字
  var desInfo2 = UILabel()
  var keyWord1 = UILabel()
  var keyWord2 = UILabel()
  // 125 + 5
  override func layoutSubviews() {
    super.layoutSubviews()
    titleLabel.frame = CGRect.init(x: 30, y: 0, width: 80, height: 20)
    if userType == .master  {
      desInfo.frame = CGRect.init(x: 0, y: 25, width: 40, height: 20)
      keyWord1.frame = CGRect.init(x: 42, y: 25, width: 90, height: 20)
      desInfo2.frame = CGRect.init(x: 0, y: desInfo.maxY + 8, width: desInfo.width, height: desInfo.height)
      keyWord2.frame = CGRect.init(x: 42, y: keyWord1.maxY + 8, width: keyWord1.width, height: keyWord1.height)
      whiteBack.frame = CGRect.init(x: 0, y: 10, width: 135, height: 70)
      purpleBack.frame = CGRect(x: 5, y: 15, width: whiteBack.width, height: whiteBack.height)
    } else {
      whiteBack.frame = CGRect.init(x: 0, y: 10, width: 135, height: 40)
      purpleBack.frame = CGRect(x: 5, y: 15, width: whiteBack.width, height: whiteBack.height)
    }
    timeLabel.frame = CGRect(x: width - 40, y: purpleBack.maxY + 5, width: 35, height: 35)
    clickImageView.frame = CGRect(x: width -  65, y: purpleBack.maxY + 10, width: 25, height: 25)
    helperLabel.frame = CGRect(x: 5, y: purpleBack.maxY + 5, width: width - 70, height: 35)

    if userType == .player {
      if titleType == .desState {
        desInfo.frame = CGRect.init(x: 0, y: 25, width: 40, height: 20)
        keyWord1.frame = CGRect.init(x: 42, y: 25, width: 90, height: 20)
      } else {
        desInfo.frame = CGRect.init(x: 0, y: 25, width: 140, height: 22)
      }
    } else if userType == .watcher {
      desInfo.frame = CGRect.init(x: 0, y: 25, width: 140, height: 22)
      keyWord1.frame = .zero
    }
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init Fail Line ChatViewDintingNotice")
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(purpleBack)
    addSubview(whiteBack)
    addSubview(titleLabel)
    addSubview(desInfo)
    addSubview(desInfo2)
    addSubview(keyWord1)
    addSubview(keyWord2)
    addSubview(timeLabel)
    addSubview(helperLabel)
    addSubview(clickImageView)
    helperLabel.textColor = UIColor.white
    helperLabel.font = UIFont.boldSystemFont(ofSize: 12)
    helperLabel.textAlignment = .center
    clickImageView.image = UIImage.init(named: "chatDintingClick")
    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont.boldSystemFont(ofSize: 12)
    titleLabel.backgroundColor = UIColor.pinkColor()
    titleLabel.textAlignment = .center
    titleLabel.layer.cornerRadius = 10
    titleLabel.layer.masksToBounds = true
    titleLabel.text = "谁是卧底".localized
    timeLabel.textColor = UIColor.white
    timeLabel.font = UIFont.systemFont(ofSize: 12)
    timeLabel.textAlignment = .center
    whiteBack.backgroundColor = UIColor.white
    whiteBack.layer.cornerRadius = 5
    whiteBack.layer.borderColor = UIColor.textPurpleDarkColor().cgColor
    purpleBack.backgroundColor = UIColor.purpleBackColor()
    purpleBack.layer.cornerRadius = 5
    purpleBack.layer.borderColor = UIColor.textPurpleDarkColor().cgColor
    purpleBack.layer.borderWidth = 0.5
    setLabelColor(label: desInfo, UIColor.pinkColor(), nil, nil)
    setLabelColor(label: desInfo2, UIColor.textPurpleDarkColor(), nil, nil)
    setLabelColor(label: keyWord1, UIColor.white, UIColor.pinkColor(), UIColor.textPurpleDarkColor())
    setLabelColor(label: keyWord2, UIColor.textPurpleDarkColor(), UIColor.white, UIColor.textPurpleDarkColor())
    showTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(hideTime), userInfo: nil, repeats: true)
  }
  
  func hideTime() -> Void {
    if showTime == 0 {
      timeLabel.text = ""
      clickImageView.isHidden = true
      showTimer?.fireDate = Date.distantFuture
      return
    }
    timeLabel.text = "\(showTime)s"
    showTime -= 1
  }
  
  deinit {
    showTimer?.invalidate()
  }
  
  func setLabelColor(label: UILabel, _ textColor: UIColor, _ backColor: UIColor?, _ bordColor: UIColor?) -> Void {
    label.font = UIFont.systemFont(ofSize: 11)
    label.textAlignment = .center
    label.layer.cornerRadius = 5
    label.layer.masksToBounds = true
    label.textColor = textColor
    if let borderColors = bordColor?.cgColor {
      label.layer.borderColor = borderColors
      label.layer.borderWidth = 0.5
    }
    if let back = backColor {
      label.backgroundColor = back
    }
  }
}























