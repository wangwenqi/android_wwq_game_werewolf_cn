//
//  ChatApplauseView.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/2.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatApplauseView: BaseNoticeView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sureButton: UIButton!
    var position: Int = 0
    var price = 1 {
      didSet {
        titleLabel.text = "鼓掌需要花费".localized + "\(price)" + "钻石      哦~".localized
      }
    }
    override func initial() {
      self.width = Screen.width * 0.85
      self.height = 152
      self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
      self.layer.cornerRadius = 6
    }

    class func showApplauseView(_ position: Int, _ price: Int) -> ChatApplauseView {
      let view = Bundle.main.loadNibNamed(ChatApplauseView.name(), owner: nil, options: nil)?.last as! ChatApplauseView
      view.position = position
      view.price = price
      return view
    }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    cancelButton.layer.cornerRadius = 5
    sureButton.layer.cornerRadius = 5
    cancelButton.layer.masksToBounds = true
    sureButton.layer.masksToBounds = true
  }
  
    @IBAction func sureAction(_ sender: Any) {
      messageDispatchManager.sendMessage(type: .show_game_emoticon,
                                               payLoad: ["mark": "applause",
                                                         "position": position,
                                                         "data": []])
      Utils.hideNoticeView()
    }
  
    @IBAction func cancelButtonActio(_ sender: Any) {
      Utils.hideNoticeView()
    }
}
