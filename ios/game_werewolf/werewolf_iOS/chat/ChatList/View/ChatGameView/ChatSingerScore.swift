//
//  ChatSingerScore.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/31.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatSingerScore: BaseNoticeView {
  
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var sureButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var scoreLabel: UILabel!
    var position = 0
    override func initial() {
    self.width = Screen.width * 0.85
    self.height = 185
    self.center = CGPoint.init(x: Screen.width * 0.5, y: Screen.height * 0.5)
//    self.backgroundColor = UIColor(hex: "#e2dee8")
    self.layer.cornerRadius = 12
    }
  
  class func showScoreView(_ position: Int) -> ChatSingerScore {
    let view = Bundle.main.loadNibNamed(ChatSingerScore.name(), owner: nil, options: nil)?.last as! ChatSingerScore
    view.segmentControl.selectedSegmentIndex = 4
    view.position = position
    return view
  }

    override func awakeFromNib() {
      super.awakeFromNib()
      self.sureButton.layer.cornerRadius = 5
      self.cancelButton.layer.cornerRadius = 5
      self.sureButton.layer.masksToBounds = true
      self.cancelButton.layer.masksToBounds = true
    }
  
    @IBAction func cancelAction(_ sender: Any) {
      Utils.hideNoticeView()
    }
    @IBAction func sureAction(_ sender: Any) {
      messageDispatchManager.sendMessage(type: .show_game_emoticon,
                                               payLoad: ["mark": "points",
                                                        "position": position,
                                                        "data": ["res": segmentControl.selectedSegmentIndex + 1]])
      Utils.hideNoticeView()
    }
    @IBAction func segmentAction(_ sender: Any) {
      print(segmentControl.selectedSegmentIndex)
    }
}
