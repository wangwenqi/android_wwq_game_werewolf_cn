//
//  ChatRoomGameViewController.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/6.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomGameViewController: RotationViewController {

   let gameView = ChatRoomShowGif(frame: .zero, type: .gameGif)
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      let myHeight = 50 + (Screen.width - 45) / 3 + 30
      NotificationCenter.default.addObserver(self, selector: #selector(showGifChooseView(notification:)), name: NotifyName.kToolBarGifCanSelected.name(), object: nil)
      view.backgroundColor = UIColor.clear
      
      let backView = UIView()
      backView.backgroundColor = UIColor(hex: "#150c22")
      backView.frame = CGRect(x: 0, y: Screen.height - myHeight + 15, width: Screen.width, height: myHeight + 15)
      view.addSubview(backView)
      
      backView.addSubview(gameView)
      gameView.frame = CGRect(x: 0, y: 20, width: Screen.width, height: (Screen.width - 45) / 3 + 35)
    }
  
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      super.touchesBegan(touches, with: event)
      self.dismiss(animated: true, completion: nil)
    }
  
    func showGifChooseView(notification: NSNotification) -> Void {
      if let show = notification.object as? Bool, show {
        canSendGif(true)
      } else {
        canSendGif(false)
      }
    }
  
  func canSendGif(_ isCan: Bool) -> Void {
      self.gameView.alpha = isCan ? 1 : 0.5
      self.gameView.isUserInteractionEnabled = isCan
    }

  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    deinit {
      NotificationCenter.default.removeObserver(self, name: NotifyName.kToolBarGifCanSelected.name(), object: nil)
    }

}
