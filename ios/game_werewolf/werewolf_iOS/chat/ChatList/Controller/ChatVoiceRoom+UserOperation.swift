//
//  ChatVoiceRoom+UserOperation.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/26.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import orangeLab_iOS
import RxSwift
import RxCocoa

extension ChatVoiceRoomViewController {
  
  func changeRoomTitle() {
    let view = ChatRoomChangeTitle.showChatRoomChangeTitle()
    view.roomTitleTextField.text = roomNameLabel.text ?? ""
    view.selectTypeStr = roomType
    view.selectBlock = { [weak self] (title, index) in
      guard let iWeakSelf = self  else { return }
      if index < SwiftConverter.shareInstance.setAudio.types.count {
        let type = SwiftConverter.shareInstance.setAudio.types[index].type
        iWeakSelf.chatSendMessage(type: .update_title, payLoad: ["title": title, "type": type ?? ""])
      } else {
        iWeakSelf.getRoomConfig()
      }
    }
    Utils.showNoticeView(view: view)
  }
  
  func playGameAction() -> Void {
    gameGif.modalPresentationStyle = .overCurrentContext
    present(gameGif, animated: true, completion: nil)
  }
  
  // 查看资料
  func checkInfo(_ user: ChatRoomUserModel?) -> Void{
    Utils.runInMainThread { [weak self] in
      guard let weakSelf = self else { return }
      if let user = user, user.player != nil {
        let controller = UserInfoViewController()
        controller.peerID = user.id
        if controller.peerID == weakSelf.currentUserInfo.id {
          controller.isself = true
        }
        controller.peerSex = "\(user.sex)"
        controller.peerName = user.name
        controller.peerIcon = user.avatar
        let rootVC = RotationNavigationViewController.init(rootViewController: controller)
        weakSelf.present(rootVC, animated: true, completion: nil)
      }
    }
  }
  
  // 踢人走了
  func kickPlayer(_ uid: String) -> Void {
    if isGaming {
      XBHHUD.showError("游戏中不可踢出麦上的人".localized)
      return
    }
    chatSendMessage(type: .kick_out, payLoad: ["uid": uid])
  }
  
  // 封麦
  func lockPlayer(_ index: Int) -> Void {
    if isGaming {
      XBHHUD.showError("游戏中不可封麦".localized)
      return
    }
    chatSendMessage(type: .down_seat, payLoad: ["position": index])
    chatSendMessage(type: .lock, payLoad: ["position": index])
  }
  
  // 解除封麦
  func unLockPlayer(_ index: Int) -> Void {
    chatSendMessage(type: .unlock, payLoad: ["position": index])
  }
  
  // 上麦
  func upSeat(_ index: Int) {
    if isGaming {
      XBHHUD.showError("游戏中不可上麦".localized)
      return
    }
    if let id = dataSource[index]?.id, !id.isEmpty {
      XBHHUD.showTextOnlyTop(text: "此位置已有人".localized)
    } else {
      chatSendMessage(type: .up_seat, payLoad: ["dest_position": index])
    }
  }
  
  // 下麦
  func downSeat(_ index: Int) {
    if isGaming {
      XBHHUD.showError("游戏中不可下麦".localized)
      return
    }
    chatSendMessage(type: .down_seat, payLoad: ["position": index])
  }
  
  // 赠送礼物
  func sendGiftToUser(_ from: Int, _ to: Int, _ user: ChatRoomUserModel?) -> Void {
    Utils.runInMainThread { [weak self] in
      if let model = user, !model.id.isEmpty {
        RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id":model.id], success: { (json) in
          let gift = giftView.initGiftViewWith(envtype: .AUDIO, People:model.name)
          gift.toID = model.id
          gift.audioForm = from
          gift.audioTo = to
          gift.delegate = self
          Utils.getKeyWindow()?.addSubview(gift)
          gift.animation = "fadeIn"
          gift.animate()
          gift.getGiftFormRemote()
        }) { (code, message) in
          if code == 1001 {
            XBHHUD.showError("对方登录后可赠送礼物".localized)
            return;
          }
          XBHHUD.showError(message)
        }
      } else {
        XBHHUD.showError("对方登录后可赠送礼物".localized);
      }
    }
  }
  
  func holdingToSeat(_ index: Int) -> Void {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      if weakSelf.isGaming {
        XBHHUD.showError("游戏中不可上麦".localized)
        return
      }
      weakSelf.onLineListView.array = weakSelf.dataSource
      weakSelf.onLineListView.state = .holdSeat
      let view = ChatRoomAlertList.showSendGiftListView(weakSelf.onLineListView, weakSelf.currentUserInfo.oderIndex)
      view.titleLabel.text = "抱人上麦".localized
      view.emptyLabel.text = "暂无小伙伴可以上麦".localized
      Utils.showNoticeView(view: view)
      view.chatListVC?.selectIndex = { [weak self] (tmpindex, model) in
        if !model.id.isEmpty {
          self?.unLockPlayer(index)
          self?.chatSendMessage(type: .force_seat,
                               payLoad: [
                                "position": model.oderIndex,
                                "dest_position":index,
                                "user_id":model.id])
        }
        Utils.hideNoticeView()
      }
    }
  }
  
  func chaneUserStateLimit(_ position: Int, _ user: ChatRoomUserModel) -> Void {
    if isGaming {
      XBHHUD.showError("游戏中不可禁麦".localized)
      return
    }
    let state = user.state == "limit" ? "" : "limit"
    chatSendMessage(type: .change_user_state, payLoad: ["state": state, "position": position])
  }
  
  func chaneUserStateFree(_ position: Int, _ user: ChatRoomUserModel) -> Void {
    if isGaming {
      XBHHUD.showError("游戏中不可使用自由模式".localized)
      return
    }
    let state = user.state == "free" ? "" : "free"
    if state == "free" {
      chatSendMessage(type: GameMessageType.request_free_mode, payLoad:["position": position] )
    } else {
      chatSendMessage(type: .change_user_state, payLoad: ["state": "", "position": position])
    }
  }
  
  // 点击头像弹出的Alert
  func chatRoomAlertShow(_ position: Int, _ users: ChatRoomUserModel) -> Void {
    let uid = users.id
    view.endEditing(true)
    let user = users
    let isMaster = currentUserInfo.oderIndex == 0 ? true : false
    switch user.userRoomState {
    case .roomOfClose:
      if isMaster { // 房主点击被封的麦
        let view = ChatRoomManagerView.showChatRoomManagerView(.chooseLock)
        view.seletdBlock = { [weak self] state in
          switch state {
          case .leftChoose:
            self?.holdingToSeat(position)
          case .rightChoose:
            self?.unLockPlayer(position)
          default:
            break
          }
        }
        Utils.showAlertView(view: view)
      } else {
        XBHHUD.showError("此麦已经封禁".localized)
        return
      }
    case .roomOfEmpty:
      if isMaster {
        // 房主点空位置
        let view = ChatRoomManagerView.showChatRoomManagerView(.chooseEmpty)
        view.seletdBlock = { [weak self] state in
          switch state {
          case .leftChoose:
            self?.holdingToSeat(position)
          case .rightChoose:
            self?.lockPlayer(position)
          default:
            break
          }
        }
        Utils.showAlertView(view: view)
      } else {
        // 非房主点击空位置
        upSeat(position)
      }
    case .roomOfOnMicro, .roomOfMaster:
      if isMaster {             // 自己是房主 点击的不是自己
        if currentUserInfo.oderIndex != position {
          let view = ChatRoomChooseUser.showChatRoomChooseUser(true, user, position)
          view.selectBlock = {  [weak self] state in
            switch state {
            case .checkUserInfo:
              self?.checkInfo(user)
            case .sendGift:
              self?.sendGiftToUser(self?.currentUserInfo.oderIndex ?? 999, position, user)
            case .setCloseSeat:
              self?.lockPlayer(position)
            case .setDownSeat:
              self?.downSeat(position)
            case .setLeaveRoom:
              self?.kickPlayer(uid)
            case .closeSomeOne:
              self?.chaneUserStateLimit(position, user)
            case .freeModel:
              self?.chaneUserStateFree(position, user)
            default:
              break
            }
          }
          Utils.showAlertView(view: view)
        } else {
          // 自己是房主 点击的是自己
          let view = ChatRoomChooseSelf.showChatRoomChooseSelf(true, user)
          view.selectBlock = { [weak self] state in
            switch state {
            case .checkUserInfo:
              self?.checkInfo(user)
            case .downSeat:
              self?.chaneUserStateFree(position, user)
            case .sendGift:
              self?.sendGiftToUser(self?.currentUserInfo.oderIndex ?? 999, position, user)
            }
          }
          Utils.showAlertView(view: view)
        }
      } else {
        // 自己不是房主, 点击了自己
        if currentUserInfo.oderIndex == position {
          let view = ChatRoomChooseSelf.showChatRoomChooseSelf(false, user)
          view.selectBlock = { [weak self] state in
            switch state {
            case .checkUserInfo:
              self?.checkInfo(user)
            case .downSeat:
              self?.downSeat(position)
            case .sendGift:
              self?.sendGiftToUser(self?.currentUserInfo.oderIndex ?? 999, position, user)
            }
          }
          Utils.showAlertView(view: view)
        } else {
          // 自己不是房主, 点击别人
          let view = ChatRoomChooseUser.showChatRoomChooseUser(false, user, position)
          view.selectBlock = { [weak self] state in
            switch state {
             case .checkUserInfo:
                self?.checkInfo(user)
             case .sendGift:
               self?.sendGiftToUser(self?.currentUserInfo.oderIndex ?? 999, position, user)
             default:
               break
            }
          }
          Utils.showAlertView(view: view)
        }
      }
    }
  }
  
  func updateMasterInfo() -> Void {
    
    if let model = dataSource[0] {
      // 更新房主信息
      if let _ = model.player {
        ownerNameLabel.text = model.name
      } else {
        managerView.model = model
        ownerNameLabel.text = ""
      }
      
      // 是否允许编辑聊天主题
      if CurrentUser.shareInstance.id == model.id {
        gameButton.isHidden = false
        currentUserInfo.oderIndex = 0
        CurrentUser.shareInstance.currentPosition = 0
        roomTitleLabel.isUserInteractionEnabled = true
        roomTitleLabel.isHidden = false
        titleLabel.editImageView.image = UIImage(named: "chatRoom_Edit_Icon")
        moreOrderButton.isHidden = false
      } else {
        moreOrderButton.isHidden = canPurchaseroom ? !(CurrentUser.shareInstance.id == CurrentUser.shareInstance.roomOwnerId) : true
        titleLabel.isEditState = false
        roomTitleLabel.isUserInteractionEnabled = false
        roomTitleLabel.isHidden = true
        titleLabel.editImageView.image = UIImage(named: "chatRoom_unEdit_icon")
        
      }
    }
  }
  
  func changUserMicState(_ state: String) -> Void {
    if tooBar.changeUserState == state, state == "", currentUserInfo.oderIndex == 0 {
    } else {
      tooBar.changeUserState = state
    }
  }
  
  func gameCloseOrOpen(state: Bool) -> Void {
    guard CurrentUser.shareInstance.currentPosition <= chatRoomOnLineNumber else {
      if tooBar.isOnMir == true {
        tooBar.isOnMir = false
      }
      return
    }
    if CurrentUser.shareInstance.can_cut_speaker {
      if state == true {
        if tooBar.isOnMir != true {
          tooBar.isOnMir = true
        }
      }
    } else {
      if tooBar.isOnMir != state {
        tooBar.isOnMir = state
        if state == false {
          chatSendMessage(type: .unspeak)
        }
      }
    }
  }
  
  func closeOrOpenMir() -> Void {
    // 此处增加游戏按钮
    layoutDintingView()
    let state = dataSource[currentUserInfo.oderIndex]?.state
    gameButton.isHidden = currentUserInfo.oderIndex > 8
    if  state == "free" {
      if tooBar.changeUserState != "free" {
        tooBar.changeUserState = "free"
      }
      changeSound(false)
      return
    } else if state == "limit" {
      if tooBar.changeUserState != "limit" {
        tooBar.changeUserState = "limit"
      }
      gameButton.isHidden = false
      changeSound(true)
      return
    } else {
      changUserMicState("")
    }
    if tooBar.isOnMir,  currentUserInfo.oderIndex > 8 {
      tooBar.isOnMir = false
      UIView.animate(withDuration: 0.2, animations: { [weak self] in
        self?.tooBarFrame?.value = CGRect(x: 0, y: Screen.height - 50, width: Screen.width, height: 50)
      })
      changeSound(true)
    }
    if !tooBar.isOnMir, currentUserInfo.oderIndex <= chatRoomOnLineNumber {
      tooBar.isOnMir = true
      changeSound(true)
    }
  }
  
  
  func asslysisData(_ room: String) {
    CurrentUser.shareInstance.currentRoomType = "audio"
    dataSource = [Int: ChatRoomUserModel]()
    // 先来9个空位...
    for index in 0..<10 {
      dataSource[index] = ChatRoomUserModel()
    }
    if data[room]["likeCount"].intValue != 0 {
      likeLabel.text = data[room]["likeCount"].stringValue
    }
    roomNameLabel.text = data[room]["title"].stringValue
    CurrentUser.shareInstance.currentRoomID = data[room]["room_id"].stringValue
    CurrentUser.shareInstance.realCurrentRoomID = data[room]["room_id"].stringValue
    CurrentUser.shareInstance.roomOwnerId = data[room]["owner_id"].stringValue
    CurrentUser.shareInstance.ownerType = data[room]["config"]["room_info"]["owner_type"].stringValue
    canPurchaseroom = data[room]["config"]["purchase_room"].boolValue
    titleLabel.titleText = data[room]["title"].stringValue
    onLineView.roomId = data[room]["room_id"].stringValue
    getRoomConfig()
    CurrentUser.shareInstance.can_cut_speaker = data[room]["config"]["can_cut_speaker"].boolValue
    // 用户们
    onLineView.onLine = 0
    currentUserInfo.player = nil
    for (key, value) in data[room]["users"].dictionaryValue {
      onLineView.onLine += 1
      let model = ChatRoomUserModel()
      model.player = value
      model.userRoomState = .roomOfOnMicro
      if let number = Int(key) {
        dataSource[number] = model
      }
      if value["id"].stringValue == CurrentUser.shareInstance.id {
        currentUserInfo.player = model.player
        CurrentUser.shareInstance.name = currentUserInfo.name
        currentUserInfo.userRoomState = .roomOfOnMicro
        if let position = Int(key) {
          currentUserInfo.oderIndex = position
          CurrentUser.shareInstance.currentPosition = position
        }
        if room != "room" {
          // 重连的时候
          if tooBar.changeUserState != value["state"].stringValue {
            tooBar.changeUserState = value["state"].stringValue
          } else {
            tooBar.isOnMir = currentUserInfo.oderIndex <= chatRoomOnLineNumber ? true : false
          }
        } else {
          tooBar.isOnMir = currentUserInfo.oderIndex <= chatRoomOnLineNumber ? true : false
        }
      }
    }
    isGaming = data[room]["isPlaying"].boolValue
    if isGaming {
      layoutDintingView()
      let deathArr = data["game_info"]["death"].arrayValue
      for posi in deathArr {
        dataSource[posi.intValue]?.isDeath = true
      }
      let speedInfo = data["game_info"]["speech_info"]["current"].dictionaryValue
      if let tmpPosition = speedInfo["position"]?.intValue, tmpPosition != 0 {
        CurrentUser.shareInstance.isGameing = .isSpeechState
        dintingNotice.setHelpWorld(text: "\(tmpPosition)" + "号发言".localized)
        gameCloseOrOpen(state: tmpPosition == currentUserInfo.oderIndex)
        dintingNotice.setDuring(speedInfo["last_time"]?.intValue)
      }
    }
    getTypeIndex(data[room]["child_type"].stringValue, data[room]["theme"]["background"].stringValue)
    // 封麦位置
    let _ = dataSource.map { (key, value) -> Void in
      if data[room]["locked_positions"].arrayValue.contains(JSON(key)) {
        value.userRoomState = .roomOfClose
      }
    }
    reloadAllUserInfos()
    guard !currentUserInfo.id.isEmpty else {
      leave() // 守護一下 永無幽靈模式
      return
    }
    let notiMessage =
      room == "room_info"
        ? "您已经重新连接".localized
        : "欢迎来到娱乐房间，与朋友一起K歌，嗨麦，闲聊，玩小游戏。请大家文明游戏，愉快玩耍".localized
    messageViewController.addNoticeConversion(type: .system, messge: notiMessage, colorType: .hide)
    
    guard let _ = session else {
      // 链接语音服务器
      DispatchQueue.global().async { [weak self] in
        guard let weakSelf = self else { return }
        let mediaServerData = weakSelf.data["media_server"]
        if let serverType = mediaServerData["server_type"].string {
          weakSelf.startConnect(weakSelf.data[room]["room_id"].stringValue, config: weakSelf.data["media_server"]);
        }
      }
      return
    }
  }
  
  // 当前的主题
  func getTypeIndex(_ type: String, _ theme: String = "") {
    
    CurrentUser.shareInstance.messageShow = true
    CurrentUser.shareInstance.isGameing = .isNotAGame
    if let iconImage = UIImage(named: ("icon_" + type)) {
      onLineView.roomImage.image = iconImage
    } else {
      onLineView.roomImage.image = UIImage.init(named: "chatRoom_userIcon_Icon")
    }
    if let bgImage = UIImage(named: "bg_" + type), let themeURL = URL.init(string: theme) {
      backGroundImageView.sd_setImage(with: themeURL, placeholderImage: bgImage)
    } else {
      if let themeURL = URL.init(string: theme) {
        backGroundImageView.sd_setImage(with: themeURL, placeholderImage: UIImage(named: "bg_sing"))
      } else {
        backGroundImageView.image = UIImage(named: "bg_sing")
      }
    }
    
    if type == "undercover" {
      if isGaming {
        XBHHUD.showTextOnlyTop(text: "游戏正在进行中".localized)
      }
      CurrentUser.shareInstance.messageShow = !isGaming
      CurrentUser.shareInstance.isGameing = isGaming ? .isSpeechState : .isGameOverStates
    }
    if roomType != type {
      reloadAllUserInfos()
    }
    layoutDintingView()
    roomType = type
  }
  
  func showGameoverView(_ jsonData: JSON) -> Void {
    if CurrentUser.shareInstance.currentPosition <= chatRoomOnLineNumber,
      CurrentUser.shareInstance.currentPosition > 0 {
      let tmp = ChatRoomDintingResultVC.init(nibName: "ChatRoomDintingResultVC", bundle: nil)
      tmp.modalPresentationStyle = .overCurrentContext
      tmp.modalTransitionStyle = .crossDissolve
      var loses = [ChatRoomUserModel]()
      var wins = [ChatRoomUserModel]()
      let peoples = jsonData["roles"]["people"].arrayValue
      let undercovers = jsonData["roles"]["undercover"].arrayValue
      let undercoverWin = jsonData["win_type"].stringValue == "undercover"
      for people in peoples {
        let position = people["position"].intValue
        if let detailWord = people["detail"].string, !detailWord.isEmpty {
          tmp.winText = detailWord // 写入好人词语
        }
        if position == currentUserInfo.oderIndex {
          tmp.isSuccess = !undercoverWin
          tmp.myRole = true
        } // 我是好人
        if let model = dataSource[position] {
          model.oderIndex = position
          wins.append(model)
        }
      }
      for undercover in undercovers {
        let position = undercover["position"].intValue
        if let detailWord = undercover["detail"].string, !detailWord.isEmpty {
          tmp.lostText = detailWord // 写入狼人词语
        }
        if position == currentUserInfo.oderIndex {
          tmp.isSuccess = undercoverWin
          tmp.myRole = false
        } // 我是卧底
        if let model = dataSource[position] {
          model.oderIndex = position
          loses.append(model)
        }
      }
      tmp.wins = wins
      tmp.loses = loses
      if tmp.lostText.isEmpty {
        if tmp.winText == jsonData["words"]["first"].stringValue {
          tmp.lostText = jsonData["words"]["second"].stringValue
        } else {
          tmp.lostText = jsonData["words"]["first"].stringValue
        }
      }
      messageViewController.addDintingConversion(mType: ChatRoomDintingCell.DintingState.gameOver, message: "游戏结束".localized + (undercoverWin ? "卧底胜利".localized : "好人胜利".localized) + "\n" + "好人词".localized + ": \(tmp.winText)" + "\n" + "卧底词".localized + ": \(tmp.lostText)")
      present(tmp, animated: true, completion: nil)
    }
  }
  
  // 游戏结束重置房间状态
  func resetGame() -> Void {
    isGaming = false
    dintingNotice.isHidden = true
    if currentUserInfo.oderIndex == 0 {
      dintingSetView.isHidden = false
      dintingSetView.dintingSetView.textField1.text = ""
      dintingSetView.dintingSetView.textField2.text = ""
    }
    CurrentUser.shareInstance.isGameing = .isGameOverStates
    CurrentUser.shareInstance.isDeath = false
    for i in 1...8 {
      dataSource[i]?.isDeath = false
      dataSource[i]?.prepared = false
      dataSource[i]?.isVoted = false
      dataSource[i]?.votes = [Int]()
    }
    dataSource[0]?.masterReady = false
    reloadAllUserInfos()
    layoutDintingView()
    noticeView.isHidden = false
  }
  
  // 展示投票结果
  func voteResultShow(_ jsonData: JSON) -> Void {
    let finished = jsonData["finished"].boolValue
    let voteInfo = jsonData["vote_info"].dictionaryValue
    var voteArray = [String]()
    for index in 1...8  {
      var tmpVotes = [Int]()
      if let votes = voteInfo["\(index)"]?.arrayValue {
        for vote in votes {
          tmpVotes.append(vote.intValue)
        }
      }
      dataSource[index]?.votes = tmpVotes
      dataSource[index]?.isVoted = false
      reloadUserState(index)
      if finished, !tmpVotes.isEmpty {
        var noticeStr = ""
        for num in tmpVotes {
          noticeStr = noticeStr + "\(num)" + "号".localized
        }
        noticeStr = noticeStr + "投票给".localized + "\(index)" + "号玩家".localized
        voteArray.append(noticeStr)
      }
    }
    if finished, let giveUpvotes = voteInfo["-1"]?.arrayValue, !giveUpvotes.isEmpty {
      var giveUpStr = ""
      for vote in giveUpvotes {
        giveUpStr = giveUpStr + "\(vote.intValue)" + "号".localized
      }
      voteArray.append(giveUpStr + "弃票".localized)
    }
    if !voteArray.isEmpty {
      var sendMessage = ""
      for mess in voteArray {
        if sendMessage.length == 0 {
          sendMessage = mess
        } else {
          sendMessage = sendMessage + "\n" + mess
        }
      }
      messageViewController.addDintingConversion(mType: ChatRoomDintingCell.DintingState.voteResult, message: sendMessage)
    }
    if jsonData["type"].stringValue == "death" {
      let duration = jsonData["duration"].intValue
      if duration != 0 {
        dintingNotice.setDuring(duration)
      }
      voteShowPK(jsonData, duration)
    }
  }
  
  // 展示PK
  func voteShowPK(_ jsonData: JSON, _ duration: Int) -> Void {
    if jsonData["need_pk"].boolValue {
      // 显示PK
      var max  = 0
      var subTitle = ""
      for i in 1...8 {
        let tmpNum = dataSource[i]?.votes.count ?? 0
        if tmpNum > max {
          max = tmpNum
          subTitle = "\(i)" + "号玩家".localized
        } else if tmpNum == max {
          subTitle = subTitle + "与".localized + "\(i)" + "号玩家".localized
        }
      }
      subTitle = subTitle + "平票, 进入PK阶段".localized
      messageViewController.addDintingConversion(mType: ChatRoomDintingCell.DintingState.voteResult,message: subTitle)
      let pkView = ChatDintingAlert.init(frame: .zero, time: duration, title: "投票结果", subTitle: subTitle)
      Utils.showAlertView(view: pkView)
      for i in 0...8 {
        dataSource[i]?.isVoted = false
        dataSource[i]?.votes = [Int]()
        reloadUserState(i)
      }
    }
  }
  
}
