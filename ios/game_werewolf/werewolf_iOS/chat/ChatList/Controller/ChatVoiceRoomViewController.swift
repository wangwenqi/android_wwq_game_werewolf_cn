//
//  ChatVoiceRoomViewController.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/24.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import orangeLab_iOS
import ChatKit
import RxSwift
import RxCocoa

var chatRoomOnLineNumber = 8  // 有多少个麦位

class ChatVoiceRoomViewController: RotationViewController {
    // 当前用户
    let  tooBar: ChatRoomVoiceToolBar = Bundle.main.loadNibNamed("ChatRoomVoiceToolBar", owner: nil, options: nil)?.last as! ChatRoomVoiceToolBar   // 底部工具栏
    var tooBarFrame: Observable<CGRect>?      // 工具栏frame的监听
    let noticeView = ChatNoticeView(frame: CGRect(x: Screen.width, y: 75, width: 0, height: 25))  // 通知页面
    let onLineView = ChatRoomOnLineLabel(frame: CGRect.zero)  // 在线人数与房间号码
    let managerView = ChatRoomUserView.init(frame: .zero)     // 房主
    let backGroundImageView = UIImageView()           // 背景图片
    let collectionBackView = UIView()                 // 麦上列表的背景    // 可优化
    let ownerNameLabel = UILabel()                    // 房主名称的label
    let Wifi_State = UIImageView()                    // Wifi状态图标
    let roomNameLabel = UILabel()                     // 顶部房间名称的label
    let chatView = UIView()                           // 聊天view北京图
    let likeButton = SpringView()                     // 点赞按钮
    let likeLabel = UILabel()                         // 点赞的label数量   // 可优化
    let gameButton = UIButton(type: .system)          // 小游戏按钮
    let backButton = UIButton(type: .system)          // 返回按钮
    let shareButton = UIButton(type: .system)         // 分享按钮
    let moreOrderButton = UIButton(type: .system)     // 房主的设置按钮
    let roomTitleLabel = UIView()                     // 头像底部房间名称View // 可优化
    let titleLabel = ChatRoomTitleView()              // 房主名称底部label
    let messageViewController = ChatRoomChatList()    // 消息框
    var onLineListView = ChatListViewController()     // 在线列表
    weak var delegate:GiftSysDelegate?                // 礼物代理
    var giftmanager:giftManager = giftManager()       // 礼物管理
    var giftViewsArray = [ChatRoomUserView]()         // 接收礼物的views
    var roomType = ""                                 // 默认唱歌房间类型
    var currentUserInfo = ChatRoomUserModel()         //  当前用户信息
    var dataSource = [Int: ChatRoomUserModel]()       //  此房间上麦克人数组, 包括房主
    var dismissSelf: ((URL?) -> Void)?                //  返回退出的回掉
    internal var session: OLSession?                  //  Olive
    var olConfig:[String: Any] = [:];                 //  Olive
    var data = JSON.init(parseJSON: "")               //  进入房间的数据
    weak internal var publisher: OLPublisher?         //  Olive
    var connectUser:OLConnectorInfos = OLConnectorInfos() // Olive
    var canMiniModel = false
    var canPurchaseroom = false
    let disposeBag = DisposeBag.init()
    lazy var roomOpenView = ChatRoomOpenMic()
    lazy var miniButton = MoveButton(frame: .zero)
    lazy var frendListSet = Set<String>()                              // 添加过好友的列表
    lazy var frendRequests = [ChatRoomAddFriendRequest]()              // 好友请求
    lazy var gameGif = ChatRoomGameViewController()                    // 语音放Gif游戏
    lazy var blackList = ChatBlackListController()                     // 黑名单
    lazy var chat = { return LCChatKit.sharedInstance().client }()     // 发送消息
    lazy var dintingSetView = ChatViewReadyView()                      // 谁是卧底房主的设置与开始游戏
    lazy var dintingNotice = ChatViewDintingNotice.init(frame: CGRect(x: self.view.frame.width - 140, y: 65, width: 135, height: 100))
    var isGaming = false {
      didSet {
        if isGaming {
          self.moreOrderButton.isHidden = true
        } else {
          if CurrentUser.shareInstance.currentPosition == 0 {
            self.moreOrderButton.isHidden = false
          }
        }
      }
    }
    var myWord = ""
  
    init() {
      super.init(nibName: nil, bundle: nil)
      DispatchQueue.main.async { [weak self] in
        self?.configUserViews()      // 主頁面組件
        self?.configNavi()           // 基礎按鈕等
        self?.configBaseView()       // 基礎按鈕block等
        self?.registerNotification() // 注册在线通知
        self?.toolBarNoticeAction()
        self?.configGiftView()
      }
    }
  
    required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }
  
    override func viewDidLoad() {
      super.viewDidLoad()
      self.title = ""
      view.backgroundColor = .white
      tooBarFrame = Observable.init(tooBar.frame)
      tooBarFrame?.afterChange += { [weak self] in
        guard let weakSelf = self  else { return }
        if $1.height == 50 {
          weakSelf.tooBar.backgroundColor = UIColor.clear //UIColor(hex: "#6D50BD")
        } else {
          weakSelf.tooBar.backgroundColor = UIColor(hex: "#150c22")
        }
        if kIsIPhoneX {
          var myBounds = $1
          if myBounds.height + (UIScreen.main.bounds.height - myBounds.origin.y) < 110 {
            myBounds.origin.y -= kSafeAreaBottomHeight
            weakSelf.tooBar.frame = myBounds
            return
          }
        }
        weakSelf.tooBar.frame = $1
      }
      
      NotificationCenter.default.rx.notification(NotifyName.kSendGift.name()).subscribe(onNext: { [weak self] (note) in
        guard let weakSelf = self else { return } 
        if let model = note.object as? ChatRoomUserModel {
          weakSelf.sendGiftToUser(0, model.position, model)
        }
      }).disposed(by: disposeBag)
      
      UIViewController.topWindow()?.addSubview(miniButton)
      miniButton.tag = 9901
      miniButton.frame = CGRect.init(x: UIScreen.main.bounds.width - 70, y: UIScreen.main.bounds.height - 200, width: 50, height: 50)
      miniButton.layer.cornerRadius = 25
      miniButton.layer.masksToBounds = true
      miniButton.imageView?.contentMode = .scaleAspectFill
      miniButton.backgroundColor = UIColor.clear
      miniButton.isHidden = true
      miniButton.rx.tap.subscribe(onNext: { (_) in
        NotificationCenter.default.post(name: NotifyName.kBackGroundModel.name(), object: true)
      }).disposed(by: disposeBag)
    }
  
  func getRoomConfig() -> Void {
    RequestManager().get(url: RequestUrls.audioConfig, parameters: ["room_id": CurrentUser.shareInstance.currentRoomID], success: { [weak self] (json) in
      if let jsons = json["audio_room_types"].array {
        self?.canMiniModel = json["mini_mode"].boolValue
        SwiftConverter.shareInstance.setAudio.setModels(jsons)
      }
    }) { (code, msg) in
      
    }
  }
  
    override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      if messageViewController.view.frame != chatView.bounds {
        messageViewController.view.frame = chatView.bounds
      }
    }
  
    override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(true)
      UIApplication.shared.isIdleTimerDisabled = true
      CurrentUser.shareInstance.loadingGame = false
    }
  
  
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(true)
      navigationController?.setNavigationBarHidden(true, animated: true)
    }
  
  deinit {
    CurrentUser.shareInstance.inRoomStatus = .none
    XBHHUD.hide()
  }
}

// 点击某个用户的事件
extension ChatVoiceRoomViewController {
  // 刷新某个人
  func reloadUserState(_ index: Int?) {
    if let index = index, index <= chatRoomOnLineNumber, index >= 0 {
      if index == 0 {
        if dataSource[0]?.state == "limit" {
          dataSource[0]?.state = ""
        }
        updateMasterInfo()
      } else {
        if dataSource[index] == nil {
          dataSource[index] = ChatRoomUserModel()
        }
      }
      if index >= giftViewsArray.count {
        return
      }
      let tmpChatView = giftViewsArray[index]
      tmpChatView.model = dataSource[index] ?? ChatRoomUserModel()
    }
  }
  
  // 刷新所有人
  func reloadAllUserInfos() -> Void {
    for i in 0...8 {
      reloadUserState(i)
    }
  }
  
  func chooseUserIndex(control: UIControl) -> Void {
    print(control.tag)
    let index = control.tag - 10000
    if let model = dataSource[index] {
      chatRoomAlertShow(index, model)
    }
  }
}

// 按钮事件
extension ChatVoiceRoomViewController {
  
  func leave() -> Void {
    Utils.stopPlaySound()
    Utils.hideNoticeView()
    miniButton.removeFromSuperview()
    UIApplication.shared.isIdleTimerDisabled = false
    CurrentUser.shareInstance.clearCurrentInformation()
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "giftviewout"), object: nil)
    giftmanager.view = nil  
    CurrentUser.shareInstance.inRoomStatus = .none
    // 退出房间后进行重连次数修改
    CurrentUser.shareInstance.clearCurrentInformation()
    CurrentUser.shareInstance.currentRoomID = ""
    CurrentUser.shareInstance.realCurrentRoomID = ""
    CurrentUser.shareInstance.from = ""
    CurrentUser.shareInstance.currentRoomPassword = ""
    SwiftConverter.shareInstance.roomTypeName = ""
    messageDispatchManager.sendMessage(type: GameMessageType.leave)
    disConnect()
    SwiftConverter.shareInstance.sendMessagesToRN(json: JSON(["action":"LEAVE_AUDIO_ROOM"]))
    XBHHUD.showCanNotHidden(title: NSLocalizedString("正在清理房间数据", comment: ""))
    messageDispatchManager.close()
  }
  
  func sendLike() -> Void {
    likeButton.animation = "pop"
    likeButton.curve = "easeIn"
    likeButton.repeatCount = 1
    likeButton.damping = 0.8
    likeButton.force = 1.5
    likeButton.duration = 0.8
    likeButton.animate()
    chatSendMessage(type: .like_room, payLoad: ["type": "like_room"])
  }
  
  func pushToListView() -> Void {
    onLineListView.array = dataSource
    onLineListView.state = .allOnline
    let mView = ChatRoomAlertList.showSendGiftListView(onLineListView, currentUserInfo.oderIndex)
    mView.titleLabel.text = "在线列表".localized
    Utils.showNoticeView(view: mView)
    mView.chatListVC?.selectIndex = { [weak self] (index, model) in
      Utils.hideNoticeView()
      if let _ = model.player {
        let controller = UserInfoViewController()
        controller.peerID = model.id
        if controller.peerID == CurrentUser.shareInstance.id {
          controller.isself = true
        }
        controller.peerSex = "\(model.sex)"
        controller.peerName = model.name
        controller.peerIcon = model.avatar
        let rootVC = RotationNavigationViewController.init(rootViewController: controller)
        self?.present(rootVC, animated: true, completion: nil)
      }
    }
  }
  
  func editRoomTitle() -> Void {
    if !CurrentUser.shareInstance.messageShow {
      XBHHUD.showError("游戏中不可更改主题".localized)
      return
    }
    if SwiftConverter.shareInstance.setAudio.types.isEmpty {
      getRoomConfig()
    } else {
      realEditTitle()
    }
    
  }
  
  func realEditTitle() -> Void {
    let view = ChatRoomChangeTitle.showChatRoomChangeTitle()
    view.roomTitleTextField.text = roomNameLabel.text ?? ""
    view.selectTypeStr = roomType
    view.selectBlock = { [weak self] (title, index) in
      let type = SwiftConverter.shareInstance.setAudio.types[index].type
      self?.chatSendMessage(type: .update_title, payLoad: ["title": title, "type": type ?? ""])
    }
    Utils.showNoticeView(view: view)
  }
  
  func roomOwnerAction() -> Void {
    if let model = dataSource[0] {
      chatRoomAlertShow(0, model)
    }
  }
  
  func shareAction() -> Void {
    for tType in SwiftConverter.shareInstance.setAudio.types {
      if roomType == tType.type {
        SwiftConverter.shareInstance.roomTypeName = tType.name ?? ""
      }
    }
    let shareView = ChatRoomShareFrends.showChatRoomShareFrends(roomId: onLineView.roomId, roomType, roomNameLabel.text)
    Utils.showNoticeView(view: shareView)
  }
  
  func applyCertMethod() -> Void {
    // 申请房契
    let applyView = ChatRoomApplyCert.init(height: 0, title: "申请房契".localized, money: 50, roomid: CurrentUser.shareInstance.currentRoomID)
    applyView.buyAction = { buy in
      if buy {
        Utils.hideAnimated()
        messageDispatchManager.sendMessage(type: GameMessageType.purchase_room)
      }
    }
    Utils.showAlertView(view: applyView)
  }
  
  func exCertMethod(_ model: ChatRoomUserModel?) -> Void {
    let excert = ChatRoomApplyCert.init(height: 0, title: "转让房契".localized, money: 50, roomid: CurrentUser.shareInstance.currentRoomID, model: model)
    excert.buyAction = { [weak self] buy in
      if buy {
        if let _ = model {
          messageDispatchManager.sendMessage(type: GameMessageType.handover_creator, payLoad: ["user_id": model?.id ?? ""])
          Utils.hideAnimated()
        } else {
          self?.chooseExCert()
        }
      } else {
        self?.chooseExCert()
      }
    }
    Utils.showAlertView(view: excert)
  }
  
  func chooseExCert() -> Void {
    onLineListView.array = dataSource
    onLineListView.state = .sendManager
    let mView = ChatRoomAlertList.showSendGiftListView(onLineListView, currentUserInfo.oderIndex)
    mView.titleLabel.text = "转让房契".localized
    mView.emptyLabel.text = "暂无小伙伴可以转让".localized
    mView.chatListVC?.selectIndex = { [weak self] (_, model) in
      guard let weakSelf = self else { return }
      weakSelf.exCertMethod(model)
    }
    Utils.showNoticeView(view: mView)
  }
  
  func becomeMasterMethod() -> Void {
    messageDispatchManager.sendMessage(type: GameMessageType.request_master)
    Utils.hideAnimated()
  }
  
  func moreOrderButtonAction() {
    enum RoomOwnerState {
      case masterUnBuyCreate // 如果我是房主 未购买 我创建的
      case masterBuyCreate   // 如果我是房主 购买过 我创建的
      case masterUnCreate    // 如果我是房主 不是我创建的 // 购买不购买不重要了
      case seaterUnBuyCreate // 我不是房主, 但是我是创建者, 我没买过
      case seaterBuyCreate   // 我不是房主, 我是创建者, 我买过
      case unknown
    }
    var firstIcon: ChatActionStruct?
    var secondIcon: ChatActionStruct?
    var mView: ChatRoomOrderAlert?
    var roomState = RoomOwnerState.unknown
    
    // 如果自己是房主
    let changeName = ChatActionStruct.init(image: UIImage.init(named: "chatRoom_managerUserTitle"), title: "房间主题".localized, tag: 0)
    let roomWord = ChatActionStruct.init(image: UIImage.init(named: "chatRoom_managerUserPassword"), title: "房间密码".localized, tag: 1)
    let applyCert = ChatActionStruct.init(image: UIImage.init(named: "chatRoom_requestCert"), title: "申请房契".localized, tag: 2)
    let roomBlack = ChatActionStruct.init(image: UIImage.init(named: "chatRoom_blackList"), title: "房间黑名单".localized, tag: 3)
    let exCert = ChatActionStruct.init(image: UIImage.init(named: "chatRoom_exchangeCert"), title: "转让房契".localized, tag: 4)
    let becomeMaster = ChatActionStruct.init(image: UIImage.init(named: "chatRoom_becomeMaster"), title: "成为房主".localized, tag: 5)
    
    if !canPurchaseroom {
      CurrentUser.shareInstance.roomOwnerId = ""
      CurrentUser.shareInstance.ownerType = ""
    }
    
    if CurrentUser.shareInstance.currentPosition == 0 {
      if CurrentUser.shareInstance.roomOwnerId == CurrentUser.shareInstance.id,
        CurrentUser.shareInstance.ownerType != "purchased" {
        mView = ChatRoomOrderAlert.init(height: 165, title: "", icons: [applyCert, roomBlack,changeName,roomWord])
        roomState = .masterUnBuyCreate
      }
      if CurrentUser.shareInstance.roomOwnerId == CurrentUser.shareInstance.id,
        CurrentUser.shareInstance.ownerType == "purchased" {
        mView = ChatRoomOrderAlert.init(height: 165, title: "", icons: [exCert,roomBlack,changeName,roomWord])
        roomState = .masterBuyCreate
      }
      if CurrentUser.shareInstance.roomOwnerId != CurrentUser.shareInstance.id {
        mView = ChatRoomOrderAlert.init(height: 165, title: "", icons: [roomBlack,changeName,roomWord])
        roomState = .masterUnCreate
      }
    } else {
      if CurrentUser.shareInstance.roomOwnerId == CurrentUser.shareInstance.id,
        CurrentUser.shareInstance.ownerType != "purchased" {
        mView = ChatRoomOrderAlert.init(height: 165, title: "", icons: [applyCert])
        roomState = .seaterUnBuyCreate
      }
      if CurrentUser.shareInstance.roomOwnerId == CurrentUser.shareInstance.id,
        CurrentUser.shareInstance.ownerType == "purchased" {
        mView = ChatRoomOrderAlert.init(height: 165, title: "", icons: [becomeMaster,exCert])
        roomState = .seaterBuyCreate
      }
    }
    
    func buttonAction(_ tag: Int) {
      switch tag {
      case 0:
        changeRoomTitle()
      case 1:
        setPassword()
      case 2:
        applyCertMethod()
      case 3:
        Utils.hideAnimated()
        messageDispatchManager.sendMessage(type: .block_user_list)
        navigationController?.pushViewController(blackList, animated: true)
      case 4:
        exCertMethod(nil)
      case 5:
        becomeMasterMethod()
      default:
        break
      }
    }
    
    guard let view = mView else { return }
    // 房主编辑主题/密码
    view.selectBlock = { selectIndex in
      if let tag = view.icons?[selectIndex].tag {
        buttonAction(tag)
      } else {
        Utils.hideAnimated()
      }
    }
    Utils.showAlertView(view: view)
  }
  
  func setPassword() -> Void {
    let passwordView = ChatRoomChangePassword.showChatRoomChangePassword()
    passwordView.selectPassword = { password in
      messageDispatchManager.sendMessage(type: GameMessageType.change_password, payLoad: ["password": password])
      XBHHUD.showTextOnlyTop(text: NSLocalizedString("密码修改成功", comment: ""))
    }
    Utils.showAlertView(view: passwordView)
  }
  
  func backAction() {
    DispatchQueue.main.async {[weak self] in
      self?.makeSureLeave()
    }
  }
  
  func masterLeave() -> Void {
    if currentUserInfo.oderIndex == 0 {
      onLineListView.array = dataSource
      onLineListView.isShow = true
      onLineListView.sendManagerList()
      onLineListView.isShow = false
      onLineListView.state = .sendManager
      let array = onLineListView.dataSource
      if array.count != 0 {
        let view = ChatRoomAlertList.showSendGiftListView(onLineListView, currentUserInfo.oderIndex)
        view.titleLabel.text = "移交房主给".localized
        Utils.showNoticeView(view: view)
        view.chatListVC?.selectIndex = { [weak self] (index, model) in
          guard let weakSelf = self else { return }
          if !model.id.isEmpty {
            // 移交房主
            weakSelf.chatSendMessage(type: .handover_master, payLoad: ["position": model.oderIndex])
          }
          Utils.hideNoticeView()
          weakSelf.dismissVoiceRoom()
          weakSelf.onLineView.onLine = 0
          NotificationCenter.default.post(name: NotifyName.kBackGroundModel.name(), object: false)
        }
        return
      }
    }
    Utils.hideNoticeView()
    dismissVoiceRoom()
    onLineView.onLine = 0
    NotificationCenter.default.post(name: NotifyName.kBackGroundModel.name(), object: false)
  }
  
  func makeSureLeave() {
    if canMiniModel {
      let mini = ChatActionStruct.init(image: UIImage.init(named: "chatRoom_miniState"), title: "小窗逛逛", tag: 0)
      let leave = ChatActionStruct.init(image: UIImage.init(named: "chatRoom_leaveRoom"), title: "退出房间",tag: 0)
      let leaveView = ChatRoomLeaveAlert.init(height: 220, title: "确认退出房间?", icons: [mini, leave])
      Utils.showNoticeView(view: leaveView)
      leaveView.selectBlock = { [weak self] index in
        guard let weakSelf = self else { return }
        switch index {
        case 0:
          Utils.hideNoticeView()
          weakSelf.dismissVoiceRoom()
          if weakSelf.dataSource[CurrentUser.shareInstance.currentPosition]?.state == "" {
            if weakSelf.dataSource[CurrentUser.shareInstance.currentPosition]?.speaking == true {
              weakSelf.chatSendMessage(type: GameMessageType.unspeak)
            }
            weakSelf.changeSound(true)
          }
        case 1:
          if weakSelf.isGaming {
            if CurrentUser.shareInstance.currentPosition <= chatRoomOnLineNumber {
              XBHHUD.showError("游戏中无法退出房间".localized)
              return
            }
          }
          weakSelf.masterLeave()
        default:
          debugPrint("let leaveView = ChatRoomLeaveAlert.init(height: 520")
          Utils.hideNoticeView()
        }
      }
    } else {
      let noticeView = ChatRoomLeaveRoom.showChatRoomLeaveRoom()
      Utils.showAlertView(view: noticeView)
      noticeView.selectBlock = { [weak self] state in
        guard let weakSelf = self else { return }
        switch state {
        case .makeSureButton:
          Utils.hideNoticeView()
          if weakSelf.isGaming {
            if CurrentUser.shareInstance.currentPosition <= chatRoomOnLineNumber {
              XBHHUD.showError("游戏中无法退出房间".localized)
              return
            }
          }
          if CurrentUser.shareInstance.currentPosition == 0 {
            DispatchQueue.main.async {
              weakSelf.masterLeave()
            }
            return
          }
          weakSelf.dismissVoiceRoom()
          weakSelf.onLineView.onLine = 0
          NotificationCenter.default.post(name: NotifyName.kBackGroundModel.name(), object: false)
        case .cancelButton:
          ()
        }
      }
    }
  }
  
  func dismissVoiceRoom() -> Void {
    SwiftConverter.shareInstance.sendMessagesToRN(json: JSON(["action":"LEAVE_AUDIO_ROOM"]))
    let url = URL.init(string: dataSource[0]?.avatar ?? "")
    dismissSelf?(url)
    dismiss(animated: true, completion: nil)
  }
  
  override func didReceiveMemoryWarning() {
    view.endEditing(true)
    super.didReceiveMemoryWarning()
  }  
}
