	

//
//  ChatVoiceRoom+SocketNotification.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/29.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import SDWebImage
import ObjectMapper
  
extension ChatVoiceRoomViewController {
  
  
  func registerNotification() -> Void {
    // 游戏服务器的断开
    NotificationCenter.default.rx.notification(Notification.Name.init(GameMessageType.reconnect.rawValue)).subscribe(onNext: { [weak self] (note) in
      self?.reconnect(notification: note)
    }).disposed(by: disposeBag)
    
    NotificationCenter.default.rx.notification(NotifyName.kBackGroundModel.name()).subscribe(onNext: {  [weak self] (note) in
      guard let rootNavi = messageDispatchManager.viewController as? UINavigationController, self == rootNavi.viewControllers.first else {
        return
      }
      Utils.runInMainThread { [weak self] in
        XBHHUD.showLoading()
        if let show = note.object as? Bool, show {
          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: { [weak self] in
            self?.miniButton.isHidden = true
            UIViewController.topViewController()?.present(messageDispatchManager.viewController!, animated: true, completion: nil)
            XBHHUD.hide()
          })
        } else {
          if CurrentUser.shareInstance.currentPosition == 0 {
            if let onNum = self?.onLineView.onLine, onNum > 1 {
              if self?.isGaming == true {
                XBHHUD.showError("游戏中无法退出房间".localized)
                return
              }
              if self?.miniButton.isHidden == false {
                self?.miniButton.isHidden = true
                UIViewController.topViewController()?.present(messageDispatchManager.viewController!, animated: true, completion: nil)
                return
              }
              self?.masterLeave()
              messageDispatchManager.close()
              XBHHUD.hide()
              return
            }
          }
          if CurrentUser.shareInstance.currentPosition <= chatRoomOnLineNumber {
            if self?.isGaming == true {
              XBHHUD.showError("游戏中无法退出房间".localized)
              return
            }
          }
          self?.onLineView.onLine = 0
          if self?.miniButton.isHidden == true {
            self?.dismiss(animated: false, completion: nil)
          } else {
            self?.miniButton.isHidden = true
          }
          self?.leave()
          messageDispatchManager.close()
          messageDispatchManager.resetRoomInfo()
          DispatchQueue.global().async {
            if let noteUserinfo = note.userInfo {
              NotificationCenter.default.post(name: NotifyName.kEnterToGameRoom.name(), object: nil, userInfo: noteUserinfo)
            } else {
              XBHHUD.hide()
            }
          }
        }
      }
    }).disposed(by: disposeBag)
  }
  
  func reconnect(notification: Notification) -> Void {
    DispatchQueue.main.async { [weak self] in
      let jsonData = notification.object as! JSON
      if jsonData.intValue != 1000 {
        // 重连失败
        Utils.hideNoticeView()
        if jsonData.intValue != 9999 {
          XBHHUD.showError("你已经掉线，请检查您的网络".localized)
        }
        self?.onLineView.onLine = 0
        self?.dismissVoiceRoom()
        self?.leave()
        NotificationCenter.default.post(name: NotifyName.kBackGroundModel.name(), object: false)
      }
    }
  }
  
  func unPerpareMethod(notification: Notification) -> Void {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = notification.object as? JSON
      if let position = jsonData?["position"].intValue {
        weakSelf.dataSource[position]?.prepared = false
        weakSelf.reloadUserState(position)
      }
    }
  }
  
  //处理返现消息
  func dealWithRebate(rebate:[String:JSON],toView:JSON?,gift_type:String) {
    var peer_id = ""
    var rebatevalu = 0
    if let peer = rebate["peer"]?.string {
      peer_id = peer
    }
    if let value = rebate["value"]?.int {
      rebatevalu = value
    }
    //发消息
    if CurrentUser.shareInstance.id != peer_id {
      if toView != nil {
         sendGiftMessages(data: toView!, type: gift_type, value: rebatevalu, valuetype:"")
      }
    }
  }
  
  //发送聊天消息
  func sendGiftMessages(data:JSON,type:String,value:Int,valuetype:String) {
    let name = data["name"].stringValue
    let ava = data["avatar"].stringValue
    let rec_id = data["id"].stringValue
    let rec_sex = data["sex"].stringValue
    chat?.createConversation(withName: name,
                             clientIds: [rec_id],
                             attributes: nil,
                             options: AVIMConversationOption.unique,
                             callback: { (conversation, error) in
                              
                              let sendMessage = OLGiftMessage.init(attibutes: [
                                "USER_SEX": "\(CurrentUser.shareInstance.sex)",
                                "USER_ICON": CurrentUser.shareInstance.avatar,
                                "USER_NAME": CurrentUser.shareInstance.name,
                                "USER_ID": CurrentUser.shareInstance.id,
                                GIFT_MSG_KEY_GITF_TYPE:type,
                                "GIFT_REBATE":"\(value)",
                                "APP": Config.app,
                                "CHAT_TIME": Utils.getCurrentTimeStamp()
                                ]);
                              
                              //此时也要发送通知，告诉RN更新
                              conversation?.send(sendMessage, callback: { (isSucceeded, error) in
                                if isSucceeded {
                                  //此时也要发送通知，告诉RN更新
                                  let dic = ["USER_NAME":name,"USER_SEX":rec_sex,"USER_ICON":ava,"MESSAGE_TYPE":"MESSAGE_TYPE_CHAT","APP": Config.app,"USER_ID":rec_id,"CHAT_TIME":String(Utils.getCurrentTimeStamp()),"CHAT_MESSAGE":NSLocalizedString("🎁赠送成功", comment: ""),"READ":true] as [String : Any]
                                  NotificationCenter.default.post(name: NotifyName.kSendMessToRN.name(), object: dic)
                                }
                              });
    });
  }
}
