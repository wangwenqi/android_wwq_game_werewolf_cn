//
//  ChatVoiceRoom+Dinting.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

// 谁是卧底
extension ChatVoiceRoomViewController {

  func layoutDintingView() -> Void {
    if CurrentUser.shareInstance.isGameing != .isNotAGame {
      let positon = currentUserInfo.oderIndex
      if positon == 0 {
        dintingSetView.frame = CGRect.init(x: view.width - 140, y: 90, width: 135, height: 25)
        view.addSubview(dintingSetView)
        dintingSetView.isHidden =  isGaming ? true : false
      } else if positon <= chatRoomOnLineNumber, positon > 0 {
        dintingSetView.isHidden = true
      } else {
        dintingSetView.isHidden = true
        return
      }
    } else {
      dintingSetView.isHidden = true
    }
  }
  
  func startGameView() -> Void {
    noticeView.isHidden = true
    dintingSetView.isHidden = false
  }
  
}
