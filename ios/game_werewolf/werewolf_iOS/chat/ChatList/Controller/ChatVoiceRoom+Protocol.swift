//
//  ChatVoiceRoom+Protocol.swift
//  game_werewolf
//
//  Created by xiaozao on 2018/1/19.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import UIKit

extension ChatVoiceRoomViewController: EnterChatRoomMessage {
  
  func action_handover_creator_result(_ rawData: [String:Any]) {
    let jsonData = JSON.init(rawData)
    let from = jsonData["from"].stringValue
    let to = jsonData["to"].stringValue
    guard CurrentUser.shareInstance.id == from || CurrentUser.shareInstance.id == to else { return }
    if CurrentUser.shareInstance.currentPosition != 0 {
      moreOrderButton.isHidden = true
    }
    let success = jsonData["succeed"].boolValue
    if  success {
      XBHHUD.showSuccess("交接成功".localized)
    } else {
      XBHHUD.showError("交接失败".localized)
    }
  }
  
  func action_purchase_room_result(_ rawData: [String : Any]) {
    let jsonData = JSON.init(rawData)
    let success = jsonData["succeed"].boolValue
    if  success {
      XBHHUD.showSuccess("购买成功".localized)
    } else {
      XBHHUD.showError("购买失败".localized)
    }
  }
  
  func action_update_config(_ rawData: [String : Any]) {
    DispatchQueue.main.async {  [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData) as? JSON
      if let purchase = jsonData?["config"]["purchase_room"].boolValue {
        weakSelf.canPurchaseroom = purchase
        if purchase {
          if CurrentUser.shareInstance.roomOwnerId == CurrentUser.shareInstance.id {
            self?.moreOrderButton.isHidden = false
          }
        }
      }
      if let ownerid = jsonData?["config"]["room_info"]["owner_id"].stringValue {
        CurrentUser.shareInstance.roomOwnerId = ownerid
      }
      if let ownerType = jsonData?["config"]["room_info"]["owner_type"].stringValue {
        CurrentUser.shareInstance.ownerType = ownerType
      }
      if CurrentUser.shareInstance.roomOwnerId == CurrentUser.shareInstance.id {
        weakSelf.moreOrderButton.isHidden = false
      } else {
        if CurrentUser.shareInstance.currentPosition != 0 {
          weakSelf.moreOrderButton.isHidden = true
        }
      }
    }
  }
  
  func action_join(_ rawData: [String : Any]) {
    DispatchQueue.main.async {  [weak self] in
      let jsonData = JSON.init(rawData) as? JSON
      if let position = jsonData?["position"].intValue {
        let model = ChatRoomUserModel()
        model.player = jsonData?["user"]
        if model.id == CurrentUser.shareInstance.id {
          CurrentUser.shareInstance.groupID = model.group_id
        }
        model.userRoomState = .roomOfOnMicro
        guard let weakSelf = self else { return }
        weakSelf.onLineView.onLine += 1
        weakSelf.noticeView.notice(name: model.name, state: .enterRoom)
        objc_sync_enter(weakSelf.dataSource)
        weakSelf.dataSource[position] = model
        objc_sync_exit(weakSelf.dataSource)
        weakSelf.onLineListView.array = weakSelf.dataSource
      }
    }
  }
  
  func action_leave(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData) as? JSON
      if let position = jsonData?["position"].intValue {
        if let remove = jsonData?["remove"].bool, remove {
          weakSelf.onLineView.onLine -= 1
          weakSelf.noticeView.notice(name: weakSelf.dataSource[position]?.name, state: .leaveRoom)
          objc_sync_enter(weakSelf.dataSource)
          weakSelf.dataSource[position] = ChatRoomUserModel()
          objc_sync_exit(weakSelf.dataSource)
        } else {
          weakSelf.dataSource[position]?.is_leaved = true
          weakSelf.dataSource[position]?.isDeath = rawData["dead"] as? Bool ?? false
        }
        weakSelf.reloadUserState(position)
        weakSelf.onLineListView.array = weakSelf.dataSource
      } else {
        weakSelf.leave()
      }
    }
  }
  
  func action_update_master(_ rawData: [String : Any]) {
    DispatchQueue.global().async { [weak self] in
      let jsonData = JSON.init(rawData) as? JSON
      let position = jsonData?["position"].stringValue
      DispatchQueue.main.async {
        guard let weakSelf = self else { return }
        if position == "-1" {
          weakSelf.dataSource[0]?.userRoomState = .roomOfEmpty
        }
      }
    }
  }
  
  func action_speak(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData) as? JSON
      if let positionStr = jsonData?["position"].stringValue,
        !positionStr.isEmpty,
        let position = jsonData?["position"].intValue {
        if position <= chatRoomOnLineNumber {
          weakSelf.dataSource[position]?.speaking = true
        }
        if position <= chatRoomOnLineNumber {
          weakSelf.reloadAllUserInfos()
        }
      }
    }
  }
  
  func action_unspeak(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData) as? JSON
      if let positionStr = jsonData?["position"].stringValue,
        !positionStr.isEmpty,
        let position = jsonData?["position"].intValue {
        if position <= chatRoomOnLineNumber {
          weakSelf.dataSource[position]?.speaking = false
        }
        if position <= chatRoomOnLineNumber {
          weakSelf.reloadAllUserInfos()
        }
      }
    }
  }
  
  func action_lock(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      if let dataSource = self?.dataSource {
        for (_, value) in dataSource {
          if value.userRoomState == .roomOfClose {
            value.userRoomState = .roomOfEmpty
          }
        }
      }
      let jsondata = JSON.init(rawData) as? JSON
      if let myArray = jsondata?["locked_positions"].arrayValue {
        for positon in myArray {
          self?.dataSource[positon.intValue]?.userRoomState = .roomOfClose
          self?.dataSource[positon.intValue]?.id = ""
        }
      }
      self?.reloadAllUserInfos()
    }
  }
  
  func action_kick_out(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      weakSelf.onLineView.onLine -= 1
      let jsonData = JSON.init(rawData) as? JSON
      if let position = jsonData?["position"].intValue {
        if position == self?.currentUserInfo.oderIndex {
          if position != 0 {
            XBHHUD.showError(NSLocalizedString("你被踢出了房间", comment: ""))
          }
          self?.onLineView.onLine = 0
          weakSelf.dismissVoiceRoom()
          weakSelf.leave()
          weakSelf.isGaming = false
          NotificationCenter.default.post(name: NotifyName.kBackGroundModel.name(), object: false)
        } else {
          if position != 0 {
            weakSelf.noticeView.notice(name: self?.dataSource[position]?.name, state: .kickout)
          }
          self?.dataSource[position] = ChatRoomUserModel()
          self?.reloadAllUserInfos()
        }
      }
    }
  }
  
  func action_chat(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      weakSelf.giftmanager.view = weakSelf.view
      let jsonData = JSON.init(rawData) as? JSON
      guard let position = jsonData?["position"].intValue else { return }
      guard let message = jsonData?["message"].stringValue else { return }
      if let giftData = jsonData?["gift"].dictionaryValue,
        let from = giftData["from"]?.intValue,
        let to = giftData["to"]?.intValue,
        let typeString = giftData["type"]?.stringValue {
        let fromView = weakSelf.giftFromToView(from)
        let toView = weakSelf.giftFromToView(to)
        let json = weakSelf.dataSource[to]?.player
        //有没有返利
        var rebateValue = -1
        if let rebate = giftData["rebate"]?.dictionary {
          //是自己送的再发送
          if from == CurrentUser.shareInstance.currentPosition  {
            weakSelf.dealWithRebate(rebate: rebate,toView:json,gift_type:typeString)
          }
          if let value = rebate["value"]?.int {
            rebateValue = value
          }
        }
        // let giftType = OLGiftType.changeOLGiftTypeToGiftType(type: typeString);
        let gift = Gifts(fromView: fromView, toView: toView, typeString: typeString, rebate:rebateValue)
        if to <= chatRoomOnLineNumber {
          weakSelf.giftmanager.add(gift)  // 接收礼物的人人是自己或者非麦上则显示动画
        }
        let model = self?.dataSource[position]
        model?.position = position
        self?.messageViewController.addGiftClearConversion(from: "\(position)", model: model,  to: to, giftType: typeString, messge: message);
      } else {
        let model = self?.dataSource[position]
        model?.position = position
        self?.messageViewController.addVoiceConversion(playerMessge: message, positionNumber:"\(position)", model: model)
      }
    }
  }
  
  func action_reconnect(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      let jsonData = JSON.init(rawData)
      if jsonData.intValue != 1000 {
        // 重连失败
        Utils.hideNoticeView()
        if jsonData.intValue != 9999 {
          XBHHUD.showError("你已经掉线，请检查您的网络".localized)
        }
        self?.onLineView.onLine = 0
        self?.dismissVoiceRoom()
        self?.leave()
        NotificationCenter.default.post(name: NotifyName.kBackGroundModel.name(), object: false)
      }
    }
  }
  
  func action_restore_room(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData)
      weakSelf.data = jsonData
      weakSelf.asslysisData("room_info")
    }
  }
  
  func action_system_msg(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON,
        let content = jsonData["content"].string {
        if let type = jsonData["type"].string {
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            switch type {
            case "dialog":
              let view = systemDialog.view(title: content)
              view.liveTime = jsonData["duration"].intValue / 1000
              Utils.showNoticeView(view: view)
            case "toast":
              XBHHUD.showSuccess(content)
            case "text":
              self?.messageViewController.addNoticeConversion(type: .system, messge: content, colorType: ChatNoticeView.ChatRoomUserState.hide)
            default:
              self?.messageViewController.addNoticeConversion(type: .system, messge: content, colorType: ChatNoticeView.ChatRoomUserState.hide)
            }
          })
        }else{
          self?.messageViewController.addNoticeConversion(type: .system, messge: content, colorType: ChatNoticeView.ChatRoomUserState.hide)
        }
      }
    }
  }
  
  func action_olive_changed(_ rawData: [String : Any]) {
    
  }
  
  func action_change_password(_ rawData: [String : Any]) {
    Utils.runInMainThread {
      let jsonData = JSON.init(rawData) as? JSON
      CurrentUser.shareInstance.currentRoomPassword = jsonData?["password"].stringValue ?? ""
    }
  }
  
  func action_add_friend(_ rawData: [String : Any]) {
    //添加好友請求
    DispatchQueue.main.async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON,
        let user_id = jsonData["user_id"].string,
        let name = jsonData["name"].string {
        
        guard let weakSelf = self else { return }
        if weakSelf.frendListSet.contains(user_id) { return }
        weakSelf.frendListSet.insert(user_id)
        
        var avater = jsonData["avatar"].string
        if avater == nil {
          avater = ""
        }
        var level = "LV. 0"
        let popular = jsonData["popular"].stringValue
        if !popular.isEmpty {
          level = "LV." + jsonData["game"]["level"].stringValue + "    " +  popular
        } else {
          level = "LV." + jsonData["game"]["level"].stringValue
        }
        let request = ChatRoomAddFriendRequest.init(id: user_id, avaImage: avater!, name: name, level: level, sex: "")
        self?.showAddFrendView(request)
      }
    }
  }
  
  func action_send_card(_ rawData: [String : Any]) {
    Utils.runInMainThread {
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SENDCARD"), object: nil)
    }
  }
  
  func action_send_gift(_ rawData: [String : Any]) {
    
  }
  
  func action_prepare(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData) as? JSON
      if let position = jsonData?["position"].intValue {
        weakSelf.dataSource[position]?.prepared = true
        weakSelf.reloadUserState(position)
      }
    }
  }
  
  func action_start(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      weakSelf.myWord = ""
      CurrentUser.shareInstance.messageShow = false
      CurrentUser.shareInstance.isGameing = .isStartState
      weakSelf.isGaming = true
      if !CurrentUser.shareInstance.can_cut_speaker {
        if CurrentUser.shareInstance.currentPosition != 0 {
          weakSelf.tooBar.isOnMir = false
        }
      }
      weakSelf.dintingSetView.isHidden = true
      weakSelf.view.addSubview(weakSelf.dintingNotice)
      weakSelf.dintingNotice.isHidden = false
      if let jsonData = JSON.init(rawData) as? JSON {
        weakSelf.dintingNotice.setDuring(jsonData["duration"].intValue)
        if weakSelf.currentUserInfo.oderIndex == 0 {
          weakSelf.dintingNotice.setUserType(type: .master)
        } else if weakSelf.currentUserInfo.oderIndex > 0 && weakSelf.currentUserInfo.oderIndex < 9  {
          weakSelf.dintingNotice.setUserType(type: .player)
          weakSelf.dintingNotice.setNoticeType(type: .unKnown, text: "游戏正在进行".localized)
        } else {
          weakSelf.dintingNotice.setUserType(type: .watcher)
          weakSelf.dintingNotice.setNoticeType(type: .unKnown, text: "游戏正在进行".localized)
        }
      }
      weakSelf.reloadAllUserInfos()
    }
  }
  
  func action_assigned_role(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData) as? JSON
      weakSelf.myWord = jsonData?["role"].stringValue ?? ""
      weakSelf.dintingNotice.setNoticeType(type: .desState, text: weakSelf.myWord)
      weakSelf.dintingNotice.setDuring(jsonData?["duration"].intValue)
    }
  }
  
  func action_vote(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      for i in 1...8 {
        weakSelf.giftViewsArray[i].cutDown.end()
        weakSelf.reloadUserState(i)
      }
      weakSelf.dintingNotice.setHelpWorld(text: "开始投票".localized)
      weakSelf.gameCloseOrOpen(state: false)
      if let jsonData = JSON.init(rawData) as? JSON {
        let duration = jsonData["duration"].intValue
        if duration != 0 {
          weakSelf.dintingNotice.setNoticeType(type: .voteState, text: "正在进行投票".localized)
          weakSelf.dintingNotice.setDuring(duration)
        }
        if !CurrentUser.shareInstance.isDeath {
          let alives = jsonData["alives"].arrayValue
          if !alives.isEmpty {
            for alive in alives {
              weakSelf.dataSource[alive.intValue]?.isVoted = true
              weakSelf.dataSource[alive.intValue]?.votes = [Int]()
            }
          }
        }
        weakSelf.reloadAllUserInfos()
      }
    }
  }
  
  func action_vote_result(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      weakSelf.dintingNotice.setHelpWorld(text: "")
      weakSelf.dintingNotice.setNoticeType(type: .desState, text: weakSelf.myWord)
      CurrentUser.shareInstance.isGameing = .isVotedState
      if let jsonData =  JSON.init(rawData) as? JSON  {
        weakSelf.voteResultShow(jsonData)
      }
    }
  }
  
  func action_speech(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData) as? JSON
      CurrentUser.shareInstance.isGameing = .isSpeechState
      if let position = jsonData?["position"].intValue {
        weakSelf.dintingNotice.setHelpWorld(text: "\(position)" + "号发言".localized)
        let durtime = jsonData?["duration"].intValue
        weakSelf.dintingNotice.setDuring(durtime)
        weakSelf.dintingNotice.setNoticeType(type: .desState, text: weakSelf.myWord)
        for i in 0...8 {
          if i == position {
            if let durtime = durtime {
              weakSelf.giftViewsArray[position].setProgressLayer(time: durtime / 1000)
            }
          } else {
            weakSelf.giftViewsArray[i].cutDown.end()
          }
        }
        guard CurrentUser.shareInstance.currentPosition != 0 else { return }
        if CurrentUser.shareInstance.currentPosition == position {
          weakSelf.gameCloseOrOpen(state: true)
        } else {
          weakSelf.gameCloseOrOpen(state: false)
        }
      }
    }
  }
  
  func action_game_over(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      CurrentUser.shareInstance.messageShow = true
      guard let weakSelf = self else { return }
      if CurrentUser.shareInstance.currentPosition < 9,
        CurrentUser.shareInstance.currentPosition > 0 {
        weakSelf.gameCloseOrOpen(state: true)
      }
      if let jsonData = JSON.init(rawData) as? JSON {
        weakSelf.showGameoverView(jsonData)
      }
      weakSelf.resetGame()
    }
  }
  
  func action_death_info(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData) as? JSON
      if let deathArr = jsonData?["death_info"].arrayValue {
        for users in deathArr {
          let killed = users["killed"].intValue
          let role = users["role"].stringValue
          weakSelf.dataSource[killed]?.isDeath = true
          weakSelf.reloadUserState(killed)
          let roleStr = role == "people" ? "好人".localized : "卧底".localized
          let infoStr = "\(killed)" + "号死亡, 身份是".localized + roleStr
          weakSelf.messageViewController.addDintingConversion(mType: ChatRoomDintingCell.DintingState.deathInfo, message: infoStr)
          weakSelf.dataSource[killed]?.isDeath = true
          weakSelf.dataSource[killed]?.votes = [Int]()
          weakSelf.reloadUserState(killed)
          if killed == weakSelf.currentUserInfo.oderIndex {
            CurrentUser.shareInstance.isDeath = true
          }
        }
      }
    }
  }
  
  func action_update_title(_ rawData: [String : Any]) {
    DispatchQueue.global().async {
      let jsonData = JSON.init(rawData) as? JSON
      if let title = jsonData?["title"].stringValue,
      let type = jsonData?["type"].stringValue,
      let theme = jsonData?["theme"]["background"].stringValue {
        DispatchQueue.main.async { [weak self] in
          if !title.isEmpty {
            self?.roomNameLabel.text = title
            self?.titleLabel.titleText = title
            self?.getTypeIndex(type, theme)
          }
        }
      }
    }
  }
  
  func action_up_seat(_ rawData: [String : Any]) {
    DispatchQueue.global().async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON {
        let userId = jsonData["user_id"].stringValue                    // 上麦Uid
        let afterPosition = jsonData["dest_position"].stringValue       // 上麦的位置
        let afterInt = jsonData["dest_position"].intValue
        DispatchQueue.main.async {
          guard let weakSelf = self else { return }
          let currentPosition = weakSelf.checkUserIdBackPosition(userId)  // 当前位置
          if let currentPosition = currentPosition, !afterPosition.isEmpty {
            // 上座/下座动态
            if afterInt > 8 {
              weakSelf.dataSource[currentPosition]?.state = ""
              weakSelf.dataSource[currentPosition]?.speaking = false
            }
            if afterInt == 0, CurrentUser.shareInstance.isGameing != .isNotAGame {
              weakSelf.dataSource[0]?.masterReady = false
              CurrentUser.shareInstance.isGameing = .isGameOverStates
              weakSelf.reloadUserState(0)
            }
            weakSelf.noticeView.notice(name: weakSelf.dataSource[currentPosition]?.name, state: afterInt < 9 ? .upSeat : .downSeat)
            weakSelf.swapObjc(currentPosition, afterInt)
            if currentPosition == weakSelf.currentUserInfo.oderIndex {           // 如果换位置的是自己
              weakSelf.currentUserInfo.oderIndex = afterInt
              CurrentUser.shareInstance.currentPosition = afterInt
              weakSelf.closeOrOpenMir()
            }
          }
          self?.reloadAllUserInfos()
        }
      }
    }
  }
  
  func action_down_seat(_ rawData: [String : Any]) {
    self.action_up_seat(rawData)
  }
  
  func action_force_seat(_ rawData: [String : Any]) {
    DispatchQueue.global().async { [weak self] in
      let jsonData = JSON.init(rawData)
      let userId = jsonData["user_id"].stringValue                    // 上麦Uid
      let afterPosition = jsonData["dest_position"].stringValue       // 上麦的位置
      let afterInt = jsonData["dest_position"].intValue
      DispatchQueue.main.async {
        guard let weakSelf = self else { return }
        let currentPosition = weakSelf.checkUserIdBackPosition(userId)     // 当前位置
        if let currentPosition = currentPosition, !afterPosition.isEmpty {
          weakSelf.noticeView.notice(name: weakSelf.dataSource[currentPosition]?.name, state: .forceSeat)
          weakSelf.swapObjc(currentPosition, afterInt)
          if currentPosition == weakSelf.currentUserInfo.oderIndex {           // 如果换位置的是自己
            weakSelf.currentUserInfo.oderIndex = afterInt
            CurrentUser.shareInstance.currentPosition = afterInt
            ChatRoomForceSeat.showChatRoomForceSeat()
            weakSelf.closeOrOpenMir()
          }
          if afterInt < 9 {
            weakSelf.dataSource[afterInt]?.userRoomState = .roomOfOnMicro
          } else {
            self?.dataSource[afterInt]?.state = ""
          }
        }
        weakSelf.reloadAllUserInfos()
      }
    }
  }
  
  func action_handover_master(_ rawData: [String : Any]) {
    DispatchQueue.global().async {
      let jsonData = JSON.init(rawData)
      let title = jsonData["position"].intValue
      self.swapObjc(0, title)
      self.reloadUserState(0)
    }
  }
  
  func action_change_user_state(_ rawData: [String : Any]) {
    // 改变用户麦上状态
    DispatchQueue.main.async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON {
        let position = jsonData["position"].intValue
        let state = jsonData["state"].stringValue
        self?.dataSource[position]?.state = state
        if position == self?.currentUserInfo.oderIndex {
          self?.closeOrOpenMir()
        } else {
          if state == "free" {
            self?.changeSound(true)
          }
        }
        if state == "free" {
          var name = "\(position)号"
          if position == 0 { name = "房主" }
          self?.messageViewController.addNoticeConversion(type: MessageType.system, messge: "房主已开启自由模式, ".localized + "\(name)" + "无需按麦即可发言, 如需更换请房主重新选择 ", colorType:.freeModel)
        }
        self?.reloadAllUserInfos()
      }
    }
  }
  
  func action_show_emoticon(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON {
        let mark = jsonData["mark"].stringValue
        let position = jsonData["position"].intValue
        let url = jsonData["url"].stringValue
        self?.showGifAnimation(position, ChatGifModel(url: url, mark: mark, thumbnail: "", res_url: "", res: "", sound: "", stay_time: 0))
      }
    }
  }
  
  func action_like_room(_ rawData: [String:Any]) {
    DispatchQueue.main.async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON {
        let position = jsonData["position"].intValue
        let count = jsonData["count"].stringValue
        self?.likeLabel.text = count
        if let name = self?.dataSource[position]?.name, !name.isEmpty {
          self?.messageViewController.addNoticeConversion(type: MessageType.system, messge: name + "为房间点赞".localized, colorType: ChatNoticeView.ChatRoomUserState.freeModel)
        }
        guard let weakSelf = self else { return }
        let heart = HeartView(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
        weakSelf.view.addSubview(heart)
        heart.center = weakSelf.likeButton.center
        heart.animateInView(weakSelf.view)
      }
    }
  }
  
  func action_show_game_emoticon(_ rawData: [String:Any]) {
    DispatchQueue.main.async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON {
        if let markType = GamEmotice(rawValue: jsonData["mark"].stringValue) {
          if markType == .applause {
            if !jsonData["data"]["code"].stringValue.isEmpty {
              XBHHUD.showTextOnly(text: jsonData["data"]["msg"].stringValue)
              return
            }
          }
          let position = jsonData["position"].intValue
          let res = jsonData["data"]["res"].stringValue
          let stay_time = jsonData["data"]["stay_time"].intValue
          let res_url = jsonData["data"]["res_url"].stringValue
          let sound = jsonData["data"]["sound"].stringValue
          self?.showGifAnimation(position, ChatGifModel(url: "", mark: markType.rawValue, thumbnail: "", res_url: res_url, res: res, sound: sound, stay_time: Double(stay_time)))
          if markType == .points {
            let scoreInt = Int(res) ?? 0
            let model = self?.dataSource[position]
            model?.position = position
            self?.messageViewController.addScoreConversion(score: scoreInt, positionNumber: "\(position)", model: model)
          }
        }
      }
    }
  }
  
  func action_block_user_list(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON {
        let array = jsonData["users"].arrayValue
        var listArray = [ChatBlackModel]()
        for json in array {
          let name = json["name"].stringValue
          let id = json["id"].stringValue
          let image = json["image"].stringValue
          let sex = json["sex"].intValue
          let model = ChatBlackModel.init(name: name, id: id, image: image, sex: sex)
          listArray.append(model)
        }
        self?.blackList.dataSource = listArray
        self?.blackList.tableview.reloadData()
      }
    }
  }
  
  func action_unprepare(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData) as? JSON
      if let position = jsonData?["position"].intValue {
        weakSelf.dataSource[position]?.prepared = false
        weakSelf.reloadUserState(position)
      }
    }
  }
  
  func action_uc_get_words_result(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData) as? JSON
      weakSelf.dintingSetView.dintingSetView.textField1.text = jsonData?["first"].stringValue
      weakSelf.dintingSetView.dintingSetView.textField2.text = jsonData?["second"].stringValue
    }
  }
  
  func action_uc_guess_word(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let jsonData = JSON.init(rawData) as? JSON
      let time = jsonData?["duration"].intValue
      weakSelf.dintingNotice.setDuring(time)
      if let position = jsonData?["position"].intValue, position == weakSelf.currentUserInfo.oderIndex  {
        let guess = ChatDintingGuessWord.init(frame: .zero, time: (time ?? 0) / 1000)
        Utils.showAlertView(view: guess)
      } else {
        weakSelf.dintingNotice.setNoticeType(type: .interState, text: "卧底正在猜词")
      }
    }
  }
  
  func action_uc_guess_word_result(_ rawData: [String : Any]) {
    
  }
  
  func action_uc_start_config(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      weakSelf.dataSource[0]?.masterReady = false
      CurrentUser.shareInstance.isGameing = .isGameOverStates
      weakSelf.reloadUserState(0)
    }
  }
  
  func action_uc_stop_config(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      weakSelf.dataSource[0]?.masterReady = true
      CurrentUser.shareInstance.isGameing = .isUpdateConfig
      weakSelf.reloadUserState(0)
    }
  }
  
  func action_uc_update_config(_ rawData: [String : Any]) {
    DispatchQueue.main.async { // [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON {
        CurrentUser.shareInstance.can_cut_speaker = jsonData["config"]["can_cut_speaker"].boolValue
      }
    }
  }
  
  func action_disconnected(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON {
        let position = jsonData["position"].intValue
        let remove = jsonData["remove"].boolValue
        self?.dataSource[position]?.is_leaved = true
        self?.dataSource[position]?.isDeath = remove
        self?.reloadUserState(position)
      }
    }
  }
  
  func actino_connected(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON {
        let position = jsonData["position"].intValue
        let remove = jsonData["remove"].boolValue
        self?.dataSource[position]?.is_leaved = false
        self?.dataSource[position]?.isDeath = remove
        self?.reloadUserState(position)
      }
    }
  }
  
  func action_request_free_mode(_ rawData: [String : Any]) {
    DispatchQueue.main.async {  [weak self] in
      guard let weakSelf = self else { return }
      if let jsonData = JSON.init(rawData) as? JSON {
        let position = jsonData["position"].intValue
        if position == weakSelf.currentUserInfo.oderIndex {
          Utils.hideNoticeView()
          guard CurrentUser.shareInstance.currentPosition != 0 else {
            self?.chatSendMessage(type: .accept_free_mode)
            return
          }
          let duration = jsonData["duration"].intValue / 1000
          weakSelf.roomOpenView.toSetTimer(duration)
          weakSelf.roomOpenView.selectBlock = { allow in
            weakSelf.chatSendMessage(type: allow ? .accept_free_mode : .reject_free_mode)
          }
          Utils.showAlertView(view: weakSelf.roomOpenView)
        } else {
          guard !weakSelf.roomOpenView.isShow else {
            Utils.hideNoticeView()
            return
          }
        }
      }
    }
  }
  
  func action_accept_free_mode(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON {
        let position = jsonData["position"].stringValue
        if !position.isEmpty {
          let intPos = jsonData["position"].intValue
          self?.dataSource[intPos]?.state = "free"
          if intPos == self?.currentUserInfo.oderIndex {
            self?.closeOrOpenMir()
          } else {
            self?.changeSound(true)
          }
          self?.reloadAllUserInfos()
          guard intPos != 0 else {
            self?.messageViewController.addNoticeConversion(type: MessageType.system, messge: "房主为自己打开了自由模式".localized, colorType: ChatNoticeView.ChatRoomUserState.freeModel)
            return
          }
          let name = "\(intPos)号"
          self?.messageViewController.addNoticeConversion(type: MessageType.system, messge: name + "接受了自由模式".localized, colorType: ChatNoticeView.ChatRoomUserState.freeModel)
        }
      }
    }
  }
  
  func action_reject_free_mode(_ rawData: [String : Any]) {
    DispatchQueue.main.async { [weak self] in
      if let jsonData = JSON.init(rawData) as? JSON {
        let position = jsonData["position"].stringValue
        if !position.isEmpty {
          let intPos = jsonData["position"].intValue
          let name = "\(intPos)号"
          self?.messageViewController.addNoticeConversion(type: MessageType.system, messge: name + "拒绝了自由模式".localized, colorType: ChatNoticeView.ChatRoomUserState.freeModel)
          self?.reloadAllUserInfos()
        }
      }
    }
  }
  
  func action_connect_info(_ rawData: [String : Any]) {
    let data = JSON.init(rawData)
    
    if LocalSaveVar.connectID == "" {
      LocalSaveVar.connectID = data["connect_id"].stringValue
    }
    
    if(messageDispatchManager.isReconnecting()) {
      messageDispatchManager.sendMessage(type: .reconnect, payLoad: ["connect_id": LocalSaveVar.connectID]);
      messageDispatchManager.setReconnectingStatus(false);
    } else {
 
      if  CurrentUser.shareInstance.from != "" {
        messageDispatchManager.sendMessage(type: .enterRoom, payLoad: [
          "room_id": CurrentUser.shareInstance.currentRoomID,
          "password": CurrentUser.shareInstance.currentRoomPassword,
          "type"    : CurrentUser.shareInstance.currentRoomType,
          "token": CurrentUser.shareInstance.token,
          "extra" : [
            "pt": "\(Config.platform)",
            "v": "\(Config.buildVersion)",
            "b": "\(Config.appStoreVersion)",
            "sv": "\(Config.serverVersion)",
            "tz": "\(Config.currentTimeZoneGMT)",
            "lg": "\(Config.systemLanage)"
          ],
          "user": [
            "id": CurrentUser.shareInstance.id,
            "name": CurrentUser.shareInstance.name,
            "avatar": CurrentUser.shareInstance.avatar,
            "level": CurrentUser.shareInstance.level,
            "experience": CurrentUser.shareInstance.experience
          ],
          "from":"export"
          ])
        
      }else{
        messageDispatchManager.sendMessage(type: .enterRoom, payLoad: [
          "room_id": CurrentUser.shareInstance.currentRoomID,
          "password": CurrentUser.shareInstance.currentRoomPassword,
          "type"    : CurrentUser.shareInstance.currentRoomType,
          "token": CurrentUser.shareInstance.token,
          "extra" : [
            "pt": "\(Config.platform)",
            "v": "\(Config.buildVersion)",
            "b": "\(Config.appStoreVersion)",
            "sv": "\(Config.serverVersion)",
            "tz": "\(Config.currentTimeZoneGMT)",
            "lg": "\(Config.systemLanage)"
          ],
          "user": [
            "id": CurrentUser.shareInstance.id,
            "name": CurrentUser.shareInstance.name,
            "avatar": CurrentUser.shareInstance.avatar,
            "level": CurrentUser.shareInstance.level,
            "experience": CurrentUser.shareInstance.experience
          ]
          ])
      }
    }
  }
  
  func action_enter(_ rawData: [String:Any]) {
    Utils.runInMainThread { [weak self] in
      if CurrentUser.shareInstance.currentRoomType == "audio" {
        XBHHUD.hide()
        // ChatVoiceRoomViewController // 语音房间
        let jsonData = JSON.init(rawData)
        if jsonData.isEmpty {
          return
        }
        if let rootVC = messageDispatchManager.viewController {
          messageDispatchManager.socketManager.currentRetryTime = messageDispatchManager.MAX_RETRY_TIMES
          self?.data = jsonData
          CurrentUser.shareInstance.currentRoomID = jsonData["room"]["room_id"].stringValue
          CurrentUser.shareInstance.realCurrentRoomID = jsonData["room"]["room_id"].stringValue
          // 真正进入房间后进行重连次数修改
          self?.dismissSelf = { [weak self] url in
            if self?.onLineView.onLine == 0 {
              self?.miniButton.isHidden = true
            } else {
              self?.miniButton.isHidden = false
            }
            if let url = url {
              self?.miniButton.sd_setBackgroundImage(with:  url, for: UIControlState.normal, placeholderImage: UIImage.init(named: "chatRoom_cancel"))
            } else {
              self?.miniButton.setBackgroundImage(UIImage.init(named: "chatRoom_cancel"), for: UIControlState.normal)
            }
            UIViewController.topWindow()?.bringSubview(toFront: (self?.miniButton)!)
          }
          rootVC.modalTransitionStyle = .flipHorizontal
          self?.asslysisData("room")
          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: { //[weak self] in
            UIViewController.topViewController()?.present(rootVC, animated: true, completion: nil)
          })
        }
      }
    }
  }
  
  
  
  
}
