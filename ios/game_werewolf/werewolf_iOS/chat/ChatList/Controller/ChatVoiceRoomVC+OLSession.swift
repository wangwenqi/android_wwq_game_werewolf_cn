//
//  ChatVoiceRoomVC+OLSession.swift
//  game_werewolf
//
//  Created by Tony on 2017/9/2.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import orangeLab_iOS
// 这里面都是socket代理
extension ChatVoiceRoomViewController: OLSessionDelegate {
  
  // 对外开始进行连接
  internal func startConnect(_ roomId:String, config:JSON) {
    DispatchQueue.main.async {
      XBHHUD.showTextOnlyTop(text: "正在连接语音服务器".localized)
    }
    var sendConfig:[String:Any] = [
      "connect_room_type": "\(config["type"].int ?? 4)",
      "node_server_host": config["media_server"].stringValue,
      "turn_server_conf": [:]
    ];
    
    // 不去做检测
    //    if let standbyRelayConf = config["standby_relay"].dictionaryObject,
    //      var turnServerConf = sendConfig["turn_server_conf"] as? [String:Any] {
    //      turnServerConf.updateValue(standbyRelayConf, forKey: "standby_relay");
    //      sendConfig.updateValue(turnServerConf, forKey: "turn_server_conf");
    //    }
    
    if let inTurnServers = config["turn_servers"].arrayObject,
      inTurnServers.count > 0,
      let inTurnServer = inTurnServers[0] as? [String:Any],
      var turnServerConf = sendConfig["turn_server_conf"] as? [String:Any] {
      turnServerConf.updateValue(inTurnServer, forKey: "turn_server");
      sendConfig.updateValue(turnServerConf, forKey: "turn_server_conf");
    }
    
    //    sendConfig["turn_server_conf"] = false;
    
    let user = OLConnectorInfos();
    user.name = CurrentUser.shareInstance.name;
    user.uid  = "\(CurrentUser.shareInstance.id)";
    
    print("------sendConfig:\(sendConfig)");
    startConnect(roomId, withUser: user, config:sendConfig);
  }
  
  internal func disConnect() {
    OLSession.endSetUp()
    publisher?.clear();
    session?.disConnect();
    publisher = nil;
    session = nil;
  }
  
  internal func muteAll() {
    session?.sendAction("muteAll", data: ["":"" as AnyObject]);
  }
  
  internal func setMute(mute: Bool) {
    changeSound(mute)
  }
  
  private func startConnect(_ roomId:String,withUser user:OLConnectorInfos, config:[String:Any]) {
    
    olConfig = config;
    onLineView.roomId = roomId;
    connectUser = user;
    
    OLSession.beginSetUp()
    
    NotificationCenter.default.rx.notification(NSNotification.Name(rawValue: kMeetingMediaErrorNotification)).subscribe(onNext: { [weak self] (note) in
      self?.handleError(notifiction: note)
    }).disposed(by: disposeBag)
    
    session = OLSession(roomId: roomId, userInfo: connectUser, config: olConfig, withDelegate: self, embedToView: view);
    
    session?.startConnect([:]);
  }
  
  func handleError(notifiction:Notification) {
    if let info = notifiction.userInfo as? [String: Any],
      let orginError = info["orginError"] as? NSError {
      if (orginError.code == 111
        || orginError.code == 113
        || orginError.code == 114) {
        
        session?.restartSocketIO();
        
      }
    }
  }
  
  
  public func olSession(_ session: orangeLab_iOS.OLSession, publisherInited publisher: orangeLab_iOS.OLPublisher) {
    self.publisher = publisher;
  }
  
  //自己成功进入
  public func olSession(_ session: orangeLab_iOS.OLSession, userDidComeIn subscribes: [orangeLab_iOS.OLSubscribe]) {
    session.changeSoundType(true);
    Wifi_State.image = UIImage(named: "wifi_great-white")
    XBHHUD.hide()
  }
  
  //有用户将要进入
  public func olSession(_ session: orangeLab_iOS.OLSession, userWillComming userInfo: orangeLab_iOS.OLConnectorInfos) {
    print("userInfo --------------------- \(userInfo.name) \(userInfo.uuid)");
  }
  
  //有用户离开
  public func olSessionConnectorLeave(_ connector: orangeLab_iOS.OLConnectorInfos) {
    
  }
  
  //当自己的状态发生改变时
  public func olSession(_ session: orangeLab_iOS.OLSession, statedChange changeData: [String : Any]) {
    DispatchQueue.global().async {
      if let mute = changeData["mute"] as? Bool {
        session.changeSoundType(mute)
        //改变自己的按钮状态
      }
    }
  }
  
  //有用户的状态发生改变
  func olSessionUserStatusChanged(_ session: OLSession, subscriber: OLSubscribe, changeData: [String : Any]) {
    
  }
  
  func olSessionStatsReport(_ session: OLSession, report: String) {
    //    print(report)
  }
  
  func olSessionIOError(data: [Any]) {
    Wifi_State.image = UIImage(named: "wifi_great-shutdown")
    publisher?.clear();
    session?.disConnect();
    session = OLSession(roomId: onLineView.roomId, userInfo: connectUser, config: olConfig, withDelegate: self, embedToView: view);
    session?.startConnect([:]);
  }
  
  internal func errorNotification(notification: Notification) {
    print("info = \(notification.userInfo)");
  }
  
}
