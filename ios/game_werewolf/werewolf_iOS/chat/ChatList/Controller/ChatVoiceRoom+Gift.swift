//
//  ChatVoiceRoom+Gift.swift
//  game_werewolf
//
//  Created by Tony on 2017/9/5.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import YYWebImage
import RxSwift
import RxCocoa

extension ChatVoiceRoomViewController :giftSend {
  func sendGift(typeString: giftType, to: PlayerView?) {
    
  }

  func sendGift(type: giftType, form: Int, to: Int) {
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      let olGift = OLGiftType.changeGiftTypeToOLGiftType(type: type)
      let ticket = CurrentUser.shareInstance.ticket ?? ""
      let local = "赠送给".localized
      CurrentUser.shareInstance.ticket = nil
      let fromName = weakSelf.dataSource[form]?.name
      var toName = weakSelf.dataSource[to]?.name
      if let toName = toName, toName.length > 1 {}
      else { toName = weakSelf.giftmanager.toName }
      weakSelf.chatSendMessage(type: .chat, payLoad:
        [
          "position": weakSelf.currentUserInfo.oderIndex,
          "message": "『\(fromName ?? "")』" + local + "『\(toName ?? "")』" + "\(OLGiftType.getGiftDescription(type: olGift.rawValue))",
          "gift": [
            "from": form,
            "to": to,
            "type": olGift.rawValue,
            "ticket": ticket
          ],
          ])
    }
  }
  func sendGift(type: giftType, to: PlayerView?) {
  }
  
}

extension ChatVoiceRoomViewController {

  enum GifFileNameURL: String {
    case Gif_爱你
    case Gif_白眼
    case Gif_大笑
    case Gif_鬼脸
    case Gif_流汗
    case Gif_流泪
    case Gif_色眯眯
    case Gif_生气
    case Gif_偷笑
    case Gif_吐
    case Gif_无语
    case Gif_羞羞
    case Gif_dice        // 骰子
    case Gif_light       // 亮灯
    case Gif_hand_up     // 举手
    case Gif_finger_guessing  // 猜拳
    case Gif_points      // 打分
    case Gif_octopus     // 章鱼机
    case Gif_coins       // 抛硬币
    case Gif_mic         // 抽麦序
    case Gif_gifts       // 求礼物
    case Gif_applause    // 故障
    case UnKnown
    
    func GifFile() -> YYImage? {
      return YYImage(named: "\(rawValue)" + "_dynamic")
    }
    
    func imageFile() -> UIImage? {
      return UIImage(named: "\(rawValue)")
    }
    
    func textName() -> String {
      switch self {
      case .Gif_爱你:
        return "爱你"
      case .Gif_白眼:
        return "白眼"
      case .Gif_大笑:
        return "大笑"
      case .Gif_鬼脸:
        return "鬼脸"
      case .Gif_流汗:
        return "流汗"
      case .Gif_流泪:
        return "流泪"
      case .Gif_色眯眯:
        return "色咪咪"
      case .Gif_生气:
        return "生气"
      case .Gif_偷笑:
        return "偷笑"
      case .Gif_吐:
        return "吐"
      case .Gif_无语:
        return "无语"
      case .Gif_羞羞:
        return "羞羞"
      case .Gif_dice:
        return "骰子"
      case .Gif_light:
        return "亮灯"
      case .Gif_hand_up:
        return "举手"
      case .Gif_finger_guessing:
        return "猜拳"
      case .Gif_points:
        return "打分"
      case .Gif_octopus:
        return "章鱼机"
      case .Gif_coins:
        return "抛硬币"
      case .Gif_mic:
        return "抽麦序"
      case .Gif_gifts:
        return "求礼物"
      case .Gif_applause:
        return "鼓掌"
      default:
        return ""
      }
    }
    
  }
  
  
  func showGifAnimation(_ position: Int, _ model: ChatGifModel) -> Void {
    if position <= chatRoomOnLineNumber {
      let chatView: ChatRoomUserView = giftViewsArray[position]
      chatView.appendModel(model)
    }
  }
  
  
  // 用户操作
  func toolBarNoticeAction() -> Void {
    noticeView.noticeUpdate = { [weak self] (width, height, state) in
      switch state {
      case .enterRoom:
        UIView.beginAnimations("noticeAnimation", context: nil)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationCurve(.easeIn)
        self?.noticeView.frame = CGRect(x: Screen.width - width, y: 75, width: width, height: height)
        UIView.commitAnimations()
        guard CurrentUser.shareInstance.messageShow else {
          return
        }
      case .hide:
        UIView.beginAnimations("noticeAnimation", context: nil)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationCurve(.easeIn)
        self?.noticeView.frame = CGRect(x: Screen.width - width, y: 75, width: width, height: height)
        UIView.commitAnimations()
        return
      case .leaveRoom:
        guard CurrentUser.shareInstance.messageShow else {
          return
        }
      default:
        break
      }
      
      if let name = self?.noticeView.nameLabel.text,
         let info = self?.noticeView.infoLabel.text {
        Utils.runInMainThread { [weak self] in
          self?.messageViewController.addNoticeConversion(type: .system, messge: name + info, colorType: state)
        }
      }
    }
    
    tooBar.speechBlock = { [weak self] isShow in
      DispatchQueue.global().async { [weak self] in
        var trueShow = isShow
        guard let weakSelf = self else { return }
        if weakSelf.currentUserInfo.oderIndex > 8 { trueShow = false }
        weakSelf.chatSendMessage(type: trueShow ? .speak : .unspeak)
        weakSelf.changeSound(!trueShow)
      }
    }
    
    tooBar.inputTextBlock = { [weak self] (isShow, height) in
//      self?.tooBarFrame?.value = CGRect(x: 0, y: Screen.height - 50 - height, width: Screen.width, height: 50)
      self?.tooBarFrame?.value = CGRect(x: 0, y: Screen.height - 50 - height, width: Screen.width, height: 50)
    }
    
    // 选择了表情
    tooBar.gifView.selectGifBlock = { [weak self] (model, _) in
      UIView.animate(withDuration: 0.2, animations: {
        self?.tooBarFrame?.value = CGRect(x: 0, y: Screen.height - 50, width: Screen.width, height: 50)
      })
      if let position = self?.currentUserInfo.oderIndex {
        self?.chatSendMessage(type: .show_emoticon, payLoad: ["mark": "\(model.mark)", "url":"\(model.url)", "position": position])
      }
    }
    
    messageViewController.keyboardDissBlock = { [weak self] model in
      guard let weakSelf = self else { return }
      var inRoom = false
      if let id = model?.id {
        for (_ , value) in weakSelf.dataSource {
          if value.id == id {
            inRoom = true
          }
        }
      }
      helpSendGift(inRoom, model)
    }
    
    func helpSendGift(_ inRoom: Bool, _ model: ChatRoomUserModel?) -> Void {
      guard inRoom else { return }
      var isInRoom = false
      let view = ChatMessageUserInfo.init(frame: .zero)
      view.model = model
      view.selectBlock = { [weak self] selectIndex in
        guard let weakSelf = self else { return }
        for (_ , value) in weakSelf.dataSource {
          if value.id == model?.id {
            isInRoom = true
          }
        }
        guard isInRoom else {
          Utils.hideNoticeView()
          XBHHUD.showTextOnly(text: "玩家已经离开房间".localized)
          return
        }
        switch selectIndex {
        case .addToBlock:
          Utils.hideAnimated()
          if weakSelf.isGaming, let postion = model?.position, postion <= chatRoomOnLineNumber {
            XBHHUD.showError("不能踢出游戏中的玩家".localized)
            return
          }
          if CurrentUser.shareInstance.ownerType == "purchased", CurrentUser.shareInstance.roomOwnerId == model?.id {
            XBHHUD.showTextOnly(text: "不能拉黑房契拥有者".localized)
            return
          }
          messageDispatchManager.sendMessage(type: .kick_out, payLoad: ["uid": model?.id ?? "", "block": 1])
        case .sendGift:
          Utils.hideAnimated()
          Utils.runInMainThread {
            NotificationCenter.default.post(name: NotifyName.kSendGift.name(), object: model)
          }
        }
      }
      Utils.showAlertView(view: view)
    }
    // 选择了游戏
    gameGif.gameView.selectGifBlock = { [weak self] (model, index) in
      self?.gameGif.dismiss(animated: true, completion: nil)
      if let position = self?.currentUserInfo.oderIndex {
        if model.mark == "points" {
          let scoreView = ChatSingerScore.showScoreView(position)
          Utils.showAlertView(view: scoreView)
        } else if model.mark == "applause" {
          RequestManager().post(url: RequestUrls.gameGif, parameters: ["version": "1"], success: { (response) in
            if let myObject = response["prices"]["0"]["price"].arrayValue.first {
               let count =  myObject["count"].intValue
              if count != 0 {
                if let position = self?.currentUserInfo.oderIndex {
                  let view = ChatApplauseView.showApplauseView(position, count)
                  Utils.showAlertView(view: view)
                }
              }
            }
          }) {  [weak self] (code, message) in
            if code != -999 {
              XBHHUD.showError(NSLocalizedString("获取礼物信息失败", comment: ""))
            }
          }
        } else {
          self?.chatSendMessage(type: .show_game_emoticon,
                                payLoad: ["mark": "\(model.mark)", "position": position,
                                          "data": ["stay_time": model.stay_time, "res_url": model.res_url, "res": model.res]])
        }

      }
    }
    
    tooBar.sendGifImageBlock = { [weak self] isBlack in
      guard let weakSelf = self else { return }
      if isBlack {
        weakSelf.tooBarFrame?.value = CGRect(x: 0, y: weakSelf.tooBarFrame?.value.origin.y ?? 0, width: Screen.width, height: 50)
        return
      }
      if weakSelf.tooBar.height <= 50 {
        let myHeight = 50 + (Screen.width - 45) / 3 + 30 + 20
        UIView.animate(withDuration: 0.2, animations: {
          weakSelf.tooBarFrame?.value = CGRect(x: 0, y: Screen.height - myHeight, width: Screen.width, height: myHeight)
        })
      } else {
        UIView.animate(withDuration: 0.2, animations: {
          weakSelf.tooBarFrame?.value = CGRect(x: 0, y: Screen.height - 50, width: Screen.width, height: 50)
        })
      }
    }
    
    
    tooBar.sendGiftBlock = { [weak self] in
      if let dataSource = self?.dataSource, let onLineListView = self?.onLineListView, let ordIndex =  self?.currentUserInfo.oderIndex {
        self?.onLineListView.array = dataSource
        self?.onLineListView.state = .sendGift
        let view = ChatRoomAlertList.showSendGiftListView(onLineListView, ordIndex)
        view.titleLabel.text = "赠送给".localized
        Utils.showNoticeView(view: view)
        view.chatListVC?.selectIndex = { [weak self] (index, model) in
          if !model.id.isEmpty {
            let numID = Int(model.id)
            if String(describing: numID) == model.id {
              XBHHUD.showError("对方登录后可赠送礼物".localized)
              return
            }
          }
          if !model.id.isEmpty {
            let gift = giftView.initGiftViewWith(envtype: .AUDIO, People:model.name)
            gift.toID = model.id
            gift.audioForm = ordIndex
            gift.audioTo = model.oderIndex
            gift.delegate = self
            Utils.getKeyWindow()?.addSubview(gift)
            gift.animation = "fadeIn"
            gift.animate()
            gift.getGiftFormRemote()
          }
          Utils.hideNoticeView()
        }
      }
    }
    
    tooBar.sendMessageBlock = { [weak self] msg in
      if !msg.isEmpty {
        self?.chatSendMessage(type: .chat, payLoad: ["position": self?.currentUserInfo.name ?? CurrentUser.shareInstance.name, "message": msg])
      }
    }
  }
}
