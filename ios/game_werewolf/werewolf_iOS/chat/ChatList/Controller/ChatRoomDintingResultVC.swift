//
//  ChatRoomDintingResultVC.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/28.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomDintingResultVC: UIViewController {
  
    var isSuccess = true
    var datqSource = [ChatRoomUserModel]()
    var loses = [ChatRoomUserModel]()
    var wins = [ChatRoomUserModel]()
    var winText = ""
    var lostText = ""
    var myRole = false
    var isCommit = false
  
    func setImages(_ models: [ChatRoomUserModel], fView: UIView) -> Void {
      let cX = UIScreen.main.bounds.width / 2 - CGFloat(models.count * 22)
      for index in 0..<models.count {
        let model = models[index]
        let cView = ChatDintingResultView.init(model: model)
        cView.frame = CGRect.init(x: cX + 44.0 * CGFloat(index), y: 0, width: 44, height: 48)
        fView.addSubview(cView)
      }
    }
  
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var successKeyWord: UILabel!
    @IBOutlet weak var failureKeyWord: UILabel!
    @IBOutlet weak var topSpace: NSLayoutConstraint!
    @IBOutlet weak var successImageView: UIImageView!
    @IBOutlet weak var niceImageView: UIImageView!
    @IBOutlet weak var badImageView: UIImageView!
    @IBOutlet weak var successTips: UIImageView!
    @IBOutlet weak var successNoticeLabel: UILabel!
    @IBOutlet weak var successBackView: UIView!
    @IBOutlet weak var failureTips: UIImageView!
    @IBOutlet weak var failureNoticeLabel: UILabel!
    @IBOutlet weak var failureBack: UIView!
    @IBOutlet weak var makeSureButton: UIButton!
    @IBOutlet weak var shareButton: UILabel!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var successTitle: UILabel!
    @IBOutlet weak var failureTitle: UILabel!

    let share = Bundle.main.loadNibNamed("gameResultShare", owner: nil, options: nil)?.last as! gameResultShareMenu
  
    
    @IBAction func niceButtonAction(_ sender: Any) {
      if isCommit {
        XBHHUD.showTextOnlyTop(text: "评价了还要评? 下一局吧".localized)
      } else {
        isCommit = true
        niceImageView.alpha = 0.7
        showAnimation(true)
        messageDispatchManager.sendMessage(type: GameMessageType.comment, payLoad: ["approve":true])
      }
    }
    
    @IBAction func badWordAction(_ sender: Any) {
      if isCommit {
        XBHHUD.showTextOnlyTop(text: "评价了还要评? 下一局吧!".localized)
      } else {
        isCommit = true
        badImageView.alpha = 0.7
        showAnimation(false)
        messageDispatchManager.sendMessage(type: GameMessageType.comment, payLoad: ["approve":false])
      }
    }
  
  func showAnimation(_ isNice: Bool) -> Void {
      let commitImage = UIImageView.init(image: UIImage.init(named: "chatNiceWord"))
      commitImage.contentMode = .scaleAspectFit
      self.view.addSubview(commitImage)
    let tmpFrame = CGRect(x: self.view.width / 2 - (isNice ? 50 : -50), y: successImageView.maxY + 60, width: 48, height: 30)
      commitImage.frame = tmpFrame
      
      UIView.animate(withDuration: 1, animations: {
        commitImage.frame = CGRect(x: self.view.width / 2, y: tmpFrame.origin.y - 30, width: 1.6, height: 1)
      }) { (finish) in
        commitImage.removeFromSuperview()
      }
    }
    
    @IBAction func sureButtonAction(_ sender: Any) {
      dismiss(animated: true, completion: nil)
    }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(true)
    share.setUp()
    share.shareImage = Utils.screenShot(view: view)
  }
    override func viewDidLoad() {
        super.viewDidLoad()
      successKeyWord.layer.cornerRadius = 5
      successKeyWord.layer.masksToBounds = true
      failureKeyWord.layer.cornerRadius = 5
      failureKeyWord.layer.masksToBounds = true
      
      successTitle.layer.cornerRadius = 9
      failureTitle.layer.cornerRadius = 9
      successTitle.layer.masksToBounds = true
      failureTitle.layer.masksToBounds = true
      makeSureButton.layer.cornerRadius = 8
      makeSureButton.layer.masksToBounds = true
      makeSureButton.shadowTopDown(colors: [UIColor(hexInt: 0xff33ff), UIColor(hexInt: 0xff35c9), UIColor(hexInt: 0xff378c)], locations: [0, 0.5, 1])
      
      let isIphone5 = UIScreen.main.bounds.width == 320
      if kIsIPhoneX {
        topSpace.constant = 80
        imageHeight.constant = UIScreen.main.bounds.height - 160 - 395
      } else {
        if isIphone5 {
          topSpace.constant = 20
          imageHeight.constant = UIScreen.main.bounds.height - 40 - 395
        } else {
          topSpace.constant = 40
          imageHeight.constant = UIScreen.main.bounds.height - 80 - 395
        }
      }
      // 分享按钮
      share.backgroundColor = UIColor.clear
      share.buttonWidth.constant = 25
      share.buttonHeight.constant = 25
      
      share.frame = CGRect(x: UIScreen.main.bounds.width / 2 - (isIphone5 ? 114 : 100), y: 2 , width: 260 ,height: 25)
      shareView.addSubview(share)
     
      successKeyWord.text = winText
      failureKeyWord.text = lostText
      // isSuccess + myRole
      // 成功 + 好人 = 好人胜利  1  win
      //              卧底失败  2 lose
     // 成功 + 卧底 = 卧底胜利  1  lose
      //             好人失败  2 wins
      // 失败 + 好人 = 卧底胜利 1 lose
      //              好人失败 2 wins
      // 失败 + 卧底 = 好人胜利 win
      // 第一行
      setImages( isSuccess == myRole ? wins : loses, fView: successBackView)
      // 第二行
      setImages(isSuccess == myRole ? loses : wins, fView: failureBack)
      successNoticeLabel.text = (isSuccess == myRole ? "好人".localized : "卧底".localized ) + "胜利".localized
      failureNoticeLabel.text = (isSuccess == myRole ? "卧底".localized :  "好人".localized) + "失败".localized
      if myRole {
        successImageView.image = UIImage.init(named: isSuccess ? "chatDint_peopleWin" : "chatDint_peopleLose")
      } else {
        successImageView.image = UIImage.init(named: isSuccess ? "chatDint_UndervoerWin" : "chatDint_UndervoerLose")
      }
      shareButton.text = isSuccess ? "炫耀一下".localized : "喊人报仇".localized
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

class ChatDintingResultView: UIView {
  
  var imageView = UIImageView()
  var nameLabel = UILabel()
  let numberImage = UIImageView()
  
  convenience init(model: ChatRoomUserModel) {
    self.init(frame: CGRect(x: 0, y: 0, width: 44, height: 48))
    if !model.avatar.isEmpty, let iUrl = URL.init(string: model.avatar) {
      imageView.sd_setImage(with: iUrl, placeholderImage: UIImage.init(named: "room_head_default"))
    } else {
      imageView.image = UIImage.init(named: "room_head_default")
    }
    if !model.name.isEmpty {
      nameLabel.text = model.name
      numberImage.image = UIImage(named: "chatPosition_\(model.oderIndex)");
    }
    if CurrentUser.shareInstance.currentPosition == model.oderIndex {
      imageView.layer.borderColor = UIColor.yellow.cgColor
    }
    
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(imageView)
    addSubview(nameLabel)
    addSubview(numberImage)
    
    imageView.layer.cornerRadius = 18
    imageView.layer.masksToBounds = true
    imageView.contentMode = .scaleAspectFill
    imageView.layer.borderColor = UIColor.white.cgColor
    imageView.layer.borderWidth = 1
    nameLabel.font = UIFont.systemFont(ofSize: 8)
    nameLabel.textAlignment = .center
    nameLabel.textColor = UIColor.white
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    imageView.frame = CGRect.init(x: 4, y: 0, width: 36, height: 36)
    nameLabel.frame = CGRect.init(x: 0, y: 36, width: 44, height: 12)
    let tmpX = sqrt(self.width * self.width / 32)
    numberImage.frame = CGRect(x: tmpX - 8, y: tmpX - 8, width: 15, height: 15)
  }
  
}












