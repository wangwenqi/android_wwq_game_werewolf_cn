//
//  ChatBlackListController.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/9.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

struct ChatBlackModel {
  var name = ""
  var id = ""
  var image = ""
  var sex = 1
}

class ChatBlackListController: UIViewController {

    var dataSource = [ChatBlackModel]()
    let tableview: UITableView = {
      let myTableView  = UITableView.init(frame: .zero, style: .plain)
      myTableView.register(UINib.init(nibName: ChatBlackCell.name(), bundle: nil), forCellReuseIdentifier: ChatBlackCell.name())
      return myTableView
    }()
  
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(true)
      self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
  
    override func viewDidLoad() {
      super.viewDidLoad()
      self.title = "房间黑名单".localized
      self.view.backgroundColor = UIColor.init(hex: "#3a2954")
      self.view.addSubview(tableview)
      tableview.backgroundColor = UIColor.init(hex: "#3a2954")
      tableview.delegate = self
      tableview.dataSource = self
      tableview.separatorStyle = .none
      tableview.frame = CGRect.init(x: 0, y: 0, width: view.width, height: view.height)
   }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ChatBlackListController: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableview.deselectRow(at: indexPath, animated: true)
    let model = dataSource[indexPath.row]
    let controller = UserInfoViewController()
    controller.peerID = model.id
    if controller.peerID == CurrentUser.shareInstance.id {
      controller.isself = true
    }
    controller.peerSex = "\(model.sex)"
    controller.peerName = model.name
    controller.peerIcon = model.image
    let rootVC = RotationNavigationViewController.init(rootViewController: controller)
    self.present(rootVC, animated: true, completion: nil)
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataSource.count
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableview.dequeueReusableCell(withIdentifier: ChatBlackCell.name()) as! ChatBlackCell
    cell.model = dataSource[indexPath.row]
    return cell
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    let action = UITableViewRowAction.init(style: .destructive, title: "从黑名单中移除".localized) { [weak self] (_, _) in
      guard let weakSelf = self else { return }
      let model = weakSelf.dataSource[indexPath.row]
      if !model.id.isEmpty {
        messageDispatchManager.sendMessage(type: GameMessageType.unblock_user, payLoad: ["userId": model.id])
        weakSelf.dataSource.remove(at: indexPath.row)
        weakSelf.tableview.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
      }
    }
    return [action]
  }
  
}
