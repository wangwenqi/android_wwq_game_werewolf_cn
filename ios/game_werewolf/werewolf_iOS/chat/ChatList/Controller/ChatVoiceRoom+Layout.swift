//
//  ChatVoiceRoom+Layout.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/23.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

// UI布局 -- 逻辑不用向下看
extension ChatVoiceRoomViewController {
  
  
  func configUserViews() -> Void {
    let space = (Screen.width / 3 - 40) / 3 - 5
    let itemWidth = Screen.width / 6
    let itemHeight = Screen.width / 6 + space
    let itemSpace = (Screen.width / 3 - 50) / 3;
    // 左右50间距
    
    managerView.number = "0"
    managerView.nameLabel.isHidden = true
    managerView.numberImage.isHidden = false
    managerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(roomOwnerAction)))
    giftViewsArray.append(managerView);
    for index in 0..<chatRoomOnLineNumber {
      let xSpace = Int(index % 4)
      let ySpace = Int(index / 4)
      let xLeft = CGFloat(xSpace) * (itemSpace + itemWidth)
      let yTop = CGFloat(ySpace) * itemHeight
      let userViewFrame = CGRect(x: xLeft, y: yTop, width: itemWidth, height: itemHeight)
      let giftViews = ChatRoomUserView.init(frame: userViewFrame)
      giftViews.number = "\(index + 1)"
      giftViews.isUserInteractionEnabled = true;
      giftViews.tag = 10001 + index
      giftViews.addTarget(self, action: #selector(chooseUserIndex(control:)), for: .touchUpInside)
      collectionBackView.addSubview(giftViews)
      giftViewsArray.append(giftViews)
    }
  }
  // 初始化视图部分
  func configBaseView() -> Void {
    
    view.addSubview(noticeView)
    view.bringSubview(toFront: noticeView)
    
    // 加入聊天页面
    chatView.addSubview(messageViewController.view)
    chatView.layer.masksToBounds = true
    
    roomTitleLabel.addSubview(titleLabel)
    titleLabel.isEditState = false
    titleLabel.endEditBlock = { [weak self] width, text in
      guard let weakSelf = self else { return }
      weakSelf.titleLabel.frame = CGRect(x: 0, y: 0, width: width, height: 18)
      weakSelf.titleLabel.sizeToFit()
      weakSelf.titleLabel.center = CGPoint(x: Screen.width / 2, y: 9)
      if text != weakSelf.titleLabel.titleText {
        weakSelf.titleLabel.titleText = text
      }
    }
    
    roomTitleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(editRoomTitle)))
    onLineView.isUserInteractionEnabled = true
    onLineView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(pushToListView)))
    view.addSubview(onLineView)
    onLineView.frame = CGRect(x: 0, y: 65, width: 140, height: 56)
    
    let likeWidth: CGFloat = 35
    likeLabel.font = UIFont.systemFont(ofSize: 12)
    likeLabel.textAlignment = .center
    likeLabel.frame = CGRect(x: Screen.width - 46 - 20, y: Screen.height - 63 - kSafeAreaBottomHeight, width: likeWidth + 40, height: 10)
    likeLabel.textColor = UIColor.white
    likeLabel.shadowColor = UIColor.black
    likeLabel.shadowOffset = CGSize(width: 1, height: 1)
    view.addSubview(likeLabel)
    
    likeButton.frame = CGRect(x: Screen.width - 46, y: Screen.height -  100 - kSafeAreaBottomHeight, width: likeWidth, height: likeWidth)
    likeButton.alpha = 1
    likeButton.layer.cornerRadius = 17.5
    let tapGes = UITapGestureRecognizer.init(target: self, action: #selector(sendLike))
    likeButton.addGestureRecognizer(tapGes)
    let likeImage = UIImageView()
    likeImage.frame = CGRect(x: 0, y: 0, width: likeWidth, height: likeWidth)
    likeImage.image = UIImage.init(named: "chatRoom_like_button")
    likeButton.layer.contents = likeImage
    likeButton.addSubview(likeImage)
    view.addSubview(likeButton)
    
    gameButton.frame = CGRect(x: Screen.width - 96, y: Screen.height - 100 - kSafeAreaBottomHeight, width: likeWidth, height: likeWidth)
    gameButton.setBackgroundImage(UIImage.init(named: "chatVoice_game_icon"), for: .normal)
    gameButton.addTarget(self, action: #selector(playGameAction), for: .touchUpInside)
    gameButton.layer.cornerRadius = 17.5
    gameButton.layer.masksToBounds = true
    view.addSubview(gameButton)
    tooBar.frame = CGRect(x: 0, y: Screen.height - 50 - kSafeAreaBottomHeight, width: Screen.width, height: 50)
    tooBar.layer.masksToBounds = true
    gameButton.isHidden = true
    tooBar.isOnMir = false
    view.addSubview(tooBar)
    view.bringSubview(toFront: tooBar)
  }
  
  func configGiftView() {
    let rect = view.bounds
    giftmanager.frame = rect
    giftmanager.type = .AUDIO
    giftmanager.backgroundColor = UIColor.clear.withAlphaComponent(0)
    view.addSubview(giftmanager)
    shareGift.shareInstance.updateGift()
    view.bringSubview(toFront: giftmanager)
    //检查是否需要下载新的礼物信息
    GiftSourceManager.checkVersion()
  }
  
  func configNavi() -> Void {
    let tintColor = UIColor(hex: "#d9357c")
    backButton.tintColor = tintColor
    shareButton.tintColor = tintColor
    moreOrderButton.tintColor = tintColor
    moreOrderButton.tintColor = tintColor
    
    view.addSubview(backGroundImageView)
    backGroundImageView.frame = CGRect(x: 0, y: 0, width: Screen.width, height: Screen.height)
    let space: CGFloat = 20
    let itemWidth: CGFloat = 27
    let topSpace: CGFloat = 30
    // 退出房间按钮
    view.addSubview(backButton)
    backButton.frame = CGRect(x: space, y: topSpace, width: itemWidth, height: itemWidth)
    backButton.setImage(UIImage(named: "chatRoom_Back_Icon"), for: .normal)
    backButton.contentMode = .scaleAspectFit
    backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    
    view.addSubview(Wifi_State)
    Wifi_State.frame = CGRect(x: space + 4 + itemWidth, y: topSpace + 3.5, width: itemWidth - 7 , height: itemWidth - 7)
    Wifi_State.contentMode = .scaleAspectFit
    Wifi_State.image = UIImage(named: "wifi_great-shutdown")
    
    view.addSubview(roomNameLabel)
    let nameLeft = space + itemWidth * 2 + 5
    roomNameLabel.frame = CGRect(x: nameLeft, y: topSpace, width: Screen.width - (2 * nameLeft), height: itemWidth)
    roomNameLabel.textAlignment = .center
    roomNameLabel.font = UIFont.systemFont(ofSize: 16)
    roomNameLabel.textColor = .white
    
    view.addSubview(shareButton)
    shareButton.frame = CGRect(x: Screen.width - space - itemWidth , y: topSpace, width: itemWidth, height: itemWidth)
    shareButton.setImage(UIImage(named: "chatRoom_Close_Icon"), for: .normal)
    shareButton.contentMode = .scaleAspectFit
    shareButton.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
    
    view.addSubview(moreOrderButton)
    moreOrderButton.frame = CGRect(x: Screen.width - space - (2 * itemWidth) - 4 , y: topSpace, width: itemWidth, height: itemWidth)
    moreOrderButton.setImage(UIImage(named: "chatRoom_order"), for: .normal)
    moreOrderButton.contentMode = .scaleAspectFit
    moreOrderButton.addTarget(self, action: #selector(moreOrderButtonAction), for: .touchUpInside)
    
    let managerWidth = Screen.width / 5
    let managerTop = topSpace + itemWidth + 5
    view.addSubview(managerView)
    managerView.frame = CGRect(x: Screen.width / 2 - managerWidth / 2, y: managerTop, width: managerWidth, height: managerWidth)
    
    view.addSubview(ownerNameLabel)
    ownerNameLabel.frame = CGRect(x: Screen.width / 2 - 100, y: managerTop + managerWidth + 8, width: 200, height: 18)
    ownerNameLabel.font = UIFont.systemFont(ofSize: 14)
    ownerNameLabel.textAlignment = .center
    ownerNameLabel.textColor = .white
    
    let roomTitleTop = managerWidth + managerTop + 30
    view.addSubview(roomTitleLabel)
    roomTitleLabel.frame = CGRect(x: 0, y: roomTitleTop, width: Screen.width, height: 20)
    
    let collectionHeight = Screen.width / 3 + (2 * ((Screen.width / 3 - 40) / 3 - 5))
    view.addSubview(collectionBackView)
    collectionBackView.frame = CGRect(x: 25, y: roomTitleTop + 25, width: Screen.width - 50, height: collectionHeight)
    
    view.addSubview(chatView)
    chatView.frame = CGRect(x: 10, y: roomTitleTop + 25 + collectionHeight, width: Screen.width - 20, height: Screen.height - (roomTitleTop + 25 + collectionHeight) - kSafeAreaBottomHeight - 50)
  }
}
