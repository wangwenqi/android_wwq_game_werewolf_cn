//
//  GameRoomMessageViewController.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatRoomChatList: UITableViewController {
  
  struct CellID {
    static let ChatRoomNoticeCell = "ChatRoomNoticeCell"
    static let ChatRoomUserMessageCell = "ChatRoomUserMessageCell"
    static let ChatGiftMessageCell = "ChatGiftMessageCell"
    static let ChatRoomScoreCell = "ChatRoomScoreCell"
    static let ChatRoomDintingCell = "ChatRoomDintingCell"
  }
  var keyboardDissBlock: ((ChatRoomUserModel?) -> Void)?
  var lastTimeReloaded:TimeInterval = 0
  var currentInsInBottom = true
  class ChatMessgeModel: MessgeModel {
    var colorType: ChatNoticeView.ChatRoomUserState = .hide
    var position = ""
    var name = ""
    var score: Int = 5
    var model: ChatRoomUserModel?
  }
  
  // 数据数组
  var modelArray = [ChatMessgeModel]()
  //是不是在送礼物
  var isSendingGift = false
  override func viewDidLoad() {
    
    super.viewDidLoad()
    tableView.backgroundColor = UIColor.clear
    tableView.separatorColor = UIColor.clear
    tableView.showsVerticalScrollIndicator = false
    //    tableView.allowsSelection = false
    tableView.estimatedRowHeight = 44
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.register(ChatRoomNoticeCell.self, forCellReuseIdentifier: CellID.ChatRoomNoticeCell)
    tableView.register(ChatRoomUserMessageCell.self, forCellReuseIdentifier: CellID.ChatRoomUserMessageCell)
    tableView.register(ChatGiftMessageCell.self, forCellReuseIdentifier: CellID.ChatGiftMessageCell)
    tableView.register(ChatRoomScoreCell.self, forCellReuseIdentifier: CellID.ChatRoomScoreCell)
    tableView.register(ChatRoomDintingCell.self, forCellReuseIdentifier: CellID.ChatRoomDintingCell)
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
  }
  
  override func didReceiveMemoryWarning() {
    clearAll()
    super.didReceiveMemoryWarning()
  }
  
  func clearAll(){
    modelArray.removeAll()
    tableView.reloadData()
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard CurrentUser.shareInstance.currentPosition == 0 else {
      return
    }
    if indexPath.row <= modelArray.count - 1 {
      let model = modelArray[indexPath.row]
      guard model.model?.position != 0 else { return }
      switch model.type {
      case .voicePlayer, .voiceGift, .score:
        keyboardDissBlock?(model.model)
      default:
        break
      }
      print(model.type)
    }
  }
  
  func sendingGift() {
    isSendingGift = true
  }
  
  func endSending() {
    isSendingGift = false
  }
  
  func addDintingConversion(mType: ChatRoomDintingCell.DintingState, message: String) -> Void {
    let model = ChatMessgeModel()
    model.type = .dinting
    switch mType {
    case .gameOver:
      model.name = "游戏结果".localized
    case .voteResult:
      model.name = "投票结果".localized
    case .deathInfo:
      model.name = "死亡信息".localized
    }
    model.position = model.name
    model.message = message
    addModel(model: model)
  }
  
  func addScoreConversion(score: Int, positionNumber: String = "", model: ChatRoomUserModel?) {
    if let model = model {
      DispatchQueue.global().asyncAfter(wallDeadline: DispatchWallTime.now() + 1) { [weak self] in
        let cellModel = ChatMessgeModel()
        cellModel.score = score
        cellModel.type = .score
        cellModel.positionNumber = positionNumber
        cellModel.name = model.name
        cellModel.model = model
        self?.addModel(model: cellModel)
      }
    }
  }
  
  func addNoticeConversion(type:MessageType, messge: String, colorType: ChatNoticeView.ChatRoomUserState) {
    let model = ChatMessgeModel()
    model.message = messge
    model.type = type
    model.colorType = colorType
    addModel(model: model)
  }
  
  func addGiftConversion(from: String, to: Int, giftType:String, messge: String) {
    
    let model = ChatMessgeModel()
    model.message = messge
    model.type = .gift
    model.positionNumber = from;
    model.to = to;
    model.giftType = giftType;
    addModel(model: model);
  }
  
  func addVoiceConversion(playerMessge: String, positionNumber: String = "", model: ChatRoomUserModel?) {
    if let model = model {
      let cellModel = ChatMessgeModel()
      cellModel.message = playerMessge
      cellModel.type = .voicePlayer
      cellModel.positionNumber = positionNumber
      cellModel.name = model.name
      cellModel.model = model
      addModel(model: cellModel);
    }
  }
  
  func addGiftClearConversion(from: String, model: ChatRoomUserModel?,  to: Int, giftType:String, messge: String) {
    if let model = model {
      let cellModel = ChatMessgeModel()
      cellModel.message = messge
      cellModel.type = .voiceGift
      cellModel.positionNumber = from
      cellModel.name = model.name
      cellModel.to = to;
      cellModel.giftType = giftType;
      cellModel.model = model
      addModel(model: cellModel);
    }
  }
  
  
  private func addModel(model:ChatMessgeModel) {
      DispatchQueue.main.async { [weak self] in
        guard let weakself = self else { return }
        if weakself.modelArray.count >= 300 {
          for _ in 0...150 {
            weakself.modelArray.removeFirst()
          }
        }
        weakself.modelArray.append(model)
        weakself.tableView.reloadData()
        if !weakself.currentInsInBottom {
          weakself.tableView.reloadData()
        } else {
          weakself.tableView.scrollToRow(at: IndexPath.init(row: weakself.modelArray.count - 1, section: 0), at: .bottom, animated: false)
        }
      }
  }
  
  
  
  // MARK: - tableViewDelegate
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return modelArray.count
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if indexPath.row >= modelArray.count - 2 {
      currentInsInBottom = true
    } else {
      currentInsInBottom = false
    }
    
    if indexPath.row <= modelArray.count - 1 {
      let model = modelArray[indexPath.row]
      switch model.type {
      case .system:
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.ChatRoomNoticeCell) as! ChatRoomNoticeCell
        cell.model = model
        return cell
      case .voiceGift, .gift:
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.ChatGiftMessageCell) as! ChatGiftMessageCell
        cell.model = model
        return cell
      case .score:
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.ChatRoomScoreCell) as! ChatRoomScoreCell
        cell.model = model
        return cell
      case .dinting:
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.ChatRoomDintingCell) as! ChatRoomDintingCell
        cell.model = model
        return cell
      default:
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.ChatRoomUserMessageCell) as! ChatRoomUserMessageCell
        cell.model = model
        return cell
      }
    } else {
      let cell = tableView.dequeueReusableCell(withIdentifier: CellID.ChatRoomNoticeCell) as! ChatRoomNoticeCell
      return cell
    }
  }
  
}

