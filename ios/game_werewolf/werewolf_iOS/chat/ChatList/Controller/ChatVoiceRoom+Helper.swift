//
//  ChatVoiceRoom+Helper.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/28.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

extension ChatVoiceRoomViewController {
  
  // 以下方法均为被动调用
  
  
  // 开关声音
  func changeSound(_ close: Bool) -> Void {
    DispatchQueue.global().async { [weak self] in
      guard let weakSelf = self else { return }
      weakSelf.session?.changeSoundType(close)
    }
    
  }
  
  // 展示好友请求的view
  func showAddFrendView(_ request: ChatRoomAddFriendRequest?) -> Void {
    if let request = request {
      frendRequests.append(request)
    } else {
      frendRequests.removeFirst()
    }
    if let viewRequest = frendRequests.first, view.viewWithTag(90001) == nil {
      let addView = ChatRoomAddFrend.showChatRoomAddFrend(viewRequest)
      addView.tag = 90001
      addView.selectedBlock = { [weak self] state in
        Utils.runInMainThread { [weak self] in
          guard let weakSelf = self else { return }
          switch state {
          case .agreeAction, .refuseAction, .closeAction:
            addView.removeFromSuperview()
            weakSelf.showAddFrendView(nil)
          case .checkUserInfo:
            let controller = UserInfoViewController()
            controller.peerID = viewRequest.id
            if controller.peerID == self?.currentUserInfo.id {
              controller.isself = true
            }
            controller.peerSex = viewRequest.sex
            controller.peerName = viewRequest.name
            controller.peerIcon = viewRequest.avaImage
            let rootVC = RotationNavigationViewController.init(rootViewController: controller)
            self?.present(rootVC, animated: true, completion: nil)
          }
        }
      }
      view.addSubview(addView)
    }
  }
  
  // 交换用以上麦/下麦
  func swapObjc(_ obj1: Int, _ obj2: Int) -> Void {
    objc_sync_enter(dataSource)
    dataSource[obj2]?.player = nil
    swap(&dataSource[obj1], &dataSource[obj2])
    objc_sync_enter(dataSource)
  }
  
  // 聊天发消息
  func chatSendMessage(type: GameMessageType, payLoad: [String: Any] = ["":""]) {
    messageDispatchManager.sendMessage(type: type, payLoad: payLoad);
  }
  
  
  // 用uid找positon
  func checkUserIdBackPosition(_ userId: String) -> Int?  {
    for (key, value) in dataSource {
      if value.id == userId {
        return Int(key)
      }
    }
    return nil
  }

  // 获取发送/接收礼物的位置view
  func giftFromToView(_ index: Int) -> UIView {
    if index <= chatRoomOnLineNumber {
      return giftViewsArray[index]
    } else {
      return tooBar as UIView
    }
  }

}
