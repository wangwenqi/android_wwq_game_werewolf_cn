//
//  ChatListViewController.swift
//  game_werewolf
//
//  Created by Tony on 2017/9/6.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class ChatListViewController: RotationViewController {

    enum PageState {
      case allOnline      //  所有在线人数
      case sendGift       //  发送礼物列表
      case sendManager    //  移交房主列表
      case holdSeat       //  抱人上麦
    }
    var currentIndex: Int = 0
    var selectIndex: ((Int, ChatRoomUserModel) -> Void)?
    var state: PageState = .allOnline {
      didSet {
        configListView()
      }
    }
    fileprivate let tableView = UITableView(frame: CGRect.zero, style: .plain)
    var array = [Int: ChatRoomUserModel]() {
      didSet {
        configListView()
      }
    }
    var dataSource = [ChatRoomUserModel]() {
      didSet {
        DispatchQueue.main.async { [ weak self] in
          guard let weakSelf = self else { return }
          weakSelf.tableView.reloadData()
          weakSelf.tableView.isUserInteractionEnabled = true
        }
      }
    }
    var isShow = false
    private func configListView() -> Void {
      if isShow {
        switch state {
        case .allOnline:             // 所有在线列表
          allOnLineList()
        case .sendGift:              // 发送礼物列表
          sendGiftList()
        case .sendManager:
          sendManagerList()    // 移交房主
        case .holdSeat:
          holdSeatList()       // 抱人上麦
        }
      }
    }
  
  func sendManagerList() {
    let tmpArray = array.filter { (key, value) -> Bool in
      if !value.id.isEmpty, key != currentIndex {
        return true
      }
      return false
      }.map { (key, value) -> ChatRoomUserModel in
        let model = value
        if key == 0 { model.userRoomState = .roomOfMaster }
        if key > 0 && key <= chatRoomOnLineNumber { model.userRoomState = .roomOfOnMicro }
        if key > chatRoomOnLineNumber { model.userRoomState = .roomOfEmpty }
        model.oderIndex = key
        return model
      }.sorted { (model1, model2) -> Bool in
        return model1.oderIndex > model2.oderIndex
    }
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      if tmpArray.count == 0 {
        weakSelf.tableView.backgroundColor = UIColor.clear
      } else {
        weakSelf.tableView.backgroundColor = UIColor(hex: "#E2DEE8")

      }
    }
    tableView.isUserInteractionEnabled = false
    dataSource = tmpArray
  }
  
  private func allOnLineList() {
    let mArray = array.filter { (key, value) -> Bool in
      if !value.id.isEmpty {
        return true
      }
      return false
      }.map { (key, value) -> ChatRoomUserModel in
        let model = value
        if key == 0 { model.userRoomState = .roomOfMaster }
        if key > 0 && key <= chatRoomOnLineNumber { model.userRoomState = .roomOfOnMicro }
        if key > chatRoomOnLineNumber { model.userRoomState = .roomOfEmpty }
        model.oderIndex = key
        return model
      }.sorted { (model1, model2) -> Bool in
        if currentIndex == 0 {
          return model1.oderIndex > model2.oderIndex
        }
        return model1.oderIndex < model2.oderIndex
    }
    
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      if mArray.count == 0 {
        weakSelf.tableView.backgroundColor = UIColor.clear
      } else {
        weakSelf.tableView.backgroundColor = UIColor(hex: "#E2DEE8")
      }
    }
    tableView.isUserInteractionEnabled = false
    dataSource = mArray
  }
  
  private func holdSeatList() {
    
    let tmpArray = array.filter { (key, value) -> Bool in
      if !value.id.isEmpty, key != currentIndex, key > chatRoomOnLineNumber {
        return true
      }
      return false
      }.map { (key, value) -> ChatRoomUserModel in
        let model = value
        if key == 0 { model.userRoomState = .roomOfMaster }
        if key > 0 && key <= chatRoomOnLineNumber { model.userRoomState = .roomOfOnMicro }
        if key > chatRoomOnLineNumber { model.userRoomState = .roomOfEmpty }
        model.oderIndex = key
        return model
      }.sorted { (model1, model2) -> Bool in
        return model1.oderIndex > model2.oderIndex
    }
    
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      if tmpArray.count == 0 {
        weakSelf.tableView.backgroundColor = UIColor.clear
      } else {
        weakSelf.tableView.backgroundColor = UIColor(hex: "#E2DEE8")
      }
    }
    tableView.isUserInteractionEnabled = false
    dataSource = tmpArray
  }
  
  private func sendGiftList() {
    var myArray = [Int: ChatRoomUserModel]()
    for (key, value) in array {
      if !value.id.isEmpty {
        myArray[key] = value
      }
    }
    let tmpArray = myArray.map { (key, value) -> ChatRoomUserModel in
        let model = value
        if key == 0 { model.userRoomState = .roomOfMaster }
        if key > 0 && key <= chatRoomOnLineNumber { model.userRoomState = .roomOfOnMicro }
        if key > chatRoomOnLineNumber { model.userRoomState = .roomOfEmpty }
        model.oderIndex = key
        return model
      }.sorted { (model1, model2) -> Bool in
        return model1.oderIndex < model2.oderIndex
    }
    
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else { return }
      if tmpArray.count == 0 {
        weakSelf.tableView.backgroundColor = UIColor.clear
      } else {
        weakSelf.tableView.backgroundColor = UIColor(hex: "#E2DEE8")
      }
    }
    tableView.isUserInteractionEnabled = false
    dataSource = tmpArray
  }
  
    override func viewDidLoad() {
      super.viewDidLoad()
      self.title = "在线列表".localized
      view.backgroundColor = UIColor.clear
      configTableView()
      let image = UIImage(named: "chatRoom_back_icon-1")?.withRenderingMode(.alwaysOriginal)
      self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: image, style: .done, target: self, action: #selector(backAction))
    }
  
    func backAction() {
      self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func configTableView() -> Void {
      tableView.delegate = self
      tableView.dataSource = self
      tableView.backgroundColor = UIColor.clear
      tableView.separatorStyle = .none
      tableView.register(UINib(nibName: ChatListTableViewCell.name(), bundle: nil), forCellReuseIdentifier: ChatListTableViewCell.reuseName())
      view.addSubview(tableView)
      tableView.snp.makeConstraints { (make) in
        make.bottom.leading.trailing.equalToSuperview()
        make.top.equalToSuperview().offset(0)
      }
    }
  
  
  override func viewDidDisappear(_ animated: Bool) {
    isShow = false
    self.navigationController?.setNavigationBarHidden(true, animated: true)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    isShow = true
    configListView()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(true)
  }
  
}


extension ChatListViewController: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: ChatListTableViewCell.reuseName()) as! ChatListTableViewCell
    if state == .allOnline, indexPath.row != dataSource.count - 1, currentIndex == 0 {
       cell.kickOut.isHidden = false
    } else {
      cell.kickOut.isHidden = true
    }
    if dataSource.count > indexPath.row {
      cell.model = dataSource[indexPath.row]
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 75
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let model = dataSource[indexPath.row]
    selectIndex?(indexPath.row, model)
  }
  
}
