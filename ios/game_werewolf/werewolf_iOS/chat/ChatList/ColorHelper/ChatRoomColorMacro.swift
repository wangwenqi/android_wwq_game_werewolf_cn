//
//  ChatRoomColorMacro.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/11/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

extension UIView {
  
  // 此方法设置渐变, 使用注意: 字体必须在调用方法后设置
  func shadowTopDown(colors: [UIColor], locations: [NSNumber]) -> Void {
    let gradientLayer = CAGradientLayer()
    var myColors = [Any]()
    colors.forEach { (myColor) in
      myColors.append(myColor.cgColor)
    }
    gradientLayer.colors = myColors
    gradientLayer.locations = locations
    gradientLayer.startPoint = CGPoint.init(x: 0.0, y: 0.0)
    gradientLayer.endPoint = CGPoint.init(x: 0.0, y: 1.0)
    gradientLayer.frame = self.bounds
    self.layer.addSublayer(gradientLayer)
  }
  
}

extension UIColor {
  
  // 文字的紫色与背景紫色
  class func textPurpleLightColor() -> UIColor {
    return UIColor(hexInt: 0x8460E3)
  }
  
  class func textPurpleDarkColor() -> UIColor {
    return UIColor(hexInt: 0x6D50BA)
  }
  
  class func chatNoticeBack() -> UIColor {
    return UIColor(hexInt: 0xE2DEE8)
  }
  
  class func oragenBack() -> UIColor {
    return UIColor(hexInt: 0xf29101)
  }
  // 粉色背景
  class func pinkColor() -> UIColor {
    return UIColor(hexInt: 0xfd14a8)
  }

  
  // 紫色叠层背景
  class func purpleBackColor() -> UIColor {
    return UIColor(hexInt: 0x7a64ff)
  }
  
  class func noticeBackGroundColor() -> UIColor {
    return UIColor(hexInt: 0xE2DEE8)
  }
  
  func textColor() -> UIColor {
    return UIColor(hexInt: 0xffffff)
  }
  
  func titleColor() -> UIColor {
    return UIColor(hexInt: 0xffffff)
  }
  
  func subColor() -> UIColor {
    return UIColor(hexInt: 0xffffff)
  }
  
  convenience init(mred: Int, mgreen: Int, mblue: Int) {
    assert(mred >= 0 && mred <= 255, "Invalid red component")
    assert(mgreen >= 0 && mgreen <= 255, "Invalid green component")
    assert(mblue >= 0 && mblue <= 255, "Invalid blue component")
    
    self.init(red: CGFloat(mred) / 255.0, green: CGFloat(mgreen) / 255.0, blue: CGFloat(mblue) / 255.0, alpha: 1.0)
  }
  
  convenience init(hexInt: Int) {
    self.init(
      red: (hexInt >> 16) & 0xFF,
      green: (hexInt >> 8) & 0xFF,
      blue: hexInt & 0xFF
    )
  }
}
