//
//  MoveButton.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/12/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class MoveButton: UIButton {

  var panG: UIPanGestureRecognizer?
  
 override init(frame: CGRect) {
    super.init(frame: frame)
    panG = UIPanGestureRecognizer.init(target: self, action: #selector(changePosition(recognizer:)))
    self.addGestureRecognizer(panG!)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
 func changePosition(recognizer: UIPanGestureRecognizer) -> Void{
    guard let parent = UIViewController.topWindow() else {
      return
    }
    let point = recognizer.translation(in: parent)
    let width: CGFloat = parent.bounds.size.width
    let height: CGFloat = parent.bounds.size.height
    var  originalFrame = self.frame
    if originalFrame.origin.x >= 0 && originalFrame.origin.x + originalFrame.size.width <= width {
      originalFrame.origin.x += point.x
    }
    if (originalFrame.origin.y >= 0 && originalFrame.origin.y+originalFrame.size.height <= height) {
      originalFrame.origin.y += point.y
    }
    self.frame = originalFrame
    recognizer.setTranslation(CGPoint.zero, in: parent)
    if panG?.state == UIGestureRecognizerState.began {
      self.isEnabled = false
    } else if panG?.state == UIGestureRecognizerState.changed {
      var frame = self.frame
      if (frame.origin.x < 0) {
        frame.origin.x = 0
      } else if (frame.origin.x + frame.size.width > width) {
        frame.origin.x = width - frame.size.width
      }
      if (frame.origin.y < 0) {
        frame.origin.y = 0
      } else if (frame.origin.y + frame.size.height > height) {
        frame.origin.y = height - frame.size.height
      }
      self.frame = frame;
    } else {
      var frame = self.frame
      frame.origin.x = self.center.x <= width / 2.0 ? 20 : width - frame.size.width - 20
      if (frame.origin.y < 44) {
        frame.origin.y = 44
      } else if (frame.origin.y + frame.size.height > height - 34) {
        frame.origin.y = height - frame.size.height - 34
      }
      UIView.animate(withDuration: 0.3, animations: {
        self.frame = frame
      })
      self.isEnabled = true
    }
  }

}
