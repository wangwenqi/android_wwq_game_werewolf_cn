//
//  ChatListModel.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/24.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class BaseModel: NSObject {
  
  convenience init(_ dic: [String: Any]) {
    self.init()
    setValuesForKeys(dic)
  }
  
  override func setNilValueForKey(_ key: String) {}
  override func value(forUndefinedKey key: String) -> Any? { return nil }
  override func setValue(_ value: Any?, forUndefinedKey key: String) {
    print("FinishTaskViewModel undefine Key \(key)")
  }
}


class ChatListModel: BaseModel {

  var header: String?
  var name: String?
  var sign: String?
  
}

extension UITableViewCell {
  
  class func reuseName() -> String {
    return String(describing: self)
  }
}

extension UIView {
  
  class func name() -> String {
    return String(describing: self)
  }
}
