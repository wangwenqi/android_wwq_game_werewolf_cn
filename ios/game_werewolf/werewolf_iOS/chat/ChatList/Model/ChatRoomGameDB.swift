//
//  ChatRoomGameDB.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/31.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import SQLite3

extension ChatGifSQLiteManager {
  
  // 增
  func insertGameToDB(_ model: ChatGifModel) -> Bool {
    let insertSQL = "INSERT INTO 'GameGifTable' (mark,url,thumbnail, res_url, res, sound, stay_time) VALUES ('\(model.mark)','\(model.url)','\(model.thumbnail)','\(model.res_url)', '\(model.res)', '\(model.sound)', '\(model.stay_time)')"
    return GifDBManager.execSQL(SQL: insertSQL)
  }
  
  
  // 查 单个
  func queryGameDB(_ mark: String) -> ChatGifModel? {
    var stmt: OpaquePointer? = nil
    let cQuerySQL = "SELECT *FROM 'GameGifTable' WHERE mark = '\(mark)' ".cString(using: .utf8)
    if sqlite3_prepare_v2(db, cQuerySQL, -1, &stmt, nil) == SQLITE_OK {
      // 准备好之后进行解析
      while sqlite3_step(stmt) == SQLITE_ROW {
        if  let tmpMark = UnsafePointer(sqlite3_column_text(stmt, 1)),
          let tmpUrl = UnsafePointer(sqlite3_column_text(stmt, 2)),
          let tmpThumbnail = UnsafePointer(sqlite3_column_text(stmt, 3)),
          let res_url = UnsafePointer(sqlite3_column_text(stmt, 4)),
          let res = UnsafePointer(sqlite3_column_text(stmt, 5)),
          let sound = UnsafePointer(sqlite3_column_text(stmt, 6)),
          let stay_time = UnsafePointer(sqlite3_column_text(stmt, 7)) {
          return ChatGifModel(url: String(cString: tmpUrl), mark: String(cString: tmpMark), thumbnail: String(cString: tmpThumbnail), res_url: String(cString: res_url), res: String(cString: res), sound: String(cString: sound), stay_time: Double(String(cString: stay_time)) ?? 0)
        }
      }
    }
    return nil
  }
  
  // 查全部
  func queryGameDBAll() -> [ChatGifModel]? {
    var allModel = [ChatGifModel]()
    var stmt: OpaquePointer? = nil
    let cQuerySQL = "SELECT *FROM 'GameGifTable'".cString(using: .utf8)
    if sqlite3_prepare_v2(db, cQuerySQL, -1, &stmt, nil) == SQLITE_OK {
      // 准备好之后进行解析
      while sqlite3_step(stmt) == SQLITE_ROW {
        if let tmpMark = UnsafePointer(sqlite3_column_text(stmt, 1)),
          let tmpUrl = UnsafePointer(sqlite3_column_text(stmt, 2)),
          let tmpThumbnail = UnsafePointer(sqlite3_column_text(stmt, 3)),
          let res_url = UnsafePointer(sqlite3_column_text(stmt, 4)),
          let res = UnsafePointer(sqlite3_column_text(stmt, 5)),
          let sound = UnsafePointer(sqlite3_column_text(stmt, 6)),
          let stay_time = UnsafePointer(sqlite3_column_text(stmt, 7)) {
          let chatGifModel = ChatGifModel(url: String(cString: tmpUrl), mark: String(cString: tmpMark), thumbnail: String(cString: tmpThumbnail), res_url: String(cString: res_url), res: String(cString: res), sound: String(cString: sound), stay_time: Double(String(cString: stay_time)) ?? 0)
          allModel.append(chatGifModel)
        }
      }
    }
    return allModel
  }
  
  
  
}
