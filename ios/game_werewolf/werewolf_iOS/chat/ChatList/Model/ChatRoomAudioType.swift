//
//  ChatRoomAudioType.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/19.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import ObjectMapper

//enum AudioType: String {
//  case sing     // k歌
//  case chat     // 闲聊
//  case love     // 相亲
//  case fate     // 算命
//  case merry    // 婚礼
//  case seek     // 情感咨询
//  case game     // 小游戏
//  case unKnown  // 未知
//}

class MapperAudioTypeArray {
  
  var types = [ChatRoomAudioType]()
  
  func setModels(_ json: [JSON]) -> Void {
    types = [ChatRoomAudioType]()
    for tmp in json {
        let model = ChatRoomAudioType.init(tmp.dictionaryValue)
        types.append(model)
    }
  }
  
}

class ChatRoomAudioType: BaseModel {
  
  init(_ dic: [String: JSON]) {
    name = dic["t"]?.stringValue
    image = dic["i"]?.stringValue
    type = dic["k"]?.stringValue
    small = dic["ai"]?.stringValue
  }
  
  var type: String?
  var name: String?
  var image: String?
  var small: String?
  
}
