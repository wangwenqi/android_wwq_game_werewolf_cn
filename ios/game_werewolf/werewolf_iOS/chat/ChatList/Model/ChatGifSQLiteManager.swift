//
//  GifDBManager.swift
//  game_werewolf
//
//  Created by xiaozao on 2017/10/21.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import SQLite3

// 小表情数据库
// Gif用到的model
class ChatGifModel {
  
  convenience init(url: String, mark: String, thumbnail: String, res_url: String, res: String, sound: String, stay_time: Double) {
    self.init()
    self.url = url
    self.mark = mark
    self.thumbnail = thumbnail
    self.res_url = res_url
    self.res = res
    self.sound = sound
    self.stay_time = Int(stay_time)
  }
  
  var url = ""
  var mark = ""
  var thumbnail = ""
  // 小游戏使用
  var res_url = ""
  var res = ""
  var sound = ""
  var stay_time: Int = 0
}

let GifDBManager = ChatGifSQLiteManager()

class ChatGifSQLiteManager: NSObject {

  // 数据库变量
  var db: OpaquePointer? = nil
  // 目前表名: GifTable
  
  // 关闭
  func closeDB() -> Void {
    if let db = db {
      print("关闭数据库成功")
      sqlite3_close(db)
    }
  }
  
  // 增
  func insertToDB(_ model: ChatGifModel) -> Bool {
    let insertSQL = "INSERT INTO 'GifTable' (mark,url,thumbnail) VALUES ('\(model.mark)','\(model.url)','\(model.thumbnail)')"
    return GifDBManager.execSQL(SQL: insertSQL)
  }
  
  
  // 查 单个
  func queryDB(_ mark: String) -> ChatGifModel? {
    var stmt: OpaquePointer? = nil
    let cQuerySQL = "SELECT *FROM 'GifTable' WHERE mark = '\(mark)' ".cString(using: .utf8)
    if sqlite3_prepare_v2(db, cQuerySQL, -1, &stmt, nil) == SQLITE_OK {
      // 准备好之后进行解析
      while sqlite3_step(stmt) == SQLITE_ROW {
      if  let tmpMark = UnsafePointer(sqlite3_column_text(stmt, 1)),
          let tmpUrl = UnsafePointer(sqlite3_column_text(stmt, 2)),
          let tmpThumbnail = UnsafePointer(sqlite3_column_text(stmt, 3)) {
          return ChatGifModel(url: String(cString: tmpUrl), mark: String(cString: tmpMark), thumbnail: String(cString: tmpThumbnail), res_url: "", res: "", sound: "", stay_time: 0)
        }
      }
    }
    return nil
  }
  
  // 查全部
  func queryDBAll() -> [ChatGifModel]? {
    var allModel = [ChatGifModel]()
    var stmt: OpaquePointer? = nil
    let cQuerySQL = "SELECT *FROM 'GifTable'".cString(using: .utf8)
    if sqlite3_prepare_v2(db, cQuerySQL, -1, &stmt, nil) == SQLITE_OK {
      // 准备好之后进行解析
      while sqlite3_step(stmt) == SQLITE_ROW {
        if let tmpMark = UnsafePointer(sqlite3_column_text(stmt, 1)),
          let tmpUrl = UnsafePointer(sqlite3_column_text(stmt, 2)),
          let tmpThumbnail = UnsafePointer(sqlite3_column_text(stmt, 3)) {
          let chatGifModel = ChatGifModel(url: String(cString: tmpUrl), mark: String(cString: tmpMark), thumbnail: String(cString: tmpThumbnail), res_url: "", res: "", sound: "", stay_time: 0)
          allModel.append(chatGifModel)
        }
      }
    }
    return allModel
  }
  
  // 打开数据库 并且建表
  func openDB() -> Bool {
    func createTable() -> Bool {
      func createTableExecSQL(_ SQL_ARR: [String]) -> Bool {
        for item in SQL_ARR {
          if !execSQL(SQL: item) {
            return false
          }
        }
        return true
      }
      let creatUserTable = "CREATE TABLE IF NOT EXISTS 'GifTable' ( 'ID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,'mark' TEXT,'url' TEXT,'thumbnail' TEXT);"
      let creatUserGameTable = "CREATE TABLE IF NOT EXISTS 'GameGifTable' ( 'ID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,'mark' TEXT,'url' TEXT,'thumbnail' TEXT, 'res_url' TEXT, 'res' TEXT, 'sound' TEXT, 'stay_time' TEXT);"
      // 可能有多张表 以后在这里加就行
      return createTableExecSQL([creatUserTable, creatUserGameTable])
    }
    if let dicPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last {
      // 先搞数据库路径
      let DBPath = dicPath + "/chatGifdb.sqlite"
      let cDBPath = DBPath.cString(using: .utf8)
      // 如果打开了数据库
      if sqlite3_open(cDBPath, &db) != SQLITE_OK {
        print("打开数据库失败了")
        return false
      }
      // 建表
      return createTable()
    }
    return false
  }
  
  
  func removeDBSource() -> Void {
    closeDB()
    if let dicPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last {
      // 先搞数据库路径
      let DBPath = dicPath + "/chatGifdb.sqlite"
      sqlite3_close(db)
      do {
       try FileManager.default.removeItem(atPath: DBPath)
      } catch {
        print("删除失败")
      }
    }
  }

  
  // 执行数据库操作
  func execSQL(SQL: String) -> Bool {
    let cSQL = SQL.cString(using: .utf8)
    let errMsg: UnsafeMutablePointer<UnsafeMutablePointer<Int8>?>? = nil
    if sqlite3_exec(db, cSQL, nil, nil, errMsg) == SQLITE_OK {
      return true
    }
    print("SQL 语句执行出错 -> 错误信息: 一般是SQL语句写错了 \(String(describing: errMsg))")
    return false
  }
  
  
  
  
  
}
