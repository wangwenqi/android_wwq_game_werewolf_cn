//
//  ChatRoomUserModel.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/25.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import orangeLab_iOS

class ChatRoomUserModel: NSObject {
  
    enum EnterChatRoomState {
      case roomOfOnMicro      //  麦克上的人们
      case roomOfClose        //  封麦的空座位
      case roomOfEmpty        //  暂时无人的空座位
      case roomOfMaster
    }
  
//    var changeUserState = ""    // limit // free // ""
    var animation: Int?                                           // 人员动画
    var isGameing = true
    var votes = [Int]()
    var votesState: GameVoteState = .hideButton
    var masterReady = false
    var userRoomState: EnterChatRoomState = .roomOfEmpty          // 用户当前状态, 上麦房主等
    var isMaster: Bool = false                                    // 是否为房主
    var oderIndex: Int = -1                                       // 座位顺序(-1为未上麦)
    var countDownNumber = 0                                       // 用户发言倒计时
    var isDeath = false
    var isVoted = false
    var name = ""
    var is_escaped = false
    var group_id = ""
    var is_leaved = false
    var level: Int = 0
    var speaking = false
    var experience: Int = 0
    var position: Int = 999
    var status: Int = 0
    var is_disconnected = false
    var prepared = false
    var id = ""
    var signature = ""
    var avatar = ""
    var is_master = false
    var sex: Int = 0
    var state = ""
    var uid = ""
  
    var player: JSON? {
      didSet {
        if let player = player {
          name = player["name"].stringValue
          is_escaped = player["name"].boolValue
          group_id = player["group_id"].stringValue
          is_leaved = player["is_leaved"].boolValue
          level = player["level"].intValue
          speaking = player["speaking"].boolValue
          experience = player["experience"].intValue
          position = player["position"].intValue
          status = player["status"].intValue
          is_disconnected = player["is_disconnected"].boolValue
          prepared = player["prepared"].boolValue
          id = player["id"].stringValue
          signature = player["signature"].stringValue
          avatar = player["avatar"].stringValue
          is_master = player["is_master"].boolValue
          sex = player["sex"].intValue
          state = player["state"].stringValue
          uid = player["uid"].stringValue
        }
      }
    }
}
