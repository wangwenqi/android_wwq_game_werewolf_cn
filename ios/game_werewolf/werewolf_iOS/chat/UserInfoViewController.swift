//
//  UserInfoViewController.swift
//  game_werewolf
//
//  Created by Hang on 2017/6/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import ChatKit

class UserInfoViewController: RotationViewController {
  
    var peerID = ""
    var peerSex = ""
    var peerIcon = ""
    var peerName = ""
    var isFriend = true
    var isTourist = false
    var isself = false {
    didSet {
      if isself {
        self.header.addFriend.isHidden = true
        self.header.sendMessage.isHidden = true
        giftButton.isHidden = true
        }
      }
    }
    var comeFormRank = false
    var effectView:UIVisualEffectView?
    var Blocked = false
    var convId = "";
    lazy var imClient:AVIMClient = LCChatKit.sharedInstance().client
  
    let header = Bundle.main.loadNibNamed("userInfoHeaderView", owner: nil, options: nil)?.last as! userInfoHeaderView
    let family = Bundle.main.loadNibNamed("userInfoFamilyView", owner: nil, options: nil)?.last as! userInfoFamilyView
    var sign = Bundle.main.loadNibNamed("userInfoSignView", owner: nil, options: nil)?.last as! userInfoSignView
    var gameRecord = Bundle.main.loadNibNamed("userInfoGameRecord", owner: nil, options: nil)?.last as! userInfoGameRecordView
    let photoWall = Bundle.main.loadNibNamed("userInfoPhotoWallView", owner: nil, options: nil)?.last as! usreInfoPhotoWallView
    let gift = Bundle.main.loadNibNamed("userInfoGiftView", owner: nil, options: nil)?.last as! userInfoGiftView
    var comeFromFamily = false //是不是从家族进来的
    var scrollview:UIScrollView?
    var height:CGFloat = 0
    var giftHeight:CGFloat = 0
    let giftButton = SpringButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.lightGray
        navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "room_bg_navbar"), for: .default)
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "room_icon_leftarrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(UserInfoViewController.leftClicked))
      
        let moreButton = UIButton()
        moreButton.setImage(UIImage(named:"me_icon_more"), for: .normal)
      if #available(iOS 11, *) {
        moreButton.snp.makeConstraints { (make) in
          make.width.equalTo(27)
          make.height.equalTo(7)
        }
      } else {
        moreButton.frame = CGRect(x: 0, y: 0, width: 27, height: 7)
      }

        moreButton.addTarget(self, action: #selector(UserInfoViewController.rightClicked), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: moreButton)
  
        self.navigationItem.title =  NSLocalizedString("个人资料", comment: "")
        var rect = self.view.frame
        rect.origin.y = 0
        self.view.frame = rect
        scrollview = UIScrollView()
        scrollview?.frame = rect//CGRect(x: 0, y: 0, width: Screen.width, height: Screen.height)//self.view.frame
        self.scrollview?.backgroundColor = UIColor.hexColor("F7F7F7")
        self.view.addSubview(scrollview!)
        scrollview?.delegate = self
        if peerID == CurrentUser.shareInstance.id {
          isself = true
        }
        //get data
        getConentent()
      
    }
  
    func getConentent() {
      //如果这个人是游客
      if CurrentUser.shareInstance.isTourist {
        self.navigationItem.rightBarButtonItem = nil
        giftButton.isHidden = true
      }
      if peerID == CurrentUser.shareInstance.id {
        isself = true
      }
      if !self.isTourist {
        // 请求用户信息
        gameRecord.gameHistory.isHidden = true
        gameRecord.seeGameHistory.isHidden = false
        XBHHUD.showLoading()
        RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id": peerID], success: { [weak self] (json) in
          XBHHUD.hide()
          self?.setUpWith(json: json)
        }) { (code, message) in
          XBHHUD.hide()
          XBHHUD.showError(message)
          self.dismiss(animated: true, completion: nil);
        }
      
      }else{
        //游客隐藏举报
        self.navigationItem.rightBarButtonItem = nil // 是自己就隐藏
        generateHeaderView()
        header.delegate = self
        generateGapView(baseHeight:header.maxY)
        generateFamilyView()
        family.setTuroseModel()
        generateGapView(baseHeight: family.maxY)
        generateGameRecordView()
        gameRecord.gameHistory.isHidden = true
        gameRecord.seeGameHistory.isHidden = true
        gameRecord.setDefaultInfo()
        generateGapView(baseHeight: gameRecord.maxY)
        header.showDelegate = self
        photoWall.delegate = self
        photoWall.frame = CGRect(x: 0, y: (self.gameRecord.maxY) + 10, width: Screen.width, height:60)
        scrollview?.addSubview(photoWall)
        photoWall.photoWall.reloadData()
        generateGapView(baseHeight: photoWall.maxY)
        generateGiftView()
        gameRecord.gameHistory.isHidden = true
        gameRecord.seeGameHistory.isHidden = true
        gift.delegate = self
        self.giftHeight = 80
        self.gift.frame = CGRect(x: 0, y: (self.photoWall.maxY) + 10, width: Screen.width, height:giftHeight)
        scrollview?.contentSize = CGSize(width: 0, height: photoWall.maxY + 64 + 10 + self.giftHeight)
      }
  
      if isself  || isTourist {
        self.header.addFriend.isHidden = true
        self.header.sendMessage.isHidden = true
        giftButton.isHidden = true
      }
      //排行榜查看个人主页隐藏发消息
      if comeFormRank {
        self.header.sendMessage.isHidden = true
        //加好友的按钮重新
        self.header.addFriend.snp.makeConstraints({ (make) in
          make.right.equalTo(self.header.snp.right).offset(-20)
        })
      }
    }
  
    func setUpWith(json:JSON){
      print(json.dictionary)
      generateHeaderView()
      header.setNeedsLayout()
      header.layoutIfNeeded()
      if let isBlock = json["isBlock"].bool {
        if isBlock == true {
          Blocked = isBlock
          let effect = UIBlurEffect.init(style: .light)
          effectView = UIVisualEffectView.init(effect: effect)
          self.view.addSubview(effectView!)
          effectView?.snp.makeConstraints({ (make) in
            make.width.equalTo(self.view.snp.width)
            make.top.equalTo(self.header.snp.bottom)
            make.bottom.equalTo(self.view.snp.bottom)
          })
          //黑名单图标
          let blockView = UIImageView()
          blockView.image = UIImage(named: "blockImage")
          blockView.contentMode = .scaleAspectFit
          effectView?.contentView.addSubview(blockView)
          blockView.snp.makeConstraints({ (make) in
            make.width.equalTo(60)
            make.height.equalTo(60)
            make.centerX.equalTo((self.effectView?.snp.centerX)!)
            make.centerY.equalTo((self.effectView?.snp.centerY)!).offset(-20)
            
          })
          //黑名单文字
          let lable = UILabel()
          lable.text = NSLocalizedString("此人已经被你拉黑，如需解除拉黑，请前往“设置-黑名单”左滑移除", comment: "")
          self.effectView?.contentView.addSubview(lable)
          lable.snp.makeConstraints({ (make) in
            make.top.equalTo(blockView.snp.bottom).offset(0)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(80)
          })
          lable.font = UIFont.systemFont(ofSize: 13)
          lable.textColor = UIColor.black
          lable.textAlignment = .center
          lable.numberOfLines = 3
          self.scrollview?.isScrollEnabled = false
          giftButton.isHidden = true
          self.header.addFriend.isHidden = true
          self.header.sendMessage.isHidden = true
        }
      }
      peerSex = json["sex"].stringValue
      header.setUpWith(json: json)
      if peerID.lowercased() == Config.customCareUserID {
        self.header.sendMessage.isHidden = true;
      }
      header.delegate = self
      header.frame = CGRect(x: 0, y: 0, width: Screen.width,height: header.level.maxY + 9 + header.tagView.contentSize.height + 50 )
      //设置签名
      header.signContent.text = json["signature"].stringValue
      if json["signature"].stringValue.isEmpty || json["signature"].stringValue == "" {
        if isself {
          header.signContent.text = NSLocalizedString(NSLocalizedString("写一段个性签名，释放你的个性", comment: ""), comment: "")
          header.signContent.textColor = UIColor.gray
        }else{
          header.signContent.text = NSLocalizedString(NSLocalizedString("Ta很懒，没有留下签名", comment: ""), comment: "")
          header.signContent.textColor = UIColor.gray
        }
      }
      generateGapView(baseHeight:header.maxY)
      if comeFromFamily {
        generateGameRecordView()
        var rect = gameRecord.frame
        rect.origin.y = header.maxY + 10
        gameRecord.frame = rect
      }else{
        generateFamilyView()
        family.setUpWith(json: json)
        family.isMyself = isself
        generateGapView(baseHeight: family.maxY)
        generateGameRecordView()
      }
      gameRecord.setUpWith(json: json)
      gameRecord.goToDes = { [weak self] in
        let web = StoreCenterViewController()
        web.type = .BattleLevel
        let nav = RotationNavigationViewController(rootViewController: web)
        self?.present(nav, animated: true, completion: nil)
      }
      gameRecord.userid = peerID
      generateGapView(baseHeight: gameRecord.maxY)
      header.showDelegate = self
      photoWall.delegate = self
      //是好友就隐藏加好友
      self.isFriend = json["is_friend"].boolValue
      if (self.isFriend) {
        self.header.addFriend.isHidden = true
      }

      //设置图片墙
      if let photos = json["photos"].array {
        for item in photos {
          var dictionary = item.dictionary
          if let urlString = dictionary?["url"]?.string {
            self.photoWall.photo.add(urlString)
          }
        }
      }
      if self.photoWall.photo.count > 0 {
        self.photoWall.noticeLable.isHidden = true
      }
      if self.photoWall.photo.count == 0 {
          self.height = 60
      }else{
        let singleWidth = (Screen.width - 25 - 35)/3
        let row = ceilf((Float((self.photoWall.photo.count)) / Float(3)))
        self.height = CGFloat(Float(singleWidth ) * row + (row) * 5 + 60)
      }
      self.photoWall.frame = CGRect(x: 0, y: (self.gameRecord.maxY) + 10, width: Screen.width, height:(self.height))
      scrollview?.addSubview(photoWall)
      if isself {
        self.photoWall.noticeLable.text = NSLocalizedString("上传照片，展示最美的自己", comment: "")
        self.navigationItem.rightBarButtonItem = nil // 是自己就隐藏
      }else{
        self.photoWall.noticeLable.text = NSLocalizedString("Ta很懒，暂时没有上传个人照片", comment: "")
      }
      photoWall.photoWall.reloadData()
      
      generateGapView(baseHeight: photoWall.maxY)
      generateGiftView()
      gift.delegate = self
      if isself {
        self.gift.noticeLable.text = NSLocalizedString("多增加好友互动，可以有机会收到好多礼物", comment: "")
        gameRecord.gameHistory.isHidden = true
        gameRecord.seeGameHistory.isHidden = false
      }else{
        self.gift.noticeLable.text = NSLocalizedString("赠送给ta一个小礼物，表达你的心意", comment: "")
        gameRecord.gameHistory.isHidden = true
        gameRecord.seeGameHistory.isHidden = true
      }
      gift.setup(withJson: json["gift"])
      scrollview?.contentSize = CGSize(width: 0, height: gift.maxY + 64)
      isself = json["id"].stringValue == CurrentUser.shareInstance.id ? true : false
    }
  
    func generateHeaderView() {
      header.frame = CGRect(x: 0, y: 0, width: Screen.width, height: 160)
      scrollview?.addSubview(header)
    }
  
    func generateGapView(baseHeight:CGFloat) {
        let gap = Bundle.main.loadNibNamed("userInfoGapView", owner: nil, options: nil)?.last as! UIView
        gap.frame = CGRect(x: 0, y: baseHeight, width: Screen.width, height: 10)
        scrollview?.addSubview(gap)
    }
  
    func generateSignView() {
      sign.frame = CGRect(x: 0, y: header.maxY + 10, width: Screen.width, height: 50)
      scrollview?.addSubview(sign)
    }
  
    func generateFamilyView() {
      family.frame = CGRect(x: 0, y: header.maxY + 10, width: Screen.width, height: 140)
      scrollview?.addSubview(family)
    }
    func generateGameRecordView() {
      gameRecord.frame = CGRect(x: 0, y: family.maxY + 10, width: Screen.width, height: 170)
      scrollview?.addSubview(gameRecord)
    }
  
    func generatePhotoWallView() {
      photoWall.frame = CGRect(x: 0, y: gameRecord.maxY + 10, width: Screen.width, height:80)  //Screen.width - 70
      scrollview?.addSubview(photoWall)
    }
  
    func generateGiftView() {
      gift.frame = CGRect(x: 0, y: photoWall.maxY + 10, width: Screen.width, height:80)  //Screen.width - 70
      scrollview?.addSubview(gift)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      
      let giftImage = UIImage(named: "送礼物图标")
      giftButton.frame = CGRect(x: Screen.width - 60, y: Screen.height * 0.6, width:60, height: 60)
      giftButton.setImage(giftImage, for: .normal)
      giftButton.addTarget(self, action: #selector(showSendGift), for:.touchUpInside)
      self.view.addSubview(giftButton)
      self.view.bringSubview(toFront: giftButton)
      giftButton.animation = "shake"
      giftButton.animate()
      
    }
  
    func rightClicked() {
    
      var actionSheet:UIActionSheet?
      if self.isFriend {
        
        if Blocked {
            actionSheet = UIActionSheet.init(title: NSLocalizedString("更多", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("取消", comment: ""), destructiveButtonTitle: nil, otherButtonTitles: NSLocalizedString("解除好友关系", comment: ""),NSLocalizedString("举报", comment: ""))
        }else{
            actionSheet = UIActionSheet.init(title: NSLocalizedString("更多", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("取消", comment: ""), destructiveButtonTitle: nil, otherButtonTitles: NSLocalizedString("解除好友关系", comment: ""),NSLocalizedString("拉黑", comment: ""),NSLocalizedString("举报", comment: ""))
        }
      }else{
        if Blocked {
            actionSheet = UIActionSheet.init(title: NSLocalizedString("更多", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("取消", comment: ""), destructiveButtonTitle: nil, otherButtonTitles: NSLocalizedString("举报", comment: ""))
        }else{
            actionSheet = UIActionSheet.init(title: NSLocalizedString("更多", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("取消", comment: ""), destructiveButtonTitle: nil, otherButtonTitles: NSLocalizedString("拉黑", comment: ""),NSLocalizedString("举报", comment: ""))
        }
      }
      actionSheet?.show(in: view)
      
    }
    func leftClicked() {
      dismiss(animated: true, completion: nil)
    }
  
    func showSendGift() {
      let gift = giftView.initGiftViewWith(envtype: .PROFILE, People:peerName)
      gift.toID = peerID
      gift.delegate = self
      Utils.getKeyWindow()?.addSubview(gift)
      gift.animation = "fadeIn"
      gift.animate()
      gift.getGiftFormRemote()
    }
  
    deinit {
      //告诉首页刷新
      RNMessageSender.emitEvent(name: "EVENT_USER_LEAVE_FROM_WEB")
    }
}

extension UserInfoViewController: giftSend {
  
  func sendGift(type: giftType, to: PlayerView?) {
    
  }
  
  func sendGiftWithRebate(type: String, to: PlayerView?, json: JSON) {
    
    //重新获取数据
    self.gift.totalGifts = 0 //重制为0
    self.photoWall.photo.removeAllObjects()
    self.photoWall.photoArr.removeAll()
    if !self.isTourist {
      // 请求用户信息
      XBHHUD.showLoading()
      RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id": peerID], success: { [weak self] (json) in
        XBHHUD.hide()
        XBHHUD.showSuccess(NSLocalizedString("赠送礼物成功", comment: ""))
        self?.setUpWith(json: json)
      }) { (code, message) in
        XBHHUD.hide()
      }
    }
    
    if self.peerID == CurrentUser.shareInstance.id {
      return
    }
    
    var rebateValue = ""
    if let rebate = json["rebate"].dictionary {
      if let reValue = rebate["value"]?.int {
        rebateValue = "\(reValue)"
      }
    }
    // 发送消息
    imClient.createConversation(withName: peerName,
                                clientIds: [peerID],
                                attributes: nil,
                                options: AVIMConversationOption.unique,
                                callback: { (conversation, error) in

                                  let sendMessage = OLGiftMessage.init(attibutes: [
                                    "USER_SEX": "\(CurrentUser.shareInstance.sex)",
                                    "USER_ICON": CurrentUser.shareInstance.avatar,
                                    "USER_NAME": CurrentUser.shareInstance.name,
                                    "USER_ID": CurrentUser.shareInstance.id,
                                    GIFT_MSG_KEY_GITF_TYPE:type,
                                    "GIFT_REBATE":rebateValue,
                                    "APP": Config.app,
                                    "CHAT_TIME": Utils.getCurrentTimeStamp()
                                    ]);

                                  //此时也要发送通知，告诉RN更新
                                  conversation?.send(sendMessage, callback: { (isSucceeded, error) in
                                    if isSucceeded {
                                      //此时也要发送通知，告诉RN更新
                                      let dic = ["USER_NAME":self.peerName,"USER_SEX":self.peerSex,"USER_ICON":self.peerIcon,"APP": Config.app,"MESSAGE_TYPE":"MESSAGE_TYPE_CHAT","USER_ID":self.peerID,"CHAT_TIME":String(Utils.getCurrentTimeStamp()),"CHAT_MESSAGE":NSLocalizedString("🎁赠送成功", comment: ""),"READ":true] as [String : Any]
                                      NotificationCenter.default.post(name: NotifyName.kSendMessToRN.name(), object: dic)
                                    }
                                  });

    });
  }

  func sendGift(typeString:String, to: PlayerView?) {
   
    //重新获取数据
    self.gift.totalGifts = 0 //重制为0
    self.photoWall.photo.removeAllObjects()
    self.photoWall.photoArr.removeAll()
    if !self.isTourist {
      // 请求用户信息
      XBHHUD.showLoading()
      RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id": peerID], success: { [weak self] (json) in
        XBHHUD.hide()
        XBHHUD.showSuccess(NSLocalizedString("赠送礼物成功", comment: ""))
        self?.setUpWith(json: json)
      }) { (code, message) in
         XBHHUD.hide()
      }
    }
    
    // 发送消息
    imClient.createConversation(withName: peerName,
                                 clientIds: [peerID],
                                 attributes: nil,
                                 options: AVIMConversationOption.unique,
                                 callback: { (conversation, error) in
 
                                  let sendMessage = OLGiftMessage.init(attibutes: [
                                    "USER_SEX": "\(CurrentUser.shareInstance.sex)",
                                    "USER_ICON": CurrentUser.shareInstance.avatar,
                                    "USER_NAME": CurrentUser.shareInstance.name,
                                    "USER_ID": CurrentUser.shareInstance.id,
                                    "APP": Config.app,
                                    GIFT_MSG_KEY_GITF_TYPE:typeString,
                                    "CHAT_TIME": Utils.getCurrentTimeStamp()
                                    ]);
                                  
                                  //此时也要发送通知，告诉RN更新
                                  conversation?.send(sendMessage, callback: { (isSucceeded, error) in
                                    if isSucceeded {
                                      //此时也要发送通知，告诉RN更新
                                      let dic = ["USER_NAME":self.peerName,"APP": Config.app,"USER_SEX":self.peerSex,"USER_ICON":self.peerIcon,"MESSAGE_TYPE":"MESSAGE_TYPE_CHAT","USER_ID":self.peerID,"CHAT_TIME":String(Utils.getCurrentTimeStamp()),"CHAT_MESSAGE":NSLocalizedString("🎁赠送成功", comment: ""),"READ":true] as [String : Any]
                                       NotificationCenter.default.post(name: NotifyName.kSendMessToRN.name(), object: dic)
                                    }
                                  });
                                  
    });
  }
}

extension UserInfoViewController: userInfoHeaderDelegate {
  
  func addFriend() {
    if !isTourist && CurrentUser.shareInstance.isTourist == false {
      //在黑名单中的不能添加好友
      if let block = Utils.getCachedBLContent() {
        if peerID != nil {
          if block[peerID] != nil {
            XBHHUD.showError("此人已经被你拉黑")
            return
          }
        }else{
          return
        }
      }
      RequestManager().post(url: RequestUrls.friendAdd, parameters: ["friend_id":peerID], success: { (json) in
        XBHHUD.showTextOnly(text: NSLocalizedString("好友请求发送成功！", comment: ""))
      }) { (code, message) in
        if message == "" {
          XBHHUD.showTextOnly(text: NSLocalizedString("好友请求发送失败", comment: ""))
        } else {
          XBHHUD.showTextOnly(text: "\(message)")
        }
      }
    }else{
        XBHHUD.showError(NSLocalizedString("请您先去登录", comment: ""))
    }
  }
  
  func sendMessage() {
    if !isTourist && CurrentUser.shareInstance.isTourist == false {
      var controller:ChatViewController?;
      if (convId != "") {
        controller = ChatViewController.init(conversationId: convId);
      } else {
        controller = ChatViewController.init(peerId: peerID);
      }
        controller?.peerSex = peerSex
        controller?.peerName = peerName
        controller?.peerIcon = peerIcon
        controller?.peerID = peerID
        navigationController?.pushViewController(controller!, animated: true)
    }else{
        if CurrentUser.shareInstance.isTourist {
            XBHHUD.showError(NSLocalizedString("请您先去登录", comment: ""))
        }
    }
  }
}

extension UserInfoViewController:UIScrollViewDelegate {

  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    
    giftButton.animation = "shake"
    giftButton.animate()
  }
}


extension UserInfoViewController: goToGiftList {
  func goToGiftList() {
   let giftList = userGiftListViewController()
    giftList.userid = peerID
    giftList.view.frame = CGRect(x: 0, y: 0, width: Screen.width, height: Screen.height)
    self.navigationController?.pushViewController(giftList, animated: true)
  }
}

extension UserInfoViewController:showPhotoDelegate {
  func showPhoto(_ view: MWPhotoBrowser) {
      self.navigationController?.pushViewController(view, animated: true)
  }
}

extension UserInfoViewController:UIActionSheetDelegate {
  
  func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
    if self.isFriend {
      switch buttonIndex {
      case 1:
        //解除好友关系
        deletefriend()
        break
      case 2:
        //拉黑
        addToBL()
        break
      case 3:
        //举报
        report()
      default:
        break
      }
    }else{
      switch buttonIndex {
      case 1:
        //拉黑
        addToBL()
        break
      case 2:
        //举报
        //弹出新的对话框
        report()
        break
      default:
        break
      }
    }
  }
  
  func addToBL() {
    XBHHUD.showLoading()
    RequestManager().get(url: RequestUrls.block + "/\(peerID)", success: {[weak self] (json) in
      XBHHUD.hide()
      XBHHUD.showSuccess(NSLocalizedString("拉黑成功！", comment: ""))
      Utils.addToBL(id: (self?.peerID)!)
      if (Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)?.isKind(of: UserInfoViewController.self))! {
        //如果还在当前页面就退出
        self?.dismiss(animated: true, completion: nil)
      }
    }) { (code, message) in
      XBHHUD.hide()
      XBHHUD.showError(NSLocalizedString("拉黑失败！", comment: ""))
    }
  }
  
  func deletefriend() {
    let localtitle = NSLocalizedString("您确定要删除好友", comment: "")
    let localend = NSLocalizedString("吗", comment: "")
    let alert = UIAlertController(title: "", message: "\(localtitle) \(self.peerName) \(localend)", preferredStyle: .alert)
    let cancle = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: .cancel) { (aciton) in
    }

    let confirm = UIAlertAction(title: NSLocalizedString("确定", comment: ""), style: .default) { (action) in
      RequestManager().post(url: RequestUrls.deletefriend, parameters: ["friend_id":self.peerID], success: {[weak self] (json) in
        XBHHUD.showSuccess(NSLocalizedString("删除好友成功", comment: ""))
        //告诉RN删除成功
        if let userid = self?.peerID {
          SwiftConverter.shareInstance.sendMessagesToRN(json:(self?.generateRNMessage(user_id:userid))!)
        }
        if (Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)?.isKind(of: UserInfoViewController.self))! {
          //如果还在当前页面就退出
          self?.dismiss(animated: true, completion: nil)
        }
      }, error: { (code, message) in
          XBHHUD.showError(message)
      })
      
    }
    alert.addAction(cancle)
    alert.addAction(confirm)
    self.present(alert, animated: true, completion: nil)
  }
  
  func generateRNMessage(user_id:String) -> JSON {
    let data:Dictionary<String,Any> = ["action":"ACTION_DELETE_FRIEND","params":["user_id":user_id],"options":["needcallback":false,"sync":true]]
    let json = JSON.init(data)
    return json
  }

  func report() {
    let report = Bundle.main.loadNibNamed("reportView", owner: nil, options: nil)?.last as! reportView
    report.peerId = self.peerID
    report.setup(peerName)
    report.form = .PROFILE
    Utils.showNoticeView(view: report)
  }
  
}
