//
//  AppDelegate+RedPacket.h
//  ChatKit-OC
//
//  Created by 都基鹏 on 16/8/22.
//  Copyright © 2016年 ElonChan. All rights reserved.
//
#ifdef XIAOYU
#import "xiaoyu-Swift.h"
#else
#import "game_werewolf-Swift.h"
#endif


@interface AppDelegate (RedPacket)

+ (void)swizzleRedPacketMethod;

@end
