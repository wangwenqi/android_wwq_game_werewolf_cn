//
//  GetKeys.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/15.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

@objc enum KeyType: Int {
  
  case kFBAdKey             // facebook闪屏图广告key
  case kFBAdNaviveKey       // facebook领取金币游戏结束的广告
  case kFBAdGameOverKey     // facebook游戏结束广告
  case kFBAdGameDeadKey     // facebook游戏中死亡广告
  case kFBAdEnterRoomKey    // facebook进入游戏房间广告
  case kFBAdRoundBannerKey  // facebook转盘横幅广告
  case kFBAdRoundGetKey     // facebook转盘领奖广告
  case kLeanCloudId_CN      // LeanCloudID_cn
  case kleanCloudKey_CN     // leanCloudkey_cn
  case kLeanCloudId_US      // leanCloudID_us
  case kleanCloudKey_US     // leanCloudKey_us
  //  case
  
  func keys() -> String {
    switch self {
    case .kFBAdKey:
      return "341507119614410_347856522312803"
    case .kFBAdNaviveKey:
      return "341507119614410_357352281363227"
    case .kFBAdGameOverKey:
      return "341507119614410_357453428019779"
    case .kFBAdGameDeadKey:
      return "341507119614410_357452584686530"
    case .kFBAdEnterRoomKey:
      return "341507119614410_357451938019928"
    case .kFBAdRoundBannerKey:
      return "341507119614410_361254317639690"
    case .kFBAdRoundGetKey:
      return "341507119614410_361258257639296"
    // 这些还没有换
    case .kLeanCloudId_CN:
      return "bcuITM75nQJu5fNoRd2EmppI-gzGzoHsz"
    case .kleanCloudKey_CN:
      return "HE7kvoQ7ddPC7AKuAHKfl4ft"
    case .kLeanCloudId_US:
      return "fjckqKaLIXfrIqHMVFwzvzhx-MdYXbMMI"
    case .kleanCloudKey_US:
      return "Yb24WAXknbANFVMYMjB8YUIG"
    }
  }
}

class GetKeys: NSObject {
  class func info(_ type: KeyType) -> String {
    return type.keys()
  }

}
