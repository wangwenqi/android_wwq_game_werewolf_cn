//
//  GetNotificationName.swift
//  game_werewolf
//
//  Created by Tony on 2017/8/15.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

@objc enum NotifyName: Int {
  
  case kAbortApp // appdelegate中关于程序的通知
  case kEventEmitted //
  case kSocketDidOpen
  case kEnter
  case kBackGroundModel
  case kEnterToGameRoom // 进入房间的通知
  case kSendMessToRN    // chatKit退出登录的通知
  case kChatKitLogInOut //
  case kToolBarGifCanSelected
  case kSendGift
  
  func name() -> Notification.Name {
    switch self {
    case .kAbortApp:
      return Notification.Name("abortApplication")
    case .kEventEmitted:
      return Notification.Name("event-emitted")
    case .kSocketDidOpen:
      return Notification.Name("socketDidOpen")
    case .kEnter:
      return Notification.Name("enter")
    case .kBackGroundModel:
      return Notification.Name("kBackGroundModel")
    case .kEnterToGameRoom:
      return Notification.Name("OL_enterToGameRoomNotificationName")
    case .kSendMessToRN:
      // 此字符串不要修改 在LCCKSessionService.m中 Line402有使用字符
      return Notification.Name("sendMessageToRNNotificationName")
    case .kChatKitLogInOut:
      return Notification.Name("chatKitLoginOutNotificationName")
    case .kToolBarGifCanSelected:
      return Notification.Name("kToolBarGifCanSelected")
    case .kSendGift:
      return Notification.Name("kSendGift")
    }
  }
}

@objc(OCNotifyName)
class OCNotifyName: NSObject {
  @objc class func name(_ type: NotifyName) -> String {
    return "\(type.name())"
  }
}
