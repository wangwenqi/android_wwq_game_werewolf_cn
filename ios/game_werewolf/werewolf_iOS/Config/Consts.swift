//
//  Consts.swift
//  werewolf_iOS
//
//  Created by QiaoYijie on 17/3/13.r
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation

private var timezone = "";

struct Config {
  // appStore的版本号
  static let appStoreVersion = 1.02
  static let platform = "ios"
  static let serverVersion = 8
  static let app = "werewolf_cn"
  static let devSetting: DevSetting = .release
  static let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] ?? 1
  static let buildShortVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] ?? 1
  // 客服的ID
  static let customCareUserID = "5923d10843f4e7000773274b"
  static let systemLanage:String =
    Bundle.main.preferredLocalizations.count > 0 ?
    Bundle.main.preferredLocalizations[0] : "zh-Hans-CN"
  static var currentTimeZoneGMT:String {
    get {
      if timezone == "" {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "z";
        timezone = dateFormat.string(from: Date())
      }
      return timezone;
    }
  }
  
  static var urlHost: String {
    get {
      switch devSetting {
      case .dev:
        return UrlHost.urlDevHost
      case .test:
        return UrlHost.urlTestHost
      case .release:
        return UrlHost.urlReleaseHost
      case .given:
        return ""
      }
    }
  }
}

// 本地存储的变量
struct LocalSaveVar {
  static var controlconnectID = ""
  static var connectID = ""
//  static var appStoreUrl = "https://itunes.apple.com/cn/app/id1228173973?mt=8"
  static var appStoreUrl = "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1228173973"
}


enum DevSetting: String {
  case test
  case dev
  case release
  case given
}

struct UrlHost {

  static let testHost = "192.168.18.110"
  static let devHost = "qingyong.intviu.cn"
  static let releaseHose = "werewolf-cn.xiaobanhui.com"
  

  static let urlTestHost = "http://\(UrlHost.testHost):8100"
//  static let urlDevHost = "http://192.168.8.4:8100"
  static let urlDevHost = "http://staging.intviu.cn:8200"
  static let urlReleaseHost = "https://\(UrlHost.releaseHose)"
}

// 所有请求接口
struct RequestUrls {
  static let userInfo = Config.urlHost + "/user/info"          // 获取玩家信息
  static let friendAdd = Config.urlHost + "/friend/add"        // 添加好友
  static let friendList = Config.urlHost + "/friend/list"      // 好友列表
  static let upgrade = Config.urlHost + "/upgrade"             // 检查更新
  static let gift = Config.urlHost + "/gift/list"              // 礼物价格
  static let gameGif = Config.urlHost + "/expression/list"      // 鼓掌價格
  static let buy = Config.urlHost + "/gift/buy_v2"              // 购买礼物
  static let add = Config.urlHost + "/test/addWealth"           //自己充值
  static let giftHistory = Config.urlHost + "/gift/history"     //礼物历史
  static let unusedCards = Config.urlHost + "/wealth/unusedCards"  //未使用卡片
  static let addCard = Config.urlHost + "/test/addWealth"          //添加卡片
  static let getSocketServerHost = Config.urlHost + "/server/get"; // 获取服务器返回的地址
  static let getAudioSocketServerGet = Config.urlHost + "/server/get" // 语音房间的serverGet
  static let sendTaskData = "\(Config.urlHost)/task/do";           // 做任务时告诉服务器
  static let sendCard = Config.urlHost + "/gift/give"                 //送卡牌
  static let serviceRegion = Config.urlHost + "/lean/config"          //服务器选择
  static let report = Config.urlHost + "/account/report"              //举报
  static let canplay = Config.urlHost + "/isnoplay"                     //是否可以玩游戏
  static let iap = Config.urlHost + "/pay/applePay"                    //内购
  static let order = Config.urlHost + "/pay/generateOrderID"           //获取订单号
  static let deletefriend = Config.urlHost + "/friend/delete"           //删除好友
  static let showAdConfig = Config.urlHost + "/config/gg"              // 广告配置
  static let rejectFriend = Config.urlHost + "/friend/reject"    //拒绝好友
  static let acceptFriend = Config.urlHost + "/friend/accept"    //加好友
  static let exportRoom = Config.urlHost + "/room/export"        //房间导出功能
  static let resumeApp = Config.urlHost + "/user/status/resumeApp"  // 进入App
  static let leaveApp = Config.urlHost + "/user/status/leaveApp"  // 进入App
  static let block = Config.urlHost + "/block/add"               //加入黑名单
  static let blockRmove = Config.urlHost + "/block/remove"       //移除黑名单
  static let blockList = Config.urlHost + "/block/list/withInfo"   //黑名单列表
  static let allBlockList = Config.urlHost + "/block/list/all"         //全部黑名单列表
  static let audioEmot = Config.urlHost + "/config/audio_emoticon"         // 全部的后台Gif图片
  static let audioConfig = Config.urlHost + "/config/audio"       // 语音房间环境
  static let groupInfo = Config.urlHost + "/group/info/"             //家族信息
  static let groupMembers = Config.urlHost + "/group/members"        //家族成员
  static let updateGroup = Config.urlHost + "/group/update"         //更新家族信息
  static let groupRequset = Config.urlHost +  "/group/member/require_list"    //申请列表
  static let groupRemove = Config.urlHost +  "/group/member/remove/"          //移除列表
  static let qnToken = Config.urlHost +  "/image/token"                       //生成token
  static let reject_all = Config.urlHost +  "/group/member/reject_all"        //拒绝所有
  static let accept_all = Config.urlHost +  "/group/member/accept_all"        //同意所有
  static let quit = Config.urlHost +  "/group/member/quit"                     //退出家族
  static let addToGroup = Config.urlHost +  "/group/member/accept/"            //同意加入
  static let changeMater = Config.urlHost +  "/group/member/change_own/"        //转让族长
  static let unfinishedGame = Config.urlHost +  "/server/disconnectInfo"        //未完成的游戏
  static let leaveUnfinishedGame = Config.urlHost +  "/server/escape"           //离开未完成的游戏
  static let chatRoomBlackList = Config.urlHost + "/block/list/withInfo"        // 语音房黑名单
  static let getNewGifs = Config.urlHost +  "/gifts/ios"                        //获取新的礼物
  static let setPosition = Config.urlHost +  "/group/member/change_title/"     //设置官职
  static let requestJoin = Config.urlHost +  "/group/member/join/"             //申请加入家族
  static let gameList = Config.urlHost +  "/game/list"                         //小游戏列表
  
}

// 消息类型
enum GameMessageType: String {
  // game
  case INIT = "init"               // 初始化
  case createRoom                  // 创建房间
  case enterRoom = "enter"         // 进入房间
  case join                        // 其他玩家进入房间
  case leave                       // 有玩家离开
  case prepare                     // 准备
  case unprepare                   // 取消准备
  case start                       // 开局
  case update_master               // 房主变更
  case assigned_role               // 分配角色
  case link                        // 爱神连接
  case link_result                 // 爱神连接结果
  case sunset                      // 日落
  case sunup                       // 日出
  case death_info                  // 死亡信息
  case speech                      // 说话
  case vote                        // 投票
  case vote_result                 // 投票结果
  case speak                       // 发言
  case unspeak                     // 取消发言
  case wake_to_kill                // 狼人准备杀人
  case kill                        // 杀人
  case kill_result                 // 狼人杀人结果
  case check                       // 预言家查人
  case check_result                // 查验结果
  case save                        // 女巫救人 （救的话传position， 不救的话什么都不传）
  case poison                      // 女巫毒人
  case take_away                   // 猎人带走
  case take_away_result            // 猎人带走结果
  case game_over                   // 游戏结束
  case disconnected                 // 游戏离开
  case connected                    // 游戏恢复
  case lock                        // 上锁
  case unlock                      // 解锁
  case kick_out                    // 踢出
  case mute_all                    // 全体禁言
  case unmute_all                  // 解除禁言
  case end_speech                  // 结束发言
  case chat                        // 聊天
  case connect_info                // 确定连接成功，返回ID，用于重连
  case apply                       // 是否参加竞选警长
  case apply_result                // 最终参加竞选的人警长
  case give_up_apply               // 放弃竞选
  case sheriff_result              // 警长
  case hand_over_result            // 警长交接结果
  case hand_over                   // 警长交接
  case restore_room                // 重连获取信息
  case unactive_warning            // 长时间未操作的提示
  case `continue`                  // 继续游戏
  case change_password             // 修改密码
  case reset_master                // 更换房主
  case system_msg                  // 系统消息
  case apply_role                  // 抢角色
  case apply_role_result           // 抢角色结果
  case append_time                 // 调用延迟卡
  case append_time_result          // 延时卡使用的结果
  case card_check                  // 使用查验卡
  case card_check_result           // 查验卡的结果
  case olive_changed               // 服务器进行更换了
  //游戏内加好友
  case add_friend                  // 添加好友
  //语音房
  case up_seat                     // 上麦
  case down_seat                   // 下麦
  case update_title                // 语音房更改主题
  case force_seat                  // 抱人上麦
  case change_user_state           // 对某个人进行操作 禁麦/自由/随意模式'|limit|""
  case request_free_mode           // 请求打开自由模式
  case accept_free_mode            // 接受了自由模式
  case reject_free_mode            // 拒绝了自由模式
  case show_emoticon               // 发表情gif
  case show_game_emoticon          // 发游戏表情gif
  case like_room                   // 房间内点赞
  case block_user_list             // 黑名单
  case unblock_user                // 解除黑名单
  case uc_get_words                // 获取词语 system|custom
  case uc_get_words_result         // 获取词语的返回
  case purchase_room               // 购买房间
  case request_master              // 创建者强制成为房主
  case handover_creator            // 转交房契
  case uc_start_config
  case uc_stop_config
  case uc_update_config
  case uc_guess_word                   // 猜词框出现
  case uc_guess_word_result            // 猜词动作,发词语
  case comment
  //守衛
  case protect                      //守卫保护
  case protect_result               //守卫结果
  //白狼王模式
  case boom                         //自爆
  case boom_away                    //自爆带走人
  case boom_away_result             //自爆带人结果
  case boom_state                   //是否可以自爆
  
  case handover_master             // 移交房主
  // other
  case socketDidOpen
  case reconnect
  //召集令export
  case export                      //召集令
  case export_pay                  //花钱的召集令
  //女巫毒人
  case poison_result               //毒人结果
  case save_result                 //救人结果
  //更新config
  case  update_config               //更新config
  case  speech_direction             //发言方向
  case  speech_direction_result      //发言方向结果
  case  request_reset_master         //请求更换房主
  case  reset_master_result          //更换房主结果
  case  reject_reset_master          //拒绝更换房主
  //游戏内送礼物
  case send_gift                     //游戏内送礼物
  case send_card                     //游戏内送卡牌
  
  case can_speak                     //狼人夜间说话功能
  //魔术师
  case exchange                      //交换
  case exchange_result               //交换结果
  //恶魔
  case demon_check
  case demon_check_result 
  var notificationName: Notification.Name {
    return Notification.Name.init(self.rawValue)
  }
}

enum GameRole: String {
  case seer             // 预言家
  case werewolf         // 狼人
  case people           // 村名
  case witch            // 女巫
  case hunter           // 猎人
  case cupid            // 爱神
  case sheriff          // 警长
  case iguard            //守卫
  case werewolf_king      //狼王
  case demon               //👿
  case magician           //魔术师
  case none
  
  static func getRole(name: String) -> GameRole {
    switch name {
    case "seer":
      return .seer
    case "werewolf":
      return .werewolf
    case "people":
      return .people
    case "witch":
      return .witch
    case "hunter":
      return .hunter
    case "cupid":
      return .cupid
    case "sheriff":
      return .sheriff
    case "guard":
      return .iguard
    case "werewolf_king":
      return .werewolf_king
    case "magician":
      return .magician
    case "demon":
      return .demon
    default:
      return .none
    }
  }
  
  var descriptionString: String {
    switch self {
    case .seer:
      return NSLocalizedString("预言家", comment: "")
    case .werewolf:
      return NSLocalizedString("狼人", comment: "")
    case .people:
      return NSLocalizedString("村民", comment: "")
    case .witch:
      return NSLocalizedString("女巫", comment: "")
    case .hunter:
      return NSLocalizedString("猎人", comment: "")
    case .cupid:
      return NSLocalizedString("爱神", comment: "")
    case .sheriff:
      return NSLocalizedString("警长", comment: "")
    case .iguard:
      return NSLocalizedString("守卫", comment: "")
    case .werewolf_king:
      return NSLocalizedString("白狼王", comment: "")
    case .magician:
      return NSLocalizedString("魔术师", comment: "")
    case .demon:
      return NSLocalizedString("恶魔", comment: "")
    case .none:
      return ""
    }
  }
  
  var showRole:String {
    switch self {
    case .seer:
      return NSLocalizedString("预", comment: "")
    case .werewolf:
      return NSLocalizedString("狼", comment: "")
    case .people:
      return NSLocalizedString("民", comment: "")
    case .witch:
      return NSLocalizedString("巫", comment: "")
    case .hunter:
      return NSLocalizedString("猎", comment: "")
    case .cupid:
      return NSLocalizedString("爱", comment: "")
    case .sheriff:
      return NSLocalizedString("警", comment: "")
    case .iguard:
      return NSLocalizedString("守", comment: "")
    case .werewolf_king:
      return NSLocalizedString("王", comment: "")
    case .magician:
      return NSLocalizedString("术", comment: "")
    case .demon:
       return NSLocalizedString("恶", comment: "")
    case .none:
      return ""
    }
  }
  
  var gameDescribe:String {
    switch self {
    case .seer:
      return NSLocalizedString("预", comment: "")
    case .werewolf:
      return NSLocalizedString("狼", comment: "")
    case .people:
      return NSLocalizedString("民", comment: "")
    case .witch:
      return NSLocalizedString("巫", comment: "")
    case .hunter:
      return NSLocalizedString("猎", comment: "")
    case .cupid:
      return NSLocalizedString("爱神", comment: "")
    case .sheriff:
      return NSLocalizedString("警", comment: "")
    case .iguard:
      return NSLocalizedString("守卫", comment: "")
    case .werewolf_king:
      return NSLocalizedString("白狼王", comment: "")
    case .magician:
      return NSLocalizedString("魔术师", comment: "")
    case .demon:
      return NSLocalizedString("恶魔", comment: "")
    case .none:
      return ""
    }
  }
  
  static func getCardImageName(name: String) -> UIImage {
    return OLGiftType.getGiftPic(type: name);
  }
}

// 死亡方式
enum DeathWay: String {
  case voted         // 被投死
  case killed        // 被杀
  case poisoned      // 被毒杀
  case token_away    // 被猎人带走
  case linked        // 殉情
  case leaved        // 离开
  case alive         // 存活
  case boom          //自爆
  case boom_away     //自爆被带走
  case none
  
  static func getWay(name: String) -> DeathWay {
    switch name {
    case "voted":
      return .voted
    case "killed":
      return .killed
    case "poisoned":
      return .poisoned
    case "token_away":
      return .token_away
    case "linked":
      return .linked
    case "leaved":
      return .leaved
    case "alive":
      return .alive
    case "boom":
      return .boom
    case "boom_away":
      return .boom_away
    default:
      return .none
    }
  }
  
  var descriptionString: String {
    switch self {
    case .voted:
      return NSLocalizedString("被投票处决", comment: "")
    case .killed:
      return NSLocalizedString("狼杀", comment: "")
    case .poisoned:
      return NSLocalizedString("被毒杀", comment: "")
    case .token_away:
      return NSLocalizedString("被猎人带走", comment: "")
    case .linked:
      return NSLocalizedString("自动殉情", comment: "")
    case .leaved:
      return NSLocalizedString("中途退出", comment: "")
    case .alive:
      return NSLocalizedString("存活", comment: "")
    case .boom:
        return NSLocalizedString("自爆", comment: "")
    case .boom_away:
        return NSLocalizedString("被狼人自爆带走", comment: "")
    case .none:
      return ""
    }
  }
}

// 游戏状态
enum GameStatus {
  case prepare
  case started
  case end
}

// 颜色
struct CustomColor {
  static let userBackColor = #colorLiteral(red: 0.1921568627, green: 0.09411764706, blue: 0.3137254902, alpha: 1)
  static let roomYellow = #colorLiteral(red: 0.9098039216, green: 0.6862745098, blue: 0.0862745098, alpha: 1)
  static let roomGreen = #colorLiteral(red: 0, green: 0.662745098, blue: 0.5254901961, alpha: 1)
  static let roomPink = #colorLiteral(red: 0.9882352941, green: 0.07450980392, blue: 0.6196078431, alpha: 1)
  static let newRoomYellow = #colorLiteral(red: 1, green: 0.9960784314, blue: 0.01176470588, alpha: 1)
  static let judgeMessageTextColor = UIColor.hexColor("666bff")
  static let judgeResult = UIColor.hexColor("ca2e99")
  static let userMessageTextColor = UIColor.hexColor("4b0885")
  static let userMessageTextLight = UIColor.white
  static let userMessageTextDark = UIColor.hexColor("1f0064")
  
  static let nightBackColor = UIColor.hexColor("1B0538")
  static let buttonRedColor = UIColor.rgb(219, 75, 34)
  static let speechingColor = UIColor.hexColor("8117DB")
  static let lightYellowTextColor = UIColor.hexColor("C99A79")
  static let newTextColor = UIColor.hexColor("000054")
}

//MARK:- 屏幕尺寸
struct Screen {
  /// bounds
  static var bounds: CGRect { return self.sceenC.bounds }
  /// size
  static var size: CGSize { return self.bounds.size }
  /// height
  static var height: CGFloat { return self.size.height }
  /// width
  static var width: CGFloat{ return self.size.width }
  
  static var screenP: UIScreen?
  static var sceenC: UIScreen {
    if self.screenP == nil {
      //创建对象
      self.screenP = UIScreen.main
    }
    return self.screenP!
  }
}
