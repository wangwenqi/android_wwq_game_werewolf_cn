//
//  GiftInfo.swift
//  xiaoyu
//
//  Created by Hang on 2018/1/8.
//  Copyright © 2018年 orangelab. All rights reserved.
//

import Foundation

enum giftType:String {
  
  case Rose = "Rose.gif"
  case Rose3 = "Rose3.gif"
  case Rose99 = "Rose99.gif"
  case Chocolate = "Chocolate.gif"
  case Kiss = "Kiss.gif"
  case Lollipop = "Lollipop.gif"
  case Vegetable = "Vegetable.gif"
  case Egg = "Egg.gif"
  case Crown = "Crown.gif"
  case Cake = "Cake.gif"
  case Teddy = "Teddy.gif"
  case Weddingdress = "Weddingdress.gif"
  case Heart = "Heart.gif"
  case Boat = "Boat.gif"
  case Ring = "Ring.gif"
  case Clap = "Clap.gif"
  case Car = "Car.gif"
  case Rocket = "Rocket.gif"
  case Airplane = "Airplane.gif"
  
  //卡片
  
  case delayTime = "延时卡"
  case checkID = "查验卡"
  
  case Sheriff = "警长卡"
  case Wolf = "狼人卡"
  case Lover = "爱神卡"
  case Hunter = "猎人卡"
  case Civilian = "平民卡"
  case Witch = "女巫卡"
  case Prophet = "预言家卡"
  case Werewolf_king = "白狼王卡"
  case Iguard = "守卫卡"
  case Magican = "魔术师卡"
  case Demon = "恶魔卡"
  
  var name:String {
    switch self {
    case .Rose:
      return NSLocalizedString("玫瑰花", comment: "")
    case .Rose3:
      return NSLocalizedString("3朵玫瑰花", comment: "")
    case .Rose99:
      return NSLocalizedString("99朵玫瑰花", comment: "")
    case .Chocolate:
      return NSLocalizedString("巧克力", comment: "")
    case .Kiss:
      return NSLocalizedString("么么哒", comment: "")
    case .Lollipop:
      return NSLocalizedString("棒棒糖", comment: "")
    case .Vegetable:
      return NSLocalizedString("烂菜叶", comment: "")
    case .Egg:
      return NSLocalizedString("臭鸡蛋", comment: "")
    case .Crown:
      return NSLocalizedString("皇冠", comment: "")
    case .Cake:
      return NSLocalizedString("蛋糕", comment: "")
    case .Teddy:
      return NSLocalizedString("小熊", comment: "")
    case .Weddingdress:
      return NSLocalizedString("婚纱", comment: "")
    case .Heart:
      return "520"
    case .Boat:
      return NSLocalizedString("爱情的轮船", comment: "")
    case .Ring:
      return NSLocalizedString("钻戒", comment: "")
    case .Clap:
      return NSLocalizedString("啪啪啪", comment: "")
    case .Car:
      return NSLocalizedString("法拉利", comment: "")
    case .Rocket:
      return NSLocalizedString("幸福火箭", comment: "")
    case .Airplane:
      return NSLocalizedString("私人飞机", comment: "")
      
    case .checkID:
      return NSLocalizedString("查验卡", comment: "")
    case .delayTime:
      return NSLocalizedString("延时卡", comment: "")
      
    case .Sheriff:
      return NSLocalizedString("警长卡", comment: "")
    case .Wolf:
      return NSLocalizedString("狼人卡", comment: "")
    case .Lover:
      return NSLocalizedString("爱神卡", comment: "")
    case .Hunter:
      return  NSLocalizedString("猎人卡", comment: "")
    case .Civilian:
      return NSLocalizedString("平民卡", comment: "")
    case .Witch:
      return NSLocalizedString("女巫卡", comment: "")
    case .Prophet:
      return NSLocalizedString("预言家卡", comment: "")
    case .Werewolf_king:
      return NSLocalizedString("白狼王卡", comment: "")
    case .Iguard:
      return NSLocalizedString("守卫卡", comment: "")
    case .Demon:
      return NSLocalizedString("恶魔卡", comment: "")
    case .Magican:
      return NSLocalizedString("魔术师卡", comment: "")
    default:
      return ""
    }
  }
  
  var buyName:String {
    switch self {
    case .Rose:
      return "flower_1"
    case .Rose3:
      return "flower_3"
    case .Rose99:
      return "flower_99"
    case .Chocolate:
      return "chocolate"
    case .Kiss:
      return "kiss"
    case .Lollipop:
      return "lollipop"
    case .Vegetable:
      return "cabbage"
    case .Egg:
      return "egg"
    case .Crown:
      return "crown"
    case .Cake:
      return "cake"
    case .Teddy:
      return "tidy"
    case .Weddingdress:
      return "wedding_dress"
    case .Heart:
      return "heart"
    case .Boat:
      return "ship"
    case .Ring:
      return "dim_ring"
    case .Clap:
      return "clap"
    case .Car:
      return "ferrari"
    case .Rocket:
      return "rocket"
    case .Airplane:
      return "airplane"
      
    case .checkID:
      return "check"
    case .delayTime:
      return "append_time"
      
      
    case .Sheriff:
      return "警长"
    case .Wolf:
      return "werewolf"
    case .Lover:
      return "cupid"
    case .Hunter:
      return  "hunter"
    case .Civilian:
      return "people"
    case .Witch:
      return "witch"
    case .Prophet:
      return "seer"
    case .Werewolf_king:
      return "werewolf_king"
    case .Iguard:
      return "guard"
    case .Magican:
      return "magician"
    case .Demon:
      return "demon"
      
    default:
      return ""
    }
  }
  
  
  var image:String {
    
    switch self {
    case .Rose:
      return "room_gift_rose"
    case .Rose3:
      return "room_gift_rose3"
    case .Rose99:
      return "room_gift_rose99"
    case .Chocolate:
      return "room_gift_chocolate"
    case .Kiss:
      return "room_gift_kiss"
    case .Lollipop:
      return "room_gift_jawbreaker"
    case .Vegetable:
      return "room_gift_vegetable"
    case .Egg:
      return "room_gift_egg"
    case .Crown:
      return "room_gift_crown"
    case .Cake:
      return "room_gift_cake"
    case .Teddy:
      return "room_gift_teddybear"
    case .Weddingdress:
      return "room_gift_weddingdress"
    case .Heart:
      return "room_gift_heart"
    case .Boat:
      return "room_gift_boat"
    case .Ring:
      return "room_gift_ring"
    case .Clap:
      return "room_gift_clap"
    case .Car:
      return "room_gift_car"
    case .Rocket:
      return "room_gift_rocket"
    case .Airplane:
      return "room_gift_airplane"
      
    case .checkID:
      return "查验卡"
    case .delayTime:
      return "延时卡"
      
      
    case .Sheriff:
      return "警长卡"
    case .Wolf:
      return "狼人卡"
    case .Lover:
      return "爱神卡"
    case .Hunter:
      return  "猎人卡"
    case .Civilian:
      return "平民卡"
    case .Witch:
      return "女巫卡"
    case .Prophet:
      return "预言家卡"
    case .Werewolf_king:
      return "白狼王卡"
    case .Iguard:
      return "守卫卡"
      
    case .Magican:
      return "魔术师卡"
    case .Demon:
      return "恶魔卡"
      
    default:
      return ""
    }
  }
}
