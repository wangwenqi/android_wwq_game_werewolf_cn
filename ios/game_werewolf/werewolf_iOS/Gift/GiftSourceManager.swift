//
//  GiftSourceManager.swift
//  game_werewolf
//
//  Created by Hang on 2017/11/11.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit


class GiftSourceManager: NSObject {
  static func checkVersion() {
    //查看本地是否有了版本
    let versionKey = "GIFT_VERSION"
    var para:[String:Any]?
    if let version = Utils.defaultsReadObjectWithKey(versionKey) {
//      para = ["rev":"\(version)"]
    }
    if para == nil {
      RequestManager().get(url: RequestUrls.getNewGifs, success: { (json) in
        if let dic = json.dictionaryObject {
          let config = NSMutableDictionary.init(dictionary: dic)
          if let rev = config["rev"] as? Int {
            //开始本地没有，就要保存起来
            Utils.defaultsSaveObject(rev, key: versionKey)
          }
          shareGift.shareInstance.giftInfo = config
          saveGiftConfig(config)
          getAllNeedDownloadSource(config: config)
        }
      }) { (code, message) in
        shareGift.shareInstance.updateGift()
      }
    }else{
      RequestManager().get(url: RequestUrls.getNewGifs, parameters:para!, success: {(json) in
        if let dic = json.dictionaryObject {
          let config = NSMutableDictionary.init(dictionary: dic)
          if let rev = config["rev"] as? Int {
            //开始本地没有，就要保存起来
            Utils.defaultsSaveObject(rev, key: versionKey)
          }
          saveGiftConfig(config)
          shareGift.shareInstance.giftInfo = config
          getAllNeedDownloadSource(config: config)
        }
      }) { (code, message) in
         shareGift.shareInstance.updateGift()
      }
    }
  }
  
  static func checkVersionWithoutLocalVersion() {
    let versionKey = "GIFT_VERSION"
    RequestManager().get(url: RequestUrls.getNewGifs, success: { (json) in
    if let dic = json.dictionaryObject {
    let config = NSMutableDictionary.init(dictionary: dic)
    if let rev = config["rev"] as? Int {
    //开始本地没有，就要保存起来
      Utils.defaultsSaveObject(rev, key: versionKey)
    }
    shareGift.shareInstance.giftInfo = config
    saveGiftConfig(config)
    getAllNeedDownloadSource(config: config)
    }
    }) { (code, message) in
    shareGift.shareInstance.updateGift()
    }
  }
  
  static func saveGiftConfig(_ dic:NSMutableDictionary) {
    checkFolder()
    let cachePath = NSHomeDirectory() + "/Documents/LWSource/LW.config"
    NSKeyedArchiver.archiveRootObject(dic, toFile: cachePath)
    do {
      var fileUrl = URL.init(fileURLWithPath: cachePath)
      var resourceValues = URLResourceValues()
      resourceValues.isExcludedFromBackup = true
      try  fileUrl.setResourceValues(resourceValues)
    } catch {
      print("Error: \(error)")
    }
  }
  
  static func checkFolder() {
    let cachedDir = NSHomeDirectory() + "/Documents/LWSource"
    if  FileManager.default.fileExists(atPath: cachedDir) == false {
      do {
      try FileManager.default.createDirectory(atPath: cachedDir, withIntermediateDirectories: false , attributes: nil)
      }catch {
        print("Error: \(error)")
      }
    }
    do {
      var fileUrl = URL.init(fileURLWithPath: cachedDir)
      var resourceValues = URLResourceValues()
      resourceValues.isExcludedFromBackup = true
      try  fileUrl.setResourceValues(resourceValues)
    } catch {
      print("Error: \(error)")
    }
  }
  //获取gif的路径
  static func getGifBy(url:[String:JSON]) -> String? {
    if let local = url["local"]?.string {
      if let path = Bundle.main.path(forResource:local, ofType:nil) {
        return path
      }
    }
    if let hash = url["remote"]?.string {
      let cachePath = NSHomeDirectory() + "/Documents/LWSource/"
      //得到文件后缀
      var fileFormat = ""
      if let file = hash.split("/").last {
        if let format = file.split(".").last {
          fileFormat = format
        }
      }
      let urlPath = cachePath + hash.sha1() + ".\(fileFormat)"
      if FileManager.default.fileExists(atPath: urlPath) {
        return urlPath
      }
    }
    return nil
  }
  
  //获取全部配置
  static func getTotalConfig() -> NSMutableDictionary?{
    let cachePath = NSHomeDirectory() + "/Documents/LWSource/LW.config"
    let file = FileManager.default
    if file.fileExists(atPath: cachePath) {
      if let dic = NSKeyedUnarchiver.unarchiveObject(withFile: cachePath) as?NSMutableDictionary {
        return dic
      }else{
        return nil
      }
    }
    return nil
  }
  //获取全部需要下载的图片，并下载
  static func getAllNeedDownloadSource(config:NSMutableDictionary) {
    var allNeedDownloadImages:[String] = []
    if let gitfs =  config["gifts"] as? NSArray {
      let json = JSON.init(gitfs)
      if let allGift = json.array {
        for gift in allGift {
          //需要下载的图片，有礼物默认图片，和每个点的gif
          //检查本地是否存在
          if let gitImage = gift["image"].string {
            if let local = gift["localImage"].string {
              //是不是本地存在
              if let _ = Bundle.main.path(forResource: local, ofType:"png") {

              }else{
                 allNeedDownloadImages.append(gitImage)
              }
            }
          }
          if let defaultImage = gift["animate"]["defaultIOSImage"]["remote"].string {
              if let local = gift["animate"]["defaultIOSImage"]["local"].string {
                //是不是本地存在
                if let _ = Bundle.main.path(forResource:local, ofType:nil) {

                }else{
                  if let status = gift["status"].int {
                    if status == 1 {
                       allNeedDownloadImages.append(defaultImage)
                    }else{
                      //已经下线的动画，不需要下载
                       print("\(gift["gift_type"].stringValue)" + "被下线了")
                    }
                  }else{
                      allNeedDownloadImages.append(defaultImage)
                  }
                }
              }
          }
          //得到point
          if let points = gift["animate"]["point"].array {
            //得到点的所有gift
            for point in points {
              //每个pointe的gift
              if let pointGif = point["anim"]["imageIOS"].array {
                var remote = ""
                if let remotePath = pointGif.first?["remote"].string {
                  remote = remotePath
                }
                if let local = pointGif.first?["local"].string {
                  //是不是本地存在
                  if let _ = Bundle.main.path(forResource:local, ofType:nil) {
                    
                  }else{
                    if let status = gift["status"].int {
                      if status == 1 {
                        allNeedDownloadImages.append(remote)
                      }else{
                         //已经下线的动画，不需要下载
                         print("\(gift["gift_type"].stringValue)" + "被下线了")
                      }
                    }else{
                      allNeedDownloadImages.append(remote)
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    shareGift.shareInstance.needDownloadImageArr = allNeedDownloadImages
    //得到了所有需要下载的图片，先检查一下，本地是否已经存在
    downloadImage(urls:allNeedDownloadImages)
  }
  
  static func downloadImage(urls:Array<String>) {
    let serial = DispatchQueue(label: "uploadSerialQueue")
    let semaphore = DispatchSemaphore(value: 2)
    var needReDownload = false
    for url in urls {
      serial.async {
        semaphore.wait()
        RequestManager.downloadFile(url: url, toPath: "", success: {
          semaphore.signal()
         if urls.index(of: url) == urls.count - 1 {
            //完成下载
            shareGift.shareInstance.downloadImageError = needReDownload
          }
          
        }, error: {
          semaphore.signal()
          needReDownload = true
          if urls.index(of: url) == urls.count - 1 {
            //完成下载,并且正好完成啦
            shareGift.shareInstance.downloadImageError = true
          }
        })
      }
    }
  }
}



//string sha1
extension String {
  func sha1() -> String {
    let data = self.data(using: String.Encoding.utf8)!
    var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
    data.withUnsafeBytes {
      _ = CC_SHA1($0, CC_LONG(data.count), &digest)
    }
    let hexBytes = digest.map { String(format: "%02hhx", $0) }
    return hexBytes.joined()
  }
}


