//
//  shareGift.swift
//  game_werewolf
//
//  Created by Hang on 2017/11/14.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

class shareGift: NSObject {
  static let shareInstance = shareGift()
  fileprivate override init () {}
  var needDownloadImageArr:[String]?
  var downloadImageError:Bool = false {
    didSet{
    if downloadImageError {
        if needDownloadImageArr != nil {
          currentTryTime = currentTryTime + 1
          if currentTryTime <= MaxDownloadTime {
            GiftSourceManager.downloadImage(urls: needDownloadImageArr!)
          }
        }
      }
    }
  }
  var currentTryTime = 0
  var MaxDownloadTime = 3
  var giftInfo:NSMutableDictionary? = GiftSourceManager.getTotalConfig() {
    didSet{
      //更新消息
       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UPDATEGIFT"), object: nil)
       giftJson = JSON.init(giftInfo)
    }
  }
  var cachedGift:NSMutableArray?
  var giftJson:JSON?
  //礼物信息获取
  func updateGift(){
    giftInfo = GiftSourceManager.getTotalConfig()
  }
  
  func rnGetThumailImaegBy(type:String) -> String {
      var thumbnail = ["gift_image":"","gift_name":""]
      var thumbnailImagePath:String?
      if giftInfo != nil {
        let json = JSON.init(giftInfo)
        //根据type找到图片
        if let giftArr = json["gifts"].array {
          let finalGift = giftArr.first(where: { (gift) -> Bool in
            if let types = gift["gift_type"].string {
              if types == type {
                return true
              }else{
                return false
              }
            }else{
              return false
            }
          })
          var didLocalHas = false
          //找到了指定的type的缩略图地址
          if let thumbnail = finalGift?["localImage"].string {
            //本地是不是有缩略图
            if let path = Bundle.main.path(forResource:thumbnail, ofType:"png") {
              didLocalHas = true
              thumbnailImagePath = "local"
            }
          }
          //本地没有，看看有没有下载过
          if didLocalHas == false {
            if let remoteUrl = finalGift?["image"].string {
              let hash = remoteUrl.sha1() //看看本地有没有
              let cachePath = NSHomeDirectory() + "/Documents/LWSource/"
              let urlPath = cachePath + hash + ".png"
              if FileManager.default.fileExists(atPath: urlPath) {
                thumbnailImagePath = urlPath
              }
            }
          }
          //type名字
          if let name = finalGift?["name"].string {
            thumbnail["gift_name"] = name
          }
          //国际化名字
          if let name_i18n = finalGift?["name_i18n"].dictionary {
            if let lange = Bundle.main.preferredLocalizations.first {
              if let international = name_i18n[lange]?.string {
                thumbnail["gift_name"] = international
              }
            }
          }
          thumbnail["gift_image"] = thumbnailImagePath ?? ""
        }
      }
      let jsondata = try? JSONSerialization.data(withJSONObject: thumbnail, options: [])
      let str =  String(data: jsondata!, encoding: .utf8)
      return str!
    }
  
  func getThumbnailImagePopularAndName(type:String) -> [String:String] {
    var thumbnail = ["gift_image":"","gift_name":"","gift_pop":""]
    var thumbnailImagePath:String?
    if giftInfo != nil {
      if let giftArr = giftInfo?["gifts"] as? NSArray {
        let finalGift = giftArr.first(where: { (gifts) -> Bool in
          if let gift = gifts as? NSDictionary {
            let types = gift["gift_type"] as? String
            if types == type {
              return true
            }else{
              return false
            }
            
          }else{
            return false
          }
        }) as? NSDictionary
        
        var didLocalHas = false
        //找到了指定的type的缩略图地址
        if let thumbnail = finalGift?["localImage"] as? String {
          //本地是不是有缩略图
          if let path = Bundle.main.path(forResource:thumbnail, ofType:"png") {
            didLocalHas = true
            thumbnailImagePath = path
          }
        }
        if didLocalHas == false {
          if let remoteUrl = finalGift?["image"] as? String {
            let hash = remoteUrl.sha1() //看看本地有没有
            let cachePath = NSHomeDirectory() + "/Documents/LWSource/"
            let urlPath = cachePath + hash + ".png"
            if FileManager.default.fileExists(atPath: urlPath) {
              thumbnailImagePath = urlPath
            }
          }
        }
        //type名字
        if let name = finalGift?["name"] as? String {
          thumbnail["gift_name"] = name
        }
        //国际化名字
        if let name_i18n = finalGift?["name_i18n"] as? NSDictionary {
          if let lange = Bundle.main.preferredLocalizations.first {
            if let international = name_i18n[lange] as? String {
              thumbnail["gift_name"] = international
            }
          }
        }
        thumbnail["gift_image"] = thumbnailImagePath ?? ""
        //人气值
        if let pop = finalGift?["popular"] as? Int {
          thumbnail["gift_pop"] = "\(pop)"
        }
      }
    }
    return thumbnail
  }

  func getgetThumbnailImageOnly(type:String) -> [String:String] {
    var thumbnail = ["gift_image":"","gift_name":""]
    var thumbnailImagePath:String?
    if giftInfo != nil {
      if let giftArr = giftInfo?["gifts"] as? NSArray {
        let finalGift = giftArr.first(where: { (gifts) -> Bool in
            if let gift = gifts as? NSDictionary {
               let types = gift["gift_type"] as? String
              if types == type {
                return true
              }else{
                return false
              }
              
            }else{
              return false
          }
        }) as? NSDictionary

      var didLocalHas = false
             //找到了指定的type的缩略图地址
       if let thumbnail = finalGift?["localImage"] as? String {
         //本地是不是有缩略图
         if let path = Bundle.main.path(forResource:thumbnail, ofType:"png") {
           didLocalHas = true
           thumbnailImagePath = path
         }
       }
       if didLocalHas == false {
          if let remoteUrl = finalGift?["image"] as? String {
            let hash = remoteUrl.sha1() //看看本地有没有
            let cachePath = NSHomeDirectory() + "/Documents/LWSource/"
            let urlPath = cachePath + hash + ".png"
            if FileManager.default.fileExists(atPath: urlPath) {
              thumbnailImagePath = urlPath
            }
          }
        }
        //type名字
        if let name = finalGift?["name"] as? String {
          thumbnail["gift_name"] = name
        }
        //国际化名字
        if let name_i18n = finalGift?["name_i18n"] as? NSDictionary {
          if let lange = Bundle.main.preferredLocalizations.first {
            if let international = name_i18n[lange] as? String {
              thumbnail["gift_name"] = international
            }
          }
        }
        thumbnail["gift_image"] = thumbnailImagePath ?? ""
      }
    }
    return thumbnail
  }
        
  func getThumbnailImagePathBy(type:String) -> [String:String]{
      var thumbnail = ["gift_image":"","gift_name":""]
      var thumbnailImagePath:String?
      if giftInfo != nil {
        let json = JSON.init(giftInfo)
          //根据type找到图片
          if let giftArr = json["gifts"].array {
            let finalGift = giftArr.first(where: { (gift) -> Bool in
              if let types = gift["gift_type"].string {
                if types == type {
                  return true
                }else{
                  return false
                }
              }else{
                return false
              }
            })
            var didLocalHas = false
            //找到了指定的type的缩略图地址
            if let thumbnail = finalGift?["localImage"].string {
              //本地是不是有缩略图
              if let path = Bundle.main.path(forResource:thumbnail, ofType:"png") {
                didLocalHas = true
                thumbnailImagePath = path
              }
            }
            //本地没有，看看有没有下载过
            if didLocalHas == false {
              if let remoteUrl = finalGift?["image"].string {
                let hash = remoteUrl.sha1() //看看本地有没有
                let cachePath = NSHomeDirectory() + "/Documents/LWSource/"
                let urlPath = cachePath + hash + ".png"
                if FileManager.default.fileExists(atPath: urlPath) {
                  thumbnailImagePath = urlPath
                }
              }
            }
            //type名字
            if let name = finalGift?["name"].string {
              thumbnail["gift_name"] = name
            }
            //国际化名字
            if let name_i18n = finalGift?["name_i18n"].dictionary {
              if let lange = Bundle.main.preferredLocalizations.first {
                if let international = name_i18n[lange]?.string {
                  thumbnail["gift_name"] = international
                }
              }
            }
            thumbnail["gift_image"] = thumbnailImagePath ?? ""
          }
      }
      return thumbnail
  }
 
  
  func checkGifByType(str:String) -> String? {
    if giftInfo != nil {
      guard let json = giftJson else {
        //生成一下json
        giftJson = JSON.init(giftInfo)
        return nil
      }
      //根据type找到图片
      if let giftArr = json["gifts"].array {
        let finalGift = giftArr.first(where: { (gift) -> Bool in
          if let types = gift["gift_type"].string {
            if types == str {
              return true
            }else{
              return false
            }
          }else{
            return false
          }
        })
        if let local = finalGift?["animate"]["defaultIOSImage"]["local"].string {
          //本地是不是有缩略图
          if let path = Bundle.main.path(forResource:local, ofType:nil) {
            return path
          }
        }
        if let hash = finalGift?["animate"]["defaultIOSImage"]["remote"].string {
          //本地是不是有缩略图，得到后缀
          var fileFormat = ""
          if let file = hash.split("/").last {
            if let format = file.split(".").last {
              fileFormat = format
            }
          }
          let cachePath = NSHomeDirectory() + "/Documents/LWSource/"
          let urlPath = cachePath + hash.sha1() + ".\(fileFormat)"
          if FileManager.default.fileExists(atPath: urlPath) {
            return urlPath
          }
        }
      }
    }
    return nil
  }
}
