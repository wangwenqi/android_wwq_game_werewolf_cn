#!/bin/bash 
set -e
fatLib=$HOME/Documents/werewolf/fatLib 
thinLib=$HOME/Documents/werewolf/thinLib 
#destination="${TARGET_BUILD_DIR}/"
destination="${TARGET_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"
function  install_framework()
{
    for file in $1/*; do
        code_sign $file 
    done
    cp -r $1/* $destination
}
code_sign() {
      # Use the current code_sign_identitiy
        echo "Code Signing $1 with Identity ${EXPANDED_CODE_SIGN_IDENTITY_NAME}"
        echo "/usr/bin/codesign --force --sign ${EXPANDED_CODE_SIGN_IDENTITY} --preserve-metadata=identifier,entitlements $1"
            /usr/bin/codesign --force --sign ${EXPANDED_CODE_SIGN_IDENTITY} --preserve-metadata=identifier,entitlements "$1"
 }
 if [ $CONFIGURATION == "Release" ]; then
        echo "打包啦"
        echo "=============================================================="
        # copy release oranglelib and webrtc to embed framework  arm64 ramv7s 
        #install_to_embeFramwork
        install_framework $thinLib
    else
        echo "debug模式"
        # copy build  oranglelib and webrtc to embed framework x86_64 arm64 ramv7s
        #install_to_embeFramwork
        install_framework $fatLib
 fi

