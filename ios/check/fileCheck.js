const fs = require('fs');
const proc = require('process');

const fileLocation = proc.argv[2];
const expectArg = proc.argv[3];
const expectArgValue = proc.argv[4];

// 获取内容
const analysisString = (data) => {
    data = data.trim();
    // 这里的匹配还有点问题 基本上大部分能用了
    if(!/^[\t ]*\/\/.*$/.test(data) && /^(.*)=([^;]*);?.*$/.test(data)) {

        let posibleKeys = RegExp.$1;
        let posibleValue = RegExp.$2.trim();

        posibleKeys = posibleKeys.replace(/\t/g,' ');
        posibleKeys = posibleKeys.replace(/:/g,'');
        posibleKeys = posibleKeys.split(' ');

        for(let kindex=0; kindex < posibleKeys.length; kindex += 1) {
            if(posibleKeys[kindex] == expectArg) {
                posibleValue = posibleValue.replace(/'/g,'');
                posibleValue = posibleValue.replace(/"/g,'');
                if(posibleValue == expectArgValue) {
                    console.log(`${expectArg} is expect`);
                    proc.exit(0);
                } else {
                    console.log(`${fileLocation}'s arg ${expectArg} is ${posibleValue} not ${expectArgValue}`);
                    proc.exit(-1);
                }
            }
        }
    }
}

// 确认文件是否正常
fs.access(fileLocation, (err) => {
  if (err) {
    if (err.code === 'ENOENT') {
      console.error(`${fileLocation} does not exist`);
      process.exit(-1);
      return;
    }

    process.exit(-1);
  }

  fs.readFile(fileLocation, 'utf-8', (err, data) => {
    if (err) {
        console.error(`${fileLocation} error : ${JSON.stringify(err)}`);
        process.exit(-1);
        return;
    }

    let isInCommon = false;
    const fileArray = data.split('\n');
    for(let i=0; i < fileArray.length; i += 1) {
        if(isInCommon) {
            if(/^.*\*\/.*$/.test(fileArray[i])) {
                isInCommon = false;
            }
        } else {
            if(/^(.*)\/\*.*$/.test(fileArray[i])) {
                isInCommon = true;
                if(RegExp.$1) {
                    analysisString(RegExp.$1);
                }
            } else {
                analysisString(fileArray[i]);
            }
        }
    }

    console.error(`${fileLocation}'s arg:${expectArg} not found`);
    process.exit(-1);

  });
});