const fs = require('fs');
const proc = require('process');

const fileLocation = proc.argv[2];

// 确认文件是否正常
fs.access(fileLocation, (err) => {
  if (err) {
    if (err.code === 'ENOENT') {
      console.error(`${fileLocation} does not exist`);
      process.exit(-1);
      return;
    }

    process.exit(-1);
  }

  let data = fs.readFileSync(fileLocation, 'utf-8');
  data = JSON.parse(data);

  let p3Files = [];

  for(let i = 0; data[i]; i += 1) {
    if(data[i].DisplayGamut && data[i].DisplayGamut == 'P3') {
        p3Files.push(data[i].Name);
    }
  }

  if(p3Files.length) {
    console.error(`find P3 files : ${p3Files.join(',')}`);
    process.exit(-1);
  } else {
    console.error(`no P3 finds`);
    process.exit(0);
  }

});