//
//  SwiftConverter.swift
//  game_werewolf
//
//  Created by 边宁 on 2017/3/27.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import Foundation
import ChatKit
import StoreKit
import LeanCloudFeedback
import AVFoundation
import SwiftyStoreKit
import Qiniu
import SocketIO
import RxSwift
import RxCocoa

class SwiftConverter: NSObject, UIAlertViewDelegate {
  let disposeBag = DisposeBag()
  static let shareInstance = SwiftConverter()
  fileprivate override init () {}
  
  internal var password  = ""
  internal var type      = ""
  internal var roomId    = ""
  internal var userName  = ""
  internal var userId    = ""
  internal var userImage = ""
  internal var token     = ""
  internal var roomTypeName     = ""
  internal var appRunning = false
  internal var remoteInfo:[String:Any]?
  
  private var rnCreateFamilyConvTimeoutTimer:Timer?;
  
  // 狼人杀
  let uMengAppKey = "5aa894d48f4a9d2a4d000089"  // 友盟分享
  let wXSDKKey = "wxa214fc8471ec6cc6"           // weixin
  let wXSDKAppSecret = "b4a735a535d45656a69e51a8581483a0"
  let QQSDKID = "1106755666"    // 41EDFC66      // QQ
  let QQAppKey = "HVi5vDDZj2MRJv5x"
  let faceBookID = "225714094591874"
  let buglyKey = "b15995bf3e"
  let homeUrl = "http://www.xiaobanhui.com"     // 主页地址
  
  // 小语
//  let uMengAppKey = "5a06c94fb27b0a311b0003ac"
//  let wXSDKKey = "wx306ff91559c4ac9e"
//  let wXSDKAppSecret = "7fd0d22bf454131d1bea7f134b484b6e"
//  let QQSDKID = "1106498837"  // 41F3D515
//  let QQAppKey = "4wF4MeRgWMPMYHTA"
//  let homeUrl = "http://xiaoyu.xiaobanhui.com"  // 主页地址
//  let buglyID = "747a5d94de"
//  let faceBookID = "225714094591874"
  
  //app 好评
  let ratingpage = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id="
  
  //是否验证成功
  var validate = false
  
  // chatKit是否已经登录
  internal var chatKitIsloginIn = true
  //是否可以取消上传flag
  internal var flag:Bool = false
  let semaphore = DispatchSemaphore(value: 1)
  //请求返回值
  typealias rnReturn = (Dictionary<String, JSON>) -> ()
  var rnCallBack:rnReturn?
  var setAudio = MapperAudioTypeArray()
  var rnMessage:Dictionary<String, JSON>? {
    didSet {
      if let message = rnMessage {
        if let callBack =  rnCallBack {
          callBack(message)
          rnMessage = nil
          rnCallBack = nil
        }
      }
    }
  }
  
  func sendMessageToRN(notification: Notification) {
    
    if var dic = notification.object as? [String: Any] {
      // leancloud 有个bug，未发出的消息会重复发。如果是自己的话，就过滤了
      // 但是家族又需要这条消息，所以家族的特殊处理下
      if let userId = dic["USER_ID"],
        userId as! String == CurrentUser.shareInstance.id{
        if let msgType = dic["MESSAGE_TYPE"] as? String,
          msgType != "FAMILY" {
          dic["READ"] = true;
          RequestManager().post(
            url: RequestUrls.sendTaskData,
            parameters: ["type": "family_chat","count": 1],
            success: { (response) in
              
          }, error: { (code, message) in
          });
        } else {
          return;
        }
      }
      
      // 处理家族消息的免打扰功能
      if let msgType = dic["MESSAGE_TYPE"] as? String,
        msgType != "FAMILY",
        let convId = dic["CONVERSATION_ID"] as? String {
        let client = LCChatKit.sharedInstance().client
        let query = client?.conversationQuery()
        query?.getConversationById(convId, callback: { (conversation, error) in
          DispatchQueue.main.async {
            if let conv = conversation,
              conv.muted {
              dic["READ"] = true;
            }
            RNMessageSender.emitEvent(name: "EVENT_CHAT", andPayload: dic as NSDictionary)
          }
        });
        return;
      }
      RNMessageSender.emitEvent(name: "EVENT_CHAT", andPayload: dic as NSDictionary)
    }
  }
  
  @objc fileprivate func waitReady() {
    LCChatKitExample.invokeThisMethodAfterLoginSuccess(withClientId: CurrentUser.shareInstance.id, success: {
      //游戏邀请
      OLGameInviteMessage.registerSubclass()
      OLGameMessageCell.registerSubclass()
      // 邀请类型的消息
      OLInviteMessage.registerSubclass();
      OLInviteMessageCell.registerSubclass();
      // 送礼物的消息类型
      OLGiftMessage.registerSubclass();
      OLGiftMessageCell.registerSubclass();
      //
      OLSystemMessage.registerSubclass()
      OLSystemMessageCell.registerSubclass()
      if self.appRunning == false {
        self.appRunning = true
        if let info = self.remoteInfo {
          self.didReceiveRemoteNotification(userInfo: info)
        }
      }
      //现在开始连接全局socket
      self.connectControlSocket()
    }) { (error) in
      
    }
  }
  
  func connectControlSocket() {
    //刚打开不连接
    if  CurrentUser.shareInstance.name == "qiaoyijie_yh_temp_fake_name" {
      //默认生成的ID
      return
    }
    //游戏不连接
    if let uid = Int(CurrentUser.shareInstance.id) {
      //都是数字不连接
      return
    }
    //如果已经连接了就不再连接
    if messageDispatchManager.controlManager.socketIO?.status == .connected {
      return
    }
    messageDispatchManager.getLobbyServer()
  }

  // 1man 2woman
  internal func loginSuccess(userId: String, userName: String, sex: Int, avator: String, token: String, isTourist:Bool) {
    
    print("\(userId)  \(userName) \(sex) \(avator) \(token)")
    
    CurrentUser.shareInstance.id = userId
    CurrentUser.shareInstance.name = userName
    CurrentUser.shareInstance.sex = sex
    CurrentUser.shareInstance.avatar = avator
    CurrentUser.shareInstance.token = token
    CurrentUser.shareInstance.isTourist = isTourist
    
    RequestManager.appendHeader(key: "x-access-token", value: token)
    //添加did
    RequestManager.appendHeader(key: "did", value:Utils.getUUID() ?? "")
    RequestManager().get(url: RequestUrls.resumeApp, success: nil, error: nil)
    if let _ = LCChatKit.sharedInstance().client {
      LCChatKitExample.invokeThisMethod(beforeLogoutSuccess: {
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.waitReady), userInfo: nil, repeats: false);
      }) { (error) in
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.waitReady), userInfo: nil, repeats: false);
      }
    } else {
      waitReady();
    }
    
    
    UINavigationBar.appearance().barTintColor = CustomColor.nightBackColor
    UINavigationBar.appearance().tintColor = UIColor.white;
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white];
    
    if CurrentUser.shareInstance.currentRoomID.length > 1 {
      NotificationCenter.default.post(
        name: NotifyName.kEnterToGameRoom.name(),
        object: nil,
        userInfo: [
          "password": CurrentUser.shareInstance.currentRoomPassword,
          "type": CurrentUser.shareInstance.currentRoomType,
          "roomId": CurrentUser.shareInstance.currentRoomID,
          "userName": CurrentUser.shareInstance.name,
          "userId": CurrentUser.shareInstance.id,
          "userImage": CurrentUser.shareInstance.avatar,
          "token": CurrentUser.shareInstance.token
        ])
    }
    
    if self.isGaming() == false {
      //这个时候就获取是不是有未完成的游戏
      getUnfinishedGame()
    }
  }
  
  internal func modifyUserInfo(userName: String, sex: Int, avator: String) {
    CurrentUser.shareInstance.name = userName;
    CurrentUser.shareInstance.sex = sex;
    CurrentUser.shareInstance.avatar = avator;
  }
  
  internal func didSelectUser(uerId: String, type: String, sex: String, icon: String, name: String, convId: String) {
    
    guard chatKitIsloginIn == true else {
      XBHHUD.showError("您的帐号已在其他设备登录。")
      return
    }
    
    Utils.runInMainThread {
      if type == chatType.chat.rawValue {

        
        let controller:ChatViewController?;
        
        if (convId != "") {
          controller = ChatViewController.init(conversationId: convId);
        } else {
          controller = ChatViewController.init(peerId: uerId)
        }

        controller?.peerSex = sex
        controller?.peerName = name
        controller?.peerIcon = icon
        controller?.peerID = uerId
        let nav = RotationNavigationViewController.init(rootViewController: controller!)
        UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
        
      } else {
        // 点击联系人中的好友
        
          let controller = UserInfoViewController()
        print("\(uerId), \(sex), \(name),\(icon)")
          controller.peerID = uerId
          controller.peerSex = sex
          controller.peerName = name
          controller.peerIcon = icon
          controller.convId = convId;
          let nav = RotationNavigationViewController.init(rootViewController: controller)
          UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
        
      }
    }
    
  }
  
  enum chatType: String {
    case chat
    case detail
  }
  //分享
  internal func share() {
    let image = UIImage(named: "shareIcon")
    let dic = getRandomInviteString()
    let localdetail2 = dic["localdetail2"]!
    let localinvate = dic["localinvate"]!
    Utils.runInMainThread {
      let localtitle = localinvate
      let localinvate = localdetail2
      Utils.UMShare("\(localtitle)", detail: "\(localinvate)", url: "\(Config.urlHost)/share_kuaiwan",icon: image! ,type: .normal)
    }
  }
  //web调用本地分享
  func shareWithContent(_ para:Dictionary<String,JSON>) {
    let image = UIImage(named: "shareIcon")
    if let url = para["url"]?.string,
      let title = para["title"]?.string,
      let message = para["message"]?.string{
      Utils.runInMainThread {
        let localtitle = title
        let localinvate = message
        Utils.UMShare("\(localtitle)", detail: "\(localinvate)", url:url,icon: image! ,type: .normal)
      }
    }
  }
  
  
  //随机产生邀请文字
  func getRandomInviteString() -> Dictionary<String,String> {
//    return ["localinvate":NSLocalizedString("与萌妹一起在语音房嗨麦一刻", comment: ""),"localdetail2":NSLocalizedString("与附近的萌妹帅哥一起k歌，嗨麦，闲聊，相亲，算命，情感咨询，真心话大冒险，掷骰子，章鱼机，玩小游戏...", comment: "")];
    let randomNum = arc4random_uniform(9)
    let random = randomNum % 3
    switch random {
    case 0:
      let dic = ["localinvate":NSLocalizedString("爱神，魔术师，恶魔，白狼王，女巫..百变角色", comment: ""),"localdetail2":NSLocalizedString("各个综艺节目都在玩的狼人杀游戏，最专业的交流，最烧脑的花板子，还有娱乐房让你游戏过后和萌妹男神一起放松...", comment: "")]
      return dic
    case 1:
      let dic = ["localinvate":NSLocalizedString("与更多有趣的人一起PK热门小游戏", comment: ""),"localdetail2":NSLocalizedString("热门综艺推荐的PK小游戏震撼上线，邀请好友实时对战。还可狼人杀，真心话大冒险，掷骰子，章鱼机，生日/婚礼/节日派对...", comment: "")]
      return dic
    case 2:
      let dic = ["localinvate":NSLocalizedString("快玩小游戏吧最具人气排行榜有你最关注的Ta", comment: ""),"localdetail2":NSLocalizedString("在萌妹最多的游戏中随时了解喜欢的Ta最新动态，每时每刻可以偶遇去Ta的娱乐房间..", comment: "")]
      return dic
    default:
      let dic = ["localinvate":NSLocalizedString("快玩小游戏吧最具人气排行榜有你最关注的Ta", comment: ""),"localdetail2":NSLocalizedString("在萌妹最多的游戏中随时了解喜欢的Ta最新动态，每时每刻可以偶遇去Ta的娱乐房间..", comment: "")]
      return dic
    }
  }

  
  //常见问题
  func openPayFAQ() {

    DispatchQueue.main.async {
      let web = StoreCenterViewController()//gameAndMissionCenterViewController()
      web.type = .FAQ
      let nav = RotationNavigationViewController(rootViewController: web)
      UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
    }
    
  }
  
  // 充值页
  func openStoreRecharge() {
 
    let web = StoreCenterViewController()//gameAndMissionCenterViewController()
    web.type = .Charge
    let nav = RotationNavigationViewController(rootViewController: web)
    DispatchQueue.main.async {
      UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
    }
  }
  
  // 通用的打开王旭传过来的页面
  func openWebView(url:String) {
    let web = StoreCenterViewController()//gameAndMissionCenterViewController()
    var perFix = "?";
    if url.has("?") {
      perFix = "&"
    }
    web.weburl = "\(url)\(perFix)\(web.headerURLParams())&access_token=" + "\(CurrentUser.shareInstance.token)";
    let nav = RotationNavigationViewController(rootViewController: web)
    DispatchQueue.main.async {
      UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
    }
  }
  
  //设置地区
  
  func setLocation(lc:String){
   let localLC = UserDefaults.standard.object(forKey: "LOCATION") as? String
   if localLC != nil {
    if localLC != lc && lc != nil && lc != "" {
      UserDefaults.standard.set(lc, forKey: "LOCATION")
    }
   }else{
      UserDefaults.standard.set(lc, forKey: "LOCATION")
    }
  }
  
  //权限检测
  func permissionCheck() {
    var appear = true
    if UserDefaults.standard.bool(forKey: "PERMISSION_NOT_FIRST") == true {
      //不是第一次请求
      let recordPermission = AVAudioSession.sharedInstance().recordPermission()
      switch recordPermission {
      case AVAudioSessionRecordPermission.denied:
            appear = true
      case AVAudioSessionRecordPermission.granted:
            appear = false
      default:
          appear = true
      }
    }
    if appear {
      let device = Utils.getCurrentDeviceType()
      var width:CGFloat = 0
      if device == .iPhone5 || device == .iPhone4 {
        width = Screen.width
      }else{
        width = Screen.width * 0.8
      }
      DispatchQueue.main.async {
        let permission = Bundle.main.loadNibNamed("AuthorizationView", owner: nil, options: nil)?.last as! AuthorizationView
        permission.frame = CGRect(x: 0, y: 0, width: width, height: 418)
        permission.backgroundColor = UIColor.clear
        permission.center = (UIApplication.shared.keyWindow?.rootViewController?.view.center)!
        Utils.showAlertView(view: permission)
      }
    }
    //下载黑名单
    Utils.downloadBL()
    //支付
    finishPay()
  }
  
  //未完成的游戏
  
  func getUnfinishedGame() {
    if CurrentUser.shareInstance.id != "" && CurrentUser.shareInstance.id != nil {
      RequestManager().post(url: RequestUrls.unfinishedGame, success: { (json) in
        if let roomid = json["room_id"].string,
           let type = json["type"].string{
           let password = json["password"].string ?? ""
          Utils.runInMainThread {
            let view = unfinishedGame.view()
            view.gameInfo = [
              "password": password,
              "type": type,
              "roomId": roomid,
              "userName": CurrentUser.shareInstance.name,
              "userId": CurrentUser.shareInstance.id,
              "userImage": CurrentUser.shareInstance.avatar,
              "token": CurrentUser.shareInstance.token
            ]
            Utils.showNoticeView(view: view)
          }
        }
      }, error: { (code, message) in
        
      })
    }
  }

  //检查更新
  internal func checkUpdate() {
    
    RequestManager().post(url: RequestUrls.upgrade, parameters: [
      "version_code":"\(Config.buildVersion)",
      "system":"ios",
      ], success: { (json) in
        
        if json["update_level"].stringValue == "1" {
          print("正常更新")
          let alert = UIAlertView.init(title: "发现新版本\(json["version_name"].stringValue)", message: "\(json["description"].stringValue)", delegate: self, cancelButtonTitle: "去更新", otherButtonTitles: "取消")
          alert.tag = 111
          alert.show()
        } else if json["update_level"].stringValue == "2" {
          print("强制更新")
          let alert = UIAlertView.init(title: "发现新版本\(json["version_name"].stringValue)", message: "\(json["description"].stringValue)", delegate: self, cancelButtonTitle: "去更新")
          alert.tag = 222
          alert.show()
        }
        
    }) { (code, message) in
      print("\(code)  \(message)")
    }
  }

//  internal func sendMessageEnter() {
//
//    if  CurrentUser.shareInstance.from != "" {
//      messageDispatchManager.sendMessage(type: .enterRoom, payLoad: [
//        "room_id": roomId,
//        "password": password,
//        "type"    : CurrentUser.shareInstance.currentRoomType,
//        "token": CurrentUser.shareInstance.token,
//        "extra" : [
//          "pt": "\(Config.platform)",
//          "v": "\(Config.buildVersion)",
//          "b": "\(Config.appStoreVersion)",
//          "sv": "\(Config.serverVersion)",
//          "tz": "\(Config.currentTimeZoneGMT)",
//          "lg": "\(Config.systemLanage)"
//        ],
//        "user": [
//          "id": CurrentUser.shareInstance.id,
//          "name": CurrentUser.shareInstance.name,
//          "avatar": CurrentUser.shareInstance.avatar,
//          "level": CurrentUser.shareInstance.level,
//          "experience": CurrentUser.shareInstance.experience
//        ],
//        "from":"export"
//        ])
//
//    }else{
//      messageDispatchManager.sendMessage(type: .enterRoom, payLoad: [
//        "room_id": roomId,
//        "password": password,
//        "type"    : CurrentUser.shareInstance.currentRoomType,
//        "token": CurrentUser.shareInstance.token,
//        "extra" : [
//          "pt": "\(Config.platform)",
//          "v": "\(Config.buildVersion)",
//          "b": "\(Config.appStoreVersion)",
//          "sv": "\(Config.serverVersion)",
//          "tz": "\(Config.currentTimeZoneGMT)",
//          "lg": "\(Config.systemLanage)"
//        ],
//        "user": [
//          "id": CurrentUser.shareInstance.id,
//          "name": CurrentUser.shareInstance.name,
//          "avatar": CurrentUser.shareInstance.avatar,
//          "level": CurrentUser.shareInstance.level,
//          "experience": CurrentUser.shareInstance.experience
//        ]
//        ])
//    }
//  }

  internal func sendMessageEnter() {
    if  CurrentUser.shareInstance.from != "" {
      messageDispatchManager.sendMessage(type: .enterRoom, payLoad: [
        "room_id": roomId,
        "password": password,
        "type"    : CurrentUser.shareInstance.currentRoomType,
        "token": CurrentUser.shareInstance.token,
        "extra" : [
          "pt": "\(Config.platform)",
          "v": "\(Config.buildVersion)",
          "b": "\(Config.appStoreVersion)",
          "sv": "\(Config.serverVersion)",
          "tz": "\(Config.currentTimeZoneGMT)",
          "lg": "\(Config.systemLanage)",
          "app": Config.app
        ],
        "user": [
          "id": CurrentUser.shareInstance.id,
          "name": CurrentUser.shareInstance.name,
          "avatar": CurrentUser.shareInstance.avatar,
          "level": CurrentUser.shareInstance.level,
          "experience": CurrentUser.shareInstance.experience
        ],
        "from":"export"
        ])
      
    }else{
      messageDispatchManager.sendMessage(type: .enterRoom, payLoad: [
        "room_id": roomId,
        "password": password,
        "type"    : CurrentUser.shareInstance.currentRoomType,
        "token": CurrentUser.shareInstance.token,
        "extra" : [
          "pt": "\(Config.platform)",
          "v": "\(Config.buildVersion)",
          "b": "\(Config.appStoreVersion)",
          "sv": "\(Config.serverVersion)",
          "tz": "\(Config.currentTimeZoneGMT)",
          "lg": "\(Config.systemLanage)",
          "app": Config.app
        ],
        "user": [
          "id": CurrentUser.shareInstance.id,
          "name": CurrentUser.shareInstance.name,
          "avatar": CurrentUser.shareInstance.avatar,
          "level": CurrentUser.shareInstance.level,
          "experience": CurrentUser.shareInstance.experience
        ]
        ])
    }
  }
  
  //小游戏
  func rnStartMiniGame(_ gameType:String) {
    CurrentUser.shareInstance.loadingGame = true
    NotificationCenter.default.post(
      name: NotifyName.kEnterToGameRoom.name(),
      object: nil,
      userInfo: [
        "password":"",
        "type": gameType,
        "roomId": "",
        "userName":CurrentUser.shareInstance.name,
        "userId": CurrentUser.shareInstance.id,
        "userImage": CurrentUser.shareInstance.avatar,
        "token":CurrentUser.shareInstance.token
      ])
  }
  //小游戏分享
  func rnStartMiniGameIntviu(_ title:String,message:String, _ url:String) {
    let image = UIImage(named: "shareIcon")
    Utils.runInMainThread {
      Utils.UMShare("\(title)", detail: "\(message)", url:url,icon: image! ,type: .normal)
    }
  }
  
  internal func getGameRoom(notification: Notification) -> GameRoomViewController {
    
    let jsonData = notification.object as! JSON
    let controller = UIViewController.loadFromStoryBoard("Main", identifier: "GameRoomViewController") as! GameRoomViewController
    controller.data = jsonData
    
    CurrentUser.shareInstance.currentRoomID = jsonData["room"]["room_id"].stringValue
    CurrentUser.shareInstance.realCurrentRoomID = jsonData["room"]["room_id"].stringValue
    // 真正进入房间后进行重连次数修改
    messageDispatchManager.socketManager.currentRetryTime = SocketManager.MAX_RETRY_TIMES
    return controller;
  }
  
  // 获取用户信息
  internal func getUserInformation(userId: String, success:((_ userInfo: [String: Any]) -> Void)?) {
    // 请求用户信息
    RequestManager().post(url: RequestUrls.userInfo, parameters: ["user_id":userId], success: { (json) in
      
      var userInfo: [String: Any] = [
      "name":"\(json["name"].stringValue)",
      "sex": "\(json["sex"].intValue)",
      "avator": "\(json["image"].stringValue)",
      "userId": "\(json["id"].stringValue)"
      ]
    
      if let isFriend = json["is_friend"].string {
        userInfo["is_friend"] = isFriend == "true" ? true : false;
      } else if let isFriend = json["is_friend"].bool {
        userInfo["is_friend"] = isFriend;
      }
      
      if success != nil {
         success!(userInfo)
      }
   
    }) { (code, message) in
      XBHHUD.showError(message)
    }
  }
  
  // 获取服务器地址
  func getServiceRegion(success:@escaping (_ host:NSMutableDictionary?) -> ())  {
    RequestManager().post(url: RequestUrls.serviceRegion, success: { (json) in
      if let lean = json["leancloud"].dictionaryObject {
        let host =  NSMutableDictionary(dictionary: (lean))
        success(host)
      }else{
        success(nil)
      }
    }) { (code, message) in
      
    }
  }
  // 用户反馈
  func feedBack() {
    let currentvc = Utils.getCurrentVC(vc:(Utils.getKeyWindow()?.rootViewController)!)
    LCUserFeedbackAgent.sharedInstance().showConversations(currentvc, title: nil, contact: nil)
  }
  
  // 请求好评
  func appStoreReviewAndRating() {
    UIApplication.shared.openURL(URL(string: ratingpage)!)
  }
  //获取当前版本
  func getCurrentVersion() -> String {
    return "\(Config.buildShortVersion)"
  }
  //登陆埋点
  func loginWith(Plateform:String){
    MobClick.event(Plateform)
  }
  func mobAnalytics(mobName:String){
    MobClick.event(mobName)
  }
  //初始化友盟分享 只会初始化一次
  internal func initialize() {
    
   UMSocialManager.default().umSocialAppkey = uMengAppKey//UMSocialData.setAppKey(uMengAppKey)
  //UMSocialConfig.setSupportedInterfaceOrientations(UIInterfaceOrientationMask.landscape)
    UMSocialManager.default().openLog(true)
    //微信
    UMSocialManager.default().setPlaform(.wechatSession, appKey: wXSDKKey, appSecret: wXSDKAppSecret, redirectURL: homeUrl)
    //UMSocialWechatHandler.setWXAppId(wXSDKKey, appSecret: wXSDKAppSecret, url: homeUrl)
    //QQ
    UMSocialManager.default().setPlaform(.QQ, appKey: QQSDKID, appSecret: QQAppKey, redirectURL: homeUrl)
    //UMSocialQQHandler.setQQWithAppId(QQSDKID, appKey: QQAppKey, url: homeUrl)
    
    UMSocialManager.default().setPlaform(.facebook, appKey: faceBookID, appSecret: nil, redirectURL: homeUrl)
    
    // 友盟统计
    if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
      MobClick.setAppVersion(version)
    }
    
    let config = UMAnalyticsConfig()
    config.appKey = uMengAppKey
    MobClick.start(withConfigure: config)
    
    // 监听聊天发来的消息
    NotificationCenter.default.addObserver(self, selector: #selector(SwiftConverter.sendMessageToRN(notification:)), name: NotifyName.kSendMessToRN.name(), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(SwiftConverter.enterToGameRoom(notification:)), name: NotifyName.kEnterToGameRoom.name(), object: nil)
  }
  
  // 获取当前的开发环境
  internal func getCurrentDevSetting() -> String {
    return Config.devSetting.rawValue
  }
  
  // 收到远程通知
  internal func didReceiveRemoteNotification(userInfo: [String: Any]) {
    
    if appRunning == false {
        self.remoteInfo = userInfo
        return
    }
    guard userInfo.count != 0 else { return }
    if Config.devSetting != .release {
      XBHHUD.showSuccess("\(userInfo)")
    }
    guard let _ = userInfo["conid"],
      let uid = userInfo["uid"] as? String else {
        return
    }
    guard chatKitIsloginIn == true else {
      XBHHUD.showError("您的帐号已在其他设备登录。")
      return
    }
    getUserInformation(userId: uid, success: { dic in
      guard let _ = dic["name"],
        let userid = dic["userId"] as? String,
        let _ = dic["sex"] else {
        return
      }
      var isChating = false
      var chatID:String?
      let currentvc = Utils.getCurrentVC(vc:(Utils.getKeyWindow()?.rootViewController)!)
      if (currentvc?.isKind(of: ChatViewController.self))! {
        isChating = true
        chatID = (currentvc as! ChatViewController).peerID
      }
      if isChating && chatID == userid {
        return
      }
      DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
        self.remoteJump(userinfo:dic, orginInfo:userInfo);
      })
    })
  }
  
  //获取礼物图片
  
//  func getImagePahtBy(type:String) ->[String:String]{
//    let dic = shareGift.shareInstance.rnGetThumailImaegBy(type:type)
//    return dic
//  }

  //查看收到的礼物
  
  func showAllGifts(userid:String) {
    let giftList = userGiftListViewController()
    giftList.userid = userid
    giftList.form = .REACT
    let nav = RotationNavigationViewController(rootViewController: giftList)
    DispatchQueue.main.async {
        UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
    }
  }
  
  // 完成支付
  func finishPay() {
    SwiftyStoreKit.completeTransactions(atomically: false) { purchases in
      for purchase in purchases {
        if purchase.transaction.transactionState == .purchased || purchase.transaction.transactionState == .restored {
          if purchase.needsFinishTransaction {
            XBHHUD.showCanNotHidden(title: "您有一个未验证的订单，正在为您验证")
            //客户端的验证器
            let launchValidator = LaunchValidator(service:.production)
            let product = productInfo(productId: purchase.productId, transaction: purchase.transaction, needsFinishTransaction: purchase.needsFinishTransaction)
            launchValidator.purchase = product
            SwiftyStoreKit.verifyReceipt(using: launchValidator, password: nil) { result in
    
            }
          }
        }else{
          //如果失败的话就直接移除
          SwiftyStoreKit.finishTransaction(purchase.transaction)
        }
      }
    }
  }

  //是否可以退出
  
  func couldLogout() -> NSNumber {
    if CurrentUser.shareInstance.unCheckReceipt == nil {
      //退出当前soket
      messageDispatchManager.controlManager.logOut()
      return 1
    }else{
      DispatchQueue.main.async {
        //告诉用户不能退出
        let alert = UIAlertController(title: "", message: "您还有未验证的购买，为了您的资金安全，在您完成验证以前不能退出", preferredStyle: .alert)
        let action = UIAlertAction(title: "确定", style: .default, handler: { (action) in
        })
        alert.addAction(action)
        Utils.getCurrentVC(vc:(Utils.getKeyWindow()?.rootViewController)!)?.present(alert
          , animated: true, completion: nil)
      }
      return 0
    }
  }
  
  //进入商场
  
  func storeCenter() {
    DispatchQueue.main.async {
      let web = StoreCenterViewController()//gameAndMissionCenterViewController()
      web.type = .Charge
      let nav = RotationNavigationViewController(rootViewController: web)
      UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
    }
  }
  
  //特色房间
  func openCallUp() {
    let web = StoreCenterViewController()
    web.type = .Recommend
    let nav = RotationNavigationViewController(rootViewController: web)
    DispatchQueue.main.async {
      UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
    }
  }
  
  //进入任务中心
  
  func gameAndMissionCenter() {

    DispatchQueue.main.async {
      let web = StoreCenterViewController()
      web.type = .Mission
      let nav = RotationNavigationViewController(rootViewController: web)
      UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
    }
  }
  
  //进入排行榜
  func openLeaderboard() {
    DispatchQueue.main.async {
      let web = StoreCenterViewController()
      web.type = .Leaderboard
      let nav = RotationNavigationViewController(rootViewController: web)
      UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
    }
  }
 
  //从后台进入前台
  
  func willEnterForeground() {
     RNMessageSender.emitEvent(name:"CONTINUE_LOGIN")
  }

  enum RNToNativeAction: String {
    case enterAudioRoom
    case ACTION_REQUEST_FRIENDS_LIST
    case black_list
    case standings
    case enterRoom
    case setAudioChildType  // 语音房间类型 游戏,聊天,唱歌
    case ACTION_UPLOAD_FILE
    case ACTION_FAMILY_INFO_INIT //家族信息
    case ACTION_SHOW_FAMILY_INFO //显示家族详情
    case ACTION_UP_LOAD_PHOTOS   // 上传照片墙
    case ACTION_UP_LOAD_FAMILY_HEAD //家族头像
    case ACTION_UP_LOAD_USER_HEAD   //用户头像
    case ACTION_CREATE_FAMILY_CONVERSATION //创建家族的会话
    case ACTION_ENTER_FAMILY_CONVERSATION // 家族会话创建完成后，调用
    case ACTION_LOGIN_OUT               // 退出登录
    case SHARE_HOUSE_DEEDS      // RN调用分享
    case unKnown
  }
  //RN调用native
  func sendMessageToNative(_ json:String) {
    let json = JSON.init(parseJSON: json)
    if let action = json["action"].string {
    let row = RNToNativeAction.init(rawValue: action) ?? .unKnown
    switch row {
      case .enterAudioRoom:
        enterAudioRoom(json:json["params"].dictionaryValue)
          //RN进入语音房间
      case .ACTION_REQUEST_FRIENDS_LIST:
          self.rnMessage = json["params"].dictionaryValue
      case .black_list: //进入黑名党
        DispatchQueue.main.async {
          let block = userBlockListViewController()
          let nav = RotationNavigationViewController.init(rootViewController: block)
          Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)?.present(nav, animated: true, completion: nil)
        }
      case .standings:
        DispatchQueue.main.async {
          let web = StoreCenterViewController()
          web.type = .GameRecord
          web.userid = CurrentUser.shareInstance.id
          let nav = RotationNavigationViewController(rootViewController: web)
          Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)?.present(nav, animated: true, completion: {
            
          })
        }
      case .enterRoom: //RN进入房间
        enterRoom(json:json["params"].dictionaryValue)
      case .setAudioChildType:
//        setAudio.setModels(json["params"]["list"].arrayValue)
      // 不在需要RN传来的房间类型可以删除
      break
      case .ACTION_UPLOAD_FILE:
        let action = "ACTION_UPLOAD_FILE"
        uploadDispatch(json: json,action:action)
      case .ACTION_UP_LOAD_PHOTOS:
        let action = "ACTION_UP_LOAD_PHOTOS"
        uploadDispatch(json: json,action:action)
      case .ACTION_UP_LOAD_USER_HEAD:
        let action = "ACTION_UP_LOAD_USER_HEAD"
         uploadDispatch(json: json,action:action)
      case .ACTION_UP_LOAD_FAMILY_HEAD:
        let action = "ACTION_UP_LOAD_FAMILY_HEAD"
        uploadDispatch(json: json,action:action)
      case .ACTION_FAMILY_INFO_INIT:
        if let group = json["params"]["group"].dictionary {
            setUpFamilyInfo(group: group)
        }
      case .ACTION_SHOW_FAMILY_INFO:
        if let groupId = json["params"]["group_id"].string {
          showFamilyInfo(familyId: groupId)
        }
      case .ACTION_CREATE_FAMILY_CONVERSATION:
        Utils.runInMainThread {[weak self] in
          if let timeout = json["options"]["timeout"].int,
              timeout > 0 {
            print("!!!!!------时间 ::: \(TimeInterval.init(timeout / 1000))")
            if let strongSelf = self {
              strongSelf.rnCreateFamilyConvTimeoutTimer?.invalidate();
              strongSelf.rnCreateFamilyConvTimeoutTimer = Timer.scheduledTimer(timeInterval: TimeInterval.init(timeout / 1000), target: strongSelf, selector: #selector(SwiftConverter.createFamilyConversationTimeout), userInfo: nil, repeats: false);
              if let timer = strongSelf.rnCreateFamilyConvTimeoutTimer {
                RunLoop.current.add(timer, forMode: .commonModes);
              }
            }
          }
          
          LCChatKit.sharedInstance().client
            .createConversation(withName: "家族群聊",
                                clientIds: [CurrentUser.shareInstance.id],
                                callback: { [weak self] (conversation, error) in
                                  self?.rnCreateFamilyConvTimeoutTimer?.invalidate();
                                  if let resError = error {
                                    self?.sendMessagesToRN(json: JSON([
                                      "action": "ACTION_CREATE_FAMILY_CONVERSATION",
                                      "params": [
                                        "reason": "\(resError.localizedDescription)"
                                      ]
                                    ]));
                                    return;
                                  }
                                  if let convId = conversation?.conversationId {
                                    self?.sendMessagesToRN(json: JSON([
                                      "action": "ACTION_CREATE_FAMILY_CONVERSATION",
                                      "params": [
                                        "conversationId": convId
                                      ]
                                    ]));
                                  } else {
                                    self?.sendMessagesToRN(json: JSON([
                                      "action": "ACTION_CREATE_FAMILY_CONVERSATION",
                                      "params": [
                                        "reason": "未成功创建群聊"
                                      ]
                                    ]));
                                  }
            });
          
        }
      case .ACTION_ENTER_FAMILY_CONVERSATION:
        Utils.runInMainThread {
          // TODO 偶尔说不在主线程
          print("--------ACTION_ENTER_FAMILY_CONVERSATION---")
          if CurrentUser.shareInstance.isTourist == false,
            let convid = json["params"]["conversationId"].string,
            let controller = FamilyChatViewController.init(conversationId: convid) {
            controller.peerSex = "\(CurrentUser.shareInstance.sex)";
            controller.peerName = CurrentUser.shareInstance.name;
            controller.peerIcon = CurrentUser.shareInstance.avatar;
            controller.peerID = CurrentUser.shareInstance.id;
            let nav = RotationNavigationViewController.init(rootViewController: controller)
            UIApplication.shared.keyWindow?.rootViewController?.present(nav, animated: true, completion: nil)
          }else{
            if CurrentUser.shareInstance.isTourist {
              XBHHUD.showError(NSLocalizedString("请您先去登录", comment: ""))
            }
          }
        }
    case .ACTION_LOGIN_OUT:
      DispatchQueue.main.async {
        LCChatKitExample.invokeThisMethod(beforeLogoutSuccess: {
          print("退出LeanCloud成功")
        }, failed: { (error) in
          print("退出LeanCloud失败")
        })
        
        // 断开socket // leanCloud关掉 // 本地UserInfo清理
        CurrentUser.shareInstance.currentPosition = 100
        NotificationCenter.default.post(name: NotifyName.kBackGroundModel.name(), object: false)
        if let button = UIViewController.topWindow()?.viewWithTag(9901) {
          button.removeFromSuperview()
        }
        CurrentUser.shareInstance.clearCurrentInformation()
        messageDispatchManager.close()
        messageDispatchManager.resetRoomInfo()
      }
    case .SHARE_HOUSE_DEEDS:
      DispatchQueue.main.async {
        let myView = ChatRoomShareFrends.showRoomOwner(ChatRoomCertificate.init(.zero, json["params"]))
        myView.invoteLabels.text = "分享房契".localized
        Utils.showAlertView(view: myView)
      }
    default:
      debugPrint("swiftConverter UnKnown RN Action Line 991")
    }
    }
  }
  
  func createFamilyConversationTimeout() {
    rnCreateFamilyConvTimeoutTimer?.invalidate();
    sendMessagesToRN(json: JSON([
      "action": "ACTION_CREATE_FAMILY_CONVERSATION",
      "params": [
        "reason": "网络异常"
      ]
    ]));
  }
  
  //设置家族信息
  func setUpFamilyInfo(group:[String:JSON]) {
    var family = Family()
    if let is_owner = group["is_owner"]?.bool {
      family.is_owner = is_owner
    }
    if let group_id = group["group_id"]?.string {
      family.group_id = group_id
    }
    if let gid = group["gid"]?.string {
      family.gid = gid
    }
    if let name = group["name"]?.string {
      family.name = name
    }
    if let short_name = group["short_name"]?.string {
      family.short_name = short_name
    }
    if let image = group["image"]?.string {
      family.image = image
    }
    if let lc_id = group["lc_id"]?.string {
      family.lc_id = lc_id
    }
    if let member_count = group["member_count"]?.int {
      family.member_count = member_count
    }
    //设置家族信息
    CurrentUser.shareInstance.familyInfo = family
  }
  
  //native调用RN,有回掉
  func sendMessagesToRN(json:JSON,compeletion:rnReturn?)  {
    RNMessageSender.emitEvent(name: "ReactNativeBridge", andPayload: ["Data":json.rawString()])
    self.rnCallBack = compeletion
  }
  //native调用RN,没有回掉
  func sendMessagesToRN(json:JSON)  {
    RNMessageSender.emitEvent(name: "ReactNativeBridge", andPayload: ["Data":json.rawString()])
  }
  
  //RN进入语音房间
  func enterAudioRoom(json:Dictionary<String,JSON>) {
    
    let password = json["roomPassword"]?.string
    let type = "audio"
    let roomId = json["roomId"]?.string
    XBHHUD.showLoading()
    NotificationCenter.default.post(
      name: NotifyName.kEnterToGameRoom.name(),
      object: nil,
      userInfo: [
        "password": password,
        "type": type,
        "roomId": roomId,
        "userName": CurrentUser.shareInstance.name,
        "userId": CurrentUser.shareInstance.id,
        "userImage": CurrentUser.shareInstance.avatar,
        "token": CurrentUser.shareInstance.token
      ])
  }
  //rn调用进入房间
  func enterRoom(json:Dictionary<String,JSON>) {
    
    if CurrentUser.shareInstance.loadingGame {
      MobClick.event("MultipleAccess", label:CurrentUser.shareInstance.id ?? "yk")
      return
    }
    CurrentUser.shareInstance.loadingGame = true
    let password = json["room_password"]?.string
    let type = json["room_type"]?.string
    let roomId = json["room_id"]?.string
    XBHHUD.showLoading()
    NotificationCenter.default.post(
      name: NotifyName.kEnterToGameRoom.name(),
      object: nil,
      userInfo: [
        "password": password,
        "type": type,
        "roomId": roomId,
        "userName": CurrentUser.shareInstance.name,
        "userId": CurrentUser.shareInstance.id,
        "userImage": CurrentUser.shareInstance.avatar,
        "token": CurrentUser.shareInstance.token
      ])
  }
  
  // 进入房间
  internal func enterToGameRoom(notification: NSNotification) {
    if let userInfo = notification.userInfo,
      let type = userInfo["type"] as? String,
      let roomId = userInfo["roomId"] as? String {
      if CurrentUser.shareInstance.realCurrentRoomID == roomId, type == "audio" {
        CurrentUser.shareInstance.loadingGame = true
        NotificationCenter.default.post(name: NotifyName.kBackGroundModel.name(), object: true)
        return
      }
      if !CurrentUser.shareInstance.realCurrentRoomID.isEmpty  {
        Utils.runInMainThread { [weak self] in
          let stay = ChatActionStruct(image: UIImage.init(named: "chatRoom_cancel"), title: "留在当前房间".localized, tag: 0)
          let change = ChatActionStruct(image: UIImage.init(named: "chatRoom_exchangeRoom"), title: "切换房间".localized, tag: 0)
          let leaveAction = ChatRoomLeaveAlert.init(height: 220, title: "是否要退出当前语音房间?", icons: [stay, change])
          Utils.showAlertView(view: leaveAction)
          leaveAction.selectBlock = { index in
            CurrentUser.shareInstance.loadingGame = false
            switch index {
            case 1:
              CurrentUser.shareInstance.loadingGame = true
              NotificationCenter.default.post(name: NotifyName.kBackGroundModel.name(), object: false, userInfo: userInfo)
            default:
              NotificationCenter.default.post(name: NotifyName.kBackGroundModel.name(), object: true, userInfo: nil)
              Utils.hideAnimated()
            }
          }
        }
        return
      }
      self.realEnterRoom(notification: notification)
    }
  }
  
  func realEnterRoom(notification: NSNotification) -> Void {
    Utils.runInMainThread {
        CurrentUser.shareInstance.loadingGame = true
        if let userInfo = notification.userInfo,
          let password = userInfo["password"] as? String,
          let type = userInfo["type"] as? String,
          let roomId = userInfo["roomId"] as? String,
          let userName = userInfo["userName"] as? String,
          let userId = userInfo["userId"] as? String,
          let userImage = userInfo["userImage"] as? String,
          let token = userInfo["token"] as? String {
          
      Utils.runInMainThread {
        if type != "lobby" {
          XBHHUD.showLoading();
        }
        messageDispatchManager.setRoomInfo(password: password, type: type, roomId: roomId, userName: userName, userId: userId, userImage: userImage, token: token)
        messageDispatchManager.open(roomid: roomId,type:type)

      }
    }
   }
  }
  
  // 推送跳转
  
  internal func remoteJump(userinfo:[String:Any], orginInfo:[String:Any]) {
    defer {
      self.remoteInfo = nil
    }
    let name = userinfo["name"]
    let userid = userinfo["userId"]
    let sex = userinfo["sex"]
    var isChating = false
    //var chatID:String = "";
    if !isGaming() {
      let currentvc = Utils.getCurrentVC(vc:(Utils.getKeyWindow()?.rootViewController)!)
      if (currentvc?.isKind(of: ChatViewController.self))! {
        isChating = true
        //chatID = (currentvc as! ChatViewController).peerID
      }
      //推送跳转
      if let block = Utils.getCachedBLContent() {
          if userid != nil {
            if block[userid] != nil {
              XBHHUD.showError("此人已经被你拉黑")
              return
            }
          }else{
            return
          }
      }
      
      if isChating {
        let convid = orginInfo["conid"] as! String;
        
        let client = LCChatKit.sharedInstance().client
        let query = client?.conversationQuery()
        query?.getConversationById(convid, callback: { (conversation, error) in
          if let conv = conversation {
            conv.countMembers(callback: { (count:Int, error) in
              if count == 2 {
                let controller = ChatViewController.init(conversationId: convid);
                controller?.peerID = userid as! String
                controller?.peerSex = sex as! String
                controller?.peerName = name as! String
                controller?.peerIcon =  userinfo["avator"] as! String
                if  userinfo["avator"] as! String != nil {
                  controller?.peerIcon =  userinfo["avator"] as! String
                }
                let nav = RotationNavigationViewController.init(rootViewController: controller!)
                currentvc?.dismiss(animated: true, completion:{
                  Utils.getKeyWindow()?.rootViewController?.present(nav, animated: true, completion: nil)
                }
                )
              } else {
                let controller = FamilyChatViewController.init(conversationId: convid);
                controller?.peerID = userid as! String
                controller?.peerSex = sex as! String
                controller?.peerName = name as! String
                controller?.peerIcon =  userinfo["avator"] as! String
                if  userinfo["avator"] as! String != nil {
                  controller?.peerIcon =  userinfo["avator"] as! String
                }
                let nav = RotationNavigationViewController.init(rootViewController: controller!)
                currentvc?.dismiss(animated: true, completion:{
                  Utils.getKeyWindow()?.rootViewController?.present(nav, animated: true, completion: nil)
                }
                )
              }
            })
          }
        })
      }else{
        let convid = orginInfo["conid"] as! String;
        
        let client = LCChatKit.sharedInstance().client
        let query = client?.conversationQuery()
        query?.getConversationById(convid, callback: { (conversation, error) in
          if let conv = conversation {
            conv.countMembers(callback: { (count:Int, error) in
              if count == 2 {
                let controller = ChatViewController.init(conversationId: convid);
                controller?.peerID = userid as! String
                controller?.peerSex = sex as! String
                controller?.peerName = name as! String
                controller?.peerIcon =  userinfo["avator"] as! String
                let nav = RotationNavigationViewController.init(rootViewController: controller!)
                currentvc?.present(nav, animated: true, completion: nil)
              } else {
                let controller = FamilyChatViewController.init(conversationId: convid);
                controller?.peerID = userid as! String
                controller?.peerSex = sex as! String
                controller?.peerName = name as! String
                controller?.peerIcon =  userinfo["avator"] as! String
                let nav = RotationNavigationViewController.init(rootViewController: controller!)
                currentvc?.present(nav, animated: true, completion: nil)
              }
            })
          }
        })
      }
    }
  }
  
  internal func isGaming() -> Bool {
    if CurrentUser.shareInstance.realCurrentRoomID == "" {
      return false
    }else{
      return true
    }
  }
  //获取图片上传token
  func getToken(jsons:Dictionary<String,JSON>,timeout:Int,action:String) {
    //上传
    RequestManager().post(url: RequestUrls.qnToken, success: {[weak self] (json) in
        if let token = json["token"].string {
          self?.uploadImage(jsons:jsons, timeout:timeout, token:token,action:action)
          }else{
            XBHHUD.showError("上传图片失败")
          }
      }, error: { (code, message) in
        XBHHUD.showError(message)
    })
  }
  

  

  //上传图片
  func uploadImage(jsons:Dictionary<String,JSON>,timeout:Int,token:String,action:String) {
    
      //串行队列
     self.flag = false
     let serial = DispatchQueue(label: "uploadSerialQueue")
      let configure = QNConfiguration.build({ (build) in
        if timeout > 0 {
          build?.timeoutInterval = UInt32(timeout)
        }
      })
      let uploadManager = QNUploadManager(configuration: configure)
      let uploadOption = QNUploadOption.init(mime: nil, progressHandler: { (str, percent) in
      }, params: nil, checkCrc: false) { [weak self]() -> Bool in
        return (self?.flag)!
      }
    if let urls = jsons["path"]?.arrayObject as? [String] {
        var imageUrls:[[String:String]] = []
        var totalRequest = 0
        for url in urls {
          if flag {
            return
          }
          serial.async {
            self.semaphore.wait()
            uploadManager?.putFile(url, key: nil, token:token, complete: { (info, key,rep) in
              totalRequest = totalRequest + 1
              self.semaphore.signal()
              if rep != nil {
                var imageRep:[String:String] = [:]
                if let imageUrl = rep!["url"] as? String {
                  imageRep["url"] = imageUrl
                }
                if let key = rep!["key"] as? String {
                  imageRep["key"] = key
                }
                imageUrls.append(imageRep)
              }else{
                if self.flag {
                  self.imageUploadResult(success: false, result: nil, action: action)
                }else{
                  self.imageUploadResult(success: false, result: nil, action: action)
                  //此时已经没有必要传了
                }
              }
              if totalRequest == urls.count {
                //说明全部请求已经完毕
                if imageUrls.count == totalRequest {
                  //说明已经全部上传成功，此时发消息告诉RN
                  self.imageUploadResult(success: true, result: imageUrls, action: action)
                }else{
                  self.imageUploadResult(success: false, result: nil, action: action)
                }
              }
            }, option: uploadOption)
          }
        }
      }else{
         self.imageUploadResult(success: false, result: nil, action: action)
      }
  }
  
  //上传分发
  func uploadDispatch(json:JSON,action:String) {
    var timeouts = 0
    if let timeout = json["options"]["timeout"].int {
      timeouts = timeout
    }
    if let cancle = json["params"]["cancel"].bool {
      if cancle {
        cancleUpload()
      }
    }
    if let _ = json["params"]["path"].array {
      getToken(jsons:json["params"].dictionaryValue,timeout:timeouts,action:action)
    }
  }
  //取消上传
  func cancleUpload() {
    self.flag = true
  }
  //上传回传消息
  func imageUploadResult(success:Bool,result:[[String:String]]?,action:String) {
    if success {
      if result != nil {
        SwiftConverter.shareInstance.sendMessagesToRN(json: JSON(["action":action,"params":["code":0,"data":result,"message":"上传成功"],"options":["timeout":0]]))
      }else{
        SwiftConverter.shareInstance.sendMessagesToRN(json: JSON(["action":action,"params":["code":400,"message":"图片上传失败"],"options":["timeout":0]]))
      }
    }else{
       SwiftConverter.shareInstance.sendMessagesToRN(json: JSON(["action":action,"params":["code":400,"message":"图片上传失败"],"options":["timeout":0]]))
    }
  }
  //查看加入信息
  
  func showFamilyInfo(familyId:String) {
    XBHHUD.showLoading()
    let para = ["maxUser":"30"]
    RequestManager().get(url:RequestUrls.groupInfo + "\(familyId)",parameters: para, success: { (json) in
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
        XBHHUD.hide()
        let sb = UIStoryboard.init(name: "FamilyDetail", bundle: nil)
        let familyDetail = sb.instantiateViewController(withIdentifier: "FamilyDetail") as! FamilyDetailViewController
        familyDetail.datas = json
        let nav = RotationNavigationViewController.init(rootViewController: familyDetail)
        ((Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)!) as UIViewController).present(nav, animated: true, completion: nil)
      })
    }) { (code, message) in
      XBHHUD.hide()
      XBHHUD.showError(message)
    }
  }
  
  // 添加好友成功的消息
  internal func sendUserWelcomeMessage(userId:String, and name:String) {
    LCChatKit.sharedInstance().client.createConversation(
      withName: name,
      clientIds: [userId],
      attributes: nil,
      options: AVIMConversationOption.unique,
      callback: {(conversation, error) in
        let sendMessage = AVIMTextMessage.init(text: "我们已经是好友了，一起开始聊聊未来吧", attributes: [
          "USER_SEX": "\(CurrentUser.shareInstance.sex)",
          "USER_ICON": CurrentUser.shareInstance.avatar,
          "USER_NAME": CurrentUser.shareInstance.name,
          "USER_ID": CurrentUser.shareInstance.id,
          "MESSAGE_TYPE": "MESSAGE_TYPE_CHAT",
          "APP": Config.app,
          "CHAT_TIME": Utils.getCurrentTimeStamp()
          ]);// 添加一个额外的key给rn
        conversation?.send(sendMessage, callback: { (isSucceeded, error) in
          if isSucceeded {
            //此时也要发送通知，告诉RN更新
            self.tellRN(id: userId)
          }
        })
    });
  }
  
  func tellRN(id:String) {
    let data:Dictionary<String,Any> = ["action":"ACTION_ADD_FRIEND","params":["user_id":id],"options":["needcallback":false,"sync":true]]
    let json = JSON.init(data)
    SwiftConverter.shareInstance.sendMessagesToRN(json: json)
  }
  
  //检查区域
  func checkServiceRegion() {
    self.getServiceRegion { (host) in
      if host != nil {
        //为空就要对比是否一致
        if let cached = Utils.getCachedSVList() {
          //说明缓存的不为空
          //对比是否一样的值
          if cached.isEqual(host) == false {
            //现在的值是不一样的需要更改
            Utils.saveSV(host!)
            self.noticeUser()
          }
        }else{
          //本地没有缓存，直接保存更改
          Utils.saveSV(host!)
          self.noticeUser()
        }
      }
    }
    
  }
  
  func noticeUser() {
    let alter =  UIAlertController(title:"提示", message: "配置信息已经更新，需要重新打开App", preferredStyle: .alert)
    let action = UIAlertAction(title: "确定", style: .default) { (action) in
      exit(0)
    }
    alter.addAction(action)
    Utils.getCurrentVC(vc: (Utils.getKeyWindow()?.rootViewController)!)?.present(alter, animated: true, completion: nil)
  }
  
  // 打印
  internal func log(content: String) {
    Print.dlog(content)
  }
  
  // TODO：测试代码，零时改变app 入口方便调试
  internal func getEnterRoomController() -> EnterViewController {
    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
    let controller = storyBoard.instantiateInitialViewController() as! EnterViewController
    return controller
  }
  
  // 广告的Controller
  internal func getAdViewController() -> ADFirstViewController {
    let controller = ADFirstViewController();
    return controller;
  }
  
  internal func getCurrentLoginUser() -> (CurrentUser) {
    return CurrentUser.shareInstance;
  }
  
  //MARK: - alertViewDelegate
  func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
    if alertView.tag == 222 {
      // 强制更新
      jumpAppStore()
      NotificationCenter.default.post(name: NotifyName.kAbortApp.name(), object: nil)
    } else if alertView.tag == 111 {
      // 普通更新
      if buttonIndex == 0 {
        jumpAppStore()
      }
    }
  }
  
  func jumpAppStore() {
    if let url = URL.init(string: LocalSaveVar.appStoreUrl) {
      if UIApplication.shared.canOpenURL(url) {
        UIApplication.shared.openURL(url)
      }
    }
  }
}
