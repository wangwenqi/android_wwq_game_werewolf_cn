/**
 * Created by wangxu on 20/02/2017.
 * 导航页
 */
import React, {Component} from 'react'
import Language from '../resources/language';
import Color from '../resources/themColor';
import {
    BackAndroid,
    StatusBar,
    NavigationExperimental,
    Platform,
    DeviceEventEmitter,
    NativeEventEmitter,
    NativeModules, AsyncStorage
} from 'react-native';
import {connect} from 'react-redux';
import Toast from '../support/common/Toast';
import * as PAGES from './pages';
import * as Scenes from '../support/actions/scene';
import {actions} from 'react-native-navigation-redux-helpers';
import * as WeChat from 'react-native-wechat';
import {getUserInfo, updateRoomId, updateGroupInfo} from '../support/actions/userInfoActions';
import {fetchSystemMessage, fetchServerMessage, promotedData} from '../support/actions/systemMessageActions';
import {updateChatData, readMessageData} from '../support/actions/chatActions';
import {updateFamilyChat, readFamilyChat, deleteFamilyChat} from '../support/actions/groupFamilyChatActions';
import {
    versionData,
    leancloudConfig,
    location,
    updateFriendVersion,
    updateNoticeVer
} from '../support/actions/configActions';
import {insertData} from '../support/actions/friendActions';
import AsyncStorageTool from '../support/common/AsyncStorageTool';
import rnRoNativeUtils from '../support/common/rnToNativeUtils';
import AV from 'leancloud-storage';
import {checkOpenRegister} from '../support/actions/checkRegisterActions';
import DeviceInfo from 'react-native-device-info';
import  * as Constant from '../support/common/constant';
import Util from '../support/common/utils';
import  * as apiDefines from '../support/common/gameApiDefines';
import  * as HostUrl from '../support/common/HostUrl';
const {
    CardStack: NavigationCardStack,
    Header: NavigationHeader,
    PropTypes: NavigationPropTypes,
    StateUtils: NavigationStateUtils,
} = NavigationExperimental
const {
    popRoute,
} = actions;
const {RNMessageSender}=NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
class AppNavigator extends Component {
    static propTypes = {
        page: React.PropTypes.string,
        popRoute: React.PropTypes.func,
        themeState: React.PropTypes.string,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        lastBackPressed: React.PropTypes.number,
        updateUserInfo: React.PropTypes.func,
        updateRoomId: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        getSystemMessage: React.PropTypes.func,
        getServerMessage: React.PropTypes.func,
        updateChatData: React.PropTypes.func,
        updateFamilyChatData: React.PropTypes.func,
        readMessageChat: React.PropTypes.func,
        readFamilyMessageChat: React.PropTypes.func,
        promotedData: React.PropTypes.func,
        versionData: React.PropTypes.func,
        checkNeedOpenRegister: React.PropTypes.func,
        updateLeancloudConfig: React.PropTypes.func,
        location: React.PropTypes.string,
        setLocation: React.PropTypes.func,
        insertData: React.PropTypes.func,
        updateFriendVersion: React.PropTypes.func,
        updateNoticeVer: React.PropTypes.func,
        friendVersion: React.PropTypes.number,
        deleteFamilyChat: React.PropTypes.func,
        updateGroupInfo: React.PropTypes.func,
    }

    /**
     * 1--注册native传输信息的listener
     */
    componentWillUnmount() {
        if (Platform.OS === 'android') {
            BackAndroid.removeEventListener('hardwareBackPress', this.onBackAndroid);
            this.onLeaveGameListener.remove();
            this.onFromBrowserListener.remove();
            this.onChateDataLister.remove();
            this.onLeaveChatListener.remove();
            this.onWeChatListener.remove();
        } else {
            this.onLeaveGameListener.remove();
            this.onChateDataLister.remove();
            this.onLeaveChatListener.remove();
        }
    }

    /**
     *
     */
    componentWillMount() {
        this.removeAllLog();
        //设置header did
        Util.setDeviceId();
        Util.setDevicePid();
        this.props.getSystemMessage();
        this.props.getServerMessage();
        AsyncStorageTool.getStorageData(AsyncStorageTool.PROMOTED_DATA, (promData) => {
            // console.log('本地存储提示好评信息：' + JSON.stringify(promData));
            this.props.promotedData(promData);
        });
        /**
         * 本地设置过location信息
         */
        AsyncStorageTool.getStorageData(AsyncStorageTool.DEVELOPER_CONFIG, (developerConfig) => {
            if (developerConfig) {
                if (developerConfig.location) {
                    this.props.setLocation(developerConfig.location);
                    rnRoNativeUtils.onSetLocation(developerConfig.location);
                }
            }
        })
        this.props.versionData();
        //去检测是否要开启手机号注册功能
        this.props.checkNeedOpenRegister();
        /**
         * 更新公告config
         */
        AsyncStorageTool.getStorageData(AsyncStorageTool.NOTICE_VER_CONFIG, (noticeData) => {
            if (noticeData && noticeData.notice_ver) {
                this.props.updateNoticeVer(noticeData.notice_ver);
            }
        })
    }

    /**
     *检查当前包的环境
     */
    checkAppHost = () => {
        switch (apiDefines.APP_KEY) {
            case apiDefines.WEREWOLF:
                if (apiDefines.URL_HOST != HostUrl.URL_HOST_GAME_WEREWOLF_ONLINE) {
                    Toast.show('Currently for the test environment');
                }
                break;
        }
    }

    /**
     * 1--移除listener
     */
    componentDidMount() {
        if (Platform.OS === 'android') {
            BackAndroid.addEventListener('hardwareBackPress', this.onBackAndroid);
            this.onLeaveGameListener = DeviceEventEmitter.addListener('EVENT_USER_LEAVE', this.onLeaveGameRoom);
            this.onFromBrowserListener = DeviceEventEmitter.addListener('EVENT_START_BROWSABLE', this.onFromBrowserData);
            this.onChateDataLister = DeviceEventEmitter.addListener('EVENT_CHAT', this.onChatData);
            this.onLeaveChatListener = DeviceEventEmitter.addListener('LEAVE_EVENT_CHAT', this.onLeaveChat);
            this.onWeChatListener = DeviceEventEmitter.addListener('WeChat_Resp', this.onWeChat);
        } else {
            this.onLeaveGameListener = rNMessageSenderEmitter.addListener('EVENT_USER_LEAVE', this.onLeaveGameRoom);
            this.onChateDataLister = rNMessageSenderEmitter.addListener('EVENT_CHAT', this.onChatData);
            this.onLeaveChatListener = rNMessageSenderEmitter.addListener('LEAVE_EVENT_CHAT', this.onLeaveChat);
        }
        //通知native rn  初始化完成
        rnRoNativeUtils.onReactNativeInitDone();
        try {
            WeChat.registerApp(apiDefines.WECHAT_PLATFORM[apiDefines.APP_TYPE]);//从微信开放平台申请
        } catch (e) {
            console.error(e);
        }
        console.log(WeChat);
        //去掉运行中的警告提示
        console.disableYellowBox = true;
        //初始化好友本地缓存
        //检查当前包的环境
        this.checkAppHost();
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        return (
            <NavigationCardStack
                direction='horizontal'//页面切换方向
                navigationState={this.props.navigation}
                renderScene={this._renderScene}
                enableGestures={false}//禁止滑动手势
                style={{backgroundColor:Color.statusBarColor}}/>
        );
    }

    /**
     * 1--进入对应的页面
     * @param props
     * @returns {XML}
     * @private
     */
    _renderScene(props) {
        switch (props.scene.route.key) {
            case Scenes.SCENE_APP_MAIN:
                return <PAGES.AppMain/>;
            case Scenes.SCENE_HOME:
                return <PAGES.Home/>;
            case Scenes.SCENE_SPLASH:
                return <PAGES.Splash/>;
            case Scenes.SCENE_MESSAGE:
                return <PAGES.Message/>;
            case Scenes.SCENE_CONTACT:
                return <PAGES.Contact/>;
            case Scenes.SCENE_MESSAGE_DETAIL:
                return <PAGES.MessageDetail/>;
            case Scenes.SCENE_CONTACT_DETAIL:
                return <PAGES.ContactDetail/>;
            case Scenes.SCENE_GAME_RULE:
                return <PAGES.GameRule/>;
            case Scenes.SCENE_GUIDE:
                return <PAGES.Guide/>;
            case Scenes.SCENE_ADD_CONTACT:
                return <PAGES.AddContact/>
            case Scenes.SCENE_SETTING:
                return <PAGES.Setting/>
            case Scenes.SCENE_EDIT_USER_INFO:
                return <PAGES.EditUserInfo/>
            case Scenes.SCENE_LOGIN_OTHER:
                return <PAGES.LoginOther/>
            case Scenes.SCENE_REGISTER:
                return <PAGES.Register/>
            case Scenes.SCENE_CHANGE_PASSWORD:
                return <PAGES.ChangePassword/>
            case Scenes.SCENE_REGISTER_USERINFO:
                return <PAGES.RegisterUserInfo/>
            case Scenes.SCENE_USER_AGREEMENT:
                return <PAGES.UserAgreement/>
            case Scenes.SCENE_COUNTRY_LIST:
                return <PAGES.CountryList/>
            case Scenes.SCENE_SELECT_COUNTRY:
                return <PAGES.SelectCountry/>
            case Scenes.SCENE_ABOUT:
                return <PAGES.About/>
            case Scenes.SCENE_PHOTO_WALL:
                return <PAGES.PhotoWall/>
            case Scenes.SCENE_DEVELOPER:
                return <PAGES.Developer/>
            case Scenes.SCENE_WEBVIEW:
                return <PAGES.WebViewPage/>
            case Scenes.SCENE_NEW_FRIEND:
                return <PAGES.NewFriend/>
            case Scenes.SCENE_VOICE_ROOM:
                return <PAGES.VoiceRoom/>
            case Scenes.SCENE_CREATE_GROUP:
                return <PAGES.CreateGroup/>
            case Scenes.SCENE_GROUP_LIST:
                return <PAGES.GroupList/>
            case Scenes.SCENE_DELETE_ALL_MESSAGE:
                return <PAGES.DeleteAllMessage/>
            case Scenes.SCENE_DELETE_ALL_CONTACT:
                return <PAGES.DeleteAllContact/>
            case Scenes.SCENE_MINI_GAME_MANAGER:
                return <PAGES.MiniGameManager/>
            case Scenes.SCENE_HOUSING_ESTATES:
                return <PAGES.HousingEstates/>
            case Scenes.SCENE_HOUSE_PROPERTY_CARD:
                return <PAGES.HousePropertyCard/>
            case Scenes.SCENE_LIKE_EACH_OTHER:
                return <PAGES.LikeEachOther/>
            case Scenes.SCENE_WEREWOLF_GAME_HOME:
                return <PAGES.WerewolfGameHome/>
            default:
                return <PAGES.Splash/>;
        }
        return <Splash/>;
    }

    /**
     * 1--从网页连接进入应用传入的房间ID和密码
     * @param event
     */
    onFromBrowserData = (event) => {
        if (event != null)
            this.props.updateRoomId(event.room_code, event.password);
    }
    /**
     * 1--android监听物理返回键
     * @returns {boolean}
     */
    onBackAndroid = () => {
        const routes = this.props.navigation.routes;
        if (routes.length > 1) {
            //当前页面是首页
            if (routes[routes.length - 1].key == Scenes.SCENE_APP_MAIN) {
                if (this.lastBackPressed + 2000 >= Date.now()) {
                    return false;
                }
                this.lastBackPressed = Date.now();
                Toast.show(Language().exit_app_toast);
                return true;
            } else if (routes[routes.length - 1].key == Scenes.SCENE_SPLASH) {
                BackAndroid.exitApp()
            } else {
                //返回上一级
                this.props.popRoute('global');
                return true
            }
        }
        return false
    };
    /**
     * 1--用户离开房间后
     * 2--更新用户信息
     * 3--获取服务器当前信息 服务器维护公告）
     */
    onLeaveGameRoom = () => {
        if (this.props.userInfo != null) {
            this.props.updateUserInfo(this.props.userInfo.id);
            /**
             * 1->兼容老版本，set location 每次启动应用只设置一次
             * 2->config 中有location信息，不用设置了
             * 3->user info 中无信息就不设置
             */
            if (!this.props.location) {
                if (this.props.userInfo.lc) {
                    rnRoNativeUtils.onSetLocation(this.props.userInfo.lc);
                    this.props.setLocation(this.props.userInfo.lc);
                }
            }

        }
        this.props.getServerMessage();
    }

    /**
     * 1--在运行打好了离线包的应用时，控制台打印语句可能会极大地拖累JavaScript线程
     * 2--上线版本会去除所有打印的日志以提高性能
     */
    removeAllLog() {
        if (!__DEV__) {
            global.console = {
                info: () => {
                },
                log: () => {
                },
                warn: () => {
                },
                debug: () => {
                },
                error: () => {
                },
            };
        }
    }

    /**
     * 1--native 聊天后消息传送给rn
     * 2--消息页面的展示以及本地持久化存储
     * @param NativeMap
     */
    onChatData = (NativeMap) => {
        if (NativeMap != null) {
            /**
             * 在这里要区分家族消息还是／其他聊天消息
             */
            if (NativeMap.MSG_TYPE && NativeMap.MSG_TYPE == Constant.FAMILY) {
                //家族消息
                if (this.props.userInfo) {
                    if (!this.props.userInfo.group) {
                        this.props.updateGroupInfo();
                    }
                }
                this.props.updateFamilyChatData(NativeMap);
            } else {
                this.props.updateChatData(NativeMap);
                if (NativeMap.INTENT && (NativeMap.INTENT == Constant.APPROVE_FRIEND)) {
                    //数据库插入数据
                    try {
                        let url = apiDefines.GET_SIMPLE_USERINFO + NativeMap.USER_ID;
                        Util.get(url, (code, message, data) => {
                            if (code == 1000) {
                                if (data && data.user) {
                                    //2 如果消息是加好友的消息插入数据
                                    this.props.insertData(data.user);
                                    //1.更新版本号
                                    this.updateFriendVersion();
                                }
                            }
                        }, (failed) => {
                        });
                    } catch (e) {
                        console.log('APPROVE_FRIEND error' + e);
                    }
                }
            }


        }
    }

    /**
     * 更新版本号
     */
    updateFriendVersion() {
        //1.更新版本号
        let version = 0;
        if (this.props.friendVersion) {
            version = this.props.friendVersion + 1;
        }
        if (version) {
            this.props.updateFriendVersion(version);
        }
    }

    /**
     * 1--从聊天房间离开
     * 2--消息状态改变为已读
     * @param event
     */
    onLeaveChat = (event) => {
        /**
         * 兼容iOS了聊天送礼物，更新用户money
         */
        if (this.props.userInfo != null) {
            this.props.updateUserInfo(this.props.userInfo.id);
        }
        //离开单聊或是家族聊天
        if (event) {
            switch (event.CONVERSATION_TYPE) {
                case Constant.FAMILY:
                    if (this.props.userInfo) {
                        if (this.props.userInfo.group) {
                            this.props.readFamilyMessageChat(this.props.userInfo.group.lc_id);
                        } else {
                            this.props.deleteFamilyChat();
                        }
                    }
                    break;
                case Constant.SINGLE:
                    if (event.USER_ID) {
                        this.props.readMessageChat(event.USER_ID);
                    }
                    break;
                default:
                    if (event.USER_ID) {
                        this.props.readMessageChat(event.USER_ID);
                    }
                    break;

            }
        }
    }
    /**
     * 1--微信sdk回调
     * 2--用于android native微信分享后的回调
     * @param event
     */
    onWeChat = (event) => {
        if (event != null) {
            NativeModules.NativeJSModule.weChatCallback(event.type, event.errCode);
        }
    }
}
const mapDispatchToProps = dispatch => ({
        popRoute: (key) => dispatch(popRoute(key)),
        updateUserInfo: (userId) => dispatch((getUserInfo(userId))),
        updateRoomId: (roomId, password) => dispatch((updateRoomId(roomId, password))),
        getSystemMessage: () => dispatch((fetchSystemMessage())),
        getServerMessage: () => dispatch((fetchServerMessage())),
        updateChatData: (chat) => dispatch(updateChatData(chat)),
        updateFamilyChatData: (chat) => dispatch(updateFamilyChat(chat)),
        readMessageChat: (id) => dispatch(readMessageData(id)),
        readFamilyMessageChat: (cl_id) => dispatch(readFamilyChat(cl_id)),
        promotedData: (data) => dispatch(promotedData(data)),
        versionData: () => dispatch(versionData()),
        checkNeedOpenRegister: () => dispatch((checkOpenRegister())),
        updateLeancloudConfig: (config) => dispatch((leancloudConfig(config))),
        updateNoticeVer: (noticeVer) => dispatch((updateNoticeVer(noticeVer))),
        setLocation: (lc) => dispatch((location(lc))),
        insertData: (data) => dispatch((insertData(data))),
        updateFriendVersion: (data) => dispatch((updateFriendVersion(version))),
        deleteFamilyChat: () => dispatch(deleteFamilyChat()),
        updateGroupInfo: () => dispatch(updateGroupInfo()),
    }
);
const mapStateToProps = state => ({
    navigation: state.cardNavigation,
    userInfo: state.userInfoReducer.data,
    location: state.configReducer.location,
    friendVersion: state.configReducer.friendVersion

});
export default connect(mapStateToProps, mapDispatchToProps)(AppNavigator)
