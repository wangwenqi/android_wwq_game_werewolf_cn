/**
 * Created by wangxu on 2017/5/16.
 * 选择国家
 */
'use strict';
import React, {Component} from 'react';
import {
    Content,
    Item,
    Input,
    Button,
    CardItem,
    Left,
    Body,
    Right,
    Text, Container, Icon
} from 'native-base';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {Image, StyleSheet, View, Platform, Dimensions, TouchableOpacity, ScrollView, ListView} from 'react-native';
import Color from '../../../resources/themColor';
import Toast from '../../../support/common/Toast';
import CountryList from '../../../resources/data/localCountryList';
import {selectedCountry} from '../../../support/actions/countryListActions';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import Language from '../../../resources/language'

const {
    popRoute,
    pushRoute
} = actions;
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const ScreenHeight = Dimensions.get('window').height;
const ScreenWidth = Dimensions.get('window').width;
const bgBack = require('../../../resources/imgs/ic_back.png');
const bgSearch = require('../../../resources/imgs/icon_search.png');

const SECTIONHEIGHT = 30;
const {width, height} = Dimensions.get('window');
const ROWHEIGHT = 40
let totalheight = [];//每个字母对应的国家和字母的总高度
let that;
class SelectCountry extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        selectCountry: React.PropTypes.func,
        countryInfo: React.PropTypes.object,
    }

    constructor(props) {
        super(props);
        var getSectionData = (dataBlob, sectionID) => {
            return sectionID;
        };
        var getRowData = (dataBlob, sectionID, rowID) => {
            return dataBlob[sectionID][rowID];
        };
        this.state = {
            dataSource: new ListView.DataSource({
                getRowData: getRowData,
                getSectionHeaderData: getSectionData,
                rowHasChanged: (row1, row2) => row1 !== row2,
                sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
            }),
            letters: [],
        };

        that = this;
    }

    render() {
        let country = this.props.countryInfo;
        let countryName = Language().taiwan;
        if (country != null) {
            if (country.n != null) {
                countryName = country.n;
            }
        }
        return (
            <Container style={{backgroundColor:(Color.colorWhite)}}>
                <CardItem
                    style={{backgroundColor:(Color.headColor),height:60,paddingTop:headPaddingTop,paddingBottom:0}}>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={this.onFinishPage.bind(this)}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf:'center',flex:3}}>
                    <Text
                        style={{color:(Color.colorWhite),fontSize: 18,alignSelf:'center'}}>{Language().select_country}</Text>
                    </Body>
                    <Right style={{flex:1}}/>
                </CardItem>
                <View
                    style={{backgroundColor:(Color.colorWhite),height:70, justifyContent: 'space-around',paddingLeft:10}}>
                    <Item style={{padding:0,margin:0,height:40}}>
                        <Image source={bgSearch} style={styles.bgSearch}/>
                        <Input
                            placeholder={Language().placeholder_select_country}
                            placeholderTextColor={Color.loginTextColor}
                            style={{borderWidth: 0,flex:1,fontSize:16,alignSelf:'center',marginRight:5}}
                            onChangeText={(text)=>this.onChangeCountry(text)}/>
                    </Item>

                </View>
                <View
                    style={{backgroundColor:(Color.countryListColor),height:40, justifyContent: 'space-around',paddingLeft:15}}>
                    <Text style={{fontSize:16,color:(Color.countryLetterColor)}}>{Language().selected_country}</Text>
                </View>
                <View
                    style={{backgroundColor:(Color.colorWhite),height:40, justifyContent: 'space-around',paddingLeft:15}}>
                    <Text style={{fontSize:16}}>{countryName}</Text>
                </View>
                <ListView
                    ref={listView => this._listView = listView}
                    contentContainerStyle={{ flexDirection: 'row', width: width, backgroundColor: 'white', justifyContent: 'space-around',flexWrap: 'wrap',}}
                    dataSource={this.state.dataSource}
                    renderRow={this._renderListRow}
                    renderSectionHeader={this._renderListSectionHeader}
                    enableEmptySections={true}
                    initialListSize={20}/>
                <View
                    style={{ position: 'absolute',height: height,top: 0, bottom: 0,right: 10, backgroundColor: 'transparent',justifyContent: 'center',alignItems: 'center',marginTop:40}}>
                    {this.state.letters.map((letter, index) => this._renderRightLetters(letter, index))}
                </View>
            </Container>
        );
    }

    _getSortLetters(dataList) {
        let list = [];

        for (let j = 0; j < dataList.length; j++) {
            let sortLetters = dataList[j].sortLetters.toUpperCase();

            let exist = false;
            for (let xx = 0; xx < list.length; xx++) {
                if (list[xx] === sortLetters) {
                    exist = true;
                }
                if (exist) {
                    break;
                }
            }
            if (!exist) {
                list.push(sortLetters);
            }
        }


        return list;
    }

    componentWillMount() {
        this.initCountry('');
    }

    /**
     * 根据搜索的更新界面
     * @param countryName
     */
    initCountry(countryName) {
        try {
            let ALL_CITY_LIST = new Array();
            CountryList.getLocalCountry(list => {
                ALL_CITY_LIST = list;
            });
            let letterList = this._getSortLetters(ALL_CITY_LIST);

            let dataBlob = {};
            ALL_CITY_LIST.map(cityJson => {
                let key = cityJson.sortLetters.toUpperCase();
                if (countryName == '') {
                    if (dataBlob[key]) {
                        let subList = dataBlob[key];
                        subList.push(cityJson);
                    } else {
                        let subList = [];
                        subList.push(cityJson);
                        dataBlob[key] = subList;
                    }
                } else {
                    let country = cityJson.n.toUpperCase();
                    if (country.indexOf(countryName) >= 0) {
                        if (dataBlob[key]) {
                            let subList = dataBlob[key];
                            subList.push(cityJson);
                        } else {
                            let subList = [];
                            subList.push(cityJson);
                            dataBlob[key] = subList;
                        }
                    }
                }
            });
            let sectionIDs = Object.keys(dataBlob);
            let rowIDs = sectionIDs.map(sectionID => {
                let thisRow = [];
                let count = dataBlob[sectionID].length;
                for (let ii = 0; ii < count; ii++) {
                    thisRow.push(ii);
                }

                var eachheight = SECTIONHEIGHT + ROWHEIGHT * thisRow.length;
                totalheight.push(eachheight);

                return thisRow;
            });


            this.setState({
                letters: sectionIDs,
                dataSource: this.state.dataSource.cloneWithRowsAndSections(dataBlob, sectionIDs, rowIDs)
            });
        } catch (e) {
            console.log('init country error' + e);
        }

    }

    /**
     * 选择某一个国家
     * @param cityJson
     * @private
     */
    _cityNameClick(cityJson) {
        this.onFinishPage();
        //更新选择的国家
        this.props.selectCountry(cityJson);
        //保存国家信息于本地
        AsyncStorageTool.saveCountryData(JSON.stringify(cityJson));
    }

    /**
     *
     * @param index
     * @param letter
     * @private
     */
    _scrollTo(index, letter) {
        Toast.hide();
        let position = 0;
        for (let i = 0; i < index; i++) {
            position += totalheight[i]
        }
        this._listView.scrollTo({
            y: position
        });
        Toast.show(letter);
    }

    _renderRightLetters(letter, index) {
        return (
            <TouchableOpacity
                key={'letter_idx_' + index}
                activeOpacity={0.6}
                onPress={()=> {
                    this._scrollTo(index, letter)
                }}>
                <View
                    style={{      height: height * 3.3 / 100,width: width * 3 / 50,justifyContent: 'center',alignItems: 'center',}}>
                    <Text
                        style={{textAlign: 'center',fontSize: height * 1.2 / 50,color: (Color.countryLetterColor),padding: 5}}>{letter}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    _renderListRow(cityJson, rowId) {
        return (
            <TouchableOpacity
                key={'list_item_' + cityJson.c}
                style={{height: ROWHEIGHT, paddingLeft: 10,paddingRight: 10,borderBottomColor: '#F4F4F4',borderBottomWidth: 0.5,}}
                onPress={()=> {
                    that._cityNameClick(cityJson)
                }}>
                <View style={{ paddingTop: 10,paddingBottom: 2,}}>
                    <Text style={{ color:'black',width: width,paddingLeft: 15}}>{cityJson.n}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    _renderListSectionHeader(sectionData, sectionID) {
        return (
            <View
                style={ { paddingTop: 5,paddingBottom: 5,height: 30,paddingLeft: 10,width: width, backgroundColor: (Color.countryListColor),} }>
                <Text style={{  color: (Color.countryLetterColor),fontWeight: 'bold',paddingLeft: 5}}>
                    {sectionData}
                </Text>
            </View>
        );
    }

    /**
     * 搜索国家
     * @param text
     */
    onChangeCountry(text) {
        this.initCountry(text);
    }

    onFinishPage() {
        this.props.finishPage('global');
    }
}
const styles = StyleSheet.create({
    bgSearch: {
        width: 18, height: 18, resizeMode: 'contain', marginLeft: 10, marginRight: 10, alignSelf: 'center'
    },
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#F4F4F4',
    },
    listContainner: {
        height: Dimensions.get('window').height,
        marginBottom: 10
    },
    contentContainer: {
        flexDirection: 'row',
        width: width,
        backgroundColor: 'white',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
    },
    letters: {
        position: 'absolute',
        height: height,
        top: 0,
        bottom: 0,
        right: 10,
        backgroundColor: 'transparent',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    letter: {
        height: height * 3.3 / 100,
        width: width * 3 / 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    letterText: {
        textAlign: 'center',
        fontSize: height * 1.2 / 50,
        color: (Color.countryLetterColor),
        padding: 5
    },
    sectionView: {
        paddingTop: 5,
        paddingBottom: 5,
        height: 30,
        paddingLeft: 10,
        width: width,
        backgroundColor: (Color.countryListColor),
    },
    sectionText: {
        color: (Color.countryLetterColor),
        fontWeight: 'bold',
        paddingLeft: 5
    },
    rowView: {
        height: ROWHEIGHT,
        paddingLeft: 10,
        paddingRight: 10,
        borderBottomColor: '#F4F4F4',
        borderBottomWidth: 0.5,
    },
    rowdata: {
        paddingTop: 10,
        paddingBottom: 2,
    },
    rowdatatext: {
        color: 'black',
        width: width,
        paddingLeft: 15
    },
});
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
        selectCountry: (data) => dispatch(selectedCountry(data)),
    }
);
const mapStateToProps = state => ({
    countryInfo: state.countryListReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps,)(SelectCountry);