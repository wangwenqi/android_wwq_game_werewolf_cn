/**
 * Created by wangxu on 2017/12/26.
 * 批量删除
 */
import React, {Component, Navigator} from 'react';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Modal,
    Image,
    StyleSheet,
    View,
    AsyncStorage,
    Platform,
    TouchableOpacity,
    ListView,
    Dimensions,
    InteractionManager,
    Text, RefreshControl
} from 'react-native';
//
import Language from '../../../resources/language/index';
import  * as styles from './style';
import * as Constant from '../../../support/common/constant';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import CachedImage from '../../../support/common/CachedImage';
import CustomCheckBox from '../../../support/common/customCheckBox';
import Toast from '../../../support/common/Toast';
import Color from '../../../resources/themColor/index';
import ImageManager from '../../../resources/imageManager/index';
import netWorkTool from '../../../support/common/netWorkTool';
import LoadMoreFooter from '../../../support/common/LoadMoreFooter';
import {getFriendList, deleteAllFriendData} from '../../../support/actions/friendActions';
import * as types from '../../../support/actions/actionTypes';
import * as apiDefines from '../../../support/common/gameApiDefines';
import Dialog, {Confirm} from '../../../support/common/dialog';
import Loading from '../../../support/common/Loading';
import Util from '../../../support/common/utils';

//
const {
    popRoute,
} = actions;
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
//
let selfThis;
let ContactDatas = new Array();
let deleteContactList;
class DeleteAllContact extends Component {
    static propTypes = {
        friendData: React.PropTypes.object,
        isRefreshing: React.PropTypes.bool,
        isNoMore: React.PropTypes.bool,
        getFriendList: React.PropTypes.func,
        deleteAllFriendData: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        selfThis = this;
        if (this.props.friendData) {
            ContactDatas = this.props.friendData.list;
        }
        deleteContactList = new Array();
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            contactDataSource: ds.cloneWithRows(ContactDatas),
        };
    }

    componentDidMount() {
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    componentWillMount() {

    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.finishTimer && clearTimeout(this.finishTimer);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (!_.isEqual(this.props.friendData, nextProps.data));
    }

    handleMethod(isConnected) {

    }

    render() {
        if (this.props.friendData != null && this.props.friendData.list) {
            ContactDatas = this.props.friendData.list;
        }
        return (<View style={styles.container}>
            <View flexDirection='row' style={styles.headerView}>
                <TouchableOpacity
                    style={[styles.touchableOpacity,{position: 'absolute', top: (Platform.OS === 'ios') ? 14 : 0, bottom: 0,left:20}]}
                    activeOpacity={0.8}
                    onPress={this.finishPage.bind(this)}>
                    <Text style={[styles.mainTitle,{fontSize:16}]}>{Language().cancel}</Text>
                </TouchableOpacity>
                <Text style={styles.mainTitle}>{Language().contact}</Text>
                <TouchableOpacity
                    style={[styles.touchableOpacity,{position: 'absolute', top: (Platform.OS === 'ios') ? 14 : 0, bottom: 0,right:20}]}
                    activeOpacity={0.8}
                    onPress={this.deleteAllContact.bind(this)}>
                    <Text style={[styles.mainTitle,{fontSize:16}]}>{Language().delete}</Text>
                </TouchableOpacity>
            </View>
            <ListView dataSource={this.state.contactDataSource.cloneWithRows(ContactDatas)}
                      renderRow={this.renderRow.bind(this)}
                      renderFooter={this.renderFooter.bind(this)}
                      onEndReached={this._onEndReach.bind(this)}
                      initialListSize={1}
                      pageSize={5}
                      removeClippedSubviews={false}
                      onEndReachedThreshold={10}
                      enableEmptySections={true}
                      refreshControl={
                                     <RefreshControl
                                        refreshing={this.props.isRefreshing}
                                         onRefresh={this._onRefresh.bind(this)}
                                         colors={[Color.list_loading_color]} />
                                    }/>
            {Dialog.inject()}
            <Loading ref="progressDialog" loadingTitle={Language().deleting_contact}/>
            <Confirm ref='deleteDialog' title={Language().delete} posText={Language().delete}
                     message={ Language().delete_all_contact}
                     messageStyle={{alignSelf:'center',fontSize: 14,marginBottom:10}}
                     titleStyle={{alignSelf:'center',fontSize: 17,padding: 5}}
                     contentStyle={{paddingLeft:1,paddingBottom: 3,paddingRight: 1,width: ScreenWidth/3*2+30}}
                     buttonBarStyle={{justifyContent:'center'}}
                     buttonStyle={{textAlign: 'center',fontSize: 16,padding: 10,marginLeft: 0,marginRight: 0,alignSelf:'center',justifyContent:'center',flex:1,color:Color.system_dialog_bt_text_color}}
                     negText={Language().cancel}
                     enumeSeparator={true}
                     rowSeparator={true}
                     onNegClick={this.dismissDialog.bind(this)} onPosClick={this.doDeleteAllContact.bind(this)}>
            </Confirm>
        </View>);
    }

    /**
     * 加载更多
     * @private
     */
    renderFooter() {
        return ContactDatas.length < 9 ? null : (<LoadMoreFooter isNoMore={this.props.isNoMore}/>);
    }

    /**
     * load more
     * @private
     */
    _onEndReach = () => {
        //如果网络异常情况下处理
        let data = this.props.friendData;
        if (data) {
            if (data.page < data.total_page) {
                this.props.getFriendList(data.page + 1);
            } else {
                this.props.getFriendList(data.page);
            }
        }
    }

    /**
     *对应的item
     */
    renderRow(item, sectionId, rowId) {
        return (<View style={{overflow:'hidden'}}>
            <View flexDirection='row'
                  style={{backgroundColor:Color.colorWhite,paddingLeft:40,width:ScreenWidth,height:65}}>
                <CachedImage style={styles.headThumbnail}
                             source={(!item.image)?ImageManager.defaultUser:({uri:item.image})}
                             defaultSource={ImageManager.defaultUser}/>
                <View style={{alignSelf:'center',marginLeft:8}}>
                    <Text numberOfLines={1}
                          style={{color:Color.voice_contact_name_color,padding:2,fontSize:16}}>{item.name}</Text>
                </View>

            </View>
            <View style={styles.rowSeparator}></View>
            {this.renderCheckBox(item)}
        </View>);
    }

    /**
     * 刷新
     * @private
     */
    _onRefresh = () => {
        this.props.isRefreshing = true;
        InteractionManager.runAfterInteractions(() => {
            //刷新消息列表
            this.props.getFriendList(1);
        });
    };

    /**
     *
     * @param data
     * @returns {XML}
     */
    renderCheckBox(item) {
        let data = {
            checked: false,
            id: item.id,
            name: item.name
        }
        return (
            <CustomCheckBox
                style={{flex: 1,position: 'absolute',width:ScreenWidth,height:65,paddingLeft:10}}
                onClick={()=>this.onClick(data)}
                isChecked={data.checked}/>);
    }

    /**
     *
     * @param data
     */
    onClick(data) {
        data.checked = !data.checked;
        if (data.checked) {
            deleteContactList.push(data.id);
        } else {
            let index = deleteContactList.indexOf(data.id);
            if (index != -1) {
                deleteContactList.splice(index, 1);
            }
        }
    }

    /**
     *取消编辑
     */
    finishPage() {
        if (!this.state.isFinishing) {
            this.props.finishPage('global');
            this.state.isFinishing = true;
        }
        this.finishTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }

    deleteAllContact() {
        if (deleteContactList && deleteContactList.length > 0) {
            if (this.refs.deleteDialog) {
                this.refs.deleteDialog.show();
            }
        } else {
            Toast.show(Language().prompt_delete_contact);
        }
    }

    /**
     *
     */
    doDeleteAllContact() {
        if (deleteContactList && deleteContactList.length > 0) {
            this.dismissDialog();
            this.showProgressLoading();
            this.requestApi();
        } else {
            Toast.show(Language().prompt_delete_contact);
        }
    }

    dismissDialog() {
        this.refs.deleteDialog.hide();
    }

    /**
     *
     */
    showProgressLoading() {
        if (this.refs.progressDialog) {
            this.refs.progressDialog.show();
        }
    }

    /**
     *
     */
    hideProgressLoading() {
        if (this.refs.progressDialog) {
            this.refs.progressDialog.hide();
        }
    }

    requestApi() {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                let stringDeleteList=deleteContactList.join(',');
                let url = apiDefines.DELETE_FRIEND + stringDeleteList;
                Util.get(url, (code, message, data) => {
                    this.hideProgressLoading();
                    if (code == 1000) {
                         this.props.deleteAllFriendData(deleteContactList);
                         this.finishPage();
                    }
                    Toast.show(message);
                }, (failed) => {
                    this.hideProgressLoading();
                    Toast.show(netWorkTool.NOT_NETWORK);
                });
            } else {
                this.hideProgressLoading();
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }
}

const mapDispatchToProps = dispatch => ({
    finishPage: (key) => dispatch(popRoute(key)),
    getFriendList: (page) => dispatch(getFriendList(page)),
    deleteAllFriendData: (list) => dispatch(deleteAllFriendData(list)),
});
const mapStateToProps = state => ({
    friendData: state.friendReducer.data,
    isRefreshing: state.friendReducer.isRefreshing,
    isNoMore: state.friendReducer.isNoMore,
});
export default connect(mapStateToProps, mapDispatchToProps)(DeleteAllContact);