/**
 * Created by wangxu on 2017/12/8.
 */
'use strict';
import {StyleSheet, Platform, Dimensions} from 'react-native';
import  * as apiDefines from '../../../support/common/gameApiDefines';

import Color from '../../../resources/themColor/index';
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.voice_background_color
    },
    headerView: {
        backgroundColor:apiDefines.APP_KEY == apiDefines.WEREWOLF ? Color.headColor : Color.colorWhite,
        height: (Platform.OS === 'ios') ? 60 : 50,
        paddingTop: (Platform.OS === 'ios') ? 14 : 0,
        justifyContent: 'center',
        marginBottom:2
    },
    touchableOpacity: {
        justifyContent: 'center',
        backgroundColor: Color.transparent,
    },
    mainTitle: {
        fontSize: 18,
        color:apiDefines.APP_KEY == apiDefines.WEREWOLF ? Color.colorWhite: Color.voice_main_title_color,
        backgroundColor: Color.transparent,
        textAlign: 'center',
        alignSelf: 'center',
    },
    allowView: {
        width: 13,
        height: 13,
        resizeMode: 'contain',
        alignSelf: 'flex-end',
        margin: 10,
        marginRight: 0
    },
    baseView: {
        backgroundColor: Color.colorWhite
    }, headThumbnail: {
        width: 50,
        height: 50,
        borderRadius: 25,
        alignSelf: 'center'
    }, rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: Color.voice_separator_line_color,
    },
});
module.exports = Styles;