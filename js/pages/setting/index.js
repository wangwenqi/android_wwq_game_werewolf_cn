/**
 * Created by wangxu on 2017/3/4.
 */
'use strict';
import React, {Component} from 'react';
import Language from '../../../resources/language';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Container,
    Button,
    Left,
    Text,
    Right,
    Body,
    Content,
    Card,
    CardItem,
    Item,
    ListItem
} from 'native-base';
import * as Constant from '../../../support/common/constant';

import {Image, StyleSheet, View, AsyncStorage, Platform, Dimensions, NativeModules} from 'react-native';
import Color from '../../../resources/themColor';
import * as SCENES from '../../../support/actions/scene';
import Toast from '../../../support/common/Toast';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import {removeChat} from '../../../support/actions/chatActions';
import {userInfo, updateTourist} from '../../../support/actions/userInfoActions';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import {clearFriendList} from '../../../support/actions/friendActions';
import {clearMessageList} from '../../../support/actions/messageActions';
import {removeFamilyChat} from '../../../support/actions/groupFamilyChatActions';
import {removeFriendStatus} from '../../../support/actions/friendStatusAction';
import {clearRoomOwnedList} from '../../../support/actions/recreationActions';
import {resetTaskInfo} from '../../../support/actions/taskInfoActions';
import rnToNativeUtils from '../../../support/common/rnToNativeUtils';
import {clearGameHistory} from '../../../support/actions/miniGameActions';

const {
    popRoute,
    pushRoute,
} = actions;
const androidNative = NativeModules.NativeJSModule;
const bgBack = require('../../../resources/imgs/ic_back.png');
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const headHeight = (Platform.OS === 'ios') ? 60 : 50;
const ScreenHeight = Dimensions.get('window').height;
const rightArrow = require('../../../resources/imgs/bg_arrow_right.png');
const bgshare = require('../../../resources/imgs/settings_icon_share.png');
const bgUpdate = require('../../../resources/imgs/setting_icon_update.png');
const bgAbout = require('../../../resources/imgs/setting_icon_about.png');
const bgFeedback = require('../../../resources/imgs/setting_icon_feedback.png');
const bgReview = require('../../../resources/imgs/setting_icon_review.png');
const bgPayQA = require('../../../resources/imgs/setting_icon_qa.png');
const bgBlackList = require('../../../resources/imgs/setting_icon_black.png');

class Setting extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        openPage: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        removeChatData: React.PropTypes.func,
        jumpToLogin: React.PropTypes.func,
        updateUserInfo: React.PropTypes.func,
        updateTourist: React.PropTypes.func,
        clearMessageList: React.PropTypes.func,
        removeFamilyChat: React.PropTypes.func,
        removeFriendStatus: React.PropTypes.func,
        clearFriendList: React.PropTypes.func,
        resetTaskInfo: React.PropTypes.func,
        clearRoomOwnedList: React.PropTypes.func,
        clearGameHistory: React.PropTypes.func,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        configData: React.PropTypes.object
    }

    constructor(props) {
        super(props);
        this.state = {
            disabled: false
        };
    }

    render() {
        let reviewView = this._reviewView();
        if (Platform.OS === 'ios') {
            if (this.props.configData != null && this.props.configData.ios_upgrade) {
                reviewView = this._reviewView();
            } else {
                reviewView = null;
            }
        }
        let upgradeView = this._checkVersionView();
        if (Platform.OS === 'ios') {
            if (this.props.configData != null && this.props.configData.ios_upgrade) {
                upgradeView = this._checkVersionView();
            } else {
                upgradeView = null;
            }
        }
        return (
            <Container style={{flex: 1, backgroundColor: Color.content_color}}>
                <CardItem
                    style={{
                        backgroundColor: (Color.headColor),
                        height: headHeight,
                        paddingTop: headPaddingTop,
                        paddingBottom: 0
                    }}>
                    <Left style={{flex: 1}}>
                        <Button transparent onPress={this.onFinishPage.bind(this)} disabled={this.state.disabled}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf: 'center', flex: 3}}>
                    <Text
                        style={{color: Color.colorWhite, fontSize: 18, alignSelf: 'center'}}>{Language().setting}</Text>
                    </Body>
                    <Right style={{flex: 1}}/>
                </CardItem>
                <Container style={{marginTop: 20, flex: 1}}>
                    <View style={{backgroundColor: Color.colorWhite}}>
                        <View style={styles.rowSeparator}></View>
                        <ListItem button onPress={this.onShareMessage.bind(this)}>
                            <Left>
                                <Image source={bgshare} style={styles.bgIcon}></Image>
                                <Text
                                    style={{
                                        color: Color.setting_text_color,
                                        fontSize: 16,
                                        alignSelf: 'flex-start'
                                    }}>{Language().share_friend}</Text>
                            </Left>
                            <Right>
                                <Image source={rightArrow}
                                       style={styles.rightArrow}></Image>
                            </Right>
                        </ListItem>
                        {/*{upgradeView}*/}
                        <ListItem button onPress={this.onAbout.bind(this)}>
                            <Left>
                                <Image source={bgAbout} style={styles.bgIcon}></Image>
                                <Text
                                    style={{
                                        color: Color.setting_text_color,
                                        fontSize: 16,
                                        alignSelf: 'flex-start'
                                    }}>{Language().about}</Text>
                            </Left>
                            <Right>
                                <Image source={rightArrow}
                                       style={styles.rightArrow}></Image>
                            </Right>
                        </ListItem>
                        <ListItem button onPress={this.onFeedBack.bind(this)}>
                            <Left>
                                <Image source={bgFeedback} style={styles.bgIcon}></Image>
                                <Text
                                    style={{
                                        color: Color.setting_text_color,
                                        fontSize: 16,
                                        alignSelf: 'flex-start'
                                    }}>{Language().feedback}</Text>
                            </Left>
                            <Right>
                                <Image source={rightArrow}
                                       style={styles.rightArrow}></Image>
                            </Right>
                        </ListItem>
                        {reviewView}
                        {Platform.OS === 'ios' ? <ListItem button onPress={this.openPayFAQ.bind(this)}>
                            <Left>
                                <Image source={bgPayQA} style={[styles.bgIcon, {width: 22, height: 22}]}></Image>
                                <Text
                                    style={{
                                        color: Color.setting_text_color,
                                        fontSize: 16,
                                        alignSelf: 'flex-start'
                                    }}>{Language().pay_faq}</Text>
                            </Left>
                            <Right>
                                <Image source={rightArrow}
                                       style={styles.rightArrow}></Image>
                            </Right>
                        </ListItem> : null}

                        <ListItem button onPress={this.openBlackList.bind(this)}>
                            <Left>
                                <Image source={bgBlackList} style={[styles.bgIcon, {width: 22, height: 22}]}></Image>
                                <Text
                                    style={{
                                        color: Color.setting_text_color,
                                        fontSize: 16,
                                        alignSelf: 'flex-start'
                                    }}>{Language().black_list}</Text>
                            </Left>
                            <Right>
                                <Image source={rightArrow}
                                       style={styles.rightArrow}></Image>
                            </Right>
                        </ListItem>
                        <View style={styles.rowSeparator}></View>
                    </View>
                </Container>
                <Container style={{flex: 1, justifyContent: 'flex-end'}}>
                    <Button full
                            style={{
                                margin: 10,
                                borderRadius: 3,
                                backgroundColor: Color.logout_button_color,
                                marginBottom: 30
                            }}
                            onPress={this.actionLogout.bind(this)}>
                        <Text style={{color: Color.colorWhite, fontSize: 17}}>{Language().logout}</Text>
                    </Button>
                </Container>
            </Container>
        );
    }

    /**
     * 黑名单
     */
    openBlackList() {
        // 启动黑名单列表
        // action "black_list"
        // type "RN_Native"
        let NativeData = {
            action: Constant.BLACK_LIST,
            type: Constant.RN_NATIVE
        }
        rnToNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }

    /**
     * 支付常见问题
     */
    openPayFAQ() {
        rnRoNativeUtils.openPayFAQ();
    }

    /**
     * 退出页面
     */
    onFinishPage() {
        this.finishTimer = setTimeout(() => {
            this.setState({
                disabled: false
            });
        }, 2000);
        this.props.finishPage('global');
        this.setState({
            disabled: true
        });
    }

    /**
     *
     */
    actionLogout() {
        if (Platform.OS === 'ios') {
            let main = NativeModules.MainViewController;
            if (main && main.couldLogout) {
                main.couldLogout((error, logout) => {
                    if (!error) {
                        if (logout == 1) {
                            this.onLogout();
                        }
                    } else {
                        this.onLogout();
                    }
                });
            } else {
                this.onLogout();
            }
        } else {
            if (androidNative && androidNative.permissionExit) {
                androidNative.permissionExit((logout) => {
                    if (logout == 0) {
                        this.onLogout();
                    }
                });
            } else {
                this.onLogout();
            }
        }
    }

    /**
     * 退出登录操作
     */
    onLogout() {
        this.onFinishPage();
        const routes = this.props.navigation.routes;
        if (routes.length > 1) {
            for (var i = 0; i < routes.length; i++) {
                this.props.finishPage('global');
            }
        }
        if (this.props.userInfo != null && this.props.userInfo.id != null) {
            let userId = this.props.userInfo.id;
            rnRoNativeUtils.closeLeanCloud(userId);
        }
        let nativeData = {
            action: Constant.ACTION_LOGIN_OUT,
            type: Constant.RN_NATIVE,
        }
        rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
        AsyncStorageTool.removeUserInfo();
        try {
            this.props.openPage(SCENES.SCENE_SPLASH, 'global');
        } catch (e) {
            console.log(e)
        }
        AsyncStorageTool.removeSignTourist();
        this.props.removeChatData();
        this.props.updateUserInfo(null);
        this.props.updateTourist(false);
        this.props.clearFriendList();
        this.props.clearMessageList();
        this.props.removeFamilyChat();
        this.props.removeFriendStatus();
        this.props.resetTaskInfo();
        this.props.clearRoomOwnedList();
        this.props.clearGameHistory();

    }

    /**
     * 分享
     */
    onShareMessage() {
        rnRoNativeUtils.onShareMessage(this.props.userInfo.name);
    }

    /**
     * 检测更新
     */
    onCheckVersion() {
        rnRoNativeUtils.onCheckUpgrade(true);
    }

    /**
     * 关于我们
     */
    onAbout() {
        this.props.openPage(SCENES.SCENE_ABOUT, 'global');
    }

    /**
     * 意见反馈
     */
    onFeedBack() {
        rnRoNativeUtils.onFeedback();
    }

    /**
     * 用户评论
     */
    onReview() {
        rnRoNativeUtils.onApplicationMarket();
    }

    _reviewView() {
        return (<ListItem button onPress={this.onReview.bind(this)}>
            <Left>
                <Image source={bgReview} style={styles.bgIcon}></Image>
                <Text
                    style={{
                        color: Color.setting_text_color,
                        fontSize: 16,
                        alignSelf: 'flex-start'
                    }}>{Language().review}</Text>
            </Left>
            <Right>
                <Image source={rightArrow}
                       style={styles.rightArrow}></Image>
            </Right>
        </ListItem>)
    }

    _checkVersionView() {
        return (<ListItem button onPress={this.onCheckVersion.bind(this)}>
            <Left>
                <Image source={bgUpdate} style={styles.bgIcon}></Image>
                <Text
                    style={{
                        color: Color.setting_text_color,
                        fontSize: 16,
                        alignSelf: 'flex-start'
                    }}>{Language().update_version}</Text>
            </Left>
            <Right>
                <Image source={rightArrow}
                       style={styles.rightArrow}></Image>
            </Right>
        </ListItem>);
    }

    componentWillUnmount() {
        this.finishTimer && clearTimeout(this.finishTimer);
    }
}

const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    bgIcon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    rightArrow: {
        width: 15, height: 15, resizeMode: 'contain', marginLeft: 10
    },
    rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: Color.rowSeparator_line_color,
    },
});
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        removeChatData: () => dispatch(removeChat()),
        updateUserInfo: (data) => dispatch((userInfo(data))),
        updateTourist: (isTourist) => dispatch((updateTourist(isTourist))),
        clearMessageList: () => dispatch(clearMessageList()),
        removeFamilyChat: () => dispatch(removeFamilyChat()),
        removeFriendStatus: () => dispatch(removeFriendStatus()),
        clearFriendList: () => dispatch(clearFriendList()),
        resetTaskInfo: () => dispatch(resetTaskInfo()),
        clearRoomOwnedList: () => dispatch(clearRoomOwnedList()),
        clearGameHistory: () => dispatch(clearGameHistory()),
    }
);
const mapStateToProps = state => ({
    userInfo: state.userInfoReducer.data,
    navigation: state.cardNavigation,
    configData: state.registerReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps,)(Setting);