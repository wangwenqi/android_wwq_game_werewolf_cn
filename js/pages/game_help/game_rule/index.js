/**
 * Created by wangxu on 2017/3/3.
 */
'use strict';
import React, {Component} from 'react';
import {Container, Content, Card, CardItem, Text, Body, Header, Left, Button, Icon, Title, Right} from 'native-base';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {ToastAndroid} from 'react-native';
const {
    popRoute,
} = actions;
class GameRule extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        gameHelpType: React.PropTypes.string,
    }

    onFinishPage() {
        this.props.finishPage('global');
    }

    render() {
        ToastAndroid.show(this.props.gameHelpType, ToastAndroid.SHORT);
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={this.onFinishPage.bind(this)}>
                            <Icon name={'arrow-back'}/>
                        </Button>
                    </Left>
                    <Body>
                    <Title>游戏规则</Title>
                    </Body>
                    <Right>

                    </Right>
                </Header>
                <Content>
                    {/*简单模式规则*/}
                    <Card style={{margin:10}}>
                        <CardItem header>
                            <Text>胜利方式</Text>
                        </CardItem>

                        <CardItem>
                            <Body>
                            <Text>
                                游戏分为两大阵营：好人和狼人
                                好人阵营以投票为主要手段处决狼人获取胜利；狼人阵营隐匿于好人中间，靠夜晚杀人和骗取好人信任获胜。
                                狼人 技能：
                                每个夜晚可以杀死一个人。
                                预言家 技能：
                                每个夜晚可以查验一名玩家身份，知道ta 是狼人还是好人。
                                女巫 技能：
                                女巫有两瓶药水，一瓶是灵药，可以救活在晚上被杀的玩家；另一瓶是毒药，可以在晚上毒死一位玩家。女巫一晚上不能同时使用两瓶药。
                                猎人 技能：
                                当猎人死亡时，可以开枪射杀一位玩家（俗称‘带走’）。但死于女巫毒药时，不能开枪。
                                村民 技能：
                                无特殊能力。白天可以通过分析和推理帮助好人投杀狼人。

                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>one</Text>
                        </CardItem>
                    </Card>
                    <Card style={{margin:10}}>
                        <CardItem header>
                            <Text>玩法攻略</Text>
                        </CardItem>

                        <CardItem>
                            <Body>
                            <Text>
                                狼人：狼人到了白天，要假扮好人隐藏自己的身份，故意误导或陷害其他好人。
                                预言家：思考如何帮助好人的同时又不暴露自己的身份。因为狼人最担心预言家查出自己身份，所以会优先袭击预言家。
                                女巫：要合理使用自己的两瓶药，灵药可以用来救别人也可以用来救自己，毒药请小心使用，避免误杀好人。
                                猎人：在发动技能时要精准地射杀狼人，避免误杀好人。
                                普通村民：虽然没有技能，但是可以用发言来混淆狼人视听，帮助好人获得胜利，有时甚至可以通过发言伪装成其他角色，改变局面走向。
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>two</Text>
                        </CardItem>
                    </Card>
                    {/*标准模式规则*/}
                    <Card style={{margin:10}}>
                        <CardItem header>
                            <Text>胜利方式</Text>
                        </CardItem>
                        <CardItem>
                            <Body>
                            <Text>
                                游戏分为三大阵营：狼人，好人，情侣（情侣阵营只在情侣双方为人狼恋时出现）。
                                好人阵营：以投票为主要手段处决狼人获胜。
                                狼人阵营：杀死所有村民或所有神职即可获胜（俗称“屠边”）
                                情侣阵营：杀死除了情侣和爱神以外的所有玩家即可获胜
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>third</Text>
                        </CardItem>
                    </Card>
                    <Card style={{margin:10}}>
                        <CardItem header>
                            <Text>竞选警长</Text>
                        </CardItem>
                        <CardItem>
                            <Body>
                            <Text>
                                警长通过竞选产生，由未参加竞选的玩家投票。
                                作为最有威信的人物，警长的投票将作为1.5票。
                                警长拥有归票权，他总在最后一个发言。
                                警长被杀后可以选择其他玩家继承警长。

                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>third</Text>
                        </CardItem>
                    </Card>
                    <Card style={{margin:10}}>
                        <CardItem>
                            <Body>
                            <Text>
                                狼人  技能：
                                每个夜晚可以杀死一个人。
                                爱神  技能：
                                爱神可以在第一天指定任意两个（包括自己）成为情侣，情侣中有一人死去时，另一人将自动殉情。
                                胜利方式：
                                如果射中的情侣是人人情侣或狼狼情侣，则爱神获胜方式和好人阵营相同；
                                如果射中的情侣是人狼情侣，则爱神和情侣组成情侣阵营，需要除掉其他所有玩家。

                                预言家 技能：
                                每个夜晚可以查验一名玩家身份，知道ta 是狼人还是好人。
                                女巫 技能：
                                女巫有两瓶药水，一瓶是灵药，可以救活在晚上被杀的玩家；另一瓶是毒药，可以在晚上毒死一位玩家。女巫一晚上不能同时使用两瓶药。
                                猎人 技能：
                                当猎人死亡时，可以开枪射杀一位玩家（俗称‘带走’）。但死于女巫毒药时，不能开枪。
                                村民 技能：
                                无特殊能力。白天可以通过分析和推理帮助好人投杀狼人。
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>third</Text>
                        </CardItem>
                    </Card>
                    <Card style={{margin:10}}>
                        <CardItem header>
                            <Text>玩法攻略：</Text>
                        </CardItem>
                        <CardItem>
                            <Body>
                            <Text>
                                警长：警长要利用好警徽流，可以为好人提供很多有用的线索，强神牌要积极地竞选警长，不要让警长落入狼人的手中。
                                爱神：爱神不要轻易暴露出情侣是谁，可以先通过一些方式判断情侣的身份，如果是人狼情侣的话，胜利条件是单独结算的，所以爱神需要和情侣完美地配合，不然会被狼人和好人阵营同时攻击。
                                狼人：狼人到了白天，要假扮好人隐藏自己的身份，故意误导或陷害其他好人。在形势允许的情况下，积极地竞选警长，拿到警徽流来混淆好人视听。
                                预言家：思考如何帮助好人的同时又不暴露自己的身份。因为狼人最担心预言家查出自己身份，所以会优先袭击预言家。
                                女巫：要合理使用自己的两瓶药，灵药可以用来救别人也可以用来救自己，毒药请小心使用，避免误杀好人。
                                猎人：在发动技能时要精准地射杀狼人，避免误杀好人。
                                普通村民：虽然没有技能，但是可以用发言来混淆狼人视听，帮助好人获得胜利，有时甚至可以通过发言伪装成其他角色，改变局面走向。
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>third</Text>
                        </CardItem>
                    </Card>
                    {/*狼人杀人词典*/}
                    <Card style={{margin:10}}>
                        <CardItem header>
                            <Text>常用词：</Text>
                        </CardItem>
                        <CardItem>
                            <Body>
                            <Text>
                                查杀：预言家在查验出狼人后，号召大家投票处决该狼人。
                                挡刀：指为了保护某个身份，某位玩家装作自己是其他身份去骗取狼人的信任，晚上被狼人杀害，为好人做出贡献。
                                悍跳：指狼人跳好人身份。
                                对跳：两个或以上的角色均称自己是某种身份。由于身份只有一个，对跳的身份里面至多一个是真的，其余都是假的。但是身份假的并不一定是坏人，也可能是炸身份。
                                认出：声称自己是好人身份，但是不是非常重要的角色，如果不相信，可以把自己投出去也没问题。
                                金水：预言家在查验过后是好人的角色。
                                银水：特指被女巫就过的角色（不一定是好身份）。
                                退水：先声称自己是某个身份，随后又反悔说自己并不是那个身份。
                                聊爆：指话语中有明显纰漏，比如不经意间透漏了村民不应该知道，但狼人知道的事，包括：存活狼人的人数，死者的死因。
                                扛推：某位好人玩家在发言环节被人怀疑，被投票出局或者将被票出局，为其他玩家抗住了被推出局的风险，简称“抗推”。
                                捞：A玩家认为B玩家是好角色，所以为B辩解，简称“捞”。
                                归票：通常指最后一个发言的人总结发言，号召大家集中将某人投票出局。
                                高阶词：
                                水包：自认平民的人给出的2个或者多个怀疑对象，希望在他们中通过发言来对比身份的好坏从而找出狼人。
                                反水：被预言家（无论真假）验（为好人方的身份）后，不认可对方的验人。如，有预言家声称验了一个人是村民，此人拒绝承认自己是村民，声称自己是猎人或对跳预言家。
                                上警：参与警长竞选。
                                警上：参与警长竞选的玩家。
                                警下：不参与警长竞选的玩家。
                                撕警徽：警长出局后不选择其他玩家继承警长。
                                警徽流：警徽流就是警长或想要竞选警长的玩家交代前一天晚上验身份的结果。因为夜晚被杀是没遗言的，所以警长归票前，可以说我要查几号的身份，如果他是好人，警徽会给他；如果他是坏人，警徽会给谁，这样可以让玩家万一死了也能给其他玩家留下线索。
                                抿：即猜测某人的身份。举例：如果某人的发言无理由地保全另一个人，且十分肯定，狼队可能会猜测他是一名新手预言家，晚上杀他，这一行为可称为“抿预言家”。
                                分票：在某人遭遇多人质疑的时候，提出另一个人的疑点，试图将投票分散。分票是狼人常用的技巧。
                                绑票：集体把票投给某人，试图靠人数优势将其投出局。
                                掰票：在某人被认为是狼人之后，为其辩解。
                                生推：预言家被杀后，场上村民比发言，无信息量无团队划分的游戏状况。
                                前置位：由于发言通过由被杀的下一位开始轮流发言，所以相对来说先发言的位置称为前置位。
                                后置位：由于发言通常由被杀的人下一位开始轮流发言，所以相对来说后发言的位置称为后置位。
                                贴脸：指的是做出一些威胁，发毒誓，骂人等于游戏剧情无关的额发言，以求博得他人信任的行为，例如“今天不票出我的查杀，我就死给你们看”“我这把如果是狼就不是人”“是狼死全家”。由于这种行为带入了场外的因素，所以可信度很高。

                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>third</Text>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key))
    }
);
const mapStateToProps = state => ({
    gameHelpType: state.gameHelpType,
});
export default connect(mapStateToProps, mapDispatchToProps,)(GameRule);