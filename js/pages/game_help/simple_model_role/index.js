/**
 * Created by wangxu on 2017/3/15.
 */
'use strict';
import React, {Component} from 'react';
import {Content, Card, CardItem, Text, Body, Left, Button, Icon, Title, Right, ListItem, Thumbnail} from 'native-base';
export default class SimpleModelRole extends Component {
    render() {
        return (
            <Content>
                <CardItem header><Text>胜利方式</Text></CardItem>
                <CardItem cardBody>
                    <Text> 游戏分为两大阵营：好人和狼人
                     好人阵营以投票为主要手段处决狼人获取胜利；狼人阵营隐匿于好人中间，靠夜晚杀人和骗取好人信任获胜。</Text>
                </CardItem>
            </Content>
        );
    }
}