/**
 * Created by wangxu on 2017/6/13.
 * 编辑照片墙
 */
'use strict';
import React, {Component} from 'react';
import Language from '../../../resources/language';
import Color from '../../../resources/themColor';
import * as SCENES from '../../../support/actions/scene';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Image,
    StyleSheet,
    View,
    Platform,
    TouchableOpacity,
    Dimensions,
    NativeModules,
    ListView,
    InteractionManager, DeviceEventEmitter, NativeEventEmitter
} from 'react-native';
import Toast from '../../../support/common/Toast';
import netWorkTool from '../../../support/common/netWorkTool';
import Util from '../../../support/common/utils';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import {userInfo, getUserInfo} from '../../../support/actions/userInfoActions';
import CachedImage from '../../../support/common/CachedImage';

import ImageResizer from 'react-native-image-resizer';
import Loading from '../../../support/common/Loading';

import {
    Button,
    Left,
    Text,
    Right,
    Body,
    Content, Container, Spinner, CardItem
} from 'native-base';
import Dialog, {Prompt} from '../../../support/common/dialog';
import ImagePicker from 'react-native-image-crop-picker';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import UtilsTool from '../../../support/common/UtilsTool';
import * as Constant from '../../../support/common/constant';
const {
    pushRoute,
    popRoute
} = actions;
const bgSelect = require('../../../resources/imgs/me_icon_selected.png');
const bgAdd = require('../../../resources/imgs/icon_add.png');
const bgBack = require('../../../resources/imgs/ic_back.png');
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const headHeight = (Platform.OS === 'ios') ? 60 : 50;

const ScreenHeight = Dimensions.get('window').height;
const ScreenWidth = Dimensions.get('window').width;
//
const {RNMessageSender}=NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
let progressDialog;
//获取屏幕宽度
let {width} = Dimensions.get('window');
//常量设置
let cols = 2;
let cellWH = 150;
let vMargin = (width - cellWH * cols) / (cols + 1);
let hMargin = 5;
var that;
let count = 0;
let images;
let photos;
class PhotoWall extends Component {
    constructor(props) {
        super(props);
        that = this;
        images = new Array();
        try {
            let photosJson = JSON.stringify(this.props.userInfo.photos);
            images = JSON.parse(photosJson);
        } catch (e) {

        }
        photos = new Array();
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 == r2});
        //2.设置返回数据
        let neeAdd = that.checkAddVisible();
        that.state = {
            dataSource: ds.cloneWithRows(images),
            selectedPhoto: false,
            photoIndex: 0,
            needAddFooter: neeAdd,
            photoUpdate: false,
            saveAvailable: true,
            isFinishing: false
        }
    }

    static propTypes = {
        navigator: React.PropTypes.shape({}),
        finishPage: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        updateUserInfo: React.PropTypes.func,
        getUserInfo: React.PropTypes.func,
        leancloudNode: React.PropTypes.string
    }

    onFinishPage() {
        if (!this.state.isFinishing) {
            this.state.isFinishing = true;
            this.props.finishPage('global');
        }
        this.isFinishingTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }

    componentWillUnmount() {
        this.isFinishingTimer && clearTimeout(this.isFinishingTimer);
    }

    /**
     * 检测是否可以添加照片
     * @returns {boolean}
     */
    checkAddVisible() {
        let needFooter = false;
        if (this.props.userInfo != null) {
            let game = this.props.userInfo.game;
            /**
             * 3级以上做的上传九张图片,3级以下最多上传3张
             *
             */
            if (game != null) {
                if ((game.level >= 3 && images.length < 9) || (game.level < 3 && images.length < 3)) {
                    images.push('footer');
                    needFooter = true;
                }
            }
        }
        return needFooter;
    }

    render() {
        return (
            <Container style={{backgroundColor:Color.content_color}}>
                <CardItem
                    style={{backgroundColor:(Color.headColor),height:headHeight,paddingTop:headPaddingTop,paddingBottom:0}}>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={this.onFinishPage.bind(this)}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf:'center',flex:3}}>
                    <Text style={{color: '#fff',fontSize: 18,alignSelf:'center'}}>{Language().photo_wall}</Text>
                    </Body>
                    <Right style={{flex:1}}>
                        <TouchableOpacity
                            style={{justifyContent:'center'}}
                            onPress={this.onSavePhotos.bind(this)}>
                            <Text
                                style={{color:'#fff',fontSize:17,alignSelf:'center',padding:5}}>{Language().save}</Text>
                        </TouchableOpacity>
                    </Right>
                </CardItem>
                <Content>
                    <Text
                        style={{color:(Color.about_text_color),fontSize: 16,padding:20,alignSelf:'center',textAlign:'center'}}>{Language().prompt_edit_photo}</Text>
                    <View style={[styles.rowSeparator,{margin:20,marginTop:0}]}></View>
                    <ListView dataSource={this.state.dataSource}
                              renderRow={this.renderRow}
                              removeClippedSubviews={false}
                              contentContainerStyle={styles.listViewStyle}/>
                </Content>
                {Dialog.inject()}
                <Loading ref="progressDialog" loadingTitle={Language().save_loading}/>
                <Prompt ref='setAvatar' message='' title='' posText='' negText=''
                        contentStyle={{backgroundColor:(Color.transparent),width:ScreenWidth,paddingLeft: 15,paddingBottom: 0,paddingRight: 15}}
                        dialogStyle={{backgroundColor:'rgba(0, 0, 0, 0.25)',justifyContent: 'flex-end'}}>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8}}>
                        <Text
                            style={{alignSelf:'center',padding:15,color:(Color.loginTextColor),fontSize:18}}>{Language().title_upload_photo_type}</Text>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:10}} onPress={this.pickSingle.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().with_photo_album}</Text>
                        </TouchableOpacity>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:10}} onPress={this.pickSingleWithCamera.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().with_camera}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8,marginTop:5}}>
                        <TouchableOpacity style={{padding:10}} onPress={this.onDismissDialog.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().cancel}</Text>
                        </TouchableOpacity>
                    </View>
                </Prompt>
                <Prompt ref='editPhoto' message='' title='' posText='' negText=''
                        contentStyle={{backgroundColor:(Color.transparent),width:ScreenWidth,paddingLeft: 15,paddingBottom: 0,paddingRight: 15}}
                        dialogStyle={{backgroundColor:'rgba(0, 0, 0, 0.25)',justifyContent: 'flex-end'}}>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8}}>
                        <Text
                            style={{alignSelf:'center',padding:15,color:(Color.loginTextColor),fontSize:18}}>{Language().title_edit_photo}</Text>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:10}} onPress={this.onDeletePhoto.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().delete}</Text>
                        </TouchableOpacity>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:10}} onPress={this.pickSingle.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().with_photo_album}</Text>
                        </TouchableOpacity>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:10}} onPress={this.pickSingleWithCamera.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().with_camera}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8,marginTop:5}}>
                        <TouchableOpacity style={{padding:10}} onPress={this.onDismissDialog.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().cancel}</Text>
                        </TouchableOpacity>
                    </View>
                </Prompt>
            </Container>
        );
    }

    /**
     *
     * @param data
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    renderRow(data, sectionId, rowId) {
        let selectView = (that.state.selectedPhoto && parseInt(rowId) == that.state.photoIndex) ? (
                <Image source={bgSelect} style={styles.selectImage}></Image>) : (null);
        let itemView = ((parseInt(rowId) == images.length - 1) && that.state.needAddFooter) ? (
                <TouchableOpacity style={{justifyContent:'center'}} onPress={()=>that._addPhoto()}>
                    <Image
                        source={bgAdd}
                        style={{width:140,height:140,alignSelf:'center' }}>
                    </Image>
                </TouchableOpacity>) : (
                <TouchableOpacity style={{justifyContent:'center'}} onPress={()=>that._editPhoto(rowId)}>
                    <CachedImage
                        source={(data.url==''||data.url==null)?(require('../../../resources/imgs/me_photo_default.png')):({uri:data.url})}
                        defaultSource={require('../../../resources/imgs/me_photo_default.png')}
                        style={{width:140,height:140,alignSelf:'center',justifyContent:'flex-end'}}
                        type='photo'>
                        {selectView}
                    </CachedImage>
                </TouchableOpacity> );
        return (
            <View style={styles.cellBackStyle}>
                {itemView}
            </View>
        );
    }

    /**
     * 删除照片，修改本地的数据，以及最总的数据
     */
    onDeletePhoto() {
        this.onDismissDialog();
        let newData = images;
        if (newData != null && newData.length > 0) {
            newData.splice(this.state.photoIndex, 1);
            //图片全部显示时
            if (that.state.needAddFooter) {
                images.splice(images.length - 1, 1);
            }
            let needAdd = that.checkAddVisible();
            that.setState({
                photoUpdate: true,
                selectedPhoto: false,
                needAddFooter: needAdd,
                photoIndex: 0,
                dataSource: that.state.dataSource.cloneWithRows(newData),
            });
        }
    }

    /**
     * 添加图片
     * @private
     */
    _addPhoto() {
        let newData = images;
        that.setState({
            selectedPhoto: false,
            photoIndex: 0,
            dataSource: that.state.dataSource.cloneWithRows(newData),
        });
        that.onAddPhoto();
    }

    /**
     * 编辑选中的图片
     * @private
     */
    _editPhoto(rowId) {
        let newData = images;
        that.setState({
            selectedPhoto: true,
            photoIndex: parseInt(rowId),
            dataSource: that.state.dataSource.cloneWithRows(newData),
        });
        that.onEditPhoto();
    }

    /**
     * 关闭设置头像页面
     */
    onDismissDialog() {
        this.refs.setAvatar.hide();
        this.refs.editPhoto.hide();
    }

    /**
     * 设置照片墙
     */
    onAddPhoto() {
        this.refs.setAvatar.show();
    }

    /**
     * 编辑照片
     */
    onEditPhoto() {
        this.refs.editPhoto.show();
    }

    /**
     * 操作选择的照片
     * 替换或是从新添加
     * @param image
     */
    operationSelectPhoto(image) {
        if (image != null) {
            let newImage = image;
            ImageResizer.createResizedImage(image.path, image.height, image.width, 'JPEG', 60)
                .then((resizedImageUri) => {
                    newImage.path = resizedImageUri;
                    that.setPhoto(newImage);
                    console.log('resizer success ! ! !')
                }).catch((err) => {
                that.setPhoto(newImage);
                console.log('resizer error ! ! !')
            });
        }
    }

    /**
     * 在页面照片墙更新
     * @param newImage
     */
    setPhoto(image) {
        let newImage = {
            url: image.path,
            size: image.size,
            height: image.height,
            width: image.width
        };
        if (that.state.selectedPhoto) {
            images.splice(that.state.photoIndex, 1, newImage);
            if (that.state.needAddFooter) {
                images.splice(images.length - 1, 1);
            }
            let needAdd = that.checkAddVisible();
            that.setState({
                dataSource: that.state.dataSource.cloneWithRows(images),
                needAddFooter: needAdd,
            });
            //编辑照片
        } else {
            //添加照片
            images.splice(images.length - 1, 1, newImage);
            let needAdd = that.checkAddVisible();
            that.setState({
                dataSource: that.state.dataSource.cloneWithRows(images),
                needAddFooter: needAdd,
            });
        }
    }

    /**
     * 选取相册照片，单张
     */
    pickSingle() {
        this.onDismissDialog();
        this.pickPhotoTimer = setTimeout(() => {
            ImagePicker.openPicker({
                cropping: false,
                mediaType: 'photo',
            }).then(image => {
                this.operationSelectPhoto(image);
            }).catch(e => {
                console.log(e);
            });
        }, 200)
    }

    /**
     * 拍照选取
     */
    pickSingleWithCamera() {
        this.onDismissDialog();
        this.pickCameraTimer = setTimeout(() => {
            ImagePicker.openCamera({
                cropping: false,
                mediaType: 'photo',
            }).then(image => {
                this.operationSelectPhoto(image);
            }).catch(e => {
                console.log(e);
            });
        }, 200)
    }

    /**
     * 上传图片
     * @param image
     */
    photoFileUpload() {
        try {
            photos.splice(0, photos.length);
            let uploadImages = new Array();
            for (let i = 0; i < images.length; i++) {
                let image = images[i];
                if (image && image.url) {
                    if (image.size) {
                        //说明是修改的
                        that.state.photoUpdate = true;
                        UtilsTool.filePathFormat(UtilsTool.fileHeader1,image.url,(newPath)=>{
                            uploadImages.push(newPath);
                        });
                    } else {
                        //未修改的
                        photos.push(image);
                    }
                }
            }
            if (uploadImages.length > 0) {
                //需要上传
                rnRoNativeUtils.uploadFile(uploadImages, Constant.ACTION_UP_LOAD_PHOTOS,15000);
            } else if (that.state.photoUpdate) {
               //只是删除了原数据
                that.updateFinalPhotos(photos);
            } else {
                //未修改
                that.onHideProgressDialog();
                that.onFinishPage();
            }
        }
        catch (e) {
            alert(e)
            that.onHideProgressDialog();
            Toast.show(Language().upload_photo_fail);
        }

    }

    /**
     * 请求服务器上传照片墙
     * @param uploadImages
     */
    updateFinalPhotos(photos) {
        try {
            let url = apiDefines.UPDATE_PHOTOS;
            let dataPhotos = {
                photos: photos
            }
            console.log('upload photo wall list:' + JSON.stringify(dataPhotos));
            Util.post(url, dataPhotos, (code, message, data) => {
                this.onHideProgressDialog();
                if (code == 1000) {
                    let newUserInfo = this.props.userInfo;
                    if (newUserInfo != null && newUserInfo.id != '') {
                        this.props.getUserInfo(newUserInfo.id);
                    }
                    Toast.show(Language().upload_photo_success);
                    that.onFinishPage();
                } else {
                    console.log('error:' + code);
                    Toast.show(Language().upload_photo_fail);
                }
            }, (error) => {
                console.log('error:' + JSON.stringify(error));
                this.onHideProgressDialog();
                Toast.show(Language().not_network);
            });
        } catch (e) {
            Toast.show(Language().upload_photo_fail);
            this.onHideProgressDialog();
            console.log('error:' + e);
        }
    }

    /**
     * 保存修改
     */
    onSavePhotos() {
        try {
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    if (that.state.saveAvailable) {
                        that.onShowProgressDialog();
                        that.photoFileUpload();
                        that.state.saveAvailable = false;
                        this.saveBtTimer = setTimeout(() => {
                            that.state.saveAvailable = true;
                        }, 3000);
                    }

                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        } catch (e) {
            console.log('error:' + e);
        }
    }

    componentDidMount() {
        //页面渲染完毕后定义加载view
        progressDialog = this.refs.progressDialog;
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        if (Platform.OS === 'android') {
            this.onNativeMessageLister = DeviceEventEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
        } else {
            this.onNativeMessageLister = rNMessageSenderEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
        }
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.pickCameraTimer && clearTimeout(this.pickCameraTimer);
        this.pickPhotoTimer && clearTimeout(this.pickPhotoTimer);
        this.saveBtTimer && clearTimeout(this.saveBtTimer);
        this.onNativeMessageLister.remove();
    }

    handleMethod(isConnected) {

    }

    /**
     * 统一处理native的消息
     * @param event
     */
    nativeMessage(event) {
        if (event && event.Data) {
            let data = JSON.parse(event.Data);
            switch (data.action) {
                case Constant.ACTION_UP_LOAD_PHOTOS:
                    that.state.isUploadLoading = false;
                    if (data.params) {
                        let code = data.params.code;
                        let images = data.params.data;
                        if (code == 0) {
                            //上传成功
                            if (images) {
                                for (let i = 0; images[i]; i += 1) {
                                    let image = {
                                        id: images[i].key,
                                        url: images[i].url
                                    }
                                    photos.push(image);
                                }
                                that.updateFinalPhotos(photos);
                            } else {
                                that.onHideProgressDialog();
                                Toast.show(Language().upload_photo_fail);
                            }
                        } else {
                            //上传失败
                            that.onHideProgressDialog();
                            Toast.show(data.params.message)
                        }
                    } else {
                        that.onHideProgressDialog();
                        Toast.show(Language().upload_photo_fail);
                    }
                    break;
            }
        }
    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     * 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }
}
const styles = StyleSheet.create({
    selectImage: {
        width: 22,
        height: 22,
        alignSelf: 'flex-end',
        margin: 5,
    },
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    textStyle: {
        fontSize: 16,
        color: (Color.editUserTextColor),
        padding: 5
    }, cellBackStyle: {
        width: cellWH,
        height: cellWH,
        marginLeft: vMargin,
        marginTop: hMargin,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    listViewStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: 'lightgray',
    },

});
const mapDispatchToProps = dispatch => ({
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        finishPage: (key) => dispatch(popRoute(key)),
        updateUserInfo: (data) => dispatch((userInfo(data))),
        getUserInfo: (id) => dispatch((getUserInfo(id))),
    }
)
const mapStateToProps = state => ({
    userInfo: state.userInfoReducer.data,
    leancloudNode: state.configReducer.leanCloud_node
});
export default connect(mapStateToProps, mapDispatchToProps)(PhotoWall);