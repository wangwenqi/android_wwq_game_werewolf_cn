/**
 * Created by wangxu on 2017/12/5.
 * 语音各种房间
 */
import React, {Component} from 'react';
import Language from '../../../resources/language/index'
import {
    StyleSheet,
    Image,
    Dimensions,
    View,
    Platform,
    NativeModules,
    ListView,
    RefreshControl,
    ActivityIndicator,
    InteractionManager,
    TouchableOpacity,
    Text,
    ScrollView,
    Modal, Switch, TextInput, DeviceEventEmitter,
    NativeEventEmitter,
} from 'react-native';
import {checkOpenRegister} from '../../../support/actions/checkRegisterActions';
import {Input, Spinner} from 'native-base';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import Toast from '../../../support/common/Toast';
import Color from '../../../resources/themColor/index';
import LoadMoreFooter from '../../../support/common/LoadMoreFooter'
import netWorkTool from '../../../support/common/netWorkTool';
import Loading from '../../../support/common/Loading';
import ImageManager from '../../../resources/imageManager/index';
import InternationSourceManager from '../../../resources/internationSourceManager/index';
import * as types from '../../../support/actions/actionTypes';
import {updateFilter, fetchRecreationPageListFromType} from '../../../support/actions/recreationActions';
import rnToNativeUtils from '../../../support/common/rnToNativeUtils';
import Util from '../../../support/common/utils';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import  * as Constant from '../../../support/common/constant';
import CachedImage from '../../../support/common/CachedImage';
import LoadImageView from '../../../support/common/LoadImageView';
const LoadImage = LoadImageView;
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const activeOpacityValue = (Platform.OS === 'ios') ? 0.6 : 0.6;
const disableLiftSwipe = (Platform.OS === 'ios') ? true : true;
const roomUserNameFonSize = (Platform.OS === 'ios') ? 12 : 10;
const roomUserNamePadding = (Platform.OS === 'ios') ? 1 : 0;
//image
const bgVoiceHome = require('../../../resources/imgs/icon_voice_room.png');
const bgVoiceSearch = require('../../../resources/imgs/icon_voice_search.png');
const voiceHomeIcon = require('../../../resources/imgs/ic_room_icon.png');
const voiceUserIcon = require('../../../resources/imgs/ic_user_icon.png');
const icDefaultUser = require('../../../resources/imgs/ic_default_voice_user.png');
const defaultRecreationBg = require('../../../resources/imgs/icon_vioce_deault_bg.png');
const voiceItemBg = require('../../../resources/imgs/voice_item_bg.png');
const selectAudioIcon = require('../../../resources/imgs/icon_selected_audio.png');
const normalAudioIcon = require('../../../resources/imgs/icon_normal_audio.png');
const audioLike = require('../../../resources/imgs/icon_audio_like.png');
const audioPop = require('../../../resources/imgs/icon_audio_pop.png');
const audioCreateHeader = require('../../../resources/imgs/ic_default_audio_create.png');
const defaultNoHeader = require('../../../resources/imgs/ic_audio_no_user.png');
const recreationTitleBg = require('../../../resources/imgs/icon_recreation_title.png');
const {
    pushRoute,
    popRoute
} = actions;
const {RNMessageSender}=NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
const recreationLimit = 5;
const ownedType = 'purchased';
let recreationRoomList = new Array();
let audioTypes = new Array();
let that;
let cols = 3;
let cellH = 30;
let cellW = (ScreenWidth - 130) / 3;
let vMargin = 5;
let hMargin = 5;
let audioType = '';
class VoiceRoom extends Component {
    static propTypes = {
        userInfo: React.PropTypes.object,
        fetchRecreationPageListFromType: React.PropTypes.func,
        hostLocation: React.PropTypes.string,
        configData: React.PropTypes.object,
        updateConfig: React.PropTypes.func,
        updateFilter: React.PropTypes.func,
        voiceChildType: React.PropTypes.string,
        isRefreshingSing: React.PropTypes.bool,
        isNoMoreSing: React.PropTypes.bool,
        dataSing: React.PropTypes.object,
        actionTypeSing: React.PropTypes.string,
        isRefreshingChat: React.PropTypes.bool,
        actionTypeChat: React.PropTypes.string,
        isNoMoreChat: React.PropTypes.bool,
        dataChat: React.PropTypes.object,
        isRefreshingLove: React.PropTypes.bool,
        actionTypeLove: React.PropTypes.string,
        isNoMoreLove: React.PropTypes.bool,
        dataLove: React.PropTypes.object,
        isRefreshingMerry: React.PropTypes.bool,
        actionTypeMerry: React.PropTypes.string,
        isNoMoreMerry: React.PropTypes.bool,
        dataMerry: React.PropTypes.object,
        isRefreshingSeek: React.PropTypes.bool,
        actionTypeSeek: React.PropTypes.string,
        isNoMoreSeek: React.PropTypes.bool,
        dataSeek: React.PropTypes.object,
        isRefreshingGame: React.PropTypes.bool,
        isNoMoreGame: React.PropTypes.bool,
        actionTypeGame: React.PropTypes.string,
        dataGame: React.PropTypes.object,
        isRefreshingUndercover: React.PropTypes.bool,
        isNoMoreUndercover: React.PropTypes.bool,
        actionTypeUndercover: React.PropTypes.string,
        dataUndercover: React.PropTypes.object,
        isRefreshingFate: React.PropTypes.bool,
        isNoMoreFate: React.PropTypes.bool,
        isTourist: React.PropTypes.bool,
        actionTypeFate: React.PropTypes.string,
        dataFate: React.PropTypes.object,
        isRefreshingAll: React.PropTypes.bool,
        isNoMoreAll: React.PropTypes.bool,
        actionTypeAll: React.PropTypes.string,
        dataAll: React.PropTypes.object,
        voiceType: React.PropTypes.string,
    }
    static defaultProps = {
        voiceChildType: Constant.VOICE_TYPE_ALL
    }

    constructor(props) {
        super(props);
        this.params = this.params || {...this.props};
        that = this;
        if (this.props.configData) {
            if (this.props.configData.audio_room_types) {
                audioTypes = this.props.configData.audio_room_types;
            }
        }
        if (audioTypes[0]) {
            if (audioTypes[0].k) {
                audioType = audioTypes[0].k;
            }
        }
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        //2.设置返回数据
        this.state = {
            dataSource: ds.cloneWithRows(recreationRoomList),
            audioDataSource: ds.cloneWithRows(audioTypes),
            dialogTitle: Language().room_theme,
            showVoiceModalDialog: false,
            showModelProgress: false,
            modalDialogIndex: 0,
            height: 0,
            roomThemeContent: '',
            roomPassword: '',
            roomID: '',
            dialogCancel: Language().cancel,
            dialogConfirm: Language().confirm,
            needPassword: false,
            switchIsOn: false,
            roomDetailData: null,
            password1: '',
            password2: '',
            password3: '',
            password4: '',
            audioTypeIndex: 0,
            audioTypeValue: audioType,
            filterFlag: 0,//0为：user;1为：like；2为：pop,
            isConnectedNetWork: true,
            sampleRecreationRow: 0
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        const {
            voiceChildType
        } = this.params;
        let localData = this.props.dataAll;
        switch (voiceChildType) {
            case Constant.VOICE_TYPE_LOVE:
                localData = this.props.dataLove;
                break;
            case Constant.VOICE_TYPE_CHAT:
                localData = this.props.dataChat;
                break;
            case Constant.VOICE_TYPE_SEEK:
                localData = this.props.dataSeek;
                break;
            case Constant.VOICE_TYPE_FATE:
                localData = this.props.dataFate;
                break;
            case Constant.VOICE_TYPE_MERRY:
                localData = this.props.dataMerry;
                break;
            case Constant.VOICE_TYPE_GAME:
                localData = this.props.dataGame;
                break;
            case Constant.VOICE_TYPE_UNDERCOVER:
                localData = this.props.dataUndercover;
                break;
            case Constant.VOICE_TYPE_SING:
                localData = this.props.dataSing;
                break;
        }
        return (!_.isEqual(localData, nextProps.data));
    }

    componentDidMount() {
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.subscription = DeviceEventEmitter.addListener(Constant.SCROLL_VOICE_RECREATION_LIST, this.scrollRecreationList.bind(this));
        if (Platform.OS === 'android') {
            this.onNativeMessageLister = DeviceEventEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
        } else {
            this.onNativeMessageLister = rNMessageSenderEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
        }
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.onNativeMessageLister.remove();
        this.subscription.remove();
        this.hideProgressTimer && clearTimeout(this.hideProgressTimer);
    }

    componentWillMount() {
    }

    handleMethod(isConnected) {
        that.state.isConnectedNetWork = isConnected;
    }

    /**
     * 渲染页面
     * @returns {XML}
     */
    render() {
        this.formatDataList();
        let requestFlag = this.getRequestFlag();
        if (this.props.configData) {
            if (this.props.configData.audio_room_types) {
                audioTypes = this.props.configData.audio_room_types;
            }
        }
        if (this.state.showVoiceModalDialog) {
            requestFlag.isRefreshing = false;
        }
        return (
            <View
                style={{backgroundColor:apiDefines.APP_KEY == apiDefines.WEREWOLF ? Color.voice_room_bg_color : Color.voice_room_list_bg_color,flex:1}}>
                {this._defaultView()}
                <ListView dataSource={this.state.dataSource.cloneWithRows(recreationRoomList)}
                          ref="listView"
                          renderRow={this.renderRow.bind(this)}
                          renderFooter={this.renderFooter.bind(this)}
                          initialListSize={1}
                          scrollRenderAheadDistance={50}
                          removeClippedSubviews={true}
                          pageSize={recreationLimit}
                          onScroll={this._onScroll}
                          onEndReached={this._onEndReach.bind(this)}
                          onEndReachedThreshold={10}
                          enableEmptySections={true}
                          refreshControl={
                                     <RefreshControl
                                        refreshing={requestFlag.isRefreshing}
                                         onRefresh={this._onRefresh.bind(this)}
                                         colors={[Color.list_loading_color]} />
                                    }
                          closeOnRowBeginSwipe={true}/>
                <Loading ref="progressDialog" loadingTitle={Language().operation_loading}
                         contentStyle={{justifyContent:'flex-end'}} container={{backgroundColor:'rgba(0, 0, 0, 0.1)'}}/>
                {/*创建房间以及搜索房间的弹框*/}
                <Modal visible={this.state.showVoiceModalDialog} transparent={true} animationType='fade'>
                    <View flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                          style={[styles.dialogContainer]}>

                        <ScrollView alwaysBounceVertical={false}>
                            <View
                                style={{borderRadius:8,width:ScreenWidth-30,maxHeight:ScreenHeight-80,alignSelf:'center',backgroundColor:'#e3dce9',padding:0}}>
                                <View flexDirection='row' style={{justifyContent:'center',padding:5}}>
                                    <Text
                                        style={{color:Color.voice_set_text_color,alignSelf:'center',fontSize:17,textAlign:'center'}}>{this.state.dialogTitle}</Text>
                                </View>
                                {this._modelDialogContentView()}
                                <View flexDirection='row' style={{justifyContent:'center',margin:10}}>
                                    <TouchableOpacity activeOpacity={0.9}
                                                      onPress={this.onCloseModal.bind(this)}
                                                      style={[styles.actionButton,{backgroundColor:Color.colorWhite}]}>
                                        <Text
                                            style={{alignSelf:'center',color:'#845fe4'}}>{this.state.dialogCancel}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={0.8} onPress={this.onConfirmModal.bind(this)}
                                                      style={styles.actionButton}>
                                        <Text
                                            style={{alignSelf:'center',color:Color.colorWhite}}>{this.state.dialogConfirm}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.showModelProgress ? (
                                    <View style={[styles.loading,{left:ScreenWidth/2-40,top:(ScreenHeight/4)-20}]}>
                                        <Spinner size='small' style={{height:35}}/>
                                        <Text
                                            style={{marginTop:2,fontSize: 12,color:Color.colorWhite}}>{Language().request_loading}</Text>
                                    </View>) : null}
                        </ScrollView>
                    </View>
                </Modal>
            </View>
        );
    }

    /**
     * 数据集合处理
     */
    formatDataList() {
        const {
            voiceChildType
        } = this.params;
        let localData = this.props.dataAll;
        switch (voiceChildType) {
            case Constant.VOICE_TYPE_ALL:
                if (this.props.actionTypeAll == types.RECREATION_ALL_DATA || this.props.actionTypeAll == types.RECREATION_ALL_DATA_ERROR) {
                    this.onHideProgressDialog();
                }
                if (this.props.actionTypeAll == types.RECREATION_ALL_DATA) {
                    this.state.sampleRecreationRow = 0;
                }
                localData = this.props.dataAll;
                break;
            case Constant.VOICE_TYPE_SING:
                if (this.props.actionTypeSing == types.RECREATION_SING_DATA || this.props.actionTypeSing == types.RECREATION_SING_DATA_ERROR) {
                    this.onHideProgressDialog();
                }
                if (this.props.actionTypeSing == types.RECREATION_SING_DATA) {
                    this.state.sampleRecreationRow = 0;
                }
                localData = this.props.dataSing;
                break;
            case Constant.VOICE_TYPE_LOVE:
                localData = this.props.dataLove;
                if (this.props.actionTypeLove == types.RECREATION_LOVE_DATA || this.props.actionTypeLove == types.RECREATION_LOVE_DATA_ERROR) {
                    this.onHideProgressDialog();
                }
                if (this.props.actionTypeLove == types.RECREATION_LOVE_DATA) {
                    this.state.sampleRecreationRow = 0;
                }
                break;
            case Constant.VOICE_TYPE_CHAT:
                localData = this.props.dataChat;
                if (this.props.actionTypeChat == types.RECREATION_CHAT_DATA || this.props.actionTypeChat == types.RECREATION_CHAT_DATA_ERROR) {
                    this.onHideProgressDialog();
                }
                if (this.props.actionTypeChat == types.RECREATION_CHAT_DATA) {
                    this.state.sampleRecreationRow = 0;
                }
                break;
            case Constant.VOICE_TYPE_SEEK:
                localData = this.props.dataSeek;
                if (this.props.actionTypeSeek == types.RECREATION_SEEK_DATA || this.props.actionTypeSeek == types.RECREATION_SEEK_DATA_ERROR) {
                    this.onHideProgressDialog();
                }
                if (this.props.actionTypeSeek == types.RECREATION_SEEK_DATA) {
                    this.state.sampleRecreationRow = 0;
                }
                break;
            case Constant.VOICE_TYPE_FATE:
                localData = this.props.dataFate;
                if (this.props.actionTypeFate == types.RECREATION_FATE_DATA || this.props.actionTypeFate == types.RECREATION_FATE_DATA_ERROR) {
                    this.onHideProgressDialog();
                }
                if (this.props.actionTypeFate == types.RECREATION_FATE_DATA) {
                    this.state.sampleRecreationRow = 0;
                }
                break;
            case Constant.VOICE_TYPE_MERRY:
                localData = this.props.dataMerry;
                if (this.props.actionTypeMerry == types.RECREATION_MERRY_DATA || this.props.actionTypeMerry == types.RECREATION_MERRY_DATA_ERROR) {
                    this.onHideProgressDialog();
                }
                if (this.props.actionTypeMerry == types.RECREATION_MERRY_DATA) {
                    this.state.sampleRecreationRow = 0;
                }
                break;
            case Constant.VOICE_TYPE_GAME:
                localData = this.props.dataGame;
                if (this.props.actionTypeGame == types.RECREATION_GAME_DATA || this.props.actionTypeGame == types.RECREATION_GAME_DATA_ERROR) {
                    this.onHideProgressDialog();
                }
                if (this.props.actionTypeGame == types.RECREATION_GAME_DATA) {
                    this.state.sampleRecreationRow = 0;
                }
                break;
            case Constant.VOICE_TYPE_UNDERCOVER:
                localData = this.props.dataUndercover;
                if (this.props.actionTypeUndercover == types.RECREATION_UNDERCOVER_DATA || this.props.actionTypeUndercover == types.RECREATION_UNDERCOVER_DATA_ERROR) {
                    this.onHideProgressDialog();
                }
                if (this.props.actionTypeUndercover == types.RECREATION_UNDERCOVER_DATA) {
                    this.state.sampleRecreationRow = 0;
                }
                break;
        }
        if (localData) {
            recreationRoomList = localData.rooms;
        } else {
            recreationRoomList = new Array();
        }
    }

    /**
     *通知滑动到最顶端
     */
    scrollRecreationList(event) {
        if (event) {
            let requestFlag = this.getRequestFlag()
            if (event.type == requestFlag.type) {
                netWorkTool.checkNetworkState((isConnected) => {
                    if (!isConnected) {
                        Toast.show(Language().load_more_data_error);
                    } else {
                        this.onShowProgressDialog(Language().loading_now);
                        this.state.sampleRecreationRow = 0;
                        InteractionManager.runAfterInteractions(() => {
                            this.props.fetchRecreationPageListFromType(1, event.actionType, event.type);
                        });
                        this.doScrollToList();
                    }
                });
            }
        }
    }

    /**
     *1-- 展示加载进度提示
     */
    onShowProgressDialog() {
        if (this.refs.progressDialog != null) {
            this.refs.progressDialog.show();
        }
    }

    /**
     *
     * @param message
     */
    onShowProgressDialog(message) {
        if (this.refs.progressDialog != null) {
            this.refs.progressDialog.show({loadingTitle: message});
        }
    }

    /**
     *1-- 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (this.refs.progressDialog != null) {
            this.refs.progressDialog.hide();
        }
    }

    onConfirmModal() {
        if (!this.state.showModelProgress) {
            let hostLocation = '';
            if (this.props.hostLocation) {
                hostLocation = this.props.hostLocation;
            }
            switch (this.state.modalDialogIndex) {
                case 0:
                    //创建房间
                    let url = apiDefines.CREATE_AUDIO_ROOM;
                    let {roomThemeContent, roomPassword} = this.state;
                    if (!roomThemeContent.length || roomThemeContent.length < 4) {
                        Toast.show(Language().voice_theme_limit_message)
                        return;
                    }
                    if (this.state.switchIsOn && !roomPassword.length) {
                        Toast.show(Language().placeholder_password)
                        return;
                    }
                    let body = {
                        type: 'audio',
                        title: roomThemeContent,
                        password: roomPassword,
                        lc: hostLocation,
                        child_type: this.state.audioTypeValue
                    }
                    this.showProgress();
                    this.createRequest(false, url, body);
                    break;
                case 1:
                    //搜索房间
                    if (!this.state.roomPassword) {
                        Toast.show(Language().placeholder_password)
                        return;
                    }
                    let data = {
                        roomId: this.state.roomID,
                        roomPassword: this.state.roomPassword
                    }
                    this.doEnterVoiceRoom(data);
                    break;
                case 2:
                    //直接进入
                    let newData = {
                        roomId: this.state.roomID,
                        roomPassword: this.state.roomPassword
                    }
                    this.doEnterVoiceRoom(newData);
                    break;
            }
        }
    }

    onCloseModal() {
        this.setState({
            showVoiceModalDialog: false,
            showModelProgress: false,
            modalDialogIndex: 0,
            height: 0,
            roomThemeContent: '',
            roomPassword: '',
            roomID: '',
            needPassword: false,
            switchIsOn: false,
            roomDetailData: null,
            password1: '',
            password2: '',
            password3: '',
            password4: '',
            audioTypeIndex: 0,
            audioTypeValue: audioType
        });
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    renderRow(item, sectionId, rowId) {
        let filterSameData = this.filterSameData(item.room_id, rowId);
        let paddingRightRowTitle = 0;
        if (filterSameData) {
            this.state.sampleRecreationRow++;
            return null;
        }
        let users = item.users;
        let title = '';
        if (item.title) {
            title = item.title;
            title = title.replace(/[\r\n]/g, "");
        } else {
            title = item.room_id;
        }
        let userCount = 0;
        if (users) {
            userCount = users.length;
        }
        if (item.userCount) {
            userCount = item.userCount;
        }
        let audioType = '';
        if (item.child_type) {
            audioType = item.child_type;
        }
        let user_1 = this._getUser(users, 0);
        let user_2 = this._getUser(users, 1);
        let user_3 = this._getUser(users, 2);
        let user_4 = this._getUser(users, 3);
        let room_lock_icon = item.needPassword ? InternationSourceManager().icRoomIconLock : null;
        if (item.child_type == Constant.VOICE_TYPE_UNDERCOVER) {
            if (item.needPassword) {
                paddingRightRowTitle = 18;
            } else {
                paddingRightRowTitle = 70;
            }
        }
        let audioData = this.getAudioTypeData(item.child_type);
        return (<TouchableOpacity activeOpacity={0.6} onPress={()=>{that.enterVoiceRoom(item)}}
                                  style={{backgroundColor:Color.transparent,justifyContent:'center'}}>
                <View flexDirection='row' style={{marginLeft:10,marginRight:10,padding:5,overflow:'hidden'}}>
                    <View flexDirection='row'
                          style={{borderRadius:5,borderColor:apiDefines.APP_KEY == apiDefines.WEREWOLF ? Color.voice_room_bg_color : Color.voice_room_list_bg_color,borderWidth:2,overflow:'hidden'}}>
                        <CachedImage style={styles.image} source={voiceItemBg}>
                            <View flexDirection='row'>
                                <View style={{borderRadius:3,justifyContent:'center'}}>
                                    <LoadImage style={styles.ownerHeader}
                                               type='audio_header'
                                               source={(item.image)?({uri:item.image}):audioCreateHeader}
                                               defaultSource={audioCreateHeader}/>
                                    <View style={{position: "absolute", top: 5,left:3}}>
                                        <View flexDirection='row'>
                                            <CachedImage style={styles.voiceHomeIcon} source={voiceHomeIcon}/>
                                            <View flexDirection='row'
                                                  style={{backgroundColor:'#ff009c',borderRadius:8,paddingLeft:5,paddingRight:5,height:12,alignSelf:'center'}}>
                                                <Text numberOfLines={1}
                                                      style={styles.rowStatusText}>{item.room_id}</Text>
                                            </View>
                                        </View>
                                        <View flexDirection='row' style={{marginTop:3}}>
                                            <CachedImage style={styles.voiceHomeIcon} source={voiceUserIcon}/>
                                            <View
                                                style={{backgroundColor:'#ffba13',borderRadius:8,paddingLeft:5,paddingRight:5,height:12,alignSelf:'center'}}>
                                                <Text numberOfLines={1}
                                                      style={[styles.rowStatusText,{color:Color.blackColor}]}>{userCount}人</Text>
                                            </View>
                                        </View>
                                    </View>
                                    {this.userNameView(item.name)}
                                    {this.audioTypeImageView(audioData)}
                                </View>
                                <View style={{flex:1}}>
                                    <View flexDirection='row' style={{height:37}}>
                                        <Text numberOfLines={1}
                                              style={[styles.rowTitle,{marginRight:paddingRightRowTitle}]}>{title}</Text>
                                        <CachedImage style={styles.voiceHomeLock} source={room_lock_icon}/>
                                        {this.voiceRoomStatusView(item.child_type, item.roomStatus)}
                                    </View>
                                    <View style={[styles.renderRow]}>
                                        <View flexDirection='row'
                                              style={{flex:4,justifyContent:'center',alignSelf:'center',paddingRight:10,paddingLeft:10}}>
                                            <View style={{flex:1}}>
                                                <LoadImage style={styles.userHeader}
                                                           type='audio'
                                                           source={(!user_1.avatar.length)?defaultNoHeader:({uri:user_1.avatar})}
                                                           defaultSource={icDefaultUser}/>
                                                <Text numberOfLines={1}
                                                      style={styles.rowUserName}>{user_1.name}</Text>
                                            </View>
                                            <View style={{flex:1}}>
                                                <LoadImage style={styles.userHeader}
                                                           type='audio'
                                                           source={(!user_2.avatar.length)?defaultNoHeader:({uri:user_2.avatar})}
                                                           defaultSource={icDefaultUser}/>
                                                <Text numberOfLines={1}
                                                      style={styles.rowUserName}>{user_2.name}</Text>
                                            </View>
                                            <View style={{flex:1}}>
                                                <LoadImage style={styles.userHeader}
                                                           type='audio'
                                                           source={(!user_3.avatar.length)?defaultNoHeader:({uri:user_3.avatar})}
                                                           defaultSource={icDefaultUser}/>
                                                <Text numberOfLines={1}
                                                      style={styles.rowUserName}>{user_3.name}</Text>
                                            </View>
                                            <View style={{flex:1}}>
                                                <LoadImage style={styles.userHeader}
                                                           type='audio'
                                                           source={(!user_4.avatar.length)?defaultNoHeader:({uri:user_4.avatar})}
                                                           defaultSource={icDefaultUser}/>
                                                <Text numberOfLines={1}
                                                      style={styles.rowUserName}>{user_4.name}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View flexDirection='row' style={{flex:1}}>
                                        <View flexDirection='row' style={{flex:1.3}}>
                                            {this.audioLikePopView(item.like, audioLike)}
                                            {this.audioLikePopView(item.pop, audioPop)}
                                        </View>
                                        <View style={{flex:1}}>
                                            {this.audioTypeTitleView(audioData)}
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </CachedImage>
                    </View>
                </View>
                <View style={styles.rowSeparator}></View>
            </TouchableOpacity>
        );
    }

    /**
     *
     * @param name
     */
    userNameView = (name) => {
        if (name) {
            return (   <View
                style={{position: "absolute",bottom:8,right:25,backgroundColor:'#bc06ad',borderRadius:2,left:15}}>
                <Text numberOfLines={1}
                      style={[styles.rowStatusText,{fontSize:roomUserNameFonSize,color:'#cabddd',padding:roomUserNamePadding,paddingLeft:5,paddingRight:8,backgroundColor:Color.transparent}]}>{name}</Text>
            </View>);
        } else {
            return null;
        }

    }
    /**
     *
     * @param childType
     * @param roomStatus
     * 谁是卧底房间显示'游戏中'／'准备中'
     */
    voiceRoomStatusView = (childType, roomStatus) => {
        let roomStatusView;
        if (childType == Constant.VOICE_TYPE_UNDERCOVER) {
            if (roomStatus) {
                let roomStatusText = Language().game_prepare;
                let roomStatusViewColor = '#008cff';
                if (roomStatus == 2) {
                    roomStatusViewColor = '#ff005b';
                    roomStatusText = Language().game_start;
                }
                roomStatusView = <View style={[styles.roomStatusViewStyle,{backgroundColor:roomStatusViewColor}]}>
                    <Text numberOfLines={1}
                          style={[styles.rowStatusText,{fontSize:9}]}>{roomStatusText}</Text>
                </View>;
            }

        }
        return roomStatusView;
    }

    /**
     * 过滤相同的数据
     * @param roomId
     */
    filterSameData(roomId, rowId) {
        try {
            for (let i = 0; i < rowId; i++) {
                let roomDate = recreationRoomList[i];
                if (roomDate && roomDate.room_id == roomId) {
                    return true;
                }
            }
            return false;
        } catch (e) {
            return false;
        }
    }

    /**
     * @param content
     * @param source
     * @returns {*}
     */
    audioLikePopView(content, source) {
        if (content) {
            if (content > 99999999) {
                content = 99999999 + '+';
            }
            return (<View flexDirection='row'>
                <CachedImage style={[styles.voiceHomeIcon,{height:14,width:14,marginLeft:5}]} source={source}/>
                <Text numberOfLines={1}
                      style={[styles.rowStatusText,{color:'#f30ce9',fontSize:11,backgroundColor:Color.transparent}]}>{content}</Text>
            </View>);
        } else {
            return null;
        }
    }

    audioTypeTitleView(data) {
        if (data && data.t) {
            return (<View style={{position: 'absolute',left:0,right:0,bottom:0,backgroundColor:Color.transparent}}>
                <View
                    style={{backgroundColor:'#FFFD38',paddingLeft:10,paddingRight:10,alignSelf:'center',borderTopLeftRadius:8,borderTopRightRadius:8,paddingBottom:3,paddingTop:3}}>
                    <Text numberOfLines={1}
                          style={[styles.rowStatusText,{color:'#4A1C98',fontSize:11,fontWeight:'bold'}]}>{data.t}</Text>
                </View>
            </View>);
        }
    }

    /**
     *
     * @param audioData
     * @returns {null}
     */
    audioTypeImageView(audioData) {
        if (audioData) {
            return (<CachedImage
                source={{uri:audioData.i}}
                style={{position: "absolute",bottom:5,right:5, resizeMode: 'contain',alignSelf: 'center',width:25,height:25}}/>)
        } else {
            return null;
        }
    }

    /**
     *
     * @returns {null}
     */
    getAudioTypeData(childType) {
        if (audioTypes) {
            for (let i = 0; i < audioTypes.length; i++) {
                if (audioTypes[i].k == childType) {
                    return audioTypes[i];
                }
            }
            return null;
        } else {
            return null;
        }
    }

    /**
     * 进入音频房间
     */
    enterVoiceRoom(roomData) {
        if (this.props.isTourist) {
            let event = {needShow: true, type: types.ACTION_RECREATION};
            DeviceEventEmitter.emit(Constant.ACTION_TOURIST_EVENT, event);
        } else {
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    /**
                     * 如果自己是房间持有者，有密码依然可以进入
                     */
                    let userInfo = this.props.userInfo;
                    if (roomData) {
                        let data = {
                            roomId: roomData.room_id,
                            roomPassword: ''
                        }
                        if (roomData.needPassword) {
                            if (userInfo && userInfo.id == roomData.owner_id && roomData.owner_type == ownedType) {
                                that.doEnterVoiceRoom(data);
                            } else {
                                that.setState({
                                    showVoiceModalDialog: true,
                                    dialogTitle: Language().room_number + roomData.room_id,
                                    modalDialogIndex: 2,
                                    roomID: roomData.room_id,
                                    roomDetailData: roomData
                                });
                            }
                        } else {
                            that.doEnterVoiceRoom(data);
                        }
                    } else {
                        Toast.show(Language().enter_room_error);
                    }
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        }
    }

    /**
     * 执行进入房间的操作
     */
    doEnterVoiceRoom(roomData) {
        let NativeData = {
            action: 'enterAudioRoom',
            params: roomData,
            options: null
        }
        if (this.state.showVoiceModalDialog) {
            this.onCloseModal();
        }
        rnToNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }

    /**
     *
     * @param users
     * @param index
     * @returns {{name: string, avatar: string}}
     * @private
     */
    _getUser(users, index) {
        let user = {
            name: '',
            avatar: '',
        }
        if (users) {
            if (index < users.length) {
                if (users[index].name) {
                    user.name = users[index].name;
                }
                if (users[index].avatar) {
                    user.avatar = users[index].avatar;
                }
            }
        }
        return user;
    }

    /**
     * 刷新
     * @private
     */
    _onRefresh = () => {
        netWorkTool.checkNetworkState((isConnected) => {
            if (!isConnected) {
                Toast.show(Language().load_more_data_error)
            }
            let requestFlag = this.getRequestFlag();
            requestFlag.isRefreshing = true;
            InteractionManager.runAfterInteractions(() => {
                //刷新
                this.state.sampleRecreationRow = 0;
                this.props.fetchRecreationPageListFromType(1, requestFlag.actionFresh, requestFlag.type);
            });

        });
    };
    /**
     * load more
     * @private
     */
    _onEndReach = () => {
        let requestFlag = this.getRequestFlag();
        let newData = requestFlag.data;
        if (newData) {
            this.state.sampleRecreationRow = 0;
            if (newData.page < newData.pages) {
                this.props.fetchRecreationPageListFromType(newData.page + 1, requestFlag.actionMore, requestFlag.type);
            } else if (newData.page == newData.pages) {
                this.props.fetchRecreationPageListFromType(newData.page, requestFlag.actionMore, requestFlag.type);
            }
        }
    }

    /**
     *
     * @returns {{data: Object, actionType, type: *}}
     */
    getRequestFlag() {
        const {
            voiceChildType
        } = this.params;
        let localData = this.props.dataAll;
        let requestMore = types.RECREATION_ALL_MORE_DATA;
        let requestFresh = types.RECREATION_ALL_DATA;
        let isRefreshing = this.props.isRefreshingAll;
        let isNoMore = this.props.isNoMoreAll;
        switch (voiceChildType) {
            case Constant.VOICE_TYPE_ALL:
                localData = this.props.dataAll;
                requestMore = types.RECREATION_ALL_MORE_DATA;
                requestFresh = types.RECREATION_ALL_DATA;
                isRefreshing = this.props.isRefreshingAll;
                isNoMore = this.props.isNoMoreAll;
                break;
            case Constant.VOICE_TYPE_SING:
                localData = this.props.dataSing;
                requestMore = types.RECREATION_SING_MORE_DATA;
                requestFresh = types.RECREATION_SING_DATA;
                isRefreshing = this.props.isRefreshingSing;
                isNoMore = this.props.isNoMoreSing;
                break;
            case Constant.VOICE_TYPE_LOVE:
                localData = this.props.dataLove;
                requestMore = types.RECREATION_LOVE_MORE_DATA;
                requestFresh = types.RECREATION_LOVE_DATA;
                isRefreshing = this.props.isRefreshingLove;
                isNoMore = this.props.isNoMoreLove;
                break;
            case Constant.VOICE_TYPE_CHAT:
                localData = this.props.dataChat;
                requestMore = types.RECREATION_CHAT_MORE_DATA;
                requestFresh = types.RECREATION_CHAT_DATA;
                isRefreshing = this.props.isRefreshingChat;
                isNoMore = this.props.isNoMoreChat;
                break;
            case Constant.VOICE_TYPE_SEEK:
                localData = this.props.dataSeek;
                requestMore = types.RECREATION_SEEK_MORE_DATA;
                requestFresh = types.RECREATION_SEEK_DATA;
                isRefreshing = this.props.isRefreshingSeek;
                isNoMore = this.props.isNoMoreSeek;
                break;
            case Constant.VOICE_TYPE_FATE:
                localData = this.props.dataFate;
                requestMore = types.RECREATION_FATE_MORE_DATA;
                requestFresh = types.RECREATION_FATE_DATA;
                isRefreshing = this.props.isRefreshingFate;
                isNoMore = this.props.isNoMoreFate;
                break;
            case Constant.VOICE_TYPE_MERRY:
                localData = this.props.dataMerry;
                requestMore = types.RECREATION_MERRY_MORE_DATA;
                requestFresh = types.RECREATION_MERRY_DATA;
                isRefreshing = this.props.isRefreshingMerry;
                isNoMore = this.props.isNoMoreMerry;
                break;
            case Constant.VOICE_TYPE_GAME:
                localData = this.props.dataGame;
                requestMore = types.RECREATION_GAME_MORE_DATA;
                requestFresh = types.RECREATION_GAME_DATA;
                isRefreshing = this.props.isRefreshingGame;
                isNoMore = this.props.isNoMoreGame;
                break;
            case Constant.VOICE_TYPE_UNDERCOVER:
                localData = this.props.dataUndercover;
                requestMore = types.RECREATION_UNDERCOVER_MORE_DATA;
                requestFresh = types.RECREATION_UNDERCOVER_DATA;
                isRefreshing = this.props.isRefreshingUndercover;
                isNoMore = this.props.isNoMoreUndercover;
                break;
        }
        let requestFlag = {
            data: localData,
            actionMore: requestMore,
            actionFresh: requestFresh,
            type: voiceChildType,
            isRefreshing: isRefreshing,
            isNoMore: isNoMore
        }
        return requestFlag;
    }

    /**
     * 加载更多
     * @private
     */
    renderFooter() {
        let requestFlag = this.getRequestFlag();
        //音频房间列表一页最多10条数据
        let allDataCount = recreationRoomList.length;
        let recreationData = requestFlag.data;
        if (recreationData) {
            if (recreationData.page == 1) {
                if (this.state.sampleRecreationRow < allDataCount) {
                    allDataCount = allDataCount - this.state.sampleRecreationRow;
                }
            }
        }
        const {
            voiceChildType
        } = this.params;
        if (voiceChildType == requestFlag.type) {
            if (this.state.isConnectedNetWork) {
                return allDataCount < recreationLimit ? null : (<LoadMoreFooter isNoMore={requestFlag.isNoMore}/>);
            } else {
                return (<Text style={styles.title}>{Language().load_more_data_error}</Text>);
            }
        }
    }


    /**
     * 列表固定头部
     */
    headerView() {
        return (<View>
            <View>
                <Image source={recreationTitleBg} style={styles.titleImage}/>
                <View flexDirection='row'
                      style={{justifyContent:'center',padding:8,marginTop:2,marginLeft:20,marginRight:20,marginBottom:0}}>
                    <TouchableOpacity onPress={()=>that._createVoiceRoom()}
                                      activeOpacity={activeOpacityValue}
                                      style={styles.voiceBt}>
                        <View flexDirection='row'
                              style={{backgroundColor:'#FF00E3',borderRadius:3,justifyContent:'center',height:34,paddingRight:20,paddingLeft:20}}>
                            <Image source={bgVoiceHome} style={styles.voiceHome}></Image>
                            <Text
                                style={[styles.voiceHomeText,{fontWeight:'bold',marginLeft:3,alignSelf:'center'}]}>{Language().create}</Text>
                        </View>
                    </TouchableOpacity>
                    <View flexDirection='row'
                          style={{backgroundColor:'#a582bc',borderRadius:3,borderColor:'#922faa',marginLeft:3,height:35,alignSelf:'center',padding:0,borderWidth:2,flex:1}}>
                        <TextInput
                            ref="roomIdInput"
                            underlineColorAndroid='transparent'
                            style={{fontSize: 16,color:Color.colorWhite,padding: 5,height:30,flex:1,alignSelf:'center'}}
                            placeholder={Language().search_room}
                            placeholderTextColor='#bb9edb'
                            onChangeText={(text)=>this._changeVoiceRoomId(text)} value={this.state.roomID}/>
                        <TouchableOpacity onPress={()=>that._searchVoiceRoom()}
                                          activeOpacity={activeOpacityValue}
                                          style={styles.voiceBt}>
                            <Image source={bgVoiceSearch} style={styles.voiceSearch}></Image>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
            <View style={[styles.rowSeparator,{marginBottom:2}]}></View>
        </View>);
    }

    /**
     *移动list view 到最顶端
     */
    doScrollToList() {
        if (this.refs.listView) {
            this.refs.listView.scrollTo(0, 0);
        }
    }

    /**
     *
     * @private
     * 内容可分为：
     * 1->创建音频房间
     * 2->搜索音频房间
     * 3->房间需要输入密码
     *
     */
    _modelDialogContentView() {
        switch (that.state.modalDialogIndex) {
            case 0:
                return this._modelCreateRoom();
            case 1:
                return this._modelSearchRoom();
            case 2:
                return this._modelInitPassword();
        }
    }

    passwordInputView() {
        return (<TextInput
            ref='passwordInput'
            style={{paddingLeft:0,color:(Color.transparent),fontSize:12,height:35,padding:5,paddingLeft:0,backgroundColor:Color.transparent,lineHeight:0,selectionColor:Color.transparent}}
            autoFocus={true} maxLength={4}
            selectionColor={Color.transparent}
            onChangeText={(text)=>this._changeChangePassword(text)}
            keyboardType='numeric'
            onSubmitEditing={()=>this.passwordEndEditing()}
            value={this.state.roomPassword}/>);
    }

    passwordEndEditing() {
        if (this.refs.passwordInput) {
            this.refs.passwordInput.blur();
        }
    }

    /**
     *
     * @returns {XML}
     * @private
     */
    _modelCreateRoom() {
        let setPassword = (<View
            style={{margin:3,marginTop:10}}>
            {this.passwordInputView()}
            {this.passwordView()}
        </View>);
        return (<View style={{margin:10,marginRight:20,marginLeft:20}}>
            {this.audioTypeView()}
            <Text
                style={{color:(Color.voice_set_text_color),fontSize:14}}>{Language().room_theme}</Text>
            <View
                style={{borderColor:'#a66ce9',borderRadius:3,borderWidth:1,marginRight:20,marginLeft:20,marginTop:10}}>
                <Input
                    multiline={true}
                    autoFocus={true}
                    style={{fontSize: 12,color: (Color.voice_set_text_color),padding: 5,height:Math.max(30,this.state.height),paddingRight:10,paddingLeft:10}}
                    placeholder={Language().placeholder_room_theme}
                    placeholderTextColor={Color.prompt_text_color}
                    maxLength={15}
                    onChangeText={(text)=>this._changeTitleTheme(text)} value={this.state.roomThemeContent}
                    onContentSizeChange={this.onContentSizeChange.bind(this)}/>
            </View>
            <Text
                style={{fontSize:10,paddingBottom:5,marginTop:10,marginRight:20,marginLeft:20,color:'#757575'}}>{Language().voice_limit_message}</Text>
            <View flexDirection='row' style={{marginTop:5}}>
                <Text
                    style={{color:(Color.voice_set_text_color),fontSize:14,marginRight:10,alignSelf:'center'}}>{Language().set_password}</Text>
                <Switch onValueChange={(value)=>this.setState({switchIsOn:value})}
                        value={this.state.switchIsOn}/>
            </View>
            {this.state.switchIsOn ? setPassword : null}
        </View>);
    }

    /**
     *
     * @returns {XML}
     */
    passwordView() {
        return (<View flexDirection='row' style={{position: "absolute"}}>
            <TouchableOpacity onPress={this.focusPassword.bind(this)}
                              activeOpacity={1}
                              style={{justifyContent:'center',backgroundColor:'#E1DDE8',flex:1}}>
                <View flexDirection='row'
                      style={{alignSelf:'center',justifyContent:'center',flex:1,backgroundColor:'#E1DDE8'}}>
                    <View
                        style={styles.passwordViewStyle}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password1}</Text>
                    </View>
                    <View
                        style={[styles.passwordViewStyle,{marginLeft:10}]}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password2}</Text>
                    </View>
                    <View
                        style={[styles.passwordViewStyle,{marginLeft:10}]}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password3}</Text>
                    </View>
                    <View
                        style={[styles.passwordViewStyle,{marginLeft:10}]}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password4}</Text>
                    </View>

                </View>

            </TouchableOpacity>
        </View>);
    }

    focusPassword() {
        if (this.refs.passwordInput) {
            this.refs.passwordInput.focus();
        }
    }

    /**
     * 搜索房间
     * @returns {XML}
     * @private
     */
    _modelSearchRoom() {
        let passwordView = (
            <View style={{borderColor:Color.action_room_color,borderRadius:3,borderWidth:1,marginBottom:5}}>
                <Input
                    style={{fontSize: 12,color: (Color.editUserTextColor),padding: 5,paddingRight:10,paddingLeft:10,height:30}}
                    placeholder={Language().placeholder_room_password}
                    placeholderTextColor={Color.prompt_text_color}
                    autoFocus={true} maxLength={4}
                    keyboardType='numeric'
                    onChangeText={(text)=>this._changeChangePassword(text)} value={this.state.roomPassword}/>
            </View>);
        return (<View style={{margin:10,marginRight:20,marginLeft:20}}>
            <View style={{borderColor:Color.action_room_color,borderRadius:3,borderWidth:1,marginBottom:5}}>
                <Input
                    style={{fontSize: 12,color: (Color.editUserTextColor),padding: 5,paddingRight:10,paddingLeft:10,height:30}}
                    placeholder={Language().placeholder_room_number}
                    placeholderTextColor={Color.prompt_text_color}
                    autoFocus={true}
                    onChangeText={(text)=>this._changeVoiceRoomId(text)} value={this.state.roomID}/>
            </View>
            {this.state.needPassword ? passwordView : null}
        </View>);
    }

    /**
     * 房间ID
     * @returns {XML}
     * @private
     */
    _modelInitPassword() {
        return (<View style={{marginTop:5}}>
            {this.roomDetailView(this.state.roomDetailData)}
            <View style={{marginTop:10}}>
                {this.passwordInputView()}
                {this.passwordView()}
            </View>
            <Text style={styles.roomNumber}>{Language().placeholder_room_password}</Text>
        </View>);
    }

    /**
     * 房间 号
     * @param text
     * @private
     */
    _changeChangePassword(text) {
        let password = text + '';
        let one = password[0] == null ? '' : password[0];
        let two = password[1] == null ? '' : password[1];
        let three = password[2] == null ? '' : password[2];
        let four = password[3] == null ? '' : password[3];
        if (password.length == 4) {
            this.passwordEndEditing();
        }
        if (/^[\d]+$/.test(text)) {
            this.setState({
                roomPassword: text,
                password1: one,
                password2: two,
                password3: three,
                password4: four,
            });
        } else {
            if (/^[\d{1}]$/.test(this.state.roomPassword)) {
                this.setState({
                    roomPassword: '',
                    password1: one,
                    password2: two,
                    password3: three,
                    password4: four,
                });
            }
        }
    }

    /**
     * 房间 号
     * @param text
     * @private
     */
    _changeVoiceRoomId(text) {
        this.setState({
            roomID: text
        });
    }

    /**
     * 房间主题
     * @param text
     * @private
     */
    _changeTitleTheme(text) {
        if (text) {
            text = text.replace(/[\r\n]/g, "");
        }
        this.setState({
            roomThemeContent: text
        });
    }

    /**
     * 输入框换行并自增加
     * @param event
     */
    onContentSizeChange(event) {
        this.setState({height: event.nativeEvent.contentSize.height});
    }

    /**
     * 创建房间
     * @private
     */
    _createVoiceRoom() {
        if (this.props.configData) {
            if (!this.props.configData.audio_room_types) {
                this.props.updateConfig();
            }
        } else {
            this.props.updateConfig();
        }
        that.setState({
            showVoiceModalDialog: true,
            dialogTitle: Language().create_room,
            modalDialogIndex: 0,
        });
    }

    /**
     * 搜索房间
     * @private
     */
    _searchVoiceRoom() {
        if (this.refs.roomIdInput) {
            this.refs.roomIdInput.blur();
        }
        let hostLocation = '';
        if (this.props.hostLocation) {
            hostLocation = this.props.hostLocation;
        }
        if (this.state.roomID) {
            let url = apiDefines.SERVER_GET + this.state.roomID + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id + apiDefines.PARAMETER_LOCATION + hostLocation;
            that.onShowProgressDialog();
            this.createRequest(true, url, null);
        } else {
            Toast.show(Language().placeholder_room_number);
        }
    }

    /**
     * 默认背景
     * @param height
     * @returns {*}
     * @private
     */
    _defaultView() {
        return recreationRoomList.length == 0 ? (<View
                style={{justifyContent: 'center',backgroundColor:apiDefines.APP_KEY == apiDefines.WEREWOLF ? Color.voice_room_bg_color : Color.voice_room_list_bg_color,position: 'absolute',flex:1,left:0,top:0,right:0,bottom:0}}>
                <CachedImage style={styles.defaultBg} source={ImageManager.icDefaultNewVoiceBg}/>
            </View>) : null;
    }

    /**
     * 网路请求
     * @param url
     * @param actionType
     */
    createRequest(isGet, url, body) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                if (isGet) {
                    Util.get(url, (code, message, data) => {
                        that.onHideProgressDialog();
                        if (code == 1000) {
                            //搜索房间
                            if (data.level == 'audio') {
                                if (data.password_needed) {
                                    //提示输入密码
                                    that.setState({
                                        showVoiceModalDialog: true,
                                        dialogTitle: Language().room_number + this.state.roomID,
                                        modalDialogIndex: 2,
                                        needPassword: data.password_needed
                                    });
                                } else {
                                    //直接进入
                                    let data = {
                                        roomId: this.state.roomID,
                                        roomPassword: ''
                                    }
                                    that.doEnterVoiceRoom(data);
                                }
                            } else {
                                Toast.show(Language().voice_room_error);
                            }
                        } else {
                            Toast.show(message);
                        }
                    }, (error) => {
                        that.onHideProgressDialog();
                        Toast.show(Language().not_network);
                    });
                } else {
                    Util.post(url, body, (code, message, data) => {
                        that.hideProgress();
                        if (code == 1000) {
                            let roomData = {
                                roomId: data.room_id,
                                roomPassword: this.state.roomPassword
                            }
                            that.doEnterVoiceRoom(roomData);
                        } else {
                            Toast.show(message);
                        }
                    }, (error) => {
                        that.hideProgress();
                        Toast.show(Language().not_network);
                    });
                }

            } else {
                that.hideProgress();
                that.onHideProgressDialog();
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    showProgress() {
        if (!that.state.showModelProgress) {
            that.setState({
                showModelProgress: true
            });
        }
    }

    roomDetailView(item) {
        let paddingRightRowTitle = 0;
        if (item) {
            let users = item.users;
            let title = '';
            if (item.title) {
                title = item.title;
                title = title.replace(/[\r\n]/g, "");
            } else {
                title = item.room_id;
            }
            let userCount = 0;
            if (users) {
                userCount = users.length;
            }
            if (item.userCount) {
                userCount = item.userCount;
            }
            let user_1 = that._getUser(users, 0);
            let user_2 = that._getUser(users, 1);
            let user_3 = that._getUser(users, 2);
            let user_4 = that._getUser(users, 3);
            let room_lock_icon = item.needPassword ? InternationSourceManager().icRoomIconLock : null;
            if (item.child_type == Constant.VOICE_TYPE_UNDERCOVER) {
                if (item.needPassword) {
                    paddingRightRowTitle = 18;
                } else {
                    paddingRightRowTitle = 70;
                }
            }
            let audioData = that.getAudioTypeData(item.child_type);
            return (<View flexDirection='row'
                          style={{borderRadius:8,borderColor:'#BBA8F8',borderWidth:3,padding:0,backgroundColor:apiDefines.APP_KEY == apiDefines.WEREWOLF ? Color.voice_room_bg_color : Color.voice_room_list_bg_color}}>
                <CachedImage style={[styles.image,{flex:1,margin:0}]} source={voiceItemBg}>
                    <View flexDirection='row'>
                        <View style={{borderRadius:3,justifyContent:'center'}}>
                            <CachedImage style={styles.ownerHeader}
                                         type='audio_header'
                                         source={(item.image)?({uri:item.image}):audioCreateHeader}
                                         defaultSource={audioCreateHeader}/>
                            <View style={{position: "absolute", top: 5,left:3}}>
                                <View flexDirection='row'>
                                    <CachedImage style={styles.voiceHomeIcon} source={voiceHomeIcon}/>
                                    <View flexDirection='row'
                                          style={{backgroundColor:'#ff009c',borderRadius:8,paddingLeft:5,paddingRight:5,height:12,alignSelf:'center'}}>
                                        <Text numberOfLines={1} style={styles.rowStatusText}>{item.room_id}</Text>
                                    </View>
                                </View>
                                <View flexDirection='row'>
                                    <Image style={styles.voiceHomeIcon} source={voiceUserIcon}/>
                                    <View
                                        style={{backgroundColor:'#ffba13',borderRadius:8,paddingLeft:5,paddingRight:5,height:12,alignSelf:'center'}}>
                                        <Text numberOfLines={1}
                                              style={[styles.rowStatusText,{color:Color.blackColor}]}>{userCount}人</Text>
                                    </View>
                                </View>
                            </View>
                            {this.userNameView(item.name)}
                            {that.audioTypeImageView(audioData)}
                        </View>
                        <View style={{flex:1}}>
                            <View flexDirection='row'>
                                <Text numberOfLines={1}
                                      style={[styles.rowTitle,{marginRight:paddingRightRowTitle}]}>{title}</Text>
                                <CachedImage style={styles.voiceHomeLock} source={room_lock_icon}/>
                                {this.voiceRoomStatusView(item.child_type, item.roomStatus)}
                            </View>
                            <View style={[styles.renderRow]}>
                                <View flexDirection='row'
                                      style={{flex:4,justifyContent:'center',alignSelf:'center',paddingRight:10,paddingLeft:10}}>
                                    <View style={{flex:1}}>
                                        <CachedImage style={styles.userHeader}
                                                     type='audio'
                                                     source={(!user_1.avatar.length)?defaultNoHeader:({uri:user_1.avatar})}
                                                     defaultSource={icDefaultUser}/>
                                        <Text numberOfLines={1} style={styles.rowUserName}>{user_1.name}</Text>
                                    </View>
                                    <View style={{flex:1}}>
                                        <CachedImage style={styles.userHeader}
                                                     type='audio'
                                                     source={(!user_2.avatar.length)?defaultNoHeader:({uri:user_2.avatar})}
                                                     defaultSource={icDefaultUser}/>
                                        <Text numberOfLines={1} style={styles.rowUserName}>{user_2.name}</Text>
                                    </View>
                                    <View style={{flex:1}}>
                                        <CachedImage style={styles.userHeader}
                                                     type='audio'
                                                     source={(!user_3.avatar.length)?defaultNoHeader:({uri:user_3.avatar})}
                                                     defaultSource={icDefaultUser}/>
                                        <Text numberOfLines={1} style={styles.rowUserName}>{user_3.name}</Text>
                                    </View>
                                    <View style={{flex:1}}>
                                        <CachedImage style={styles.userHeader}
                                                     type='audio'
                                                     source={(!user_4.avatar.length)?defaultNoHeader:({uri:user_4.avatar})}
                                                     defaultSource={icDefaultUser}/>
                                        <Text numberOfLines={1} style={styles.rowUserName}>{user_4.name}</Text>
                                    </View>
                                </View>
                            </View>
                            <View flexDirection='row' style={{flex:1}}>
                                <View flexDirection='row' style={{flex:1.3}}>
                                    {that.audioLikePopView(item.like, audioLike)}
                                    {that.audioLikePopView(item.pop, audioPop)}
                                </View>
                                <View style={{flex:1}}>
                                    {that.audioTypeTitleView(audioData)}
                                </View>
                            </View>
                        </View>
                    </View>
                </CachedImage>

            </View>);
        }
    }

    /**
     * 隐藏
     */
    hideProgress() {
        if (that.state.showModelProgress) {
            that.setState({
                showModelProgress: false
            });
        }
    }

    /**
     *
     * @returns {*}
     */
    audioTypeView() {
        return (audioTypes.length == 0) ? null : (<View>
                <Text style={{color:(Color.voice_set_text_color),fontSize:14}}>{Language().select_audio_type}</Text>
                <ListView dataSource={this.state.audioDataSource.cloneWithRows(audioTypes)}
                          renderRow={this.audioTypeRenderRow}
                          removeClippedSubviews={false}
                          contentContainerStyle={styles.listViewStyle}/>
            </View>);
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     */
    audioTypeRenderRow(item, sectionId, rowId) {
        let sourceImage = that.state.audioTypeIndex == rowId ? selectAudioIcon : normalAudioIcon;
        return ( <View style={styles.cellBackStyle}>
            <TouchableOpacity onPress={()=>that.selectAudio(item,rowId)} activeOpacity={0.9}
                              style={{justifyContent:'center',backgroundColor:Color.transparent,flex:1}}>
                <View flexDirection='row'>
                    <Image style={styles.audioTypeIcon} source={sourceImage}/>
                    <Text numberOfLines={1}
                          style={{color:(Color.voice_set_text_color),fontSize:12,alignSelf:'center',marginLeft:5}}>{item.t}</Text>
                </View>
            </TouchableOpacity></View>);
    }

    /**
     * 点击对应的item
     * @param data
     */
    selectAudio(data, rowId) {
        that.setState({
            audioTypeIndex: rowId,
            audioTypeValue: data.k,
            audioDataSource: that.state.audioDataSource.cloneWithRows(audioTypes),
        });
    }

    /**
     * 统一处理native的消息
     * @param event
     */
    nativeMessage(event) {
        if (event && event.Data) {
            let data = JSON.parse(event.Data);
            switch (data.action) {
                case Constant.LEAVE_AUDIO_ROOM:
                    let requestFlag = this.getRequestFlag()
                    let localType = this.props.voiceType;
                    if (localType == requestFlag.type) {
                        netWorkTool.checkNetworkState((isConnected) => {
                            if (!isConnected) {
                                Toast.show(Language().load_more_data_error);
                            } else {
                                this.onShowProgressDialog(Language().loading_now);
                                try {
                                    this.hideProgressTimer = setTimeout(() => {
                                        this.onHideProgressDialog();
                                    }, 5000);
                                } catch (e) {

                                }
                                InteractionManager.runAfterInteractions(() => {
                                    this.state.sampleRecreationRow = 0;
                                    this.props.fetchRecreationPageListFromType(1, requestFlag.actionFresh, requestFlag.type);
                                    this.doScrollToList();
                                });
                            }
                        });
                    }
                    break;
            }
        }
    }

}

const
    styles = StyleSheet.create({
        defaultBg: {
            alignSelf: 'center',
            width: ScreenWidth / 2 - 30,
            height: ScreenWidth / 2 - 30,
            resizeMode: 'contain',
        },
        voiceHome: {
            width: 18,
            height: 18,
            resizeMode: 'contain',
            alignSelf: 'center'
        },
        voiceSearch: {
            width: 25,
            height: 25,
            resizeMode: 'contain',
            alignSelf: 'center',
            marginRight: 5
        },
        voiceHomeText: {
            fontSize: 15,
            color: Color.colorWhite,
            alignSelf: 'center',
            padding: 0,
            paddingBottom: 0
        },
        voiceBt: {
            backgroundColor: Color.transparent,
            justifyContent: 'center',
            alignSelf: 'center'
        },
        renderRow: {
            justifyContent: 'center',
            padding: 1,
            flexDirection: 'row'
        },
        userHeader: {
            width: 40,
            height: 40,
            borderRadius: 20,
            alignSelf: 'center',
            marginLeft: 8,
            marginRight: 8
        },
        ownerHeader: {
            width: 100,
            height: 120,
            borderRadius: 5,
            alignSelf: 'center',
            top: 0
        },
        rowTitle: {
            color: Color.colorWhite,
            fontSize: 14,
            marginLeft: 8,
            fontWeight: 'bold',
            marginTop: 0,
            marginBottom: 5,
            flex: 1,
            alignSelf: 'flex-end',
            backgroundColor:Color.transparent,
            paddingRight: 0
        },
        rowUserName: {
            color: '#cabddd',
            fontSize: 10,
            alignSelf: 'center',
            textAlign: 'center',
            marginTop: 3,
            backgroundColor: Color.transparent, height: 13
        },
        rowStatusText: {
            color: Color.colorWhite,
            fontSize: 10,
            alignSelf: 'center',
            backgroundColor: Color.transparent
        },
        voiceIcon: {
            width: 50,
            height: 35,
            resizeMode: 'contain',
            alignSelf: 'center'
        },
        voiceHomeIcon: {
            width: 13,
            height: 14,
            resizeMode: 'contain',
            alignSelf: 'center',
            marginRight: 1
        },
        actionButton: {
            backgroundColor: '#845fe4', justifyContent: 'center', height: 40, flex: 1, borderRadius: 5, margin: 10
        },
        loading: {
            position: 'absolute',
            backgroundColor: 'rgba(0, 0, 0, 0.6)',
            height: 60,
            width: 80,
            borderRadius: 6,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
        },
        dialogContainer: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
        },
        rowSeparator: {
            height: StyleSheet.hairlineWidth,
            backgroundColor: '#4C3B52',
        },
        voiceHomeLock: {
            width: 55,
            height: 37,
            resizeMode: 'contain',
            alignSelf: 'center',
        },
        roomNumber: {
            color: '#b09ce5',
            fontSize: 14,
            alignSelf: 'center',
            marginTop: 8
        }, image: {
            width: ScreenWidth - 30,
            height: 120,
            resizeMode: 'stretch',
        }, containerView: {
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'transparent',
        }, listViewStyle: {
            flexDirection: 'row',
            flexWrap: 'wrap',
            marginRight: 20,
            marginLeft: 20
        }, audioTypeIcon: {
            width: 18,
            height: 18,
            resizeMode: 'contain',
            alignSelf: 'center',
        },
        cellBackStyle: {
            width: cellW,
            height: cellH,
            marginLeft: vMargin,
            marginTop: hMargin,
            alignSelf: 'center',
            justifyContent: 'center',
        }, filterButtonStyle: {
            justifyContent: 'center',
            backgroundColor: Color.transparent,
            padding: 5,
            paddingLeft: 25,
            paddingRight: 25,
        }, filterText: {
            fontSize: 14,
            alignSelf: 'center',
            color: '#965f92'
        }, titleImage: {
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
            resizeMode: 'stretch',
            height: 50,
            width: ScreenWidth
        }, title: {
            fontSize: 14,
            color: 'gray',
            alignSelf: 'center', margin: 5
        }, passwordViewStyle: {
            backgroundColor: '#835EDF',
            borderColor: '#6939DA',
            borderWidth: 1,
            padding: 5,
            borderRadius: 3,
            width: 25,
            height: 35
        }, passwordTextStyle: {fontSize: 16, color: Color.colorWhite, fontWeight: 'bold', alignSelf: 'center'},
        roomStatusViewStyle: {
            backgroundColor: '#008cff',
            borderRadius: 8,
            paddingLeft: 5,
            paddingRight: 5,
            height: 11,
            position: 'absolute',
            bottom: 8,
            right: 35
        }
    });
const
    mapDispatchToProps = dispatch => ({
            fetchRecreationPageListFromType: (page, actionType, type) => dispatch(fetchRecreationPageListFromType(page, actionType, type)),
            updateConfig: () => dispatch(checkOpenRegister()),
            updateFilter: (filter) => dispatch(updateFilter(filter)),
        }
    );
const mapStateToProps = state => ({
    userInfo: state.userInfoReducer.data,
    hostLocation: state.configReducer.location,
    configData: state.checkRegisterReducer.data,
    isRefreshingSing: state.voiceRecreationReducer.isRefreshingSing,
    isNoMoreSing: state.voiceRecreationReducer.isNoMoreSing,
    dataSing: state.voiceRecreationReducer.dataSing,
    isRefreshingChat: state.voiceRecreationReducer.isRefreshingChat,
    isNoMoreChat: state.voiceRecreationReducer.isNoMoreChat,
    dataChat: state.voiceRecreationReducer.dataChat,
    isRefreshingLove: state.voiceRecreationReducer.isRefreshingLove,
    isNoMoreLove: state.voiceRecreationReducer.isNoMoreLove,
    dataLove: state.voiceRecreationReducer.dataLove,
    isRefreshingMerry: state.voiceRecreationReducer.isRefreshingMerry,
    isNoMoreMerry: state.voiceRecreationReducer.isNoMoreMerry,
    dataMerry: state.voiceRecreationReducer.dataMerry,
    isRefreshingSeek: state.voiceRecreationReducer.isRefreshingSeek,
    isNoMoreSeek: state.voiceRecreationReducer.isNoMoreSeek,
    dataSeek: state.voiceRecreationReducer.dataSeek,
    isRefreshingGame: state.voiceRecreationReducer.isRefreshingGame,
    isNoMoreGame: state.voiceRecreationReducer.isNoMoreGame,
    dataGame: state.voiceRecreationReducer.dataGame,
    isRefreshingFate: state.voiceRecreationReducer.isRefreshingFate,
    isNoMoreFate: state.voiceRecreationReducer.isNoMoreFate,
    dataFate: state.voiceRecreationReducer.dataFate,
    isRefreshingUndercover: state.voiceRecreationReducer.isRefreshingUndercover,
    isRefreshingAll: state.voiceRecreationReducer.isRefreshingAll,
    isNoMoreUndercover: state.voiceRecreationReducer.isNoMoreUndercover,
    isNoMoreAll: state.voiceRecreationReducer.isNoMoreAll,
    dataUndercover: state.voiceRecreationReducer.dataUndercover,
    dataAll: state.voiceRecreationReducer.dataAll,
    actionTypeSing: state.voiceRecreationReducer.typesSing,
    actionTypeChat: state.voiceRecreationReducer.typesChat,
    actionTypeLove: state.voiceRecreationReducer.typesLove,
    actionTypeMerry: state.voiceRecreationReducer.typesMerry,
    actionTypeSeek: state.voiceRecreationReducer.typesSeek,
    actionTypeGame: state.voiceRecreationReducer.typesGame,
    actionTypeUndercover: state.voiceRecreationReducer.typesUndercover,
    actionTypeFate: state.voiceRecreationReducer.typesFate,
    actionTypeAll: state.voiceRecreationReducer.typesAll,
    voiceType: state.voiceRecreationReducer.voiceType,
    isTourist: state.userInfoReducer.isTourist,
});
export default connect(mapStateToProps, mapDispatchToProps)(VoiceRoom);