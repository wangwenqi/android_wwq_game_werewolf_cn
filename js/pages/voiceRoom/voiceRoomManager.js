/**
 * Created by wangxu on 2017/12/5.
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Platform,
    Dimensions,
    ListView,
    InteractionManager,
    Modal,
    ScrollView,
    TextInput,
    Text,
    Image,
    TouchableOpacity, View, Switch, DeviceEventEmitter
} from 'react-native';
import {Input, Spinner} from 'native-base';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
//自定义工具
import Language from '../../../resources/language/index';
import Color from '../../../resources/themColor/index';
import ImageManager from '../../../resources/imageManager/index';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import netWorkTool from '../../../support/common/netWorkTool';
import Loading from '../../../support/common/Loading';
import Toast from '../../../support/common/Toast';
import VoiceScrollableTabBar from '../../../support/common/VoiceScrollableTabBar';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import  CachedImage from '../../../support/common/CachedImage';
import  * as Constant from '../../../support/common/constant';
import  * as types from '../../../support/actions/actionTypes';

import {checkOpenRegister} from '../../../support/actions/checkRegisterActions';
import rnToNativeUtils from '../../../support/common/rnToNativeUtils';
import ScrollableTabBar from '../../../support/common/ScrollableTabBar';
import VoiceRoom from './voiceRoom';
import Util from '../../../support/common/utils';
import {fetchRecreationPageListFromType, updateVoiceType} from '../../../support/actions/recreationActions';
import localVoiceChildTypes from '../../../resources/data/localVoiceChildTypes';

//
const needLock = (Platform.OS === 'ios') ? true : false;
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const requestRandomRoom = 0;//随机进入房间请求

//
let self;
let audioTypes = new Array();
let allChildTypes = new Array();
let progressDialog;
let cols = 3;
let cellH = 30;
let cellW = (ScreenWidth - 130) / 3;
let vMargin = 5;
let hMargin = 5;
let audioType = '';
class VoiceRoomManager extends Component {
    constructor(props) {
        super(props);
        self = this;
        localVoiceChildTypes.getLocalVoiceTypeList((list) => {
            allChildTypes = list;
        });
        if (this.props.configData) {
            if (this.props.configData.audio_room_types) {
                audioTypes = this.props.configData.audio_room_types;
            }
        }
        if (audioTypes[0]) {
            if (audioTypes[0].k) {
                audioType = audioTypes[0].k;
            }
        }
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 == r2});
        //2.设置返回数据
        this.state = {
            index: 0,
            audioDataSource: ds.cloneWithRows(audioTypes),
            voiceChildDataSource: ds.cloneWithRows(allChildTypes),
            dialogTitle: Language().room_theme,
            showVoiceModalDialog: false,
            showModelProgress: false,
            modalDialogIndex: 0,
            height: 0,
            roomThemeContent: '',
            roomPassword: '',
            roomID: '',
            dialogCancel: Language().cancel,
            dialogConfirm: Language().confirm,
            needPassword: false,
            switchIsOn: false,
            roomDetailData: null,
            password1: '',
            password2: '',
            password3: '',
            password4: '',
            audioTypeIndex: 0,
            audioTypeValue: audioType,
            showVoiceAllTypes: false
        };
    }

    static propTypes = {
        configData: React.PropTypes.object,
        userInfo: React.PropTypes.object,
        updateConfig: React.PropTypes.func,
        fetchRecreationPageListFromType: React.PropTypes.func,
        updateVoiceType: React.PropTypes.func,
        voiceChildType: React.PropTypes.string,
        isTourist: React.PropTypes.bool
    }

    componentDidMount() {
        progressDialog = this.refs.progressDialog;
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.subscription = DeviceEventEmitter.addListener(Constant.SHOW_ALL_CHILD_TYPE, this.showAllTypes.bind(this));

        InteractionManager.runAfterInteractions(() => {
            this.props.fetchRecreationPageListFromType(1, types.RECREATION_ALL_DATA, Constant.VOICE_TYPE_ALL);
        });
        //通知语音房间滑动到最顶
        let flagData = {
            type: Constant.VOICE_TYPE_ALL,
            actionType: types.RECREATION_ALL_DATA
        }
        DeviceEventEmitter.emit(Constant.SCROLL_VOICE_RECREATION_LIST, flagData);
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.subscription.remove();
        this.closeModalTimer && clearTimeout(this.closeModalTimer);
    }

    componentWillMount() {
    }

    handleMethod(isConnected) {

    }

    /**
     *
     * @returns {XML}
     */
    render() {
        if (this.props.configData) {
            if (this.props.configData.audio_room_types) {
                audioTypes = this.props.configData.audio_room_types;
            }
        }
        return <View style={styles.containerView}>
            {(Platform.OS === 'ios') ? (<View style={{height:20}}/>) : null}
            {this.headerView()}
            <VoiceRoom tabLabel={Language().voice_type_all} voiceChildType={Constant.VOICE_TYPE_ALL}/>
            {/*<ScrollableTabView*/}
                {/*renderTabBar={this.getScrollableTabBar}*/}
                {/*style={styles.tabView}*/}
                {/*tabBarPosition='top'*/}
                {/*scrollWithoutAnimation={false}*/}
                {/*locked={false}*/}
                {/*onChangeTab={(data)=>this.changeMessageTab(data)}>*/}
                {/*<VoiceRoom tabLabel={Language().voice_type_all} voiceChildType={Constant.VOICE_TYPE_ALL}/>*/}
                {/*<VoiceRoom tabLabel={Language().voice_type_sing} voiceChildType={Constant.VOICE_TYPE_SING}/>*/}
                {/*<VoiceRoom tabLabel={Language().voice_type_chat} voiceChildType={Constant.VOICE_TYPE_CHAT}/>*/}
                {/*<VoiceRoom tabLabel={Language().voice_type_love} voiceChildType={Constant.VOICE_TYPE_LOVE}/>*/}
                {/*<VoiceRoom tabLabel={Language().voice_type_undercover} voiceChildType={Constant.VOICE_TYPE_UNDERCOVER}/>*/}
                {/*<VoiceRoom tabLabel={Language().voice_type_fate} voiceChildType={Constant.VOICE_TYPE_FATE}/>*/}
                {/*<VoiceRoom tabLabel={Language().voice_type_merry} voiceChildType={Constant.VOICE_TYPE_MERRY}/>*/}
                {/*<VoiceRoom tabLabel={Language().voice_type_seek} voiceChildType={Constant.VOICE_TYPE_SEEK}/>*/}
                {/*<VoiceRoom tabLabel={Language().voice_type_game} voiceChildType={Constant.VOICE_TYPE_GAME}/>*/}
            {/*</ScrollableTabView>*/}
            <View style={{ position: 'absolute',right:15,bottom:10}}>
                <TouchableOpacity style={{justifyContent:'center',padding:5}}
                                  activeOpacity={0.8} onPress={this.randomEnterGame.bind(this)}>
                    <CachedImage style={{width:60,height:60,alignSelf:'center'}}
                                 source={ImageManager.icHyperchannel}/>
                </TouchableOpacity>
            </View>
            <Loading ref="progressDialog" loadingTitle={Language().operation_loading}/>
            {/*创建房间以及搜索房间的弹框*/}
            <Modal visible={this.state.showVoiceModalDialog} transparent={true} animationType='fade'>
                <View flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                      style={[styles.dialogContainer]}>

                    <ScrollView alwaysBounceVertical={false}>
                        <View
                            style={{borderRadius:8,width:ScreenWidth-30,maxHeight:ScreenHeight-80,alignSelf:'center',backgroundColor:'#e3dce9',padding:0}}>
                            <View flexDirection='row' style={{justifyContent:'center',padding:5}}>
                                <Text
                                    style={{color:Color.voice_set_text_color,alignSelf:'center',fontSize:17,textAlign:'center'}}>{this.state.dialogTitle}</Text>
                            </View>
                            {this._modelDialogContentView()}
                            <View flexDirection='row' style={{justifyContent:'center',margin:10}}>
                                <TouchableOpacity activeOpacity={0.9}
                                                  onPress={this.onCloseModal.bind(this)}
                                                  style={[styles.actionButton,{backgroundColor:Color.colorWhite}]}>
                                    <Text
                                        style={{alignSelf:'center',color:'#845fe4'}}>{this.state.dialogCancel}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity activeOpacity={0.8} onPress={this.onConfirmModal.bind(this)}
                                                  style={styles.actionButton}>
                                    <Text
                                        style={{alignSelf:'center',color:Color.colorWhite}}>{this.state.dialogConfirm}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {this.state.showModelProgress ? (
                                <View style={[styles.loading,{left:ScreenWidth/2-55,top:45}]}>
                                    <Spinner size='small' style={{height:35}}/>
                                    <Text
                                        style={{ marginTop: 5,fontSize: 14,color:Color.colorWhite}}>{Language().request_loading}</Text>
                                </View>) : null}
                    </ScrollView>
                </View>
            </Modal>
            {/*所有类型展示*/}
            <Modal visible={this.state.showVoiceAllTypes} transparent={true} animationType='fade'
                   onRequestClose={()=>this.setState({showVoiceAllTypes: false})}>
                <View
                    style={{ alignItems:'center', backgroundColor:'rgba(0, 0, 0, 0.5)',justifyContent:'center',flex:1}}>
                    {this.allChildTypeView()}
                </View>
            </Modal>
        </View>
    }

    getScrollableTabBar() {
        return (<ScrollableTabBar ref="getSwordButton" activeTextColor={Color.voice_active_text_color}
                                  inactiveTextColor={Color.voice_inactiveTextColor}
                                  underlineStyle={styles.underlineStyle}
                                  textStyle={styles.textStyle} style={styles.style} tabStyle={styles.tabStyle}
                                  tabsContainerStyle={styles.tabsContainerStyle}/>);
    }

    /**
     * 列表固定头部
     */
    headerView() {
        return (<View>
            <View flexDirection='row'
                  style={{justifyContent:'center',padding:8,marginTop:1,marginLeft:5,marginRight:5,backgroundColor:Color.transparent}}>
                <TouchableOpacity onPress={()=>self._createVoiceRoom()}
                                  activeOpacity={0.6}
                                  style={styles.voiceBt}>
                    <View flexDirection='row'
                          style={{backgroundColor:Color.voice_room_create_bg_color,borderRadius:3,justifyContent:'center',height:34,width:80}}>
                        <Image source={ImageManager.bgVoiceHome} style={styles.voiceHome}></Image>
                        <Text
                            style={[styles.voiceHomeText,{fontWeight:'bold',marginLeft:3,alignSelf:'center'}]}>{Language().create}</Text>
                    </View>
                </TouchableOpacity>

                <View flexDirection='row'
                      style={{backgroundColor:Color.voice_room_search_bg_color,borderRadius:3,borderColor:Color.voice_room_search_border_color,marginLeft:3,height:35,alignSelf:'center',padding:3,borderWidth:1,flex:1,marginRight:3}}>
                    <TextInput
                        ref="roomIdInput"
                        keyboardType='numeric'
                        underlineColorAndroid='transparent'
                        style={{fontSize: 16,color:Color.colorWhite,padding: 5,height:30,flex:1,alignSelf:'center'}}
                        placeholder={Language().search_room}
                        placeholderTextColor='#bb9edb'
                        onChangeText={(text)=>this._changeVoiceRoomId(text)} value={this.state.roomID}/>
                    <TouchableOpacity onPress={()=>self._searchVoiceRoom()}
                                      activeOpacity={0.6}
                                      style={styles.voiceBt}>
                        <Image source={ImageManager.bgVoiceSearch} style={styles.voiceSearch}></Image>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={()=>self._openRankingList()}
                                  activeOpacity={0.6}
                                  style={styles.voiceBt}>
                    <View flexDirection='row'
                          style={{backgroundColor:Color.voice_room_create_bg_color,borderRadius:3,justifyContent:'center',height:34,width:80}}>
                        <Image source={ImageManager.bgRankingList} style={styles.voiceHome}></Image>
                        <Text
                            style={[styles.voiceHomeText,{fontWeight:'bold',marginLeft:3,alignSelf:'center'}]}>{Language().ranking_list}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>);
    }

    /**
     * 1--当tab改变时
     * @param data
     */
    changeMessageTab(data) {
        if (data) {
            let actionType = types.RECREATION_ALL_DATA;
            let type = Constant.VOICE_TYPE_ALL;
            switch (data.i) {
                case 0:
                    actionType = types.RECREATION_ALL_DATA;
                    type = Constant.VOICE_TYPE_ALL;
                    break;
                case 1:
                    actionType = types.RECREATION_SING_DATA;
                    type = Constant.VOICE_TYPE_SING;
                    break;
                case 2:
                    actionType = types.RECREATION_CHAT_DATA;
                    type = Constant.VOICE_TYPE_CHAT;
                    break;
                case 3:
                    actionType = types.RECREATION_LOVE_DATA;
                    type = Constant.VOICE_TYPE_LOVE;
                    break;
                case 5:
                    actionType = types.RECREATION_FATE_DATA;
                    type = Constant.VOICE_TYPE_FATE;
                    break;
                case 6:
                    actionType = types.RECREATION_MERRY_DATA;
                    type = Constant.VOICE_TYPE_MERRY;
                    break;
                case 7:
                    actionType = types.RECREATION_SEEK_DATA;
                    type = Constant.VOICE_TYPE_SEEK;
                    break;
                case 8:
                    actionType = types.RECREATION_GAME_DATA;
                    type = Constant.VOICE_TYPE_GAME;
                    break;
                case 4:
                    actionType = types.RECREATION_UNDERCOVER_DATA;
                    type = Constant.VOICE_TYPE_UNDERCOVER;
                    break;
            }
            this.props.updateVoiceType(type);
            //通知语音房间滑动到最顶
            let flagData = {
                type: type,
                actionType: actionType
            }
            DeviceEventEmitter.emit(Constant.SCROLL_VOICE_RECREATION_LIST, flagData);
        }
    }

    /**
     *1-- 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     *1-- 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    /**
     *
     */
    onConfirmModal() {
        if (!this.state.showModelProgress) {
            let hostLocation = '';
            if (this.props.hostLocation) {
                hostLocation = this.props.hostLocation;
            }
            switch (self.state.modalDialogIndex) {
                case 0:
                    //创建房间
                    let url = apiDefines.CREATE_AUDIO_ROOM;
                    let {roomThemeContent, roomPassword} = this.state;
                    roomThemeContent = roomThemeContent.trim();
                    if (!roomThemeContent.length || roomThemeContent.length < 4) {
                        Toast.show(Language().voice_theme_limit_message)
                        return;
                    }
                    let body = {
                        type: 'audio',
                        title: roomThemeContent,
                        password: roomPassword,
                        lc: hostLocation,
                        child_type: this.state.audioTypeValue
                    }
                    self.showProgress();
                    self.createRequest(false, url, body);
                    break;
                case 1:
                    //搜索房间
                    if (!this.state.roomPassword) {
                        Toast.show(Language().placeholder_password)
                        return;
                    }
                    let data = {
                        roomId: this.state.roomID,
                        roomPassword: this.state.roomPassword
                    }
                    self.doEnterVoiceRoom(data);
                    break;
                case 2:
                    //直接进入
                    let newData = {
                        roomId: this.state.roomID,
                        roomPassword: this.state.roomPassword
                    }
                    self.doEnterVoiceRoom(newData);
                    break;
            }
        }
    }

    /**
     *
     */
    onCloseModal() {
        self.setState({
            showVoiceModalDialog: false,
            showModelProgress: false,
            modalDialogIndex: 0,
            height: 0,
            roomThemeContent: '',
            roomPassword: '',
            roomID: '',
            needPassword: false,
            switchIsOn: false,
            roomDetailData: null,
            password1: '',
            password2: '',
            password3: '',
            password4: '',
            audioTypeIndex: 0,
            audioTypeValue: audioType
        });
    }

    /**
     *
     * @private
     * 内容可分为：
     * 1->创建音频房间
     * 2->搜索音频房间
     * 3->房间需要输入密码
     *
     */
    _modelDialogContentView() {
        switch (self.state.modalDialogIndex) {
            case 0:
                return this._modelCreateRoom();
            case 2:
                return this._modelInitPassword();
        }
    }

    passwordInputView() {
        return (<TextInput
            ref='passwordInput'
            style={{paddingLeft:0,color:(Color.transparent),fontSize:12,height:35,padding:5,paddingLeft:0,backgroundColor:Color.transparent,lineHeight:0,selectionColor:Color.transparent}}
            autoFocus={true} maxLength={4}
            selectionColor={Color.transparent}
            onChangeText={(text)=>this._changeChangePassword(text)}
            keyboardType='numeric'
            onSubmitEditing={()=>this.passwordEndEditing()}
            value={this.state.roomPassword}/>);
    }

    /**
     *
     * @returns {XML}
     */
    passwordView() {
        return (<View flexDirection='row' style={{position: "absolute"}}>
            <TouchableOpacity onPress={this.focusPassword.bind(this)}
                              activeOpacity={1}
                              style={{justifyContent:'center',backgroundColor:'#E1DDE8',flex:1}}>
                <View flexDirection='row'
                      style={{alignSelf:'center',justifyContent:'center',flex:1,backgroundColor:'#E1DDE8'}}>
                    <View
                        style={styles.passwordViewStyle}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password1}</Text>
                    </View>
                    <View
                        style={[styles.passwordViewStyle,{marginLeft:10}]}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password2}</Text>
                    </View>
                    <View
                        style={[styles.passwordViewStyle,{marginLeft:10}]}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password3}</Text>
                    </View>
                    <View
                        style={[styles.passwordViewStyle,{marginLeft:10}]}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password4}</Text>
                    </View>

                </View>

            </TouchableOpacity>
        </View>);
    }

    /**
     * 房间ID
     * @returns {XML}
     * @private
     */
    _modelInitPassword() {
        return (<View style={{marginTop:5}}>
            <View style={{marginTop:10}}>
                {this.passwordInputView()}
                {this.passwordView()}
            </View>
            <Text style={styles.roomNumber}>{Language().placeholder_room_password}</Text>
        </View>);
    }

    /**
     *
     * @returns {XML}
     * @private
     */
    _modelCreateRoom() {
        let setPassword = (<View
            style={{margin:3,marginTop:10}}>
            {this.passwordInputView()}
            {this.passwordView()}
        </View>);
        return (<View style={{margin:10,marginRight:20,marginLeft:20}}>
            {this.audioTypeView()}
            <Text
                style={{color:(Color.voice_set_text_color),fontSize:14}}>{Language().room_theme}</Text>
            <View
                style={{borderColor:'#a66ce9',borderRadius:3,borderWidth:1,marginRight:20,marginLeft:20,marginTop:10}}>
                <Input
                    multiline={true}
                    autoFocus={true}
                    style={{fontSize: 12,color: (Color.voice_set_text_color),padding: 5,height:Math.max(30,this.state.height),paddingRight:10,paddingLeft:10}}
                    placeholder={Language().placeholder_room_theme}
                    placeholderTextColor={Color.prompt_text_color}
                    maxLength={15}
                    onChangeText={(text)=>this._changeTitleTheme(text)} value={this.state.roomThemeContent}
                    onContentSizeChange={this.onContentSizeChange.bind(this)}/>
            </View>
            <Text
                style={{fontSize:10,paddingBottom:5,marginTop:10,marginRight:20,marginLeft:20,color:'#757575'}}>{Language().voice_limit_message}</Text>
            <View flexDirection='row' style={{marginTop:5}}>
                <Text
                    style={{color:(Color.voice_set_text_color),fontSize:14,marginRight:10,alignSelf:'center'}}>{Language().set_password}</Text>
                <Switch onValueChange={(value)=>this.setState({switchIsOn:value})}
                        value={this.state.switchIsOn}/>
            </View>
            {this.state.switchIsOn ? setPassword : null}
        </View>);
    }

    /**
     * 密码
     * @param text
     * @private
     */
    _changeChangePassword(text) {
        let password = text + '';
        let one = password[0] == null ? '' : password[0];
        let two = password[1] == null ? '' : password[1];
        let three = password[2] == null ? '' : password[2];
        let four = password[3] == null ? '' : password[3];
        if (password.length == 4) {
            this.passwordEndEditing();
        }
        if (/^[\d]+$/.test(text)) {
            this.setState({
                roomPassword: text,
                password1: one,
                password2: two,
                password3: three,
                password4: four,
            });
        } else {
            if (/^[\d{1}]$/.test(this.state.roomPassword)) {
                this.setState({
                    roomPassword: '',
                    password1: one,
                    password2: two,
                    password3: three,
                    password4: four,
                });
            }
        }
    }

    /**
     *
     */
    passwordEndEditing() {
        if (this.refs.passwordInput) {
            this.refs.passwordInput.blur();
        }
    }

    /**
     * 房间 号
     * @param text
     * @private
     */
    _changeVoiceRoomId(text) {
        this.setState({
            roomID: text
        });
    }

    /**
     * 房间主题
     * @param text
     * @private
     */
    _changeTitleTheme(text) {
        if (text) {
            text = text.replace(/[\r\n]/g, "");
        }
        this.setState({
            roomThemeContent: text
        });
    }

    /**
     * 输入框换行并自增加
     * @param event
     */
    onContentSizeChange(event) {
        this.setState({height: event.nativeEvent.contentSize.height});
    }

    /**
     * 创建房间
     * @private
     */
    _createVoiceRoom() {
        if (this.props.isTourist) {
            let event = {needShow: true, type: types.ACTION_RECREATION};
            this.sendEvent(Constant.ACTION_TOURIST_EVENT, event);
        } else {
            if (this.props.configData) {
                if (!this.props.configData.audio_room_types) {
                    this.props.updateConfig();
                }
            } else {
                this.props.updateConfig();
            }
            if (audioTypes) {
                if (audioTypes[0]) {
                    if (audioTypes[0].k) {
                        this.state.audioTypeValue = audioTypes[0].k;
                    }
                }
            }
            self.setState({
                showVoiceModalDialog: true,
                dialogTitle: Language().create_room,
                modalDialogIndex: 0,
            });
        }
    }

    /**
     * 搜索房间
     * @private
     */
    _searchVoiceRoom() {
        if (this.props.isTourist) {
            let event = {needShow: true, type: types.ACTION_RECREATION};
            this.sendEvent(Constant.ACTION_TOURIST_EVENT, event);
        } else {
            if (this.refs.roomIdInput) {
                this.refs.roomIdInput.blur();
            }
            let hostLocation = '';
            if (this.props.hostLocation) {
                hostLocation = this.props.hostLocation;
            }
            if (this.state.roomID) {
                let url = apiDefines.SERVER_GET + this.state.roomID + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id + apiDefines.PARAMETER_LOCATION + hostLocation;
                self.onShowProgressDialog();
                self.createRequest(true, url, null);
            } else {
                Toast.show(Language().placeholder_room_number);
            }
        }
    }

    /**
     *
     * @private
     */
    _openRankingList() {
        if (this.props.isTourist) {
            let event = {needShow: true, type: types.ACTION_RECREATION};
            this.sendEvent(Constant.ACTION_TOURIST_EVENT, event);
        } else {
            rnToNativeUtils.openWebView(apiDefines.RANK_AUDIO);
        }
    }

    /**
     *
     * @returns {*}
     */
    audioTypeView() {
        return (audioTypes.length == 0) ? null : (<View>
                <Text style={{color:(Color.voice_set_text_color),fontSize:14}}>{Language().select_audio_type}</Text>
                <ListView dataSource={this.state.audioDataSource.cloneWithRows(audioTypes)}
                          renderRow={this.audioTypeRenderRow}
                          removeClippedSubviews={false}
                          contentContainerStyle={styles.listViewStyle}/>
            </View>);
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     */
    audioTypeRenderRow(item, sectionId, rowId) {
        let sourceImage = self.state.audioTypeIndex == rowId ? ImageManager.selectAudioIcon : ImageManager.normalAudioIcon;
        return ( <View style={styles.cellBackStyle}>
            <TouchableOpacity onPress={()=>self.selectAudio(item,rowId)} activeOpacity={0.9}
                              style={{justifyContent:'center',backgroundColor:Color.transparent,flex:1}}>
                <View flexDirection='row'>
                    <Image style={styles.audioTypeIcon} source={sourceImage}/>
                    <Text numberOfLines={1}
                          style={{color:(Color.voice_set_text_color),fontSize:12,alignSelf:'center',marginLeft:5}}>{item.t}</Text>
                </View>
            </TouchableOpacity></View>);
    }

    /**
     * 点击对应的item
     * @param data
     */
    selectAudio(data, rowId) {
        self.setState({
            audioTypeIndex: rowId,
            audioTypeValue: data.k,
            audioDataSource: self.state.audioDataSource.cloneWithRows(audioTypes),
        });
    }

    showProgress() {
        if (!self.state.showModelProgress) {
            self.setState({
                showModelProgress: true
            });
        }
    }

    /**
     * 隐藏
     */
    hideProgress() {
        if (self.state.showModelProgress) {
            self.setState({
                showModelProgress: false
            });
        }
    }

    focusPassword() {
        if (this.refs.passwordInput) {
            this.refs.passwordInput.focus();
        }
    }

    /**
     * 执行进入房间的操作
     */
    doEnterVoiceRoom(roomData) {
        let NativeData = {
            action: 'enterAudioRoom',
            params: roomData,
            options: null
        }
        if (this.state.showVoiceModalDialog) {
            this.onCloseModal();
        }
        rnToNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }

    /**
     * 网路请求
     * @param url
     * @param actionType
     */
    createRequest(isGet, url, body) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                if (isGet) {
                    Util.get(url, (code, message, data) => {
                        self.onHideProgressDialog();
                        if (code == 1000) {
                            //搜索房间
                            if (data.level == 'audio') {
                                if (data.password_needed) {
                                    //提示输入密码
                                    self.setState({
                                        showVoiceModalDialog: true,
                                        dialogTitle: Language().room_number + this.state.roomID,
                                        modalDialogIndex: 2,
                                        needPassword: data.password_needed
                                    });
                                } else {
                                    //直接进入
                                    let data = {
                                        roomId: this.state.roomID,
                                        roomPassword: ''
                                    }
                                    self.doEnterVoiceRoom(data);
                                }
                            } else {
                                Toast.show(Language().voice_room_error);
                            }
                        } else {
                            Toast.show(message);
                        }
                    }, (error) => {
                        self.onHideProgressDialog();
                        Toast.show(Language().not_network);
                    });
                } else {
                    Util.post(url, body, (code, message, data) => {
                        self.hideProgress();
                        if (code == 1000) {
                            let roomData = {
                                roomId: data.room_id,
                                roomPassword: this.state.roomPassword
                            }
                            self.doEnterVoiceRoom(roomData);
                        } else {
                            Toast.show(message);
                        }
                    }, (error) => {
                        self.hideProgress();
                        Toast.show(Language().not_network);
                    });
                }

            } else {
                self.hideProgress();
                self.onHideProgressDialog();
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     *展示
     */
    showAllTypes() {
        this.setState({
            showVoiceAllTypes: true
        })
    }

    /**
     *
     * @param i
     */
    goToPage(i) {
        let pagData = {
            page: i - 0
        }
        DeviceEventEmitter.emit(Constant.GO_TO_PAGE, pagData);
        this.closeModalTimer = setTimeout(() => {
            this.setState({
                showVoiceAllTypes: false
            });
        }, 0)
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     */
    allChildTypeRenderRow(item, sectionId, rowId) {
        let marginLeft = 10;
        if (rowId % 3 == 0) {
            marginLeft = 0;
        }
        return (<View style={{marginLeft:marginLeft,alignSelf:'center',marginTop:10,width:60,justifyContent:'center'}}>
            <TouchableOpacity style={{justifyContent:'center',padding:0}}
                              activeOpacity={0.8} onPress={()=>self.goToPage(rowId)}>
                <CachedImage style={{width:45,height:45,alignSelf:'center'}}
                             source={item.image}/>
                <Text style={{color:Color.colorWhite,alignSelf:'center',fontSize:14}}>{item.name}</Text>
            </TouchableOpacity>
        </View>);
    }

    /**
     *
     * @returns {*}
     */
    allChildTypeView() {
        return (<View
            style={{backgroundColor:'rgba(55, 37, 72, 0.8)',borderRadius:8,alignSelf:'center',padding:30,paddingTop:10}}>
            <View style={{ position: 'absolute',right:0,top:0}}>
                <TouchableOpacity style={{justifyContent:'center',padding:5}}
                                  activeOpacity={0.8} onPress={()=>this.setState({showVoiceAllTypes:false})}>
                    <CachedImage style={{width:20,height:20,alignSelf:'center'}}
                                 source={ImageManager.icCloseAllVoiceType}/>
                </TouchableOpacity>
            </View>

            <Text style={{color:'#bbbbbb',fontSize:16,alignSelf:'center'}}>{Language().all_voice_child_type}</Text>
            <View style={{width:200,height:ScreenWidth/3*2,marginTop:20}}>
                <ListView dataSource={this.state.voiceChildDataSource.cloneWithRows(allChildTypes)}
                          renderRow={this.allChildTypeRenderRow}
                          removeClippedSubviews={false}
                          contentContainerStyle={{flexDirection: 'row',flexWrap: 'wrap'}}/>
            </View>
        </View>);
    }

    /**
     * 随机进入对应类型房间
     * 游客模式下提示登录后操作
     * @param data
     */
    randomEnterGame() {
        if (this.props.isTourist) {
            let event = {needShow: true, type: types.ACTION_RECREATION};
            this.sendEvent(Constant.ACTION_TOURIST_EVENT, event)
        } else {
            this.onShowProgressDialog();
            let voiceChildType = '';
            if (this.props.voiceChildType != Constant.VOICE_TYPE_ALL) {
                voiceChildType = this.props.voiceChildType;
            }
            this.requestApi(apiDefines.GET_AUDIO_RANDOM_ROOM + voiceChildType, requestRandomRoom);
        }
    }

    /**
     *
     * @param action
     * @param event
     */
    sendEvent(action, event) {
        DeviceEventEmitter.emit(action, event);
    }

    /**
     * request api
     */
    requestApi(url, type) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                Util.get(url, (code, message, data) => {
                    this.onHideProgressDialog();
                    if (code == 1000) {
                        if (data) {
                            if (type == requestRandomRoom) {
                                let roomInfo = {
                                    roomId: data.room_id,
                                    roomPassword: ''
                                }
                                this.doEnterVoiceRoom(roomInfo);
                            }
                        }
                    } else {
                        Toast.show(message);
                    }
                }, (error) => {
                    this.onHideProgressDialog();
                    Toast.show(Language().not_network);
                });
            } else {
                this.onHideProgressDialog();
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }
}
const styles = StyleSheet.create({
    tabView: {
        backgroundColor: apiDefines.APP_KEY == apiDefines.WEREWOLF ? Color.voice_room_bg_color : Color.voice_room_list_bg_color,
    },
    underlineStyle: {backgroundColor: Color.voice_active_line_color, height: 1, borderRadius: 3},
    textStyle: {fontSize: 14, padding: 0},
    tabStyle: {
        height: 32,
        backgroundColor: Color.transparent,
        paddingLeft: 10,
        paddingRight: 10,
    },
    tabsContainerStyle: {
        justifyContent: 'center',
    },
    style: {
        height: 32,
        borderWidth: 0,
        backgroundColor: apiDefines.APP_KEY == apiDefines.WEREWOLF ? Color.voice_room_bg_color : Color.voice_room_list_bg_color,
        paddingLeft: 20,
        paddingRight: 35
    },
    voiceBt: {
        backgroundColor: Color.transparent,
        justifyContent: 'center',
        alignSelf: 'center'
    }, voiceHome: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    voiceSearch: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        alignSelf: 'center',
        margin: 5
    },
    voiceHomeText: {
        fontSize: 15,
        color: Color.colorWhite,
        alignSelf: 'center',
        padding: 0,
        paddingBottom: 0
    }, actionButton: {
        backgroundColor: '#845fe4', justifyContent: 'center', height: 40, flex: 1, borderRadius: 5, margin: 10
    },
    loading: {
        position: 'absolute',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        height: 70,
        width: 110,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    dialogContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
    }, containerView: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: apiDefines.APP_KEY == apiDefines.WEREWOLF ? Color.voice_room_bg_color : Color.voice_room_list_bg_color,
    }, listViewStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginRight: 20,
        marginLeft: 20
    }, audioTypeIcon: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    cellBackStyle: {
        width: cellW,
        height: cellH,
        marginLeft: vMargin,
        marginTop: hMargin,
        alignSelf: 'center',
        justifyContent: 'center',
    }, rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: Color.colorWhite,
    }, roomNumber: {
        color: '#b09ce5',
        fontSize: 14,
        alignSelf: 'center',
        marginTop: 8
    }, passwordViewStyle: {
        backgroundColor: '#835EDF',
        borderColor: '#6939DA',
        borderWidth: 1,
        padding: 5,
        borderRadius: 3,
        width: 25,
        height: 35
    }, passwordTextStyle: {fontSize: 16, color: Color.colorWhite, fontWeight: 'bold', alignSelf: 'center'}
})
const mapStateToProps = state => ({
    configData: state.checkRegisterReducer.data,
    userInfo: state.userInfoReducer.data,
    voiceChildType: state.voiceRecreationReducer.voiceType,
    isTourist: state.userInfoReducer.isTourist,
});
const mapDispatchToProps = dispatch => ({
        updateConfig: () => dispatch(checkOpenRegister()),
        fetchRecreationPageListFromType: (page, actionType, type) => dispatch(fetchRecreationPageListFromType(page, actionType, type)),
        updateVoiceType: (type) => dispatch(updateVoiceType(type)),
    }
);
export default connect(mapStateToProps, mapDispatchToProps)(VoiceRoomManager);