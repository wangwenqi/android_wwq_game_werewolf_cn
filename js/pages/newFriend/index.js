/**
 * Created by wangxu on 2017/8/30.
 * 好友请求页面
 */
import React, {Component} from 'react';
import Language from '../../../resources/language'
import {Left, Right, Body, Text, CardItem, Button} from 'native-base';
import {
    StyleSheet,
    Image,
    ToastAndroid,
    Dimensions,
    View,
    Platform,
    NativeModules,
    ListView,
    RefreshControl,
    ActivityIndicator,
    InteractionManager,
    TouchableOpacity,
} from 'react-native';
import * as SCENES from '../../../support/actions/scene'
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import Toast from '../../../support/common/Toast';
import Dialog, {Alert,Confirm} from '../../../support/common/dialog';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import {removeChat} from '../../../support/actions/chatActions';
import CachedImage from '../../../support/common/CachedImage';
import {userInfo, updateTourist} from '../../../support/actions/userInfoActions';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import {
    fetchPageFriendList, clearFriendList
} from '../../../support/actions/friendActions';
import {
    clearMessageList,
    fetchMoreFriendRequest,
    fetchPageFriendRequestList,
    refreshPageFriendRequestList,
    removeFriendRequest,
} from '../../../support/actions/messageActions';
import {removeFriendStatus} from '../../../support/actions/friendStatusAction';
import Color from '../../../resources/themColor';
import LoadMoreFooter from '../../../support/common/LoadMoreFooter'
import * as Constant from '../../../support/common/constant';
import netWorkTool from '../../../support/common/netWorkTool';
import Loading from '../../../support/common/Loading';
import Util from '../../../support/common/utils';
import  * as apiDefines from '../../../support/common/gameApiDefines';
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const gender = require('../../../resources/imgs/ic_male.png');
const genderFemale = require('../../../resources/imgs/ic_female.png');
const defaultContactBg = require('../../../resources/imgs/bg_friend_request_blank.png');
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const headHeight = (Platform.OS === 'ios') ? 60 : 50;
const bgBack = require('../../../resources/imgs/ic_back.png');
const PersonalInfoActivity = 'com.game_werewolf.activity.PersonalInfoActivity';

const {
    pushRoute,
    popRoute
} = actions;
let needLoginDialog;
let friendRequest = new Array();
let progressDialog;
let that;
let selectUserData;
let clearFriendRequest;
class NewFriend extends Component {
    static propTypes = {
        navigator: React.PropTypes.shape({}),
        openPage: React.PropTypes.func,
        code: React.PropTypes.number,
        message: React.PropTypes.string,
        data: React.PropTypes.object,
        fetchRequestPageList: React.PropTypes.func,
        fetchMoreRequestList: React.PropTypes.func,
        fetchRefreshRequestList: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        fetchPageList: React.PropTypes.func,
        token: React.PropTypes.string,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        removeChatData: React.PropTypes.func,
        updateUserInfo: React.PropTypes.func,
        updateTourist: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        clearMessageList: React.PropTypes.func,
        removeFriendStatus: React.PropTypes.func,
        clearFriendList: React.PropTypes.func,
        isRefreshing: React.PropTypes.bool,
        isNoMore: React.PropTypes.bool,
    }

    constructor(props) {
        super(props);
        that = this;
        if (this.props.data != null) {
            friendRequest = this.props.data.list;
        }
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        //2.设置返回数据
        this.state = {
            dataSource: ds.cloneWithRows(friendRequest),
            userId: '',
            deleteIndex: 0,
            isFinishing: false,
        }
    }

    componentDidMount() {
        needLoginDialog = this.refs.login;
        progressDialog = this.refs.progressDialog;
        clearFriendRequest = this.refs.clearRequestFriendRequest;
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    componentWillMount() {
        if (this.props.data && this.props.data.list.length > 0) {
        } else {
            InteractionManager.runAfterInteractions(() => {
                this.props.fetchRequestPageList();
            });
        }
    }

    handleMethod(isConnected) {

    }

    /**
     *1-- 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     *1-- 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    onFinishPage() {
        if (!this.state.isFinishing) {
            this.state.isFinishing = true;
            this.props.finishPage('global');
        }
        this.isFinishingTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.code != 0 && this.props.code != 1000) {
            if (this.props.message != '' && this.props.message != null) {
                if (this.props.code != 1004) {
                } else {
                    this.checkToken();
                }
            }
        }
        return (!_.isEqual(this.props.data, nextProps.data));
    }

    /**
     * 渲染页面
     * @returns {XML}
     */
    render() {
        if (this.props.data && this.props.data.list) {
            friendRequest = this.props.data.list;
        }else {
            friendRequest=new Array();
        }
        return (
            <View style={{backgroundColor:Color.colorWhite,flex:1}}>
                <CardItem
                    style={{backgroundColor:(Color.headColor),height:headHeight,paddingTop:headPaddingTop,paddingBottom:0}}>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={this.onFinishPage.bind(this)}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf:'center',flex:3}}>
                    <Text style={{color: '#fff',fontSize: 18,alignSelf:'center'}}>{Language().new_friend}</Text>
                    </Body>
                    <Right style={{flex:1}}>
                        {friendRequest.length == 0 ? null : (
                                <Button transparent onPress={this.showClearAllFriendRequest.bind(this)}>
                                    <Text
                                        style={{color:Color.colorWhite,fontSize:14}}>{Language().clear_all_request}</Text>
                                </Button>)}
                    </Right>
                </CardItem>
                <View style={{backgroundColor:Color.colorWhite,flex:1}}>
                    {this.defaultView()}
                    <ListView dataSource={this.state.dataSource.cloneWithRows(friendRequest)}
                              ref="listView"
                              renderRow={this.renderRow}
                              renderFooter={this.renderFooter}
                              initialListSize={1}
                              removeClippedSubviews={true}
                              pageSize={6}
                              onScroll={this._onScroll}
                              onEndReached={this._onEndReach}
                              onEndReachedThreshold={10}
                              enableEmptySections={true}
                              refreshControl={
                                     <RefreshControl
                                        refreshing={this.props.isRefreshing}
                                         onRefresh={this._onRefresh}
                                         colors={[Color.list_loading_color]} />
                                    }/>
                </View>
                <Loading ref="progressDialog" loadingTitle={Language().operation_loading}/>
                {Dialog.inject()}
                <Alert ref='login' title={Language().title_login} message={Language().need_login_message}
                       messageStyle={{alignSelf:'center',fontSize: 14,marginBottom:10}}
                       titleStyle={{alignSelf:'center',fontSize: 17,padding: 5}}
                       contentStyle={{paddingLeft: 1,paddingBottom: 3,paddingRight: 1,width: ScreenWidth/3*2+30}}
                       buttonBarStyle={{justifyContent:'center'}}
                       buttonStyle={{textAlign: 'center',fontSize: 16,padding: 10,marginLeft: 0,marginRight: 0,alignSelf:'center',justifyContent:'center',flex:1,color:Color.system_dialog_bt_text_color}}
                       posText={Language().confirm}
                       rowSeparator={true}
                       onPosClick={this.login.bind(this)}> </Alert>
                <Confirm ref='clearRequestFriendRequest' title={Language().prompt}
                         posText={Language().confirm}
                         message={Language().prompt_clear_all_request}
                         messageStyle={{alignSelf:'center',fontSize: 14,marginBottom:10}}
                         titleStyle={{alignSelf:'center',fontSize: 17,padding: 5}}
                         contentStyle={{paddingLeft:1,paddingBottom: 3,paddingRight: 1,width: ScreenWidth/3*2+30}}
                         buttonBarStyle={{justifyContent:'center'}}
                         buttonStyle={{textAlign: 'center',fontSize: 16,padding: 10,marginLeft: 0,marginRight: 0,alignSelf:'center',justifyContent:'center',flex:1,color:Color.system_dialog_bt_text_color}}
                         negText={Language().cancel}
                         enumeSeparator={true}
                         rowSeparator={true}
                         onNegClick={this.dismissFriendRequestDialog.bind(this)}
                         onPosClick={this.clearAllFriend.bind(this)}>
                </Confirm>
            </View>
        );
    }

    /**
     *
     * @returns {*}
     */
    defaultView() {
        return friendRequest.length == 0 ? (
                <View
                    style={{justifyContent: 'center',backgroundColor:Color.colorWhite,position: 'absolute',top:0,bottom:0,left:0,right:0}}>
                    <Image style={styles.defaultBg} source={defaultContactBg}/>
                    <Text
                        style={{alignSelf:'center',padding:5,marginTop:15,color:'#9B9B9B',fontSize:13}}>{Language().no_friend_request}</Text>
                    <Text
                        style={{alignSelf:'center',padding:5,color:'#9B9B9B',fontSize:13}}>{Language().no_friend_request_content}</Text>
                </View>) : null;
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    renderRow(item, sectionId, rowId) {
        return (
            <View style={{overflow:'hidden'}}>
                <View style={{height: 65, marginLeft: -17, paddingLeft: 5,alignSelf:'flex-start'}}
                      flexDirection='row'>
                    <TouchableOpacity
                        style={{backgroundColor:(Color.transparent),justifyContent:'center',flex:1}}
                        activeOpacity={0.6}
                        onPress={()=>that.selectUserInfo(item)}>
                        <View style={{flex:1,alignSelf:'center',justifyContent:'center',marginLeft:15}}>
                            <CachedImage style={styles.headThumbnail}
                                         source={(item.image==''||item.image==null)?(require('../../../resources/imgs/ic_default_user.png')):({uri:item.image})}
                                         defaultSource={require('../../../resources/imgs/ic_default_user.png')}/>
                            <Image source={(item.sex==null||item.sex==1)?gender:genderFemale}
                                   style={styles.gender}></Image>
                        </View>

                    </TouchableOpacity>
                    <View
                        style={{flex:2,paddingBottom:0,paddingLeft:8,alignSelf:'center',justifyContent:'center'}}>
                        <Text numberOfLines={1} style={{color:'#313035',padding:2,fontSize:16}}>{item.name}</Text>
                        <Text numberOfLines={1}
                              style={{color: '#9B9B9B',padding:2,fontSize:14}}>{item.name + Language().request_add}</Text>
                    </View>
                    <Right style={{flex:2}}>
                        <View flexDirection='row' style={{justifyContent:'center'}}>
                            <TouchableOpacity
                                style={{borderRadius: 3,height:23,backgroundColor:'#56BC92',justifyContent:'center',width:50}}
                                activeOpacity={0.6}
                                onPress={()=>that.acceptAdd(item,rowId)}>
                                <Text
                                    style={{color:'#fff',fontSize:12,padding:5,alignSelf:'center'}}>{Language().accept}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{borderRadius: 3,height:23,backgroundColor:(Color.phoneLoginButColor),justifyContent:'center',width:50,marginLeft:10,marginRight:10}}
                                activeOpacity={0.6}
                                onPress={()=>that.refuseAdd(item,rowId)}>
                                <Text
                                    style={{color:'#fff',fontSize:12,padding:5,alignSelf:'center'}}>{Language().refuse}</Text>
                            </TouchableOpacity>
                        </View>
                    </Right>
                </View>
                <View style={styles.rowSeparator}></View>
            </View>);
    }

    selectUserInfo(item) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                if (item && item.id) {
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                    return;
                }
                if (Platform.OS === 'ios') {
                    var main = NativeModules.MainViewController;
                    let sex = '2';
                    if (item.sex == 1) {
                        sex = '1';
                    }
                    let image = item.image;
                    if (image == null) {
                        image = '';
                    }
                    main.didSelectUser(item.id, 'detail', sex, image, item.name, '');
                } else {
                    NativeModules.NativeJSModule.rnStartPersonalInfoActivtiy(PersonalInfoActivity, item.id, this.props.token, false);
                }
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 刷新
     * @private
     */
    _onRefresh = () => {
        this.props.isRefreshing = true;
        InteractionManager.runAfterInteractions(() => {
            this.props.fetchRefreshRequestList();
        });
    };
    /**
     * load more
     * @private
     */
    _onEndReach = () => {
        let data = this.props.data;
        if (data) {
            if (data.page < data.total_page) {
                InteractionManager.runAfterInteractions(() => {
                    this.props.fetchMoreRequestList(data.page + 1);
                });
            } else {
                InteractionManager.runAfterInteractions(() => {
                    this.props.fetchMoreRequestList(data.page);
                });
            }
        }
    }

    onShowLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.show();
        }
    }

    onHideLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.hide();
        }
    }

    refuseAdd(userData, rowId) {
        that.state.userId = userData.id;
        that.state.deleteIndex = rowId;
        selectUserData = userData;
        that.actionRequest(apiDefines.REJECT_FRIEND, 'refuse');
    }

    acceptAdd(userData, rowId) {
        that.state.userId = userData.id;
        that.state.deleteIndex = rowId;
        selectUserData = userData;
        that.actionRequest(apiDefines.ACCEPT_FRIEND, 'accept');
    }

    actionRequest(requestUrl, type) {
        try {
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    that.onShowProgressDialog();
                    InteractionManager.runAfterInteractions(() => {
                        let url = requestUrl + that.state.userId;
                        Util.get(url, (code, message, data) => {
                            that.onHideProgressDialog();
                            if (code == 1000) {
                                that.props.removeFriendRequest(this.state.userId, this.state.deleteIndex);
                                if (type == 'accept') {
                                    that.sendMessageToNative();
                                }
                            }
                            Toast.show(message);
                        }, (failed) => {
                            that.onHideProgressDialog();
                            Toast.show(Language().not_network);
                        });
                    });
                } else {
                    that.onHideProgressDialog();
                    Toast.show(Language().not_network);
                }
            });
        } catch (err) {

        }
    }

    /**
     *
     *添加好友成功后，发消息
     */
    sendMessageToNative() {
        let user = selectUserData;
        if (user != null) {
            let sex = user.sex;
            if (user.sex == null) {
                sex = 2;
            }
            if (Platform.OS === 'android') {
                try {
                    NativeModules.NativeJSModule.sendTextWhenAgreeFriend(user.id, user.name, sex, user.image);
                } catch (e) {
                }
            } else {
                try {
                    var main = NativeModules.MainViewController;
                    if (main && main.sendUserWelcomeMessage) {
                        main.sendUserWelcomeMessage(user.id, user.name);
                    }
                } catch (e) {
                }
            }
        }
        selectUserData = null;
    }

    /**
     * 加载更多
     * @private
     */
    renderFooter() {
        return friendRequest.length < 11 ? null : (<LoadMoreFooter isNoMore={that.props.isNoMore}/>);
    }

    /**
     * 返回去登录
     */
    login() {
        const routes = this.props.navigation.routes;
        if (routes.length > 1) {
            for (var i = 0; i < routes.length; i++) {
                this.props.finishPage('global');
            }
        }
        if (this.props.userInfo != null && this.props.userInfo.id != null) {
            let userId = this.props.userInfo.id;
            rnRoNativeUtils.closeLeanCloud(userId);
        }
        AsyncStorageTool.removeUserInfo();
        try {
            this.props.openPage(SCENES.SCENE_SPLASH, 'global');
        } catch (e) {
            console.log(e)
        }
        AsyncStorageTool.removeSignTourist();
        this.props.removeChatData();
        this.props.updateUserInfo(null);
        this.props.updateTourist(false);
        this.props.clearFriendList();
        this.props.clearMessageList();
        this.props.removeFriendStatus();
        this.onHideLoginDialog();

    }

    /**
     * 检测token是否失效
     */
    checkToken() {
        if (this.props.code == 1004) {
            //token 失效
            try {
                this.onShowLoginDialog();
            } catch (error) {
                console.log('token error show:' + error);
            }
        }
    }

    /**
     *
     */
    dismissFriendRequestDialog() {
        if (clearFriendRequest) {
            clearFriendRequest.hide();
        }
    }

    /**
     *
     */
    clearAllFriend() {
        try {
            that.dismissFriendRequestDialog();
            that.onShowProgressDialog();
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    InteractionManager.runAfterInteractions(() => {
                        Util.get(apiDefines.CLEAR_REQUEST_FRIEND, (code, message, data) => {
                            that.onHideProgressDialog();
                            if (code == 1000) {
                                that.props.clearMessageList();
                            }
                            Toast.show(message);
                        }, (failed) => {
                            that.onHideProgressDialog();
                            Toast.show(Language().not_network);
                        });
                    });
                } else {
                    that.onHideProgressDialog();
                    Toast.show(Language().not_network);
                }
            });
        } catch (err) {

        }
    }

    /**
     *
     */
    showClearAllFriendRequest() {
        if (clearFriendRequest) {
            clearFriendRequest.show();
        }
    }
}

const
    styles = StyleSheet.create({
        headThumbnail: {
            width: 50,
            height: 50,
            borderRadius: 25,
            alignSelf: 'center',
        },
        nameTextStyle: {
            color: '#313035',
        },
        messageTextStyle: {
            color: '#9B9B9B'
        },
        gender: {
            alignSelf: 'center',
            width: 15,
            height: 15,
            position: 'absolute',
            left: 20,
            bottom: 5,
        },
        defaultBg: {
            alignSelf: 'center',
            width: ScreenWidth / 2,
            height: ScreenWidth / 2,
            resizeMode: 'contain',
        },
        rowImage: {
            position: 'absolute',
            left: 0,
            top: 0,
            bottom: 0,
            right: 0,
            alignSelf: 'center',
            justifyContent: 'center'
        },
        rowSeparator: {
            height: StyleSheet.hairlineWidth,
            backgroundColor: 'lightgray',
        },
        rowBack: {
            alignItems: 'center',
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingLeft: 15,
        },
        backRightBtn: {
            alignItems: 'center',
            bottom: 0,
            justifyContent: 'center',
            position: 'absolute',
            top: 0,
            width: 75
        },
        backRightBtnRight: {
            backgroundColor: Color.delete_bg_color,
            right: 0
        },
        bgBack: {
            width: 20,
            height: 20,
            resizeMode: 'contain',
            padding: 5
        },
    });
const
    mapDispatchToProps = dispatch => ({
            openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
            finishPage: (key) => dispatch(popRoute(key)),
            removeChatData: () => dispatch(removeChat()),
            updateUserInfo: (data) => dispatch((userInfo(data))),
            updateTourist: (isTourist) => dispatch((updateTourist(isTourist))),
            clearMessageList: () => dispatch(clearMessageList()),
            removeFriendStatus: () => dispatch(removeFriendStatus()),
            clearFriendList: () => dispatch(clearFriendList()),
            fetchRequestPageList: () => dispatch(fetchPageFriendRequestList()),
            fetchMoreRequestList: (page) => dispatch(fetchMoreFriendRequest(page)),
            fetchRefreshRequestList: () => dispatch(refreshPageFriendRequestList()),
            removeFriendRequest: (userId, index) => dispatch(removeFriendRequest(userId, index)),
            fetchPageList: () => dispatch(fetchPageFriendList()),
        }
    );
const
    mapStateToProps = state => ({
        code: state.messageReducer.code,
        message: state.messageReducer.message,
        data: state.messageReducer.data,
        navigation: state.cardNavigation,
        userInfo: state.userInfoReducer.data,
        isRefreshing: state.messageReducer.isRefreshing,
        isNoMore: state.messageReducer.isNoMore,
    });
export default connect(mapStateToProps, mapDispatchToProps)(NewFriend);