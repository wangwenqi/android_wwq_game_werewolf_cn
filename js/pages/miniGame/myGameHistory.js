/**
 * Created by wangxu on 2018/1/18.
 * 我的对战
 */
'use strict';
import React, {Component, Navigator} from 'react';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Image,
    StyleSheet,
    View,
    Platform,
    Dimensions,
    TouchableOpacity,
    ListView,
    Text,
    RefreshControl, InteractionManager
} from 'react-native';
import {gameHistoryList, isRefreshGameHistory} from '../../../support/actions/miniGameActions';
import LoadImageView from '../../../support/common/LoadImageView';
import rnToNativeUtils from '../../../support/common/rnToNativeUtils';
import CachedImage from '../../../support/common/CachedImage';
import ImageManager from '../../../resources/imageManager';
import LoadMoreFooter from '../../../support/common/LoadMoreFooter'
import * as types from '../../../support/actions/actionTypes';
import netWorkTool from '../../../support/common/netWorkTool';
import Language from '../../../resources/language';

import Toast from '../../../support/common/Toast';
import Color from '../../../resources/themColor';
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
let gameHistoryListData = new Array();
class MyGameHistory extends Component {
    static propTypes = {
        isRefreshing: React.PropTypes.bool,
        data: React.PropTypes.object,
        userInfo: React.PropTypes.object,
        isNoMore: React.PropTypes.bool,
        fetchGameHistoryPageList: React.PropTypes.func,
        isRefreshGameHistory: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataGameHistoryListSource: ds.cloneWithRows(gameHistoryListData),
        };
    }

    componentDidMount() {
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.checkNewWork();
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (!_.isEqual(this.props.data, nextProps.data));
    }

    componentWillMount() {
        this.props.isRefreshGameHistory();
        InteractionManager.runAfterInteractions(() => {
            this.props.fetchGameHistoryPageList(types.GET_GAME_HISTORY_LIST_DATA, 0);
        });
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    render() {
        if (this.props.data && this.props.data.data) {
            gameHistoryListData = this.props.data.data;
           // this.sortListData();
        } else {
            gameHistoryListData = new Array();
        }
        return (<View style={{flex:1,backgroundColor:Color.colorWhite}}>
            <View style={{height:7,backgroundColor:'#d9d9d9',marginTop:10}}/>
            <View style={{backgroundColor:'#f2f2f2',flex:1}}>
                <ListView dataSource={this.state.dataGameHistoryListSource.cloneWithRows(gameHistoryListData)}
                          ref="listView"
                          renderRow={this.renderRow.bind(this)}
                          renderFooter={this.renderFooter.bind(this)}
                          initialListSize={1}
                          scrollRenderAheadDistance={50}
                          removeClippedSubviews={true}
                          pageSize={6}
                          onEndReached={this._onEndReach.bind(this)}
                          onEndReachedThreshold={10}
                          enableEmptySections={true}
                          refreshControl={
                                     <RefreshControl
                                         refreshing={this.props.isRefreshing}
                                         onRefresh={this._onRefresh.bind(this)}
                                         colors={[Color.list_loading_color]} />
                                    }
                          contentContainerStyle={styles.gameHistoryListViewStyle}
                          closeOnRowBeginSwipe={true}/>
            </View>
            {this._defaultView()}
        </View>);
    }

    /**
     *排序
     */
    sortListData = () => {
        try {
            if (gameHistoryListData) {
                if (gameHistoryListData.length > 0) {
                    gameHistoryListData.sort(function (x, y) {
                        return x.last_played_at > y.last_played_at ? -1 : 1;
                    });
                }
            }
        } catch (e) {

        }
    }

    /**
     * 默认背景
     * @param height
     * @returns {*}
     * @private
     */
    _defaultView() {
        return gameHistoryListData.length == 0 ? (<View
                style={{justifyContent: 'center',position: 'absolute',flex:1,left:0,top:0,right:0,bottom:0}}>
                <CachedImage style={styles.defaultBg} source={ImageManager.ic_mini_game_no_data}/>
            </View>) : null;
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     */
    renderRow(item, sectionId, rowId) {
        let name = '';
        let image;
        let user;
        let time = 0;
        if (item) {
            time = item.last_played_at;
            if (item.oppo) {
                user = item.oppo;
                if (user.name) {
                    name = user.name;
                }
                if (user.image) {
                    image = user.image;
                }
            }
        }
        return (<View style={{overflow: 'hidden',marginLeft:10,marginTop:20}}>
            <View>
                <LoadImageView source={image?{uri:image}:ImageManager.defaultUser}
                               style={styles.userHeaderStyle}
                               defaultSource={ImageManager.defaultUser}/>
                <View style={{flex:1,justifyContent:'center',paddingLeft:10,paddingRight:10}}>
                </View>
                <View
                    style={{position: 'absolute',justifyContent:'center',borderRadius:7,padding:1,backgroundColor:'rgba(0, 0, 0, 0.9)',right:6,top:5,paddingRight:10,paddingLeft:10}}>
                    <Text numberOfLines={1}
                          style={{fontSize:14,alignSelf:'center',color:Color.colorWhite,backgroundColor:Color.transparent}}>{this._onDateFormat(time)}</Text>
                </View>
                <View
                    style={{position: 'absolute',justifyContent:'center',borderRadius:6,backgroundColor:'rgba(255, 255, 255, 0.9)',bottom:5,padding:1,paddingLeft:5,paddingRight:5,left:5,marginRight:8}}>
                    <Text numberOfLines={1}
                          style={{fontSize:12,alignSelf:'center',backgroundColor:Color.transparent,color:'#44155F'}}>{name}</Text>
                </View>
                <TouchableOpacity
                    style={[styles.buttonStyle,{width: (ScreenWidth - 40) / 3, height: (ScreenWidth - 40) / 3,backgroundColor:Color.transparent,position: 'absolute'}]}
                    activeOpacity={0.7} onPress={()=>this.continueGame(user)}/>
            </View>
            {/*<View style={{justifyContent:'center',marginTop:10,marginBottom:6}}>*/}
            {/*<TouchableOpacity style={styles.buttonStyle}*/}
            {/*activeOpacity={0.7} onPress={()=>this.continueGame(user)}>*/}
            {/*<Text style={{color:Color.colorWhite,fontSize:14}}>{Language().continue_mini_game}</Text>*/}
            {/*</TouchableOpacity>*/}
            {/*</View>*/}
        </View>);
    }

    /**
     *
     * @param user
     */
    continueGame(user) {
        let userInfo = this.props.userInfo;
        let sex = 1;
        let image;
        let name;
        let id;
        if (userInfo) {
            sex = userInfo.sex;
            image = userInfo.image;
            name = userInfo.name;
            id = userInfo.id;
        }
        let rightData = {
            image: image,
            sex: sex,
            name: name,
            id: id
        };
        rnToNativeUtils.openChatMessage(user, rightData);
    }

    /**
     *
     * @private
     */
    _onEndReach() {
        let skip = 0;
        let limit = 20;
        if (this.props.data) {
            if (this.props.data.skip) {
                skip = this.props.data.skip;
            }
            if (this.props.data.limit) {
                limit = this.props.data.limit;
            }
            InteractionManager.runAfterInteractions(() => {
                this.props.fetchGameHistoryPageList(types.GET_GAME_HISTORY_LIST_DATA_MORE, skip + limit);
            });
        }
    }

    /**
     *
     * @private
     */
    _onRefresh() {
        this.checkNewWork();
        this.props.isRefreshGameHistory();
        InteractionManager.runAfterInteractions(() => {
            this.props.fetchGameHistoryPageList(types.GET_GAME_HISTORY_LIST_DATA, 0);
        });
    }

    /**
     * 加载更多
     * @private
     */
    renderFooter() {
        return gameHistoryListData.length < 15 ? null : (<LoadMoreFooter isNoMore={this.props.isNoMore}/>);
    }

    /**
     *
     * @param isConnected
     */
    handleMethod(isConnected) {
    }

    /**
     *
     */
    checkNewWork() {
        netWorkTool.checkNetworkState((isConnected) => {
            if (!isConnected) {
                Toast.show(Language().load_more_data_error);
            }
        });
    }

    /**
     * 时间格式化
     * 最多显示15天内，其他不显示
     * @param time
     * @private
     */
    _onDateFormat(time) {
        let formatTime = '';
        try {
            let statusDate = new Date((time - 0));
            let newDate = new Date();
            let hour = newDate.getHours() - statusDate.getHours();
            let day = newDate.getDate() - statusDate.getDate();
            let year = newDate.getFullYear() - statusDate.getFullYear();
            let month = newDate.getMonth() - statusDate.getMonth();
            if (year == 0) {
                if (month == 0) {
                    if (day > 3) {
                        //三天前
                        formatTime = 3 + Language().day;
                    } else {
                        if ((day > 0 && day <= 3) || hour == 24) {
                            formatTime = day + Language().day;
                        } else if (hour == 0) {
                            formatTime = Language().just_now;
                        } else if (hour > 0 && hour < 24) {
                            formatTime = hour + Language().hour;
                        } else {
                            //三天前
                            formatTime = 3 + Language().day;
                        }
                    }
                } else {
                    //三天前
                    formatTime = 3 + Language().day;
                }
            } else {
                //三天前
                formatTime = 3 + Language().day;
            }
            return formatTime;
        } catch (e) {
            return formatTime;
        }
        return formatTime;
    }
}
const styles = StyleSheet.create({
    rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: Color.rowSeparator_line_color,
    }, userHeaderStyle: {
        width: (ScreenWidth - 40) / 3, height: (ScreenWidth - 40) / 3, borderRadius: 12, alignSelf: 'center'
    }, itemViewStyle: {overflow: 'hidden', marginLeft: 10, marginTop: 10},
    buttonStyle: {
        justifyContent: 'center',
        borderRadius: 8,
        backgroundColor: '#FF912C',
        alignSelf: 'center',
        padding: 15,
        paddingTop: 5,
        paddingBottom: 5
    }, gameHistoryListViewStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        width: ScreenWidth,
    }, defaultBg: {
        alignSelf: 'center',
        width: ScreenWidth / 3 + 30,
        height: ScreenWidth / 3 + 30,
        resizeMode: 'contain',
    },
});
const mapDispatchToProps = dispatch => ({
        fetchGameHistoryPageList: (actionType, page) => dispatch(gameHistoryList(actionType, page)),
        isRefreshGameHistory: () => dispatch(isRefreshGameHistory())
    }
);
const mapStateToProps = state => ({
    isNoMore: state.gameHistoryListReducer.isNoMore,
    isRefreshing: state.gameHistoryListReducer.isRefreshing,
    data: state.gameHistoryListReducer.data,
    userInfo: state.userInfoReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps)(MyGameHistory);