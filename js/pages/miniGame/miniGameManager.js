/**
 * Created by wangxu on 2018/1/18.
 * 小游戏管理
 */
import React, {Component} from 'react';
import {StyleSheet, Platform} from 'react-native';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';

import Language from '../../../resources/language';
import Color from '../../../resources/themColor';
import MiniGameList from '../../pages/miniGame/miniGameList';
import MyGameHistory from '../../pages/miniGame/myGameHistory';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import MiniGameTabBar from '../../../support/common/MiniGameTabBar';
const tabNames = [Language().matching, Language().game_history];
class MiniGameManager extends Component {
    static propTypes = {}

    constructor() {
        super();
        {/*默认显示首页*/
        }
        this.state = {index: 0};
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        return <ScrollableTabView
            style={styles.tabView}
            renderTabBar={() => <MiniGameTabBar tabNames={tabNames}/>}
            tabBarPosition='top'
            scrollWithoutAnimation={false}
            locked={true}
            onChangeTab={(data)=>this.changeMessageTab(data)}>
            <MiniGameList tabLabel="MiniGameList"/>
            <MyGameHistory tabLabel="MyGameHistory"/>
        </ScrollableTabView>;
    }

    componentWillMount() {
        //
    }

    /**
     * 1--当tab改变时
     * @param data
     */
    changeMessageTab(data) {

    }
}
const styles = StyleSheet.create({
    tabView: {
        backgroundColor: Color.colorWhite,
        paddingTop: (Platform.OS === 'ios') ? 14 : 0
    }

})
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps,)(MiniGameManager);