/**
 * Created by wangxu on 2018/1/18.
 * 匹配对手
 * 支持游戏列表
 */
'use strict';
import React, {Component, Navigator} from 'react';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    StyleSheet,
    View,
    Platform,
    Dimensions,
    TouchableOpacity,
    InteractionManager,
    ListView,
    Text, Image, DeviceEventEmitter,NativeModules,NativeEventEmitter,
} from 'react-native';
import Language from '../../../resources/language';
import {supportGameList} from '../../../support/actions/miniGameActions';
import LoadImageView from '../../../support/common/LoadImageView';
import rnToNativeUtils from '../../../support/common/rnToNativeUtils';
import ImageManager from '../../../resources/imageManager';
import Toast from '../../../support/common/Toast';
import Color from '../../../resources/themColor';
import * as gameApiDefines from '../../../support/common/gameApiDefines';
import * as Constant from '../../../support/common/constant';
import Util from '../../../support/common/utils';

const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const RANDOM_MINI_GAME = 'random_mini_game';
const {RNMessageSender}=NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
let gameList = new Array();
class MiniGameList extends Component {
    static propTypes = {
        gameListArray: React.PropTypes.array,
        updateSupportGame: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataGameListSource: ds.cloneWithRows(gameList),
            selectModelType: RANDOM_MINI_GAME
        };
    }

    componentDidMount() {
        if (Platform.OS === 'android') {
            this.onNativeMessageLister = DeviceEventEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
        } else {
            this.onNativeMessageLister = rNMessageSenderEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
        }
    }

    componentWillMount() {
        InteractionManager.runAfterInteractions(() => {
            this.props.updateSupportGame();
        });
    }

    componentWillUnmount() {
        this.onNativeMessageLister.remove();
    }

    render() {
        if (this.props.gameListArray) {
            gameList = this.props.gameListArray;
        }
        return (<View style={{flex:1}}>
            <LoadImageView source={ImageManager.ic_mini_game_list_bg}
                           style={{width:ScreenWidth,height:ScreenHeight,position: 'absolute'}}/>
            <View style={{flex:1,justifyContent:'center',marginTop:20,marginBottom:20}}>
                <ListView dataSource={this.state.dataGameListSource.cloneWithRows(gameList)}
                          showsVerticalScrollIndicator={false}
                          renderRow={this.renderRow.bind(this)}
                          initialListSize={1}
                          pageSize={6}
                          removeClippedSubviews={false}
                          contentContainerStyle={styles.listViewStyle}/>
            </View>
            <View style={{position: 'absolute',width:ScreenWidth,left:0,right:0,bottom:0}}>
                <LoadImageView source={ImageManager.ic_mini_game_button_bg}
                               style={{width:ScreenWidth,height:ScreenWidth*0.216,resizeMode: 'stretch'}}/>
                <View flexDirection='row' style={{position: 'absolute',top:10,justifyContent:'center',left:0,right:0}}>
                    <TouchableOpacity
                        style={{justifyContent:'center'}}
                        activeOpacity={0.6} onPress={()=>this.intviuGame()}>
                        <Image source={ImageManager.ic_intive_mini_game}
                               style={{width:ScreenWidth*0.216*0.5*2.4,height:ScreenWidth*0.216*0.5,justifyContent:'center'}}>
                            <Text
                                style={[styles.gameActionTextStyle,{color:Color.blackColor}]}>{Language().invite_game_for_fb}</Text>
                        </Image>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{justifyContent:'center',marginLeft:50}}
                        activeOpacity={0.6} onPress={()=>this.intviuGameContact()}>
                        <Image source={ImageManager.ic_pk_mini_game}
                               style={{width:ScreenWidth*0.216*0.5*2.4,height:ScreenWidth*0.216*0.5,justifyContent:'center'}}>
                            <Text style={styles.gameActionTextStyle}>{Language().invite_game_contact}</Text>
                        </Image>
                    </TouchableOpacity>
                </View>

            </View>
        </View>);
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     */
    renderRow(item, sectionId, rowId) {
        return (<View
            style={{width:(ScreenWidth-110)/3,margin:10,height:(ScreenWidth-110)/3+30,marginTop:5,marginBottom:5,alignSelf:'flex-start'}}>
            <TouchableOpacity style={{justifyContent:'center',alignSelf:'center'}}
                              activeOpacity={0.5} onPress={()=>this.selectGameModel(item)}>
                <View style={{justifyContent:'center',width:(ScreenWidth-110)/3,height:(ScreenWidth-110)/3}}>
                    {item.type == RANDOM_MINI_GAME ? ( <LoadImageView source={ImageManager.ic_default_mini_game_}
                                                                      style={styles.gameCoverStyle}/>) : (
                        <LoadImageView source={item.icon?{uri:item.icon}:ImageManager.ic_default_mini_game_bg}
                                       type="miniGame"
                                       style={styles.gameCoverStyle}
                                       defaultSource={ImageManager.ic_default_mini_game_bg}/>)}
                </View>

                <Text style={styles.gameTitleStyle}>{item.name}</Text>
            </TouchableOpacity>
            {/*{this.selectView(item)}*/}
        </View>)
    }

    /**
     *
     * @param item
     */
    selectView(item) {
        return this.state.selectModelType == item.type ? (<LoadImageView
            source={ImageManager.ic_selected_mini_game}
            style={[{position: 'absolute',width:(ScreenWidth-70)/3, height: (ScreenWidth-70)/3,top:-6,left:-6}]}/>) : null

    }

    /**
     *进行匹配
     */
    intviuGameContact = () => {
        let NativeData = {
            action: 'ACTION_INVITE_FRIENDS_MINI_GAME',
            options: null
        }
        rnToNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }

    /**
     *小游戏邀请
     */
    intviuGame() {
        rnToNativeUtils.rnStartMiniGameIntviu(Language().invite_mini_game_title, Language().invite_mini_game_message, gameApiDefines.URL_HOST + gameApiDefines.SHARE_MINI_GAME);
    }

    /**
     *
     * @param item
     */
    selectGameModel(item) {
        // this.setState({
        //     selectModelType: item.type,
        //     dataGameListSource: this.state.dataGameListSource.cloneWithRows(gameList)
        // });
        rnToNativeUtils.rnStartMiniGame(item.type);
    }

    /**
     * 统一处理native的消息
     * @param event
     */
    nativeMessage(event) {
        if (event && event.Data) {
            let data = JSON.parse(event.Data);
            switch (data.action) {
                case Constant.ACTION_REQUEST_FRIENDS_LIST_MINI_GAME:
                    if (data.options && data.options.needcallback) {
                        //修改为请求接口
                        try {
                            let url = gameApiDefines.GET_FRIEND_STATUSLIST + '2,1' + gameApiDefines.PARAMETER_WITHINFO + 1;
                            Util.get(url, (code, message, data) => {
                                if (data) {
                                    this.sendListToNative(data.data);
                                    console.log('ACTION_REQUEST_FRIENDS_LIST_MINI_GAME:' + JSON.stringify(data.data))
                                } else {
                                    this.sendListToNative(null);
                                }
                            }, (failed) => {
                                this.sendListToNative(null);
                            });
                        } catch (e) {
                            console.log('error' + e);
                        }
                    }
                    break;
            }
        }
    }

    /**
     * 给native传在线好友列表
     * @param data
     */
    sendListToNative = (data) => {
        let newData = {
            list: data
        }
        let Options = {
            needcallback: false,
            sync: true
        }
        let NativeData = {
            action: Constant.ACTION_REQUEST_FRIENDS_LIST_MINI_GAME,
            params: newData,
            options: Options
        }
        rnToNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }

}
const styles = StyleSheet.create({
    listViewStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignSelf: 'center',
        alignItems: 'center',
    }, gameCoverStyle: {
        width: (ScreenWidth - 110) / 3, height: (ScreenWidth - 110) / 3, resizeMode: 'contain',
    }, gameTitleStyle: {
        alignSelf: 'center', marginTop: 6, fontSize: 14, color: Color.blackColor
    }, gameActionTextStyle: {
        alignSelf: 'center',
        fontSize: 14,
        color: Color.colorWhite,
        backgroundColor: Color.transparent
    },
});
const mapDispatchToProps = dispatch => ({
        updateSupportGame: () => dispatch(supportGameList()),
    }
);
const mapStateToProps = state => ({
    gameListArray: state.gameListReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps)(MiniGameList);