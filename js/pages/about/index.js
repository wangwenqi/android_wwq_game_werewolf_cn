/**
 * Created by wangxu on 2017/6/9.
 * 应用详情
 */
'use strict';
import React, {Component} from 'react';
import Language from '../../../resources/language';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Container,
    Button,
    Left,
    Text,
    Right,
    Content,
    CardItem,
    ListItem,
    Body
} from 'native-base';
import {Image, StyleSheet, View, Platform, Dimensions, TouchableOpacity} from 'react-native';
import Color from '../../../resources/themColor';
import * as SCENES from '../../../support/actions/scene';
import Toast from '../../../support/common/Toast';
import * as Constant from '../../../support/common/constant';
import * as gameApiDefines from '../../../support/common/gameApiDefines';
import  ImageManager from '../../../resources/imageManager';

const {
    popRoute,
    pushRoute,
} = actions;
const bgBack = require('../../../resources/imgs/ic_back.png');
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const ScreenHeight = Dimensions.get('window').height;
const rightArrow = require('../../../resources/imgs/bg_arrow_right.png');
const company_wx = gameApiDefines.APP_KEY == gameApiDefines.WEREWOLF ? Language().game_werewolf : Language().xiao_yu;
const company_fb = gameApiDefines.APP_KEY == gameApiDefines.WEREWOLF ? '@orangelabcn' : Language().xiao_yu;
const company_qq = gameApiDefines.APP_KEY == gameApiDefines.WEREWOLF ? '387107568' : '632083662';
class About extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        openPage: React.PropTypes.func,
        versionName: React.PropTypes.string
    }

    constructor(props) {
        super(props);
        this.state = {
            developer: 1,
            openDeveloper: false
        }
    }

    render() {
        return (
            <Container style={{flex:1,backgroundColor:Color.content_color}}>
                <CardItem
                    style={{backgroundColor:(Color.headColor),height:60,paddingTop:headPaddingTop,paddingBottom:0}}>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={this.onFinishPage.bind(this)}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf:'center',flex:3}}>
                    <Text
                        style={{color: Color.colorWhite,fontSize: 18,alignSelf:'center'}}>{Language().about}</Text>
                    </Body>
                    <Right style={{flex:1}}/>
                </CardItem>
                <Container style={{flex:1,justifyContent:'center'}}>
                    <Image
                        source={ImageManager.launcherWerewolf}
                        style={styles.launcher}></Image>
                    <Text
                        onPress={this.developer.bind(this)}
                        style={{color:Color.about_text_color,fontSize: 14,alignSelf:'center',padding:8}}>{Language().version + this.props.versionName}</Text>
                </Container>
                <Container style={{flex:1.5}}>
                    <View style={{backgroundColor:Color.colorWhite}}>
                        <View style={styles.rowSeparator}></View>
                        <ListItem style={{height:45}}>
                            <Left>
                                <Text
                                    style={{color:Color.about_text_color,fontSize: 16,alignSelf:'flex-start'}}>{Language().company_qq}</Text>
                            </Left>
                            <Right style={{flex:1}}>
                                <Text
                                    style={{color:Color.setting_text_color,fontSize: 16,alignSelf:'flex-end'}}>632083662</Text>
                            </Right>
                        </ListItem>
                        <ListItem style={{height:45}}>
                            <Left>
                                <Text
                                    style={{color:Color.about_text_color,fontSize: 16,alignSelf:'flex-start'}}>{Language().company_wx}</Text>
                            </Left>
                            <Right style={{flex:1}}>
                                <Text
                                    style={{color:Color.setting_text_color,fontSize: 16,alignSelf:'flex-end'}}>{Language().mini_game_app_name}</Text>
                            </Right>
                        </ListItem>
                        <ListItem style={{height:45}}>
                            <Left>
                                <Text
                                    style={{color:Color.about_text_color,fontSize: 16,alignSelf:'flex-start'}}>{Language().company_email}</Text>
                            </Left>
                            <Right style={{flex:2}}>
                                <Text
                                    style={{color:Color.setting_text_color,fontSize: 16,alignSelf:'flex-end'}}>support@orangelab.cn</Text>
                            </Right>
                        </ListItem>
                        <View style={styles.rowSeparator}></View>
                    </View>
                </Container>
                <Container style={{flex:1,justifyContent:'flex-end'}}>
                    {this._developerView()}
                    <TouchableOpacity onPress={this.readUserAgreement.bind(this)}>
                        <Text
                            style={{color:((Color.phoneLoginButColor)),fontSize:16,alignSelf:'center',padding:5}}>{'《' + Language().user_agreement + '》'}</Text>
                    </TouchableOpacity>
                    {/*<Text*/}
                        {/*style={{color:Color.about_text_color,fontSize: 14,alignSelf:'center',padding:5,marginBottom:10,paddingLeft:20,paddingRight:20,textAlign:'center'}}>{Language().company_copyright}</Text>*/}
                   <Text
                    style={{color:Color.about_text_color,fontSize: 14,alignSelf:'center',padding:5,marginBottom:10,paddingLeft:20,paddingRight:20,textAlign:'center'}}>
                       北京心橙互联信息技术有限公司copyright
                   </Text>

                </Container>
            </Container>
        );
    }

    onFinishPage() {
        this.props.finishPage('global');
    }

    /**
     * 用户协议
     */
    readUserAgreement() {
        this.props.openPage(SCENES.SCENE_USER_AGREEMENT, 'global');
    }

    /**
     * 开发者开启
     */
    developer() {
        if (this.state.developer == 5) {
            this.setState({
                openDeveloper: true
            })
        } else if (this.state.developer < 6) {
            this.state.developer = this.state.developer + 1;
        }
    }

    /**
     * 开发者模式view
     * @private
     */
    _developerView() {
        return this.state.openDeveloper ? <View>
                <View style={styles.rowSeparator}></View>
                <TouchableOpacity onPress={this.developerSetting.bind(this)}
                                  style={{height:40,backgroundColor:Color.colorWhite,justifyContent:'center'}}
                                  activeOpacity={0.6}>
                    <View flexDirection='row' style={{marginLeft:20,marginRight:20,alignSelf:'center'}}>
                        <Left>
                            <Text
                                style={{color:Color.about_text_color,fontSize: 16,alignSelf:'flex-start'}}>开发者模式</Text>
                        </Left>
                        <Right style={{flex:1}}>
                            <Image source={rightArrow}
                                   style={styles.rightArrow}></Image>
                        </Right>
                    </View>
                </TouchableOpacity>
                <View style={[styles.rowSeparator,{marginBottom:20}]}></View>
            </View> : null
    }

    /**
     * 进入开发者设置页面
     */
    developerSetting() {
        this.props.openPage(SCENES.SCENE_DEVELOPER, 'global');
    }
}

const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    launcher: {
        width: 80,
        height: 80,
        alignSelf: 'center'
    },
    rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: 'lightgray',
    },
    rightArrow: {
        width: 13, height: 13, resizeMode: 'contain'
    },
});
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
    }
);
const mapStateToProps = state => ({
    versionName: state.configReducer.version_name,
});
export default connect(mapStateToProps, mapDispatchToProps,)(About);