/**
 * Created by wangxu on 2017/9/22.
 */
import React, {Component} from 'react';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {StyleSheet, Dimensions, View, Platform, Image} from 'react-native';
import {Left, Right, Body, Text, CardItem, Button} from 'native-base';
import Language from '../../../resources/language';
import Color from '../../../resources/themColor';
import Recreation from '../../pages/recreation';
import RecreationPageList from '../../pages/recreation/recreation_page_list';
const {
    pushRoute,
    popRoute
} = actions;
const bgBack = require('../../../resources/imgs/ic_back.png');
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const headHeight = (Platform.OS === 'ios') ? 60 : 50;
class VoiceRoom extends Component {
    static propTypes = {
        openPage: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        configData: React.PropTypes.object
    }

    constructor(props) {
        super(props);
        this.state = {
            isFinishing: false,
        }
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        this.isFinishingTimer && clearTimeout(this.isFinishingTimer);
    }

    componentWillMount() {

    }

    /**
     * 渲染页面
     * @returns {XML}
     */
    render() {
        return (
            <View style={{backgroundColor:Color.colorWhite,flex:1}}>
                <CardItem
                    style={{backgroundColor:(Color.headColor),height:headHeight,paddingTop:headPaddingTop,paddingBottom:0}}>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={this.onFinishPage.bind(this)}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf:'center',flex:3}}>
                    <Text style={{color: '#fff',fontSize: 18,alignSelf:'center'}}>{Language().recreation}</Text>
                    </Body>
                    <Right style={{flex:1}}/>
                </CardItem>
                {this._checkShowReactAudio() ? <RecreationPageList tabLabel="RecreationPageList"/> :
                    <Recreation tabLabel="Recreation"/>}
            </View>
        );
    }

    _checkShowReactAudio() {
        if (this.props.configData && this.props.configData.audio_room_show) {
            if (this.props.configData.audio_room_show != 'react') {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    onFinishPage() {
        if (!this.state.isFinishing) {
            this.state.isFinishing = true;
            this.props.finishPage('global');
        }
        this.isFinishingTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }
}
const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
});
const mapDispatchToProps = dispatch => ({
    openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
    finishPage: (key) => dispatch(popRoute(key)),
});
const mapStateToProps = state => ({
    configData: state.checkRegisterReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps)(VoiceRoom);