/**
 * Created by wangxu on 2017/9/15.
 */
import React, {Component} from 'react';
import Language from '../../../resources/language'
import {
    StyleSheet,
    Image,
    Dimensions,
    View,
    Platform,
    NativeModules,
    ListView,
    RefreshControl,
    ActivityIndicator,
    InteractionManager,
    TouchableOpacity,
    Text,
    ScrollView,
    Modal, Switch, TextInput, DeviceEventEmitter,
    NativeEventEmitter,
} from 'react-native';
import {checkOpenRegister} from '../../../support/actions/checkRegisterActions';

import {Input, Spinner} from 'native-base';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import Toast from '../../../support/common/Toast';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import CachedImage from '../../../support/common/CachedImage';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import Color from '../../../resources/themColor';
import LoadMoreFooter from '../../../support/common/LoadMoreFooter'
import netWorkTool from '../../../support/common/netWorkTool';
import Loading from '../../../support/common/Loading';
import * as types from '../../../support/actions/actionTypes';
import {fetchRecreationPageList, updateFilter} from '../../../support/actions/recreationActions';
import rnToNativeUtils from '../../../support/common/rnToNativeUtils';
import Util from '../../../support/common/utils';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import  * as Constant from '../../../support/common/constant';

const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const activeOpacityValue = (Platform.OS === 'ios') ? 0.6 : 0.6;
const disableLiftSwipe = (Platform.OS === 'ios') ? true : true;
const roomUserNameFonSize = (Platform.OS === 'ios') ? 12 : 10;
const roomUserNamePadding = (Platform.OS === 'ios') ? 1 : 0;
//image
const bgVoiceHome = require('../../../resources/imgs/icon_voice_room.png');
const bgVoiceSearch = require('../../../resources/imgs/icon_voice_search.png');
const voiceHomeIcon = require('../../../resources/imgs/ic_room_icon.png');
const voiceHomeIconLock = require('../../../resources/imgs/ic_room_icon_lock.png');
const voiceUserIcon = require('../../../resources/imgs/ic_user_icon.png');
const icDefaultUser = require('../../../resources/imgs/ic_default_voice_user.png');
const defaultRecreationBg = require('../../../resources/imgs/icon_vioce_deault_bg.png');
const voiceItemBg = require('../../../resources/imgs/voice_item_bg.png');
const selectAudioIcon = require('../../../resources/imgs/icon_selected_audio.png');
const normalAudioIcon = require('../../../resources/imgs/icon_normal_audio.png');
const audioLike = require('../../../resources/imgs/icon_audio_like.png');
const audioPop = require('../../../resources/imgs/icon_audio_pop.png');
const audioCreateHeader = require('../../../resources/imgs/ic_default_audio_create.png');
const defaultNoHeader = require('../../../resources/imgs/ic_audio_no_user.png');
const recreationTitleBg = require('../../../resources/imgs/icon_recreation_title.png');
const {
    pushRoute,
    popRoute
} = actions;
const {RNMessageSender}=NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
const recreationLimit = 5;
let sampleRecreationRow = 0;
let recreationRoomList = new Array();
let audioTypes = new Array();
let that;
let progressDialog;
let cols = 3;
let cellH = 30;
let cellW = (ScreenWidth - 130) / 3;
let vMargin = 5;
let hMargin = 5;
let audioType = '';
class RecreationPageList extends Component {
    static propTypes = {
        userInfo: React.PropTypes.object,
        isRefreshing: React.PropTypes.bool,
        isNoMore: React.PropTypes.bool,
        data: React.PropTypes.object,
        fetchRecreationPageList: React.PropTypes.func,
        hostLocation: React.PropTypes.string,
        configData: React.PropTypes.object,
        updateConfig: React.PropTypes.func,
        updateFilter: React.PropTypes.func,
        actionType: React.PropTypes.string
    }

    constructor(props) {
        super(props);
        that = this;
        if (this.props.data != null) {
            recreationRoomList = this.props.data.rooms;
        }
        if (this.props.configData) {
            if (this.props.configData.audio_room_types) {
                audioTypes = this.props.configData.audio_room_types;
            }
        }
        if (audioTypes[0]) {
            if (audioTypes[0].k) {
                audioType = audioTypes[0].k;
            }
        }
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 == r2});
        //2.设置返回数据
        this.state = {
            dataSource: ds.cloneWithRows(recreationRoomList),
            audioDataSource: ds.cloneWithRows(audioTypes),
            dialogTitle: Language().room_theme,
            showVoiceModalDialog: false,
            showModelProgress: false,
            modalDialogIndex: 0,
            height: 0,
            roomThemeContent: '',
            roomPassword: '',
            roomID: '',
            dialogCancel: Language().cancel,
            dialogConfirm: Language().confirm,
            needPassword: false,
            switchIsOn: false,
            roomDetailData: null,
            password1: '',
            password2: '',
            password3: '',
            password4: '',
            audioTypeIndex: 0,
            audioTypeValue: audioType,
            filterFlag: 0,//0为：user;1为：like；2为：pop,
            isConnectedNetWork: true
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (!_.isEqual(this.props.data, nextProps.data) || !_.isEqual(this.props.configData, nextProps.data));
    }

    componentDidMount() {
        progressDialog = this.refs.progressDialog;
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.onShowProgressDialog(Language().loading_now);
        this.subscription = DeviceEventEmitter.addListener(Constant.SCROLL_RECREATION_LIST, this.scrollRecreationList.bind(this));
        if (Platform.OS === 'android') {
            this.onNativeMessageLister = DeviceEventEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
        } else {
            this.onNativeMessageLister = rNMessageSenderEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
        }
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.onNativeMessageLister.remove();
        this.subscription.remove();
    }

    componentWillMount() {

    }

    handleMethod(isConnected) {
        that.state.isConnectedNetWork = isConnected;
    }

    /**
     * 渲染页面
     * @returns {XML}
     */
    render() {
        if (this.props.data) {
            recreationRoomList = this.props.data.rooms;
        }
        if (this.props.configData) {
            if (this.props.configData.audio_room_types) {
                audioTypes = this.props.configData.audio_room_types;
            }
        }
        if (this.props.actionType == types.RECREATION_DATA || this.props.actionType == types.RECREATION_DATA_ERROR) {
            this.onHideProgressDialog();
        }
        if (this.props.actionType == types.RECREATION_DATA) {
            sampleRecreationRow = 0;
        }
        if (this.state.showVoiceModalDialog) {
            this.props.isRefreshing = false;
        }
        return (
            <View style={{backgroundColor:Color.voice_room_bg_color,flex:1}}>
                {that._defaultView()}
                {this.headerView()}
                <ListView dataSource={this.state.dataSource.cloneWithRows(recreationRoomList)}
                          ref="listView"
                          renderRow={this.renderRow}
                          renderFooter={this.renderFooter}
                          initialListSize={1}
                          scrollRenderAheadDistance={50}
                          removeClippedSubviews={true}
                          pageSize={recreationLimit}
                          onScroll={this._onScroll}
                          onEndReached={this._onEndReach}
                          onEndReachedThreshold={10}
                          enableEmptySections={true}
                          refreshControl={
                                     <RefreshControl
                                        refreshing={this.props.isRefreshing}
                                         onRefresh={this._onRefresh}
                                         colors={[Color.list_loading_color]} />
                                    }
                          closeOnRowBeginSwipe={true}/>
                <Loading ref="progressDialog" loadingTitle={Language().operation_loading}/>
                {/*创建房间以及搜索房间的弹框*/}
                <Modal visible={this.state.showVoiceModalDialog} transparent={true} animationType='fade'>
                    <View flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                          style={[styles.dialogContainer]}>

                        <ScrollView alwaysBounceVertical={false}>
                            <View
                                style={{borderRadius:8,width:ScreenWidth-30,maxHeight:ScreenHeight-80,alignSelf:'center',backgroundColor:'#e3dce9',padding:0}}>
                                <View flexDirection='row' style={{justifyContent:'center',padding:5}}>
                                    <Text
                                        style={{color:Color.voice_set_text_color,alignSelf:'center',fontSize:17,textAlign:'center'}}>{this.state.dialogTitle}</Text>
                                </View>
                                {this._modelDialogContentView()}
                                <View flexDirection='row' style={{justifyContent:'center',margin:10}}>
                                    <TouchableOpacity activeOpacity={0.9}
                                                      onPress={this.onCloseModal.bind(this)}
                                                      style={[styles.actionButton,{backgroundColor:Color.colorWhite}]}>
                                        <Text
                                            style={{alignSelf:'center',color:'#845fe4'}}>{this.state.dialogCancel}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={0.8} onPress={this.onConfirmModal.bind(this)}
                                                      style={styles.actionButton}>
                                        <Text
                                            style={{alignSelf:'center',color:Color.colorWhite}}>{this.state.dialogConfirm}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.showModelProgress ? (
                                    <View style={[styles.loading,{left:ScreenWidth/2-55,top:45}]}>
                                        <Spinner size='small' style={{height:35}}/>
                                        <Text
                                            style={{marginTop:2,fontSize: 12,color:Color.colorWhite}}>{Language().request_loading}</Text>
                                    </View>) : null}
                        </ScrollView>
                    </View>
                </Modal>
            </View>
        );
    }

    /**
     *
     */
    scrollRecreationList() {
        this.doScrollToList();
    }

    /**
     *1-- 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     *
     * @param message
     */
    onShowProgressDialog(message) {
        if (progressDialog != null) {
            progressDialog.show({loadingTitle: message});
        }
    }

    /**
     *1-- 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    onConfirmModal() {
        if (!this.state.showModelProgress) {
            let hostLocation = '';
            if (this.props.hostLocation) {
                hostLocation = this.props.hostLocation;
            }
            switch (that.state.modalDialogIndex) {
                case 0:
                    //创建房间
                    let url = apiDefines.CREATE_AUDIO_ROOM;
                    let {roomThemeContent, roomPassword} = this.state;
                    roomThemeContent=roomThemeContent.trim();
                    if (!roomThemeContent.length || roomThemeContent.length < 4) {
                        Toast.show(Language().voice_theme_limit_message)
                        return;
                    }
                    if (this.state.switchIsOn && !roomPassword.length) {
                        Toast.show(Language().placeholder_password)
                        return;
                    }
                    let body = {
                        type: 'audio',
                        title: roomThemeContent,
                        password: roomPassword,
                        lc: hostLocation,
                        child_type: this.state.audioTypeValue
                    }
                    that.showProgress();
                    this.createRequest(false, url, body);
                    break;
                case 1:
                    //搜索房间
                    if (!this.state.roomPassword) {
                        Toast.show(Language().placeholder_password)
                        return;
                    }
                    let data = {
                        roomId: this.state.roomID,
                        roomPassword: this.state.roomPassword
                    }
                    that.doEnterVoiceRoom(data);
                    break;
                case 2:
                    //直接进入
                    let newData = {
                        roomId: this.state.roomID,
                        roomPassword: this.state.roomPassword
                    }
                    that.doEnterVoiceRoom(newData);
                    break;
            }
        }
    }

    onCloseModal() {
        that.setState({
            showVoiceModalDialog: false,
            showModelProgress: false,
            modalDialogIndex: 0,
            height: 0,
            roomThemeContent: '',
            roomPassword: '',
            roomID: '',
            needPassword: false,
            switchIsOn: false,
            roomDetailData: null,
            password1: '',
            password2: '',
            password3: '',
            password4: '',
            audioTypeIndex: 0,
            audioTypeValue: audioType
        });
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    renderRow(item, sectionId, rowId) {
        let filterSameData = that.filterSameData(item.room_id, rowId);
        if (filterSameData) {
            sampleRecreationRow++;
            return null;
        }
        let users = item.users;
        let title = '';
        if (item.title) {
            title = item.title;
            title = title.replace(/[\r\n]/g, "");
        } else {
            title = item.room_id;
        }
        let userCount = users.length;
        if (item.userCount) {
            userCount = item.userCount;
        }
        let audioType = '';
        if (item.child_type) {
            audioType = item.child_type;
        }
        let user_1 = that._getUser(users, 0);
        let user_2 = that._getUser(users, 1);
        let user_3 = that._getUser(users, 2);
        let user_4 = that._getUser(users, 3);
        let room_lock_icon = item.needPassword ? voiceHomeIconLock : null;
        let audioData = that.getAudioTypeData(item.child_type);
        return (<TouchableOpacity activeOpacity={0.6} onPress={()=>{that.enterVoiceRoom(item)}}
                                  style={{backgroundColor:Color.transparent,justifyContent:'center'}}>
                <View flexDirection='row' style={{marginLeft:10,marginRight:10,padding:5,overflow:'hidden'}}>
                    <View flexDirection='row'
                          style={{borderRadius:5,borderColor:Color.voice_room_bg_color,borderWidth:2,overflow:'hidden'}}>
                        <CachedImage style={styles.image} source={voiceItemBg}>
                            <View flexDirection='row'>
                                <View style={{borderRadius:3,justifyContent:'center'}}>
                                    <CachedImage style={styles.ownerHeader}
                                                 type='audio_header'
                                                 source={(item.image)?({uri:item.image}):audioCreateHeader}
                                                 defaultSource={audioCreateHeader}/>
                                    <View style={{position: "absolute", top: 5,left:3}}>
                                        <View flexDirection='row'>
                                            <CachedImage style={styles.voiceHomeIcon} source={voiceHomeIcon}/>
                                            <View flexDirection='row'
                                                  style={{backgroundColor:'#ff009c',borderRadius:8,paddingLeft:5,paddingRight:5,height:12,alignSelf:'center'}}>
                                                <Text numberOfLines={1}
                                                      style={styles.rowStatusText}>{item.room_id}</Text>
                                            </View>
                                        </View>
                                        <View flexDirection='row' style={{marginTop:3}}>
                                            <CachedImage style={styles.voiceHomeIcon} source={voiceUserIcon}/>
                                            <View
                                                style={{backgroundColor:'#ffba13',borderRadius:8,paddingLeft:5,paddingRight:5,height:12,alignSelf:'center'}}>
                                                <Text numberOfLines={1}
                                                      style={[styles.rowStatusText,{color:Color.blackColor}]}>{userCount}人</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View
                                        style={{position: "absolute",bottom:8,right:25,backgroundColor:'#bc06ad',borderRadius:2,left:15}}>
                                        <Text numberOfLines={1}
                                              style={[styles.rowStatusText,{fontSize:roomUserNameFonSize,color:'#cabddd',padding:roomUserNamePadding,paddingLeft:5,paddingRight:8,backgroundColor:Color.transparent}]}>{item.name}</Text>
                                    </View>
                                    {that.audioTypeImageView(audioData)}
                                </View>
                                <View style={{flex:1}}>
                                    <View flexDirection='row' style={{height:37}}>
                                        <Text numberOfLines={1} style={styles.rowTitle}>{title}</Text>
                                        <CachedImage style={styles.voiceHomeLock} source={room_lock_icon}/>
                                    </View>
                                    <View style={[styles.renderRow]}>
                                        <View flexDirection='row'
                                              style={{flex:4,justifyContent:'center',alignSelf:'center',paddingRight:10,paddingLeft:10}}>
                                            <View style={{flex:1}}>
                                                <CachedImage style={styles.userHeader}
                                                             type='audio'
                                                             source={(!user_1.avatar.length)?defaultNoHeader:({uri:user_1.avatar})}
                                                             defaultSource={icDefaultUser}/>
                                                <Text numberOfLines={1}
                                                      style={styles.rowUserName}>{user_1.name}</Text>
                                            </View>
                                            <View style={{flex:1}}>
                                                <CachedImage style={styles.userHeader}
                                                             type='audio'
                                                             source={(!user_2.avatar.length)?defaultNoHeader:({uri:user_2.avatar})}
                                                             defaultSource={icDefaultUser}/>
                                                <Text numberOfLines={1}
                                                      style={styles.rowUserName}>{user_2.name}</Text>
                                            </View>
                                            <View style={{flex:1}}>
                                                <CachedImage style={styles.userHeader}
                                                             type='audio'
                                                             source={(!user_3.avatar.length)?defaultNoHeader:({uri:user_3.avatar})}
                                                             defaultSource={icDefaultUser}/>
                                                <Text numberOfLines={1}
                                                      style={styles.rowUserName}>{user_3.name}</Text>
                                            </View>
                                            <View style={{flex:1}}>
                                                <CachedImage style={styles.userHeader}
                                                             type='audio'
                                                             source={(!user_4.avatar.length)?defaultNoHeader:({uri:user_4.avatar})}
                                                             defaultSource={icDefaultUser}/>
                                                <Text numberOfLines={1}
                                                      style={styles.rowUserName}>{user_4.name}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View flexDirection='row' style={{flex:1}}>
                                        <View flexDirection='row' style={{flex:1.3}}>
                                            {that.audioLikePopView(item.like, audioLike)}
                                            {that.audioLikePopView(item.pop, audioPop)}
                                        </View>
                                        <View style={{flex:1}}>
                                            {that.audioTypeTitleView(audioData)}
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </CachedImage>
                    </View>
                </View>
                <View style={styles.rowSeparator}></View>
            </TouchableOpacity>
        );
    }

    /**
     * 过滤相同的数据
     * @param roomId
     */
    filterSameData(roomId, rowId) {
        try {
            for (let i = 0; i < rowId; i++) {
                let roomDate = recreationRoomList[i];
                if (roomDate && roomDate.room_id == roomId) {
                    return true;
                }
            }
            return false;
        } catch (e) {
            return false;
        }
    }

    /**
     * @param content
     * @param source
     * @returns {*}
     */
    audioLikePopView(content, source) {
        if (content) {
            if (content > 9999999) {
                content = 9999999 + '+';
            }
            return (<View flexDirection='row'>
                <Image style={[styles.voiceHomeIcon,{height:14,width:14,marginLeft:5}]} source={source}/>
                <Text numberOfLines={1}
                      style={[styles.rowStatusText,{color:'#f30ce9',fontSize:12,backgroundColor:Color.transparent}]}>{content}</Text>
            </View>);
        } else {
            return null;
        }
    }

    audioTypeTitleView(data) {
        if (data && data.t) {
            return (<View style={{position: 'absolute',left:0,right:0,bottom:0,backgroundColor:Color.transparent}}>
                <View
                    style={{backgroundColor:'#FFFD38',paddingLeft:10,paddingRight:10,alignSelf:'center',borderTopLeftRadius:8,borderTopRightRadius:8,paddingBottom:3,paddingTop:3}}>
                    <Text numberOfLines={1}
                          style={[styles.rowStatusText,{color:'#4A1C98',fontSize:11,fontWeight:'bold'}]}>{data.t}</Text>
                </View>
            </View>);
        }
    }

    /**
     *
     * @param audioData
     * @returns {null}
     */
    audioTypeImageView(audioData) {
        if (audioData) {
            return (<Image
                source={{uri:audioData.i}}
                style={{position: "absolute",bottom:5,right:5, resizeMode: 'contain',alignSelf: 'center',width:25,height:25}}/>)
        } else {
            return null;
        }
    }

    /**
     *
     * @returns {null}
     */
    getAudioTypeData(childType) {
        if (audioTypes) {
            for (let i = 0; i < audioTypes.length; i++) {
                if (audioTypes[i].k == childType) {
                    return audioTypes[i];
                }
            }
            return null;
        } else {
            return null;
        }
    }

    /**
     * 进入音频房间
     */
    enterVoiceRoom(roomData) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                if (roomData && roomData.needPassword) {
                    that.setState({
                        showVoiceModalDialog: true,
                        dialogTitle: Language().room_number + roomData.room_id,
                        modalDialogIndex: 2,
                        roomID: roomData.room_id,
                        roomDetailData: roomData
                    });
                } else {
                    let data = {
                        roomId: roomData.room_id,
                        roomPassword: ''
                    }
                    that.doEnterVoiceRoom(data);
                }
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });

    }

    /**
     * 执行进入房间的操作
     */
    doEnterVoiceRoom(roomData) {
        let NativeData = {
            action: 'enterAudioRoom',
            params: roomData,
            options: null
        }
        if (this.state.showVoiceModalDialog) {
            this.onCloseModal();
        }
        rnToNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }

    /**
     *
     * @param users
     * @param index
     * @returns {{name: string, avatar: string}}
     * @private
     */
    _getUser(users, index) {
        let user = {
            name: '',
            avatar: '',
        }
        if (users && index < users.length) {
            if (users[index].name) {
                user.name = users[index].name;
            }
            if (users[index].avatar) {
                user.avatar = users[index].avatar;
            }
        }
        return user;
    }

    /**
     * 刷新
     * @private
     */
    _onRefresh = () => {
        netWorkTool.checkNetworkState((isConnected) => {
            if (!isConnected) {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
            this.props.isRefreshing = true;
            InteractionManager.runAfterInteractions(() => {
                //刷新
                sampleRecreationRow = 0;
                this.props.fetchRecreationPageList(1, types.RECREATION_REFRESH_DATA, this.state.filterFlag);
            });

        });
    };
    /**
     * load more
     * @private
     */
    _onEndReach = () => {
        let data = this.props.data;
        if (data) {
            if (data.page < data.pages) {
                this.props.fetchRecreationPageList(data.page + 1, types.RECREATION_MORE_DATA, this.state.filterFlag);
            } else if ((data.page == data.pages)) {
                this.props.fetchRecreationPageList(data.page, types.RECREATION_MORE_DATA, this.state.filterFlag);
            }
        }
    }

    /**
     * 加载更多
     * @private
     */
    renderFooter() {
        //音频房间列表一页最多10条数据
        let allDataCount = recreationRoomList.length;
        let recreationData = that.props.data;
        if (recreationData) {
            if (recreationData.page == 1) {
                if (sampleRecreationRow < allDataCount) {
                    allDataCount = allDataCount - sampleRecreationRow;
                }
            }
        }
        if (that.state.isConnectedNetWork) {
            return allDataCount < recreationLimit ? null : (<LoadMoreFooter isNoMore={that.props.isNoMore}/>);
        } else {
            return (<Text style={styles.title}>{Language().load_more_data_error}</Text>);
        }
    }


    /**
     * 列表固定头部
     */
    headerView() {
        return (<View>
            <View>
                <Image source={recreationTitleBg} style={styles.titleImage}/>
                <View flexDirection='row'
                      style={{justifyContent:'center',padding:8,marginTop:2,marginLeft:20,marginRight:20,marginBottom:0}}>
                    <TouchableOpacity onPress={()=>that._createVoiceRoom()}
                                      activeOpacity={activeOpacityValue}
                                      style={styles.voiceBt}>
                        <View flexDirection='row'
                              style={{backgroundColor:'#FF00E3',borderRadius:3,justifyContent:'center',height:34,paddingRight:20,paddingLeft:20}}>
                            <Image source={bgVoiceHome} style={styles.voiceHome}></Image>
                            <Text
                                style={[styles.voiceHomeText,{fontWeight:'bold',marginLeft:3,alignSelf:'center'}]}>{Language().create}</Text>
                        </View>
                    </TouchableOpacity>
                    <View flexDirection='row'
                          style={{backgroundColor:'#a582bc',borderRadius:3,borderColor:'#922faa',marginLeft:3,height:35,alignSelf:'center',padding:0,borderWidth:2,flex:1}}>
                        <TextInput
                            ref="roomIdInput"
                            underlineColorAndroid='transparent'
                            style={{fontSize: 16,color:Color.colorWhite,padding: 5,height:30,flex:1,alignSelf:'center'}}
                            placeholder={Language().search_room}
                            placeholderTextColor='#bb9edb'
                            onChangeText={(text)=>this._changeVoiceRoomId(text)} value={this.state.roomID}/>
                        <TouchableOpacity onPress={()=>that._searchVoiceRoom()}
                                          activeOpacity={activeOpacityValue}
                                          style={styles.voiceBt}>
                            <Image source={bgVoiceSearch} style={styles.voiceSearch}></Image>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
            {that.filterView()}
            <View style={[styles.rowSeparator,{marginBottom:2}]}></View>
        </View>);
    }

    /**
     * 条件过滤view
     */
    filterView() {
        return (<View flexDirection='row' style={{alignSelf:'center',marginBottom:3}}>
            <TouchableOpacity onPress={this.filterToPeople.bind(this)} style={styles.filterButtonStyle}
                              activeOpacity={0.6}>
                <Text
                    style={[styles.filterText,{color:that.state.filterFlag==0?'#FF00E3':'#965f92'}]}>{Language().user}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.filterToPop.bind(this)} style={styles.filterButtonStyle}
                              activeOpacity={0.6}>
                <Text
                    style={[styles.filterText,{color:that.state.filterFlag==1?'#FF00E3':'#965f92'}]}>{Language().pop}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.filterToLike.bind(this)} style={styles.filterButtonStyle}
                              activeOpacity={0.6}>
                <Text
                    style={[styles.filterText,{color:that.state.filterFlag==2?'#FF00E3':'#965f92'}]}>{Language().like}</Text>
            </TouchableOpacity>
        </View>);
    }

    /**
     *
     * @param filter
     */
    updateFilterData(filter) {
        that.props.updateFilter(filter);
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                that.onShowProgressDialog(Language().loading_now);
                InteractionManager.runAfterInteractions(() => {
                    sampleRecreationRow = 0;
                    that.props.fetchRecreationPageList(1, types.RECREATION_DATA, filter);
                    that.doScrollToList();

                });
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     *移动list view 到最顶端
     */
    doScrollToList() {
        if (that.refs.listView) {
            that.refs.listView.scrollTo(0, 0);
        }
    }

    /**
     * 人数排序
     */
    filterToPeople() {
        that.setState({
            filterFlag: 0

        });
        that.updateFilterData(0);
    }


    /**
     * 人气排序
     */
    filterToPop() {
        that.setState({
            filterFlag: 1

        });
        that.updateFilterData(1);
    }

    /**
     * 点赞排序
     */
    filterToLike() {
        that.setState({
            filterFlag: 2

        });
        that.updateFilterData(2);
    }

    /**
     *
     * @private
     * 内容可分为：
     * 1->创建音频房间
     * 2->搜索音频房间
     * 3->房间需要输入密码
     *
     */
    _modelDialogContentView() {
        switch (that.state.modalDialogIndex) {
            case 0:
                return this._modelCreateRoom();
            case 1:
                return this._modelSearchRoom();
            case 2:
                return this._modelInitPassword();
        }
    }

    passwordInputView() {
        return (<TextInput
            ref='passwordInput'
            style={{paddingLeft:0,color:(Color.transparent),fontSize:12,height:35,padding:5,paddingLeft:0,backgroundColor:Color.transparent,lineHeight:0,selectionColor:Color.transparent}}
            autoFocus={true} maxLength={4}
            selectionColor={Color.transparent}
            onChangeText={(text)=>this._changeChangePassword(text)}
            keyboardType='numeric'
            onSubmitEditing={()=>this.passwordEndEditing()}
            value={this.state.roomPassword}/>);
    }

    passwordEndEditing() {
        if (this.refs.passwordInput) {
            this.refs.passwordInput.blur();
        }
    }

    /**
     *
     * @returns {XML}
     * @private
     */
    _modelCreateRoom() {
        let setPassword = (<View
            style={{margin:3,marginTop:10}}>
            {this.passwordInputView()}
            {this.passwordView()}
        </View>);
        return (<View style={{margin:10,marginRight:20,marginLeft:20}}>
            {this.audioTypeView()}
            <Text
                style={{color:(Color.voice_set_text_color),fontSize:14}}>{Language().room_theme}</Text>
            <View
                style={{borderColor:'#a66ce9',borderRadius:3,borderWidth:1,marginRight:20,marginLeft:20,marginTop:10}}>
                <Input
                    multiline={true}
                    autoFocus={true}
                    style={{fontSize: 12,color: (Color.voice_set_text_color),padding: 5,height:Math.max(30,this.state.height),paddingRight:10,paddingLeft:10}}
                    placeholder={Language().placeholder_room_theme}
                    placeholderTextColor={Color.prompt_text_color}
                    maxLength={15}
                    onChangeText={(text)=>this._changeTitleTheme(text)} value={this.state.roomThemeContent}
                    onContentSizeChange={this.onContentSizeChange.bind(this)}/>
            </View>
            <Text
                style={{fontSize:10,paddingBottom:5,marginTop:10,marginRight:20,marginLeft:20,color:'#757575'}}>{Language().voice_limit_message}</Text>
            <View flexDirection='row' style={{marginTop:5}}>
                <Text
                    style={{color:(Color.voice_set_text_color),fontSize:14,marginRight:10,alignSelf:'center'}}>{Language().set_password}</Text>
                <Switch onValueChange={(value)=>this.setState({switchIsOn:value})}
                        value={this.state.switchIsOn}/>
            </View>
            {this.state.switchIsOn ? setPassword : null}
        </View>);
    }

    passwordView() {
        return (<View flexDirection='row' style={{position: "absolute"}}>
            <TouchableOpacity onPress={this.focusPassword.bind(this)}
                              activeOpacity={1}
                              style={{justifyContent:'center',backgroundColor:'#E1DDE8',flex:1}}>
                <View flexDirection='row'
                      style={{alignSelf:'center',justifyContent:'center',flex:1,backgroundColor:'#E1DDE8'}}>
                    <View
                        style={{backgroundColor:'#835EDF',borderColor:'#6939DA',borderWidth:1,padding:5,borderRadius:3,width:25,height:35}}>
                        <Text
                            style={{fontSize:16,color:Color.colorWhite,fontWeight:'bold',alignSelf:'center'}}>{this.state.password1}</Text>
                    </View>
                    <View
                        style={{backgroundColor:'#835EDF',borderColor:'#6939DA',borderRadius:3,borderWidth:1,padding:5,marginLeft:10,width:25,height:35}}>
                        <Text
                            style={{fontSize:16,color:Color.colorWhite,fontWeight:'bold',alignSelf:'center'}}>{this.state.password2}</Text>
                    </View>
                    <View
                        style={{backgroundColor:'#835EDF',borderColor:'#6939DA',borderRadius:3,borderWidth:1,padding:5,marginLeft:10,width:25,height:35}}>
                        <Text
                            style={{fontSize:16,color:Color.colorWhite,fontWeight:'bold',alignSelf:'center'}}>{this.state.password3}</Text>
                    </View>
                    <View
                        style={{backgroundColor:'#835EDF',borderColor:'#6939DA',borderRadius:3,borderWidth:1,padding:5,marginLeft:10,width:25,height:35}}>
                        <Text
                            style={{fontSize:16,color:Color.colorWhite,fontWeight:'bold',alignSelf:'center'}}>{this.state.password4}</Text>
                    </View>

                </View>

            </TouchableOpacity>
        </View>);
    }

    focusPassword() {
        if (this.refs.passwordInput) {
            this.refs.passwordInput.focus();
        }
    }

    /**
     * 搜索房间
     * @returns {XML}
     * @private
     */
    _modelSearchRoom() {
        let passwordView = (
            <View style={{borderColor:Color.action_room_color,borderRadius:3,borderWidth:1,marginBottom:5}}>
                <Input
                    style={{fontSize: 12,color: (Color.editUserTextColor),padding: 5,paddingRight:10,paddingLeft:10,height:30}}
                    placeholder={Language().placeholder_room_password}
                    placeholderTextColor={Color.prompt_text_color}
                    autoFocus={true} maxLength={4}
                    keyboardType='numeric'
                    onChangeText={(text)=>this._changeChangePassword(text)} value={this.state.roomPassword}/>
            </View>);
        return (<View style={{margin:10,marginRight:20,marginLeft:20}}>
            <View style={{borderColor:Color.action_room_color,borderRadius:3,borderWidth:1,marginBottom:5}}>
                <Input
                    style={{fontSize: 12,color: (Color.editUserTextColor),padding: 5,paddingRight:10,paddingLeft:10,height:30}}
                    placeholder={Language().placeholder_room_number}
                    placeholderTextColor={Color.prompt_text_color}
                    autoFocus={true}
                    onChangeText={(text)=>this._changeVoiceRoomId(text)} value={this.state.roomID}/>
            </View>
            {this.state.needPassword ? passwordView : null}
        </View>);
    }

    /**
     * 房间ID
     * @returns {XML}
     * @private
     */
    _modelInitPassword() {
        return (<View style={{marginTop:5}}>
            {this.roomDetailView(this.state.roomDetailData)}
            <View style={{marginTop:10}}>
                {this.passwordInputView()}
                {this.passwordView()}
            </View>
            <Text style={styles.roomNumber}>{Language().placeholder_room_password}</Text>
        </View>);
    }

    /**
     * 房间 号
     * @param text
     * @private
     */
    _changeChangePassword(text) {
        let password = text + '';
        let one = password[0] == null ? '' : password[0];
        let two = password[1] == null ? '' : password[1];
        let three = password[2] == null ? '' : password[2];
        let four = password[3] == null ? '' : password[3];
        if (password.length == 4) {
            this.passwordEndEditing();
        }
        if (/^[\d]+$/.test(text)) {
            this.setState({
                roomPassword: text,
                password1: one,
                password2: two,
                password3: three,
                password4: four,
            });
        } else {
            if (/^[\d{1}]$/.test(this.state.roomPassword)) {
                this.setState({
                    roomPassword: '',
                    password1: one,
                    password2: two,
                    password3: three,
                    password4: four,
                });
            }
        }
    }

    /**
     * 房间 号
     * @param text
     * @private
     */
    _changeVoiceRoomId(text) {
        this.setState({
            roomID: text
        });
    }

    /**
     * 房间主题
     * @param text
     * @private
     */
    _changeTitleTheme(text) {
        if (text) {
            text = text.replace(/[\r\n]/g, "");
        }
        this.setState({
            roomThemeContent: text
        });
    }

    /**
     * 输入框换行并自增加
     * @param event
     */
    onContentSizeChange(event) {
        this.setState({height: event.nativeEvent.contentSize.height});
    }

    /**
     * 创建房间
     * @private
     */
    _createVoiceRoom() {
        if (this.props.configData) {
            if (!this.props.configData.audio_room_types) {
                this.props.updateConfig();
            }
        } else {
            this.props.updateConfig();
        }
        that.setState({
            showVoiceModalDialog: true,
            dialogTitle: Language().create_room,
            modalDialogIndex: 0,
        });
    }

    /**
     * 搜索房间
     * @private
     */
    _searchVoiceRoom() {
        if (this.refs.roomIdInput) {
            this.refs.roomIdInput.blur();
        }
        let hostLocation = '';
        if (this.props.hostLocation) {
            hostLocation = this.props.hostLocation;
        }
        if (this.state.roomID) {
            let url = apiDefines.SERVER_GET + this.state.roomID + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id + apiDefines.PARAMETER_LOCATION + hostLocation;
            that.onShowProgressDialog();
            this.createRequest(true, url, null);
        } else {
            Toast.show(Language().placeholder_room_number);
        }
    }

    /**
     * 默认背景
     * @param height
     * @returns {*}
     * @private
     */
    _defaultView() {
        return recreationRoomList.length == 0 ? (<View
                style={{justifyContent: 'center',backgroundColor:Color.transparent,position: 'absolute',flex:1,left:0,top:0,right:0,bottom:0}}>
                <CachedImage style={styles.defaultBg} source={defaultRecreationBg}/>
                <Text
                    style={{alignSelf:'center',padding:5,marginTop:0,color:'#fcb1fe',fontSize:12,textAlign:'center',lineHeight:20}}>{Language().no_voice_room}</Text>
            </View>) : null;
    }

    /**
     * 网路请求
     * @param url
     * @param actionType
     */
    createRequest(isGet, url, body) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                if (isGet) {
                    Util.get(url, (code, message, data) => {
                        that.onHideProgressDialog();
                        if (code == 1000) {
                            //搜索房间
                            if (data.level == 'audio') {
                                if (data.password_needed) {
                                    //提示输入密码
                                    that.setState({
                                        showVoiceModalDialog: true,
                                        dialogTitle: Language().room_number + this.state.roomID,
                                        modalDialogIndex: 2,
                                        needPassword: data.password_needed
                                    });
                                } else {
                                    //直接进入
                                    let data = {
                                        roomId: this.state.roomID,
                                        roomPassword: ''
                                    }
                                    that.doEnterVoiceRoom(data);
                                }
                            } else {
                                Toast.show(Language().voice_room_error);
                            }
                        } else {
                            Toast.show(message);
                        }
                    }, (error) => {
                        that.onHideProgressDialog();
                        Toast.show(Language().not_network);
                    });
                } else {
                    Util.post(url, body, (code, message, data) => {
                        that.hideProgress();
                        if (code == 1000) {
                            let roomData = {
                                roomId: data.room_id,
                                roomPassword: this.state.roomPassword
                            }
                            that.doEnterVoiceRoom(roomData);
                        } else {
                            Toast.show(message);
                        }
                    }, (error) => {
                        that.hideProgress();
                        Toast.show(Language().not_network);
                    });
                }

            } else {
                that.hideProgress();
                that.onHideProgressDialog();
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    showProgress() {
        if (!that.state.showModelProgress) {
            that.setState({
                showModelProgress: true
            });
        }
    }

    roomDetailView(item) {
        if (item) {
            let users = item.users;
            let title = '';
            if (item.title) {
                title = item.title;
                title = title.replace(/[\r\n]/g, "");
            } else {
                title = item.room_id;
            }
            let userCount = users.length;
            if (item.userCount) {
                userCount = item.userCount;
            }
            let user_1 = that._getUser(users, 0);
            let user_2 = that._getUser(users, 1);
            let user_3 = that._getUser(users, 2);
            let user_4 = that._getUser(users, 3);
            let room_lock_icon = item.needPassword ? voiceHomeIconLock : null;
            let audioData = that.getAudioTypeData(item.child_type);
            return (<View flexDirection='row'
                          style={{borderRadius:8,borderColor:'#BBA8F8',borderWidth:3,padding:0,backgroundColor:Color.voice_room_bg_color}}>
                <CachedImage style={[styles.image,{flex:1,margin:0}]} source={voiceItemBg}>
                    <View flexDirection='row'>
                        <View style={{borderRadius:3,justifyContent:'center'}}>
                            <CachedImage style={styles.ownerHeader}
                                         type='audio_header'
                                         source={(item.image)?({uri:item.image}):audioCreateHeader}
                                         defaultSource={audioCreateHeader}/>
                            <View style={{position: "absolute", top: 5,left:3}}>
                                <View flexDirection='row'>
                                    <CachedImage style={styles.voiceHomeIcon} source={voiceHomeIcon}/>
                                    <View flexDirection='row'
                                          style={{backgroundColor:'#ff009c',borderRadius:8,paddingLeft:5,paddingRight:5,height:12,alignSelf:'center'}}>
                                        <Text numberOfLines={1} style={styles.rowStatusText}>{item.room_id}</Text>
                                    </View>
                                </View>
                                <View flexDirection='row'>
                                    <Image style={styles.voiceHomeIcon} source={voiceUserIcon}/>
                                    <View
                                        style={{backgroundColor:'#ffba13',borderRadius:8,paddingLeft:5,paddingRight:5,height:12,alignSelf:'center'}}>
                                        <Text numberOfLines={1}
                                              style={[styles.rowStatusText,{color:Color.blackColor}]}>{userCount}人</Text>
                                    </View>
                                </View>
                            </View>
                            <View
                                style={{position: "absolute",bottom:8,right:25,backgroundColor:'#bc06ad',borderRadius:2,left:15}}>
                                <Text numberOfLines={1}
                                      style={[styles.rowStatusText,{fontSize:roomUserNameFonSize,color:'#cabddd',padding:roomUserNamePadding,paddingLeft:5,paddingRight:8}]}>{item.name}</Text>
                            </View>
                            {that.audioTypeImageView(audioData)}
                        </View>
                        <View style={{flex:1}}>
                            <View flexDirection='row'>
                                <Text numberOfLines={1} style={styles.rowTitle}>{title}</Text>
                                <CachedImage style={styles.voiceHomeLock} source={room_lock_icon}/>
                            </View>
                            <View style={[styles.renderRow]}>
                                <View flexDirection='row'
                                      style={{flex:4,justifyContent:'center',alignSelf:'center',paddingRight:10,paddingLeft:10}}>
                                    <View style={{flex:1}}>
                                        <CachedImage style={styles.userHeader}
                                                     type='audio'
                                                     source={(!user_1.avatar.length)?defaultNoHeader:({uri:user_1.avatar})}
                                                     defaultSource={icDefaultUser}/>
                                        <Text numberOfLines={1} style={styles.rowUserName}>{user_1.name}</Text>
                                    </View>
                                    <View style={{flex:1}}>
                                        <CachedImage style={styles.userHeader}
                                                     type='audio'
                                                     source={(!user_2.avatar.length)?defaultNoHeader:({uri:user_2.avatar})}
                                                     defaultSource={icDefaultUser}/>
                                        <Text numberOfLines={1} style={styles.rowUserName}>{user_2.name}</Text>
                                    </View>
                                    <View style={{flex:1}}>
                                        <CachedImage style={styles.userHeader}
                                                     type='audio'
                                                     source={(!user_3.avatar.length)?defaultNoHeader:({uri:user_3.avatar})}
                                                     defaultSource={icDefaultUser}/>
                                        <Text numberOfLines={1} style={styles.rowUserName}>{user_3.name}</Text>
                                    </View>
                                    <View style={{flex:1}}>
                                        <CachedImage style={styles.userHeader}
                                                     type='audio'
                                                     source={(!user_4.avatar.length)?defaultNoHeader:({uri:user_4.avatar})}
                                                     defaultSource={icDefaultUser}/>
                                        <Text numberOfLines={1} style={styles.rowUserName}>{user_4.name}</Text>
                                    </View>
                                </View>
                            </View>
                            <View flexDirection='row' style={{flex:1}}>
                                <View flexDirection='row' style={{flex:1.3}}>
                                    {that.audioLikePopView(item.like, audioLike)}
                                    {that.audioLikePopView(item.pop, audioPop)}
                                </View>
                                <View style={{flex:1}}>
                                    {that.audioTypeTitleView(audioData)}
                                </View>
                            </View>
                        </View>
                    </View>
                </CachedImage>

            </View>);
        }
    }

    /**
     * 隐藏
     */
    hideProgress() {
        if (that.state.showModelProgress) {
            that.setState({
                showModelProgress: false
            });
        }
    }

    /**
     *
     * @returns {*}
     */
    audioTypeView() {
        return (audioTypes.length == 0) ? null : (<View>
                <Text style={{color:(Color.voice_set_text_color),fontSize:14}}>{Language().select_audio_type}</Text>
                <ListView dataSource={this.state.audioDataSource.cloneWithRows(audioTypes)}
                          renderRow={this.audioTypeRenderRow}
                          removeClippedSubviews={false}
                          contentContainerStyle={styles.listViewStyle}/>
            </View>);
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     */
    audioTypeRenderRow(item, sectionId, rowId) {
        let sourceImage = that.state.audioTypeIndex == rowId ? selectAudioIcon : normalAudioIcon;
        return ( <View style={styles.cellBackStyle}>
            <TouchableOpacity onPress={()=>that.selectAudio(item,rowId)} activeOpacity={0.9}
                              style={{justifyContent:'center',backgroundColor:Color.transparent,flex:1}}>
                <View flexDirection='row'>
                    <Image style={styles.audioTypeIcon} source={sourceImage}/>
                    <Text numberOfLines={1}
                          style={{color:(Color.voice_set_text_color),fontSize:12,alignSelf:'center',marginLeft:5}}>{item.t}</Text>
                </View>
            </TouchableOpacity></View>);
    }

    /**
     * 点击对应的item
     * @param data
     */
    selectAudio(data, rowId) {
        that.setState({
            audioTypeIndex: rowId,
            audioTypeValue: data.k,
            audioDataSource: that.state.audioDataSource.cloneWithRows(audioTypes),
        });
    }

    /**
     * 统一处理native的消息
     * @param event
     */
    nativeMessage(event) {
        if (event && event.Data) {
            let data = JSON.parse(event.Data);
            switch (data.action) {
                case Constant.LEAVE_AUDIO_ROOM:
                    this.onShowProgressDialog(Language().loading_now);
                    InteractionManager.runAfterInteractions(() => {
                        this.props.fetchRecreationPageList(1, types.RECREATION_DATA, this.state.filterFlag);
                        this.doScrollToList();
                    });
                    break;
            }
        }
    }

}
const
    styles = StyleSheet.create({
        defaultBg: {
            alignSelf: 'center',
            width: ScreenWidth / 2,
            height: ScreenWidth / 2,
            resizeMode: 'contain',
        },
        voiceHome: {
            width: 18,
            height: 18,
            resizeMode: 'contain',
            alignSelf: 'center'
        },
        voiceSearch: {
            width: 25,
            height: 25,
            resizeMode: 'contain',
            alignSelf: 'center',
            marginRight: 5
        },
        voiceHomeText: {
            fontSize: 15,
            color: Color.colorWhite,
            alignSelf: 'center',
            padding: 0,
            paddingBottom: 0
        },
        voiceBt: {
            backgroundColor: Color.transparent,
            justifyContent: 'center',
            alignSelf: 'center'
        },
        renderRow: {
            justifyContent: 'center',
            padding: 1,
            flexDirection: 'row'
        },
        userHeader: {
            width: 40,
            height: 40,
            borderRadius: 20,
            alignSelf: 'center',
            marginLeft: 8,
            marginRight: 8
        },
        ownerHeader: {
            width: 100,
            height: 120,
            borderRadius: 5,
            alignSelf: 'center',
            top: 0
        },
        rowTitle: {
            color: Color.colorWhite,
            fontSize: 14,
            marginLeft: 15,
            fontWeight: 'bold',
            marginTop: 0,
            marginBottom: 5,
            flex: 1,
            alignSelf: 'flex-end',
            backgroundColor: Color.transparent
        },
        rowUserName: {
            color: '#cabddd',
            fontSize: 10,
            alignSelf: 'center',
            textAlign: 'center',
            marginTop: 3,
            backgroundColor: Color.transparent, height: 13
        },
        rowStatusText: {
            color: Color.colorWhite,
            fontSize: 10,
            alignSelf: 'center',
            backgroundColor: Color.transparent
        },
        voiceIcon: {
            width: 50,
            height: 35,
            resizeMode: 'contain',
            alignSelf: 'center'
        },
        voiceHomeIcon: {
            width: 13,
            height: 14,
            resizeMode: 'contain',
            alignSelf: 'center',
            marginRight: 1
        },
        actionButton: {
            backgroundColor: '#845fe4', justifyContent: 'center', height: 40, flex: 1, borderRadius: 5, margin: 10
        },
        loading: {
            position: 'absolute',
            backgroundColor: 'rgba(0, 0, 0, 0.6)',
            height: 60,
            width: 80,
            borderRadius: 6,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
        },
        dialogContainer: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
        },
        rowSeparator: {
            height: StyleSheet.hairlineWidth,
            backgroundColor: '#4C3B52',
        },
        voiceHomeLock: {
            width: 55,
            height: 37,
            resizeMode: 'contain',
            alignSelf: 'center',
        },
        roomNumber: {
            color: '#b09ce5',
            fontSize: 14,
            alignSelf: 'center',
            marginTop: 8
        }, image: {
            width: ScreenWidth - 30,
            height: 120,
            resizeMode: 'stretch',
        }, containerView: {
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'transparent',
        }, listViewStyle: {
            flexDirection: 'row',
            flexWrap: 'wrap',
            marginRight: 20,
            marginLeft: 20
        }, audioTypeIcon: {
            width: 18,
            height: 18,
            resizeMode: 'contain',
            alignSelf: 'center',
        },
        cellBackStyle: {
            width: cellW,
            height: cellH,
            marginLeft: vMargin,
            marginTop: hMargin,
            alignSelf: 'center',
            justifyContent: 'center',
        }, filterButtonStyle: {
            justifyContent: 'center',
            backgroundColor: Color.transparent,
            padding: 5,
            paddingLeft: 25,
            paddingRight: 25,
        }, filterText: {
            fontSize: 14,
            alignSelf: 'center',
            color: '#965f92'
        }, titleImage: {
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
            resizeMode: 'stretch',
            height: 50,
            width: ScreenWidth
        }, title: {
            fontSize: 14,
            color: 'gray',
            alignSelf: 'center', margin: 5
        }
    });
const mapDispatchToProps = dispatch => ({
        fetchRecreationPageList: (page, actionType, fliter) => dispatch(fetchRecreationPageList(page, actionType, fliter)),
        updateConfig: () => dispatch(checkOpenRegister()),
        updateFilter: (filter) => dispatch(updateFilter(filter)),
    }
);
const mapStateToProps = state => ({
    userInfo: state.userInfoReducer.data,
    isRefreshing: state.recreationReducer.isRefreshing,
    isNoMore: state.recreationReducer.isNoMore,
    data: state.recreationReducer.data,
    hostLocation: state.configReducer.location,
    configData: state.checkRegisterReducer.data,
    actionType: state.recreationReducer.types,
});
export default connect(mapStateToProps, mapDispatchToProps)(RecreationPageList);