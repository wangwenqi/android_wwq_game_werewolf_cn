/**
 * Created by wangxu on 2017/9/7.
 */
'use strict';
import React, {Component} from 'react';
import Language from '../../../resources/language';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Image,
    StyleSheet,
    View,
    Platform,
    Dimensions,
    TouchableOpacity,
    WebView,
    DeviceEventEmitter,
    NativeModules,
    NativeEventEmitter,
} from 'react-native';
import Color from '../../../resources/themColor';
import Toast from '../../../support/common/Toast';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import rnToNativeUtils from '../../../support/common/rnToNativeUtils';
import Loading from '../../../support/common/Loading';
import  * as Constant from '../../../support/common/constant';

const bgWebViewError = require('../../../resources/imgs/icon_webview_error.png');
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const {RNMessageSender}=NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
let webView;
let progress;
class Recreation extends Component {
    static propTypes = {
        token: React.PropTypes.string,
        tabName: React.PropTypes.string,
        userInfo: React.PropTypes.object,
        hostLocation: React.PropTypes.string
    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            loadEnd: false,
        }
    }

    componentDidMount() {
        webView = this.refs.webView;
        progress = this.refs.progressDialog;
        if (Platform.OS === 'android') {
            this.onNativeMessageLister = DeviceEventEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
        } else {
            this.onNativeMessageLister = rNMessageSenderEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
        }
    }

    componentWillUnmount() {
        this.onNativeMessageLister.remove();
    }

    render() {
        let webUrl = apiDefines.AUDIO_URL + this.props.token;
        if (this.props.hostLocation) {
            webUrl = webUrl + apiDefines.PARAMETER_LOCATION + this.props.hostLocation;
        }
        if (this.props.userInfo) {
            if (this.props.userInfo.id) {
                webUrl = webUrl + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id;
            }
        }
        return (
            <View style={{flex: 1,backgroundColor:Color.content_color}}>
                <WebView
                    style={{flex:1}}
                    ref="webView"
                    source={{uri: webUrl}}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    decelerationRate="normal"
                    startInLoadingState={true}
                    messagingEnabled={typeof this.props.onMessage === 'function'}
                    onMessage={this.onMessage}
                    renderError={this._webViewError.bind(this)}/>
                <Loading ref="progressDialog" loadingTitle={Language().operation_loading}/>
            </View>
        );
    }

    /**
     * 统一处理native的消息
     * @param event
     */
    nativeMessage(event) {
        if (event && event.Data) {
            let data = JSON.parse(event.Data);
            switch (data.action) {
                case Constant.LEAVE_AUDIO_ROOM:
                    this._onReloadButton();
                    break;
            }
        }
    }

    onMessage(event) {
        let data = event.nativeEvent.data;
        let Options = {
            needcallback: false,
            sync: false
        }
        if (data) {
            let nativeEventData = JSON.parse(data);
            switch (nativeEventData.functionName) {
                case 'enterAudioRoom':
                    let NativeData = {
                        action: nativeEventData.functionName,
                        params: nativeEventData.parameter,
                        options: Options
                    }
                    rnToNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
                    break;
                case 'showLoading':
                    if (progress) {
                        progress.show();
                    }
                    break;
                case 'hideLoading':
                    if (progress) {
                        progress.hide();
                    }
                    break;
                case 'refresh':
                    this._onReloadButton();
                    break;
                case 'toast':
                    let parameterData = nativeEventData.parameter;
                    if (parameterData && parameterData.message) {
                        Toast.show(parameterData.message)
                    }
                    break;
            }
        }
    }

    _onReloadButton() {
        if (webView) {
            webView.reload();
        }
    }

    _webViewError() {
        return (
            <View style={{flex: 1,backgroundColor:Color.content_color,justifyContent:'center'}}>
                <TouchableOpacity style={styles.webErrorBt} onPress={this._onReloadButton.bind(this)}>
                    <Image style={styles.webViewError} source={bgWebViewError}/>
                </TouchableOpacity>
            </View>);
    }
}
const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    webViewError: {
        alignSelf: 'center',
        width: ScreenWidth / 2 - 40,
        height: ScreenWidth / 2 - 60,
        resizeMode: 'contain',
        alignItems: 'center'
    },
    webErrorBt: {
        justifyContent: 'center',
        backgroundColor: Color.transparent,
        alignSelf: 'center',
    }
});
const mapDispatchToProps = dispatch => ({}
);
const mapStateToProps = state => ({
    token: state.userInfoReducer.token,
    userInfo: state.userInfoReducer.data,
    tabName: state.userInfoReducer.tabName,
    hostLocation: state.configReducer.location
});
export default connect(mapStateToProps, mapDispatchToProps)(Recreation);