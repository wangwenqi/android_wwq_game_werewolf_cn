/**
 * Created by wangxu on 2017/5/12.
 * 用户协议
 */
'use strict';
import React, {Component} from 'react';
import Language from '../../../resources/language';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import  * as apiDefines from '../../../support/common/gameApiDefines';

import {
    Container,
    Left,
    Text,
    Right,
    Body,
    Content,
    CardItem,
    Button
} from 'native-base';
import {StyleSheet, View, Platform, Dimensions, Image, WebView,TouchableOpacity} from 'react-native';
import Color from '../../../resources/themColor';
const {
    popRoute,
} = actions;
const bgBack = require('../../../resources/imgs/ic_back.png');
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const ScreenHeight = Dimensions.get('window').height;
const ScreenWidth = Dimensions.get('window').width;
const bgWebViewError = require('../../../resources/imgs/icon_webview_error.png');


class UserAgreement extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
    }

    onFinishPage() {
        this.props.finishPage('global');
    }

    render() {
        return (
            <Container style={{flex:1,backgroundColor:'#1B0737'}}>
                <CardItem
                    style={{backgroundColor:(Color.headColor),height:60,paddingTop:headPaddingTop,paddingBottom:0}}>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={this.onFinishPage.bind(this)}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf:'center',flex:3}}>
                    <Text style={{color: '#fff',fontSize: 18,alignSelf:'center'}}>{Language().userAgreement}</Text>
                    </Body>
                    <Right style={{flex:1}}/>
                </CardItem>
                <Container style={{flex:1,backgroundColor:(Color.colorWhite)}}>
                    <WebView
                        ref="webView"
                        style={{backgroundColor:(Color.colorWhite)}}
                        automaticallyAdjustContentInsets={false}
                        source={{uri: apiDefines.USER_AGREEMENT}}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        decelerationRate="normal"
                        startInLoadingState={true}
                        scalesPageToFit={true}
                        renderError={this._webViewError.bind(this)}/>
                </Container>
            </Container>
        );
    }

    /**
     *
     * @returns {XML}
     * @private
     */
    _webViewError() {
        return (<View style={{backgroundColor:Color.colorWhite,justifyContent:'center',position: 'absolute',top:0,bottom:0,left:0,right:0}}>
                <TouchableOpacity style={styles.webErrorBt} onPress={this._onReloadView.bind(this)}>
                    <Image style={[styles.webViewError]}
                           source={bgWebViewError}/>
                </TouchableOpacity>
            </View>);
    }

    _onReloadView() {
        if (this.refs.webView) {
            this.refs.webView.reload();
        }
    }
}
const styles = StyleSheet.create({
        bgBack: {
            width: 20,
            height: 20,
            resizeMode: 'contain',
            padding: 5
        }, webViewError: {
            alignSelf: 'center',
            width: ScreenWidth /3,
            height: ScreenWidth /3,
            resizeMode: 'contain',
        },  webErrorBt: {
            justifyContent: 'center',
            backgroundColor: 'rgba(0, 0, 0, 0)',
            alignSelf: 'center',
        },
    })
    ;
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
    }
);
export default connect(null, mapDispatchToProps,)(UserAgreement);