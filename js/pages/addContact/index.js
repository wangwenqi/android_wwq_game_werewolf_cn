/**
 * Created by wangxu on 2017/3/4.
 */
'use strict';
import React, {Component} from 'react';
import Language from '../../../resources/language';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Container,
    Header,
    Title,
    Button,
    Left,
    Text,
    Right,
    Body,
    Icon,
    Content,
    Item, Input
} from 'native-base';
const {
    popRoute,
} = actions;
class AddContact extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
    }

    onFinishPage() {
        this.props.finishPage('global');
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={this.onFinishPage.bind(this)}>
                            <Icon name={'arrow-back'}/>
                        </Button>
                    </Left>
                    <Body>
                    <Title>{Language().addContact}</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <Item regular style={{flex:1,marginRight:5,marginLeft:5,marginTop:5}}>
                        <Input placeholder='请输入对方昵称' style={{font: 8,padding: 10,flex:3}}/>
                        <Right style={{marginRight:10,flex:1}}>
                            <Button small>
                                <Text>{Language().search}</Text>
                            </Button>
                        </Right>
                    </Item>
                </Content>
            </Container>
        );
    }
}
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key))
    }
);
export default connect(null, mapDispatchToProps,)(AddContact);
