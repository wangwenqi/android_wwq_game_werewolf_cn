/**
 * Created by wangxu on 2017/8/9.
 * 开发者设置
 */
'use strict';
import React, {Component} from 'react';
import Language from '../../../resources/language';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {Container, Left, Text, Right, Body, Item, Input, CardItem, Button} from 'native-base';
import {Image, StyleSheet, View, Platform, Dimensions, TouchableOpacity} from 'react-native';
import Color from '../../../resources/themColor';
import Toast from '../../../support/common/Toast';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import {location} from '../../../support/actions/configActions';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import Dropdown from '../../../support/common/Dropdown';
import  * as apiDefines from '../../../support/common/gameApiDefines';

const {
    popRoute,
    pushRoute,
} = actions;
const bgBack = require('../../../resources/imgs/ic_back.png');
const launcher = require('../../../resources/imgs/ic_launcher.png');
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const headHeight = (Platform.OS === 'ios') ? 60 : 50;

const ScreenHeight = Dimensions.get('window').height;
const ScreenWidth = Dimensions.get('window').width;
const rightArrow = require('../../../resources/imgs/bg_arrow_right.png');
const developer_pw = 'xchl8192';
const TW = 'tw';
const CN = 'cn';
const MY = 'my';
let hostListArray = new Array();
let host1 = 'http://staging2.intviu.cn:7201';
let host2 = 'http://staging.intviu.cn:8201';
let host3 = 'http://staging.intviu.cn:8200';
let host4 = 'https://werewolf.xiaobanhui.com';
class Developer extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        location: React.PropTypes.string,
        setLocation: React.PropTypes.func
    }

    constructor(props) {
        super(props);
        hostListArray.push(apiDefines.URL_HOST);
        hostListArray.push(host1);
        hostListArray.push(host2);
        hostListArray.push(host3);
        hostListArray.push(host4);
        this.state = {
            showDeveloperView: false,
            password: '',
            host: apiDefines.URL_HOST,
            newHost: apiDefines.URL_HOST,
            hostList: hostListArray
        }
    }

    componentDidMount() {
        AsyncStorageTool.getStorageData(AsyncStorageTool.HOST_URL, (hostData) => {
            if (hostData) {
                if (hostData.hostList) {
                    hostListArray = hostData.hostList;
                    this.setState({
                        hostList: hostListArray
                    })
                }
            }
        });
    }

    render() {
        return (
            <Container style={{flex:1,backgroundColor:Color.content_color}}>
                <CardItem
                    style={{backgroundColor:(Color.headColor),height:headHeight,paddingTop:headPaddingTop,paddingBottom:0}}>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={this.onFinishPage.bind(this)}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf:'center',flex:3}}>
                    <Text
                        style={{color: Color.colorWhite,fontSize: 18,alignSelf:'center'}}>开发者模式</Text>
                    </Body>
                    <Right style={{flex:1}}/>
                </CardItem>
                <Container style={{flex:1,backgroundColor:Color.colorWhite}}>
                    <View flexDirection='row' style={{margin:5}}>
                        <Item style={{margin:5,borderColor:(Color.transparent),flex:1}}>
                            <Input password={true} secureTextEntry={true}
                                   placeholder='输入操作密码'
                                   placeholderTextColor={Color.loginTextColor}
                                   style={{borderWidth: 1, borderColor:(Color.loginBorderColor), borderRadius: 3,paddingLeft:10,fontSize:14,height:40}}
                                   onChangeText={(text)=>this.onChangePassword(text)}/>
                        </Item>
                        <TouchableOpacity onPress={this.checkPassword.bind(this)}
                                          style={{height:38,backgroundColor:Color.phoneLoginButColor,justifyContent:'center',width:80,borderRadius:3,alignSelf:'center'}}
                                          activeOpacity={0.6}>
                            <Text style={{color:Color.colorWhite,alignSelf:'center'}}>{Language().confirm}</Text>
                        </TouchableOpacity>
                    </View>
                </Container>
                <Container style={{flex:4,backgroundColor:Color.colorWhite}}>
                    {this.developerView()}
                </Container>
                <Container style={{flex:4,backgroundColor:Color.colorWhite}}>
                    {this.hostView()}
                </Container>
            </Container>
        );
    }

    checkPassword() {
        let {password} = this.state;
        if (!password.length) {
            Toast.show('密码不能为空');
            return;
        }
        if (password == developer_pw) {
            this.setState({
                showDeveloperView: true
            });
        } else {
            Toast.show('密码错误，无下一步操作权限');
        }
    }

    hostView() {
        return this.state.showDeveloperView ? (<View>
                <View style={styles.rowSeparator}></View>
                <View flexDirection='row'
                      style={ { paddingTop: 5,paddingBottom: 5,height: 40,paddingLeft: 10,width:ScreenWidth, backgroundColor: (Color.countryListColor),justifyContent:'center'} }>
                    <Text
                        style={{color: (Color.countryLetterColor),fontWeight: '400',paddingLeft: 5,flex:1,alignSelf:'center'}}>
                        Host：{this.state.host}
                    </Text>
                </View>
                <View style={[styles.rowSeparator]}></View>
                {this.hostListView()}
                <View style={styles.rowSeparator}></View>
                <View flexDirection='row' style={{margin:5,marginTop:15}}>
                    <Item style={{margin:5,borderColor:(Color.transparent),flex:1}}>
                        <Input
                            placeholder='输入服务器地址'
                            placeholderTextColor={Color.loginTextColor}
                            style={{borderWidth: 1, borderColor:(Color.loginBorderColor), borderRadius: 3,paddingLeft:10,fontSize:14,height:40}}
                            onChangeText={(text)=>this.onChangeHost(text)} value={this.state.newHost}/>
                    </Item>
                    <TouchableOpacity onPress={this.updateHost.bind(this)}
                                      style={{height:38,backgroundColor:Color.phoneLoginButColor,justifyContent:'center',width:80,borderRadius:3,alignSelf:'center'}}
                                      activeOpacity={0.6}>
                        <Text style={{color:Color.colorWhite,alignSelf:'center'}}>{Language().save}</Text>
                    </TouchableOpacity>
                </View>
            </View>) : null
    }

    developerView() {
        return this.state.showDeveloperView ? (  <View>
                <View style={styles.rowSeparator}></View>
                <View
                    style={ { paddingTop: 5,paddingBottom: 5,height: 40,paddingLeft: 10,width:ScreenWidth, backgroundColor: (Color.countryListColor),justifyContent:'center'} }>
                    <Text style={{  color: (Color.countryLetterColor),fontWeight: '400',paddingLeft: 5}}>
                        Location：{this.formatLocation()}
                    </Text>
                </View>
                <View style={[styles.rowSeparator,{marginBottom:20}]}></View>
                <View style={styles.rowSeparator}></View>
                <TouchableOpacity onPress={this.openCN.bind(this)}
                                  style={{height:40,backgroundColor:Color.colorWhite,justifyContent:'center'}}
                                  activeOpacity={0.6}>
                    <View flexDirection='row' style={{marginLeft:20,marginRight:20,alignSelf:'center'}}>
                        <Left>
                            <Text
                                style={{color:Color.about_text_color,fontSize: 16,alignSelf:'flex-start'}}>{Language().china}</Text>
                        </Left>
                        <Right style={{flex:1}}>
                            <Image source={rightArrow}
                                   style={styles.rightArrow}></Image>
                        </Right>
                    </View>
                </TouchableOpacity>
                <View style={styles.rowSeparator}></View>
                <TouchableOpacity onPress={this.openTW.bind(this)}
                                  style={{height:40,backgroundColor:Color.colorWhite,justifyContent:'center'}}
                                  activeOpacity={0.6}>
                    <View flexDirection='row' style={{marginLeft:20,marginRight:20,alignSelf:'center'}}>
                        <Left>
                            <Text
                                style={{color:Color.about_text_color,fontSize: 16,alignSelf:'flex-start'}}>{Language().taiwan}</Text>
                        </Left>
                        <Right style={{flex:1}}>
                            <Image source={rightArrow}
                                   style={styles.rightArrow}></Image>
                        </Right>
                    </View>
                </TouchableOpacity>
                <View style={styles.rowSeparator}></View>
                <TouchableOpacity onPress={this.openMY.bind(this)}
                                  style={{height:40,backgroundColor:Color.colorWhite,justifyContent:'center'}}
                                  activeOpacity={0.6}>
                    <View flexDirection='row' style={{marginLeft:20,marginRight:20,alignSelf:'center'}}>
                        <Left>
                            <Text
                                style={{color:Color.about_text_color,fontSize: 16,alignSelf:'flex-start'}}>{Language().malaysia}</Text>
                        </Left>
                        <Right style={{flex:1}}>
                            <Image source={rightArrow}
                                   style={styles.rightArrow}></Image>
                        </Right>
                    </View>
                </TouchableOpacity>
                <View style={styles.rowSeparator}></View>
            </View>) : null
    }

    hostListView() {
        return <Dropdown style={[styles.dropDown]}
                         dropdownStyle={styles.dropdownDropdown}
                         options={this.state.hostList}
                         renderRow={this._dropdownRenderRow.bind(this)}
                         onSelect={(idx, value) => this._dropdownOnSelect(idx, value)}>
            <View flexDirection='row' style={{height:30,paddingLeft:20}}>
                <Text style={{fontSize:10,alignSelf:'center',textAlign:'center'}}>点击选择地址>>>>>>>>>>></Text>
            </View>
        </Dropdown>;
    }

    onChangePassword(text) {
        this.setState({
            password: text
        });
    }

    onChangeHost(text) {
        this.setState({
            newHost: text
        });
    }

    /**
     * host
     * @param rowData
     * @param rowID
     * @param highlighted
     * @returns {XML}
     * @private
     */
    _dropdownRenderRow(rowData, rowID, highlighted) {
        return (
            <TouchableOpacity activeOpacity={0.6}>
                <View style={[styles.dropdownRow]}>
                    <Text
                        style={{marginHorizontal: 4,fontSize: 13,color:Color.dropdown_text_color,flex:1,textAlign:'left',paddingLeft:10}}>
                        {rowData}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    /**
     * 选中的host
     * @param idx
     * @param value
     * @private
     */
    _dropdownOnSelect(idx, value) {
        this.setState({
            newHost: value
        });
    }

    updateHost() {
        let {newHost} = this.state;
        if (!newHost.length) {
            Toast.show('host不能为空');
            return;
        }
        if (!this.checkUrl(newHost)) {
            Toast.show('请输入正确格式的地址');
            return;
        }

        let hostArray = this.state.hostList;
        if (!this.array_contain(hostArray, newHost)) {
            hostArray.push(newHost);
        }
        let hostData = {
            host: newHost,
            hostList: hostArray
        }
        Toast.show('保存成功');
        AsyncStorageTool.updateHost(JSON.stringify(hostData), newHost);
    }

    /**
     * 是否包含在集合中
     * @param array
     * @param obj
     * @returns {boolean}
     */
    array_contain(array, obj) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] == obj)//如果要求数据类型也一致，这里可使用恒等号===
                return true;
        }
        return false;
    }

    /**
     * 检查是否是正常的URL
     * @param urlString
     * @returns {boolean}
     */
    checkUrl(urlString) {
        let reg = /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?/;
        if (reg.test(urlString)) {
            return true;
        }
        return false;
    }

    formatLocation() {
        let location = this.props.location;
        if (location) {
            switch (location) {
                case CN:
                    return Language().china;
                case TW:
                    return Language().taiwan;
                case MY:
                    return Language().malaysia;
                default:
                    return '未设置';
            }
        } else {
            return '未设置';
        }
    }

    openTW() {
        this.props.setLocation(TW);
        rnRoNativeUtils.onSetLocation(TW);
        Toast.show('设置成功');
        this.onFinishPage();
    }

    openCN() {
        this.props.setLocation(CN);
        rnRoNativeUtils.onSetLocation(CN);
        Toast.show('设置成功');
        this.onFinishPage();
    }

    openMY() {
        this.props.setLocation(MY);
        rnRoNativeUtils.onSetLocation(MY);
        Toast.show('设置成功');
        this.onFinishPage();
    }

    onFinishPage() {
        this.props.finishPage('global');
    }
}

const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: 'lightgray',
    },
    rightArrow: {
        width: 13, height: 13, resizeMode: 'contain'
    }, dropdownDropdown: {
        width: ScreenWidth - 100,
        borderRadius: 3,
        height: 90,
        marginTop: 10
    },
    dropdownRow: {
        flexDirection: 'row',
        height: 30,
        borderWidth: 0,
        borderRadius: 3,
        alignItems: 'center',
        backgroundColor: '#F8F7FA',
    },
});
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        setLocation: (lc) => dispatch(location(lc))
    }
);
const mapStateToProps = state => ({
    location: state.configReducer.location
});
export default connect(mapStateToProps, mapDispatchToProps,)(Developer);