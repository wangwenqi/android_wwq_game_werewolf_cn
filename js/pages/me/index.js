/**
 * Created by wangxu on 2017/6/9.
 */
'use strict';
import React, {Component} from 'react';
import Language from '../../../resources/language';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Container,
    Button,
    Left,
    Text,
    Right,
    Body, CardItem, Content
} from 'native-base';
import {Image, StyleSheet, View, Platform, Dimensions, TouchableOpacity} from 'react-native';
import Color from '../../../resources/themColor';
import * as SCENES from '../../../support/actions/scene';
import * as gameApiDefines from '../../../support/common/gameApiDefines';
import Toast from '../../../support/common/Toast';
import ImageManager from '../../../resources/imageManager';
import UtilsTool from '../../../support/common/UtilsTool';

import CachedImage from '../../../support/common/CachedImage';
import CustomProgressBar from '../../../support/common/ProgressBar';
import  * as Constant from '../../../support/common/constant';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
const {
    popRoute,
    pushRoute,
} = actions;
const ScreenHeight = Dimensions.get('window').height;
const arrows = require('../../../resources/imgs/bg_arrow_right.png');
const male = require('../../../resources/imgs/ic_male.png');
const female = require('../../../resources/imgs/ic_female.png');
const bgShare = require('../../../resources/imgs/me_icon_share.png');
const bgSetting = require('../../../resources/imgs/me_icon_settings.png');
const bgTotal = require('../../../resources/imgs/me_icon_total.png');
const bgWin = require('../../../resources/imgs/me_icon_win.png');
const bgLose = require('../../../resources/imgs/me_icon_lose.png');
const bgPopular = require('../../../resources/imgs/icon_popular.png');
const bgRecordDetail = require('../../../resources/imgs/ic_record_detail.png');
const bgFamily = require('../../../resources/imgs/ic_family.png');
const bgDefaultFamily = require('../../../resources/imgs/ico_family_default.png');
const roleType = {
    manager: 1 << 0,
    teacher: 1 << 1
}
class Me extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        openPage: React.PropTypes.func,
        userInfoData: React.PropTypes.object,
        configData: React.PropTypes.object,
    }

    constructor(props) {
        super(props);
    }

    onFinishPage() {
        this.props.finishPage('global');
    }

    render() {
        let signature = Language().prompt_signature;
        let signatureColor = '#666666';
        let gender = female;
        let game = null;
        let image = '';
        let name = '';
        let address = '';
        let role = null;
        let custom = '';
        let uid = 0;
        let user = this.props.userInfoData;
        let popular = 0;
        let userLevelTitle;
        let userLevelSource;
        let userLevelStar = 1;
        let userExperience = 0;
        let userLevelData;

        if (user != null) {
            game = this.props.userInfoData.game;
            if (user.image != null) {
                image = user.image;
            }
            if (user.signature != null && user.signature != '') {
                signature = user.signature;
                signatureColor = Color.setting_text_color;
            }
            name = user.name;
            if (user.sex == 1) {
                gender = male;
            }
            if (user.location) {
                if (user.location.address) {
                    address = user.location.address;
                }
            }
            if (user.role) {
                role = user.role;
                if (role.custom) {
                    custom = role.custom;
                }
            }
            if (user.uid) {
                uid = user.uid;
            }
            if (user.popular) {
                popular = user.popular;
            }
            if (user.active) {
                userLevelData = user.active;
                if (userLevelData) {
                    userLevelTitle = userLevelData.title;
                    userExperience = userLevelData.experience;
                    if (userLevelData.star) {
                        userLevelStar = userLevelData.star;
                    }
                    UtilsTool.userLevelSource(userLevelData.type, (source) => {
                        userLevelSource = source;
                    });
                }
            }
        } else {
            return (null);
        }
        let level = 0;
        let win = 0;
        let lose = 0;
        let odds = 0;
        let escapeRate = 0;
        let escape = 0;
        if (game != null && game.level != null && game.lose != null) {
            level = game.level;
            win = game.win;
            lose = game.lose;
            escape = game.escape;
            if ((win + lose + escape) > 0) {
                odds = win / (win + lose + escape) * 100;
                odds = odds.toFixed(0);
                escapeRate = escape / (win + lose + escape) * 100;
                escapeRate = escapeRate.toFixed(0);
            }
        }
        return (
            <Container style={{flex:1,backgroundColor:Color.colorWhite}}>
                <Content style={{flex:1,backgroundColor:Color.colorWhite}} showsVerticalScrollIndicator={false}>
                    <CardItem style={{backgroundColor:(Color.colorWhite),paddingBottom:17,paddingTop:28}} button
                              onPress={this.doUserDetail.bind(this)}>
                        <Left>
                            <View style={{justifyContent:'center'}}>
                                <View style={{alignSelf:'center'}}>
                                    <CachedImage
                                        style={styles.headThumbnail}
                                        source={(image=='')?(require('../../../resources/imgs/ic_default_user.png')):{uri:image}}
                                        defaultSource={require('../../../resources/imgs/ic_default_user.png')}/>
                                    <Image source={gender} style={styles.gender}/>
                                </View>
                                {this.uidView(uid)}
                            </View>
                            <Body style={{marginLeft:15,flex:12}}>
                            <Text style={{color:'#000000',fontWeight: '400',fontSize: 16}}
                                  numberOfLines={1}>{name}</Text>
                            <View flexDirection='row' style={{marginTop:5}}>
                                <Text numberOfLines={1}
                                      style={{color:(signatureColor),fontSize: 11,flex:1}}>{signature}</Text>
                            </View>

                            <View flexDirection='row' style={{marginTop:5}}>
                                <Text numberOfLines={1}
                                      style={{color:'#ec6f4e',fontSize: 14,fontWeight: '400',padding:0}}>LV.{level}</Text>
                                {this.popularityView(popular)}
                            </View>
                            {custom ? (<View flexDirection='row'>
                                    <View
                                        style={{backgroundColor:'#ff9ec8',marginTop:5,borderRadius:10,marginRight:5,justifyContent:'center'}}>
                                        <Text numberOfLines={1}
                                              style={{color:'#2d1c4c',fontSize: 12,fontWeight: '400',textAlign:'center',padding:0.5,paddingRight:5,paddingLeft:5,backgroundColor:Color.transparent}}>{custom}</Text>
                                    </View>
                                </View>) : null}
                            {custom ? null : this.roleView(role)}
                            </Body>
                            <Right>
                                <Image source={arrows}
                                       style={styles.bgBack}></Image>
                            </Right>
                        </Left>
                    </CardItem>
                    <View style={styles.rowSeparator}/>

                    <View style={styles.separator}/>
                    <View style={styles.rowSeparator}/>

                    <View flexDirection='row' style={{backgroundColor:Color.colorWhite,marginTop:5}}>
                        <Image source={bgFamily}
                               style={[styles.bgGame,{marginLeft:20,width:20,height:20,marginRight:3}]}/>
                        <Text
                            style={{color:'#150435',fontSize: 14,padding:5,flex:1}}>{Language().group_family}</Text>
                    </View>
                    <CardItem style={{backgroundColor:(Color.colorWhite),padding:15,paddingBottom:10,paddingTop:0}}
                              button activeOpacity={0.6} onPress={this.openFamilyDetail.bind(this)}>
                        {this.familyDetailView()}
                        <Image source={arrows}
                               style={styles.bgBack}/>
                    </CardItem>
                    <View style={styles.rowSeparator}/>
                    <View style={styles.separator}/>
                    <View style={styles.rowSeparator}/>
                    <TouchableOpacity onPress={this.openRankInstruction.bind(this)}
                                      activeOpacity={0.6}
                                      style={{backgroundColor:Color.transparent,justifyContent:'center'}}>
                        <View flexDirection='row' style={{backgroundColor:Color.colorWhite,marginTop:5}}>
                            {userLevelSource ? ( <CachedImage source={userLevelSource}
                                                              style={[styles.bgGame,{width:40,height:40,margin:5,marginLeft:20}]}/>) : (
                                    <View style={{width:35,height:35,marginBottom:8,marginTop:8}}/>)}

                            <Text
                                style={{color:'#ec6f4e',fontSize: 16,padding:0,flex:1,alignSelf:'center'}}>{userLevelTitle + userLevelStar + Language().level_star}</Text>
                            <Text
                                style={{color:'#ec6f4e',fontSize: 16,padding:0,flex:1.5,alignSelf:'center'}}>{Language().military_power + userExperience}</Text>
                            <Image source={arrows}
                                   style={[styles.bgBack,{alignSelf:'center',marginRight:15}]}/>
                        </View>

                    </TouchableOpacity>
                    <CardItem style={{backgroundColor:(Color.colorWhite),padding:15,paddingBottom:10,paddingTop:5}}
                              button activeOpacity={0.6} onPress={this.openRecordDetails.bind(this)}>
                        <View style={{flex:1}}>
                            <View flexDirection='row' style={{justifyContent:'center',marginRight:20,marginLeft:20}}>
                                <View flexDirection='row' style={{flex:1,justifyContent:'center'}}>
                                    <Image source={bgTotal} style={styles.bgGame}></Image>
                                    <Text numberOfLines={1}
                                          style={{color:(Color.record_text_color),fontSize: 14,alignSelf:'center',padding:5}}>{win + lose}{Language().bureau}</Text>
                                </View>
                                <View flexDirection='row' style={{flex:1,justifyContent:'center'}}>
                                    <Image source={bgWin} style={styles.bgGame}></Image>
                                    <Text numberOfLines={1}
                                          style={{color:(Color.record_text_color),fontSize: 14,alignSelf:'center',padding:5}}>{win}{Language().bureau}</Text>

                                </View>
                                <View flexDirection='row' style={{flex:1,justifyContent:'center'}}>
                                    <Image source={bgLose} style={styles.bgGame}></Image>
                                    <Text numberOfLines={1}
                                          style={{color:(Color.record_text_color),fontSize: 14,alignSelf:'center',padding:5}}>{lose}{Language().bureau}</Text>
                                </View>
                            </View>
                            <View flexDirection='row'>
                                <View style={{flex:1,justifyContent:'center',margin:10,marginBottom:5}}>
                                    <View flexDirection='row' style={{justifyContent:'center'}}>
                                        <Text
                                            style={{color:(Color.win_color),fontSize: 14,alignSelf:'center',padding:3}}>{Language().odds}</Text>
                                        <Text numberOfLines={1}
                                              style={{color:(Color.win_color),fontSize: 14,alignSelf:'center',padding:3}}>{odds}%</Text>
                                    </View>
                                    <CustomProgressBar progress={odds*0.01} progressColor={'#ffac03'}
                                                       bufferColor={Color.colorWhite}
                                                       containerStyle={{borderColor: Color.win_color, borderWidth:1,height:14}}
                                                       borderRadius={8}/>
                                </View>
                                <View style={{flex:1,justifyContent:'center',margin:10,marginBottom:5}}>
                                    <View flexDirection='row'
                                          style={{justifyContent:'center'}}>
                                        <Text
                                            style={{color:(Color.lose_color),fontSize: 14,alignSelf:'center',padding:3}}>{Language().leave}</Text>
                                        <Text numberOfLines={1}
                                              style={{color:(Color.lose_color),fontSize: 14,alignSelf:'center',padding:3}}>{escapeRate}%</Text>
                                    </View>
                                    <CustomProgressBar progress={escapeRate*0.01} progressColor={'#9b92ff'}
                                                       bufferColor={Color.colorWhite}
                                                       containerStyle={{borderColor:Color.lose_color, borderWidth:1,height:14}}
                                                       borderRadius={8}/>
                                </View>
                            </View>
                        </View>
                        <Image source={arrows}
                               style={styles.bgBack}/>
                    </CardItem>
                    <View style={styles.rowSeparator}/>
                    <View style={styles.separator}/>
                    <View style={styles.rowSeparator}/>
                    {this.houseView()}
                    <View style={styles.rowSeparator}/>
                    <CardItem style={{backgroundColor:(Color.colorWhite),height:50}} button
                              onPress={this.onShareMessage.bind(this)}>
                        <Left>
                            <Image source={bgShare} style={styles.bgIcon}/>
                            <Text
                                style={{color:'#845fe4',fontSize: 14,alignSelf:'center'}}>{Language().share_friend}</Text>
                        </Left>
                        <Right>
                            <Image source={arrows}
                                   style={styles.bgBack}/>
                        </Right>
                    </CardItem>
                    <View style={styles.rowSeparator}/>
                    <View style={styles.separator}/>
                    <View style={styles.rowSeparator}/>
                    <CardItem style={{backgroundColor:(Color.colorWhite),height:50}} button
                              onPress={this.onSetting.bind(this)}>
                        <Left>
                            <Image source={bgSetting} style={styles.bgIcon}/>
                            <Text
                                style={{color:'#845fe4',fontSize: 14,alignSelf:'center'}}>{Language().setting}</Text>
                        </Left>
                        <Right>
                            <Image source={arrows}
                                   style={styles.bgBack}/>
                        </Right>
                    </CardItem>
                    <View style={styles.rowSeparator}/>
                </Content>
            </Container>
        );
    }

    /**
     *
     * @param uid
     */
    uidView(uid) {
        if (uid) {
            return (
                <Text
                    style={{color:Color.blackColor,fontSize: 12,alignSelf:'center',textAlign:'center',backgroundColor:Color.colorWhite,marginLeft:0}}>ID:{uid}</Text>);
        }
    }

    houseView() {
        {/*开启房契*/
        }
        let needShowHouse = false;
        if (this.props.configData) {
            needShowHouse = this.props.configData.purchase_room;
        }
        return needShowHouse ? ( <View>
                <CardItem style={{backgroundColor:(Color.colorWhite),height:50}} button
                          onPress={this.openMyRoom.bind(this)}>
                    <Left>
                        <Image source={ImageManager.myHouseIcon} style={styles.bgIcon}/>
                        <Text style={{color:'#845fe4',fontSize: 14,alignSelf:'center'}}>{Language().my_house}</Text>
                    </Left>
                    <Right>
                        <Image source={arrows} style={styles.bgBack}/>
                    </Right>
                </CardItem>
                <View style={styles.rowSeparator}/>
                <View style={styles.separator}/>
            </View>) : null;
    }

    /**
     * 进入家族详情
     */
    openFamilyDetail() {
        let groupInfo = this.props.userInfoData.group;
        if (groupInfo && groupInfo.group_id) {
            let nativeData = {
                action: Constant.ACTION_SHOW_FAMILY_INFO,
                params: {group_id: groupInfo.group_id},
                type: Constant.RN_NATIVE
            }
            rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
        } else {
            try {
                this.props.openPage(SCENES.SCENE_GROUP_LIST, 'global');
            } catch (e) {
                console.log(e)
            }
        }
    }

    /**
     *
     * @param levelVal
     * @param levelImage
     * @returns {*}
     */
    getGroupLevelICon(levelVal, levelImage) {
        let imageSource;
        switch (levelVal) {
            case 0:
            case 1:
                imageSource = ImageManager.ic_group_level_val_1;
                break;
            case 2:
                imageSource = ImageManager.ic_group_level_val_2;
                break;
            case 3:
                imageSource = ImageManager.ic_group_level_val_3;
                break;
            case 4:
                imageSource = ImageManager.ic_group_level_val_4;
                break;
            case 5:
                imageSource = ImageManager.ic_group_level_val_5;
                break;
            case 6:
                imageSource = ImageManager.ic_group_level_val_6;
                break;
            case 7:
                imageSource = ImageManager.ic_group_level_val_7;
                break;
            case 8:
                imageSource = ImageManager.ic_group_level_val_8;
                break;
            case 9:
                imageSource = ImageManager.ic_group_level_val_9;
                break;
            case 10:
                imageSource = ImageManager.ic_group_level_val_10;
                break;
        }
        if (!imageSource) {
            if (levelImage) {
                imageSource = {uri: levelImage}
            } else {
                imageSource = ImageManager.ic_group_level_val_1;
            }
        }
        return imageSource;
    }

    /**
     *
     */
    familyDetailView() {
        if (this.props.userInfoData) {
            let groupInfo = this.props.userInfoData.group;
            let levelVal = 0;
            if (groupInfo) {
                if (groupInfo.level_val) {
                    levelVal = groupInfo.level_val;
                }
                return (<View style={{flex:1}}>
                    <View flexDirection='row' style={{marginLeft:25}}>
                        <CachedImage
                            type='family'
                            style={{width: 50, height: 50,borderRadius: 25,alignSelf:'center'}}
                            defaultSource={bgDefaultFamily}
                            source={groupInfo.image?{uri:groupInfo.image}:bgDefaultFamily}/>
                        <View style={{marginLeft:8,alignSelf:'center'}}>
                            <View flexDirection='row' style={{marginBottom:0}}>
                                <Text
                                    numberOfLines={1}
                                    style={{color:'#808080',fontSize: 12,alignSelf:'center'}}>{groupInfo.name}</Text>
                                <Image
                                    style={{width: 25, height: 25,resizeMode: 'contain',alignSelf:'center',marginLeft:5}}
                                    source={this.getGroupLevelICon(levelVal,groupInfo.level_image)}/>
                            </View>
                            <View flexDirection='row'>
                                <View
                                    style={{borderRadius:8,backgroundColor:'#845fe4',justifyContent:'center'}}>
                                    <Text
                                        style={{color:(Color.prompt_text_color),fontSize: 10,paddingLeft:5,paddingRight:5,alignSelf:'center',backgroundColor:Color.transparent}}>{groupInfo.member_count}人</Text>
                                </View>
                            </View>
                            <Text numberOfLines={1}
                                  style={{color:'#808080',fontSize: 11,marginTop:2}}>{groupInfo.gid}</Text>
                        </View>
                    </View>
                </View>);
            } else {
                return (<View style={{flex:1}}>
                    <Text
                        style={{color:(Color.prompt_text_color),fontSize: 12,padding:5,marginLeft:25}}>{Language().no_group_message_prompt}</Text>
                </View>);
            }
        } else {
            return (<View style={{flex:1}}>
                <Text
                    style={{color:(Color.prompt_text_color),fontSize: 12,padding:5,marginLeft:25}}>{Language().no_group_message_prompt}</Text>
            </View>);
        }
    }

    /**
     * 更新信息详情
     */
    doUserDetail() {
        try {
            this.props.openPage(SCENES.SCENE_CONTACT_DETAIL, 'global');
        } catch (e) {
            console.log(e)
        }
    }

    /**
     * 设置
     */
    onSetting() {
        try {
            this.props.openPage(SCENES.SCENE_SETTING, 'global');
        } catch (e) {
            console.log(e)
        }

    }

    /**
     * 分享
     */
    onShareMessage() {
        rnRoNativeUtils.onShareMessage(this.props.userInfoData.name);
    }

    /**
     * 地理位置
     * @param address
     * @returns {*}
     */
    locationView(address) {
        if (address) {
            return (<Text numberOfLines={1}
                          style={{color:(Color.setting_text_color),fontSize: 12,paddingTop:2}}>{address}</Text>)
        } else {
            return null;
        }
    }

    /**
     * 用户身份
     * @param role
     * @returns {*}
     */
    roleView(role) {
        let content = null;
        let type = 0;
        if (role) {
            type = role.type;
        }
        if ((type & roleType.teacher) != 0) {
            content = (<View flexDirection='row'>
                <View
                    style={{backgroundColor:'#ff9ec8',marginTop:5,borderRadius:10,marginRight:5,justifyContent:'center'}}>
                    <Text numberOfLines={1}
                          style={{color:'#2d1c4c',fontSize: 12,fontWeight: '400',textAlign:'center',padding:0.5,paddingRight:8,paddingLeft:8,backgroundColor:Color.transparent}}>{Language().teacher}</Text>
                </View>
            </View>);
        }
        if ((type & roleType.manager) != 0) {
            content = <View flexDirection='row'>
                {content}
                <View style={{backgroundColor:'#AEE4FD',marginTop:5,borderRadius:10,justifyContent:'center'}}>
                    <Text numberOfLines={1}
                          style={{color:'#2d1c4c',fontSize: 12,fontWeight: '400',textAlign:'center',padding:0.5,paddingRight:8,paddingLeft:8,backgroundColor:Color.transparent}}>{Language().manager}</Text>
                </View>
            </View>
        }
        return content;
    }

    /**
     * 人气值view
     * @param popular
     * @returns {*}
     */
    popularityView(popular) {
        return popular ? (<View style={{marginLeft:30,flexDirection: 'row',justifyContent:'center'}}>
                <Image style={{width:13,height:13,alignSelf:'center', resizeMode: 'contain',}} source={bgPopular}/>
                <Text
                    style={{color:'#ef8468',fontSize: 14,fontWeight: '400',padding:0}}>{Language().popular}</Text>
                <Text
                    style={{color:'#ef8468',fontSize: 14,fontWeight: '400',padding:0,marginLeft:5}}>{popular}</Text>
            </View>) : null;
    }

    /**
     * 战绩详情
     */
    openRecordDetails() {
        // 启动战绩
        // action "standings"
        // type "RN_Native"
        let NativeData = {
            action: Constant.STANDINGS,
            type: Constant.RN_NATIVE
        }
        rnRoNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }

    /**
     *
     */
    openRankInstruction() {
        rnRoNativeUtils.openWebView(gameApiDefines.RANK_INSTRUCTION);
    }

    /**
     *
     */
    openMyRoom() {
        try {
            this.props.openPage(SCENES.SCENE_HOUSING_ESTATES, 'global')
        } catch (e) {
        }
    }
}

const styles = StyleSheet.create({
    bgBack: {
        width: 5,
        height: 15,
        resizeMode: 'contain',
    },
    bgIcon: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
    },
    bgGame: {
        width: 22,
        height: 22,
        resizeMode: 'contain',
        padding: 10,
        alignSelf: 'center'
    },
    headThumbnail: {
        width: 70, height: 70, borderRadius: 35,
    },
    gender: {
        alignSelf: 'center', width: 15, height: 15, position: 'absolute', left: 28, bottom: 2
    }, rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#e1dfe4',
    }, separator: {
        height: 6, backgroundColor: '#f2f2f2'
    },
});
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
    }
);
const mapStateToProps = state => ({
    userInfoData: state.userInfoReducer.data,
    configData: state.checkRegisterReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps)(Me);