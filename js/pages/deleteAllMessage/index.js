/**
 * Created by wangxu on 2017/12/26.
 * 批量删除消息
 */
import React, {Component, Navigator} from 'react';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Modal,
    Image,
    StyleSheet,
    View,
    AsyncStorage,
    Platform,
    TouchableOpacity,
    ListView,
    Dimensions,
    InteractionManager,
    Text, RefreshControl
} from 'react-native';
//
import Language from '../../../resources/language/index';
import  * as styles from './style';
import * as Constant from '../../../support/common/constant';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import CachedImage from '../../../support/common/CachedImage';
import CustomCheckBox from '../../../support/common/customCheckBox';
import Toast from '../../../support/common/Toast';
import Color from '../../../resources/themColor/index';
import ImageManager from '../../../resources/imageManager/index';
import netWorkTool from '../../../support/common/netWorkTool';
import LoadMoreFooter from '../../../support/common/LoadMoreFooter';
import {getChatMessageForPage, deleteAllChatData} from '../../../support/actions/chatActions';
import * as types from '../../../support/actions/actionTypes';
import Dialog, {Confirm} from '../../../support/common/dialog';

//
const {
    popRoute,
} = actions;
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
//
let selfThis;
let chatMessageDatas = new Array();
let deleteMessageList;
class DeleteAllMessage extends Component {
    static propTypes = {
        chatData: React.PropTypes.object,
        isRefreshing: React.PropTypes.bool,
        isNoMore: React.PropTypes.bool,
        getChatMessageForPage: React.PropTypes.func,
        deleteAllChatData: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        selfThis = this;
        if (this.props.chatData) {
            chatMessageDatas = this.props.chatData.list;
        }
        deleteMessageList = new Array();
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            chatMessageDataSource: ds.cloneWithRows(chatMessageDatas),
        };
    }

    componentDidMount() {
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    componentWillMount() {

    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.finishTimer && clearTimeout(this.finishTimer);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (!_.isEqual(this.props.chatData, nextProps.data));
    }

    handleMethod(isConnected) {

    }

    render() {
        if (this.props.chatData != null && this.props.chatData.list) {
            chatMessageDatas = this.props.chatData.list;
        } else {
            chatMessageDatas = new Array();
        }
        return (<View style={styles.container}>
            <View flexDirection='row' style={styles.headerView}>
                <TouchableOpacity
                    style={[styles.touchableOpacity,{position: 'absolute', top: (Platform.OS === 'ios') ? 14 : 0, bottom: 0,left:20}]}
                    activeOpacity={0.8}
                    onPress={this.finishPage.bind(this)}>
                    <Text style={[styles.mainTitle,{fontSize:16}]}>{Language().cancel}</Text>
                </TouchableOpacity>
                <Text style={styles.mainTitle}>{Language().message}</Text>
                <TouchableOpacity
                    style={[styles.touchableOpacity,{position: 'absolute', top: (Platform.OS === 'ios') ? 14 : 0, bottom: 0,right:20}]}
                    activeOpacity={0.8}
                    onPress={this.deleteAllMessage.bind(this)}>
                    <Text style={[styles.mainTitle,{fontSize:16}]}>{Language().delete}</Text>
                </TouchableOpacity>
            </View>
            <ListView dataSource={this.state.chatMessageDataSource.cloneWithRows(chatMessageDatas)}
                      renderRow={this.renderRow.bind(this)}
                      renderFooter={this.renderFooter.bind(this)}
                      onEndReached={this._onEndReach.bind(this)}
                      initialListSize={1}
                      pageSize={6}
                      removeClippedSubviews={false}
                      onEndReachedThreshold={10}
                      enableEmptySections={true}
                      refreshControl={
                                     <RefreshControl
                                        refreshing={this.props.isRefreshing}
                                         onRefresh={this._onRefresh.bind(this)}
                                         colors={[Color.list_loading_color]} />
                                    }/>
            {Dialog.inject()}
            <Confirm ref='deleteDialog' title={Language().delete} posText={Language().delete}
                     message={ Language().delete_all_message}
                     messageStyle={{alignSelf:'center',fontSize: 14,marginBottom:10}}
                     titleStyle={{alignSelf:'center',fontSize: 17,padding: 5}}
                     contentStyle={{paddingLeft:1,paddingBottom: 3,paddingRight: 1,width: ScreenWidth/3*2+30}}
                     buttonBarStyle={{justifyContent:'center'}}
                     buttonStyle={{textAlign: 'center',fontSize: 16,padding: 10,marginLeft: 0,marginRight: 0,alignSelf:'center',justifyContent:'center',flex:1,color:Color.system_dialog_bt_text_color}}
                     negText={Language().cancel}
                     enumeSeparator={true}
                     rowSeparator={true}
                     onNegClick={this.dismissDialog.bind(this)} onPosClick={this.doDeleteAllMessage.bind(this)}>
            </Confirm>
        </View>);
    }

    /**
     * 加载更多
     * @private
     */
    renderFooter() {
        return chatMessageDatas.length < 30 ? null : (<LoadMoreFooter isNoMore={selfThis.props.isNoMore}/>);
    }

    /**
     * load more
     * @private
     */
    _onEndReach = () => {
        //如果网络异常情况下处理
        let data = this.props.chatData;
        if (data) {
            if (data.page < data.total_page) {
                this.props.getChatMessageForPage(data.page + 1, types.GET_MORE_CHAT_DATA);
            } else {
                this.props.getChatMessageForPage(data.page, types.GET_MORE_CHAT_DATA);
            }
        }
    }

    /**
     *对应的item
     */
    renderRow(item, sectionId, rowId) {
        return (<View style={{overflow:'hidden'}}>
            <View flexDirection='row'
                  style={{backgroundColor:Color.colorWhite,paddingLeft:40,width:ScreenWidth,height:65}}>
                <CachedImage style={styles.headThumbnail}
                             source={(!item.image)?ImageManager.defaultUser:({uri:item.image})}
                             defaultSource={ImageManager.defaultUser}/>
                <View style={{alignSelf:'center',marginLeft:8}}>
                    <Text numberOfLines={1}
                          style={{color:Color.voice_contact_name_color,padding:2,fontSize:16}}>{item.name}</Text>
                    <Text numberOfLines={1}
                          style={{color: Color.voice_contact_message_color,padding:2,fontSize:14}}>{item.message}</Text>
                </View>

            </View>
            <View style={styles.rowSeparator}></View>
            {this.renderCheckBox(item)}
        </View>);
    }

    /**
     * 刷新
     * @private
     */
    _onRefresh = () => {
        this.props.isRefreshing = true;
        InteractionManager.runAfterInteractions(() => {
            //刷新消息列表
            this.props.getChatMessageForPage(1, types.REFRESH_CHAT_DATA);
        });
    };

    /**
     *
     * @param data
     * @returns {XML}
     */
    renderCheckBox(item) {
        let data = {
            checked: false,
            id: item.id,
            name: item.name
        }
        return (
            <CustomCheckBox
                style={{flex: 1,position: 'absolute',width:ScreenWidth,height:65,paddingLeft:10}}
                onClick={()=>this.onClick(data)}
                isChecked={data.checked}/>);
    }

    /**
     *
     * @param data
     */
    onClick(data) {
        data.checked = !data.checked;
        if (data.checked) {
            deleteMessageList.push(data.id);
        } else {
            let index = deleteMessageList.indexOf(data.id);
            if (index != -1) {
                deleteMessageList.splice(index, 1);
            }
        }
    }

    /**
     *取消编辑
     */
    finishPage() {
        if (!this.state.isFinishing) {
            this.props.finishPage('global');
            this.state.isFinishing = true;
        }
        this.finishTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }

    deleteAllMessage() {
        if (deleteMessageList && deleteMessageList.length > 0) {
            if (this.refs.deleteDialog) {
                this.refs.deleteDialog.show();
            }
        } else {
            Toast.show(Language().prompt_delete_message);
        }
    }

    /**
     *
     */
    doDeleteAllMessage() {
        if (deleteMessageList && deleteMessageList.length > 0) {
            this.props.deleteAllChatData(deleteMessageList);
            Toast.show(Language().delete_success);
            this.dismissDialog();
            this.finishPage();
        } else {
            Toast.show(Language().prompt_delete_message);
        }
    }

    dismissDialog() {
        this.refs.deleteDialog.hide();
    }
}

const mapDispatchToProps = dispatch => ({
    finishPage: (key) => dispatch(popRoute(key)),
    getChatMessageForPage: (page, actionType) => dispatch(getChatMessageForPage(page, actionType)),
    deleteAllChatData: (list) => dispatch(deleteAllChatData(list)),
});
const mapStateToProps = state => ({
    chatData: state.chatReducer.data,
    isRefreshing: state.chatReducer.isRefreshing,
    isNoMore: state.chatReducer.isNoMore,
});
export default connect(mapStateToProps, mapDispatchToProps)(DeleteAllMessage);