/**
 * Created by wangxu on 2017/3/2.
 */
'use strict';
import React, {Component, Navigator} from 'react';
import Language from '../../../resources/language';
import * as SCENES from '../../../support/actions/scene';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {Modal, Image, StyleSheet, View, AsyncStorage, Platform, TouchableOpacity, ListView} from 'react-native';
import Dialog, {Alert} from '../../../support/common/dialog';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import {removeChat} from '../../../support/actions/chatActions';
import {userInfo, updateTourist, getUserInfo} from '../../../support/actions/userInfoActions';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import {clearFriendList} from '../../../support/actions/friendActions';
import {clearMessageList} from '../../../support/actions/messageActions';
import {removeFriendStatus} from '../../../support/actions/friendStatusAction';
import CachedImage from '../../../support/common/CachedImage';
import Toast from '../../../support/common/Toast';
import UtilsTool from '../../../support/common/UtilsTool';
import * as Constant from '../../../support/common/constant';
import * as gameApiDefines from '../../../support/common/gameApiDefines';
import ImageViewer from 'react-native-image-zoom-viewer';
import {Container, Button, Left, Text, Right, Body, Content, Item, CardItem} from 'native-base';
import Color from '../../../resources/themColor';
import CustomProgressBar from '../../../support/common/ProgressBar';
import ImageManager from '../../../resources/imageManager';

const {
    pushRoute,
    popRoute
} = actions;
const rightArrow = require('../../../resources/imgs/bg_arrow_right.png');
const bgBack = require('../../../resources/imgs/ic_back.png');
const bgEditUser = require('../../../resources/imgs/icon_edit_user_info.png');
const avatar = require('../../../resources/imgs/ic_default_user.png');
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const headHeight = (Platform.OS === 'ios') ? 60 : 50;

const male = require('../../../resources/imgs/ic_male.png');
const female = require('../../../resources/imgs/ic_female.png');
const bgTotal = require('../../../resources/imgs/me_icon_total.png');
const bgWin = require('../../../resources/imgs/me_icon_win.png');
const bgLose = require('../../../resources/imgs/me_icon_lose.png');
const bgRecordDetail = require('../../../resources/imgs/ic_record_detail.png');
const bgFamily = require('../../../resources/imgs/ic_family.png');
const bgPhotoWall = require('../../../resources/imgs/ic_photo_wall.png');
const bgGifts = require('../../../resources/imgs/ic_gifts.png');
//**********************************************************************
const bgPopular = require('../../../resources/imgs/icon_popular.png');
const arrows = require('../../../resources/imgs/bg_arrow_right.png');
const bgDefaultFamily = require('../../../resources/imgs/ico_family_default.png');
//
//获取屏幕宽度
let Dimensions = require("Dimensions");
let {width} = Dimensions.get('window');
const ScreenWidth = width;
//常量设置
let cols = 3;
let cellWH = 86;
let vMargin = ((width - cellWH) - cellWH * cols) / (cols + 1);
let giftCols = 4;
let giftCellH = 75;
let giftCellW = 75;
let giftVMargin = (width - giftCellH * giftCols) / (giftCols + 1);
let hMargin = 3;
let that;
let gifts;
let images;
const roleType = {
    manager: 1 << 0,
    teacher: 1 << 1
}
class ContactDetail extends Component {
    constructor(props) {
        super(props);
        that = this;
        images = new Array();
        gifts = new Array();
        if (this.props.userInfo != null) {
            if (this.props.userInfo.photos && this.props.userInfo.photos != null) {
                images = this.props.userInfo.photos;
            }
            if (this.props.userInfo.gift && this.props.userInfo.gift != null) {
                gifts = this.props.userInfo.gift;
            }
        }
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        //2.设置返回数据
        this.state = {
            disabled: false,
            previewIndex: 0,
            showPreview: false,
            dataSource: ds.cloneWithRows(images),
            giftDataSource: ds.cloneWithRows(gifts)
        };
    }

    static propTypes = {
        navigator: React.PropTypes.shape({}),
        openPage: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        code: React.PropTypes.number,
        message: React.PropTypes.string,
        updateUserInfo: React.PropTypes.func,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        removeChatData: React.PropTypes.func,
        clearUserInfo: React.PropTypes.func,
        updateTourist: React.PropTypes.func,
        clearMessageList: React.PropTypes.func,
        removeFriendStatus: React.PropTypes.func,
        clearFriendList: React.PropTypes.func,
        data: React.PropTypes.object,
        giftTypeList: React.PropTypes.any
    }

    /**
     * 关闭页面
     */
    onFinishPage() {
        this.disabledTimer = setTimeout(() => {
            this.setState({
                disabled: false
            });
        }, 2000);
        this.props.finishPage('global');
        this.setState({
            disabled: true
        });
    }

    /**
     * 用户编辑页面
     */
    onOpenUserInfo() {
        try {
            this.props.openPage(SCENES.SCENE_EDIT_USER_INFO, 'global');
        } catch (e) {
            console.log(e)
        }

    }

    /**
     * 页面初始化时更新用户信息
     */
    componentWillMount() {
        this.props.updateUserInfo(this.props.userInfo.id);
    }

    /**
     * 页面渲染完毕
     */
    componentDidMount() {
        this.checkTokenTimer = setTimeout(() => {
            this.checkToken();
        }, 500);
    }

    /**
     * 页面销毁
     */
    componentWillUnmount() {
        this.checkTokenTimer && clearTimeout(this.checkTokenTimer);
        this.disabledTimer && clearTimeout(this.disabledTimer);
    }

    /**
     * 检查token是否失效
     */
    checkToken() {
        if (this.props.code == 1004) {
            //token 失效
            try {
                this.refs.login.show();
            } catch (error) {
                console.log('token error show:' + error);
            }
        }
    }

    /**
     * token失效重新登录
     */
    login() {
        const routes = this.props.navigation.routes;
        if (routes.length > 1) {
            for (var i = 0; i < routes.length; i++) {
                this.props.finishPage('global');
            }
        }
        if (this.props.userInfo != null && this.props.userInfo.id != null) {
            let userId = this.props.userInfo.id;
            rnRoNativeUtils.closeLeanCloud(userId);
        }
        AsyncStorageTool.removeUserInfo();
        try {
            this.props.openPage(SCENES.SCENE_SPLASH, 'global');
        } catch (e) {
            console.log(e)
        }
        AsyncStorageTool.removeSignTourist();
        this.props.removeChatData();
        this.props.clearUserInfo(null);
        this.props.updateTourist(false);
        this.props.clearFriendList();
        this.props.clearMessageList();
        this.props.removeFriendStatus();
        this.refs.login.hide();

    }

    /**
     *
     * @returns {*}
     */
    render() {
        let giftCount = 0;
        let signature = Language().prompt_signature;
        let signatureColor = '#666666';
        let gender = female;
        let game = null;
        let image = '';
        let name = '';
        let address = '';
        let role = null;
        let custom = '';
        let giftCountText = Language().gift_title;
        let user = this.props.userInfo;
        let uid = 0;
        let popular = 0;
        let userLevelTitle;
        let userLevelSource;
        let userLevelStar = 1;
        let userExperience = 0;
        let userLevelData;
        if (user != null) {
            game = this.props.userInfo.game;
            if (user.image != null) {
                image = user.image;
            }
            if (user.signature && user.signature != null) {
                signature = user.signature;
                signatureColor = Color.setting_text_color;
            }
            name = user.name;
            if (user.sex == 1) {
                gender = male;
            }
            if (user.gift && user.gift != null) {
                gifts = user.gift;
                that.state.giftDataSource = that.state.giftDataSource.cloneWithRows(user.gift);
                for (let i = 0; i < gifts.length; i++) {
                    giftCount += gifts[i].count;
                }
            }
            if (user.photos && user.photos != null) {
                images = user.photos;
                that.state.dataSource = that.state.dataSource.cloneWithRows(user.photos);
            }
            if (giftCount > 0) {
                giftCountText = giftCountText + '(' + giftCount + ')';
            }
            if (user.location) {
                if (user.location.address) {
                    address = user.location.address;
                }
            }
            if (user.role) {
                role = user.role;
                if (role.custom) {
                    custom = role.custom;
                }
            }
            if (user.uid) {
                uid = user.uid;
            }
            if (user.popular) {
                popular = user.popular;
            }
            if (user.active) {
                userLevelData = user.active;
                if (userLevelData) {
                    userLevelTitle = userLevelData.title;
                    userExperience = userLevelData.experience;
                    if (userLevelData.star) {
                        userLevelStar = userLevelData.star;
                    }
                    UtilsTool.userLevelSource(userLevelData.type, (source) => {
                        userLevelSource = source;
                    });
                }
            }

        } else {
            return (null);
        }
        let level = 0;
        let win = 0;
        let lose = 0;
        let odds = 0;
        let escape = 0;
        let escapeRate = 0;
        if (game != null && game.level != null && game.lose != null) {
            level = game.level;
            win = game.win;
            lose = game.lose;
            escape = game.escape;
            if ((win + lose + escape) > 0) {
                odds = win / (win + lose + escape) * 100;
                odds = odds.toFixed(0);
                escapeRate = escape / (win + lose + escape) * 100;
                escapeRate = escapeRate.toFixed(0);
            }
        }
        return (
            <Container style={{backgroundColor:Color.colorWhite}}>
                <CardItem
                    style={{backgroundColor:(Color.headColor),height:headHeight,paddingTop:headPaddingTop,paddingBottom:0,paddingRight:3}}>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={this.onFinishPage.bind(this)} disabled={this.state.disabled}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf:'center',flex:3}}>
                    <Text
                        style={{color: Color.colorWhite,fontSize: 18,alignSelf:'center'}}>{Language().register_user_info}</Text>
                    </Body>
                    <Right style={{flex:1,justifyContent:'center'}}>
                        <Button bordered onPress={this.onOpenUserInfo.bind(this)}
                                style={{backgroundColor:Color.headColor,borderColor:Color.headColor}}>
                            <Image source={bgEditUser} style={styles.bgEdit}></Image>
                        </Button>
                    </Right>
                </CardItem>
                <Content showsVerticalScrollIndicator={false}>
                    <CardItem
                        style={{backgroundColor:(Color.colorWhite),paddingBottom:17,paddingTop:17,paddingRight:0}}>
                        <Left style={{flex:1}}>
                            <View style={{justifyContent:'center'}}>
                                <View style={{alignSelf:'center'}}>
                                    <CachedImage
                                        style={styles.headThumbnail}
                                        source={(image=='')?ImageManager.defaultUser:{uri:image}}
                                        defaultSource={ImageManager.defaultUser}/>
                                    <Image source={gender} style={styles.gender}/>
                                </View>
                                {this.uidView(uid)}
                            </View>
                            <Body style={{marginLeft:15,flex:15}}>
                            <Text style={{color:'#000000',fontWeight: '400',fontSize: 16}}
                                  numberOfLines={1}>{name}</Text>
                            <View flexDirection='row' style={{marginTop:5}}>
                                {/*<Text*/}
                                {/*style={{color:'#666666',fontSize: 11}}>{Language().signature}:</Text>*/}
                                <Text numberOfLines={1}
                                      style={{color:(signatureColor),fontSize: 11,flex:1}}>{signature}</Text>
                            </View>
                            <View flexDirection='row' style={{marginTop:5}}>
                                <Text numberOfLines={1}
                                      style={{color:'#ec6f4e',fontSize: 14,fontWeight: '400',padding:0}}>LV.{level}</Text>
                                {this.popularityView(popular)}
                            </View>
                            {custom ? (<View flexDirection='row'>
                                    <View
                                        style={{backgroundColor:'#ff9ec8',marginTop:5,borderRadius:10,marginRight:5,justifyContent:'center'}}>
                                        <Text numberOfLines={1}
                                              style={{color:'#2d1c4c',fontSize: 12,fontWeight: '400',textAlign:'center',padding:0.5,paddingRight:5,paddingLeft:5,backgroundColor:Color.transparent}}>{custom}</Text>
                                    </View>
                                </View>) : null}
                            {custom ? null : this.roleView(role)}
                            </Body>
                            <Right style={{width:0}}/>
                        </Left>
                    </CardItem>
                    <View style={styles.rowSeparator}/>
                    <View style={styles.separator}/>
                    <View style={styles.rowSeparator}/>
                    <View flexDirection='row' style={{backgroundColor:Color.colorWhite,marginTop:5}}>
                        <Image source={bgFamily}
                               style={[styles.bgGame,{marginLeft:20,width:20,height:20,marginRight:3}]}/>
                        <Text
                            style={{color:'#150435',fontSize: 14,padding:5,flex:1}}>{Language().group_family}</Text>
                    </View>
                    <CardItem
                        style={{backgroundColor:(Color.colorWhite),padding:15,paddingBottom:10,paddingTop:0}}
                        button activeOpacity={0.6} onPress={this.openFamilyDetail.bind(this)}>
                        {this.familyDetailView()}
                        <Image source={arrows}
                               style={[styles.bgBack,{width: 5,height: 15,padding:0}]}/>
                    </CardItem>
                    <View style={styles.rowSeparator}/>
                    <View style={styles.separator}/>

                    <View style={styles.rowSeparator}/>
                    {gameApiDefines.APP_KEY == gameApiDefines.WEREWOLF ? ( <View>
                            <TouchableOpacity onPress={this.openRankInstruction.bind(this)}
                                              activeOpacity={0.6}
                                              style={{backgroundColor:Color.transparent,justifyContent:'center'}}>
                                <View flexDirection='row' style={{backgroundColor:Color.colorWhite,marginTop:5}}>
                                    {userLevelSource ? ( <CachedImage source={userLevelSource}
                                                                      style={[styles.bgGame,{width:40,height:40,margin:5,marginLeft:20}]}/>) : (
                                            <View style={{width:35,height:35,marginBottom:8,marginTop:8}}/>)}
                                    <Text
                                        style={{color:'#ec6f4e',fontSize: 16,padding:0,flex:1,alignSelf:'center'}}>{userLevelTitle + userLevelStar + Language().level_star}</Text>
                                    <Text
                                        style={{color:'#ec6f4e',fontSize: 16,padding:0,flex:1.5,alignSelf:'center'}}>{Language().military_power + userExperience}</Text>
                                    <Image source={arrows}
                                           style={{alignSelf:'center',marginRight:15,width: 5,height: 15,resizeMode: 'contain',}}/>
                                </View>

                            </TouchableOpacity>
                            <CardItem
                                style={{backgroundColor:(Color.colorWhite),padding:15,paddingBottom:10,paddingTop:5}}
                                button activeOpacity={0.6} onPress={this.openRecordDetails.bind(this)}>
                                <View style={{flex:1}}>
                                    <View flexDirection='row'
                                          style={{justifyContent:'center',marginRight:20,marginLeft:20}}>
                                        <View flexDirection='row' style={{flex:1,justifyContent:'center'}}>
                                            <Image source={bgTotal} style={styles.bgGame}></Image>
                                            <Text numberOfLines={1}
                                                  style={{color:(Color.record_text_color),fontSize: 14,alignSelf:'center',padding:5}}>{win + lose}{Language().bureau}</Text>
                                        </View>
                                        <View flexDirection='row' style={{flex:1,justifyContent:'center'}}>
                                            <Image source={bgWin} style={styles.bgGame}></Image>
                                            <Text numberOfLines={1}
                                                  style={{color:(Color.record_text_color),fontSize: 14,alignSelf:'center',padding:5}}>{win}{Language().bureau}</Text>

                                        </View>
                                        <View flexDirection='row' style={{flex:1,justifyContent:'center'}}>
                                            <Image source={bgLose} style={styles.bgGame}></Image>
                                            <Text numberOfLines={1}
                                                  style={{color:(Color.record_text_color),fontSize: 14,alignSelf:'center',padding:5}}>{lose}{Language().bureau}</Text>
                                        </View>
                                    </View>
                                    <View flexDirection='row'>
                                        <View style={{flex:1,justifyContent:'center',margin:10,marginBottom:5}}>
                                            <View flexDirection='row' style={{justifyContent:'center'}}>
                                                <Text
                                                    style={{color:(Color.win_color),fontSize: 14,alignSelf:'center',padding:3}}>{Language().odds}</Text>
                                                <Text numberOfLines={1}
                                                      style={{color:(Color.win_color),fontSize: 14,alignSelf:'center',padding:3}}>{odds}%</Text>
                                            </View>
                                            <CustomProgressBar progress={odds*0.01} progressColor={'#ffac03'}
                                                               bufferColor={Color.colorWhite}
                                                               containerStyle={{borderColor: Color.win_color, borderWidth:1,height:14}}
                                                               borderRadius={8}/>
                                        </View>
                                        <View style={{flex:1,justifyContent:'center',margin:10,marginBottom:5}}>
                                            <View flexDirection='row'
                                                  style={{justifyContent:'center'}}>
                                                <Text
                                                    style={{color:(Color.lose_color),fontSize: 14,alignSelf:'center',padding:3}}>{Language().leave}</Text>
                                                <Text numberOfLines={1}
                                                      style={{color:(Color.lose_color),fontSize: 14,alignSelf:'center',padding:3}}>{escapeRate}%</Text>
                                            </View>
                                            <CustomProgressBar progress={escapeRate*0.01} progressColor={'#9b92ff'}
                                                               bufferColor={Color.colorWhite}
                                                               containerStyle={{borderColor:Color.lose_color, borderWidth:1,height:14}}
                                                               borderRadius={8}/>
                                        </View>
                                    </View>
                                </View>
                                <Image source={arrows}
                                       style={[styles.bgBack,{width: 5,height: 15,padding:0}]}/>
                            </CardItem>
                            <View style={styles.rowSeparator}/>
                            <View style={styles.separator}/>
                        </View>) : null}

                    <View style={styles.rowSeparator}/>
                    <View flexDirection='row' style={{backgroundColor:Color.colorWhite,marginTop:5}}>
                        <Image source={bgPhotoWall}
                               style={[styles.bgGame,{marginLeft:20,width:20,height:20,marginRight:3}]}/>
                        <Text
                            style={{color:'#150435',fontSize: 14,padding:5,flex:1}}>{Language().photo_wall}</Text>
                    </View>
                    <CardItem style={{backgroundColor:(Color.colorWhite),padding:10,paddingTop:5}}>
                        {this._photoWallView()}
                    </CardItem>
                    <View style={styles.rowSeparator}/>

                    <View style={styles.separator}/>
                    <View style={styles.rowSeparator}/>


                    <CardItem style={{backgroundColor:(Color.colorWhite),padding:10,paddingTop:5}}>
                        <View>
                            <View flexDirection='row'>
                                <Image source={bgGifts}
                                       style={[styles.bgGame,{marginLeft:10,width:20,height:20,marginRight:3}]}/>
                                <Text
                                    style={{color:'#150435',fontSize: 14,padding:5,flex:1}}>{giftCountText}</Text>
                            </View>
                            {this._giftView()}
                        </View>
                    </CardItem>
                    <View style={styles.rowSeparator}></View>
                </Content>
                {Dialog.inject()}
                <Alert ref='login' title={Language().title_login} message={Language().need_login_message}
                       messageStyle={{alignSelf:'center',fontSize: 14,marginBottom:10}}
                       titleStyle={{alignSelf:'center',fontSize: 17,padding: 5}}
                       contentStyle={{paddingLeft:1,paddingBottom: 3,paddingRight: 1,width: ScreenWidth/3*2+30}}
                       buttonBarStyle={{justifyContent:'center'}}
                       buttonStyle={{textAlign: 'center',fontSize: 16,padding: 10,marginLeft: 0,marginRight: 0,alignSelf:'center',justifyContent:'center',flex:1,color:Color.system_dialog_bt_text_color}}
                       posText={Language().confirm}
                       rowSeparator={true}
                       onPosClick={this.login.bind(this)}> </Alert>
                <Modal visible={that.state.showPreview} transparent={true}
                       onRequestClose={that.onClosePreview.bind(this)}>
                    <ImageViewer imageUrls={images}
                                 saveToLocalByLongPress={false}
                                 failImageSource={require('../../../resources/imgs/me_photo_default.png')}
                                 onClick={that.onClosePreview.bind(this)}
                                 index={that.state.previewIndex}/>
                </Modal>
            </Container>
        );
    }

    /**
     *uid view
     * @param uid
     */
    uidView(uid) {
        if (uid) {
            return (
                <Text
                    style={{color:Color.blackColor,fontSize: 12,alignSelf:'center',textAlign:'center',backgroundColor:Color.colorWhite,marginLeft:0}}>ID:{uid}</Text>);
        }
    }

    /**
     * 人气值view
     * @param popular
     * @returns {*}
     */
    popularityView(popular) {
        return popular ? (<View style={{marginLeft:30,flexDirection: 'row',justifyContent:'center'}}>
                <Image style={{width:13,height:13,alignSelf:'center', resizeMode: 'contain',}} source={bgPopular}/>
                <Text
                    style={{color:'#ef8468',fontSize: 14,fontWeight: '400',padding:0}}>{Language().popular}</Text>
                <Text
                    style={{color:'#ef8468',fontSize: 14,fontWeight: '400',padding:0,marginLeft:5}}>{popular}</Text>
            </View>) : null;
    }

    /**
     * 地理位置view
     * @param address
     * @returns {*}
     */
    locationView(address) {
        if (address) {
            return (<Text numberOfLines={1}
                          style={{color:(Color.setting_text_color),fontSize: 12,paddingTop:2}}>{address}</Text>)
        } else {
            return null;
        }
    }

    /**
     * 用户身份
     * @param role
     * @returns {*}
     */
    roleView(role) {
        let content = null;
        let type = 0;
        if (role) {
            type = role.type;
        }
        if ((type & roleType.teacher) != 0) {
            content = (<View flexDirection='row'>
                <View
                    style={{backgroundColor:'#ff9ec8',marginTop:5,borderRadius:10,marginRight:5,justifyContent:'center'}}>
                    <Text numberOfLines={1}
                          style={{color:'#2d1c4c',fontSize: 12,fontWeight: '400',textAlign:'center',padding:0.5,paddingRight:8,paddingLeft:8,backgroundColor:Color.transparent}}>{Language().teacher}</Text>
                </View></View>);
        }
        if ((type & roleType.manager) != 0) {
            content = <View flexDirection='row'>
                {content}
                <View style={{backgroundColor:'#AEE4FD',marginTop:5,borderRadius:10,justifyContent:'center'}}>
                    <Text numberOfLines={1}
                          style={{color:'#2d1c4c',fontSize: 12,fontWeight: '400',textAlign:'center',padding:0.5,paddingRight:8,paddingLeft:8,backgroundColor:Color.transparent}}>{Language().manager}</Text>
                </View>
            </View>
        }
        return content;
    }

    /**
     * 关闭预览模式
     */
    onClosePreview() {
        this.setState({
            showPreview: false,
            previewIndex: 0
        });
    }

    /**
     * 开启预览模式
     */
    onShowPreview(rowId) {
        this.setState({
            showPreview: true,
            previewIndex: parseInt(rowId)
        });
    }

    /**
     * 照片墙
     * @private
     */
    _photoWallView() {
        return (images != null && images.length == 0) ? (<Text
                style={{color:(Color.prompt_text_color),fontSize: 14,padding:10}}>{Language().no_photos_prompt}</Text>) : (
                <View style={{justifyContent:'center',alignSelf:'center',width:ScreenWidth}}>
                    <ListView dataSource={this.state.dataSource}
                              renderRow={this.renderRow}
                              removeClippedSubviews={false}
                              contentContainerStyle={styles.listViewStyle}/></View>);
    }

    /**
     * 礼物的展示
     * @private
     */
    _giftView() {
        return (gifts != null && gifts.length == 0) ? (<Text
                style={{color:(Color.prompt_gift_color),fontSize: 14,padding:10}}>{Language().no_gift_prompt}</Text>) : (
                <View flexDirection='row' style={{justifyContent:'center',marginLeft:10,marginTop:8}}>
                    <ListView dataSource={this.state.giftDataSource}
                              renderRow={this.renderGiftRow}
                              removeClippedSubviews={true}
                              initialListSize={1}
                              pageSize={4}
                              contentContainerStyle={styles.giftListViewStyle}/>
                    <TouchableOpacity style={{justifyContent:'center'}}
                                      activeOpacity={0.6}
                                      onPress={()=>that.onShowDetailGift()}>
                        <Image source={arrows}
                               style={[styles.bgBack,{width: 5,height: 15,padding:0}]}/>
                    </TouchableOpacity>
                </View>);
    }

    /**
     * 照片墙
     * @param data
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    renderRow(data, sectionId, rowId) {
        let marginLeft = hMargin;
        if (rowId % cols == 0) {
            marginLeft = 0;
        }
        return (
            <View style={[styles.cellBackStyle,{marginLeft:marginLeft}]}>
                <TouchableOpacity style={{justifyContent:'center'}}
                                  activeOpacity={0.6}
                                  onPress={()=>that.onShowPreview(rowId)}>
                    <CachedImage
                        source={(data.url==''||data.url==null)?(require('../../../resources/imgs/me_photo_default.png')):({uri:data.url})}
                        defaultSource={require('../../../resources/imgs/me_photo_default.png')}
                        style={{width:cellWH,height:cellWH,alignSelf:'center'}}
                        type='photo'
                    ></CachedImage>
                </TouchableOpacity>
            </View>
        );
    }

    /**
     * 礼物展示的item
     * @param data
     */
    renderGiftRow(data) {
        let giftData = that.getGiftSource(data.type);
        return giftData ? (<View style={styles.cellBackGiftStyle}>
                <TouchableOpacity style={{justifyContent:'center'}} activeOpacity={0.6}
                                  onPress={()=>that.onShowDetailGift()}>
                    <CachedImage
                        source={(giftData.load_type==Constant.giftLoadTypeLocal)?(giftData.gift_image):({uri:giftData.gift_image})}
                        style={{width:35,height:35,alignSelf:'center',resizeMode: 'contain'}}
                        type='photo'/>
                    <Text
                        style={{fontSize:12,color:'#62709F',padding:5,alignSelf:'center'}}>{giftData.gift_name}</Text>
                    {that._giftCountView(data.count)}
                </TouchableOpacity>
            </View>) : null;
    }

    /**
     * 礼物数
     * @param count
     * @returns {*}
     * @private
     */
    _giftCountView(count) {
        let newCount = count;
        if (newCount > 999) {
            newCount = 999 + '+'
        } else {
            newCount = 'x' + newCount;
        }
        return (<View style={[styles.giftCount]}>
            <Text
                style={{fontSize: 10,color: '#2D1A4D',textAlign:'center',backgroundColor:Color.transparent}}>{newCount}</Text>
        </View>)
    }

    /**
     * 礼物列表
     * @param type
     * @returns {{name: string, icon: string}}
     */
    getGiftSource(type) {
        let gifts = this.props.giftTypeList;
        // console.log('getGiftSource:'+type);
        for (let i = 0; gifts[i]; i += 1) {
            let gift = gifts[i];
            if (gift && gift.type == type) {
                return gift;
            }
        }
        return null;
    }

    /**
     * 收到礼物的详情
     */
    onShowDetailGift() {
        let id = this.props.userInfo.id;
        rnRoNativeUtils.enterExhibitionRecord(id);
    }

    /**
     * 战绩详情
     */
    openRecordDetails() {
        // 启动战绩
        // action "standings"
        // type "RN_Native"
        let NativeData = {
            action: Constant.STANDINGS,
            type: Constant.RN_NATIVE
        }
        rnRoNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }

    /**
     * 进入家族详情
     */
    openFamilyDetail() {
        let groupInfo = this.props.userInfo.group;
        if (groupInfo && groupInfo.group_id) {
            let nativeData = {
                action: Constant.ACTION_SHOW_FAMILY_INFO,
                params: {group_id: groupInfo.group_id},
                type: Constant.RN_NATIVE
            }
            rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
        } else {
            try {
                this.props.openPage(SCENES.SCENE_GROUP_LIST, 'global');
            } catch (e) {
                console.log(e)
            }
        }
    }

    /**
     *
     */
    familyDetailView() {
        if (this.props.userInfo) {
            let groupInfo = this.props.userInfo.group;
            let levelVal = 0;
            if (groupInfo) {
                if (groupInfo.level_val) {
                    levelVal = groupInfo.level_val;
                }
                return (<View style={{flex:1}}>
                    <View flexDirection='row' style={{marginLeft:25}}>
                        <CachedImage
                            style={{width: 50, height: 50, borderRadius: 25,alignSelf:'center'}}
                            defaultSource={bgDefaultFamily}
                            type='family'
                            source={groupInfo.image?{uri:groupInfo.image}:bgDefaultFamily}/>
                        <View style={{marginLeft:8,alignSelf:'center'}}>
                            <View flexDirection='row' style={{marginBottom:0}}>
                                <Text
                                    numberOfLines={1}
                                    style={{color:'#808080',fontSize: 12,alignSelf:'center'}}>{groupInfo.name}</Text>
                                <Image
                                    style={{width: 25, height: 25,resizeMode: 'contain',alignSelf:'center',marginLeft:5}}
                                    source={this.getGroupLevelICon(levelVal,groupInfo.level_image)}/>
                            </View>
                            <View flexDirection='row'>
                                <View style={{borderRadius:8,backgroundColor:'#845fe4',justifyContent:'center'}}>
                                    <Text
                                        style={{color:(Color.prompt_text_color),fontSize: 10,paddingLeft:5,paddingRight:5,alignSelf:'center',backgroundColor:Color.transparent}}>{groupInfo.member_count}人</Text>
                                </View>
                            </View>
                            <Text numberOfLines={1}
                                  style={{color:'#808080',fontSize: 11,marginTop:2}}>{groupInfo.gid}</Text>
                        </View>
                    </View>
                </View>);
            } else {
                return (<View style={{flex:1}}>
                    <Text
                        style={{color:(Color.prompt_text_color),fontSize: 12,padding:5,marginLeft:25}}>{Language().no_group_message_prompt}</Text>
                </View>);
            }
        } else {
            return (<View style={{flex:1}}>
                <Text
                    style={{color:(Color.prompt_text_color),fontSize: 12,padding:5,marginLeft:25}}>{Language().no_group_message_prompt}</Text>
            </View>);
        }
    }

    /**
     *
     * @param levelVal
     * @param levelImage
     * @returns {*}
     */
    getGroupLevelICon(levelVal, levelImage) {
        let imageSource;
        switch (levelVal) {
            case 0:
            case 1:
                imageSource = ImageManager.ic_group_level_val_1;
                break;
            case 2:
                imageSource = ImageManager.ic_group_level_val_2;
                break;
            case 3:
                imageSource = ImageManager.ic_group_level_val_3;
                break;
            case 4:
                imageSource = ImageManager.ic_group_level_val_4;
                break;
            case 5:
                imageSource = ImageManager.ic_group_level_val_5;
                break;
            case 6:
                imageSource = ImageManager.ic_group_level_val_6;
                break;
            case 7:
                imageSource = ImageManager.ic_group_level_val_7;
                break;
            case 8:
                imageSource = ImageManager.ic_group_level_val_8;
                break;
            case 9:
                imageSource = ImageManager.ic_group_level_val_9;
                break;
            case 10:
                imageSource = ImageManager.ic_group_level_val_10;
                break;
        }
        if (!imageSource) {
            if (levelImage) {
                imageSource = {uri: levelImage}
            } else {
                imageSource = ImageManager.ic_group_level_val_1;
            }
        }
        return imageSource;
    }

    /**
     *
     */
    openRankInstruction() {
        rnRoNativeUtils.openWebView(gameApiDefines.RANK_INSTRUCTION);
    }
}
const styles = StyleSheet.create({
    giftCount: {
        position: 'absolute',
        left: (width - 60) / 4 / 2 + 8,
        top: 3,
        backgroundColor: '#FCB900',
        borderRadius: 6,
        justifyContent: 'center',
        paddingLeft: 3, paddingRight: 3

    },
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    bgEdit: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5,
        alignSelf: 'center'
    },
    bgGame: {
        width: 22,
        height: 22,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    headThumbnail: {
        width: 70,
        height: 70,
        borderRadius: 35,
    },
    gender: {
        alignSelf: 'center', width: 15, height: 15, position: 'absolute', left: 32, bottom: 2
    },
    nameStyle: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    gradeStyle: {
        color: '#F8E81C',
        fontSize: 16,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    lineView: {
        height: 2,
        backgroundColor: '#747474'
    },
    buttonStyle: {
        margin: 10,
        borderRadius: 3,
    },
    cellBackStyle: {
        width: cellWH,
        height: cellWH,
        marginLeft: hMargin,
        marginTop: hMargin,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    cellBackGiftStyle: {
        width: (width - 60) / 4,
        height: giftCellH,
        marginLeft: 0,
        marginTop: 0,
        alignSelf: 'center',
        justifyContent: 'flex-start',
        backgroundColor: Color.transparent
    },
    listViewStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignSelf: 'center',
        width: (cellWH * 3) + (hMargin * 3),
    },
    giftListViewStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: width - 40,
        alignItems: 'center',
    },
    rightArrow: {
        width: 15, height: 15, resizeMode: 'contain', alignSelf: 'center'
    },
    rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#e1dfe4',
    }, separator: {
        height: 6, backgroundColor: '#f2f2f2'
    },
});
const mapDispatchToProps = dispatch => ({
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        finishPage: (key) => dispatch(popRoute(key)),
        updateUserInfo: (userId) => dispatch((getUserInfo(userId))),
        removeChatData: () => dispatch(removeChat()),
        clearUserInfo: (data) => dispatch((userInfo(data))),
        updateTourist: (isTourist) => dispatch((updateTourist(isTourist))),
        clearMessageList: () => dispatch(clearMessageList()),
        removeFriendStatus: () => dispatch(removeFriendStatus()),
        clearFriendList: () => dispatch(clearFriendList())
    }
);
const mapStateToProps = state => ({
    code: state.userInfoReducer.code,
    message: state.userInfoReducer.message,
    userInfo: state.userInfoReducer.data,
    navigation: state.cardNavigation,
    data: state.friendReducer.data,
    giftTypeList: state.giftManifestReducer.gifts,
});
export default connect(mapStateToProps, mapDispatchToProps)(ContactDetail);