/**
 * Created by wangxu on 2017/5/5.
 * 忘记密码页面
 */
'use strict';
import React, {Component} from 'react';
import {
    Content,
    Form,
    Item,
    Input,
    Label,
    Button,
    CardItem,
    Left,
    Body,
    Right,
    Text,
    Spinner, Container
} from 'native-base';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {Image, StyleSheet, View, Platform, NativeModules, Dimensions, TouchableOpacity} from 'react-native';
import Color from '../../../resources/themColor';
import netWorkTool from '../../../support/common/netWorkTool';
import Toast from '../../../support/common/Toast';
import Dialog, {Prompt,} from '../../../support/common/dialog';
import Language from '../../../resources/language'
import * as SCENES from '../../../support/actions/scene';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import {userInfo} from '../../../support/actions/userInfoActions';
import {getChatData} from '../../../support/actions/chatActions';
import {getFamilyChat} from '../../../support/actions/groupFamilyChatActions';
import {location} from '../../../support/actions/configActions';

import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import Util from '../../../support/common/utils';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import Loading from '../../../support/common/Loading';
import {initFriendRealm} from '../../../support/actions/friendActions';
import RealmManager from '../../../support/common/realmManager';

const {
    popRoute,
    pushRoute
} = actions;
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const ScreenHeight = Dimensions.get('window').height;
const ScreenWidth = Dimensions.get('window').width;
const bgBack = require('../../../resources/imgs/ic_back.png');
const rightArrow = require('../../../resources/imgs/bg_arrow_right.png');
//密码校验
let regPassword = /^[A-Za-z0-9]{6,16}$/;
let progressDialog;

class ChangePassWord extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        openPage: React.PropTypes.func,
        updateUserInfo: React.PropTypes.func,
        countryInfo: React.PropTypes.object,
        getChatData: React.PropTypes.func,
        getFamilyChat: React.PropTypes.func,
        setLocation: React.PropTypes.func,
        initFriendRealm: React.PropTypes.func,

    }

    constructor(props) {
        super(props);
        this.state = {
            phone: '',
            password: '',
            code: '',
            verifyCodeText: Language().get_verify_code,
            countryCode: '886',
            disabled: false,
            disabledSelectCountryBt: false
        };
        this.timer = null;
        this.timeHit = 0;
    }

    render() {
        let country = this.props.countryInfo;
        let countryName = Language().taiwan;
        let countryCode = '886';
        if (country != null) {
            if (country.n != null) {
                countryName = country.n;
            }
            if (country.c != null) {
                countryCode = country.c;
                this.state.countryCode = countryCode;
            }
        }
        return (
            <Container style={{backgroundColor:'#fff'}}>
                <Content >
                    <CardItem
                        style={{backgroundColor:(Color.headColor),height:60,paddingTop:headPaddingTop,paddingBottom:0}}>
                        <Left style={{flex:1}}>
                            <Button transparent onPress={this.onFinishPage.bind(this)} disabled={this.state.disabled}>
                                <Image source={bgBack} style={styles.bgBack}></Image>
                            </Button>
                        </Left>
                        <Body style={{alignSelf:'center',flex:3}}>
                        <Text style={{color: '#fff',fontSize: 18,alignSelf:'center'}}>{Language().forgetPassword}</Text>
                        </Body>
                        <Right style={{flex:1}}/>
                    </CardItem>
                    <Form style={{marginTop:10}}>
                        {/*<Item style={{marginLeft:15,marginRight:15,borderColor:(Color.transparent)}}>*/}
                        {/*<Input keyboardType='numeric' onChangeText={(text)=>this.onChangePhone(text)}*/}
                        {/*placeholder="请输入手机号"*/}
                        {/*placeholderTextColor={Color.loginTextColor}*/}
                        {/*style={{borderWidth: 1, borderColor: (Color.loginBorderColor), borderRadius: 3,paddingLeft:10}}*/}
                        {/*value={this.state.phone} maxLength={11}/>*/}
                        {/*</Item>*/}
                        <View flexDirection='row'
                              style={{marginLeft:15,marginRight:15,marginBottom:15,borderWidth: 1, borderColor: (Color.loginBorderColor), borderRadius: 3,height:50}}>
                            <Button transparent style={{margin:0,padding:0,flex:1}}
                                    onPress={this.onSelectCountry.bind(this)}
                                    disabled={this.state.disabledSelectCountryBt}>
                                <Text
                                    style={{fontSize:16,color:(Color.blackColor),paddingLeft:10,flex:1}}>{Language().country_region}</Text>
                                <View flexDirection='row'>
                                    <Text
                                        style={{fontSize:16,color:(Color.blackColor)}}>{countryName}</Text>
                                    <Image source={rightArrow}
                                           style={styles.rightArrow}></Image>
                                </View>
                            </Button>
                        </View>
                        <View flexDirection='row'
                              style={{marginLeft:15,marginRight:15,borderWidth: 1, borderColor: (Color.loginBorderColor), borderRadius: 3,height:50}}>
                            <Text
                                style={{textAlign:'center',color:(Color.blackColor),alignSelf:'center',paddingLeft:10}}>+{countryCode}</Text>
                            <Input keyboardType='numeric' onChangeText={(text)=>this.onChangePhone(text)}
                                   placeholder={Language().placeholder_phone}
                                   placeholderTextColor={Color.loginTextColor}
                                   style={{paddingLeft:10,flex:1,fontSize:16}}
                                   value={this.state.phone} maxLength={11}/>
                        </View>
                        <View flexDirection='row'
                              style={{margin:15,borderWidth: 1, borderColor: (Color.loginBorderColor), borderRadius: 3,height:50}}>
                            <Input keyboardType='numeric' maxLength={6}
                                   placeholder={Language().placeholder_verify_code}
                                   placeholderTextColor={Color.loginTextColor}
                                   style={{borderWidth: 0,paddingLeft:10,flex:1,fontSize:16}}
                                   onChangeText={(text)=>this.onChangeVerifyCode(text)}/>
                            <TouchableOpacity onPress={this.onGetVerifyCode.bind(this)}
                                              style={{marginRight:10,alignSelf:'center',justifyContent:'center',width:90,borderColor:(Color.phoneLoginButColor),height:35,borderRadius:3,padding:0,borderWidth:1}}>
                                <Text
                                    style={{textAlign:'center',color:(Color.phoneLoginButColor)}}>{this.state.verifyCodeText}</Text>
                            </TouchableOpacity>
                        </View>
                        <Item style={{marginLeft:15,marginRight:15,borderColor:(Color.transparent)}}>
                            <Input password={true} secureTextEntry={true}
                                   placeholder={Language().placeholder_password}
                                   placeholderTextColor={Color.loginTextColor}
                                   style={{borderWidth: 1, borderColor:(Color.loginBorderColor), borderRadius: 3,paddingLeft:10}}
                                   onChangeText={(text)=>this.onChangePassword(text)}/>
                        </Item>
                    </Form>
                    <TouchableOpacity
                        style={{borderRadius: 3, marginRight: 15, marginLeft: 15, marginTop: 30,backgroundColor:(Color.phoneLoginButColor),justifyContent:'center',height:45}}
                        onPress={this.onSave.bind(this)}>
                        <Text
                            style={{color:'#fff',fontSize:16,padding:5,alignSelf:'center'}}>{Language().save_and_login}</Text>
                    </TouchableOpacity>
                </Content>
                <Loading ref="progressDialog" loadingTitle={Language().loading}/>
                {/*{Dialog.inject()}*/}
                {/*<Prompt ref='progressDialog' message='' title='' posText='' negText=''*/}
                {/*contentStyle={{paddingTop: 0,paddingLeft: 10,paddingBottom: 0,paddingRight: 10 , width:ScreenWidth/2,maxHeight:200}}*/}
                {/*progress={true}>*/}
                {/*<View>*/}
                {/*<Spinner/>*/}
                {/*<Text style={{alignSelf:'center'}}>{Language().loading}</Text>*/}
                {/*</View>*/}
                {/*</Prompt>*/}
            </Container>

        );
    }

    closeProgress() {

    }

    /**
     * 选取国家
     */
    onSelectCountry() {
        if (!this.state.disabledSelectCountryBt) {
            this.props.openPage(SCENES.SCENE_SELECT_COUNTRY, 'global');
        }
        this.state.disabledSelectCountryBt = true;
        this.selectCountryTimer = setTimeout(() => {
            this.state.disabledSelectCountryBt = false;
        }, 1000);
    }

    /**
     * 手机号
     * @param text
     */
    onChangePhone(text) {
        // if (/^[\d]+$/.test(text)) {
        //     this.setState({
        //         phone: text
        //     });
        // } else {
        //     if (/^[\d{1}]$/.test(this.state.phone)) {
        //         this.setState({
        //             phone: ''
        //         });
        //     }
        // }
        //手机号客户端不做验证，交由服务器端，订规则进行验证
        this.setState({
            phone: text
        });
    }

    /**
     * 密码
     * @param text
     */
    onChangePassword(text) {
        this.state.password = text;
    }

    /**
     * 验证码
     * @param text
     */
    onChangeVerifyCode(text) {
        this.state.code = text;
    }

    /**
     * 获取验证码
     */
    onGetVerifyCode() {
        let {phone}=this.state;
        if (!phone.length) {
            Toast.show(Language().placeholder_phone);
            return;
        }
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                let newPhone = "+" + this.state.countryCode + phone;
                if (this.timeHit == 0) {
                }
                if (!this.timer) {
                    this.timer = setInterval(function () {
                        const maxSeconds = 60;
                        let txt = '';
                        this.timeHit++;
                        if (this.timeHit > maxSeconds) {
                            txt = Language().get_verify_code;
                            this.timeHit = 0;
                            clearInterval(this.timer);
                            this.timer = null;
                        } else {
                            txt = (parseInt(maxSeconds) - parseInt(this.timeHit)) + 's';
                        }
                        this.setState({verifyCodeText: txt});
                    }.bind(this), 1000);
                    //点击后60s后才可以重新发送
                    let url = apiDefines.GET_CHANGE_PASSWORD_CODE + newPhone;
                    Util.get(url, (code, message, data) => {
                        if (code != 1000) {
                            this.resetVerifyCodeText();
                            Toast.show(message);
                        }
                    }, (failed) => {
                        this.resetVerifyCodeText();
                        Toast.show(Language().not_network);
                    });
                }

            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 重置获取验证码文案以及停止计时
     */
    resetVerifyCodeText() {
        let txt = Language().get_verify_code;
        this.timeHit = 0;
        clearInterval(this.timer);
        this.timer = null;
        this.setState({verifyCodeText: txt});
    }

    /**
     * 用户昵称
     * @param text
     */
    onChangeUserName(text) {
        this.state.name = text;
    }

    componentDidMount() {
        //页面渲染完毕后定义加载view
        progressDialog = this.refs.progressDialog;
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.selectCountryTimer && clearTimeout(this.selectCountryTimer);
        this.disabledTimer && clearTimeout(this.disabledTimer);
    }

    handleMethod(isConnected) {

    }

    onSave() {
        let {phone, password, code} = this.state;
        if (!phone.length) {
            Toast.show(Language().placeholder_phone);
            return;
        }
        if (!code.length) {
            Toast.show(Language().placeholder_verify_code);
            return;
        }
        if (!password.length) {
            Toast.show(Language().placeholder_password);
            return;
        } else if (!regPassword.test(password)) {
            Toast.show(Language().checkout_password_prompt);
            return;
        }
        let newPhone = "+" + this.state.countryCode + phone;
        try {
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    this.onShowProgressDialog();
                    let url = apiDefines.CHANGE_PASSWORD + newPhone + apiDefines.CODE + code + apiDefines.PASSWORD + password;
                    Util.get(url, (code, message, data) => {
                        this.onHideProgressDialog();
                        if (code == 1000) {
                            //找回密码成功后，直接登录进入应用
                            //this.onUserLogin(newPhone, password);
                            //直接返回所有信息直接进入主页
                            this.onFinishPage();
                            this.props.updateUserInfo(data);
                            this.props.openPage(SCENES.SCENE_APP_MAIN, 'global');
                            AsyncStorageTool.saveUserInfo(JSON.stringify(data));
                            if (data != null) {
                                rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token);
                                rnRoNativeUtils.onSetLocation(data.lc);
                                RealmManager.initRealmManager(data.id, (callback) => {
                                    this.props.getChatData(data.id, callback);
                                    this.props.getFamilyChat(data.id, callback);
                                    this.props.initFriendRealm(callback);
                                });
                                this.configLocation(data.lc);
                            }
                        } else {
                            //this.onHideProgressDialog();
                            Toast.show(message);
                        }
                    }, (failed) => {
                        //this.onHideProgressDialog();
                        Toast.show(Language().not_network);
                    });
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        } catch (e) {
            console.log('change password error:' + e);
        }

    }

    /**
     * location信息设置
     * @param lc
     */
    configLocation(lc) {
        if (lc) {
            this.props.setLocation(lc);
        }
    }

    /**
     * 用户找回密码后直接登录应用
     * @param phone
     * @param password
     */
    onUserLogin(phone, password) {
        // try {
        //     netWorkTool.checkNetworkState((isConnected) => {
        //         if (isConnected) {
        //             let url = apiDefines.LOGIN_FROM_PHONE + phone + apiDefines.PASSWORD + password;
        //             Util.get(url, (code, message, data) => {
        //                this.onHideProgressDialog();
        //                 if (code == 1000) {
        //                     this.onFinishPage();
        //                     this.props.updateUserInfo(data);
        //                     this.props.openPage(SCENES.SCENE_APP_MAIN, 'global');
        //                     AsyncStorageTool.saveUserInfo(JSON.stringify(data));
        //                     if (data != null) {
        //                         rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token);
        //                     }
        //                 } else {
        //                     //登录失败直接返回到欢迎页的登录界面
        //                     Toast.show(message);
        //                     this.returnLogin();
        //                 }
        //             }, (failed) => {
        //                 this.onHideProgressDialog();
        //                 Toast.show('操作失败，请检查网络');
        //                 //登录失败直接返回到欢迎页的登录界面
        //                 this.returnLogin();
        //             });
        //         } else {
        //            this.onHideProgressDialog();
        //             Toast.show(netWorkTool.NOT_NETWORK);
        //             //登录失败直接返回到欢迎页的登录界面
        //             this.returnLogin();
        //         }
        //     });
        // } catch (e) {
        //     console.log('login error:' + e);
        // }

    }

    /**
     * 返回到登录的页面
     */
    returnLogin() {
        this.onFinishPage();
        const routes = this.props.navigation.routes;
        if (routes.length > 1) {
            for (var i = 0; i < routes.length; i++) {
                this.props.finishPage('global');
            }
        }
    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     * 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    onFinishPage() {
        this.disabledTimer = setTimeout(() => {
            this.setState({
                disabled: false
            });
        }, 2000);
        this.props.finishPage('global');
        this.setState({
            disabled: true
        });
    }
}
const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    rightArrow: {
        width: 15, height: 15, resizeMode: 'contain', marginLeft: 10, marginRight: 10, alignSelf: 'center'
    },
    loginButton: {
        borderRadius: 3, backgroundColor: '#F2C454', marginRight: 10, marginLeft: 10, marginTop: 20
    }
});
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        updateUserInfo: (data) => dispatch((userInfo(data))),
        getChatData: (id, realm) => dispatch(getChatData(id, realm)),
        getFamilyChat: (id, realm) => dispatch(getFamilyChat(id, realm)),
        initFriendRealm: (realm) => dispatch(initFriendRealm(realm)),
        setLocation: (lc) => dispatch(location(lc)),
    }
);
const mapStateToProps = state => ({
    countryInfo: state.countryListReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps,)(ChangePassWord);