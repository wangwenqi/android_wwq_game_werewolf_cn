/**
 * Created by wangxu on 2017/5/5.
 */
'use strict';
import React, {Component} from 'react';
import {
    Content,
    Form,
    Item,
    Input,
    Label,
    Button,
    CardItem,
    Left,
    Body,
    Right,
    Text,
    Spinner,
    Container
} from 'native-base';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {Image, StyleSheet, View, Platform, NativeModules, Dimensions, TouchableOpacity} from 'react-native';
import Color from '../../../resources/themColor';
import netWorkTool from '../../../support/common/netWorkTool';
import Toast from '../../../support/common/Toast';
import Util from '../../../support/common/utils';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import * as SCENES from '../../../support/actions/scene';
import {saveRegisterData} from '../../../support/actions/registerActions';
import Dialog, {Prompt,} from '../../../support/common/dialog';
import Language from '../../../resources/language';
import Loading from '../../../support/common/Loading';
import CheckBox from '../../../support/common/CheckBox';
const bgBack = require('../../../resources/imgs/ic_back.png');
const {
    popRoute,
    pushRoute
} = actions;
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const ScreenHeight = Dimensions.get('window').height;
const ScreenWidth = Dimensions.get('window').width;
const rightArrow = require('../../../resources/imgs/bg_arrow_right.png');

//密码校验
let regPassword = /^[A-Za-z0-9]{6,16}$/;
let progressDialog;

class Register extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        openPage: React.PropTypes.func,
        saveRegisterData: React.PropTypes.func,
        countryInfo: React.PropTypes.object,
    }

    constructor(props) {
        super(props);
        this.state = {
            phone: '',
            password: '',
            confirmPassword: '',
            code: '',
            verifyCodeText: Language().get_verify_code,
            countryCode: '886',
            disabled: false,
            disabledSelectCountryBt: false,
            useragreement: true

        };
        this.timer = null;
        this.timeHit = 0;
    }

    render() {
        let country = this.props.countryInfo;
        let countryName = Language().taiwan;
        let countryCode = '886';
        if (country != null) {
            if (country.n != null) {
                countryName = country.n;
            }
            if (country.c != null) {
                countryCode = country.c;
                this.state.countryCode = countryCode;
            }
        }
        return (
            <Container style={{backgroundColor:'#fff'}}>
                <Content >
                    <CardItem
                        style={{backgroundColor:(Color.headColor),height:60,paddingTop:headPaddingTop,paddingBottom:0}}>
                        <Left style={{flex:1}}>
                            <Button transparent onPress={this.onFinishPage.bind(this)} disabled={this.state.disabled}>
                                <Image source={bgBack} style={styles.bgBack}></Image>
                            </Button>
                        </Left>
                        <Body style={{alignSelf:'center',flex:3}}>
                        <Text style={{color: '#fff',fontSize: 18,alignSelf:'center'}}>{Language().phone_register}</Text>
                        </Body>
                        <Right style={{flex:1}}/>
                    </CardItem>
                    <Form style={{marginTop:10}}>
                        <View flexDirection='row'
                              style={{marginLeft:15,marginRight:15,marginBottom:15,borderWidth: 1, borderColor: (Color.loginBorderColor), borderRadius: 3,height:50}}>
                            <Button transparent style={{margin:0,padding:0,flex:1}}
                                    onPress={this.onSelectCountry.bind(this)}
                                    disabled={this.state.disabledSelectCountryBt}>
                                <Text
                                    style={{fontSize:16,color:(Color.blackColor),paddingLeft:10,flex:1}}>{Language().country_region}</Text>
                                <View flexDirection='row'>
                                    <Text
                                        style={{fontSize:16,color:(Color.blackColor)}}>{countryName}</Text>
                                    <Image source={rightArrow}
                                           style={styles.rightArrow}></Image>
                                </View>
                            </Button>
                        </View>
                        <View flexDirection='row'
                              style={{marginLeft:15,marginRight:15,borderWidth: 1, borderColor: (Color.loginBorderColor), borderRadius: 3,height:50}}>
                            <Text
                                style={{textAlign:'center',color:(Color.blackColor),alignSelf:'center',paddingLeft:10}}>+{countryCode}</Text>
                            <Input keyboardType='numeric' onChangeText={(text)=>this.onChangePhone(text)}
                                   placeholder={Language().placeholder_phone}
                                   placeholderTextColor={Color.loginTextColor}
                                   style={{paddingLeft:10,flex:1,fontSize:16}}
                                   value={this.state.phone} maxLength={11}/>
                        </View>
                        <View flexDirection='row'
                              style={{margin:15,borderWidth: 1, borderColor: (Color.loginBorderColor), borderRadius: 3,height:50}}>
                            <Input keyboardType='numeric' maxLength={6}
                                   placeholder={Language().placeholder_verify_code}
                                   placeholderTextColor={Color.loginTextColor}
                                   style={{borderWidth: 0,paddingLeft:10,flex:1,fontSize:16}}
                                   onChangeText={(text)=>this.onChangeVerifyCode(text)}/>
                            <TouchableOpacity onPress={this.onGetVerifyCode.bind(this)}
                                              activeOpacity={0.6}
                                              style={{marginRight:10,alignSelf:'center',justifyContent:'center',width:90,borderColor:(Color.phoneLoginButColor),height:35,borderRadius:3,padding:0,borderWidth:1}}>
                                <Text
                                    style={{textAlign:'center',color:(Color.phoneLoginButColor),fontSize:14}}>{this.state.verifyCodeText}</Text>
                            </TouchableOpacity>
                        </View>
                        <Item style={{marginLeft:15,marginRight:15,borderColor:(Color.transparent)}}>
                            <Input password={true} secureTextEntry={true}
                                   placeholder={Language().placeholder_password}
                                   placeholderTextColor={Color.loginTextColor}
                                   style={{borderWidth: 1, borderColor:(Color.loginBorderColor), borderRadius: 3,paddingLeft:10}}
                                   onChangeText={(text)=>this.onChangePassword(text)}/>
                        </Item>
                        <Item style={{margin:15,borderColor:(Color.transparent)}}>
                            <Input
                                placeholder={Language().placeholder_confirm_password}
                                placeholderTextColor={Color.loginTextColor}
                                style={{borderWidth: 1, borderColor:(Color.loginBorderColor), borderRadius: 3,paddingLeft:10}}
                                onChangeText={(text)=>this.onChangeConfirmPassword(text)}
                                password={true}
                                secureTextEntry={true}/>
                        </Item>
                    </Form>
                    <TouchableOpacity
                        style={{borderRadius: 3, marginRight: 15, marginLeft: 15, marginTop: 30,height:45,backgroundColor:(Color.phoneLoginButColor),justifyContent:'center'}}
                        activeOpacity={0.6}
                        onPress={this.onRegister.bind(this)}>
                        <Text style={{color:'#fff',fontSize:16,padding:5,alignSelf:'center'}}>{Language().next}</Text>
                    </TouchableOpacity>
                    <View flexDirection='row' style={{margin:10,justifyContent:'center',marginTop:20}}>
                        <CheckBox checked={this.state.useragreement} onChange={this.checkAgreement.bind(this)}
                                  label='' checkboxStyle={{width:15,height:15}}/>
                        <TouchableOpacity onPress={this.readUserAgreement.bind(this)}
                                          activeOpacity={0.6} style={{marginLeft:2,justifyContent:'center'}}>
                            <View flexDirection='row' style={{justifyContent:'center',alignSelf:'center'}}>
                                <Text
                                    style={{color:'#9b9b9b',fontSize:15,alignSelf:'center',padding:1}}>{Language().agree_and_read}</Text>
                                <Text
                                    style={{color:((Color.phoneLoginButColor)),fontSize:15,alignSelf:'center',padding:1}}>{'《' + Language().user_agreement + '》'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </Content>
                <Loading ref="progressDialog" loadingTitle={Language().operation_loading}/>
            </Container>
        );
    }

    /**
     *
     */
    checkAgreement() {
        this.setState({
            useragreement: !this.state.useragreement
        });
    }

    closeProgress() {

    }

    /**
     * 用户协议
     */
    readUserAgreement() {
        try {
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    this.props.openPage(SCENES.SCENE_USER_AGREEMENT, 'global');
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        }catch (e){
            console.log(e);
        }
    }

    /**
     * 选取国家
     */
    onSelectCountry() {
        if (!this.state.disabledSelectCountryBt) {
            this.props.openPage(SCENES.SCENE_SELECT_COUNTRY, 'global');
        }
        this.state.disabledSelectCountryBt = true;
        this.selectCountryTimer = setTimeout(() => {
            this.state.disabledSelectCountryBt = false;
        }, 1000);
    }

    /**
     * 手机号
     * @param text
     */
    onChangePhone(text) {
        // if (/^[\d]+$/.test(text)) {
        //     this.setState({
        //         phone: text
        //     });
        // } else {
        //     if (/^[\d{1}]$/.test(this.state.phone)) {
        //         this.setState({
        //             phone: ''
        //         });
        //     }
        // }
        //手机号客户端不做验证，交由服务器端，订规则进行验证
        this.setState({
            phone: text
        });
    }

    /**
     * 密码
     * @param text
     */
    onChangePassword(text) {
        this.state.password = text;
    }

    /**
     * 验证码
     * @param text
     */
    onChangeVerifyCode(text) {
        this.state.code = text;
    }

    /**
     * 获取验证码
     */
    onGetVerifyCode() {
        let {phone}=this.state;
        if (!phone.length) {
            Toast.show(Language().placeholder_phone);
            return;
        }
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                let newPhone = "+" + this.state.countryCode + phone;
                if (this.timeHit == 0) {
                }
                if (!this.timer) {
                    this.timer = setInterval(function () {
                        const maxSeconds = 60;
                        let txt = '';
                        this.timeHit++;
                        if (this.timeHit > maxSeconds) {
                            txt = Language().get_verify_code;
                            this.timeHit = 0;
                            clearInterval(this.timer);
                            this.timer = null;
                        } else {
                            txt = (parseInt(maxSeconds) - parseInt(this.timeHit)) + 's';
                        }
                        this.setState({verifyCodeText: txt});
                    }.bind(this), 1000);
                    //点击后60s后才可以重新发送
                    let url = apiDefines.GET_REGISTER_CODE + newPhone;
                    Util.get(url, (code, message, data) => {
                        if (code != 1000) {
                            this.resetVerifyCodeText();
                            Toast.show(message);
                        }
                    }, (failed) => {
                        this.resetVerifyCodeText();
                        Toast.show(Language().not_network);
                    });
                }

            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 重置获取验证码文案以及停止计时
     */
    resetVerifyCodeText() {
        let txt = Language().get_verify_code;
        this.timeHit = 0;
        clearInterval(this.timer);
        this.timer = null;
        this.setState({verifyCodeText: txt});
    }

    /**
     * 用户昵称
     * @param text
     */
    onChangeConfirmPassword(text) {
        this.state.confirmPassword = text;
    }

    componentDidMount() {
        //页面渲染完毕后定义加载view
        progressDialog = this.refs.progressDialog;
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.selectCountryTimer && clearTimeout(this.selectCountryTimer);
        this.finishTimer && clearTimeout(this.finishTimer);
    }

    handleMethod(isConnected) {

    }

    /**
     * 注册
     */
    onRegister() {
        //this.props.openPage(SCENES.SCENE_REGISTER_USERINFO, 'global');
        let {phone, password, code, confirmPassword,useragreement} = this.state;
        if(!useragreement){
            Toast.show(Language().agreement_prompt);
            return;
        }
        if (!phone.length) {
            Toast.show(Language().placeholder_phone);
            return;
        }
        if (!code.length) {
            Toast.show(Language().placeholder_verify_code);
            return;
        }

        if (!password.length) {
            Toast.show(Language().placeholder_password);
            return;
        } else if (!regPassword.test(password)) {
            Toast.show(Language().checkout_password_prompt);
            return;
        }
        if (!confirmPassword.length) {
            Toast.show(Language().placeholder_confirm_password);
            return;
        }
        if (password != confirmPassword) {
            Toast.show(Language().passwords_do_not_match);
            return;
        }
        let newPhone = "+" + this.state.countryCode + phone;
        let registerData = {
            phone: newPhone,
            code: code,
            password: password,
            gender: 1,
            name: ''
        }
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                this.onShowProgressDialog();
                let url = apiDefines.CHECK_VERIFY + newPhone + apiDefines.CODE + code;
                Util.get(url, (code, message, data) => {
                    this.onHideProgressDialog();
                    if (code == 1000) {
                        this.props.saveRegisterData(registerData);
                        try {
                            this.props.openPage(SCENES.SCENE_REGISTER_USERINFO, 'global');
                        }catch (e){
                            console.log(e);
                        }
                    } else {
                        Toast.show(message);
                    }
                }, (failed) => {
                    this.onHideProgressDialog();
                    Toast.show(Language().not_network);
                });
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     * 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    onFinishPage() {
        this.finishTimer = setTimeout(() => {
            this.setState({
                disabled: false
            });
        }, 2000);
        this.props.finishPage('global');
        this.setState({
            disabled: true
        });
    }
}
const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    rightArrow: {
        width: 15, height: 15, resizeMode: 'contain', marginLeft: 10, marginRight: 10, alignSelf: 'center'
    },
    loginButton: {
        borderRadius: 3, backgroundColor: '#F2C454', marginRight: 10, marginLeft: 10, marginTop: 20
    }
});
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        saveRegisterData: (data) => dispatch(saveRegisterData(data)),
    }
);
const mapStateToProps = state => ({
    countryInfo: state.countryListReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps,)(Register);