/**
 * Created by wangxu on 2017/9/5.
 * 管理消息以及好友
 */
import React, {Component} from 'react';
import {StyleSheet, Platform} from 'react-native';
import Language from '../../../resources/language';
import Color from '../../../resources/themColor';
import Message from '../../pages/message';
import Contact from '../../pages/contact';
import * as SCENES from '../../../support/actions/scene';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import MessageTabBar from '../../../support/common/MessageTabBar';
import {deleteFamilyChat, getNewFamilyChat} from '../../../support/actions/groupFamilyChatActions';

const tabNames = [Language().contact, Language().message];
const needLock = (Platform.OS === 'ios') ? true : false;

class MainMessage extends Component {
    static propTypes = {
        userInfo: React.PropTypes.object,
        familyChatData: React.PropTypes.object,
        deleteFamilyChat: React.PropTypes.func,
    }

    constructor() {
        super();
        {/*默认显示首页*/
        }
        this.state = {index: 0};
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        return <ScrollableTabView
            style={styles.tabView}
            renderTabBar={() => <MessageTabBar tabNames={tabNames}/>}
            tabBarPosition='top'
            scrollWithoutAnimation={false}
            locked={needLock}
            onChangeTab={(data)=>this.changeMessageTab(data)}>
            <Contact tabLabel="Contact"/>
            <Message tabLabel="Message"/>
        </ScrollableTabView>
    }

    componentWillMount() {
        //更新家族聊天消息
        if (this.props.userInfo) {
            if (this.props.userInfo.group) {
                if (this.props.userInfo.group.lc_id) {
                    this.props.getNewFamilyChat(this.props.userInfo.group.lc_id);
                }
            }else {
                this.props.getNewFamilyChat(null);
            }
        }
    }

    /**
     * 1--当tab改变时
     * @param data
     */
    changeMessageTab(data) {
        if (data) {
            if (data.i == 1) {
                if (this.props.userInfo) {
                    let userInfo = this.props.userInfo;
                    let groupFamilyInfo = null;
                    if (userInfo) {
                        if (userInfo.group) {
                            groupFamilyInfo = userInfo.group;
                        }
                    }
                    let familyChatData = this.props.familyChatData;
                    if (!groupFamilyInfo && !familyChatData) {
                        this.props.deleteFamilyChat();
                    }
                }
            }
        }
    }
}
const styles = StyleSheet.create({
    tabView: {
        backgroundColor: Color.colorWhite
    }

})
const mapStateToProps = state => ({
    userInfo: state.userInfoReducer.data,
    familyChatData: state.familyChatReducer.data
});
const mapDispatchToProps = dispatch => ({
    deleteFamilyChat: () => dispatch(deleteFamilyChat()),
    getNewFamilyChat: (id) => dispatch(getNewFamilyChat(id)),
});
export default connect(mapStateToProps, mapDispatchToProps,)(MainMessage);