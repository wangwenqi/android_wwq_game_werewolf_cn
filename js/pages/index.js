/**
 * Created by vellee on 23/02/2017.
 */

import Splash from './splash'
import Home from './home';
import Message from './message';
import AppMain from './main';
import Contact from './contact';
import MessageDetail from './message_detail';
import ContactDetail from './contact_detail';
import GameRule from './game_help/game_rule';
import Guide from './guide';
import Setting from './setting';
import AddContact from './addContact';
import EditUserInfo from './edit_user_info';
import LoginOther from './login_other';
import Register from './register';
import ChangePassword from './changePassword';
import RegisterUserInfo from './registerUserInfo';
import UserAgreement from './userAgreement';
import CountryList from './countryIndexList';
import SelectCountry from './selectCountry';
import About from './about';
import PhotoWall from './photo_wall';
import Developer from './developer';
import WebViewPage from './webViewPage';
import NewFriend from './newFriend';
import VoiceRoom from './recreation/voiceRoom';
import GroupList from './groups/group_list';
import CreateGroup from './groups/createGroup';
import MiniGameManager from './miniGame/miniGameManager';
import HousingEstates from './housingEstates';
import HousePropertyCard from './housePropertyCard';
import LikeEachOther from './like_each_other';
import WerewolfGameHome from './home/werewolf_game_home';
import DeleteAllMessage from './deleteAllMessage';
import DeleteAllContact from './deleteAllContact';

export {
    Splash,
    Home,
    Message,
    AppMain,
    Contact,
    MessageDetail,
    GameRule,
    ContactDetail,
    Guide,
    Setting,
    AddContact,
    EditUserInfo,
    LoginOther,
    Register,
    ChangePassword,
    RegisterUserInfo,
    UserAgreement,
    CountryList,
    SelectCountry,
    About,
    PhotoWall,
    Developer,
    WebViewPage,
    NewFriend,
    VoiceRoom,
    CreateGroup,
    GroupList,
    MiniGameManager,
    HousingEstates,
    HousePropertyCard,
    LikeEachOther,WerewolfGameHome,DeleteAllMessage,
    DeleteAllContact,
}