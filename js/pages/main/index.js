/**
 * Created by wangxu on 2017/3/1.
 * main
 * 1--装载home／message／constant／me页面
 */
import React, {Component} from 'react';
import Language from '../../../resources/language';
import Color from '../../../resources/themColor';
import Home from '../../pages/home/mini_game_home';
import MainMessage from '../../pages/mainMessage';
import VoiceRoomManager from '../voiceRoom/voiceRoomManager';
import Me from '../../pages/me';
import Toast from '../../../support/common/Toast';
import Dialog, {Alert, Confirm, Prompt,} from '../../../support/common/dialog';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import {removeChat, deleteChatData} from '../../../support/actions/chatActions';
import {userInfo, updateTourist, getUserInfo, updateGroupInfo} from '../../../support/actions/userInfoActions';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import WerewolfTabBar from '../../../support/common/WerewolfTabBar';
import {fetchMessageList} from '../../../support/actions/messageActions';
import {deleteFriendData, synchronizationFriendList, insertData} from '../../../support/actions/friendActions';
import {fetchFriendStatus} from '../../../support/actions/friendStatusAction';
import {taskInfo} from '../../../support/actions/taskInfoActions';
import {deleteFamilyChat} from '../../../support/actions/groupFamilyChatActions';
import {updateGiftManifest} from '../../../support/actions/giftManifestActions';
import {supportGameList} from '../../../support/actions/miniGameActions';
import {checkOpenRegister} from '../../../support/actions/checkRegisterActions';
import {
    updateEnterNoviceCount,
    setIsNewUser,
    initFriendVersion,
    updateFriendVersion
} from '../../../support/actions/configActions';
import {fetchPageFriendRequestList} from '../../../support/actions/messageActions';
import * as types from '../../../support/actions/actionTypes';
import {promotedData} from '../../../support/actions/systemMessageActions';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import Util from '../../../support/common/utils';
import * as apiDefines from '../../../support/common/gameApiDefines';
import * as Constant from '../../../support/common/constant';
import GuideView from '../../../support/common/GuideView';
import RealmManager from '../../../support/common/realmManager';
import {fetchRecreationPageList} from '../../../support/actions/recreationActions';

import {
    Container, Left, Text, Right, Body, Item, CardItem
} from 'native-base';
import * as SCENES from '../../../support/actions/scene';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Image,
    StatusBar,
    View,
    Platform,
    NativeModules,
    AsyncStorage,
    Dimensions,
    DeviceEventEmitter,
    NativeEventEmitter,
    TouchableOpacity,
    InteractionManager,
} from 'react-native';
import styles from "./style";

const {
    pushRoute,
    popRoute
} = actions;
const {RNMessageSender} = NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
const bgHeader = require('../../../resources/imgs/room_bg_navbar.png');
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const iconAdd = require('../../../resources/imgs/friends_icon_add.png');
const BadgeMarginLeft = ScreenWidth / 3 / 2;
const tabNames = [Language().home, Language().recreation, Language().message, Language().myself];
const tabIconNames = [require('../../../resources/imgs/icon_game.png'), require('../../../resources/imgs/ic_recreation.png'), require('../../../resources/imgs/icon_message.png'), require('../../../resources/imgs/icon_me.png')];
const DialogYellowTitleBg = require('../../../resources/imgs/dialog_base_top_bg.png');
const upgrade_badge = require('../../../resources/imgs/upgrade_badge.png');
let systemMessageDialog;
let serverMessageDialog;
let touristModalDialog;
let reviewModalDialog;

class AppMain extends Component {

    constructor() {
        super();
        {/*默认显示首页*/
        }
        this.state = {index: 0};
    }

    static propTypes = {
        openPage: React.PropTypes.func,
        systemMessage: React.PropTypes.object,
        serverMessage: React.PropTypes.object,
        unReadMessageNumber: React.PropTypes.number,
        isTourist: React.PropTypes.bool,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        removeChatData: React.PropTypes.func,
        deleteFamilyChat: React.PropTypes.func,
        updateUserInfo: React.PropTypes.func,
        updateTourist: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        fetchMessageList: React.PropTypes.func,
        fetchRequestPageList: React.PropTypes.func,
        tabName: React.PropTypes.string,
        userInfo: React.PropTypes.object,
        promotedData: React.PropTypes.object,
        getUserInfo: React.PropTypes.func,
        updatePromotedData: React.PropTypes.func,
        friendListData: React.PropTypes.object,
        friendRequestListData: React.PropTypes.object,
        recreationPageData: React.PropTypes.object,
        enterNoviceCount: React.PropTypes.number,
        updateEnterNoviceCount: React.PropTypes.func,
        setIsNewUser: React.PropTypes.func,
        deleteFriendData: React.PropTypes.func,
        isNewUser: React.PropTypes.bool,
        isTraditional: React.PropTypes.bool,
        fetchRecreationPageList: React.PropTypes.func,
        configData: React.PropTypes.object,
        deleteChatData: React.PropTypes.func,
        synchronizationFriendList: React.PropTypes.func,
        insertData: React.PropTypes.func,
        initFriendVersion: React.PropTypes.func,
        updateFriendVersion: React.PropTypes.func,
        fetchFriendStatus: React.PropTypes.func,
        taskInfo: React.PropTypes.func,
        updateGroupInfo: React.PropTypes.func,
        friendPages: React.PropTypes.number,
        friendVersion: React.PropTypes.number,
        familyChatData: React.PropTypes.object,
        filterFlag: React.PropTypes.number,
        updateGiftManifest: React.PropTypes.func,
        updateSupportGame: React.PropTypes.func,
        updateConfig: React.PropTypes.func,
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        let level = 0;
        let systemMessageTitle = '';
        let systemMessage = '';
        let serverMessageTitle = '';
        let serverMessage = '';
        if (this.props.systemMessage != null && this.props.systemMessage.text != null && this.props.systemMessage.text != '') {
            systemMessage = this.props.systemMessage.text;
            systemMessageTitle = this.props.systemMessage.title;
        }
        if (this.props.serverMessage != null && this.props.serverMessage.text != null && this.props.serverMessage.text != '') {
            serverMessage = this.props.serverMessage.text;
            serverMessageTitle = this.props.serverMessage.title;
        }
        if (this.props.userInfo != null && this.props.userInfo.game != null) {
            level = this.props.userInfo.game.level;
        }
        let locked = this.props.isTourist;
        if (Platform.OS === 'ios') {
            locked = true;
        }
        let iosHead = (Platform.OS === 'ios') ? (
            <CardItem cardBody style={{backgroundColor: (Color.statusBarColor), height: 24}}></CardItem>) : (null);
        return (
            <Container>
                <StatusBar backgroundColor={Color.statusBarColor} barStyle='default' hidden={false}/>
                {iosHead}
                <ScrollableTabView
                    renderTabBar={() => <WerewolfTabBar tabNames={tabNames} tabIconNames={tabIconNames}/>}
                    tabBarPosition='bottom'
                    scrollWithoutAnimation={false}
                    locked={locked}
                    onChangeTab={(data) => this.onChangeWerewolfTab(data)}>
                    <Home tabLabel="Home"/>
                    <VoiceRoomManager tabLabel="VoiceRoomManager"/>
                    <MainMessage tabLabel="MainMessage"/>
                    <Me tabLabel="Mine"/>
                </ScrollableTabView>
                {Dialog.inject()}
                <Prompt ref='systemMessage' message='' title='' posText='' negText=''
                        contentStyle={{
                            backgroundColor: (Color.message_bg_color),
                            width: ScreenWidth - 50,
                            paddingBottom: 0,
                            paddingLeft: 10,
                            paddingRight: 10,
                            maxHeight: ScreenHeight / 3 * 2,
                            borderRadius: 8
                        }}
                        dialogStyle={{backgroundColor: 'rgba(0, 0, 0, 0.25)'}}
                        buttonBarStyle={{height: 0, backgroundColor: Color.transparent}}>
                    <Text
                        style={{
                            alignSelf: 'center',
                            padding: 10,
                            fontSize: 17,
                            fontWeight: '500',
                            textAlign: 'center',
                            color: Color.message_bt_color
                        }}>{systemMessageTitle}</Text>
                    <Text
                        style={{
                            padding: 10,
                            fontSize: 16,
                            fontWeight: '500',
                            color: Color.message_content_color,
                            marginTop: 5
                        }}>
                        {systemMessage}
                    </Text>
                    <TouchableOpacity activeOpacity={0.6}
                                      style={{
                                          backgroundColor: (Color.message_bt_color),
                                          justifyContent: 'center',
                                          height: 42,
                                          margin: 20,
                                          marginBottom: 15,
                                          borderRadius: 8
                                      }}
                                      onPress={this.onSystemMessage.bind(this)}>
                        <Text
                            style={{
                                alignSelf: 'center',
                                fontSize: 16,
                                fontWeight: '500',
                                textAlign: 'center',
                                color: Color.colorWhite
                            }}>{Language().confirm}</Text>
                    </TouchableOpacity>
                </Prompt>
                <Prompt ref='serverMessage' message='' title='' posText='' negText=''
                        contentStyle={{
                            backgroundColor: (Color.message_bg_color),
                            width: ScreenWidth - 50,
                            paddingBottom: 0,
                            paddingLeft: 10,
                            paddingRight: 10,
                            maxHeight: ScreenHeight / 3 * 2,
                            borderRadius: 8
                        }}
                        dialogStyle={{backgroundColor: 'rgba(0, 0, 0, 0.25)'}}
                        buttonBarStyle={{height: 0, backgroundColor: Color.transparent}}>
                    <Text
                        style={{
                            alignSelf: 'center',
                            padding: 10,
                            fontSize: 17,
                            fontWeight: '500',
                            textAlign: 'center',
                            color: Color.message_bt_color
                        }}>{serverMessageTitle}</Text>
                    <Text
                        style={{
                            padding: 10,
                            fontSize: 16,
                            fontWeight: '500',
                            color: Color.message_content_color,
                            marginTop: 5
                        }}>
                        {serverMessage}
                    </Text>
                    <TouchableOpacity activeOpacity={0.6}
                                      style={{
                                          backgroundColor: (Color.message_bt_color),
                                          justifyContent: 'center',
                                          height: 42,
                                          margin: 20,
                                          marginBottom: 15,
                                          borderRadius: 8
                                      }}
                                      onPress={this.onServerMessage.bind(this)}>
                        <Text
                            style={{
                                alignSelf: 'center',
                                fontSize: 16,
                                fontWeight: '500',
                                textAlign: 'center',
                                color: Color.colorWhite
                            }}>{Language().confirm}</Text>
                    </TouchableOpacity>
                </Prompt>
                <GuideView ref="guideView" loadingTitle={Language().request_loading}
                           isTraditional={this.props.isTraditional}/>
                <Confirm ref='touristModal' title={Language().title_prompt_tourist_login} posText={Language().go_login}
                         messageStyle={{alignSelf: 'center', fontSize: 14, marginBottom: 10}}
                         titleStyle={{alignSelf: 'center', fontSize: 17, padding: 5}}
                         contentStyle={{
                             paddingLeft: 1,
                             paddingBottom: 3,
                             paddingRight: 1,
                             width: ScreenWidth / 3 * 2 + 30
                         }}
                         buttonBarStyle={{justifyContent: 'center'}}
                         buttonStyle={{
                             textAlign: 'center',
                             fontSize: 16,
                             padding: 10,
                             marginLeft: 0,
                             marginRight: 0,
                             alignSelf: 'center',
                             justifyContent: 'center',
                             flex: 1,
                             color: Color.system_dialog_bt_text_color
                         }}
                         negText={Language().cancel}
                         enumeSeparator={true}
                         rowSeparator={true}
                         onNegClick={this.dismissTouristDialog.bind(this)} onPosClick={this.toLogin.bind(this)}>
                </Confirm>
                <Prompt ref='reviewDialog' message='' title='' posText='' negText=''
                        contentStyle={{
                            backgroundColor: (Color.transparent),
                            width: ScreenWidth,
                            paddingBottom: 0,
                            paddingLeft: 10,
                            paddingRight: 10,
                            maxHeight: ScreenHeight
                        }}
                        dialogStyle={{backgroundColor: 'rgba(0, 0, 0, 0.25)'}}>
                    <Image
                        style={{
                            height: ScreenWidth / 18 * 17 / 13 * 3,
                            resizeMode: 'contain',
                            width: ScreenWidth / 18 * 17,
                            justifyContent: 'center',
                            alignSelf: 'center'
                        }}
                        source={DialogYellowTitleBg}>
                        <Text
                            style={{
                                fontSize: 22,
                                fontWeight: 'bold',
                                textAlign: 'center',
                                alignSelf: 'center',
                                color: Color.colorWhite,
                                marginBottom: 15
                            }}>{Language().remind}</Text>
                    </Image>
                    <View
                        style={{
                            backgroundColor: (Color.transparent),
                            borderRadius: 5,
                            width: ScreenWidth / 18 * 14,
                            alignSelf: 'center',
                            top: -(ScreenWidth / 18 * 17 / 13 * 3 / 15 * 5 + 1)
                        }}>
                        <View
                            style={{
                                backgroundColor: (Color.review_dialog_color),
                                marginLeft: 2,
                                marginRight: 2,
                                borderBottomLeftRadius: 5,
                                borderBottomRightRadius: 5
                            }}>
                            <Image source={upgrade_badge} style={styles.upgradeBadge}/>
                            <Text
                                style={{
                                    fontSize: 17,
                                    fontWeight: '500',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    color: Color.promoted_text_color
                                }}>{Language().promoted_text}
                                LV{level}</Text>
                            <Text
                                style={{
                                    fontSize: 16,
                                    fontWeight: '400',
                                    alignSelf: 'center',
                                    color: Color.promoted_content_color,
                                    padding: 15
                                }}>{Language().promoted_content}</Text>
                            <TouchableOpacity
                                onPress={this._doReview.bind(this)}
                                style={{
                                    height: 40,
                                    backgroundColor: (Color.promoted_button_color),
                                    justifyContent: 'center'
                                }}>
                                <Text
                                    style={{
                                        color: Color.colorWhite,
                                        fontSize: 16,
                                        padding: 5,
                                        alignSelf: 'center',
                                        fontWeight: '400'
                                    }}>{Language().promoted_review}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={this._doFeedback.bind(this)}
                                style={{
                                    marginTop: 1,
                                    height: 40,
                                    backgroundColor: (Color.promoted_button_color),
                                    justifyContent: 'center'
                                }}>
                                <Text
                                    style={{
                                        color: Color.colorWhite,
                                        fontSize: 16,
                                        padding: 5,
                                        alignSelf: 'center',
                                        fontWeight: '400'
                                    }}>{Language().promoted_feedback}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={this._doCancel.bind(this)}
                                style={{
                                    marginTop: 1,
                                    height: 40,
                                    backgroundColor: (Color.promoted_button_color),
                                    justifyContent: 'center',
                                    borderBottomLeftRadius: 5,
                                    borderBottomRightRadius: 5
                                }}>
                                <Text
                                    style={{
                                        color: Color.colorWhite,
                                        fontSize: 16,
                                        padding: 5,
                                        alignSelf: 'center',
                                        fontWeight: '400'
                                    }}>{Language().promoted_cancel}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Prompt>
            </Container>
        );
    }

    /**
     * 检查是否要展示语音房间列表
     * @returns {boolean}
     * @private
     */
    _checkShowReactAudio() {
        if (this.props.configData && this.props.configData.audio_room_show) {
            if (this.props.configData.audio_room_show != 'react') {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    componentWillUnmount() {
        this.onLeaveGameListener.remove();
        this.onContiuneLoginListener.remove();
        this.onOpenStoreLoginLister.remove();
        this.onStartGameLister.remove();
        this.onNativeMessageLister.remove();
        this.onRNTouristLister.remove();
        this.upgradeTimer && clearTimeout(this.upgradeTimer);
        this.serverMessageTimer && clearTimeout(this.serverMessageTimer);
        this.ReviewModalTimer && clearTimeout(this.ReviewModalTimer);
        this.marketTimer && clearTimeout(this.marketTimer);
        this.feedbckTimer && clearTimeout(this.feedbckTimer);
        this.permissionTimer && clearTimeout(this.permissionTimer);
    }

    componentWillMount() {
        //a每次进入用于进行升级的检查
        this.upgradeTimer = setTimeout(() => {
            rnRoNativeUtils.onCheckUpgrade(false);
        }, 1000);
        //如果不是游客进行以下操作
        if (!this.props.isTourist) {
            //
            this.props.initFriendVersion();
            //请求好友申请列表
            if (this.props.friendRequestListData && this.props.friendRequestListData.list.length > 0) {
                //do nothing
            } else {
                InteractionManager.runAfterInteractions(() => {
                    this.props.fetchRequestPageList();
                });
            }
            //一进去就请求个人详情
            InteractionManager.runAfterInteractions(() => {
                //请求好友列表

                this.synchronizationFriendList();
                //好友在玩

                this.props.fetchFriendStatus();

                if (this.props.userInfo != null) {
                    this.props.getUserInfo(this.props.userInfo.id);
                }
                this.props.updateSupportGame();
            });
        }
    }

    /**
     * 1--服务器消息／登录任务
     */
    componentDidMount() {
        systemMessageDialog = this.refs.systemMessage;
        serverMessageDialog = this.refs.serverMessage;
        touristModalDialog = this.refs.touristModal;
        reviewModalDialog = this.refs.reviewDialog;
        //this.checkNeedGuide();
        this.checkPermission();
        //系统消息提示
        let systemMessage = this.props.systemMessage;
        if (systemMessage != null) {
            try {
                AsyncStorageTool.getStorageData('system_message' + systemMessage.id, (data) => {
                    //console.log('本地存储system_message信息：' + JSON.stringify(data));
                    if (data != null) {
                        if (data.time < systemMessage.time) {
                            //提示系统消息并保存
                            this.promptSystemMessage();
                            let message = {
                                text: systemMessage.text,
                                type: systemMessage.type,
                                title: systemMessage.title,
                                id: systemMessage.id,
                                time: data.time + 1
                            };
                            AsyncStorageTool.mergeSystemMessage(systemMessage.id, JSON.stringify(message));
                        }
                    } else {
                        //提示并保存
                        this.promptSystemMessage();
                        //text: '欢迎来到终极狼人杀', type: 'toast', title: '欢迎', id: 1, time: 1
                        let message = {
                            text: systemMessage.text,
                            type: systemMessage.type,
                            title: systemMessage.title,
                            id: systemMessage.id,
                            time: 1
                        };
                        AsyncStorageTool.saveSystemMessage(systemMessage.id, JSON.stringify(message));
                    }
                });
            } catch (error) {
            }
        }
        try {
            if (Platform.OS === 'android') {
                this.onLeaveGameListener = DeviceEventEmitter.addListener('EVENT_USER_LEAVE', this.onLeaveGameRoom.bind(this));
                this.onContiuneLoginListener = DeviceEventEmitter.addListener('CONTINUE_LOGIN', this.onContinueLogin.bind(this));
                this.onOpenStoreLoginLister = DeviceEventEmitter.addListener('EVENT_USER_LEAVE_FROM_WEB', this.onOpenStoreLogin.bind(this));
                this.onStartGameLister = DeviceEventEmitter.addListener('EVENT_USER_START', this.onStartGame.bind(this));
                this.onNativeMessageLister = DeviceEventEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
                this.onRNTouristLister = DeviceEventEmitter.addListener(Constant.ACTION_TOURIST_EVENT, this.rnTouristMessage.bind(this));
            } else {
                this.onLeaveGameListener = rNMessageSenderEmitter.addListener('EVENT_USER_LEAVE', this.onLeaveGameRoom.bind(this));
                this.onContiuneLoginListener = rNMessageSenderEmitter.addListener('CONTINUE_LOGIN', this.onContinueLogin.bind(this));
                this.onOpenStoreLoginLister = rNMessageSenderEmitter.addListener('EVENT_USER_LEAVE_FROM_WEB', this.onOpenStoreLogin.bind(this))
                this.onStartGameLister = rNMessageSenderEmitter.addListener('EVENT_USER_START', this.onStartGame.bind(this));
                this.onNativeMessageLister = rNMessageSenderEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
                this.onRNTouristLister = DeviceEventEmitter.addListener(Constant.ACTION_TOURIST_EVENT, this.rnTouristMessage.bind(this));
            }
        } catch (e) {

        }
        //显示服务器消息
        this._serverMessage();
        if (!this.props.isTourist) {
            //记录登录
            this._recordLogin();
        }
    }

    /**
     * 给native传在线好友列表
     * @param data
     */
    sendListToNative(data) {
        let newData = {
            list: data
        }
        let Options = {
            needcallback: false,
            sync: true
        }
        let NativeData = {
            action: Constant.ACTION_REQUEST_FRIENDS_LIST,
            params: newData,
            options: Options
        }
        rnRoNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }

    /**
     * 游客模式下操作的消息
     * @param event
     */
    rnTouristMessage(event) {
        if (event && event.needShow) {
            this.onShowTouristModal(event.type);
        }
    }

    /**
     * 统一处理native的消息
     * @param event
     */
    nativeMessage(event) {
        if (event && event.Data) {
            let data = JSON.parse(event.Data);
            switch (data.action) {
                case Constant.ACTION_REQUEST_FRIENDS_LIST:
                    if (data.options && data.options.needcallback) {
                        //修改为请求接口
                        try {
                            let url = apiDefines.GET_FRIEND_STATUSLIST + '2,3,1' + apiDefines.PARAMETER_WITHINFO + 1;
                            Util.get(url, (code, message, data) => {
                                if (data) {
                                    this.sendListToNative(data.data);
                                } else {
                                    this.sendListToNative(null);
                                }
                            }, (failed) => {
                                this.sendListToNative(null);
                            });
                        } catch (e) {
                            console.log('error' + e);
                        }
                    }
                    break;
                case Constant.ACTION_ADD_FRIEND:
                    if (data.params && data.params.user_id) {
                        //数据库插入数据
                        try {
                            let url = apiDefines.GET_SIMPLE_USERINFO + data.params.user_id;
                            Util.get(url, (code, message, data) => {
                                if (code == 1000) {
                                    if (data && data.user) {
                                        //2 如果消息是加好友的消息插入数据
                                        this.props.insertData(data.user);
                                        //1.更新版本号
                                        this.updateFriendVersion();
                                    }
                                }
                            }, (failed) => {
                            });
                        } catch (e) {
                            console.log('ACTION_ADD_FRIEND error' + e);
                        }
                    }
                    break;
                case Constant.ACTION_DELETE_FRIEND:
                    if (data.params && data.params.user_id) {
                        this.props.deleteFriendData(data.params.user_id);
                        //1.更新版本号
                        this.updateFriendVersion();
                    }
                    break;
                case Constant.LEAVE_AUDIO_ROOM:
                    //同时更新个人信息
                    InteractionManager.runAfterInteractions(() => {
                        if (this.props.userInfo) {
                            if (this.props.userInfo.id) {
                                this.props.getUserInfo(this.props.userInfo.id);
                            }
                        }
                    });
                    break;
                case Constant.BLACK_LIST_ADD:
                    //拉黑好友
                    if (data.params && data.params.user_id) {
                        this.props.deleteFriendData(data.params.user_id);
                        this.props.deleteChatData(data.params.user_id);
                        //1.更新版本号
                        this.updateFriendVersion();
                    }
                    break;
                case Constant.ACTION_QUIT_FAMILY:
                    //退出家族
                    this.props.deleteFamilyChat();
                    //更新自己信息
                    if (this.props.userInfo) {
                        if (this.props.userInfo.id) {
                            this.props.getUserInfo(this.props.userInfo.id);
                        }
                    }
                    break;
            }
        }
    }

    updateFriendVersion() {
        //1.更新版本号
        let version = 0;
        if (this.props.friendVersion) {
            version = this.props.friendVersion + 1;
        }
        if (version) {
            this.props.updateFriendVersion(version);
        }
    }

    checkPermission() {
        /**
         * 进入应用申请权限
         * 1->当前服务器/系统公告
         * 2->没有提示升级的弹框
         * 3->才提示弹框
         */
        try {
            this.permissionTimer = setTimeout(() => {
                rnRoNativeUtils.onApplyPermission();
            }, 2000);
        } catch (e) {

        }
    }

    checkNeedGuide() {
        /**
         * 1.level小于等于1
         * 2.没有显示过
         * 3.显示后记录标记
         */
        if (this.props.userInfo && this.props.userInfo.game) {
            if (this.props.userInfo.game.level <= 1) {
                if (!this.props.isNewUser) {
                    //显示引导
                    if (this.refs.guideView) {
                        this.refs.guideView.show();
                        //存储已经显示过了
                        let config = {
                            isNewUser: true
                        }
                        AsyncStorageTool.setNewUserConfig(JSON.stringify(config));
                        this.props.setIsNewUser();
                    } else {
                        this.checkPermission();
                    }
                } else {
                    this.checkPermission();
                }
            } else {
                this.checkPermission();
            }
        } else {
            this.checkPermission();
        }
    }

    /**
     * 游戏开始
     */
    onStartGame = (event) => {
        if (this.props.isTourist) {
            if (event && event.GAME_TYPE) {
                if (event.GAME_TYPE != 'pre_simple') {
                    let count = this.props.enterNoviceCount;
                    this.props.updateEnterNoviceCount(count + 1);
                    let touristData = {
                        noviceCount: count + 1
                    }
                    AsyncStorageTool.setTouristConfig(JSON.stringify(touristData));
                }
            }
        }
    }

    /**
     * native 发送给rn连续登录消息
     */
    onContinueLogin() {
        try {
            if (!this.props.isTourist) {
                //记录登录
                this._recordLogin();
            }
        } catch (err) {

        }
    }

    /**
     * 1--游戏离开房间通知更新
     */
    onLeaveGameRoom() {
        try {
            this.serverMessageTimer = setTimeout(() => {
                this._serverMessage();
            }, 200);
        } catch (e) {

        }
    }

    /**
     * 离开商城或是任务页面
     */
    onOpenStoreLogin() {
        if (this.props.userInfo && this.props.userInfo.id) {
            this.props.getUserInfo(this.props.userInfo.id);
        }
        //更新可领取的任务
        this.props.taskInfo();
    }

    /**
     * 1--服务器消息提示
     */
    promptSystemMessage() {
        let systemMessage = this.props.systemMessage;
        if (systemMessage.type == 'toast' && systemMessage.text != '') {
            Toast.show(systemMessage.text);
        } else if (systemMessage.type == 'dialog' && systemMessage.text != '' && systemMessage.title != '') {
            this.onShowSystemDialog();
        }
    }

    /**
     * 1--显示服务器消息的dialog
     */
    onShowServerDialog() {
        if (serverMessageDialog != null) {
            serverMessageDialog.show();
        }
    }

    /**
     * 1--hide
     */
    onHideServerDialog() {
        if (serverMessageDialog != null) {
            serverMessageDialog.hide();
        }
    }

    /**
     * 1--系统消息显示
     */
    onShowSystemDialog() {
        if (systemMessageDialog != null) {
            systemMessageDialog.show();
        }
    }

    /**
     * 1--隐藏系统消息
     */
    onHideSystemDialog() {
        if (systemMessageDialog != null) {
            systemMessageDialog.hide();
        }
    }

    /**
     * 1--游客模式登录下不同操作的提示
     * @param type
     */
    onShowTouristModal(type) {
        let content = Language().content_prompt_tourist_login;
        switch (type) {
            case types.ACTION_MESSAGE:
                content = Language().getMessage_prompt_tourist_login;
                break;
            case types.ACTION_RECREATION:
                content = Language().getRecreation_prompt_tourist_login;
                break;
            case types.ACTION_ME:
                content = Language().content_prompt_tourist_login;
                break;
            default:
                content = Language().content_prompt_tourist_login;
                break;
        }
        if (touristModalDialog != null) {
            touristModalDialog.show({message: content});
        }
    }

    onHideTouristModal() {
        if (touristModalDialog != null) {
            touristModalDialog.hide();
        }
    }

    onShowReviewModal() {
        if (reviewModalDialog != null) {
            reviewModalDialog.show();
        }
    }

    onHideReviewModal() {
        if (reviewModalDialog != null) {
            reviewModalDialog.hide();
        }
    }

    /**
     *1--登录任务的操作
     * @private
     */
    _recordLogin() {
        try {
            AsyncStorageTool.getStorageData(AsyncStorageTool.LOGIN_STATE, (loginState) => {
                //console.log('本地存储登录信息：' + JSON.stringify(loginState));
                if (loginState == null) {
                    //直接上报一次
                    this.reportLoginState();
                } else {
                    //时间进行比对是否超过一天
                    let day = (new Date()).getDate() - new Date(loginState.time).getDate();
                    if (day > 0) {
                        this.reportLoginState();
                    }
                }
            });
        } catch (error) {
        }
    }

    /**
     *1--上报服务器登录
     */
    reportLoginState() {
        let url = apiDefines.TASK_DO + apiDefines.CONTINUE_LOGIN;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                let loginState = {
                    time: new Date()
                }
                AsyncStorageTool.saveLoginState(JSON.stringify(loginState));
            } else {

            }
        }, (error) => {

        });
    }

    /**
     * 有服务器消息就不显示评价的弹框
     * @private
     */
    _serverMessage() {
        if (this.props.serverMessage != null) {
            let message = this.props.serverMessage.text;
            if (message != null && message != '') {
                this.onShowServerDialog();
            } else {
                this.checkNeedPromoted();
            }
        } else {
            this.checkNeedPromoted();
        }
    }

    /**
     *1-- 检测是否需要显示评论弹框
     *2--1.是否升级了;2.是否达到显示的级别10/20/40级;3.是否不再显示了
     *3-- 以前是否显示过  prompted: false,
     *4-- 是否需要显示  needPrompt: true
     *5--  当提示显示时，当时的等级 level:0
     */
    checkNeedPromoted() {
        let promotedData = this.props.promotedData;
        let level = 0;
        if (this.props.userInfo != null && this.props.userInfo.game != null) {
            level = this.props.userInfo.game.level;
        }
        if (promotedData != null && promotedData.needPrompt) {
            if (promotedData.prompted) {
                //以前显示过
                if ((level == 10 || level == 20 || level == 40) && level > promotedData.level) {
                    let data = {
                        prompted: true,
                        needPrompt: true,
                        level: level
                    }
                    if (level == 40) {
                        data.needPrompt = false;
                    }
                    this.onPromotedShow(data);
                }
            } else {
                //从未显示过
                if (level >= 20 && level < 40) {
                    //显示一次，然后按正常的逻辑走
                    let data = {
                        prompted: true,
                        needPrompt: true,
                        level: level
                    }
                    this.onPromotedShow(data);
                } else if (level >= 40) {
                    //只显示这一次
                    let data = {
                        prompted: true,
                        needPrompt: false,
                        level: level
                    }
                    this.onPromotedShow(data);
                } else {
                    //不显示但是记录数据
                    let data = {
                        prompted: true,
                        needPrompt: true,
                        level: level
                    }
                    this.props.updatePromotedData(data);
                    AsyncStorageTool.savePromotedData(JSON.stringify(data));
                }
            }
        }
    }

    onPromotedShow(data) {
        this.props.updatePromotedData(data);
        AsyncStorageTool.savePromotedData(JSON.stringify(data));
        this.ReviewModalTimer = setTimeout(() => {
            this.onShowReviewModal();
        }, 1000);
    }

    dismissTouristDialog() {
        this.onHideTouristModal();
    }

    /**
     * 1--token失效后提示登录
     */
    toLogin() {
        const routes = this.props.navigation.routes;
        if (routes.length > 1) {
            for (var i = 0; i < routes.length; i++) {
                this.props.finishPage('global');
            }
        }
        AsyncStorageTool.removeUserInfo();
        try {
            this.props.openPage(SCENES.SCENE_SPLASH, 'global');
        } catch (e) {
            console.log(e)
        }
        AsyncStorageTool.removeSignTourist();
        this.props.removeChatData();
        this.props.updateUserInfo(null);
        this.props.updateTourist(false);
        this.onHideTouristModal();
    }

    /**
     * 同步好友信息
     */
    synchronizationFriendList() {
        try {
            let url = apiDefines.GET_FRIEND_PAGE_LIST;
            Util.get(url, (code, message, data) => {
                if (code == 1000) {
                    if (data && data.total_page) {
                        RealmManager.deleteFriend((success) => {
                            let handleCount = 0;
                            for (let i = 0; i < data.total_page; i++) {
                                RealmManager.synchronizationFriendList(false, i, (request) => {
                                    handleCount++;
                                    if (handleCount == data.total_page) {
                                        InteractionManager.runAfterInteractions(() => {
                                            this.props.synchronizationFriendList(0, data.total_page);
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            }, (failed) => {
            });
        } catch (e) {
            console.log('synchronizationFriendList error' + e);
        }

    }

    /**
     * 1--当tab改变时
     * @param data
     */
    onChangeWerewolfTab(data) {
        if (data != null && data.i != null) {
            if (this.props.isTourist) {
                // if (this.props.tabName != types.ACTION_HOME) {
                //     this.onShowTouristModal(this.props.tabName);
                // }
            } else {
                InteractionManager.runAfterInteractions(() => {
                    this.setState({
                        index: data.i
                    });
                });
                if (data.i == 2) {
                    //更新家族消息
                    InteractionManager.runAfterInteractions(() => {
                        this.props.updateGroupInfo();
                        this.updateGroupInfoMessage();
                    });
                    //进入好友列表时，如果没有数据才进行请求
                    if (this.props.friendListData && this.props.friendListData.list.length > 0) {
                        //1-根据版本号确定是不是需要更新
                        //2-如果本地好友页数小于服务器端返回的pageCount 依然要进行同步数据操作
                        let localCount = this.props.friendListData.total_page;
                        let serverCount = this.props.friendPages;
                        if (localCount < serverCount) {
                            //同步好友信息
                            InteractionManager.runAfterInteractions(() => {
                                this.synchronizationFriendList();
                            });
                        }
                    } else {
                        //请求好友列表
                        InteractionManager.runAfterInteractions(() => {
                            this.synchronizationFriendList();
                        });
                    }
                    if (this.props.friendRequestListData && this.props.friendRequestListData.list.length > 0) {
                        //do nothing
                    } else {
                        InteractionManager.runAfterInteractions(() => {
                            this.props.fetchRequestPageList();
                        });
                    }
                } else if (data.i == 1) {
                    /**
                     * 语音房列表
                     * 暂时不处理，采用最新的语音房
                     */
                    // InteractionManager.runAfterInteractions(() => {
                    //     this.props.fetchRecreationPageList(1, types.RECREATION_DATA, this.props.filterFlag);
                    //
                    // });
                    // //通知语音房间滑动到最顶
                    // DeviceEventEmitter.emit(Constant.SCROLL_RECREATION_LIST, null);
                } else if (data.i == 3) {
                    //进入我的页面
                    InteractionManager.runAfterInteractions(() => {
                        this.props.updateConfig();
                        if (this.props.userInfo != null) {
                            this.props.getUserInfo(this.props.userInfo.id);
                            let gifts = this.props.userInfo.gift;
                            if (gifts && gifts.length > 0) {
                                this.props.updateGiftManifest(gifts);
                            }
                        }
                    });

                } else {
                    InteractionManager.runAfterInteractions((() => {
                        this.props.fetchFriendStatus();
                    }));
                }
            }
        }
    }

    /**
     *
     */
    updateGroupInfoMessage() {
        InteractionManager.runAfterInteractions(() => {
            let url = apiDefines.GROUP_INFO;
            Util.get(url, (code, message, data) => {
                if (code == 1000) {
                    if (data && data.group) {

                    } else {
                        let familyChatData = this.props.familyChatData;
                        let unReadFamilyChat = 0;
                        if (familyChatData && familyChatData.unreadNumber) {
                            unReadFamilyChat = familyChatData.unreadNumber;
                        }
                        if (unReadFamilyChat) {
                            this.props.deleteFamilyChat();
                        }
                    }
                }
            }, (failed) => {
            });
        });
    }

    onFinishPage() {
        this.props.finishPage('global');
    }

    onSystemMessage() {
        this.onHideSystemDialog();
    }

    onServerMessage() {
        this.onHideServerDialog();
    }

    _doReview() {
        this.onHideReviewModal();
        this.marketTimer = setTimeout(() => {
            rnRoNativeUtils.onApplicationMarket();
        }, 150);
        let data = {
            prompted: true,
            needPrompt: false,
            level: this.props.promotedData.level
        }
        this.props.updatePromotedData(data);
        AsyncStorageTool.savePromotedData(JSON.stringify(data));
    }

    _doFeedback() {
        this.onHideReviewModal();
        this.feedbckTimer = setTimeout(() => {
            rnRoNativeUtils.onFeedback();
        }, 150);
    }

    _doCancel() {
        this.onHideReviewModal();
    }

}

const mapStateToProps = state => ({
    systemMessage: state.systemMessageReducer.data,
    serverMessage: state.serverMessageReducer.data,
    unReadMessageNumber: state.chatReducer.messageNumber,
    isTourist: state.userInfoReducer.isTourist,
    navigation: state.cardNavigation,
    tabName: state.userInfoReducer.tabName,
    userInfo: state.userInfoReducer.data,
    promotedData: state.promotedReducer.data,
    friendListData: state.friendReducer.data,
    enterNoviceCount: state.configReducer.enter_novice_count,
    isNewUser: state.configReducer.isNewUser,
    friendRequestListData: state.messageReducer.data,
    isTraditional: state.configReducer.isTraditional,
    recreationPageData: state.recreationReducer.data,
    configData: state.checkRegisterReducer.data,
    friendPages: state.friendReducer.pages,
    friendVersion: state.configReducer.friendVersion,
    filterFlag: state.recreationReducer.filterFlag,
    familyChatData: state.familyChatReducer.data

});
const mapDispatchToProps = dispatch => ({
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        removeChatData: () => dispatch(removeChat()),
        updateUserInfo: (data) => dispatch((userInfo(data))),
        updateTourist: (isTourist) => dispatch((updateTourist(isTourist))),
        finishPage: (key) => dispatch(popRoute(key)),
        getUserInfo: (userId) => dispatch(getUserInfo(userId)),
        fetchMessageList: () => dispatch(fetchMessageList()),
        updatePromotedData: (data) => dispatch(promotedData(data)),
        updateEnterNoviceCount: (count) => dispatch(updateEnterNoviceCount(count)),
        setIsNewUser: () => dispatch(setIsNewUser()),
        fetchRequestPageList: () => dispatch(fetchPageFriendRequestList()),
        deleteFriendData: (id) => dispatch(deleteFriendData(id)),
        fetchRecreationPageList: (page, actionType, filter) => dispatch(fetchRecreationPageList(page, actionType, filter)),
        deleteChatData: (id) => dispatch(deleteChatData(id)),
        synchronizationFriendList: (pages) => dispatch(synchronizationFriendList(pages)),
        insertData: (data) => dispatch(insertData(data)),
        initFriendVersion: () => dispatch(initFriendVersion()),
        updateFriendVersion: (version) => dispatch(updateFriendVersion(version)),
        fetchFriendStatus: () => dispatch(fetchFriendStatus()),
        taskInfo: () => dispatch(taskInfo()),
        deleteFamilyChat: () => dispatch(deleteFamilyChat()),
        updateGroupInfo: () => dispatch(updateGroupInfo()),
        updateConfig: () => dispatch(checkOpenRegister()),
        updateGiftManifest: (gifts) => dispatch(updateGiftManifest(gifts)),
        updateSupportGame: () => dispatch(supportGameList()),
    }
);
export default connect(mapStateToProps, mapDispatchToProps,)(AppMain);