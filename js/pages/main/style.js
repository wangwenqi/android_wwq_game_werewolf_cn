/**
 * Created by wangxu on 2017/3/1.
 */
'use strict';
import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
        footerImage: {
            width: 25,
            height: 25,
            resizeMode: 'contain',
        },
        footerStyle: {
            backgroundColor: '#130426'
        },
        headerImageStyle: {resizeMode: 'contain'},
        homeIcon: {
            width: 20,
            height: 20,
            resizeMode: 'contain',
            padding: 5
        },
        upgradeBadge: {
            width: 65,
            height: 65,
            alignSelf: 'center',
            resizeMode: 'contain',
            margin: 10,
        },
        upgradeText: {
            fontSize: 30, fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', padding: 5
        }
    })
    ;
module.exports = styles;