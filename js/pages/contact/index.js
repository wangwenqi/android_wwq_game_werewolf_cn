/**
 * Created by wangxu on 2017/3/1.
 */
import React, {Component} from 'react';
import Language from '../../../resources/language'
import {Left, Right, Body, Text, Input} from 'native-base';
import {
    StyleSheet,
    Image,
    ToastAndroid,
    Dimensions,
    View,
    Platform,
    NativeModules,
    ListView,
    RefreshControl,
    ActivityIndicator,
    InteractionManager,
    TouchableOpacity,
    ScrollView,
    Modal
} from 'react-native';
import * as SCENES from '../../../support/actions/scene'
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import Badge from 'react-native-smart-badge';
import Moment from 'moment';
import RealmManager from '../../../support/common/realmManager';

import {
    fetchFriendList,
    fetchMoreFriend,
    fetchPageFriendList,
    refreshPageFriendList, clearFriendList, deleteFriend, getFriendList, updateGameStatus, synchronizationFriendList
} from '../../../support/actions/friendActions';
import Toast from '../../../support/common/Toast';
import Dialog, {Alert, Confirm} from '../../../support/common/dialog';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import {removeChat} from '../../../support/actions/chatActions';
import CachedImage from '../../../support/common/CachedImage';
import LoadImageView from '../../../support/common/LoadImageView';//
import {userInfo, updateTourist} from '../../../support/actions/userInfoActions';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import {clearMessageList, fetchPageFriendRequestList} from '../../../support/actions/messageActions';
import {removeFriendStatus} from '../../../support/actions/friendStatusAction';
import Color from '../../../resources/themColor';
import LoadMoreFooter from '../../../support/common/LoadMoreFooter'
import {SwipeListView} from 'react-native-swipe-list-view';
import * as Constant from '../../../support/common/constant';
import netWorkTool from '../../../support/common/netWorkTool';
import * as types from '../../../support/actions/actionTypes';
import {
    updateFriendVersion
} from '../../../support/actions/configActions';
import Loading from '../../../support/common/Loading';
import Util from '../../../support/common/utils';
import * as apiDefines from '../../../support/common/gameApiDefines';

const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const PersonalInfoActivity = 'com.game_werewolf.activity.PersonalInfoActivity';
const gender = require('../../../resources/imgs/ic_male.png');
const genderFemale = require('../../../resources/imgs/ic_female.png');
const defaultContactBg = require('../../../resources/imgs/bg_contact_blank.png');
const gameRoomBg = require('../../../resources/imgs/icon_game_room.png');
const activeOpacityValue = (Platform.OS === 'ios') ? 0.6 : 0.6;
const disableLiftSwipe = (Platform.OS === 'ios') ? true : true;
const arrows = require('../../../resources/imgs/bg_arrow_right.png');
const recreationIcon = require('../../../resources/imgs/icon_recreation_ic.png');
const newFriendIcon = require('../../../resources/imgs/icon_requset_friend.png');
const voiceIconL = require('../../../resources/imgs/icon_item_voice_left.png');
const voiceIconR = require('../../../resources/imgs/icon_itme_voice_right.png');
const LoadImage = LoadImageView;
const {
    pushRoute,
    popRoute
} = actions;
let needLoginDialog;
let deleteContactDialog;
let friendList = new Array();
let progressDialog;
let that;
//常量设置
let cellWH = 30;
let friendRequest = new Array();

class Contact extends Component {
    static propTypes = {
        navigator: React.PropTypes.shape({}),
        openPage: React.PropTypes.func,
        code: React.PropTypes.number,
        message: React.PropTypes.string,
        hostLocation: React.PropTypes.string,
        data: React.PropTypes.object,
        fetch: React.PropTypes.func,
        fetchPageList: React.PropTypes.func,
        fetchMoreList: React.PropTypes.func,
        deleteFriend: React.PropTypes.func,
        fetchRefreshList: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        token: React.PropTypes.string,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        removeChatData: React.PropTypes.func,
        updateUserInfo: React.PropTypes.func,
        updateTourist: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        clearMessageList: React.PropTypes.func,
        removeFriendStatus: React.PropTypes.func,
        clearFriendList: React.PropTypes.func,
        isRefreshing: React.PropTypes.bool,
        isNoMore: React.PropTypes.bool,
        fetchRequestPageList: React.PropTypes.func,
        updateGameStatus: React.PropTypes.func,
        fetchRequestData: React.PropTypes.object,
        getFriendList: React.PropTypes.func,
        updateFriendVersion: React.PropTypes.func,
        updateTime: React.PropTypes.object,
        configData: React.PropTypes.object,
        friendVersion: React.PropTypes.number,
    }

    constructor(props) {
        super(props);
        that = this;
        if (this.props.data && this.props.data.list) {
            friendList = this.props.data.list;
        }
        if (this.props.fetchRequestData) {
            friendRequest = this.props.fetchRequestData.list;
        }
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        //2.设置返回数据
        this.state = {
            dataSource: ds.cloneWithRows(friendList),
            friendRequestDataSource: ds.cloneWithRows(friendRequest),
            userId: '',
            deleteIndex: 0,
            roomID: '',
            roomPassword: '',
            showModalDialog: false,
            showModelProgress: false,
            game_type: ''
        }
    }

    componentDidMount() {
        needLoginDialog = this.refs.login;
        deleteContactDialog = this.refs.deleteContact;
        progressDialog = this.refs.progressDialog;
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    componentWillMount() {
    }

    handleMethod(isConnected) {

    }

    /**
     *1-- 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     *1-- 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.code != 0 && this.props.code != 1000) {
            if (this.props.message != '' && this.props.message != null) {
                if (this.props.code != 1004) {
                    //请求超时的消息不提示
                    //Toast.show(this.props.message);
                } else {
                    this.checkToken();
                }
            }
        }
        return (!_.isEqual(this.props.data, nextProps.data) || !_.isEqual(this.props.fetchRequestData, nextProps.data));
    }

    /**
     * 渲染页面
     * @returns {XML}
     */
    render() {
        if (this.props.fetchRequestData && this.props.fetchRequestData.list) {
            friendRequest = this.props.fetchRequestData.list;
        } else {
            friendRequest = new Array();
        }
        if (this.props.data != null && this.props.data.list) {
            if (this.props.data && this.props.data.list) {
                friendList = this.props.data.list;
            }

        }
        return (<View style={{backgroundColor: Color.colorWhite, flex: 1}}>
                {this._defaultView()}
                <View style={{height: 15, backgroundColor: Color.colorWhite, justifyContent: 'center', marginTop: 0}}>
                    <Text
                        style={{
                            alignSelf: 'center',
                            fontSize: 9,
                            color: Color.game_status_color
                        }}>{Language().update_time}{that.getUpdateTime()}</Text>
                </View>
                <SwipeListView dataSource={this.state.dataSource.cloneWithRows(friendList)}
                               ref="listView"
                               renderRow={this.renderRow}
                               renderFooter={this.renderFooter}
                               initialListSize={1}
                               removeClippedSubviews={true}
                               pageSize={5}
                               onScroll={this._onScroll}
                               onEndReached={this._onEndReach}
                               onEndReachedThreshold={10}
                               enableEmptySections={true}
                               rightOpenValue={-75}
                               disableRightSwipe={true}
                               disableLeftSwipe={disableLiftSwipe}
                               renderHeader={this.renderHeader}
                               refreshControl={
                                   <RefreshControl
                                       refreshing={this.props.isRefreshing}
                                       onRefresh={this._onRefresh}
                                       colors={[Color.list_loading_color]}/>
                               }
                               closeOnRowBeginSwipe={true}
                               renderHiddenRow={(data, secId, rowId, rowMap) => (
                                   this._onDeleteRowView(data, rowId, secId, rowMap)
                               )}/>
                {Dialog.inject()}
                <Alert ref='login' title={Language().title_login} message={Language().need_login_message}
                       messageStyle={{alignSelf: 'center', fontSize: 14, marginBottom: 10}}
                       titleStyle={{alignSelf: 'center', fontSize: 17, padding: 5}}
                       contentStyle={{
                           paddingLeft: 1,
                           paddingBottom: 3,
                           paddingRight: 1,
                           width: ScreenWidth / 3 * 2 + 30
                       }}
                       buttonBarStyle={{justifyContent: 'center'}}
                       buttonStyle={{
                           textAlign: 'center',
                           fontSize: 16,
                           padding: 10,
                           marginLeft: 0,
                           marginRight: 0,
                           alignSelf: 'center',
                           justifyContent: 'center',
                           flex: 1,
                           color: Color.system_dialog_bt_text_color
                       }}
                       posText={Language().confirm}
                       rowSeparator={true}
                       onPosClick={this.login.bind(this)}> </Alert>
                <Confirm ref='deleteContact' message={Language().delete_contact} title={Language().delete}
                         posText={Language().confirm} negText={Language().cancel}
                         titleStyle={{alignSelf: 'center', fontSize: 17, padding: 5}}
                         messageStyle={{alignSelf: 'center', fontSize: 14, marginBottom: 10}}
                         contentStyle={{
                             paddingLeft: 1,
                             paddingBottom: 3,
                             paddingRight: 1,
                             width: ScreenWidth / 3 * 2 + 30
                         }}
                         buttonBarStyle={{justifyContent: 'center'}}
                         enumeSeparator={true}
                         rowSeparator={true}
                         buttonStyle={{
                             textAlign: 'center',
                             fontSize: 16,
                             padding: 10,
                             marginLeft: 0,
                             marginRight: 0,
                             alignSelf: 'center',
                             justifyContent: 'center',
                             flex: 1,
                             color: Color.system_dialog_bt_text_color
                         }}
                         onNegClick={this.onCancelDelete.bind(this)} onPosClick={this.onDeleteContact.bind(this)}>
                </Confirm>
                <Loading ref="progressDialog" loadingTitle={Language().operation_loading}/>
                {/*创建房间以及搜索房间的弹框*/}
                <Modal visible={this.state.showModalDialog} transparent={true} animationType='fade'>
                    <View flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                          style={[styles.dialogContainer]}>

                        <ScrollView alwaysBounceVertical={false}>
                            <View
                                style={{
                                    borderColor: Color.action_room_color,
                                    borderRadius: 3,
                                    borderWidth: 2,
                                    width: ScreenWidth - 70,
                                    maxHeight: ScreenHeight / 3 * 2,
                                    alignSelf: 'center',
                                    backgroundColor: Color.colorWhite
                                }}>
                                <View flexDirection='row' style={{justifyContent: 'center', padding: 5}}>
                                    <Image style={styles.modelTitleIcon} source={voiceIconL}/>
                                    <Text
                                        style={{
                                            color: '#3A0042',
                                            alignSelf: 'center',
                                            fontSize: 17,
                                            fontWeight: 'bold',
                                            textAlign: 'center',
                                            padding: 5,
                                            width: (ScreenWidth - 90) / 2
                                        }}>{Language().placeholder_password}</Text>
                                    <Image style={styles.modelTitleIcon} source={voiceIconR}/>
                                </View>
                                {this._modelInitPassword()}
                                <View flexDirection='row' style={{justifyContent: 'center'}}>
                                    <TouchableOpacity activeOpacity={0.9}
                                                      onPress={this.onCloseModal.bind(this)}
                                                      style={styles.actionButton}>
                                        <Text
                                            style={{
                                                alignSelf: 'center',
                                                color: Color.colorWhite
                                            }}>{Language().cancel}</Text>
                                    </TouchableOpacity>
                                    <View style={{
                                        backgroundColor: '#8732ff',
                                        width: 1,
                                        paddingBottom: 4,
                                        paddingTop: 4
                                    }}/>
                                    <TouchableOpacity activeOpacity={0.8} onPress={this.onConfirmModal.bind(this)}
                                                      style={styles.actionButton}>
                                        <Text
                                            style={{
                                                alignSelf: 'center',
                                                color: Color.colorWhite
                                            }}>{Language().confirm}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.showModelProgress ? (
                                <View style={[styles.loading, {left: ScreenWidth / 2 - 55, top: 45}]}>
                                    <Spinner size='small' style={{height: 35}}/>
                                    <Text
                                        style={{
                                            marginTop: 5,
                                            fontSize: 14,
                                            color: Color.colorWhite
                                        }}>{Language().request_loading}</Text>
                                </View>) : null}
                        </ScrollView>
                    </View>
                </Modal>
            </View>
        );
    }

    /**
     * 房间ID
     * @returns {XML}
     * @private
     */
    _modelInitPassword() {
        return (<View style={{margin: 10, marginTop: 0}}>
            <Text
                style={{
                    color: '#666666',
                    fontSize: 12,
                    alignSelf: 'center',
                    marginBottom: 8
                }}>{Language().room_number + this.state.roomID}</Text>
            <View style={{borderColor: Color.action_room_color, borderRadius: 3, borderWidth: 1, marginBottom: 5}}>
                <Input
                    style={{
                        fontSize: 12,
                        color: (Color.editUserTextColor),
                        padding: 5,
                        paddingRight: 10,
                        paddingLeft: 10,
                        height: 30
                    }}
                    placeholder={Language().placeholder_room_password}
                    placeholderTextColor={Color.prompt_text_color}
                    autoFocus={true} maxLength={4}
                    keyboardType='numeric'
                    onChangeText={(text) => this._changeChangePassword(text)} value={this.state.roomPassword}/>
            </View>
        </View>);
    }

    /**
     * 房间 号
     * @param text
     * @private
     */
    _changeChangePassword(text) {
        if (/^[\d]+$/.test(text)) {
            this.setState({
                roomPassword: text
            });
        } else {
            if (/^[\d{1}]$/.test(this.state.roomPassword)) {
                this.setState({
                    roomPassword: ''
                });
            }
        }
    }

    getUpdateTime() {
        if (this.props.updateTime) {
            let time = Moment(this.props.updateTime).format('YYYY-MM-DD HH:mm:ss');
            return time;
        }
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    renderRow(item, sectionId, rowId) {
        return (
            <TouchableOpacity onPress={() => that.actionContact(item, rowId)}
                              activeOpacity={activeOpacityValue}
                              style={{backgroundColor: Color.colorWhite}}>
                <View
                    style={{
                        height: 65,
                        marginLeft: -17,
                        paddingLeft: 15,
                        alignSelf: 'flex-start',
                        backgroundColor: Color.colorWhite,
                        overflow: 'hidden'
                    }}
                    flexDirection='row'>
                    <View style={{flex: 1, alignSelf: 'center', justifyContent: 'center', marginLeft: 15}}>
                        <View style={styles.rowImage}>
                            <LoadImage style={styles.headThumbnail}
                                       source={(item.image == '' || item.image == null) ? (require('../../../resources/imgs/ic_default_user.png')) : ({uri: item.image})}
                                       defaultSource={require('../../../resources/imgs/ic_default_user.png')}/>
                            <Image source={(item.sex == null || item.sex == 1) ? gender : genderFemale}
                                   style={styles.gender}></Image>
                        </View>
                    </View>
                    <View style={{
                        flex: 3,
                        paddingBottom: 0,
                        paddingLeft: 12,
                        alignSelf: 'center',
                        justifyContent: 'center'
                    }}>
                        <Text numberOfLines={1} style={{color: '#313035', fontSize: 15, padding: 2}}>{item.name}</Text>
                        <View flexDirection='row'>
                            <Text numberOfLines={1}
                                  style={{
                                      color: '#000',
                                      fontSize: 10,
                                      fontWeight: 'bold',
                                      fontStyle: 'italic',
                                      borderWidth: 1,
                                      blackColor: '#000',
                                      backgroundColor: '#F7E53B',
                                      width: 40,
                                      textAlign: 'center',
                                      height: 15,
                                      padding: 1,
                                      borderRadius: 2,
                                      marginTop: 3
                                  }}>LV.{(item.game == null || item.game.level == null) ? 0 : item.game.level}</Text>
                            {that.userStatusView(item.game_status)}
                        </View>
                    </View>
                    <Right style={{flex: 2}}>
                        {that.gameStatusView(item.game_status, item.room_id, item.status)}
                    </Right>
                </View>
                <View style={styles.rowSeparator}></View>
            </TouchableOpacity>
        );
    }

    /**
     * 显示删除的view
     * @private
     */
    _onDeleteRowView(data, rowId, secId, rowMap) {
        //TODO 暂时去掉删除功能
        // return Platform.OS === 'android' ? <View style={styles.rowBack}>
        //         <TouchableOpacity style={[styles.backRightBtn, styles.backRightBtnRight]}
        //                           onPress={()=> that.deleteRow(data,rowId,secId,rowMap) }>
        //             <Text style={{color:Color.colorWhite}}>{Language().delete}</Text>
        //         </TouchableOpacity>
        //     </View> : null;
    }

    /**
     * android长按删除
     * @param item
     */
    actionLongPressMessage(item, rowId) {
        //TODO 暂时去掉
        // if (Platform.OS === 'android') {
        //     if (item && item.id) {
        //         that.state.userId = item.id;
        //         that.state.deleteIndex = rowId;
        //         that.onShowDeleteContactDialog(item.name);
        //     }
        // }
    }

    /**
     *
     * @param status
     * @returns {*}
     */
    userStatusView(status) {
        let userStatusView = null;
        if (status) {
            userStatusView = (<View
                style={{
                    backgroundColor: '#f79c21',
                    width: 6,
                    height: 6,
                    borderRadius: 6,
                    marginLeft: 5,
                    alignSelf: 'center'
                }}/>)
        }
        return userStatusView;
    }

    /**
     * 时间格式化
     * @param time
     * @private
     */
    _onDateFormat(status) {
        let formatTime = '';
        if (status) {
            let arrayStatus = new Array();
            arrayStatus = status.split(':');
            if (arrayStatus && arrayStatus.length >= 2) {
                try {
                    let statusDate = new Date((arrayStatus[1] - 0));
                    let newDate = new Date();
                    let hour = newDate.getHours() - statusDate.getHours();
                    let day = newDate.getDate() - statusDate.getDate();
                    let year = newDate.getFullYear() - statusDate.getFullYear();
                    let month = newDate.getMonth() - statusDate.getMonth();
                    if (year == 0) {
                        if (month == 0) {
                            if ((day > 0 && day <= 15) || hour == 24) {
                                formatTime = day + Language().day;
                            } else if (hour == 0) {
                                formatTime = Language().just_now;
                            } else if (hour > 0 && hour < 24) {
                                formatTime = hour + Language().hour;
                            }
                        }
                    }
                    return formatTime;
                } catch (e) {
                    return formatTime;
                }
            }
        }
        return formatTime;
    }

    /**
     *
     * @param status
     * @param roomId
     * @returns {*}
     */
    gameStatusView(status, roomId, userInfoStatus) {
        let gameStatus = null;
        let gameStatusText = '';
        switch (status) {
            case 1:
                gameStatusText = Language().on_line;
                break;
            case 2:
                gameStatusText = Language().game_prepare + ' >';
                break;
            case 3:
                gameStatusText = Language().game_start + ' >';
                break;
            case 0:
                gameStatusText = that._onDateFormat(userInfoStatus);
                break;

        }
        //准备或是游戏中都进入房间，服务器判断是游戏还是观战
        if (status == 2 || status == 3) {
            //准备状态
            gameStatus = (
                <TouchableOpacity onPress={() => that.enterRoom(roomId)}
                                  activeOpacity={0.6}
                                  style={{backgroundColor: Color.transparent}}>
                    <View style={{flex: 1, justifyContent: 'center', width: 80, alignItems: 'flex-start'}}>
                        {that.roomStatusView(roomId)}
                        <View flexDirection='row'
                              style={{
                                  backgroundColor: '#f79c21',
                                  borderRadius: 2,
                                  marginTop: 3,
                                  marginLeft: 15,
                                  paddingLeft: 5,
                                  paddingRight: 5,
                                  paddingTop: 2,
                                  paddingBottom: 2
                              }}>
                            <Text
                                style={{
                                    color: Color.colorWhite,
                                    fontSize: 11,
                                    textAlign: 'center',
                                    alignSelf: 'center'
                                }}>{gameStatusText}</Text>
                        </View>
                    </View>
                </TouchableOpacity>)
        } else {
            gameStatus = (<View style={{flex: 1, justifyContent: 'center', width: 80, alignItems: 'flex-start'}}>
                {(status == 0 || status == 1) ? null : that.roomStatusView(roomId)}
                <Text
                    style={{
                        color: Color.game_status_color,
                        fontSize: 11,
                        textAlign: 'center',
                        alignSelf: 'center',
                        marginTop: 3,
                        marginLeft: -5
                    }}>{gameStatusText}</Text>
            </View>);
        }
        return gameStatus;
    }

    /**
     *
     * @param roomID
     */
    enterRoom(roomID) {
        this.state.roomID = roomID;
        let hostLocation = '';
        if (this.props.hostLocation) {
            hostLocation = this.props.hostLocation;
        }
        let url = apiDefines.SERVER_GET + this.state.roomID + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id + apiDefines.PARAMETER_LOCATION + hostLocation;
        that.onShowProgressDialog();
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                Util.get(url, (code, message, data) => {
                    that.onHideProgressDialog();
                    if (code == 1000) {
                        if (data) {
                            that.state.game_type = data.level;
                            if (data.password_needed) {
                                that.setState({
                                    showModalDialog: true
                                })
                            } else {
                                //直接进入
                                that.rnToNativeMessage();
                            }
                        }
                    } else {
                        Toast.show(message);
                    }
                }, (error) => {
                    that.onHideProgressDialog();
                    Toast.show(Language().not_network);
                });
            } else {
                that.onHideProgressDialog();
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 调用原生进入游戏
     */
    rnToNativeMessage() {
        let params = {
            room_id: this.state.roomID,
            room_password: this.state.roomPassword,
            room_type: this.state.game_type
        }
        let data = {
            action: 'enterRoom',
            params: params,
            type: 'RN_Native',
        }
        rnRoNativeUtils.sendMessageToNative(JSON.stringify(data));
        if (this.state.showModalDialog) {
            this.onCloseModal();
        }
    }

    roomStatusView(roomId) {
        return roomId ? (<View flexDirection='row' style={{justifyContent: 'center'}}>
            <Image source={gameRoomBg} style={styles.gameStatusIcon}/>
            <Text
                style={{
                    color: Color.game_status_color,
                    fontSize: 13,
                    textAlign: 'center',
                    alignSelf: 'center',
                    marginLeft: 5
                }}>{roomId}</Text>
        </View>) : null
    }

    /**
     * 侧滑删除
     * @param item
     */
    deleteRow(item, rowId, secId, rowMap) {
        try {
            rowMap[`${secId}${rowId}`].closeRow();
        } catch (e) {

        }
        if (item && item.id) {
            that.state.userId = item.id;
            that.state.deleteIndex = rowId;
            that.onShowDeleteContactDialog(item.name);
        }
    }

    /**
     * 刷新
     * @private
     */
    _onRefresh = () => {
        this.props.isRefreshing = true;
        //判断是否是刷新用户信息／或知识更改状态
        this.checkFriendVersion();
        InteractionManager.runAfterInteractions(() => {
            //刷新好友
            //刷新好友请求
            this.props.fetchRequestPageList();
        });
    };

    /**
     * 刷新状态
     */
    updateGameStatus() {
        console.log('更新状态')
        RealmManager.updateGameStatus((success) => {
            //刷新状态
            InteractionManager.runAfterInteractions(() => {
                this.props.updateGameStatus();
            });
        });
    }

    /**
     * 同步好友信息
     */
    synchronizationFriendList() {
        console.log('同步数据 好友')
        try {
            let url = apiDefines.GET_FRIEND_PAGE_LIST;
            Util.get(url, (code, message, data) => {
                if (code == 1000) {
                    if (data && data.total_page) {
                        RealmManager.deleteFriend((success) => {
                            let handleCount = 0;
                            for (let i = 0; i < data.total_page; i++) {
                                RealmManager.synchronizationFriendList(false, i, (request) => {
                                    handleCount++;
                                    if (handleCount == data.total_page) {
                                        InteractionManager.runAfterInteractions(() => {
                                            this.props.synchronizationFriendList(data.total_page);
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            }, (failed) => {
            });
        } catch (e) {
            console.log('synchronizationFriendList error' + e);
        }

    }

    /**
     * 检查好友列表版本
     */
    checkFriendVersion() {
        let url = apiDefines.FRIEND_VERSION;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                if (data) {
                    if (this.props.friendVersion < data.version) {
                        this.props.updateFriendVersion(data.version);
                        this.synchronizationFriendList();
                    } else {
                        this.updateGameStatus();
                    }
                } else {
                    this.updateGameStatus();
                }
            } else {
                this.updateGameStatus();
            }
        }, (failed) => {
            this.updateGameStatus();
        });
    }

    /**
     * load more
     * @private
     */
    _onEndReach = () => {
        //如果网络异常情况下处理
        let data = this.props.data;
        if (data) {
            if (data.page < data.total_page) {
                this.props.getFriendList(data.page + 1);
            } else {
                this.props.getFriendList(data.page);
            }
        }
    }

    onShowLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.show();
        }
    }

    onHideLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.hide();
        }
    }

    onShowDeleteContactDialog(name) {
        if (deleteContactDialog) {
            if (name) {
                deleteContactDialog.show({title: name});
            } else {
                deleteContactDialog.show();
            }
        }
    }

    onHideDeleteContactDialog() {
        if (deleteContactDialog) {
            deleteContactDialog.hide();
        }
    }

    /**
     * 执行删除
     */
    onDeleteContact() {
        try {
            netWorkTool.checkNetworkState((isConnected) => {
                this.onHideDeleteContactDialog();
                if (isConnected) {
                    this.onShowProgressDialog();
                    InteractionManager.runAfterInteractions(() => {
                        let url = apiDefines.DELETE_FRIEND + this.state.userId;
                        Util.get(url, (code, message, data) => {
                            this.onHideProgressDialog();
                            if (code == 1000) {
                                //this.props.deleteFriend(this.state.userId, this.state.deleteIndex);
                            }
                            Toast.show(message);
                        }, (failed) => {
                            this.onHideProgressDialog();
                            Toast.show(Language().not_network);
                        });
                    });
                } else {
                    this.onHideProgressDialog();
                    Toast.show(Language().not_network);
                }
            });
            //统计删除消息事件
            rnRoNativeUtils.onStatistics(Constant.DELETE_FRIEND);
        } catch (err) {

        }
    }

    /**
     * 取消删除
     */
    onCancelDelete() {
        this.onHideDeleteContactDialog();
    }

    /**
     * 加载更多
     * @private
     */
    renderFooter() {
        return friendList.length < 9 ? null : (<LoadMoreFooter isNoMore={that.props.isNoMore}/>);
    }

    /**
     * 头部
     */
    renderHeader() {
        return (
            <View>
                <View style={styles.rowSeparator}></View>
                <TouchableOpacity style={{height: 45, backgroundColor: (Color.transparent), justifyContent: 'center'}}
                                  activeOpacity={0.6}
                                  onPress={that.openNewFriend.bind(this)}>
                    <View flexDirection='row' style={{alignItems: 'center'}}>
                        <Image source={newFriendIcon} style={styles.leftIcon}/>
                        <Text
                            style={{
                                color: '#6932c3',
                                fontSize: 13,
                                padding: 5,
                                alignSelf: 'center'
                            }}>{Language().new_friend}</Text>
                        {that._newFriendImage()}
                        <Right>
                            <View flexDirection='row'>
                                {that._onBadgeView()}
                                <Image source={arrows}
                                       style={styles.bgBack}></Image>
                            </View>
                        </Right>
                    </View>
                </TouchableOpacity>
                <View style={styles.rowSeparator}></View>
                {false ?
                    <TouchableOpacity
                        style={{height: 45, backgroundColor: (Color.transparent), justifyContent: 'center'}}
                        activeOpacity={0.6}
                        onPress={that.openVoiceRoom.bind(this)}>
                        <View flexDirection='row' style={{alignItems: 'center'}}>
                            <Image source={recreationIcon} style={styles.leftIcon}/>
                            <Text
                                style={{
                                    color: '#6d21b3',
                                    fontSize: 14,
                                    padding: 5,
                                    alignSelf: 'center',
                                    fontWeight: 'bold'
                                }}>{Language().play_recreation}</Text>
                            <Right>
                                <View flexDirection='row'>
                                    <Image source={arrows}
                                           style={styles.bgBack}></Image>
                                </View>
                            </Right>
                        </View>
                    </TouchableOpacity> : null}

                <View style={styles.rowSeparator}></View>
                <View style={{height: 5, backgroundColor: '#F7F7F7', justifyContent: 'center'}}/>
                <View style={styles.rowSeparator}/>
            </View>
        )
    }

    /**
     *
     * @param height
     * @returns {*}
     * @private
     */
    _defaultView() {
        return friendList.length == 0 ? (<View
            style={{
                justifyContent: 'center',
                backgroundColor: Color.colorWhite,
                position: 'absolute',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0
            }}>
            <Image style={styles.defaultBg} source={defaultContactBg}/>
            <Text
                style={{
                    alignSelf: 'center',
                    padding: 5,
                    marginTop: 15,
                    color: '#9B9B9B',
                    fontSize: 13
                }}>{Language().title_without_friend}</Text>
            <Text
                style={{
                    alignSelf: 'center',
                    padding: 5,
                    color: '#9B9B9B',
                    fontSize: 13
                }}>{Language().content_without_friend}</Text>
        </View>) : null;
    }

    /**
     *
     * @returns {*}
     * @private
     */
    _onBadgeView() {
        let count = 0;
        let requestData = this.props.fetchRequestData;
        if (requestData && requestData.total_user > 0) {
            count = requestData.total_user;
            if (count > 99) {
                count = '99+';
            }
        }
        return count == 0 ? (null) : (
            <Badge minWidth={5} minHeight={5} textStyle={{fontSize: 9, paddingVertical: 1}} extraPaddingHorizontal={4}
                   style={{
                       width: 6,
                       height: 6,
                       marginRight: 8,
                       backgroundColor: Color.badge_color,
                       alignSelf: 'center'
                   }}>{count}</Badge>)
    }

    /**
     *
     * @returns {*}
     * @private
     */
    _newFriendImage() {
        return friendRequest.length == 0 ? null : (
            <ListView dataSource={this.state.friendRequestDataSource.cloneWithRows(friendRequest)}
                      renderRow={this.friendRequestRenderRow}
                      removeClippedSubviews={true}
                      contentContainerStyle={styles.listViewStyle}/>);
    }

    /**
     *
     * @param data
     * @param sectionId
     * @param rowId
     * @returns {*}
     */
    friendRequestRenderRow(data, sectionId, rowId) {
        return rowId > 4 ? null : (<View style={styles.cellBackStyle}><CachedImage style={styles.newFriendIcon}
                                                                                   defaultSource={require('../../../resources/imgs/ic_default_user.png')}
                                                                                   source={(data.image == null || data.image == '') ? (require('../../../resources/imgs/ic_default_user.png')) : ({uri: data.image})}/></View>)
    }

    /**
     *
     */
    openNewFriend() {
        that.props.openPage(SCENES.SCENE_NEW_FRIEND, 'global');
    }

    /**
     *
     */
    openVoiceRoom() {
        that.props.openPage(SCENES.SCENE_VOICE_ROOM, 'global');
    }

    /**
     * 返回去登录
     */
    login() {
        const routes = this.props.navigation.routes;
        if (routes.length > 1) {
            for (var i = 0; i < routes.length; i++) {
                this.props.finishPage('global');
            }
        }
        if (this.props.userInfo != null && this.props.userInfo.id != null) {
            let userId = this.props.userInfo.id;
            rnRoNativeUtils.closeLeanCloud(userId);
        }
        AsyncStorageTool.removeUserInfo();
        try {
            this.props.openPage(SCENES.SCENE_SPLASH, 'global');
        } catch (e) {
        }
        AsyncStorageTool.removeSignTourist();
        this.props.removeChatData();
        this.props.updateUserInfo(null);
        this.props.updateTourist(false);
        this.props.clearFriendList();
        this.props.clearMessageList();
        this.props.removeFriendStatus();
        this.onHideLoginDialog();

    }

    /**
     * 检测token是否失效
     */
    checkToken() {
        if (this.props.code == 1004) {
            //token 失效
            try {
                this.onShowLoginDialog();
            } catch (error) {
                console.log('token error show:' + error);
            }
        }
    }

    /**
     * 查看好友详情
     * @param item
     */
    actionContact(item, rowId) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                if (item && item.id) {
                    that.state.userId = item.id;
                    that.state.deleteIndex = rowId;
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                    return;
                }
                if (Platform.OS === 'ios') {
                    var main = NativeModules.MainViewController;
                    let sex = '2';
                    if (item.sex == 1) {
                        sex = '1';
                    }
                    let image = item.image;
                    if (image == null) {
                        image = '';
                    }
                    main.didSelectUser(item.id, 'detail', sex, image, item.name, '');
                } else {
                    NativeModules.NativeJSModule.rnStartPersonalInfoActivtiy(PersonalInfoActivity, item.id, this.props.token, false);
                }
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    _checkShowReactAudio() {
        if (this.props.configData) {
            let needShowAudio = this.props.configData.audio_room_enter;
            if (typeof needShowAudio == 'undefined') {
                return true;
            } else {
                return needShowAudio;
            }
        } else {
            return true;
        }
    }

    /**
     * 关闭输入密码
     */
    onCloseModal() {
        this.setState({
            showModalDialog: false,
            showModelProgress: false,
            roomPassword: '',
            roomID: '',
            game_type: '',
        });
    }

    /**
     *
     */
    onConfirmModal() {
        if (!this.state.showModelProgress) {
            let {roomPassword} = this.state;
            if (!roomPassword.length) {
                Toast.show(Language().placeholder_password)
                return;
            }
            that.rnToNativeMessage();

        }
    }

    showProgress() {
        if (!that.state.showModelProgress) {
            that.setState({
                showModelProgress: true
            });
        }
    }

    /**
     * 隐藏
     */
    hideProgress() {
        if (that.state.showModelProgress) {
            that.setState({
                showModelProgress: false
            });
        }
    }
}

const
    styles = StyleSheet.create({
        headThumbnail: {
            width: 50,
            height: 50,
            borderRadius: 25,
            alignSelf: 'center',
        },
        nameTextStyle: {
            color: '#313035',
        },
        messageTextStyle: {
            color: '#9B9B9B'
        },
        gender: {
            alignSelf: 'center',
            width: 15,
            height: 15,
            position: 'absolute',
            left: 20,
            top: 10,
            bottom: 0,
            right: 0,
        },
        defaultBg: {
            alignSelf: 'center',
            width: ScreenWidth / 2,
            height: ScreenWidth / 2,
            resizeMode: 'contain',
        },
        rowImage: {
            position: 'absolute',
            left: 0,
            top: 0,
            bottom: 0,
            right: 0,
            alignSelf: 'center',
            justifyContent: 'center'
        },
        rowSeparator: {
            height: StyleSheet.hairlineWidth,
            backgroundColor: Color.rowSeparator_line_color,
        },
        rowBack: {
            alignItems: 'center',
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingLeft: 15,
        },
        backRightBtn: {
            alignItems: 'center',
            bottom: 0,
            justifyContent: 'center',
            position: 'absolute',
            top: 0,
            width: 75
        },
        backRightBtnRight: {
            backgroundColor: Color.delete_bg_color,
            right: 0
        },
        bgBack: {
            width: 15,
            height: 15,
            resizeMode: 'contain',
            padding: 5,
            marginRight: 10
        }, cellBackStyle: {
            width: cellWH,
            height: cellWH,
            marginLeft: 5,
            alignSelf: 'center',
            justifyContent: 'center',
            borderColor: '#afafaf',
            borderWidth: StyleSheet.hairlineWidth,
            borderRadius: 15,
        },
        listViewStyle: {
            flexDirection: 'row',
            marginLeft: 3,
            width: ScreenWidth / 5 * 3,
        }, newFriendIcon: {
            width: 28,
            height: 28,
            borderRadius: 15,
            alignSelf: 'center'
        },
        gameStatusIcon: {
            width: 12,
            height: 12,
            resizeMode: 'contain',
            alignSelf: 'center'
        },
        leftIcon: {
            width: 18,
            height: 18,
            resizeMode: 'contain',
            marginLeft: 20
        },
        modelTitleIcon: {
            width: 45,
            height: 40,
            resizeMode: 'contain',
            alignSelf: 'center'
        }, actionButton: {
            backgroundColor: '#904fc1', justifyContent: 'center', height: 40, flex: 1
        }, dialogContainer: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
        }, roomNumber: {
            color: '#666666', fontSize: 12, alignSelf: 'center', marginBottom: 8
        },
    });
const
    mapDispatchToProps = dispatch => ({
            openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
            fetch: () => dispatch(fetchFriendList()),
            fetchPageList: () => dispatch(fetchPageFriendList()),
            deleteFriend: (friendId, index) => dispatch(deleteFriend(friendId, index)),
            fetchMoreList: (page) => dispatch(fetchMoreFriend(page)),
            fetchRefreshList: () => dispatch(refreshPageFriendList()),
            finishPage: (key) => dispatch(popRoute(key)),
            removeChatData: () => dispatch(removeChat()),
            updateUserInfo: (data) => dispatch((userInfo(data))),
            updateTourist: (isTourist) => dispatch((updateTourist(isTourist))),
            clearMessageList: () => dispatch(clearMessageList()),
            removeFriendStatus: () => dispatch(removeFriendStatus()),
            clearFriendList: () => dispatch(clearFriendList()),
            fetchRequestPageList: () => dispatch(fetchPageFriendRequestList()),
            getFriendList: (page) => dispatch(getFriendList(page)),
            updateGameStatus: () => dispatch(updateGameStatus()),
            updateFriendVersion: (version) => dispatch(updateFriendVersion(version)),
            synchronizationFriendList: (pages) => dispatch(synchronizationFriendList(pages)),
        }
    );
const
    mapStateToProps = state => ({
        code: state.friendReducer.code,
        message: state.friendReducer.message,
        data: state.friendReducer.data,
        token: state.userInfoReducer.token,
        navigation: state.cardNavigation,
        userInfo: state.userInfoReducer.data,
        isRefreshing: state.friendReducer.isRefreshing,
        isNoMore: state.friendReducer.isNoMore,
        fetchRequestData: state.messageReducer.data,
        updateTime: state.friendReducer.updateTime,
        configData: state.checkRegisterReducer.data,
        friendVersion: state.configReducer.friendVersion,
        hostLocation: state.configReducer.location
    });
export default connect(mapStateToProps, mapDispatchToProps)(Contact);