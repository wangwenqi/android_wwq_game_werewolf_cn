/**
 * Created by wangxu on 2017/3/2.
 */
'use strict';
import React, {Component} from 'react';
import Language from '../../../resources/language';
import * as SCENES from '../../../support/actions/scene';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Container,
    Header,
    Title,
    Button,
    Left,
    Text,
    Right,
    Body,
    Icon,
    Content,
    Footer,
    FooterTab,
    Badge,
    Item,
    ListItem,
    Thumbnail,
    Input
} from 'native-base';
const {
    pushRoute,
    popRoute,
} = actions;
class MessageDetail extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        navigator: React.PropTypes.shape({}),
        openPage: React.PropTypes.func,
        finishPage: React.PropTypes.func,
    }

    onFinishPage() {
        this.props.finishPage('global');
    }

    onOpenContactDetail() {
        try {
            this.props.openPage(SCENES.SCENE_CONTACT_DETAIL, 'global');
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        return (
            <Container>
                {/*<Header>*/}
                {/*<Left>*/}
                {/*<Button transparent onPress={this.onFinishPage.bind(this)}>*/}
                {/*<Icon name={'arrow-back'}/>*/}
                {/*</Button>*/}
                {/*</Left>*/}
                {/*<Body>*/}
                {/*<Title>name</Title>*/}
                {/*</Body>*/}
                {/*<Right>*/}
                {/*<Button transparent onPress={this.onOpenContactDetail.bind(this)}>*/}
                {/*<Icon name='home'/>*/}
                {/*</Button>*/}
                {/*</Right>*/}
                {/*</Header>*/}
                <Content>
                    <ListItem thumbnail>
                        <Left>
                            <Thumbnail square size={60} source={require('../../../resources/imgs/ic_default_user.png')}/>
                        </Left>
                        <Body>
                        <Text>One</Text>
                        <Text note>Its time to ...time to time to</Text>
                        </Body>
                        <Right>
                            <Button small>
                                <Text>添加</Text>
                            </Button>
                        </Right>
                    </ListItem>
                </Content>
                <Footer style={{backgroundColor:'#dbdada'}}>
                    <FooterTab style={{backgroundColor:'#dbdada',flex:1}}>
                        <Left>
                            <Button transparent>
                                <Icon name='microphone'/>
                            </Button>
                        </Left>
                        <Item style={{flex:3}}>
                            <Input placeholder='请输入消息' style={{font: 8,padding: 10,width:30}}/>
                        </Item>
                        <Right style={{marginRight:10}}>
                            <Button small>
                                <Text>{Language().send}</Text>
                            </Button>
                        </Right>
                    </FooterTab>
                </Footer>
            < / Container >
        );
    }
}
const mapDispatchToProps = dispatch => ({
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        finishPage: (key) => dispatch(popRoute(key))
    }
);
const mapStateToProps = state => ({
    navigation: state.cardNavigation
});
export default connect(mapStateToProps, mapDispatchToProps,)(MessageDetail);