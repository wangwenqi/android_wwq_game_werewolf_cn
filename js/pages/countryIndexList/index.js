/**
 * Created by wangxu on 2017/5/16.
 */
'use strict';
import React, {Component}from 'react';
import  {
    StyleSheet,
    View,
    WebView,
    Text,
    Platform,
    Alert,
    TouchableOpacity,
    ListView,
    Dimensions,
    ScrollView,
} from 'react-native';
import CountryList from '../../../resources/data/localCountryList';
import Toast from '../../../support/common/Toast';
import Color from '../../../resources/themColor';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
const SECTIONHEIGHT = 30;
var totalheight = [];//每个字母对应的国家和字母的总高度
const {width, height} = Dimensions.get('window');
const ROWHEIGHT = 40
const {
    popRoute,
} = actions;

var that;
class CityIndexListView extends Component {

    constructor(props) {
        super(props);


        var getSectionData = (dataBlob, sectionID) => {
            return sectionID;
        };
        var getRowData = (dataBlob, sectionID, rowID) => {
            return dataBlob[sectionID][rowID];
        };

        this.state = {
            dataSource: new ListView.DataSource({
                getRowData: getRowData,
                getSectionHeaderData: getSectionData,
                rowHasChanged: (row1, row2) => row1 !== row2,
                sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
            }),
            letters: [],
        };

        that = this;
    }

    _getSortLetters(dataList) {
        let list = [];

        for (let j = 0; j < dataList.length; j++) {
            let sortLetters = dataList[j].sortLetters.toUpperCase();

            let exist = false;
            for (let xx = 0; xx < list.length; xx++) {
                if (list[xx] === sortLetters) {
                    exist = true;
                }
                if (exist) {
                    break;
                }
            }
            if (!exist) {
                list.push(sortLetters);
            }
        }


        return list;
    }

    componentDidMount() {
        this.initCountry('');
    }

    initCountry(countryName) {
        let ALL_CITY_LIST=new Array();
        CountryList.getLocalCountry(list => {
            ALL_CITY_LIST = list;
        });
        let letterList = this._getSortLetters(ALL_CITY_LIST);

        let dataBlob = {};

        ALL_CITY_LIST.map(cityJson => {
            let key = cityJson.sortLetters.toUpperCase();
            let country = cityJson.n.toUpperCase();
            if (countryName == '') {
                if (dataBlob[key]) {
                    let subList = dataBlob[key];
                    subList.push(cityJson);
                } else {
                    let subList = [];
                    subList.push(cityJson);
                    dataBlob[key] = subList;
                }
            } else {
                if (country.indexOf(countryName) > 0) {
                    if (dataBlob[key]) {
                        let subList = dataBlob[key];
                        subList.push(cityJson);
                    } else {
                        let subList = [];
                        subList.push(cityJson);
                        dataBlob[key] = subList;
                    }
                }
            }
        });
        let sectionIDs = Object.keys(dataBlob);
        let rowIDs = sectionIDs.map(sectionID => {
            let thisRow = [];
            let count = dataBlob[sectionID].length;
            for (let ii = 0; ii < count; ii++) {
                thisRow.push(ii);
            }

            var eachheight = SECTIONHEIGHT + ROWHEIGHT * thisRow.length;
            totalheight.push(eachheight);

            return thisRow;
        });


        this.setState({
            letters: sectionIDs,
            dataSource: this.state.dataSource.cloneWithRowsAndSections(dataBlob, sectionIDs, rowIDs)
        });
    }

    _cityNameClick(cityJson) {
        this.onFinishPage();
    }

    _scrollTo(index, letter) {
        Toast.hide();
        let position = 0;
        for (let i = 0; i < index; i++) {
            position += totalheight[i]
        }
        this._listView.scrollTo({
            y: position
        });
        Toast.show(letter);
    }

    _renderRightLetters(letter, index) {
        return (
            <TouchableOpacity
                key={'letter_idx_' + index}
                activeOpacity={0.6}
                onPress={()=> {
                    this._scrollTo(index, letter)
                }}>
                <View style={styles.letter}>
                    <Text style={styles.letterText}>{letter}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    _renderListRow(cityJson, rowId) {
        return (
            <TouchableOpacity
                key={'list_item_' + cityJson.c}
                style={styles.rowView}
                onPress={()=> {
                    that._cityNameClick(cityJson)
                }}>
                <View style={styles.rowdata}>
                    <Text style={styles.rowdatatext}>{cityJson.n}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    _renderListSectionHeader(sectionData, sectionID) {
        return (
            <View style={  styles.sectionView }>
                <Text style={styles.sectionText}>
                    {sectionData}
                </Text>
            </View>
        );
    }

    onFinishPage() {
        this.props.finishPage('global');
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.listContainner}>
                    <ListView
                        ref={listView => this._listView = listView}
                        contentContainerStyle={styles.contentContainer}
                        dataSource={this.state.dataSource}
                        renderRow={this._renderListRow}
                        renderSectionHeader={this._renderListSectionHeader}
                        enableEmptySections={true}
                        initialListSize={500}/>
                    <View style={styles.letters}>
                        {this.state.letters.map((letter, index) => this._renderRightLetters(letter, index))}
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#F4F4F4',
    },
    listContainner: {
        height: Dimensions.get('window').height,
        marginBottom: 10
    },
    contentContainer: {
        flexDirection: 'row',
        width: width,
        backgroundColor: 'white',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
    },
    letters: {
        position: 'absolute',
        height: height,
        top: 0,
        bottom: 0,
        right: 10,
        backgroundColor: 'transparent',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    letter: {
        height: height * 3.3 / 100,
        width: width * 3 / 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    letterText: {
        textAlign: 'center',
        fontSize: height * 1.2 / 50,
        color: (Color.countryLetterColor),
        padding: 5
    },
    sectionView: {
        paddingTop: 5,
        paddingBottom: 5,
        height: 30,
        paddingLeft: 10,
        width: width,
        backgroundColor: (Color.countryListColor),
    },
    sectionText: {
        color: (Color.countryLetterColor),
        fontWeight: 'bold',
        paddingLeft: 5
    },
    rowView: {
        height: ROWHEIGHT,
        paddingLeft: 10,
        paddingRight: 10,
        borderBottomColor: '#F4F4F4',
        borderBottomWidth: 0.5,
    },
    rowdata: {
        paddingTop: 10,
        paddingBottom: 2,
    },
    rowdatatext: {
        color: 'black',
        width: width,
        paddingLeft: 15
    },
});
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
    }
);
const mapStateToProps = state => ({
    registerData: state.registerReducer.data,
    countryName: state.countryListReducer.countryName,
    needUpdate: state.countryListReducer.needUpdate,
});
export default connect(mapStateToProps, mapDispatchToProps,)(CityIndexListView);