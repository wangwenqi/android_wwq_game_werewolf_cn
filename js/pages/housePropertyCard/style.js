/**
 * Created by wangxu on 2017/12/8.
 */
'use strict';
import {StyleSheet, Platform, Dimensions} from 'react-native';
import Color from '../../../resources/themColor';
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.colorWhite
    },
    headerView: {
        backgroundColor: Color.headColor,
        height: (Platform.OS === 'ios') ? 60 : 50,
        paddingTop: (Platform.OS === 'ios') ? 14 : 0,
        justifyContent: 'center'
    },
    touchableOpacity: {
        justifyContent: 'center',
        backgroundColor: Color.transparent,
    },
    mainTitle: {
        fontSize: 18,
        color: Color.colorWhite, backgroundColor: Color.transparent, textAlign: 'center', alignSelf: 'center',
        flex: 1
    },
    allowView: {
        width: 13,
        height: 13,
        resizeMode: 'contain',
        alignSelf: 'flex-end',
        margin: 10,
        marginRight: 0
    },
    baseView: {
        backgroundColor: Color.colorWhite
    },
    bgImage: {
        position: 'absolute',
        left: 0,
        top: (Platform.OS === 'ios') ? 60 : 50,
        bottom: 0,
        right: 0,
        width: null,
        height: null,
        resizeMode: 'stretch'
    },
    bgImageCard: {
        width: ScreenWidth * 0.785,
        height: ScreenHeight * 0.65,
        alignSelf: 'center',
        resizeMode: 'stretch'
    },
    backStyle: {
        width: 15,
        height: 15,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    buttonText: {
        fontSize: 16,
        color: '#1c1c25',
        backgroundColor: Color.transparent,
        textAlign: 'center',
        alignSelf: 'center'
    },
    house_seal_text: {
        fontSize: 9,
        color: '#000000',
        alignSelf: 'center',
        fontWeight: '800',
        backgroundColor: Color.transparent
    }
});
module.exports = Styles;