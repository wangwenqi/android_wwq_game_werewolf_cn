/**
 * Created by wangxu on 2017/12/8.
 * 推荐更多有趣的人
 */
import React, {Component, Navigator} from 'react';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    StyleSheet,
    View,
    AsyncStorage,
    Platform,
    TouchableOpacity,
    Dimensions,
    InteractionManager,
    Text
} from 'react-native';
import Moment from 'moment';
//
import  * as styles from './style';
import  * as Constant from '../../../support/common/constant';
import Language from '../../../resources/language';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import CachedImage from '../../../support/common/CachedImage';
import LoadImageView from '../../../support/common/LoadImageView';
import ImageManager from '../../../resources/imageManager';
import netWorkTool from '../../../support/common/netWorkTool';
//
const {
    popRoute,
} = actions;
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
//
let selfThis;
class HousePropertyCard extends Component {
    static propTypes = {
        ownedData: React.PropTypes.object,
    }

    constructor(props) {
        super(props);
        selfThis = this;
        Moment.defineLocale('zh-cn', {
            longDateFormat: {
                LLL: 'YYYY年MM月D日HH:mm',
                LLLL: 'YYYY-MM-D HH:mm',
            },
        });
        this.state = {
            isFinishing: false
        };
    }

    componentDidMount() {
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    componentWillMount() {
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.finishTimer && clearTimeout(this.finishTimer);
    }

    handleMethod(isConnected) {

    }

    render() {
        return (<View style={styles.container}>
            <CachedImage
                source={ImageManager.bgHouseCard}
                style={styles.bgImage}/>
            <View flexDirection='row' style={styles.headerView}>
                <TouchableOpacity
                    style={[styles.touchableOpacity]}
                    activeOpacity={0.8}
                    onPress={this.finishPage.bind(this)}>
                    <View style={{width:50,height:50,justifyContent:'center'}}>
                        <CachedImage source={ImageManager.icBack} style={styles.backStyle}/>
                    </View>
                </TouchableOpacity>
                <Text style={styles.mainTitle}>{Language().my_house_card}</Text>
                <View style={{width:50}}/>
            </View>
            <View style={{justifyContent:'center',flex:1}}>
                <View style={{alignSelf:'center'}}>
                    <CachedImage
                        source={ImageManager.icHouseCard}
                        style={styles.bgImageCard}/>
                    {this.userHouseView()}
                </View>
                <TouchableOpacity
                    style={[styles.touchableOpacity,{borderRadius:20,height:35,width:ScreenWidth/2,backgroundColor:'#ffdb42',alignSelf:'center',marginTop:20,justifyContent:'center'}]}
                    activeOpacity={0.8}
                    onPress={this.shareHouseDeeds.bind(this)}>
                    <Text style={styles.buttonText}>{Language().show}</Text>
                </TouchableOpacity>
            </View>
        </View>);
    }

    /**
     *
     */
    userHouseView() {
        let ownedData = {};
        let time1 = '';
        let time2 = '';
        if (this.props.ownedData) {
            ownedData = this.props.ownedData;
            if (ownedData.apply_time) {
                time1 = Moment(ownedData.apply_time - 0).format('LLL');
                time2 = Moment(ownedData.apply_time - 0).format('LLLL');
            }
        }
        let houseCardContent = Language().house_card_content;
        houseCardContent = houseCardContent.replace('%1$s', time1);
        houseCardContent = houseCardContent.replace('%2$s', ownedData.name);
        houseCardContent = houseCardContent.replace('%3$s', ownedData.room_id);
        return ( <View style={{left:25,top:25,right:25,bottom:25,flex:1,position: 'absolute'}}>
            <View>
                <Text
                    style={{alignSelf:'center',color:'#191919',fontSize:18,padding:5}}>{Language().mini_game_app_name+Language().werewolf_house_card}</Text>
                <LoadImageView source={ownedData.image?{uri:ownedData.image}:ImageManager.defaultUser}
                               defaultSource={ImageManager.defaultUser}
                               style={{width:60,height:60,borderRadius:30,alignSelf:'center',margin:10,marginTop:15}}/>
                <View style={{alignSelf:'center'}}>
                    <Text style={{fontSize:11,color:'#191919'}}>
                        {Language().nickname}：
                        <Text numberOfLines={1}>{ownedData.name}</Text>
                    </Text>
                    <Text style={{fontSize:11,color:'#191919'}}>
                        {Language().room_number}
                        <Text>{ownedData.room_id}</Text>
                    </Text>
                    <Text style={{fontSize:11,color:'#191919'}}>
                        ID：
                        <Text>{ownedData.uid}</Text>
                    </Text>
                </View>
                <Text style={{fontSize:12,color:'#000000',marginTop:10,lineHeight:28,paddingLeft:3,paddingRight:3}}>
                    {houseCardContent}
                </Text>
                <Text
                    style={{fontSize:13,color:'#000000',marginTop:10,paddingLeft:25}}>{Language().hereby_certify}</Text>
            </View>
            <View style={{position: 'absolute',right:10,bottom:5,justifyContent:'center',width:100,height:80}}>
                <CachedImage
                    source={ImageManager.icWerewolfSeal}
                    style={{width:96,height:80,alignSelf:'center'}}/>
                <View style={{position: 'absolute',left:0,right:0,bottom:8,justifyContent:'center'}}>
                    <Text style={styles.house_seal_text}>{ownedData.room_id}{Language().house_owned}</Text>
                    <Text style={styles.house_seal_text}>{time2}</Text>
                </View>
            </View>
        </View>);
    }

    /**
     *分享房契
     */
    shareHouseDeeds() {
        let NativeData = {
            action: Constant.SHARE_HOUSE_DEEDS,
            params: this.props.ownedData,
            options: null
        }
        rnRoNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }

    /**
     *
     */
    finishPage() {
        if (!this.state.isFinishing) {
            this.props.finishPage('global');
            this.state.isFinishing = true;
        }
        this.finishTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }
}

const mapDispatchToProps = dispatch => ({
    finishPage: (key) => dispatch(popRoute(key)),
});
const mapStateToProps = state => ({
    ownedData: state.ownedReducer.data,
});
export default connect(mapStateToProps, mapDispatchToProps)(HousePropertyCard);