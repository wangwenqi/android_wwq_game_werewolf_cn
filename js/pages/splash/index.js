/**
 * Created by wangxu on 2017/3/1.
 * 应用欢迎页面
 * 功能：进入应用的过度以及提供登录入口
 */
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {actions} from 'react-native-navigation-redux-helpers';
import {
    AsyncStorage,
    Platform,
    Image,
    NativeModules,
    DeviceEventEmitter,
    StyleSheet,
    View,
    Dimensions,
    StatusBar,
    Animated, InteractionManager, TouchableOpacity, findNodeHandle
} from 'react-native';
import Language from '../../../resources/language'
import * as SCENES from '../../../support/actions/scene'
import {Container, Content, Text, Button, Body, Spinner} from 'native-base';
import {userInfo, updateTourist} from '../../../support/actions/userInfoActions';
import Toast from '../../../support/common/Toast';
import {location, updateEnterNoviceCount, setIsNewUser} from '../../../support/actions/configActions';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import netWorkTool from '../../../support/common/netWorkTool';
import RealmManager from '../../../support/common/realmManager';
import * as WeChat from 'react-native-wechat';
import * as QQAPI from 'react-native-qq';
import Util from '../../../support/common/utils';
import Color from '../../../resources/themColor'
import  * as apiDefines from '../../../support/common/gameApiDefines';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import SplashScreen from 'react-native-splash-screen';
import {selectedCountry, initCountry} from '../../../support/actions/countryListActions';
import * as Constant from '../../../support/common/constant';
import {getChatData} from '../../../support/actions/chatActions';
import {getFamilyChat} from '../../../support/actions/groupFamilyChatActions';
import {initFriendRealm} from '../../../support/actions/friendActions';
import CachedImage from '../../../support/common/CachedImage';
import {BlurView} from 'react-native-blur';
import {AccessToken, LoginManager} from 'react-native-fbsdk';
import CheckBox from '../../../support/common/CheckBox';

import Loading from '../../../support/common/Loading';
const LineLoginManager = require('react-native').NativeModules.RNLineLoginManager;

const bg_slash = require('../../../resources/imgs/bg_splash.png');
const bgLoginWeChat = require('../../../resources/imgs/icon_wechat_login.png');
const bgLoginQQ = require('../../../resources/imgs/icon_qq_login.png');
const bgLoginPhone = require('../../../resources/imgs/icon_phone_login.png');
const bgLoginFaceBook = require('../../../resources/imgs/icon_facebook_login.png');
const bgLoginLine = require('../../../resources/imgs/icon_line.png');
const bgArrows = require('../../../resources/imgs/login_icon_rightarrow.png');
const ScreenHeight = Dimensions.get('window').height;
const ScreenWidth = Dimensions.get('window').width;
const {
    pushRoute,
    popRoute,
} = actions;
const names = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17',];
const avatars = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
const loginIconWH = (ScreenWidth - 320 > 0) ? 45 : 35;
const loginIconMG = (ScreenWidth - 320 > 0) ? 5 : 0;
const loginViewW = 73;
let loginEnd = false;
let progressDialog;
class Splash extends Component {
    static propTypes = {
        navigator: React.PropTypes.shape({}),
        openHome: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        updateUserInfo: React.PropTypes.func,
        updateTourist: React.PropTypes.func,
        jumpToHome: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        selectCountry: React.PropTypes.func,
        initCountry: React.PropTypes.func,
        getChatData: React.PropTypes.func,
        getFamilyChat: React.PropTypes.func,
        configData: React.PropTypes.object,
        setLocation: React.PropTypes.func,
        updateEnterNoviceCount: React.PropTypes.func,
        setIsNewUser: React.PropTypes.func,
        initFriendRealm: React.PropTypes.func,
    }

    constructor() {
        super();
        this.state = {
            userName: '',
            userInfo: '',
            id: '',
            login: true,
            isWXAppInstalled: false,
            isQQAppInstalled: false,
            isFaceBookAppInstalled: false,
            blurType: 'dark',
            blurRadius: 11,
            viewRef: null,
            facebookToken: '',
            currentAlpha: 0.0,//标志位，记录当前value
            fadeAnim: new Animated.Value(0.0),
            useragreement: true

        }
    }

    /**
     * 页面的布局
     * @returns {XML}
     */
    imageLoaded() {
        InteractionManager.runAfterInteractions(() => {
            this.imageLoadTimer = setTimeout(() => {
                this.setState({viewRef: findNodeHandle(this.refs.backgroundImage)});
            }, 100);
        });
    }

    /**
     * 渲染页面
     * @returns {XML}
     */
    render() {
        return (
            <View style={styles.containerView}>
                <StatusBar backgroundColor='transparent' barStyle='light-content' hidden={true}/>
                <CachedImage
                    source={bg_slash}
                    style={styles.image}
                    ref={'backgroundImage'}
                    onLoadEnd={this.imageLoaded.bind(this)}/>

                { this.state.login ? null : this.renderBlurView() }
                <Loading ref="progressDialog" loadingTitle={Language().login_loading}/>

            </View>
        );
    }

    /**
     * 登录按钮view 以及毛玻璃效果
     * @returns {XML}
     */
    renderBlurView() {
        let facebookView = this._faceBookView();
        if (this.props.configData && !this.props.configData.facebook) {
            facebookView = null;
        }
        return (
            <View style={styles.containerView}>
                {this.state.viewRef && <BlurView
                    viewRef={this.state.viewRef}
                    style={styles.blurView}
                    blurRadius={this.state.blurRadius}
                    blurType={this.state.blurType}>

                </BlurView>}

                <Text style={{ fontSize: 16,textAlign: 'center',color: Color.colorWhite,
            padding: 15}}>
                    {Language().login_type_title}
                </Text>
                <View flexDirection='row' style={{marginTop:15,justifyContent:'center'}}>
                    {/*{facebookView}*/}
                    {this._weChatView()}
                    {this._qqView()}
                </View>
                <View flexDirection='row' style={{marginTop:15,justifyContent:'center'}}>
                    {this._touristView()}
                    {this._phoneView()}
                </View>
                {this.arrangementView()}
            </View>)
    }

    /**
     * 用户协议
     */
    readUserAgreement() {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                this.props.openHome(SCENES.SCENE_USER_AGREEMENT, 'global');
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     *
     */
    checkAgreement() {
        this.setState({
            useragreement: !this.state.useragreement
        });
    }

    /**
     *
     * @returns {XML}
     */
    arrangementView() {
        return (<View style={{position: 'absolute', left: 0,bottom: 20, right: 0}}>
            <View flexDirection='row' style={{justifyContent:'center',marginLeft:10,marginRight:10}}>
                <CheckBox checked={this.state.useragreement} onChange={this.checkAgreement.bind(this)}
                          label=''
                          checkboxStyle={{width:15,height:15,marginTop:2}}/>
                <TouchableOpacity onPress={this.readUserAgreement.bind(this)}
                                  activeOpacity={0.6} style={{marginLeft:2,justifyContent:'center'}}>
                    <View flexDirection='row' style={{justifyContent:'center',alignSelf:'center'}}>
                        <Text
                            style={{color:'#ffffff',fontSize:13,alignSelf:'center',padding:1}}>{Language().agree_and_read}</Text>
                        <Text
                            style={{color:'#eb6c63',fontSize:13,alignSelf:'center',padding:1}}>{'《' + Language().user_agreement + '》'}</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <Text
                style={{color:'#dcdcdc',fontSize:9,alignSelf:'center',paddingTop:2}}>{Language().explain_agreement}</Text>
        </View>);
    }

    /**
     * Facebook登录view
     * @returns {*}
     * @private
     */
    _faceBookView() {
        return this.state.isFaceBookAppInstalled ? (
                <TouchableOpacity style={{margin:loginIconMG,justifyContent:'center',width:loginViewW}}
                                  onPress={this.onLoginFacebook.bind(this)}>
                    <CachedImage source={bgLoginFaceBook}
                           style={{width: loginIconWH, height: loginIconWH, resizeMode: 'contain', alignSelf: 'center'}}/><Text
                    style={{fontSize: 14, textAlign: 'center', color:Color.colorWhite, paddingTop: 10}}>{Language().facebook}</Text></TouchableOpacity>) : null;
    }

    /**
     * Line登录view
     * @returns {*}
     * @private
     */
    _lineView() {
        return false ? (
                <TouchableOpacity style={{margin:loginIconMG,justifyContent:'center',width:loginViewW}}
                                  onPress={this.onLoginFromLine.bind(this)}>
                    <CachedImage source={bgLoginLine}
                           style={{width: loginIconWH, height: loginIconWH, resizeMode: 'contain', alignSelf: 'center'}}/><Text
                    style={{fontSize: 14, textAlign: 'center', color:Color.colorWhite, paddingTop: 10}}>{Language().line}</Text></TouchableOpacity>) : null;
    }

    /**
     * 测试 Facebook登录view
     * @returns {*}
     * @private
     */
    _testFaceBookView() {
        return (<TouchableOpacity
            style={{margin:loginIconMG,justifyContent:'center',width:ScreenWidth/3*2,height:40,backgroundColor:'#345995',alignSelf:'center'}}
            activeOpacity={0.6}
            onPress={this.onTestLoginFacebook.bind(this)}>
            <Text style={{fontSize: 14, textAlign: 'center', color:Color.colorWhite, paddingTop: 1,alignSelf:'center'}}>facebook login now</Text>
        </TouchableOpacity>);
    }

    onTestLoginFacebook() {
        let {facebookToken}=this.state;
        if (!facebookToken.length) {
            Toast('未进行授权，请先申请授权');
            return;
        }
        this.userLoginFacebook(facebookToken);
    }

    /**
     * QQ登录view
     * @returns {*}
     * @private
     */
    _qqView() {
        return this.state.isQQAppInstalled ? (
                <TouchableOpacity style={{margin:loginIconMG,justifyContent:'center',width:loginViewW}}
                                  onPress={this.onLoginFromQQ.bind(this)}>
                    <CachedImage source={bgLoginQQ}
                           style={{width: loginIconWH, height: loginIconWH, resizeMode: 'contain', alignSelf: 'center'}}/><Text
                    style={{fontSize: 14, textAlign: 'center', color:Color.colorWhite, paddingTop: 10}}>{Language().qq_login}</Text></TouchableOpacity>) : null;
    }

    /**
     * 微信 登录view
     * @returns {*}
     * @private
     */
    _weChatView() {
        return this.state.isWXAppInstalled ? (
                <TouchableOpacity style={{margin:loginIconMG,justifyContent:'center',width:loginViewW}}
                                  activeOpacity={0.6}
                                  onPress={this.onLoginFromWeChat.bind(this)}>
                    <CachedImage source={bgLoginWeChat}
                           style={{width: loginIconWH, height: loginIconWH, resizeMode: 'contain', alignSelf: 'center'}}/><Text
                    style={{fontSize: 14, textAlign: 'center', color:Color.colorWhite, paddingTop: 10}}>{Language().wx_login}</Text></TouchableOpacity>) : null;
    }

    /**
     * 手机号登录view
     * @returns {XML}
     * @private
     */
    _phoneView() {
        let linePBottom = (Platform.OS === 'ios') ? 1 : 0;
        return (<TouchableOpacity onPress={this.onLoginFromPhone.bind(this)}
                                  activeOpacity={0.6}
                                  style={{alignSelf:'center',backgroundColor:Color.transparent,margin:10,height:25,justifyContent:'center'}}>
            <Text
                style={{color:Color.colorWhite,alignSelf:'flex-end',padding:linePBottom,fontSize:13}}>{Language().phone_login}</Text>
            <View style={{backgroundColor:Color.colorWhite,height:0.5}}/>
        </TouchableOpacity>)
    }

    /**
     * 等多登录方式
     */
    _moreLoginView() {
        return (<TouchableOpacity onPress={this.onStartAnimation.bind(this)}
                                  activeOpacity={0.6}
                                  style={{alignSelf:'center',backgroundColor:Color.transparent,height:30,justifyContent:'center',marginTop:30,marginBottom:5,width:100,justifyContent:'center'}}>
            <View flexDirection='row' style={{justifyContent:'center'}}>
                <Text
                    style={{color:Color.colorWhite,alignSelf:'center',fontSize:13}}>{Language().more_login_type}</Text>
                <CachedImage source={bgArrows} style={styles.moreLoginIcon}/>
            </View>
        </TouchableOpacity>)
    }

    /**
     * 游客模式view
     * @returns {XML}
     * @private
     */
    _touristView() {
        let linePBottom = (Platform.OS === 'ios') ? 1 : 0;
        return (<TouchableOpacity onPress={this.onTouristLogin.bind(this)}
                                  activeOpacity={0.6}
                                  style={{alignSelf:'center',backgroundColor:Color.transparent,margin:10,height:25,justifyContent:'center'}}>
            <Text
                style={{color:Color.colorWhite,alignSelf:'flex-end',padding:linePBottom,fontSize:13}}>{Language().from_tourist_login}</Text>
            <View style={{backgroundColor:Color.colorWhite,height:0.5}}/>
        </TouchableOpacity>)
    }

    /**
     * 1.判断时候安装微信
     * 2.微信授权
     * 3.授权成功后登录进入服务器
     */
    onLoginFromWeChat() {
        if (this.state.useragreement) {
            try {
                netWorkTool.checkNetworkState((isConnected) => {
                    if (isConnected) {
                        this.wechatTimer = setTimeout(() => {
                            loginEnd = false;
                        }, 3000);
                        //检测是否安装微信
                        WeChat.isWXAppInstalled().then((resp) => {
                            if (resp) {
                                //微信登录
                                if (!loginEnd) {
                                    loginEnd = true;
                                    WeChat.sendAuthRequest('snsapi_userinfo', '123').then((resp) => {
                                        this.userLogin(resp.code);
                                        loginEnd = false;
                                    }, (error) => {
                                        loginEnd = false;
                                    });
                                } else {
                                    Toast.show(Language().login_loading);
                                }
                            } else {
                                Toast.show(Language().need_install_wx);
                            }
                        }, (error) => {
                        });
                    } else {
                        Toast.show(netWorkTool.NOT_NETWORK);
                    }
                });
            } catch (e) {
                console.log('login wx error:' + e);
            }
        } else {
            Toast.show(Language().agreement_prompt);
        }

        //统计
        rnRoNativeUtils.onStatistics(Constant.WE_CHAT_LOGIN);
    }

    /**
     * 手机号登录入口
     */
    onLoginFromPhone() {
        //this.onCloseMoreLogin();
        if (this.state.useragreement) {
            //进入手机登录界面
            this.props.openHome(SCENES.SCENE_LOGIN_OTHER, 'global');
        } else {
            Toast.show(Language().agreement_prompt);
        }

        //统计
        rnRoNativeUtils.onStatistics(Constant.PHONE_LOGIN);
    }

    /**
     *游客模式登录
     */
    onTouristLogin() {
        try {
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    //其他方式登录
                    let rand = Math.round(Math.random() * names.length - 1);
                    let token = {
                        access_token: apiDefines.DEFAULT_ACCESS_TOKEN
                    };
                    let game = {
                        level: 0,
                        experience: 0,
                        win: 0,
                        lose: 0,
                    };
                    let touristName = names[rand];
                    if (touristName == null) {
                        touristName = '1';
                    }
                    let randAvatar = Math.round(Math.random() * avatars.length - 1);
                    if (randAvatar < 0 || randAvatar > 9) {
                        randAvatar = 0
                    }
                    let data = {
                        name: Language().tourist + touristName,
                        id: new Date().getTime().toString(),
                        image: apiDefines.DEFAULT_AVATAR + avatars[randAvatar] + '.png',
                        token: token,
                        game: game,
                        sex: 1
                    };
                    let tourist = {
                        isTourist: true
                    }
                    this.state.userInfo = JSON.stringify(data);
                    this.onFinishPage();
                    this.props.updateUserInfo(data);
                    this.props.updateTourist(true);
                    this.props.openHome(SCENES.SCENE_APP_MAIN, 'global');
                    AsyncStorageTool.saveUserInfo(this.state.userInfo);
                    AsyncStorageTool.signTourist(JSON.stringify(tourist));
                    rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token);
                    rnRoNativeUtils.onSetLocation(data.lc);
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        } catch (e) {
            console.log('login tourist error:' + e);
        }
        //统计
        rnRoNativeUtils.onStatistics(Constant.GUEST_LOGIN);
    }

    /**
     * 1--QQ授权
     * 2--授权成功后登录服务器
     */
    onLoginFromQQ() {
        if (this.state.useragreement) {
            try {
                netWorkTool.checkNetworkState((isConnected) => {
                    if (isConnected) {
                        this.qqTimer = setTimeout(() => {
                            loginEnd = false;
                        }, 3000);

                        QQAPI.isQQInstalled().then((resp) => {
                            if (resp) {
                                //QQ登录
                                if (!loginEnd) {
                                    loginEnd = true;
                                    QQAPI.login('get_simple_userinfo').then((resp) => {
                                        loginEnd = false;
                                        this.qqLogin(resp.access_token, resp.expires_in, resp.oauth_consumer_key, resp.openid);
                                    }, (error) => {
                                        loginEnd = false;
                                    });
                                } else {
                                    Toast.show(Language().login_loading);
                                }

                            } else {
                                Toast.show(Language().need_install_qq);
                            }
                        });
                    } else {
                        Toast.show(netWorkTool.NOT_NETWORK);
                    }
                });
            } catch (e) {
                console.log('login qq error:' + e);
            }
        } else {
            Toast.show(Language().agreement_prompt);
        }

        //统计
        rnRoNativeUtils.onStatistics(Constant.QQ_LOGIN);
    }

    /**
     * 1--QQ授权后返回的数据
     * @param access_token
     * @param expires_in
     * @param oauth_consumer_key
     * @param openid
     */
    qqLogin(access_token, expires_in, oauth_consumer_key, openid) {
        try {
            this.onShowProgressDialog();
            let url = apiDefines.GET_USER_INFO_FROM_QQ + access_token + apiDefines.EXPIRES_IN + expires_in + apiDefines.OAUTH_CONSUMER_KEY + oauth_consumer_key + apiDefines.OPENID + openid;
            Util.get(url, (code, message, data) => {
                this.onHideProgressDialog();
                if (code == 1000) {
                    if (data) {
                        this.state.userInfo = JSON.stringify(data);
                        this.onFinishPage();
                        this.props.updateUserInfo(data);
                        this.props.openHome(SCENES.SCENE_APP_MAIN, 'global');
                        AsyncStorageTool.saveUserInfo(this.state.userInfo);
                        if (data != null) {
                            RealmManager.initRealmManager(data.id, (callback) => {
                                if (callback) {
                                    this.props.getChatData(data.id, callback);
                                    this.props.getFamilyChat(data.id, callback);
                                    this.props.initFriendRealm(callback);
                                }
                            })
                            rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token);
                            rnRoNativeUtils.onSetLocation(data.lc);
                            this.configLocation(data.lc);
                        }
                    }
                } else {
                    Toast.show(message);
                }
            }, (error) => {
                this.onHideProgressDialog();
                Toast.show(Language().not_network);
            });
        } catch (e) {
            console.log('qq login error:' + e);
        }
    }

    /**
     * 1--销毁当前页面
     */
    onFinishPage() {
        // this.onCloseMoreLogin();
        this.props.finishPage('global');
    }

    /**
     *1--微信验证通过后登录用户系统
     * @param code
     */
    userLogin(code) {
        try {
            this.onShowProgressDialog();
            let url = apiDefines.GET_USER_INFO + code;
            Util.get(url, (code, message, data) => {
                this.onHideProgressDialog();
                if (code == 1000) {
                    if (data) {
                        this.state.userInfo = JSON.stringify(data);
                        this.onFinishPage();
                        this.props.updateUserInfo(data);
                        this.props.openHome(SCENES.SCENE_APP_MAIN, 'global');
                        AsyncStorageTool.saveUserInfo(this.state.userInfo);
                        if (data != null) {
                            RealmManager.initRealmManager(data.id, (callback) => {
                                if (callback) {
                                    this.props.getChatData(data.id, callback);
                                    this.props.getFamilyChat(data.id, callback);
                                    this.props.initFriendRealm(callback);
                                }
                            });
                            rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token);
                            rnRoNativeUtils.onSetLocation(data.lc);
                            this.configLocation(data.lc);
                        }
                    }
                } else {
                    Toast.show(message);
                }
            }, (error) => {
                this.onHideProgressDialog();
                Toast.show(Language().not_network);
            });
        } catch (e) {
            console.log('wechat login error:' + e);
        }
    }

    /**
     * 1--检测是否是游客
     */
    updateTouristInfo() {
        AsyncStorageTool.getStorageData(AsyncStorageTool.TOURIST_DATA, (data) => {
            //console.log('本地存储游客：' + JSON.stringify(data));
            if (data != null && data.isTourist) {
                this.props.updateTourist(true);
            } else {
                this.props.updateTourist(false);
            }
        });
    }

    /**
     * 1--更新所选国家信息
     */
    updateSelectCountry() {
        AsyncStorageTool.getStorageData(AsyncStorageTool.COUNTRY_DATA, (countryData) => {
            //console.log('本地存储国家信息：' + JSON.stringify(countryData));
            if (countryData != null) {
                this.props.selectCountry(countryData);
            } else {
                this.props.initCountry();
            }
        });
    }

    /**
     * 1--申请line授权
     * 2--授权成功后登录服务器
     */
    onLoginFromLine() {
        try {
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    this.lineTimer = setTimeout(() => {
                        loginEnd = false;
                    }, 3000);
                    if (!loginEnd) {
                        loginEnd = true;
                        LineLoginManager.login()
                            .then((user) => {
                                Toast.show(JSON.stringify(user));
                                console.log(JSON.stringify(user));
                                loginEnd = false;
                                LineLoginManager.logout();
                            })
                            .catch((err) => {
                                loginEnd = false;
                                Toast.show(err.message);
                                console.log(JSON.stringify(err));
                            })
                    } else {
                        Toast.show(Language().login_loading);
                    }
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        } catch (e) {
            console.log('login line error:' + e);
        }
    }

    /**
     * 1--申请Facebook授权
     * 2--授权成功后登录服务器
     */
    onLoginFacebook() {
        if (this.state.useragreement) {
            try {
                netWorkTool.checkNetworkState((isConnected) => {
                    if (isConnected) {
                        // Facebook获取授权并请求token
                        LoginManager.logInWithReadPermissions(['public_profile']).then(
                            (result) => {
                                if (result.isCancelled) {
                                    console.log('facebokk login cancelled');
                                    //alert('facebokk login cancelled');
                                } else {
                                    AccessToken.getCurrentAccessToken().then(
                                        (data) => {
                                            //alert(JSON.stringify(data));
                                            //console.log('data:' + JSON.stringify(data));
                                            this.userLoginFacebook(data.accessToken);
                                            //this.state.facebookToken = data.accessToken;
                                            //Toast.show('Facebook 授权成功，可以进行登录');
                                            /**
                                             * 授权成功后注销下次登录校验
                                             */
                                            LoginManager.logOut();
                                        }
                                    )
                                }
                            },
                            (error) => {
                                alert('Login fail with error: ' + error);
                                //console.log('Login fail with error: ' + error);
                                Toast.show(Language().facebook_authorization_fail);
                            }
                        );
                    } else {
                        Toast.show(netWorkTool.NOT_NETWORK);
                    }
                });
            } catch (e) {
                console.log('login facebook error:' + e);
            }
        } else {
            Toast.show(Language().agreement_prompt);
        }

    }

    /**
     * 1--Facebook授权成功请求服务器获取信息
     *
     */
    userLoginFacebook(accessToken) {
        try {
            this.onShowProgressDialog();
            let url = apiDefines.GET_USER_INFO_FROM_FACEBOOK + accessToken;
            Util.get(url, (code, message, data) => {
                this.onHideProgressDialog();
                if (code == 1000) {
                    if (data) {
                        this.state.userInfo = JSON.stringify(data);
                        this.onFinishPage();
                        this.props.updateUserInfo(data);
                        this.props.openHome(SCENES.SCENE_APP_MAIN, 'global');
                        AsyncStorageTool.saveUserInfo(this.state.userInfo);
                        //console.log(this.state.userInfo);
                        if (data != null) {
                            RealmManager.initRealmManager(data.id, (callback) => {
                                if (callback) {
                                    this.props.getChatData(data.id, callback);
                                    this.props.getFamilyChat(data.id, callback);
                                    this.props.initFriendRealm(callback);
                                }
                            });
                            rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token);
                            rnRoNativeUtils.onSetLocation(data.lc);
                            this.configLocation(data.lc);
                        }
                    }
                } else {
                    Toast.show(message);
                }
            }, (error) => {
                this.onHideProgressDialog();
                Toast.show(Language().not_network);
            });
        } catch (e) {
            console.log('facebook login error:' + e);
        }
        //统计
        rnRoNativeUtils.onStatistics(Constant.FACEBOOK_LOGIN);
    }

    componentWillMount() {
        //更新所选国家信息
        this.updateSelectCountry();
        //更新是否是游客
        this.updateTouristInfo();
        //
        this.getTouristConfig();
        //
        this.getIsNewUser();
        //
    }

    /**
     *
     */
    checkAppInstalled() {
        this.openInstallTimer = setTimeout(() => {
            QQAPI.isQQInstalled().then((resp) => {
                if (resp) {
                    this.setState({
                        isQQAppInstalled: true
                    });
                } else {
                    this.setState({
                        isQQAppInstalled: false
                    });
                }
            }, (error) => {
                this.setState({
                    isQQAppInstalled: false
                });
            });
            WeChat.isWXAppInstalled().then((resp) => {
                if (resp) {
                    this.setState({
                        isWXAppInstalled: true
                    });
                } else {
                    this.setState({
                        isWXAppInstalled: false
                    });
                }
            }, (error) => {
                this.setState({
                    isWXAppInstalled: false
                });
            });
            if (Platform.OS === 'ios') {
                this.setState({
                    isFaceBookAppInstalled: true
                });
            } else {
                try {
                    if (NativeModules.NativeJSModule.checkApkExist) {
                        NativeModules.NativeJSModule.checkApkExist('', (facebookInstalled) => {
                            this.setState({
                                isFaceBookAppInstalled: facebookInstalled
                            });
                        });
                    }
                } catch (e) {

                }
            }
        },500);

    }

    getIsNewUser() {
        AsyncStorageTool.getStorageData(AsyncStorageTool.NEW_USER_CONFIG, (config) => {
            //没有数据并且数据显示为第一次
            if (config && config.isNewUser) {
                this.props.setIsNewUser();
            }
        });
    }

    getTouristConfig() {
        AsyncStorageTool.getStorageData(AsyncStorageTool.TOURIST_CONFIG, (config) => {
            if (config) {
                this.props.updateEnterNoviceCount(config.noviceCount);
            }
        });
    }

    /**
     * 页面销毁后
     */
    componentWillunMount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.imageLoadTimer && clearTimeout(this.imageLoadTimer);
        this.wechatTimer && clearTimeout(this.wechatTimer);
        this.qqTimer && clearTimeout(this.qqTimer);
        this.lineTimer && clearTimeout(this.lineTimer);
        this.openLoginTimer && clearTimeout(this.openLoginTimer);
        this.openInstallTimer && clearTimeout(this.openInstallTimer);
        this.guideTimer && clearTimeout(this.guideTimer);
    }

    handleMethod(isConnected) {
    }

    /**
     * 1--组件渲染完成
     * 2--检测用户是否登录，以及检测客户端是否安装微信或QQ客户端
     */
    componentDidMount() {
        //页面渲染完毕后定义加载view
        this.checkAppInstalled();
        progressDialog = this.refs.progressDialog;
        //android/ios隐藏启动屏（rn启动时白屏很长是时间问题）
        InteractionManager.runAfterInteractions(() => {
            SplashScreen.hide();
        }, 500);
        this.getUserInfo();
        this.check_login();
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    /**
     * 检测是否登录或是直接进入
     */
    check_login() {
        InteractionManager.runAfterInteractions(() => {
            this.openLoginTimer = setTimeout(() => {
                this.openLogin();
            }, 500);
        });
    }

    /**
     * 1--获取本地存储的用户信息
     */
    getUserInfo() {
        AsyncStorageTool.getStorageData(AsyncStorageTool.USER_INFO, (userInfo) => {
            //console.log('本地存储用户信息：' + JSON.stringify(userInfo));
            if (userInfo != null) {
                this.state.userName = userInfo.name;
                this.state.id = userInfo.id;
                if (userInfo.token.access_token != apiDefines.DEFAULT_ACCESS_TOKEN) {
                    RealmManager.initRealmManager(userInfo.id, (callback) => {
                        if (callback) {
                            this.props.getChatData(userInfo.id, callback);
                            this.props.getFamilyChat(userInfo.id, callback);
                            this.props.initFriendRealm(callback);
                        }
                    });
                }
                rnRoNativeUtils.initUserInfoToNative(userInfo.id, userInfo.name, userInfo.sex, userInfo.image, userInfo.token.access_token);
                rnRoNativeUtils.onSetLocation(userInfo.lc);
                this.configLocation(userInfo.lc);
                this.props.updateUserInfo(userInfo);
            }
        });
    }

    /**
     * location信息设置
     * @param lc
     */
    configLocation(lc) {
        if (lc) {
            this.props.setLocation(lc);
        }
    }

    /**
     * 1--设置是否直接进入应用
     */
    openLogin() {
        if (this.state.userName == null || this.state.userName == '') {
            this.setState({
                login: false
            });
        } else {
            this.onFinishPage();
            this.props.openHome(SCENES.SCENE_APP_MAIN, 'global');
            try {
                this.guideTimer = setTimeout(() => {
                    this.state.login = false;
                }, 1000)
            } catch (e) {

            }
        }
    }

    /**
     *1-- 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     *1-- 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    /**
     * 展开更多登录方式
     */
    onStartAnimation() {
        this.state.currentAlpha = this.state.currentAlpha == 0.0 ? 1.0 : 0.0;
        Animated.timing(
            this.state.fadeAnim,
            {toValue: this.state.currentAlpha}
        ).start();
    }

    /**
     * 收取更多登录
     */
    onCloseMoreLogin() {
        if (!this.state.currentAlpha == 0.0) {
            this.onStartAnimation();
        }
    }
}

const
    styles = StyleSheet.create({
        loginImage: {
            width: 25,
            height: 25,
            resizeMode: 'contain',
            padding: 5,
            alignSelf: 'center',
        },
        TextImage: {
            marginTop: ScreenHeight * 0.73,
            resizeMode: 'contain',
            alignSelf: 'center',
            width: ScreenWidth * 0.71,
        },
        bgButton: {
            borderColor: Color.colorWhite,
            alignSelf: 'center',

        },
        image: {
            position: 'absolute',
            left: 0,
            top: 0,
            bottom: 0,
            right: 0,
            width: null,
            height: null,
            resizeMode: 'stretch'
        },
        containerView: {
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'transparent',
        },
        absolute: {
            position: "absolute", top: 0, left: 0, bottom: 0, right: 0
        },
        blurView: {
            position: 'absolute', left: 0, top: 0, bottom: 0, right: 0,
        },
        text: {
            fontSize: 16,
            textAlign: 'center',
            color: Color.colorWhite,
            padding: 15
        },
        loginText: {
            fontSize: 14, textAlign: 'center', color: Color.colorWhite, paddingTop: 10
        },
        loginIcon: {
            width: 45, height: 45, resizeMode: 'contain', alignSelf: 'center'
        },
        moreLoginIcon: {
            width: 10, height: 10, resizeMode: 'contain', alignSelf: 'center', marginLeft: 5
        }
    });
const mapDispatchToProps = dispatch => ({
    openHome: (route, key) => dispatch(pushRoute({key: route}, key)),
    finishPage: (key) => dispatch(popRoute(key)),
    updateUserInfo: (data) => dispatch((userInfo(data))),
    selectCountry: (data) => dispatch(selectedCountry(data)),
    initCountry: () => dispatch(initCountry()),
    updateTourist: (isTourist) => dispatch((updateTourist(isTourist))),
    getChatData: (id, realm) => dispatch(getChatData(id, realm)),
    getFamilyChat: (id, realm) => dispatch(getFamilyChat(id, realm)),
    setLocation: (lc) => dispatch(location(lc)),
    updateEnterNoviceCount: (count) => dispatch(updateEnterNoviceCount(count)),
    setIsNewUser: () => dispatch(setIsNewUser()),
    initFriendRealm: (realm) => dispatch(initFriendRealm(realm)),

});
const mapStateToProps = state => ({
    navigation: state.cardNavigation,
    userInfo: state.userInfoReducer.data,
    configData: state.checkRegisterReducer.data,
});
export default connect(mapStateToProps, mapDispatchToProps)(Splash);