/**
 * Created by wangxu on 2017/5/6.
 */
'use strict';
import React, {Component} from 'react';
import {
    Content,
    Form,
    Item,
    Input,
    Label,
    Button,
    CardItem,
    Left,
    Body,
    Right,
    Text,
    Spinner, Icon, Container
} from 'native-base';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {
    Image,
    StyleSheet,
    View,
    Platform,
    NativeModules,
    Dimensions,
    TouchableOpacity,
    DeviceEventEmitter,
    NativeEventEmitter,
} from 'react-native';
import Color from '../../../resources/themColor';
import netWorkTool from '../../../support/common/netWorkTool';
import Toast from '../../../support/common/Toast';
import Dialog, {Prompt,} from '../../../support/common/dialog';
import Language from '../../../resources/language'
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import Util from '../../../support/common/utils';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import ImagePicker from 'react-native-image-crop-picker';
import * as SCENES from '../../../support/actions/scene';
import {userInfo} from '../../../support/actions/userInfoActions';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import * as Constant from '../../../support/common/constant';
import Loading from '../../../support/common/Loading';
import {getChatData} from '../../../support/actions/chatActions';
import {initFriendRealm} from '../../../support/actions/friendActions';
import RealmManager from '../../../support/common/realmManager';
import UtilsTool from '../../../support/common/UtilsTool';

import {location} from '../../../support/actions/configActions';

const bgBack = require('../../../resources/imgs/ic_back.png');
const female = require('../../../resources/imgs/ic_female.png');
const male = require('../../../resources/imgs/ic_male.png');
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const ScreenHeight = Dimensions.get('window').height;
const ScreenWidth = Dimensions.get('window').width;
const {
    popRoute,
    pushRoute
} = actions;
let progressDialog;
const {RNMessageSender}=NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
class RegisterUserInfo extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        openPage: React.PropTypes.func,
        registerData: React.PropTypes.object,
        updateUserInfo: React.PropTypes.func,
        leancloudNode: React.PropTypes.string,
        getChatData: React.PropTypes.func,
        initFriendRealm: React.PropTypes.func,
        setLocation: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        this.state = {
            image: '',
            name: '',
            gender: Language().female,
            sex: 2,
            disabled: false,
            imageInfo: null,
            disableRegister: false
        };
    }

    componentDidMount() {
        //页面渲染完毕后定义加载view
        progressDialog = this.refs.progressDialog;
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        try {
            if (Platform.OS === 'android') {
                this.onNativeMessageLister = DeviceEventEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
            } else {
                this.onNativeMessageLister = rNMessageSenderEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
            }
        } catch (e) {
            //alert(e)
        }
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.finishTimer && clearTimeout(this.finishTimer);
        this.pickCameraTimer && clearTimeout(this.pickCameraTimer);
        this.pickTimer && clearTimeout(this.pickTimer);
        this.disableRTime && clearTimeout(this.disableRTime);
        this.onNativeMessageLister.remove();
    }

    handleMethod(isConnected) {

    }

    render() {
        let imagePath = this.state.image == '' ? (require('../../../resources/imgs/signup_img_portrait.png')) : (this.state.image)
        return (
            <Container style={{backgroundColor:'#fff'}}>
                <Content >
                    <CardItem
                        style={{backgroundColor:(Color.headColor),height:60,paddingTop:headPaddingTop,paddingBottom:0}}>
                        <Left style={{flex:1}}>
                            <Button transparent onPress={this.onFinishPage.bind(this)} disabled={this.state.disabled}>
                                <Image source={bgBack} style={styles.bgBack}></Image>
                            </Button>
                        </Left>
                        <Body style={{alignSelf:'center',flex:3}}>
                        <Text
                            style={{color: '#fff',fontSize: 18,alignSelf:'center'}}>{Language().register_user_info}</Text>
                        </Body>
                        <Right style={{flex:1}}/>
                    </CardItem>
                    <Form style={{marginTop:10}}>
                        <TouchableOpacity style={{alignSelf:'center'}} onPress={this.onSetAvatar.bind(this)}>
                            <Image source={imagePath}
                                   style={{width:80,height:80,borderRadius: 40,alignSelf:'center',margin:10}}>
                            </Image>
                            <Text
                                style={{fontSize:15,color:(Color.loginTextColor),padding:5,textAlign:'center'}}>{Language().set_avatar}</Text>
                        </TouchableOpacity>
                        <Item style={{marginLeft:15,marginRight:15,marginTop:15,borderColor:'rgba(0, 0, 0, 0)'}}>
                            <Input onChangeText={(text)=>this.onChangeUserName(text)} maxLength={20}
                                   placeholder={Language().placeholder_nickname}
                                   placeholderTextColor={Color.loginTextColor}
                                   style={{borderWidth: 1, borderColor: (Color.loginBorderColor), borderRadius: 3,paddingLeft:10}}/>
                        </Item>
                        <View flexDirection='row'
                              style={{margin:15,borderWidth: 1, borderColor: (Color.loginBorderColor), borderRadius: 3,height:50}}>
                            <Button transparent style={{margin:0,padding:0,flex:1}}
                                    onPress={this.onSetGender.bind(this)}>
                                <Text
                                    style={{fontSize:16,color:(Color.blackColor),paddingLeft:10,flex:1}}>{this.state.gender}</Text>
                            </Button>
                        </View>
                    </Form>
                    <TouchableOpacity
                        style={{borderRadius: 3, marginRight: 15, marginLeft: 15, marginTop: 30,backgroundColor:(Color.phoneLoginButColor),justifyContent: 'center',height:45}}
                        onPress={this.onRegister.bind(this)}>
                        <Text
                            style={{color:'#fff',fontSize:16,padding:5,alignSelf:'center'}}>{Language().into_werewolf}</Text>
                    </TouchableOpacity>
                </Content>
                <Loading ref="progressDialog" loadingTitle={Language().register_loading}/>
                {Dialog.inject()}
                <Prompt ref='setAvatar' message='' title='' posText='' negText=''
                        contentStyle={{backgroundColor:(Color.transparent),width:ScreenWidth,paddingLeft: 15,paddingBottom: 0,paddingRight: 15}}
                        dialogStyle={{backgroundColor:'rgba(0, 0, 0, 0.25)',justifyContent: 'flex-end'}}>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8}}>
                        <Text
                            style={{alignSelf:'center',padding:15,color:(Color.loginTextColor),fontSize:18}}>{Language().title_upload_type}</Text>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:12}} onPress={this.pickSingle.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().with_photo_album}</Text>
                        </TouchableOpacity>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:12}} onPress={this.pickSingleWithCamera.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().with_camera}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8,marginTop:10}}>
                        <TouchableOpacity style={{padding:12}} onPress={this.onDismissDialog.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().cancel}</Text>
                        </TouchableOpacity>
                    </View>
                </Prompt>
                <Prompt ref='selectGenderDialog' message='' title='' posText='' negText=''
                        contentStyle={{backgroundColor:(Color.transparent),width:ScreenWidth,paddingLeft: 15,paddingBottom: 0,paddingRight: 15}}
                        dialogStyle={{backgroundColor:'rgba(0, 0, 0, 0.25)',justifyContent: 'flex-end'}}>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8}}>
                        <Text
                            style={{alignSelf:'center',padding:15,color:(Color.loginTextColor),fontSize:18}}>{Language().title_select_gender}</Text>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:12}} onPress={this.onChangeGenderMale.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().male}</Text>
                        </TouchableOpacity>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:12}} onPress={this.onChangeGenderFemale.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().female}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8,marginTop:10}}>
                        <TouchableOpacity style={{padding:12}}
                                          onPress={this.onDismissSelectGenderDialog.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().cancel}</Text>
                        </TouchableOpacity>
                    </View>
                </Prompt>
            </Container>

        );
    }

    closeProgress() {

    }

    /**
     * 用户昵称
     * @param text
     */
    onChangeUserName(text) {
        this.state.name = text;
    }

    /**
     * 设置用户头像
     */
    onSetAvatar() {
        //暂时不开放，目前还有点问题
        this.refs.setAvatar.show();

    }

    /**
     * 上传图片
     * @param image
     */
    photoFileUpload(image) {
        /**
         * 上传图片
         */
        UtilsTool.filePathFormat(UtilsTool.fileHeader2,image.path,(newPath)=>{
            rnRoNativeUtils.uploadFile([newPath],Constant.ACTION_UP_LOAD_USER_HEAD, 10000);
        });
        this.onShowProgressDialog();

    }

    /**
     * 设置用户性别
     */
    onSetGender() {
        this.refs.selectGenderDialog.show();
    }

    /**
     * 注册并进入游戏
     */
    onRegister() {
        let {name} = this.state;
        name = name.trim();
        if (!name.length) {
            Toast.show(Language().placeholder_nickname);
            return;
        }
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                this.disableRTime = setTimeout(() => {
                    this.state.disableRegister = false;
                }, 2500);
                if (!this.state.disableRegister) {
                    // //如何用户设置头像，头像上传后进行注册
                    if (this.state.imageInfo != null) {
                        //上传头像
                        this.photoFileUpload(this.state.imageInfo);
                    } else {
                        //直接注册
                        this.onShowProgressDialog();
                        this.doRegister('', '');

                    }
                }
                this.state.disableRegister = true;
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 注册请求
     */
    doRegister(avatar, imageId) {
        let registerData = this.props.registerData;
        if (registerData != null) {
            registerData.gender = this.state.sex;
            registerData.name = this.state.name;
        }
        let url = apiDefines.REGISTER + registerData.phone + apiDefines.NAME + registerData.name + apiDefines.CODE + registerData.code + apiDefines.GENDER + registerData.gender + apiDefines.PASSWORD + registerData.password;
        if (avatar != '') {
            url = url + apiDefines.AVATAR + avatar + apiDefines.OBJECTID + imageId + apiDefines.HOST + this.props.leancloudNode;
        }
        Util.get(url, (code, message, data) => {
            this.onHideProgressDialog();
            if (code == 1000) {
                this.onFinishPage();
                this.props.updateUserInfo(data);
                try {
                    this.props.openPage(SCENES.SCENE_APP_MAIN, 'global');
                }catch (e){
                    console.log(e)
                }
                AsyncStorageTool.saveUserInfo(JSON.stringify(data));
                if (data != null) {
                    rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token);
                    rnRoNativeUtils.onSetLocation(data.lc);
                    RealmManager.initRealmManager(data.id, (callback) => {
                        if (callback) {
                            this.props.getChatData(data.id, callback);
                            this.props.initFriendRealm(callback);
                        }
                    });
                    this.configLocation(data.lc);
                }
            } else {
                Toast.show(message);
            }
        }, (failed) => {
            Toast.show(Language().not_network);
        });
    }

    /**
     * location信息设置
     * @param lc
     */
    configLocation(lc) {
        if (lc) {
            this.props.setLocation(lc);
        }
    }

    /**
     * 关闭设置头像页面
     */
    onDismissDialog() {
        this.refs.setAvatar.hide();
    }

    /**
     * 关闭选取性别的dialog
     */
    onDismissSelectGenderDialog() {
        this.refs.selectGenderDialog.hide();
    }

    /**
     * 选择中性别为男
     */
    onChangeGenderMale() {
        this.onDismissSelectGenderDialog();
        this.setState({
            gender: Language().male,
            sex: 1
        });
    }

    /**
     * 选择中性别为女
     */
    onChangeGenderFemale() {
        this.onDismissSelectGenderDialog();
        this.setState({
            gender: Language().female,
            sex: 2
        });
    }

    /**
     * 选取相册照片，单张
     */
    pickSingle() {
        this.onDismissDialog();
        this.pickTimer = setTimeout(() => {
            ImagePicker.openPicker({
                width: 400,
                height: 400,
                cropping: true,
                cropperTintColor: Color.headColor
            }).then(image => {
                this.setState({
                    image: {uri: image.path, width: image.width, height: image.height, mime: image.mime},
                });
                this.state.imageInfo = image;
            }).catch(e => {
                console.log(e);
            });
        }, 200)
    }

    /**
     * 拍照选取
     */
    pickSingleWithCamera() {
        this.onDismissDialog();
        this.pickCameraTimer = setTimeout(() => {
            ImagePicker.openCamera({
                width: 400,
                height: 400,
                cropping: true,
                cropperTintColor: Color.headColor
            }).then(image => {
                this.setState({
                    image: {uri: image.path, width: image.width, height: image.height},
                });
                this.state.imageInfo = image;
            }).catch(e => {
                console.log(e);
            });
        }, 200)
    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     * 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    onFinishPage() {
        this.finishTimer = setTimeout(() => {
            this.setState({
                disabled: false
            });
        }, 2000);
        this.props.finishPage('global');
        this.setState({
            disabled: true
        });
    }

    /**
     * 统一处理native的消息
     * @param event
     */
    nativeMessage(event) {
        if (event && event.Data) {
            let data = JSON.parse(event.Data);
            switch (data.action) {
                case Constant.ACTION_UP_LOAD_USER_HEAD:
                    if (data.params) {
                        let code = data.params.code;
                        let images = data.params.data;
                        if (code == 0) {
                            //上传成功
                            if (images && images[0]) {
                                this.doRegister(images[0].url, images[0].key);
                            } else {
                                this.doRegister('', '');
                            }
                        } else {
                            //上传失败
                            Toast.show(data.params.message)
                            this.doRegister('', '');
                        }
                    } else {
                        this.doRegister('', '');
                    }
                    break;
            }
        }
    }
}
const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    rightArrow: {
        width: 15, height: 15, resizeMode: 'contain', marginLeft: 10
    },
    loginButton: {
        borderRadius: 3, backgroundColor: '#F2C454', marginRight: 10, marginLeft: 10, marginTop: 20
    },
    gender: {
        alignSelf: 'center', width: 20, height: 20,
    }
});
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        updateUserInfo: (data) => dispatch((userInfo(data))),
        getChatData: (id, realm) => dispatch(getChatData(id, realm)),
        initFriendRealm: (realm) => dispatch(initFriendRealm(realm)),
        setLocation: (lc) => dispatch(location(lc)),
    }
);
const mapStateToProps = state => ({
    registerData: state.registerReducer.data,
    leancloudNode: state.configReducer.leanCloud_node
});
export default connect(mapStateToProps, mapDispatchToProps,)(RegisterUserInfo);