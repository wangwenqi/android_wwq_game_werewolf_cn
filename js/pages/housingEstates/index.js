/**
 * Created by wangxu on 2018/1/20.
 * 我的房间／房产
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Image,
    Dimensions,
    View,
    Platform,
    NativeModules,
    ListView,
    RefreshControl,
    InteractionManager,
    TouchableOpacity,
    Text, Modal, ScrollView, TextInput, DeviceEventEmitter, NativeEventEmitter
} from 'react-native';
import {Spinner} from 'native-base';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {checkOpenRegister} from '../../../support/actions/checkRegisterActions';
import {fetchRoomOwnedList, fetchingRoomOwned, updateHouseOwnedData} from '../../../support/actions/recreationActions';
import {fetchRoomUsersList, clearRoomUsersList} from '../../../support/actions/roomUsersActions';
import Toast from '../../../support/common/Toast';
import CachedImage from '../../../support/common/CachedImage';
import LoadImageView from '../../../support/common/LoadImageView';
import rnToNativeUtils from '../../../support/common/rnToNativeUtils';
import Color from '../../../resources/themColor';
import LoadMoreFooter from '../../../support/common/LoadMoreFooter'
import Language from '../../../resources/language';
import netWorkTool from '../../../support/common/netWorkTool';
import Loading from '../../../support/common/Loading';
import Util from '../../../support/common/utils';
import ImageManager from '../../../resources/imageManager/index';
import InternationSourceManager from '../../../resources/internationSourceManager';
import * as types from '../../../support/actions/actionTypes';
import * as Constant from '../../../support/common/constant';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import * as SCENES from '../../../support/actions/scene';
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const roomUserNameFonSize = (Platform.OS === 'ios') ? 12 : 10;
const roomUserNamePadding = (Platform.OS === 'ios') ? 1 : 0;
const recreationLimit = 5;
const likeCount = 99999999;
const {
    popRoute,
    pushRoute
} = actions;
const {RNMessageSender}=NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
const actionTypeApply = 'apply';
const actionTypeSelect = 'select';
const actionTypeTransfer = 'transfer';
const ownedType = 'purchased';
let recreationRoomList = new Array();
let audioTypes = new Array();
let roomUserList = new Array();
let selfThis;
let progressDialog;
let userUid;
let userName;
let userImage;
class HousingEstates extends Component {
    static propTypes = {
        userInfo: React.PropTypes.object,
        isRefreshing: React.PropTypes.bool,
        isNoMore: React.PropTypes.bool,
        data: React.PropTypes.object,
        fetchRoomOwnedList: React.PropTypes.func,
        fetchingRoomOwned: React.PropTypes.func,
        updateHouseOwnedData: React.PropTypes.func,
        hostLocation: React.PropTypes.string,
        configData: React.PropTypes.object,
        updateConfig: React.PropTypes.func,
        clearRoomUsersList: React.PropTypes.func,
        fetchRoomUsersList: React.PropTypes.func,
        openPage: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        actionType: React.PropTypes.string,
        message: React.PropTypes.string,
        roomUsers: React.PropTypes.array
    }

    constructor(props) {
        super(props);
        selfThis = this;
        if (this.props.data != null) {
            recreationRoomList = this.props.data;
        }
        if (this.props.configData) {
            if (this.props.configData.audio_room_types) {
                audioTypes = this.props.configData.audio_room_types;
            }
        }
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        //2.设置返回数据
        this.state = {
            dataSource: ds.cloneWithRows(recreationRoomList),
            selectUserDataSource: ds.cloneWithRows(roomUserList),
            height: 0,
            isConnectedNetWork: true,
            filterFlag: 0,
            isFinishing: false,
            showVoiceModalDialog: false,
            showNeedPasswordDialog: false,
            showSelectModalDialog: false,
            showModelProgress: false,
            dialogConfirm: Language().buy,
            dialogTitle: Language().apply_house,
            selectRoomID: '',
            dialogType: '',
            selectUserInfo: null,
            password1: '',
            password2: '',
            password3: '',
            password4: '',
            roomPassword: '',
            roomDetailData: null,
            roomID: '',
        }
    }

    componentDidMount() {
        progressDialog = this.refs.progressDialog;
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        if (Platform.OS === 'android') {
            this.onNativeMessageLister = DeviceEventEmitter.addListener('ReactNativeBridge', this.nativeMessage);
        } else {
            this.onNativeMessageLister = rNMessageSenderEmitter.addListener('ReactNativeBridge', this.nativeMessage);
        }
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.finishTimer && clearTimeout(this.finishTimer);
        this.onNativeMessageLister.remove();
    }

    componentWillMount() {
        InteractionManager.runAfterInteractions(() => {
            this.props.fetchingRoomOwned();
            this.props.fetchRoomOwnedList();
        });
    }

    handleMethod(isConnected) {
        selfThis.state.isConnectedNetWork = isConnected;
    }

    /**
     * 渲染页面
     * @returns {XML}
     */
    render() {
        let userInfo = this.props.userInfo;
        if (userInfo) {
            userUid = userInfo.uid;
            userImage = userInfo.image;
            userName = userInfo.name;
        }
        if (this.props.data) {
            recreationRoomList = this.props.data;
        } else {
            recreationRoomList = new Array();
        }
        if (this.props.configData) {
            if (this.props.configData.audio_room_types) {
                audioTypes = this.props.configData.audio_room_types;
            }
        }
        if (this.props.actionType == types.ROOM_OWNED_DATA_ERROR && this.props.message) {
            Toast.show(this.props.message);
        }
        if (this.props.actionType == types.ROOM_OWNED_DATA || this.props.actionType == types.ROOM_OWNED_DATA_ERROR) {
            this.onHideProgressDialog();
        }
        if (this.state.showVoiceModalDialog) {
            this.props.isRefreshing = false;
        }
        roomUserList = this.props.roomUsers;
        return (
            <View style={{backgroundColor:Color.voice_room_bg_color,flex:1}}>
                {this.headerView()}
                {this._defaultView()}
                <ListView dataSource={this.state.dataSource.cloneWithRows(recreationRoomList)}
                          ref="listView"
                          renderRow={this.renderRow.bind(this)}
                          initialListSize={1}
                          scrollRenderAheadDistance={50}
                          removeClippedSubviews={true}
                          pageSize={recreationLimit}
                          onEndReachedThreshold={10}
                          enableEmptySections={true}
                          refreshControl={
                                     <RefreshControl
                                        refreshing={this.props.isRefreshing}
                                         onRefresh={this._onRefresh.bind(this)}
                                         colors={[Color.list_loading_color]} />
                                    }
                          closeOnRowBeginSwipe={true}/>
                <Loading ref="progressDialog" loadingTitle={Language().operation_loading}/>
                <Modal visible={this.state.showVoiceModalDialog} transparent={true} animationType='fade'>
                    <View flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                          style={[styles.dialogContainer]}>
                        <ScrollView alwaysBounceVertical={false}>
                            <View
                                style={{borderRadius:8,width:ScreenWidth-30,maxHeight:ScreenHeight-80,alignSelf:'center',backgroundColor:'#e3dce9',padding:0}}>
                                <View flexDirection='row' style={{justifyContent:'center',padding:5}}>
                                    <Text
                                        style={{color:Color.voice_set_text_color,alignSelf:'center',fontSize:18,textAlign:'center',marginTop:10,fontWeight:'bold'}}>{this.state.dialogTitle}</Text>
                                </View>
                                {this._modelDialogContentView()}
                                <View flexDirection='row' style={{justifyContent:'center',margin:10}}>
                                    <TouchableOpacity activeOpacity={0.9}
                                                      onPress={this.onCloseModal.bind(this)}
                                                      style={[styles.actionButton,{backgroundColor:Color.colorWhite,marginLeft:25}]}>
                                        <Text
                                            style={{alignSelf:'center',color:'#845fe4'}}>{Language().cancel}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={0.8} onPress={this.onConfirmModal.bind(this)}
                                                      style={[styles.actionButton,{marginRight:25}]}>
                                        <Text
                                            style={{alignSelf:'center',color:Color.colorWhite}}>{this.state.dialogConfirm}</Text>
                                    </TouchableOpacity>
                                </View>
                                <Text
                                    style={{color:'#7e61dc',marginBottom:15,marginLeft:30,fontSize:12,marginTop:2}}>{Language().apply_house_prompt}
                                </Text>
                            </View>
                            {this.state.showModelProgress ? (
                                    <View style={[styles.loading,{left:ScreenWidth/2-40,top:(ScreenHeight/4)-20}]}>
                                        <Spinner size='small' style={{height:35}}/>
                                        <Text
                                            style={{marginTop:2,fontSize: 12,color:Color.colorWhite}}>{Language().request_loading}</Text>
                                    </View>) : null}
                        </ScrollView>
                    </View>
                </Modal>
                <Modal visible={this.state.showNeedPasswordDialog} transparent={true} animationType='fade'>
                    <View flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                          style={[styles.dialogContainer]}>
                        <ScrollView alwaysBounceVertical={false}>
                            <View
                                style={{borderRadius:8,width:ScreenWidth-30,maxHeight:ScreenHeight-80,alignSelf:'center',backgroundColor:'#e3dce9',padding:0}}>
                                <View flexDirection='row' style={{justifyContent:'center',padding:5}}>
                                    <Text
                                        style={{color:Color.voice_set_text_color,alignSelf:'center',fontSize:18,textAlign:'center',marginTop:10,fontWeight:'bold'}}>{Language().room_number + this.state.roomID}</Text>
                                </View>
                                {this._modelInitPassword()}
                                <View flexDirection='row' style={{justifyContent:'center',margin:10}}>
                                    <TouchableOpacity activeOpacity={0.9}
                                                      onPress={this.closePasswordModal.bind(this)}
                                                      style={[styles.actionButton,{backgroundColor:Color.colorWhite,marginLeft:25}]}>
                                        <Text
                                            style={{alignSelf:'center',color:'#845fe4'}}>{Language().cancel}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={0.8} onPress={this.confirmPasswordModel.bind(this)}
                                                      style={[styles.actionButton,{marginRight:25}]}>
                                        <Text
                                            style={{alignSelf:'center',color:Color.colorWhite}}>{Language().confirm}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </Modal>
                <Modal visible={this.state.showSelectModalDialog} transparent={true} animationType='fade'>
                    <View flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                          style={[styles.dialogContainer]}>
                        <View
                            style={{borderRadius:8,width:ScreenWidth-50,height:ScreenHeight/3*2,alignSelf:'center',backgroundColor:'#e3dce9',padding:0}}>
                            <Text
                                style={{color:Color.voice_set_text_color,alignSelf:'center',fontSize:18,textAlign:'center',marginTop:10,marginBottom:10,fontWeight:'bold'}}>{Language().title_select_transfer_user}</Text>
                            <ListView dataSource={this.state.selectUserDataSource.cloneWithRows(roomUserList)}
                                      renderRow={this.selectUserRenderRow}
                                      initialListSize={1}
                                      scrollRenderAheadDistance={50}
                                      removeClippedSubviews={true}
                                      pageSize={recreationLimit}
                                      onEndReachedThreshold={10}
                                      enableEmptySections={true}/>
                            <View flexDirection='row' style={{justifyContent:'center',margin:10}}>
                                <TouchableOpacity activeOpacity={0.8}
                                                  onPress={this.cancelSelectDialog.bind(this)}
                                                  style={[styles.actionButton,{backgroundColor:Color.colorWhite}]}>
                                    <Text
                                        style={{alignSelf:'center',color:'#845fe4'}}>{Language().cancel}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }

    /**
     *
     */
    confirmPasswordModel() {
        if (!this.state.roomPassword) {
            Toast.show(Language().placeholder_password)
            return;
        }
        let newData = {
            roomId: this.state.roomID,
            roomPassword: this.state.roomPassword
        }
        this.doEnterVoiceRoom(newData);
        this.closePasswordModal();
    }

    /**
     *
     */
    closePasswordModal() {
        this.setState({
            password1: '',
            password2: '',
            password3: '',
            password4: '',
            roomPassword: '',
            roomDetailData: null,
            roomID: '',
            showNeedPasswordDialog: false
        });
    }

    /**
     * 默认背景
     * @param height
     * @returns {*}
     * @private
     */
    _defaultView() {
        return recreationRoomList.length == 0 ? (<View
                style={{justifyContent: 'center',backgroundColor:apiDefines.APP_KEY == apiDefines.WEREWOLF ? Color.voice_room_bg_color : Color.voice_room_list_bg_color,position: 'absolute',flex:1,left:0,top:(Platform.OS === 'ios') ? 74 : 50,right:0,bottom:0}}>
                <CachedImage style={styles.defaultBg} source={ImageManager.myHouseDefaultBg}/>
            </View>) : null;
    }

    /**
     *取消选择移交者
     */
    cancelSelectDialog() {
        selfThis.setState({
            showSelectModalDialog: false,
            showVoiceModalDialog: true
        });
    }

    /**
     *1-- 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     *
     * @param message
     */
    onShowProgressDialog(message) {
        if (progressDialog != null) {
            progressDialog.show({loadingTitle: message});
        }
    }

    /**
     *1-- 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    /**
     * 取消
     */
    onCloseModal() {
        selfThis.setState({
            showVoiceModalDialog: false,
            selectUserInfo: null
        });
        InteractionManager.runAfterInteractions(() => {
            //刷新
            this.props.fetchRoomOwnedList();
            this.props.clearRoomUsersList();
        });
    }

    /**
     * 确认
     */
    onConfirmModal() {
        switch (this.state.dialogType) {
            case actionTypeTransfer:
                if (selfThis.state.selectUserInfo) {
                    let userId = selfThis.state.selectUserInfo.id;
                    let url = apiDefines.ROOM_HANDOVER + selfThis.state.selectRoomID + apiDefines.PARAMS_PEER + userId;
                    selfThis.createRequest(true, url, null);
                } else {
                    Toast.show(Language().prompt_select_transfer_user);
                }
                break;
            case actionTypeApply:
                let url = apiDefines.ROOM_PURCHASE + selfThis.state.selectRoomID;
                selfThis.createRequest(true, url, null);
                break;
        }
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    renderRow(item, sectionId, rowId) {
        let users = item.users;
        let title = '';
        if (item.title) {
            title = item.title;
            title = title.replace(/[\r\n]/g, "");
        } else {
            title = item.room_id;
        }
        let userCount = 0;
        if (users) {
            userCount = users.length;
        }
        if (item.userCount) {
            userCount = item.userCount;
        }
        let user_1 = this._getUser(users, 0);
        let user_2 = this._getUser(users, 1);
        let user_3 = this._getUser(users, 2);
        let user_4 = this._getUser(users, 3);
        let room_lock_icon = item.needPassword ? InternationSourceManager().icRoomIconLock : null;
        let audioData = this.getAudioTypeData(item.child_type);
        return (<View style={{overflow:'hidden'}}>
                <TouchableOpacity activeOpacity={0.6} onPress={()=>{this.enterVoiceRoom(item)}}
                                  style={{backgroundColor:Color.transparent,justifyContent:'center'}}>
                    <View flexDirection='row' style={{marginLeft:10,marginRight:10,padding:5,overflow:'hidden'}}>
                        <View flexDirection='row'
                              style={{borderRadius:5,borderColor:Color.voice_room_bg_color,borderWidth:2,overflow:'hidden'}}>
                            <CachedImage style={styles.image} source={ImageManager.voiceItemBg}>
                                <View flexDirection='row'>
                                    <View style={{borderRadius:3,justifyContent:'center'}}>
                                        <LoadImageView style={styles.ownerHeader}
                                                       source={(item.image)?({uri:item.image}):ImageManager.audioCreateHeader}
                                                       defaultSource={ImageManager.audioCreateHeader}/>
                                        <View style={{position: "absolute", top: 5,left:3}}>
                                            <View flexDirection='row'>
                                                <CachedImage style={styles.voiceHomeIcon}
                                                             source={ImageManager.voiceHomeIcon}/>
                                                <View flexDirection='row'
                                                      style={{backgroundColor:'#ff009c',borderRadius:8,paddingLeft:5,paddingRight:5,height:12,alignSelf:'center'}}>
                                                    <Text numberOfLines={1}
                                                          style={styles.rowStatusText}>{item.room_id}</Text>
                                                </View>
                                            </View>
                                            <View flexDirection='row' style={{marginTop:3}}>
                                                <CachedImage style={styles.voiceHomeIcon}
                                                             source={ImageManager.voiceUserIcon}/>
                                                <View
                                                    style={{backgroundColor:'#ffba13',borderRadius:8,paddingLeft:5,paddingRight:5,height:12,alignSelf:'center'}}>
                                                    <Text numberOfLines={1}
                                                          style={[styles.rowStatusText,{color:Color.blackColor}]}>{userCount}人</Text>
                                                </View>
                                            </View>
                                        </View>
                                        <View
                                            style={{position: "absolute",bottom:8,right:25,backgroundColor:'#bc06ad',borderRadius:2,left:15}}>
                                            <Text numberOfLines={1}
                                                  style={[styles.rowStatusText,{fontSize:roomUserNameFonSize,color:'#cabddd',padding:roomUserNamePadding,paddingLeft:5,paddingRight:8,backgroundColor:Color.transparent}]}>{item.name}</Text>
                                        </View>
                                        {this.audioTypeImageView(audioData)}
                                    </View>
                                    <View style={{flex:1}}>
                                        <View flexDirection='row' style={{height:37}}>
                                            <Text numberOfLines={1} style={styles.rowTitle}>{title}</Text>
                                            <CachedImage style={styles.voiceHomeLock} source={room_lock_icon}/>
                                        </View>
                                        <View style={[styles.renderRow]}>
                                            <View flexDirection='row'
                                                  style={{flex:4,justifyContent:'center',alignSelf:'center',paddingRight:10,paddingLeft:10}}>
                                                <View style={{flex:1}}>
                                                    <LoadImageView style={styles.userHeader}
                                                                   source={(!user_1.avatar.length)?ImageManager.defaultNoHeader:({uri:user_1.avatar})}
                                                                   defaultSource={ImageManager.icDefaultUser}/>
                                                    <Text numberOfLines={1}
                                                          style={styles.rowUserName}>{user_1.name}</Text>
                                                </View>
                                                <View style={{flex:1}}>
                                                    <LoadImageView style={styles.userHeader}
                                                                   source={(!user_2.avatar.length)?ImageManager.defaultNoHeader:({uri:user_2.avatar})}
                                                                   defaultSource={ImageManager.icDefaultUser}/>
                                                    <Text numberOfLines={1}
                                                          style={styles.rowUserName}>{user_2.name}</Text>
                                                </View>
                                                <View style={{flex:1}}>
                                                    <LoadImageView style={styles.userHeader}
                                                                   source={(!user_3.avatar.length)?ImageManager.defaultNoHeader:({uri:user_3.avatar})}
                                                                   defaultSource={ImageManager.icDefaultUser}/>
                                                    <Text numberOfLines={1}
                                                          style={styles.rowUserName}>{user_3.name}</Text>
                                                </View>
                                                <View style={{flex:1}}>
                                                    <LoadImageView style={styles.userHeader}
                                                                   source={(!user_4.avatar.length)?ImageManager.defaultNoHeader:({uri:user_4.avatar})}
                                                                   defaultSource={ImageManager.icDefaultUser}/>
                                                    <Text numberOfLines={1}
                                                          style={styles.rowUserName}>{user_4.name}</Text>
                                                </View>
                                            </View>
                                        </View>
                                        <View flexDirection='row' style={{flex:1}}>
                                            <View flexDirection='row' style={{flex:1.3}}>
                                                {this.audioLikePopView(item.like, ImageManager.audioLike)}
                                                {this.audioLikePopView(item.pop, ImageManager.audioPop)}
                                            </View>
                                            <View style={{flex:1}}>
                                                {this.audioTypeTitleView(audioData)}
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </CachedImage>
                        </View>
                    </View>
                </TouchableOpacity>
                {this.houseActionView(item)}
                <View style={styles.rowSeparator}></View>
            </View>
        );
    }

    /**
     * 进入音频房间
     */
    enterVoiceRoom(roomData) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                /**
                 * 如果自己是房间持有者，有密码依然可以进入
                 */
                if (roomData) {
                    let data = {
                        roomId: roomData.room_id,
                        roomPassword: ''
                    }
                    if (roomData.needPassword) {
                        if (roomData.owner_type == ownedType) {
                            selfThis.doEnterVoiceRoom(data);
                        } else {
                            selfThis.setState({
                                showNeedPasswordDialog: true,
                                roomID: roomData.room_id,
                                roomDetailData: roomData
                            });
                        }
                    } else {
                        selfThis.doEnterVoiceRoom(data);
                    }
                } else {
                    Toast.show(Language().enter_room_error);
                }
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });

    }

    /**
     * 执行进入房间的操作
     */
    doEnterVoiceRoom(roomData) {
        let NativeData = {
            action: 'enterAudioRoom',
            params: roomData,
            options: null
        }
        if (this.state.showVoiceModalDialog) {
            this.onCloseModal();
        }
        rnToNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }

    /**
     * 房间ID
     * @returns {XML}
     * @private
     */
    _modelInitPassword() {
        return (<View style={{marginTop:5}}>
            <View style={{marginTop:10}}>
                {this.passwordInputView()}
                {this.passwordView()}
            </View>
            <Text style={styles.roomNumber}>{Language().placeholder_room_password}</Text>
        </View>);
    }

    /**
     *
     * @returns {XML}
     */
    passwordInputView() {
        return (<TextInput
            ref='passwordInput'
            style={{paddingLeft:0,color:(Color.transparent),fontSize:12,height:35,padding:5,paddingLeft:0,backgroundColor:Color.transparent,lineHeight:0,selectionColor:Color.transparent}}
            autoFocus={true} maxLength={4}
            selectionColor={Color.transparent}
            onChangeText={(text)=>this._changeChangePassword(text)}
            keyboardType='numeric'
            value={this.state.roomPassword}/>);
    }

    /**
     *
     */
    passwordEndEditing() {
        if (this.refs.passwordInput) {
            this.refs.passwordInput.blur();
        }
    }

    /**
     * @param text
     * @private
     */
    _changeChangePassword(text) {
        let password = text + '';
        let one = password[0] == null ? '' : password[0];
        let two = password[1] == null ? '' : password[1];
        let three = password[2] == null ? '' : password[2];
        let four = password[3] == null ? '' : password[3];
        if (password.length == 4) {
            selfThis.passwordEndEditing();
        }
        if (/^[\d]+$/.test(text)) {
            this.setState({
                roomPassword: text,
                password1: one,
                password2: two,
                password3: three,
                password4: four,
            });
        } else {
            if (/^[\d{1}]$/.test(this.state.roomPassword)) {
                this.setState({
                    roomPassword: '',
                    password1: one,
                    password2: two,
                    password3: three,
                    password4: four,
                });
            }
        }
    }

    /**
     *
     * @returns {XML}
     */
    passwordView() {
        return (<View flexDirection='row' style={{position: "absolute"}}>
            <TouchableOpacity onPress={this.focusPassword.bind(this)}
                              activeOpacity={1}
                              style={{justifyContent:'center',backgroundColor:'#E1DDE8',flex:1}}>
                <View flexDirection='row'
                      style={{alignSelf:'center',justifyContent:'center',flex:1,backgroundColor:'#E1DDE8'}}>
                    <View
                        style={styles.passwordViewStyle}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password1}</Text>
                    </View>
                    <View
                        style={[styles.passwordViewStyle,{marginLeft:10}]}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password2}</Text>
                    </View>
                    <View
                        style={[styles.passwordViewStyle,{marginLeft:10}]}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password3}</Text>
                    </View>
                    <View
                        style={[styles.passwordViewStyle,{marginLeft:10}]}>
                        <Text
                            style={styles.passwordTextStyle}>{this.state.password4}</Text>
                    </View>

                </View>

            </TouchableOpacity>
        </View>);
    }

    /**
     *
     */
    focusPassword() {
        if (this.refs.passwordInput) {
            this.refs.passwordInput.focus();
        }
    }

    /**
     * @param content
     * @param source
     * @returns {*}
     */
    audioLikePopView(content, source) {
        if (content) {
            if (content > likeCount) {
                content = likeCount + '+';
            }
            return (<View flexDirection='row'>
                <Image style={[styles.voiceHomeIcon,{height:14,width:14,marginLeft:5}]} source={source}/>
                <Text numberOfLines={1}
                      style={[styles.rowStatusText,{color:'#f30ce9',fontSize:12,backgroundColor:Color.transparent}]}>{content}</Text>
            </View>);
        } else {
            return null;
        }
    }

    /**
     *
     * @param data
     * @returns {XML}
     */
    audioTypeTitleView(data) {
        if (data && data.t) {
            return (<View style={{position: 'absolute',left:0,right:0,bottom:0,backgroundColor:Color.transparent}}>
                <View
                    style={{backgroundColor:'#FFFD38',paddingLeft:10,paddingRight:10,alignSelf:'center',borderTopLeftRadius:8,borderTopRightRadius:8,paddingBottom:3,paddingTop:3}}>
                    <Text numberOfLines={1}
                          style={[styles.rowStatusText,{color:'#4A1C98',fontSize:11,fontWeight:'bold'}]}>{data.t}</Text>
                </View>
            </View>);
        }
    }

    /**
     *
     * @param audioData
     * @returns {null}
     */
    audioTypeImageView(audioData) {
        if (audioData) {
            return (<CachedImage
                source={{uri:audioData.i}}
                style={{position: "absolute",bottom:5,right:5, resizeMode: 'contain',alignSelf: 'center',width:25,height:25}}/>)
        } else {
            return null;
        }
    }

    /**
     *
     * @returns {null}
     */
    getAudioTypeData(childType) {
        if (audioTypes) {
            for (let i = 0; i < audioTypes.length; i++) {
                if (audioTypes[i].k == childType) {
                    return audioTypes[i];
                }
            }
            return null;
        } else {
            return null;
        }
    }

    /**
     *
     * @returns {*}
     * @private
     */
    _modelDialogContentView() {
        let transferUserView;
        let applyMessage = Language().apply;
        let applyHouseCondition1 = Language().apply_house_condition1;
        let applyHouseCondition2 = Language().apply_house_condition2;
        if (this.state.dialogType == actionTypeTransfer) {
            applyMessage = Language().transfer;
            applyHouseCondition1 = Language().transfer_house_condition1;
            applyHouseCondition2 = Language().transfer_house_condition2;
            transferUserView = this.transferUserView();
        }
        return ( <View>
            <View style={{marginLeft:20,marginRight:20}}>
                <Text style={{color:'#4d187d',lineHeight:25,padding:5,paddingLeft:10,fontSize:16}}>
                    {'      ' + applyMessage + this.state.selectRoomID}
                    <Text style={{padding:5}}>
                        {applyHouseCondition1}
                        <Text style={{color:'#ff6d0b',fontSize:16,padding:5}}>
                            {Language().common_house_condition}
                            <Text style={{color:'#4d187d',fontSize:16,padding:5}}>
                                {applyHouseCondition2}
                            </Text>
                        </Text>
                    </Text>
                </Text>
            </View>
            {this.houseJewelView()}
            {transferUserView}
        </View>);
    }

    /**
     *
     * @returns {XML}
     */
    houseJewelView() {
        return (<View flexDirection='row' style={{justifyContent:'center',marginTop:5}}>
            <Text style={{alignSelf:'center',color:'#ff1c1c',fontSize:17}}>{Language().buy_house_condition}</Text>
            <CachedImage style={styles.houseJewelStyle} source={ImageManager.icHouseJewel}/>
        </View>);
    }

    /**
     *移交房契者view
     */
    transferUserView() {
        return (<View flexDirection='row' style={{margin:10,marginLeft:15,marginRight:15,marginTop:20}}>
            <Text
                style={{color:Color.voice_set_text_color,fontSize:16,alignSelf:'center'}}>{Language().select_transfer_user}</Text>
            {this.selectUserView()}
        </View>)
    }

    /**
     *
     * @param item
     */
    selectUserRenderRow(item) {
        let userId;
        let userUid = '';
        if (item.uid) {
            userUid = 'ID:' + item.uid;
        }
        try {
            if (selfThis.props.userInfo) {
                userId = selfThis.props.userInfo.id;
            }
        } catch (e) {
        }
        if (item.id == userId) {
            return null;
        } else {
            return ( <View style={{overflow:'hidden'}}>
                <TouchableOpacity activeOpacity={0.8} style={{justifyContent:'center'}}
                                  onPress={()=>selfThis.setSelectUser(item)}>
                    <View flexDirection='row' style={{marginLeft:15,marginRight:15,flex:1}}>
                        <LoadImageView style={{width:40,height:40,borderRadius:20,margin:5,marginLeft:10}}
                                       source={item.avatar?{uri:item.avatar}:ImageManager.defaultUser}
                                       defaultSource={ImageManager.defaultUser}/>
                        <Text numberOfLines={1}
                              style={{color:Color.voice_me_text_color,alignSelf:'center',fontSize:16,marginLeft:5,flex:1}}>{item.name}</Text>
                        <Text numberOfLines={1}
                              style={{color:Color.voice_me_text_color,alignSelf:'center',fontSize:14,marginLeft:5,flex:1}}>{userUid}</Text>
                    </View>
                </TouchableOpacity>
            </View> );
        }
    }

    /**
     * 设置选中的
     */
    setSelectUser(item) {
        selfThis.setState({
            showSelectModalDialog: false,
            selectUserInfo: item,
            showVoiceModalDialog: true
        });
    }

    /**
     * 点击选择要移交者，并且显示选择的人
     */
    selectUserView() {
        let selectUserInfo = this.state.selectUserInfo;
        let userUid = '';
        let contactView = (<Text
            style={{color:Color.prompt_text_color,fontSize:16,alignSelf:'flex-end',marginRight:10}}>{Language().prompt_select_transfer_user}</Text>);
        if (selectUserInfo) {
            if (selectUserInfo.uid) {
                userUid = 'ID:' + selectUserInfo.uid;
            }
            contactView = ( <View flexDirection='row'
                                  style={{alignSelf:'flex-end',marginRight:10,marginLeft:10}}>
                <LoadImageView style={{width:30,height:30,borderRadius:15,margin:3,marginRight:8,marginLeft:8}}
                               source={selectUserInfo.avatar?{uri:selectUserInfo.avatar}:ImageManager.defaultUser}/>
                <Text numberOfLines={1}
                      style={{color:Color.voice_me_text_color,alignSelf:'center',flex:1}}>{selectUserInfo.name}</Text>
                <Text numberOfLines={1}
                      style={{color:Color.voice_me_text_color,alignSelf:'center',marginLeft:5,width:85,fontSize:13}}>{userUid}</Text>
            </View>);
        }
        return ( <TouchableOpacity onPress={()=>selfThis.openTransferUser()}
                                   style={[styles.touchableOpacityHouse,{marginBottom:0,flex:1,marginLeft:10}]}
                                   activeOpacity={0.6}>
            <View flexDirection='row'
                  style={{flex:1,justifyContent:'center',height:50}}>
                <View style={{flex:1,justifyContent:'center'}}>
                    {contactView}
                </View>
                <CachedImage style={[styles.houseStyle,{width:15,height:15}]} source={ImageManager.iconArrow}/>
            </View>
        </TouchableOpacity>);
    }

    openTransferUser() {
        selfThis.setState({
            showSelectModalDialog: true,
            showVoiceModalDialog: false
        });
    }

    /**
     *
     */
    houseActionView(item) {
        if (item) {
            if (item.owner_type == 'user') {
                return ( <TouchableOpacity onPress={()=>selfThis.applyHouse(item)}
                                           style={styles.touchableOpacityHouse}
                                           activeOpacity={0.8}>
                    <View
                        style={{flex:1,borderRadius:3,justifyContent:'center',backgroundColor:'#f9347b',marginLeft:20,marginRight:20}}>
                        <Text style={{alignSelf:'center',color:Color.colorWhite}}>{Language().apply_house}</Text>
                    </View>
                </TouchableOpacity>);
            } else if (item.owner_type == 'purchased') {
                return (<View flexDirection='row' style={{marginLeft:20,marginRight:20,marginBottom:5}}>
                    <TouchableOpacity onPress={()=>selfThis.selectHouse(item)}
                                      style={[styles.touchableOpacityHouse,{marginBottom:0,flex:1}]}
                                      activeOpacity={0.8}>
                        <View flexDirection='row'
                              style={{flex:1,borderRadius:3,justifyContent:'center',backgroundColor:'#7051ff'}}>
                            <CachedImage style={styles.houseStyle} source={ImageManager.icSelectHouse}/>
                            <Text style={{alignSelf:'center',color:Color.colorWhite}}>{Language().select_house}</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>selfThis.transferHouse(item)}
                                      style={[styles.touchableOpacityHouse,{marginBottom:0,flex:1,marginLeft:10}]}
                                      activeOpacity={0.8}>
                        <View flexDirection='row'
                              style={{flex:1,borderRadius:3,justifyContent:'center',backgroundColor:'#a54aef'}}>
                            <CachedImage style={styles.houseStyle} source={ImageManager.icTransferHouse}/>
                            <Text style={{alignSelf:'center',color:Color.colorWhite}}>{Language().transfer_house}</Text>
                        </View>
                    </TouchableOpacity>
                </View>);
            }
        }
    }

    /**
     * 申请房契
     * @param item
     */
    applyHouse(item) {
        selfThis.setState({
            showVoiceModalDialog: true,
            dialogTitle: Language().apply_house,
            dialogConfirm: Language().buy,
            selectRoomID: item.room_id,
            dialogType: actionTypeApply,

        });
    }

    /**
     * 查看房契
     * @param item
     */
    selectHouse(item) {
        let owenData = {
            name: userName,
            uid: userUid,
            apply_time: item.purchase_time,
            room_id: item.room_id,
            image: userImage
        };
        this.props.updateHouseOwnedData(owenData);
        this.props.openPage(SCENES.SCENE_HOUSE_PROPERTY_CARD, 'global');
    }

    /**
     * 移交房契
     * @param item
     */
    transferHouse(item) {
        InteractionManager.runAfterInteractions(() => {
            //刷新
            this.props.fetchRoomUsersList(item.room_id);
        });
        selfThis.setState({
            showVoiceModalDialog: true,
            dialogTitle: Language().transfer_house,
            dialogConfirm: Language().transfer,
            selectRoomID: item.room_id,
            dialogType: actionTypeTransfer,
        });
    }

    /**
     *
     * @param users
     * @param index
     * @returns {{name: string, avatar: string}}
     * @private
     */
    _getUser(users, index) {
        let user = {
            name: '',
            avatar: '',
        }
        if (users) {
            if (index < users.length) {
                if (users[index].name) {
                    user.name = users[index].name;
                }
                if (users[index].avatar) {
                    user.avatar = users[index].avatar;
                }
            }
        }
        return user;
    }

    /**
     * 刷新
     * @private
     */
    _onRefresh = () => {
        netWorkTool.checkNetworkState((isConnected) => {
            if (!isConnected) {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
            this.props.fetchingRoomOwned();
            InteractionManager.runAfterInteractions(() => {
                //刷新
                this.props.fetchRoomOwnedList();
            });

        });
    };

    /**
     * 列表固定头部
     */
    headerView() {
        return ( <View flexDirection='row' style={styles.headerView}>
            <TouchableOpacity
                style={[styles.touchableOpacity]}
                activeOpacity={0.8}
                onPress={this.finishPage.bind(this)}>
                <View style={{width:50,height:50,justifyContent:'center'}}>
                    <Image source={ImageManager.icBack} style={styles.backStyle}/>
                </View>
            </TouchableOpacity>
            <Text style={styles.mainTitle}>{Language().my_house}</Text>
            <View style={{width:50}}/>
        </View>);
    }

    /**
     * 网路请求
     * @param url
     * @param actionType
     */
    createRequest(isGet, url, body) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                selfThis.showProgress();
                if (isGet) {
                    Util.get(url, (code, message, data) => {
                        selfThis.hideProgress();
                        Toast.show(message);
                        if (code == 1000) {
                            selfThis.onCloseModal();
                        }
                    }, (failed) => {
                        selfThis.hideProgress();
                        Toast.show(netWorkTool.NOT_NETWORK);
                    });
                } else {
                }

            } else {
                selfThis.hideProgress();
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     *展示进度
     */
    showProgress() {
        if (!selfThis.state.showModelProgress) {
            selfThis.setState({
                showModelProgress: true
            });
        }
    }

    /**
     * 隐藏
     */
    hideProgress() {
        if (selfThis.state.showModelProgress) {
            selfThis.setState({
                showModelProgress: false
            });
        }
    }

    /**
     *离开
     */
    finishPage() {
        if (!this.state.isFinishing) {
            this.props.finishPage('global');
            this.state.isFinishing = true;
        }
        this.finishTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }

    /**
     * 统一处理native的消息
     * @param event
     */
    nativeMessage = (event) => {
        if (event && event.Data) {
            let data = JSON.parse(event.Data);
            switch (data.action) {
                case Constant.LEAVE_AUDIO_ROOM:
                    netWorkTool.checkNetworkState((isConnected) => {
                        if (!isConnected) {
                            Toast.show(netWorkTool.NOT_NETWORK);
                        }
                        this.props.fetchingRoomOwned();
                        InteractionManager.runAfterInteractions(() => {
                            //刷新
                            this.props.fetchRoomOwnedList();
                        });

                    });
                    break;
            }
        }
    }


// /**
//  * @returns {*}
//  * @private
//  */
// selectUser() {
//     return (<Dropdown style={[styles.dropdownStyle]}
//                       dropdownStyle={[styles.dropDown]}
//                       options={selectUserData}
//                       renderRow={this.selectUserRenderRow.bind(this)}
//                       onSelect={(idx, value) => this.selectUserOnSelect(idx, value)}>
//         <View flexDirection='row' style={{justifyContent:'center',alignSelf:'center',height:50}}>
//             <View flexDirection='row'
//                   style={{alignSelf:'center',borderWidth:1,borderColor:'#845fe4',width:ScreenWidth/2}}>
//                 <LoadImageView style={{width:30,height:30,borderRadius:15,margin:3,marginRight:8,marginLeft:8}}
//                                source={{uri:selectUserData[0].image}}/>
//                 <Text style={{color:Color.voice_me_text_color,alignSelf:'center'}}>{selectUserData[0].name}:</Text>
//                 <Text style={{color:Color.voice_me_text_color,alignSelf:'center'}}>{selectUserData[0].uid}</Text>
//             </View>
//         </View>
//     </Dropdown>);
// }
//
// /**
//  *
//  * @param rowData
//  * @param rowID
//  * @param highlighted
//  */
// selectUserRenderRow(rowData, rowID, highlighted) {
//     return ( <TouchableOpacity activeOpacity={0.8} style={{flex:1}}>
//         <View flexDirection='row'>
//             <LoadImageView style={{width:30,height:30,borderRadius:15,margin:3,marginRight:8,marginLeft:8}}
//                            source={{uri:rowData.image}}/>
//             <Text style={{color:Color.voice_me_text_color,alignSelf:'center'}}>{rowData.name}:</Text>
//             <Text style={{color:Color.voice_me_text_color,alignSelf:'center'}}>{rowData.uid}</Text>
//         </View></TouchableOpacity>);
// }
//
// /**
//  *
//  * @param idx
//  * @param value
//  */
// selectUserOnSelect(idx, value) {
// }
}
const styles = StyleSheet.create({
    defaultBg: {
        alignSelf: 'center',
        width: ScreenWidth / 3,
        height: ScreenWidth / 3,
        resizeMode: 'contain',
    },
    voiceHome: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    voiceSearch: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginRight: 5
    },
    renderRow: {
        justifyContent: 'center',
        padding: 1,
        flexDirection: 'row'
    },
    userHeader: {
        width: 40,
        height: 40,
        borderRadius: 20,
        alignSelf: 'center',
        marginLeft: 8,
        marginRight: 8
    },
    ownerHeader: {
        width: 100,
        height: 120,
        borderRadius: 5,
        alignSelf: 'center',
        top: 0
    },
    rowTitle: {
        color: Color.colorWhite,
        fontSize: 14,
        marginLeft: 15,
        fontWeight: 'bold',
        marginTop: 0,
        marginBottom: 5,
        flex: 1,
        alignSelf: 'flex-end',
        backgroundColor: Color.transparent
    },
    rowUserName: {
        color: '#cabddd',
        fontSize: 10,
        alignSelf: 'center',
        textAlign: 'center',
        marginTop: 3,
        backgroundColor: Color.transparent, height: 13
    },
    rowStatusText: {
        color: Color.colorWhite,
        fontSize: 10,
        alignSelf: 'center',
        backgroundColor: Color.transparent
    },
    voiceIcon: {
        width: 50,
        height: 35,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    voiceHomeIcon: {
        width: 13,
        height: 14,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginRight: 1
    },
    loading: {
        position: 'absolute',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        height: 60,
        width: 80,
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#453b49',
    },
    voiceHomeLock: {
        width: 55,
        height: 37,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    roomNumber: {
        color: '#b09ce5',
        fontSize: 14,
        alignSelf: 'center',
        marginTop: 8
    }, image: {
        width: ScreenWidth - 30,
        height: 120,
        resizeMode: 'stretch',
    }, title: {
        fontSize: 14,
        color: 'gray',
        alignSelf: 'center', margin: 5
    }, headerView: {
        backgroundColor: Color.headColor,
        height: (Platform.OS === 'ios') ? 60 : 50,
        paddingTop: (Platform.OS === 'ios') ? 14 : 0,
        justifyContent: 'center'
    },
    touchableOpacity: {
        justifyContent: 'center',
        backgroundColor: Color.transparent,
    },
    mainTitle: {
        fontSize: 18,
        color: Color.colorWhite,
        backgroundColor: Color.transparent,
        textAlign: 'center',
        alignSelf: 'center',
        flex: 1
    }, backStyle: {
        width: 15,
        height: 15,
        alignSelf: 'center',
        resizeMode: 'contain',
    }, touchableOpacityHouse: {
        justifyContent: 'center',
        height: 30,
        borderRadius: 3,
        marginBottom: 5
    }, dialogContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
    }, actionButton: {
        backgroundColor: '#845fe4', justifyContent: 'center', height: 35, flex: 1, borderRadius: 5, margin: 10
    }, houseStyle: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        alignSelf: 'center', marginRight: 3
    }, houseJewelStyle: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        alignSelf: 'center', marginLeft: 5
    }, dropdownStyle: {
        alignSelf: 'center',
        height: 55,
        justifyContent: 'center',
        padding: 5, flex: 1,
    },
    dropDown: {
        marginTop: 2,
        justifyContent: 'center',
        backgroundColor: Color.colorWhite,
        height: 150, borderWidth: 1, borderColor: '#845fe4', flex: 1, width: ScreenWidth / 2,
        marginLeft: 7
    }, passwordViewStyle: {
        backgroundColor: '#835EDF',
        borderColor: '#6939DA',
        borderWidth: 1,
        padding: 5,
        borderRadius: 3,
        width: 25,
        height: 35
    }, passwordTextStyle: {fontSize: 16, color: Color.colorWhite, fontWeight: 'bold', alignSelf: 'center'}
});
const mapDispatchToProps = dispatch => ({
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        finishPage: (key) => dispatch(popRoute(key)),
        fetchRoomOwnedList: () => dispatch(fetchRoomOwnedList()),
        fetchingRoomOwned: () => dispatch(fetchingRoomOwned()),
        updateConfig: () => dispatch(checkOpenRegister()),
        fetchRoomUsersList: (roomId) => dispatch(fetchRoomUsersList(roomId)),
        updateHouseOwnedData: (data) => dispatch(updateHouseOwnedData(data)),
        clearRoomUsersList: () => dispatch(clearRoomUsersList()),
    }
);
const mapStateToProps = state => ({
    userInfo: state.userInfoReducer.data,
    isRefreshing: state.roomOwnedReducer.isRefreshing,
    data: state.roomOwnedReducer.data,
    hostLocation: state.configReducer.location,
    configData: state.checkRegisterReducer.data,
    actionType: state.roomOwnedReducer.types,
    message: state.roomOwnedReducer.message,
    roomUsers: state.roomUsersReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps)(HousingEstates);