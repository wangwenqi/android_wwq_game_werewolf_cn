/**
 * Created by wangxu on 2018/2/26.
 */
import React, {Component, Navigator} from 'react';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Image,
    StyleSheet,
    View,
    AsyncStorage,
    Platform,
    TouchableOpacity,
    ListView,
    Dimensions,
    InteractionManager,
    Text, Button
} from 'react-native';
//
import Language from '../../../resources/language';
import  * as styles from './style';
import * as Constant from '../../../support/common/constant';
import LoadingLike from '../../../support/common/LoadingLike';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import LoadImageView from '../../../support/common/LoadImageView';
import CardStack from '../../../support/common/CardStack';
import Card from '../../../support/common/Card';
import Toast from '../../../support/common/Toast';
import Color from '../../../resources/themColor';
import rnToNativeUtils from '../../../support/common/rnToNativeUtils';
import ImageManager from '../../../resources/imageManager';
import netWorkTool from '../../../support/common/netWorkTool';
import {recommendLikeList} from '../../../support/actions/recommendLikeActions';
//
const {
    popRoute,
} = actions;
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
//
class LikeEachOther extends Component {
    static propTypes = {
        userInfoData: React.PropTypes.object,
        getRecommendLikeList: React.PropTypes.func,
        recommendLikeList: React.PropTypes.array,
    }

    constructor(props) {
        super(props);
        this.state = {
            isFinishing: false,
            cards: [],
        };
    }

    componentDidMount() {
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        //this.loadinglike.show();

    }

    componentWillMount() {
        this.props.getRecommendLikeList();
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.finishTimer && clearTimeout(this.finishTimer);
        this.refreshCardTimer && clearTimeout(this.refreshCardTimer);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (!_.isEqual(this.props.recommendLikeList, nextProps.data));
    }

    handleMethod(isConnected) {

    }

    /**
     * 列表固定头部
     */
    headerView = () => {
        return ( <View flexDirection='row' style={styles.headerView}>
            <TouchableOpacity
                style={[styles.touchableOpacity]}
                activeOpacity={0.8}
                onPress={this.finishPage.bind(this)}>
                <View style={{width:50,height:50,justifyContent:'center'}}>
                    <Image source={ImageManager.icBack} style={styles.backStyle}/>
                </View>
            </TouchableOpacity>
            <Text style={styles.mainTitle}>缘分天注定</Text>
            <View style={{width:50}}/>
        </View>);
    }
    /**
     *
     */
    footerView = () => {
        return (<View style={styles.footer}>
            <View style={styles.buttonContainer}>
                <View style={styles.bottomView}>
                    <TouchableOpacity style={[styles.button]} onPress={()=>{this.swiper.swipeLeft();}}>
                        <Text style={{color:'#000'}}>unlike</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.bottomView}>
                    <TouchableOpacity style={[styles.button]} onPress={()=>{this.swiper.swipeRight();}}>
                        <Text style={{color:'#fd267d'}}>like</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>);
    }
    /**
     *退出该页面
     */
    finishPage = () => {
        if (!this.state.isFinishing) {
            this.props.finishPage('global');
            if (this.loadinglike) {
                this.loadinglike.hide();
            }
            this.state.isFinishing = true;
        }
        this.finishTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }

    render() {
        if (this.props.recommendLikeList) {
            this.state.cards = this.props.recommendLikeList;
        } else {
            this.state.cards = new Array();
        }
        return (<View style={styles.container}>
            {this.headerView()}
            <View style={{height:50,width:ScreenWidth,marginTop:5,marginBottom:5,justifyContent:'center'}}>
                <View
                    style={{justifyContent:'center',width:ScreenWidth*0.91,backgroundColor:Color.blackColor,alignSelf:'center',borderRadius:5}}>
                    <Text style={{color:Color.colorWhite,alignSelf:'center',marginTop:5,marginBottom:5}}>为了更快遇见喜欢的TA，可以先完善自己的个人资料哦～</Text>
                </View>
            </View>
            <CardStack
                style={styles.content}
                renderNoMoreCards={this.renderNoMoreCards}
                ref={swiper => {this.swiper = swiper}}
                disableBottomSwipe={true}
                disableTopSwipe={true}>
                {this.state.cards.map((item, index) => {
                    return (<Card style={[styles.card]}
                                  onSwipedLeft={this.onSwipeLeft}
                                  onSwipedRight={this.onSwipedRight}>
                        {this.cardViewItem(item)}
                    </Card>);
                })}
            </CardStack>
            {this.footerView()}
            <LoadingLike ref={loadinglike => {this.loadinglike = loadinglike}} onPress={this.finishPage.bind(this)}/>
        </View>);
    }

    /**
     *
     * @param item
     */
    cardViewItem = (item) => {
        return (<View style={{flex:1,marginTop:15}}>
            <View flexDirection='row' style={{flex:2,padding:5,justifyContent:'center'}}>
                <View style={{justifyContent:'center',alignSelf:'center'}}>
                    <LoadImageView
                        style={{resizeMode: 'cover',alignSelf:'center',height:ScreenHeight*0.6/3*2-10,width:(ScreenWidth*0.91-10)/4*3}}
                        source={{uri:item.image}}
                        defaultSource={ImageManager.defaultUser}/>
                </View>
                <View style={{justifyContent:'center',marginLeft:4,alignSelf:'center'}}>
                    <LoadImageView
                        style={{resizeMode: 'cover',alignSelf:'center',height:(ScreenHeight*0.6/3*2-16)/3,width:(ScreenWidth*0.91-10)/4-4}}
                        source={{uri:item.image}}
                        defaultSource={ImageManager.defaultUser}/>
                    <LoadImageView
                        style={{resizeMode: 'cover',alignSelf:'center',height:(ScreenHeight*0.6/3*2-16)/3,width:(ScreenWidth*0.91-10)/4-4,marginTop:3}}
                        source={{uri:item.image}}
                        defaultSource={ImageManager.defaultUser}/>
                    <LoadImageView
                        style={{resizeMode: 'cover',alignSelf:'center',height:(ScreenHeight*0.6/3*2-16)/3,width:(ScreenWidth*0.91-10)/4-4,marginTop:3}}
                        source={{uri:item.image}}
                        defaultSource={ImageManager.defaultUser}/>
                </View>
            </View>
            <View style={{flex:1}}></View>
        </View>);

    }
    /**
     *
     */
    renderNoMoreCards = () => {
        return ( <Text style={{fontWeight:'700', fontSize:18, color:'gray',alignSelf:'center'}}>正在获取更新有缘人...</Text>);
    }
    /**
     *
     * @param index
     */
    onSwipeLeft = (index) => {
        if (this.state.cards) {
            if (this.state.cards[index]) {
                console.log('onSwiped:' + this.state.cards[index].name + 'left');
            }
            if (index == this.state.cards.length - 1) {
                this.refreshCardTimer = setTimeout(() => {
                    this.props.getRecommendLikeList();
                }, 1000);
            }

        }
    }
    /**
     *
     * @param index
     */
    onSwipedRight = (index) => {
        if (this.state.cards) {
            if (this.state.cards[index]) {
                console.log('onSwiped:' + this.state.cards[index].name + 'right');
            }
            if (index == this.state.cards.length - 1) {
                this.refreshCardTimer = setTimeout(() => {
                    this.props.getRecommendLikeList();
                }, 1000);
            }
        }
    }
}

const mapDispatchToProps = dispatch => ({
    finishPage: (key) => dispatch(popRoute(key)),
    getRecommendLikeList: () => dispatch(recommendLikeList()),
});
const mapStateToProps = state => ({
    recommendLikeList: state.recommendLikeReducer.data,
    userInfoData: state.userInfoReducer.data,
});
export default connect(mapStateToProps, mapDispatchToProps)(LikeEachOther);