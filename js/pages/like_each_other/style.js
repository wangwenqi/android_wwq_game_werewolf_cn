/**
 * Created by wangxu on 2018/2/26.
 */
'use strict';
import {StyleSheet, Platform, Dimensions} from 'react-native';
import Color from '../../../resources/themColor';
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.voice_room_bg_color,
        flexDirection: 'column',
    },
    headerView: {
        backgroundColor: Color.headColor,
        height: (Platform.OS === 'ios') ? 60 : 50,
        paddingTop: (Platform.OS === 'ios') ? 14 : 0,
        justifyContent: 'center'
    },
    touchableOpacity: {
        justifyContent: 'center',
        backgroundColor: Color.transparent,
    },
    mainTitle: {
        fontSize: 18,
        color: Color.colorWhite,
        backgroundColor: Color.transparent,
        textAlign: 'center',
        alignSelf: 'center',
        flex: 1
    },
    allowView: {
        width: 13,
        height: 13,
        resizeMode: 'contain',
        alignSelf: 'flex-end',
        margin: 10,
        marginRight: 0
    },
    baseView: {
        backgroundColor: Color.colorWhite
    }, backStyle: {
        width: 15,
        height: 15,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    text: {
        textAlign: 'center',
        fontSize: 50,
        backgroundColor: 'transparent'
    },
    done: {
        textAlign: 'center',
        fontSize: 30,
        color: 'white',
        backgroundColor: 'transparent'
    }, card: {
        width: ScreenWidth * 0.91,
        height: ScreenHeight * 0.6,
        backgroundColor: Color.colorWhite,
        borderRadius: 5,
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowOpacity: 0.5,
        marginTop: 0,
    },
    label: {
        textAlign: 'center',
        fontSize: 55,
        fontFamily: 'System',
        color: Color.blackColor,
        backgroundColor: 'transparent',
        alignSelf: 'center'
    }, content: {
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center',
    }, footer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    buttonContainer: {
        width: 220,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        shadowColor: 'rgba(0,0,0,0.3)',
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowOpacity: 0.5,
        backgroundColor: Color.colorWhite,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 0,
        width: 60,
        height: 60,
        borderRadius: 60,
    },
    bottomView: {
        width: 75,
        height: 75,
        borderRadius: 75,
        backgroundColor: 'rgba(201, 201, 201, 0.3)',
        justifyContent: 'center',
        alignItems: 'center'
    }
});
module.exports = Styles;