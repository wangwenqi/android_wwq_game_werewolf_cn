/**
 * Created by wangxu on 2017/10/26.
 * 创建家族
 */
'use strict';
import React, {Component} from 'react';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import ReactNative, {
    StyleSheet,
    Image,
    Dimensions,
    View,
    Platform,
    AsyncStorage,
    NativeModules,
    TouchableOpacity, Text, TextInput, ScrollView, DeviceEventEmitter, NativeEventEmitter, InteractionManager
} from 'react-native';
import {CardItem, Left, Body, Right, Button} from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
//自定义工具类
import Toast from '../../../support/common/Toast';
import CachedImage from '../../../support/common/CachedImage';
import Color from '../../../resources/themColor';
import netWorkTool from '../../../support/common/netWorkTool';
import Language from '../../../resources/language'
import Loading from '../../../support/common/Loading';
import Dialog, {Prompt} from '../../../support/common/dialog';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import UtilsTool from '../../../support/common/UtilsTool';
import * as Constant from '../../../support/common/constant';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import * as SCENES from '../../../support/actions/scene';
import Util from '../../../support/common/utils';

//
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const headHeight = (Platform.OS === 'ios') ? 60 : 50;
//图片资源
const bgBack = require('../../../resources/imgs/ic_back.png');
const bgDefaultImage = require('../../../resources/imgs/ic_faimly_df_image.png');
const bgDim = require('../../../resources/imgs/ic_faimly_dim.png');
//
const defaultMessage = '1.玩家等级达到20级\n2.消耗500钻石';
//
const {RNMessageSender}=NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
const defaultLevel = 20;
const defaultDim = 500;
const {
    popRoute
} = actions;
let progressDialog;
let familyShortInput;
let familyInput;
let familyDesInput;
let that;
let createFamilyLevel = defaultLevel;
let createFamilyDim = defaultDim;
class CreateGroup extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        userInfo: React.PropTypes.object,
        configData: React.PropTypes.object,
    }

    constructor(props) {
        super(props);
        that = this;
        this.state = {
            family_name: '',
            family_short_name: '',
            family_des: '',
            image: '',
            conversationId: '',
            createAvailable: true,
            imageUrl: '',
            isUploadLoading: false,
            canGoBack: true,
            height: 0,
            viewHeight: 85
        };
    }

    componentDidMount() {
        //页面渲染完毕后定义加载view
        progressDialog = this.refs.progressDialog;
        familyShortInput = this.refs.family_input;
        familyInput = this.refs.family_short_input;
        familyDesInput = this.refs.family_des_input;
        //监听网络
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.onHandleMethod);
        try {
            if (Platform.OS === 'android') {
                this.onNativeMessageLister = DeviceEventEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
            } else {
                this.onNativeMessageLister = rNMessageSenderEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
            }
        } catch (e) {
            //alert(e)
        }
    }

    componentWillUnmount() {
        //移除监听
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.onHandleMethod);
        this.pickCameraTimer && clearTimeout(this.pickCameraTimer);
        this.pickTimer && clearTimeout(this.pickTimer);
        this.saveBtTimer && clearTimeout(this.saveBtTimer);
        this.goBackTimer && clearTimeout(this.goBackTimer);
        this.inputTimer && clearTimeout(this.inputTimer);
        this.onNativeMessageLister.remove();
    }

    render() {
        let imagePath = this.state.image == '' ? (bgDefaultImage) : (this.state.image);
        let createFamilyMessage = defaultMessage;
        let familyConfig = this.getCreateFamilyConfig();
        if (familyConfig) {
            if (familyConfig.limitation) {
                if (familyConfig.limitation.message) {
                    createFamilyMessage = familyConfig.limitation.message;
                }
                if (familyConfig.limitation.level) {
                    createFamilyLevel = familyConfig.limitation.level;
                }
            }
            if (familyConfig.cost) {
                if (familyConfig.cost.count) {
                    createFamilyDim = familyConfig.cost.count;
                }
            }
        }
        return (
            <View style={{backgroundColor:Color.colorWhite,flex:1}}>
                {this.headerView()}
                <View style={styles.container}>
                    <ScrollView alwaysBounceVertical={false} showsVerticalScrollIndicator={false} ref="scrollView"
                                style={{flex: 1}}>
                        <Text style={styles.group_prompt_text}>{Language().placeholder_family_name_text}：</Text>
                        <View style={styles.inputView}>
                            <TextInput
                                ref="family_input"
                                underlineColorAndroid='transparent'
                                placeholder={Language().placeholder_family_name}
                                placeholderTextColor={Color.prompt_create_group_text_color}
                                style={styles.inputText}
                                autoFocus={true}
                                onChangeText={(text)=>this._changeFamilyText(text)} value={this.state.family_name}/>
                        </View>
                        <Text style={styles.group_prompt_text}>{Language().placeholder_family_shortName_text}：</Text>
                        <View style={styles.inputView}>
                            <TextInput
                                ref="family_short_input"
                                underlineColorAndroid='transparent'
                                placeholder={Language().placeholder_family_shortName}
                                placeholderTextColor={Color.prompt_create_group_text_color}
                                style={styles.inputText}
                                onChangeText={(text)=>this._changeFamilyShortText(text)}
                                value={this.state.family_short_name}/>
                        </View>
                        <Text style={styles.group_prompt_text}>{Language().placeholder_family_des_text}：</Text>
                        <View
                            ref="family_des_view"
                            style={{borderRadius: 7, borderColor: '#b2b2b2', borderWidth: 1, padding: 0, margin: 10, marginLeft: 10, marginRight: 10, marginBottom: 15,height:this.state.viewHeight}}>
                            <TextInput
                                multiline={true}
                                ref="family_des_input"
                                underlineColorAndroid='transparent'
                                placeholder={Language().placeholder_family_des}
                                placeholderTextColor={Color.prompt_create_group_text_color}
                                style={[styles.inputText,{height:Math.max(35,this.state.height)}]}
                                maxLength={200}
                                onContentSizeChange={this.onContentSizeChange.bind(this)}
                                onChangeText={(text)=>this._changeFamilyDesText(text)} value={this.state.family_des}/>
                        </View>
                        <View flexDirection='row' style={{marginLeft:10}}>
                            <TouchableOpacity
                                style={{backgroundColor:(Color.transparent),justifyContent:'center'}}
                                activeOpacity={0.6} onPress={this.onSetFamilyImage.bind(this)}>
                                <CachedImage style={styles.bgFamilyImage} type='photo' source={imagePath}
                                             defaultSource={bgDefaultImage}/>
                            </TouchableOpacity>
                            <Text style={styles.uploadImageText}>{Language().placeholder_upload_family_image}</Text>
                        </View>
                        <View style={{margin:10,marginTop:20}}>
                            <Text style={styles.conditionText}>{Language().create_conditions}:</Text>
                            <Text style={[styles.conditionText,{fontSize:14,padding:2}]}>{createFamilyMessage}</Text>
                        </View>
                        <View style={styles.buttonView}>
                            <View style={styles.buttonChildView}>
                                <TouchableOpacity
                                    style={{backgroundColor:(Color.transparent),justifyContent:'center',flex:1}}
                                    activeOpacity={0.6}
                                    onPress={this.createFamily.bind(this)}>
                                    <View flexDirection='row'
                                          style={{flex:1,justifyContent:'center',alignSelf:'center'}}>
                                        <Image style={styles.dimIcon} source={bgDim}/>
                                        <Text
                                            style={{fontSize:18,color:'#873909',fontWeight:'bold',alignSelf:'center',marginLeft:5}}>{Language().create}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </View>
                <Loading ref="progressDialog" loadingTitle={Language().request_loading}/>
                {Dialog.inject()}
                <Prompt ref='familyImage' message='' title='' posText='' negText=''
                        contentStyle={{backgroundColor:(Color.transparent),width:ScreenWidth,paddingLeft: 15,paddingBottom: 0,paddingRight: 15}}
                        dialogStyle={{backgroundColor:'rgba(0, 0, 0, 0.25)',justifyContent: 'flex-end'}}>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 7}}>
                        <Text
                            style={{alignSelf:'center',padding:15,color:(Color.loginTextColor),fontSize:16}}>{Language().uoplad_family_image_type}</Text>
                        <View style={styles.rowSeparator}/>
                        <TouchableOpacity style={{padding:10}} onPress={this.pickSingle.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:3,alignSelf:'center'}}>{Language().with_photo_album}</Text>
                        </TouchableOpacity>
                        <View style={styles.rowSeparator}/>
                        <TouchableOpacity style={{padding:10}} onPress={this.pickSingleWithCamera.bind(this)}>
                            <Text
                                style={{fontSize:14,color:(Color.selectDialogTextColor),padding:3,alignSelf:'center'}}>{Language().with_camera}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 7,marginTop:8}}>
                        <TouchableOpacity style={{padding:10}} onPress={this.onDismissDialog.bind(this)}>
                            <Text
                                style={{fontSize:14,color:(Color.selectDialogTextColor),padding:3,alignSelf:'center'}}>{Language().cancel}</Text>
                        </TouchableOpacity>
                    </View>
                </Prompt>
            </View>
        );
    }

    inputFocused(refName) {
        try {
            if (this.refs.scrollView) {
                let scrollResponder = this.refs.scrollView.getScrollResponder();
                this.inputTimer = setTimeout(() => {
                    InteractionManager.runAfterInteractions(() => {
                        scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
                            ReactNative.findNodeHandle(this.refs[refName]), 110, true)
                    });
                }, 50);
            }

        } catch (e) {

        }
    }

    /**
     * header title view
     * @returns {XML}
     */
    headerView() {
        return (<CardItem
            style={{backgroundColor:(Color.headColor),height:headHeight,paddingTop:headPaddingTop,paddingBottom:0}}>
            <Left style={{flex:1}}>
                <Button transparent onPress={this.onFinishPage.bind(this)}>
                    <Image source={bgBack} style={styles.bgBack}></Image>
                </Button>
            </Left>
            <Body style={{alignSelf:'center',flex:3}}>
            <Text style={{color:Color.colorWhite,fontSize: 18,alignSelf:'center'}}>{Language().create_group}</Text>
            </Body>
            <Right style={{flex:1}}/>
        </CardItem>);
    }

    /**
     * 个性签名输入框换行并自增加
     * @param event
     */
    onContentSizeChange(event) {
        let viewHeight = 85;
        let inputHeight = event.nativeEvent.contentSize.height;
        if (inputHeight > viewHeight) {
            viewHeight = inputHeight;
        }
        this.setState({height: inputHeight, viewHeight: viewHeight});
        this.inputFocused('family_des_view')
    }

    /**
     * 家族名称
     * @param text
     * @private
     */
    _changeFamilyText(text) {
        this.setState({
            family_name: text
        });
    }

    /**
     * 家族简称
     * @param text
     * @private
     */
    _changeFamilyShortText(text) {
        this.setState({
            family_short_name: text
        });
    }

    /**
     * 家族介绍
     * @param text
     * @private
     */
    _changeFamilyDesText(text) {
        this.setState({
            family_des: text
        });
    }

    /**
     * 网络变化回调
     * @param isConnected
     */
    onHandleMethod(isConnected) {
        console.log('create group netWork:' + isConnected);
    }

    /**
     * 关闭页面
     */
    onFinishPage() {
        if (that.state.canGoBack) {
            this.props.finishPage('global');
            that.state.canGoBack = false;
            this.goBackTimer = setTimeout(() => {
                that.state.canGoBack = true;
            }, 2000);
            //取消上传
            if (that.state.isUploadLoading) {
                let options: {
                    needcallback: false,
                    timeout: 0
                }
                let params = {
                    cancel: true
                }
                let nativeData = {
                    action: Constant.ACTION_UPLOAD_FILE,
                    type: Constant.RN_NATIVE,
                    options: options,
                    params: params
                }
                rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
            }
        }

    }

    /**
     * 创建家族
     */
    createFamily() {
        try {
            if (familyDesInput) {
                familyDesInput.blur();
            }
            if (familyInput) {
                familyInput.blur();
            }
            if (familyShortInput) {
                familyShortInput.blur();
            }
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    if (that.state.createAvailable) {
                        let {family_name, family_short_name, family_des}=this.state;
                        if (!family_name.length) {
                            Toast.show(Language().placeholder_family_name_text)
                            return;
                        }
                        //TODO 移除家族名称限制
                        // let familyNameFormat = /^[\u4E00-\u9FA5A-Za-z0]+$/;
                        // if (!familyNameFormat.test(family_name)) {
                        //     Toast.show(Language().placeholder_family_name);
                        //     return;
                        // }
                        if (family_name.length > 16) {
                            Toast.show(Language().placeholder_family_name);
                            return;
                        }
                        if (!family_short_name.length) {
                            Toast.show(Language().placeholder_family_shortName_text)
                            return;
                        } else {
                            let format = /[\u4E00-\u9FA5]{2,}/g;
                            if (!format.test(family_short_name)) {
                                Toast.show(Language().placeholder_family_shortName)
                                return;
                            }
                        }
                        if (!family_des.length) {
                            Toast.show(Language().placeholder_family_des_text)
                            return;
                        }
                        //判断是否符合创建
                        if (that.getGameLevel() < createFamilyLevel) {
                            //等级不足
                            Toast.show(Language().create_family_level_error);
                            return;
                        }
                        if (that.getDim() < createFamilyDim) {
                            //钻石不足
                            Toast.show(Language().create_family_dim_error);
                            return;
                        }
                        that.state.createAvailable = false;
                        this.saveBtTimer = setTimeout(() => {
                            that.state.createAvailable = true;
                        }, 3000);
                        let options = {
                            needcallback: true,
                            timeout: 10000
                        }
                        let nativeData = {
                            action: Constant.ACTION_CREATE_FAMILY_CONVERSATION,
                            type: Constant.RN_NATIVE,
                            options: options
                        }
                        that.onShowProgressDialog();
                        rnRoNativeUtils.sendMessageToNativeNew(JSON.stringify(nativeData), (success) => {
                            //创建家族会话ID成功
                        }, (fail) => {
                            //消息发送失败
                            that.onHideProgressDialog();
                        });
                    }
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        } catch (e) {
            console.log('error:' + e);
        }
    }

    /**
     * 获取用户等级
     */
    getGameLevel() {
        let level = 0;
        if (that.props.userInfo) {
            if (that.props.userInfo.game) {
                level = that.props.userInfo.game.level;
            }
        }
        return level;
    }

    /**
     *获取用户钻石
     */
    getDim() {
        let dim = 0;
        if (that.props.userInfo) {
            if (that.props.userInfo.money) {
                dim = that.props.userInfo.game.dim;
            }
        }
        return dim;
    }

    /**
     * 获取创建家族的config
     * @returns {*}
     */
    getCreateFamilyConfig() {
        let config = null;
        if (that.props.configData) {
            if (that.props.configData.create_family) {
                config = that.props.configData.create_family;
            }
        }
        return config;
    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     * 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    /**
     * 选取相册照片，单张
     */
    pickSingle() {
        this.onDismissDialog();
        this.pickTimer = setTimeout(() => {
            ImagePicker.openPicker({
                width: 400,
                height: 400,
                cropping: true,
                cropperTintColor: Color.headColor
            }).then(image => {
                this.setState({
                    image: {uri: image.path, width: image.width, height: image.height},
                    imageUrl: image.path
                });
            }).catch(e => {
                console.log(e);
            });
        }, 200)
    }

    /**
     * 拍照选取
     */
    pickSingleWithCamera() {
        this.onDismissDialog();
        this.pickCameraTimer = setTimeout(() => {
            ImagePicker.openCamera({
                width: 400,
                height: 400,
                cropping: true,
                cropperTintColor: Color.headColor
            }).then(image => {
                this.setState({
                    image: {uri: image.path, width: image.width, height: image.height},
                    imageUrl: image.path
                });
            }).catch(e => {
                console.log(e);
            });
        }, 200)
    }

    /**
     * 关闭设置家族图标
     */
    onDismissDialog() {
        if (this.refs.familyImage) {
            this.refs.familyImage.hide();
        }
    }

    /**
     * 设置用户头像
     */
    onSetFamilyImage() {
        if (this.refs.familyImage) {
            this.refs.familyImage.show();
        }
    }

    /**
     * 上传图片
     */
    photoFileUpload() {
        let imageUrl = that.state.imageUrl;
        UtilsTool.filePathFormat(UtilsTool.fileHeader2, imageUrl, (newPath) => {
            rnRoNativeUtils.uploadFile([newPath], Constant.ACTION_UP_LOAD_FAMILY_HEAD, 10000);
        });
        that.state.isUploadLoading = true;
    }

    /**
     * 网络请求创建家族
     * @param faimlyImageUrl
     * @param faimlyImageId
     */
    requestCreateFamily(faimlyImageUrl, familyImageKey) {
        let body = {
            name: that.state.family_name,
            short_name: that.state.family_short_name,
            desc: that.state.family_des,
            image: faimlyImageUrl,
            lc_id: that.state.conversationId,
            image_id: familyImageKey
        }
        Util.post(apiDefines.GROUP_CREATE, body, (code, message, data) => {
            that.onHideProgressDialog();
            if (code == 1000) {
                if (that.state.conversationId) {
                    let nativeData = {
                        action: Constant.ACTION_ENTER_FAMILY_CONVERSATION,
                        params: {conversationId: that.state.conversationId},
                        type: Constant.RN_NATIVE
                    }
                    rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
                    const routes = this.props.navigation.routes;
                    if (routes.length > 1) {
                        for (var i = 0; i < routes.length; i++) {
                            if (routes[i].key == SCENES.SCENE_GROUP_LIST) {
                                that.props.finishPage('global');
                                break;
                            }
                        }
                    }
                    that.props.finishPage('global');
                }

            } else {
                Toast.show(message);
            }
        }, (error) => {
            that.onHideProgressDialog();
            Toast.show(Language().not_network);
        });
    }

    /**
     * 统一处理native的消息
     * @param event
     */
    nativeMessage(event) {
        if (event && event.Data) {
            let data = JSON.parse(event.Data);
            switch (data.action) {
                case Constant.ACTION_CREATE_FAMILY_CONVERSATION:
                    //native 返回的创建会话
                    if (data.params) {
                        if (data.params.conversationId) {
                            that.state.conversationId = data.params.conversationId;
                            //
                            if (that.state.imageUrl) {
                                that.photoFileUpload();
                            } else {
                                that.requestCreateFamily('', '');
                            }
                        } else {
                            that.onHideProgressDialog();
                            Toast.show(data.params.reason)
                        }
                    } else {
                        that.onHideProgressDialog();
                    }
                    break;
                case Constant.ACTION_UP_LOAD_FAMILY_HEAD:
                    that.state.isUploadLoading = false;
                    if (data.params) {
                        let code = data.params.code;
                        let images = data.params.data;
                        if (code == 0) {
                            //上传成功
                            if (images && images[0]) {
                                that.requestCreateFamily(images[0].url, images[0].key);
                            } else {
                                that.requestCreateFamily('', '');
                            }
                        } else {
                            //上传失败
                            Toast.show(data.params.message)
                            that.requestCreateFamily('', '');
                        }
                    } else {
                        that.requestCreateFamily('', '');
                    }
                    break;
            }
        }
    }
}

const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    container: {
        padding: 20, flex: 1, paddingBottom: 0
    },
    group_prompt_text: {
        color: '#333333',
        fontSize: 15,
    },
    inputView: {
        borderRadius: 7,
        borderColor: '#b2b2b2',
        borderWidth: 1,
        padding: 0,
        margin: 10,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 15,
        height: (Platform.OS === 'ios') ? 40 : 35,
        justifyContent: 'center'
    },
    inputText: {fontSize: 14, padding: 5, color: Color.setting_text_color, height: 35},
    bgFamilyImage: {
        width: 90,
        height: 90,
        resizeMode: 'contain',
    },
    uploadImageText: {padding: 10, color: Color.setting_text_color, fontSize: 16, marginLeft: 5, alignSelf: 'center'},
    conditionText: {
        color: Color.setting_text_color, fontSize: 16
    },
    buttonView: {
        borderRadius: 7,
        backgroundColor: '#ffac03',
        paddingBottom: 3,
        height: 38,
        width: 110,
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 10
    },
    buttonChildView: {borderRadius: 7, backgroundColor: '#ffd249', flex: 1},
    dimIcon: {width: 22, height: 22, resizeMode: 'contain', alignSelf: 'center', padding: 5},
    rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: Color.rowSeparator_line_color,
    },
});
const mapDispatchToProps = dispatch => ({
    finishPage: (key) => dispatch(popRoute(key)),
});
const mapStateToProps = state => ({
    navigation: state.cardNavigation,
    userInfo: state.userInfoReducer.data,
    configData: state.checkRegisterReducer.data,

});
export default connect(mapStateToProps, mapDispatchToProps)(CreateGroup);