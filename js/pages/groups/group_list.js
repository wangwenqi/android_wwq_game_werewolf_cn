/**
 * Created by wangxu on 2017/10/25.
 * 家族列表页面
 */
import React, {Component} from 'react';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {
    StyleSheet,
    Image,
    Dimensions,
    View,
    Platform,
    AsyncStorage,
    NativeModules,
    TouchableOpacity,
    ListView,
    RefreshControl,
    InteractionManager, Text, ScrollView, TouchableWithoutFeedback, TextInput
} from 'react-native';
import {CardItem, Left, Body, Right, Button} from 'native-base';

//自定义工具类
import Toast from '../../../support/common/Toast';
import CachedImage from '../../../support/common/CachedImage';
import Color from '../../../resources/themColor';
import netWorkTool from '../../../support/common/netWorkTool';
import Language from '../../../resources/language'
import Loading from '../../../support/common/Loading';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import * as Constant from '../../../support/common/constant';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import * as SCENES from '../../../support/actions/scene';
import * as types from '../../../support/actions/actionTypes';
import {familyGroupList, updateFamilyStatus} from '../../../support/actions/familyGroupActions';
import {checkOpenRegister} from '../../../support/actions/checkRegisterActions';
import Util from '../../../support/common/utils';
import Dialog, {Prompt} from '../../../support/common/dialog';

//
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const headHeight = (Platform.OS === 'ios') ? 60 : 50;
//图片资源
const bgBack = require('../../../resources/imgs/ic_back.png');
const bgFamily = require('../../../resources/imgs/ic_family_bg.png');
const bgFamilyImageDefault = require('../../../resources/imgs/ico_family_default.png');
const defaultRecreationBg = require('../../../resources/imgs/bg_default_famliy_list.png');
const iconSearch = require('../../../resources/imgs/ic_search_group.png');
const groupPageSize = 6;
const defaultMessage = '1.玩家等级达到20级\n2.消耗500钻石';
const STATUS_DISBLOCK = '-5';
const STATUS_REMOVE_AT_REMOVE = '-4';
const STATUS_REMOVE_AT_QUIT = '-3';
const STATUS_REMOVE_AT_ACCECT = '-2';
const STATUS_BLOCK = '-1';
const STATUS_REQUIRED = '1';
const STATUS_REJECT = '2';
const STATUS_JOINED = '3';
const {
    pushRoute,
    popRoute
} = actions;
let groupList = new Array();
let progressDialog;
let promptCreateDialog;
let that;

class GroupList extends Component {
    static propTypes = {
        openPage: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        refreshFamilyGroupList: React.PropTypes.func,
        getMoreFamilyGroupList: React.PropTypes.func,
        updateFamilyStatus: React.PropTypes.func,
        familyGroupDate: React.PropTypes.object,
        isRefreshing: React.PropTypes.bool,
        configData: React.PropTypes.object,
        updateConfig: React.PropTypes.func
    }

    constructor(props) {
        super(props);
        that = this;
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(groupList),
            showSearchDialog: false,
            groupId: '',
            isFinishing: false
        };
    }

    componentDidMount() {
        //页面渲染完毕后定义加载view
        progressDialog = this.refs.progressDialog;
        promptCreateDialog = this.refs.promptCreate;
        //监听网络
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.onHandleMethod);
    }

    componentWillMount() {
        InteractionManager.runAfterInteractions(() => {
            this.props.refreshFamilyGroupList(0, types.FAMILY_GROUP_DATA);
        });
        //检查config
        if (!this.props.configData) {
            this.props.updateConfig();
        }
    }

    componentWillUnmount() {
        //移除监听
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.onHandleMethod);
        that.fetchMoreTimer && clearTimeout(that.fetchMoreTimer);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (!_.isEqual(this.props.familyGroupDate, nextProps.data));
    }

    render() {
        let groupTotal = groupList.length;
        if (this.props.familyGroupDate) {
            if (this.props.familyGroupDate.groups) {
                groupList = this.props.familyGroupDate.groups;
            }
            if (this.props.familyGroupDate.total) {
                groupTotal = this.props.familyGroupDate.total;
            }
        }
        let familyConfig = this.getCreateFamilyConfig();
        let createFamilyMessage = defaultMessage;
        if (familyConfig) {
            if (familyConfig.limitation) {
                if (familyConfig.limitation.message) {
                    createFamilyMessage = familyConfig.limitation.message;
                }
            }
        }
        return (
            <View style={{backgroundColor:Color.colorWhite,flex:1}}>
                {this.headerView()}
                <View style={{flex:5}}>
                    {that._defaultView()}
                    <ListView dataSource={this.state.dataSource.cloneWithRows(groupList)}
                              ref="listView"
                              renderRow={this.renderRow}
                              initialListSize={1}
                              removeClippedSubviews={true}
                              pageSize={3}
                              onEndReachedThreshold={groupPageSize}
                              enableEmptySections={true}
                              refreshControl={
                    <RefreshControl
                    refreshing={this.props.isRefreshing}
                    onRefresh={this._onRefresh}
                    colors={[Color.list_loading_color]} />
                    }/>
                </View>
                <View flexDirection='row' style={{flex:1,justifyContent:'center'}}>
                    {groupTotal > 10 ? this.moreFamilyView() : null}
                    {this.createFamilyView()}
                </View>
                {this.searchGroupView()}
                <Loading ref="progressDialog" loadingTitle={Language().request_loading}/>
                {Dialog.inject()}
                <Prompt ref='promptCreate' message='' title='' posText='' negText=''
                        contentStyle={{backgroundColor:(Color.colorWhite),width:ScreenWidth-80,paddingBottom: 0,paddingLeft:10,paddingRight:10,maxHeight:ScreenHeight,borderRadius:8}}
                        dialogStyle={{backgroundColor:'rgba(0, 0, 0, 0.1)'}}
                        buttonBarStyle={{height:0,backgroundColor:Color.transparent}}>
                    <Text
                        style={{alignSelf:'center',padding:10,fontSize:17,fontWeight:'400',textAlign:'center',color:Color.message_content_color}}>{Language().create_conditions}</Text>
                    <Text
                        style={{padding:10,fontSize:16,fontWeight:'400',color:Color.message_content_color}}>{createFamilyMessage}</Text>
                    <View flexDirection='row' style={{flex:1,justifyContent:'center',marginBottom:20,marginTop:10}}>
                        {this.cancelButton()}
                        {this.confirmButton()}
                    </View>
                </Prompt>
            </View>
        );
    }

    /**
     * header title view
     * @returns {XML}
     */
    headerView() {
        return (<CardItem
            style={{backgroundColor:(Color.headColor),height:headHeight,paddingTop:headPaddingTop,paddingBottom:0,paddingLeft:5,paddingRight:5}}>
            <Left style={{flex:1}}>
                <Button transparent onPress={this.onFinishPage.bind(this)}
                        style={{width:40,padding:0,justifyContent:'center'}}>
                    <Image source={bgBack} style={styles.bgBack}></Image>
                </Button>
            </Left>
            <Body style={{alignSelf:'center',flex:3}}>
            <Text style={{color:Color.colorWhite,fontSize: 18,alignSelf:'center'}}>{Language().group_list}</Text>
            </Body>
            <Right style={{flex:1}}>
                <Button transparent onPress={this.searchGroup.bind(this)}
                        style={{width:40,padding:0,justifyContent:'center'}}>
                    <Image source={iconSearch} style={styles.bgBack}></Image>
                </Button>
            </Right>
        </CardItem>);
    }

    /**
     * 默认背景
     * @param height
     * @returns {*}
     * @private
     */
    _defaultView() {
        return groupList.length == 0 ? (<View
                style={{justifyContent: 'center',backgroundColor:Color.transparent,position: 'absolute',flex:1,left:0,top:0,right:0,bottom:0}}>
                <Image style={styles.defaultBg} source={defaultRecreationBg}/>
            </View>) : null;
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     */
    renderRow(item, sectionId, rowId) {
        let member_count = 0;
        let max_members = 50;
        let require_status = 0;
        let familyLevel = Language().family;
        let group_id = '';
        if (item) {
            if (item.member_count) {
                member_count = item.member_count;
            }
            if (item.max_members) {
                max_members = item.max_members;
            }
            if (item.require_statsu) {
                require_status = item.require_statsu;
            }
            if (item.level) {
                familyLevel = item.level;
            }
            if (item._id) {
                group_id = item._id;
            }
        }
        return (<View>
            <View flexDirection='row' style={{overflow:'hidden'}}>
                <View style={{flex:2.5,justifyContent:'center'}}>
                    <Image style={styles.bgFamilyStyle} source={bgFamily}/>
                    <View style={styles.familyImageView}>
                        <CachedImage style={styles.bgFamilyImage}
                                     type='family'
                                     source={(item.image==''||item.image==null)?bgFamilyImageDefault:({uri:item.image})}
                                     defaultSource={bgFamilyImageDefault}/>
                    </View>
                    <View style={styles.familyLevelView}>
                        <Text numberOfLines={1} style={styles.familyText}>{familyLevel}</Text>
                    </View>
                </View>
                <View style={{flex:4.3,justifyContent:'center'}}>
                    <View flexDirection='row'>
                        <Text numberOfLines={1}
                              style={{fontSize:15,color:'#333333',fontWeight:'500',flex:3}}>{item.name}</Text>
                        <View
                            style={{borderRadius:6,backgroundColor:'#afe4ff',justifyContent:'center',height:13,flex:1.5,alignSelf:'flex-end'}}>
                            <Text
                                style={{fontSize:12,color:'#370191',paddingLeft:5,paddingRight:5,alignSelf:'center',backgroundColor:Color.transparent}}>{member_count}/{max_members}</Text>
                        </View>
                    </View>
                    <Text numberOfLines={1}
                          style={{fontSize:14,color:'#4d4d4d',marginTop:5}}>{Language().patriarch}：{item.own_name}</Text>
                </View>
                <View style={{flex:2.5,justifyContent:'center'}}>
                    {that.selectDetailFamilyView(item._id)}
                    {/*{(require_status == STATUS_REQUIRED || require_status == STATUS_JOINED) ? that.applyedView() : that.applyFamilyView(item._id, rowId)}*/}
                </View>
            </View>
            <View style={styles.rowSeparator}/>
        </View>);
    }

    /**
     * 查询家族
     */
    searchGroup() {
        this.setState({
            showSearchDialog: true,
            groupId: ''
        });
    }

    /**
     * 进入家族详情
     * groupId
     */
    openFamilyDetail(groupId) {
        if (groupId) {
            let nativeData = {
                action: Constant.ACTION_SHOW_FAMILY_INFO,
                params: {group_id: groupId},
                type: Constant.RN_NATIVE
            }
            rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
        }
    }

    /**
     *
     * @returns {XML}
     */
    applyedView() {
        return (<View style={styles.applyButtonView}>
            <Text
                style={[styles.applyText,{color:'#f47103'}]}>{Language().applied}</Text>
        </View>);
    }

    /**
     *
     * @returns {XML}
     */
    selectDetailFamilyView(group_id) {
        return (<View style={styles.buttonView}>
            <View style={styles.buttonChildView}>
                <TouchableOpacity
                    style={{backgroundColor:(Color.transparent),justifyContent:'center',flex:1}}
                    activeOpacity={0.6}
                    onPress={()=>that.openFamilyDetail(group_id)}>
                    <Text
                        style={[styles.applyText,{fontSize:13}]}>{Language().select_group_detail}</Text>
                </TouchableOpacity>
            </View>
        </View>);
    }

    /**
     *
     * @returns {XML}
     */
    applyFamilyView(id, rowId) {
        return (<View style={styles.buttonView}>
            <View style={styles.buttonChildView}>
                <TouchableOpacity
                    style={{backgroundColor:(Color.transparent),justifyContent:'center',flex:1}}
                    activeOpacity={0.6}
                    onPress={()=>that.applyFamily(id,rowId)}>
                    <Text
                        style={styles.applyText}>{Language().apply}</Text>
                </TouchableOpacity>
            </View>
        </View>);
    }

    /**
     *
     * @returns {XML}
     */
    createFamilyView() {
        return (<View style={[styles.buttonView, {height: 38,width: 100,marginLeft:15,marginRight:15}]}>
            <View style={styles.buttonChildView}>
                <TouchableOpacity
                    style={{backgroundColor:(Color.transparent),justifyContent:'center',flex:1}}
                    activeOpacity={0.6}
                    onPress={this.createFamily.bind(this)}>
                    <Text
                        style={styles.applyText}>{Language().create_group}</Text>
                </TouchableOpacity>
            </View>
        </View>);
    }

    /**
     *
     * @returns {XML}
     */
    moreFamilyView() {
        return (<View style={[styles.buttonView, {height: 38,width: 100,marginLeft:15,marginRight:15}]}>
            <View style={styles.buttonChildView}>
                <TouchableOpacity
                    style={{backgroundColor:(Color.transparent),justifyContent:'center',flex:1}}
                    activeOpacity={0.6}
                    onPress={this.applyMoreFamily.bind(this)}>
                    <Text
                        style={styles.applyText}>{Language().refresh_more}</Text>
                </TouchableOpacity>
            </View>
        </View>);
    }

    /**
     *
     * @returns {XML}
     */
    confirmButton() {
        return (<View style={[styles.buttonView, {height: 35,width: 100,marginLeft:15,marginRight:15}]}>
            <View style={styles.buttonChildView}>
                <TouchableOpacity
                    style={{backgroundColor:(Color.transparent),justifyContent:'center',flex:1}}
                    activeOpacity={0.6}
                    onPress={this.confirmEnterCreateFamily.bind(this)}>
                    <Text
                        style={[styles.applyText]}>{Language().confirm}</Text>
                </TouchableOpacity>
            </View>
        </View>);
    }

    /**
     *
     * @returns {XML}
     */
    cancelButton() {
        return (<View
            style={[styles.buttonView, {height: 35,width: 100,marginLeft:15,marginRight:15,backgroundColor:'#808080'}]}>
            <View style={[styles.buttonChildView,{backgroundColor:'#b2b2b2'}]}>
                <TouchableOpacity
                    style={{backgroundColor:(Color.transparent),justifyContent:'center',flex:1}}
                    activeOpacity={0.6}
                    onPress={this.doCancel.bind(this)}>
                    <Text
                        style={[styles.applyText,{color:Color.colorWhite}]}>{Language().cancel}</Text>
                </TouchableOpacity>
            </View>
        </View>);
    }

    /**
     *进入创建家族
     */
    confirmEnterCreateFamily() {
        this.doCancel();
        try {
            this.props.openPage(SCENES.SCENE_CREATE_GROUP, 'global');
        } catch (e) {
            console.log(e)
        }
    }

    /**
     *取消查询
     */
    doCancel() {
        if (promptCreateDialog) {
            promptCreateDialog.hide();
        }
    }

    /**
     *
     * @private
     */
    _onRefresh() {
        InteractionManager.runAfterInteractions(() => {
            that.props.refreshFamilyGroupList(0, types.FAMILY_GROUP_DATA);
        });
    }

    /**
     *
     * @param id
     */
    applyFamily(id, rowID) {
        netWorkTool.checkNetworkState((isConnected) => {
            that.onShowProgressDialog();
            if (isConnected) {
                let url = apiDefines.APPLY_JOIN_FAMILY + id;
                Util.get(url, (code, message, data) => {
                    that.onHideProgressDialog();
                    if (code == 1000) {
                        that.props.updateFamilyStatus(id, rowID);
                    } else {
                        Toast.show(message);
                    }
                }, (failed) => {
                    that.onHideProgressDialog();
                    Toast.show(netWorkTool.NOT_NETWORK);
                });
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     *换一批
     */
    applyMoreFamily() {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                that.onShowProgressDialog(Language().loading_now);
                let total = 0;
                let skip = 0;
                let page = 1;
                let pages = 0;
                let limit = 10;
                if (this.props.familyGroupDate) {
                    if (this.props.familyGroupDate.total) {
                        total = this.props.familyGroupDate.total;
                    }
                    if (this.props.familyGroupDate.skip) {
                        skip = this.props.familyGroupDate.skip;
                    }
                    if (this.props.familyGroupDate.limit) {
                        limit = this.props.familyGroupDate.limit;
                    }
                    if (skip) {
                        page = Math.ceil(skip / limit) + 1;
                    }
                    if (total) {
                        pages = Math.ceil(total / limit);
                    }
                    if (page < pages) {
                        InteractionManager.runAfterInteractions(() => {
                            that.props.getMoreFamilyGroupList(skip + limit, types.FAMILY_GROUP_DATA_MORE);
                        });
                    } else if (page = pages) {
                        InteractionManager.runAfterInteractions(() => {
                            that.props.refreshFamilyGroupList(0, types.FAMILY_GROUP_DATA);
                        });
                    }
                    InteractionManager.runAfterInteractions(() => {
                        //点击换一批，新列表滑动到最顶端
                        if (that.refs.listView) {
                            that.refs.listView.scrollTo(0, 0);
                        }
                        that.fetchMoreTimer = setTimeout(() => {
                            that.onHideProgressDialog();
                        }, 1000);
                    });
                }
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        })
    }

    /**
     * 获取创建家族的config
     * @returns {*}
     */
    getCreateFamilyConfig() {
        let config = null;
        if (that.props.configData) {
            if (that.props.configData.create_family) {
                config = that.props.configData.create_family;
            }
        }
        return config;
    }

    /**
     *
     */
    createFamily() {
        if (promptCreateDialog) {
            promptCreateDialog.show();
        } else {
            this.props.openPage(SCENES.SCENE_CREATE_GROUP, 'global')
        }
    }

    /**
     * 网络变化回调
     * @param isConnected
     */
    onHandleMethod(isConnected) {
        console.log('create group netWork:' + isConnected);
    }

    /**
     * 关闭页面
     */
    onFinishPage() {
        if (!this.state.isFinishing) {
            this.state.isFinishing = true
            this.props.finishPage('global');
        }
        this.finishPageTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog(message) {
        if (progressDialog != null) {
            progressDialog.show({loadingTitle: message});
        }
    }

    /**
     * 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    /**
     * 搜索家族列表
     */
    searchGroupView() {
        return this.state.showSearchDialog ? ( <View style={styles.container}>
                <TouchableWithoutFeedback>
                    <View style={styles.searchGroupContent}>
                        <Text style={styles.searchTitle}>{Language().search_group}</Text>
                        <View style={styles.inputView}>
                            <TextInput
                                ref='inputGroupId'
                                placeholder={Language().placeholder_group_id}
                                placeholderTextColor={Color.prompt_create_group_text_color}
                                underlineColorAndroid='transparent'
                                style={styles.inputText}
                                autoFocus={true}
                                onChangeText={(text)=>this._changeChangeGroupId(text)}
                                keyboardType='numeric'
                                value={this.state.groupId}/>
                        </View>
                        <View flexDirection='row' style={{justifyContent:'center',margin:10}}>
                            <View style={[styles.buttonView, {height: 30,width: 80,marginRight:25}]}>
                                <View style={styles.buttonChildView}>
                                    <TouchableOpacity
                                        style={{backgroundColor:(Color.transparent),justifyContent:'center',flex:1}}
                                        activeOpacity={0.6}
                                        onPress={this.closeModal.bind(this)}>
                                        <Text style={[styles.applyText,{fontWeight: '500'}]}>{Language().cancel}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={[styles.buttonView, {height: 30,width: 80,marginLeft:25}]}>
                                <View style={styles.buttonChildView}>
                                    <TouchableOpacity activeOpacity={0.6} onPress={this.confirmModal.bind(this)}
                                                      style={{backgroundColor:(Color.transparent),justifyContent:'center',flex:1}}>
                                        <Text style={[styles.applyText,{fontWeight: '500'}]}>{Language().confirm}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>) : null;
    }

    /**
     *关闭搜索
     */
    closeModal() {
        this.setState({showSearchDialog: false});
    }

    /**
     *确认搜索
     */
    confirmModal() {
        let {groupId} = this.state;
        if (this.refs.inputGroupId) {
            this.refs.inputGroupId.blur();
        }
        if (!groupId.length) {
            Toast.show(Language().placeholder_group_id);
            return;
        }
        this.onShowProgressDialog(Language().search_loading);
        this.requestSearchGroup(groupId);
    }

    /**
     * 搜索家族
     */
    requestSearchGroup(groupid) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                let url = apiDefines.GROUP_INFO_FROM_ID + groupid;
                Util.get(url, (code, message, data) => {
                    this.onHideProgressDialog();
                    if (code == 1000) {
                        this.closeModal();
                        let group_id = '';
                        if (data && data.group) {
                            group_id = data.group._id;
                        }
                        this.openFamilyDetail(group_id);
                    } else {
                        Toast.show(message);
                    }
                }, (failed) => {
                    this.onHideProgressDialog();
                    Toast.show(netWorkTool.NOT_NETWORK);
                });
            } else {
                this.onHideProgressDialog();
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     *
     * @param text
     * @private
     */
    _changeChangeGroupId(text) {
        let format = /^[0-9]*$/;
        if (format.test(text)) {
            this.setState({
                groupId: text
            });
        }
    }
}

const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5,
        alignSelf: 'center'
    },
    bgFamilyStyle: {
        width: 80,
        height: 65,
        resizeMode: 'contain',
        alignSelf: 'center', padding: 5, marginTop: 5, marginBottom: 5
    },
    bgFamilyImage: {
        width: 42,
        height: 42,
        borderRadius: 20,
        alignSelf: 'center'
    },
    familyLevelView: {
        position: 'absolute',
        left: 0, bottom: (Platform.OS === 'ios') ? 10 : 9, flex: 1, right: 0
    },
    familyImageView: {
        position: 'absolute',
        left: 0, top: 10, right: 0, justifyContent: 'center'
    },
    familyText: {
        fontSize: (Platform.OS === 'ios') ? 12 : 11,
        alignSelf: 'center',
        color: Color.colorWhite, backgroundColor: Color.transparent
    },
    rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: Color.rowSeparator_line_color,
    },
    buttonView: {
        borderRadius: 7,
        backgroundColor: '#ffac03',
        paddingBottom: 3,
        height: 27,
        width: 62,
        alignSelf: 'center',
    },
    applyButtonView: {
        borderRadius: 7,
        height: 27,
        width: 62,
        alignSelf: 'center',
        justifyContent: 'center',
        borderColor: '#f47103',
        borderWidth: 1.5
    },
    applyText: {
        fontSize: 15, color: '#894500', fontWeight: 'bold', alignSelf: 'center'
    },
    buttonChildView: {borderRadius: 7, backgroundColor: '#ffd249', flex: 1},
    defaultBg: {
        alignSelf: 'center',
        width: ScreenWidth / 2,
        height: ScreenWidth / 2,
        resizeMode: 'contain',
    },
    container: {
        position: 'absolute',
        width: ScreenWidth,
        height: ScreenHeight,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
    },
    dialogModal: {
        position: 'absolute',
        backgroundColor: Color.transparent,
        width: ScreenWidth,
        height: ScreenHeight,
    },
    searchGroupContent: {
        backgroundColor: (Color.colorWhite),
        width: ScreenWidth - 100,
        maxHeight: ScreenHeight,
        borderRadius: 8,
        alignSelf: 'center',
        marginBottom: 100
    },
    inputView: {
        margin: 10,
        borderRadius: 3,
        borderColor: Color.record_text_color,
        borderWidth: 1,
        height: 30,
        marginLeft: 20,
        marginRight: 20
    },
    searchTitle: {alignSelf: 'center', padding: 5, fontSize: 16, marginTop: 5, color: '#474747', fontWeight: 'bold'},
    inputText: {fontSize: 14, padding: 5, height: 30, flex: 1}
});
const mapDispatchToProps = dispatch => ({
    openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
    finishPage: (key) => dispatch(popRoute(key)),
    refreshFamilyGroupList: (page, actionType) => dispatch(familyGroupList(page, actionType)),
    getMoreFamilyGroupList: (page, actionType) => dispatch(familyGroupList(page, actionType)),
    updateFamilyStatus: (id, rowId) => dispatch(updateFamilyStatus(id, rowId)),
    updateConfig: () => dispatch(checkOpenRegister()),
});
const mapStateToProps = state => ({
    familyGroupDate: state.familyGroupReducer.data,
    isRefreshing: state.familyGroupReducer.isRefreshing,
    configData: state.checkRegisterReducer.data,
});
export default connect(mapStateToProps, mapDispatchToProps)(GroupList);