/**
 * Created by wangxu on 2017/4/24.
 */
'use strict';
import React, {Component} from 'react';
import {
    Content,
    Form,
    Item,
    Input,
    Label,
    Button,
    CardItem,
    Left,
    Body,
    Right,
    Text,
    Spinner,
    Container
} from 'native-base';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {
    Image,
    StyleSheet,
    View,
    Platform,
    NativeModules,
    Dimensions,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from 'react-native';
import Color from '../../../resources/themColor';
import netWorkTool from '../../../support/common/netWorkTool';
import Toast from '../../../support/common/Toast';
import Dialog, {Prompt,} from '../../../support/common/dialog';
import Language from '../../../resources/language'
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import Util from '../../../support/common/utils';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import {userInfo} from '../../../support/actions/userInfoActions';
import * as Scenes from '../../../support/actions/scene'
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import * as WeChat from 'react-native-wechat';
import * as QQAPI from 'react-native-qq';
import * as Constant from '../../../support/common/constant';
import {getChatData} from '../../../support/actions/chatActions';
import {getFamilyChat} from '../../../support/actions/groupFamilyChatActions';
import {initFriendRealm} from '../../../support/actions/friendActions';
import {AccessToken, LoginManager} from 'react-native-fbsdk';
import {location} from '../../../support/actions/configActions';
import RealmManager from '../../../support/common/realmManager';
import CheckBox from '../../../support/common/CheckBox';

import Loading from '../../../support/common/Loading';
const LineLoginManager = require('react-native').NativeModules.RNLineLoginManager;

const {
    popRoute,
    pushRoute
} = actions;
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const headHeight = (Platform.OS === 'ios') ? 60 : 50;

const ScreenHeight = Dimensions.get('window').height;
const ScreenWidth = Dimensions.get('window').width;
const bgBack = require('../../../resources/imgs/ic_back.png');
const bgWeChat = require('../../../resources/imgs/icon_wechat_login.png');
const bgQQ = require('../../../resources/imgs/icon_qq_login.png');
const rightArrow = require('../../../resources/imgs/bg_arrow_right.png');
const bgLoginFaceBook = require('../../../resources/imgs/icon_facebook_login.png');
const bgLoginLine = require('../../../resources/imgs/icon_line.png');
const loginIconWH = (ScreenWidth - 320 > 0) ? 35 : 25
const loginIconMG = (ScreenWidth - 320 > 0) ? 10 : 5
let loginEnd = false;
let progressDialog;
class LoginOther extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        openPage: React.PropTypes.func,
        updateUserInfo: React.PropTypes.func,
        countryInfo: React.PropTypes.object,
        checkOpenRegisterData: React.PropTypes.object,
        getChatData: React.PropTypes.func,
        getFamilyChat: React.PropTypes.func,
        setLocation: React.PropTypes.func,
        initFriendRealm: React.PropTypes.func,

    }

    constructor(props) {
        super(props);
        this.state = {
            phone: '',
            password: '',
            countryCode: '886',
            disabledSelectCountryBt: false,
            isWXAppInstalled: false,
            isQQAppInstalled: false,
            isFaceBookAppInstalled: false,
            useragreement: true
        };
    }

    render() {
        let country = this.props.countryInfo;
        let countryName = Language().taiwan;
        let countryCode = '886';
        if (country != null) {
            if (country.n != null) {
                countryName = country.n;
            }
            if (country.c != null) {
                countryCode = country.c;
                this.state.countryCode = countryCode;
            }
        }
        let top = ScreenHeight - 630;
        if (this.props.checkOpenRegisterData != null && !this.props.checkOpenRegisterData.mobile_register) {
            top = ScreenHeight - 545;
        }
        if (ScreenHeight - 568 <= 0) {
            top = 0;
        }
        let promptView = (Platform.OS === 'ios') ? this._promptLoginQQWxView(top) : (
                <View flexDirection='row' style={{margin:5,marginTop:top,justifyContent:'center'}}>
                    <View style={{height:0.3,backgroundColor:(Color.about_text_color),flex:1,margin:10}}></View>
                    <Text
                        style={{fontSize:16,color:(Color.loginTextColor),marginLeft:10,marginRight:10}}>{Language().other_login}</Text>
                    <View style={{height:0.3,backgroundColor:(Color.about_text_color),flex:1,margin:10}}></View>
                </View>);
        let qqView = (Platform.OS === 'ios') ? this._loginFromQQView() : (
                <TouchableOpacity onPress={this.onLoginFromQQ.bind(this)}
                                  activeOpacity={0.6}
                                  style={{marginRight:loginIconMG,marginLeft:loginIconMG}}>
                    <Image source={bgQQ}
                           style={{width:loginIconWH,height:loginIconWH,alignSelf:'center',margin:5,resizeMode: 'contain',}}>
                    </Image>
                    <Text
                        style={{fontSize:12,color:(Color.loginTextColor),padding:5,alignSelf:'center'}}>{Language().qq_login}</Text>
                </TouchableOpacity>);
        let weChatView = (Platform.OS === 'ios') ? this._loginFromWeChatView() : (
                <TouchableOpacity style={{marginRight:loginIconMG,marginLeft:loginIconMG}}
                                  activeOpacity={0.6}
                                  onPress={this.onLoginFormWeChat.bind(this)}>
                    <Image source={bgWeChat}
                           style={{width:loginIconWH,height:loginIconWH,alignSelf:'center',margin:5,resizeMode: 'contain',}}>
                    </Image>
                    <Text
                        style={{fontSize:12,color:(Color.loginTextColor),padding:5,alignSelf:'center'}}>{Language().wx_login}</Text>
                </TouchableOpacity>);
        let facebookView = (Platform.OS === 'ios') ? this._loginFromFacebookView() : (
                <TouchableOpacity style={{marginRight:loginIconMG,marginLeft:loginIconMG}}
                                  activeOpacity={0.6}
                                  onPress={this.onLoginFromFacebook.bind(this)}>
                    <Image source={bgLoginFaceBook}
                           style={{width:loginIconWH,height:loginIconWH,alignSelf:'center',margin:5,resizeMode: 'contain',}}>
                    </Image>
                    <Text
                        style={{fontSize:12,color:(Color.loginTextColor),padding:5,alignSelf:'center'}}>{Language().facebook}</Text>
                </TouchableOpacity>);
        if (this.props.checkOpenRegisterData != null && !this.props.checkOpenRegisterData.facebook) {
            facebookView = null;
        }
        return (
            <Container style={{backgroundColor:'#fff'}}>
                <StatusBar backgroundColor={Color.headColor} barStyle='light-content' hidden={false}/>
                <CardItem
                    style={{backgroundColor:(Color.headColor),height:headHeight,paddingTop:headPaddingTop,paddingBottom:0}}>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={this.onFinishPage.bind(this)}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf:'center',flex:3}}>
                    <Text style={{color: '#fff',fontSize: 18,alignSelf:'center'}}>{Language().phone_login}</Text>
                    </Body>
                    <Right style={{flex:1}}/>
                </CardItem>
                <ScrollView style={{backgroundColor:Color.colorWhite}} showsVerticalScrollIndicator={false}>
                    <Form style={{marginTop:10}}>
                        <View flexDirection='row'
                              style={{marginLeft:15,marginRight:15,marginBottom:15,borderWidth: 1, borderColor: (Color.loginBorderColor), borderRadius: 3,height:50}}>
                            <Button transparent style={{margin:0,padding:0,flex:1}}
                                    onPress={this.onSelectCountry.bind(this)}
                                    disabled={this.state.disabledSelectCountryBt}>
                                <Text
                                    style={{fontSize:16,color:(Color.blackColor),paddingLeft:10,flex:1}}>{Language().country_region}</Text>
                                <View flexDirection='row'>
                                    <Text
                                        style={{fontSize:16,color:(Color.blackColor)}}>{countryName}</Text>
                                    <Image source={rightArrow}
                                           style={styles.rightArrow}></Image>
                                </View>
                            </Button>
                        </View>
                        <View flexDirection='row'
                              style={{marginLeft:15,marginRight:15,borderWidth: 1, borderColor: (Color.loginBorderColor), borderRadius: 3,height:50}}>
                            <Text
                                style={{textAlign:'center',color:(Color.blackColor),alignSelf:'center',paddingLeft:10}}>+{countryCode}</Text>
                            <Input keyboardType='numeric' onChangeText={(text)=>this.onChangePhone(text)}
                                   placeholder={Language().placeholder_phone}
                                   placeholderTextColor={Color.loginTextColor}
                                   style={{paddingLeft:10,flex:1,fontSize:16}}
                                   value={this.state.phone} maxLength={11}/>
                        </View>
                        <Item style={{margin:15,borderColor:(Color.transparent)}}>
                            <Input password={true} secureTextEntry={true}
                                   placeholder={Language().placeholder_password}
                                   placeholderTextColor={Color.loginTextColor}
                                   style={{borderWidth: 1, borderColor:(Color.loginBorderColor), borderRadius: 3,paddingLeft:10}}
                                   onChangeText={(text)=>this.onChangePassword(text)}/>
                        </Item>
                    </Form>
                    <TouchableOpacity
                        style={{borderRadius: 3, marginRight: 15, marginLeft: 15, marginTop: 15,height:45,backgroundColor:(Color.phoneLoginButColor),justifyContent:'center'}}
                        activeOpacity={0.6}
                        onPress={this.onLogin.bind(this)}>
                        <Text style={{color:'#fff',fontSize:16,padding:5,alignSelf:'center'}}>{Language().login}</Text>
                    </TouchableOpacity>
                    {this._checkOpenRegisterView()}
                    <View flexDirection='row' style={{margin:15}}>
                        <Right>
                            <TouchableOpacity onPress={this.onForgetPassword.bind(this)}
                                              activeOpacity={0.6}
                                              style={{justifyContent:'center'}}>
                                <Text
                                    style={{color:(Color.phoneLoginButColor),fontSize:16,padding:5,alignSelf:'center'}}>{Language().forgetPassword}?</Text>
                            </TouchableOpacity>
                        </Right>
                    </View>
                    {promptView}
                    <View flexDirection='row' style={{margin:10,alignSelf:'center'}}>
                        {/*{facebookView}*/}
                        {weChatView}
                        {qqView}
                    </View>
                    <View>
                        <View flexDirection='row'
                              style={{justifyContent:'center',marginLeft:10,marginRight:10,marginTop:10}}>
                            <CheckBox checked={this.state.useragreement} onChange={this.checkAgreement.bind(this)}
                                      label=''
                                      checkboxStyle={{width:15,height:15,marginTop:2}}/>
                            <TouchableOpacity onPress={this.readUserAgreement.bind(this)}
                                              activeOpacity={0.6} style={{marginLeft:2,justifyContent:'center'}}>
                                <View flexDirection='row' style={{justifyContent:'center',alignSelf:'center'}}>
                                    <Text
                                        style={{color:'#505050',fontSize:13,alignSelf:'center',padding:1}}>{Language().agree_and_read}</Text>
                                    <Text
                                        style={{color:'#eb6c63',fontSize:13,alignSelf:'center',padding:1}}>{'《' + Language().user_agreement + '》'}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text
                            style={{color:'#a9a9a9',fontSize:9,alignSelf:'center',paddingTop:2}}>{Language().explain_agreement}</Text>
                    </View>
                </ScrollView>
                <Loading ref="progressDialog" loadingTitle={Language().login_loading}/>
            </Container>
        );
    }

    /**
     * 用户协议
     */
    readUserAgreement() {
        try {
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    this.props.openPage(Scenes.SCENE_USER_AGREEMENT, 'global');
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        } catch (e) {
            console.log(e)
        }
    }

    /**
     *
     */
    checkAgreement() {
        this.setState({
            useragreement: !this.state.useragreement
        });
    }

    closeProgress() {

    }

    /**
     * 选取国家
     */
    onSelectCountry() {
        if (!this.state.disabledSelectCountryBt) {
            this.props.openPage(Scenes.SCENE_SELECT_COUNTRY, 'global');
        }
        this.state.disabledSelectCountryBt = true;
        this.selectCountryTimer = setTimeout(() => {
            this.state.disabledSelectCountryBt = false;
        }, 1000);
    }

    /**
     * 注册账号
     */
    onRegister() {
        try {
            this.props.openPage(Scenes.SCENE_REGISTER, 'global');
        } catch (e) {
            console.log(e)
        }
    }

    /**
     * 忘记密码
     */
    onForgetPassword() {
        if (this.state.useragreement) {
            this.props.openPage(Scenes.SCENE_CHANGE_PASSWORD, 'global');
        } else {
            Toast.show(Language().agreement_prompt);
        }
    }

    /**
     * 手机号
     * @param text
     */
    onChangePhone(text) {
        // if (/^[\d]+$/.test(text)) {
        //     this.setState({
        //         phone: text
        //     });
        // } else {
        //     if (/^[\d{1}]$/.test(this.state.phone)) {
        //         this.setState({
        //             phone: ''
        //         });
        //     }
        // }
        //手机号客户端不做验证，交由服务器端，订规则进行验证
        this.setState({
            phone: text
        });
    }

    /**
     * 密码
     * @param text
     */
    onChangePassword(text) {
        this.state.password = text;
    }

    componentDidMount() {
        //页面渲染完毕后定义加载view
        progressDialog = this.refs.progressDialog;
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.selectCountryTimer && clearTimeout(this.selectCountryTimer);
        this.qqTimer && clearTimeout(this.qqTimer);
        this.wechatTimer && clearTimeout(this.wechatTimer);
        this.lineTimer && clearTimeout(this.lineTimer);
    }

    handleMethod(isConnected) {

    }

    componentWillMount() {
        if (Platform.OS === 'ios') {
            this.checkWeChatQQ();
            this.setState({
                isFaceBookAppInstalled: true
            });
        } else {
            if (NativeModules.NativeJSModule.checkApkExist) {
                NativeModules.NativeJSModule.checkApkExist('', (facebookInstalled) => {
                    this.setState({
                        isFaceBookAppInstalled: facebookInstalled
                    });
                });
            }
        }
    }

    /**
     * line登录view
     * @returns {XML}
     * @private
     */
    _loginFromLineView() {
        return (
            <TouchableOpacity style={{marginRight:loginIconMG,marginLeft:loginIconMG}}
                              activeOpacity={0.6}
                              onPress={this.onLoginFromLine.bind(this)}>
                <Image source={bgLoginLine}
                       style={{width:loginIconWH,height:loginIconWH,alignSelf:'center',margin:5,resizeMode: 'contain',}}>
                </Image>
                <Text
                    style={{fontSize:12,color:(Color.loginTextColor),padding:5,alignSelf:'center'}}>{Language().line}</Text>
            </TouchableOpacity>
        );
    }

    /**
     *
     * @returns {*}
     * @private
     */
    _loginFromFacebookView() {
        return (
            this.state.isFaceBookAppInstalled ? (
                    <TouchableOpacity style={{marginRight:loginIconMG,marginLeft:loginIconMG}}
                                      activeOpacity={0.6}
                                      onPress={this.onLoginFromFacebook.bind(this)}>
                        <Image source={bgLoginFaceBook}
                               style={{width:loginIconWH,height:loginIconWH,alignSelf:'center',margin:5,resizeMode: 'contain',}}>
                        </Image>
                        <Text
                            style={{fontSize:12,color:(Color.loginTextColor),padding:5,alignSelf:'center'}}>{Language().facebook}</Text>
                    </TouchableOpacity>) : (null)
        );
    }

    /**
     *
     * @returns {*}
     * @private
     */
    _loginFromQQView() {
        return (
            this.state.isQQAppInstalled ? (
                    <TouchableOpacity style={{marginRight:loginIconMG,marginLeft:loginIconMG}}
                                      activeOpacity={0.6}
                                      onPress={this.onLoginFromQQ.bind(this)}>
                        <Image source={bgQQ}
                               style={{width:loginIconWH,height:loginIconWH,alignSelf:'center',margin:5,resizeMode: 'contain',}}>
                        </Image>
                        <Text
                            style={{fontSize:12,color:(Color.loginTextColor),padding:5,alignSelf:'center'}}>{Language().qq_login}</Text>
                    </TouchableOpacity>) : (null)
        );
    }

    /**
     *
     * @returns {*}
     * @private
     */
    _loginFromWeChatView() {
        return (
            this.state.isWXAppInstalled ? (
                    <TouchableOpacity style={{marginRight:loginIconMG,marginLeft:loginIconMG}}
                                      activeOpacity={0.6}
                                      onPress={this.onLoginFormWeChat.bind(this)}>
                        <Image source={bgWeChat}
                               style={{width:loginIconWH,height:loginIconWH,alignSelf:'center',margin:5,resizeMode: 'contain',}}>
                        </Image>
                        <Text
                            style={{fontSize:12,color:(Color.loginTextColor),padding:5,alignSelf:'center'}}>{Language().wx_login}</Text>
                    </TouchableOpacity>) : (null)
        );
    }

    /**
     *
     * @param top
     * @returns {*}
     * @private
     */
    _promptLoginQQWxView(top) {
        //如果支持
        return (
            (this.state.isWXAppInstalled || this.state.isQQAppInstalled || this.state.isFaceBookAppInstalled) ? (
                    <View flexDirection='row' style={{margin:5,marginTop:top,justifyContent:'center'}}>
                        <View style={{backgroundColor:(Color.about_text_color),flex:1,margin:10,height:0.3}}></View>
                        <Text
                            style={{fontSize:16,color:(Color.loginTextColor),marginLeft:10,marginRight:10}}>{Language().other_login}</Text>
                        <View style={{height:0.3,backgroundColor:(Color.about_text_color),flex:1,margin:10}}></View>
                    </View>) : (null)
        );
    }

    _checkOpenRegisterView() {
        let registerView = (
            <TouchableOpacity
                style={{borderRadius: 3, marginRight: 15, marginLeft: 15, marginTop: 15,height:45,borderColor:(Color.phoneLoginButColor),justifyContent:'center',borderWidth:1}}
                activeOpacity={0.6}
                onPress={this.onRegister.bind(this)}>
                <Text
                    style={{color:(Color.phoneLoginButColor),fontSize:16,padding:5,alignSelf:'center'}}>{Language().register}</Text>
            </TouchableOpacity>);
        if (this.props.checkOpenRegisterData != null && !this.props.checkOpenRegisterData.mobile_register) {
            registerView = null;
        }

        return registerView;
    }

    /**
     * 检测手机是否安装的QQ或是微信
     */
    checkWeChatQQ() {
        WeChat.isWXAppInstalled().then((resp) => {
            if (resp) {
                this.setState({
                    isWXAppInstalled: true
                });
            } else {
                this.setState({
                    isWXAppInstalled: false
                });
            }
        }, (error) => {
            this.setState({
                isWXAppInstalled: false
            });
        });
        QQAPI.isQQInstalled().then((resp) => {
            if (resp) {
                this.setState({
                    isQQAppInstalled: true
                });
            } else {
                this.setState({
                    isQQAppInstalled: false
                });
            }
        }, (error) => {
            this.setState({
                isQQAppInstalled: false
            });
        });
    }

    /**
     * 手机号登录
     */
    onLogin() {
        let {phone, password, useragreement} = this.state;
        if (!useragreement) {
            Toast.show(Language().agreement_prompt);
            return;
        }
        if (!phone.length) {
            Toast.show(Language().placeholder_phone);
            return;
        }
        if (!password.length) {
            Toast.show(Language().placeholder_password);
            return;
        }
        let newPhone = "+" + this.state.countryCode + phone;
        try {
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    this.onShowProgressDialog();
                    let url = apiDefines.LOGIN_FROM_PHONE + newPhone + apiDefines.PASSWORD + password;
                    Util.get(url, (code, message, data) => {
                        this.onHideProgressDialog();
                        if (code == 1000) {
                            if (data) {
                                let userInfo = JSON.stringify(data);
                                this.props.updateUserInfo(data);
                                AsyncStorageTool.saveUserInfo(userInfo);
                                if (data != null) {
                                    RealmManager.initRealmManager(data.id, (callback) => {
                                        if (callback) {
                                            this.props.getChatData(data.id, callback);
                                            this.props.getFamilyChat(data.id, callback);
                                            this.props.initFriendRealm(callback);
                                        }
                                    });
                                    rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token);
                                    rnRoNativeUtils.onSetLocation(data.lc);
                                    this.configLocation(data.lc);
                                }
                                this.onFinishPage();
                                try {
                                    this.props.openPage(Scenes.SCENE_APP_MAIN, 'global');
                                } catch (e) {
                                    console.log(e)
                                }
                            }
                        } else {
                            Toast.show(message);
                        }
                    }, (error) => {
                        this.onHideProgressDialog();
                        Toast.show(Language().not_network);
                    });
                    // let url = apiDefines.LOGIN_FROM_PHONE_POST;
                    // let data = {
                    //     phone: newPhone,
                    //     password: password
                    // };
                    // Util.post(url, data, (code, message, data) => {
                    //     this.onHideProgressDialog();
                    //     if (code == 1000) {
                    //         let userInfo = JSON.stringify(data);
                    //         this.props.updateUserInfo(data);
                    //         AsyncStorageTool.saveUserInfo(userInfo);
                    //         if (data != null) {
                    //             rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token)
                    //         }
                    //         this.onFinishPage();
                    //         this.props.openPage(Scenes.SCENE_APP_MAIN, 'global');
                    //     } else {
                    //         Toast.show(message);
                    //     }
                    // }, (error) => {
                    //     this.onHideProgressDialog();
                    //     Toast.show('操作失败，请检查您的网络连接');
                    // });
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        } catch (e) {
            console.log('login from phone error:' + e);
        }
        //统计
        rnRoNativeUtils.onStatistics(Constant.PHONE_LOGIN);
    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     * 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    /**
     * 销毁页面
     */
    onFinishPage() {
        this.props.finishPage('global');
    }

    /**
     * 1--申请line授权
     * 2--授权成功后登录服务器
     */
    onLoginFromLine() {
        try {
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    this.lineTimer = setTimeout(() => {
                        loginEnd = false;
                    }, 3000);
                    if (!loginEnd) {
                        loginEnd = true;
                        LineLoginManager.login()
                            .then((user) => {
                                //Toast.show(JSON.stringify(user));
                                console.log(JSON.stringify(user));
                                loginEnd = false;
                                LineLoginManager.logout();
                            })
                            .catch((err) => {
                                loginEnd = false;
                                //Toast.show(err.message);
                                console.log(JSON.stringify(err));
                            })
                    } else {
                        Toast.show(Language().login_loading);
                    }
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        } catch (e) {
            console.log('login line error:' + e);
        }
    }

    /**
     * 微信登录
     */
    onLoginFormWeChat() {
        if (this.state.useragreement) {
            try {
                netWorkTool.checkNetworkState((isConnected) => {
                    if (isConnected) {
                        this.wechatTimer = setTimeout(() => {
                            loginEnd = false;
                        }, 3000);
                        //检测是否安装微信
                        WeChat.isWXAppInstalled().then((resp) => {
                            if (resp) {
                                //微信登录
                                if (!loginEnd) {
                                    loginEnd = true;
                                    WeChat.sendAuthRequest('snsapi_userinfo', '123').then((resp) => {
                                        this.weChatLogin(resp.code);
                                        loginEnd = false;
                                    }, (error) => {
                                        loginEnd = false;
                                    });
                                } else {
                                    Toast.show(Language().login_loading);
                                }
                            } else {
                                Toast.show(Language().need_install_wx);
                            }
                        }, (error) => {
                        });
                    } else {
                        Toast.show(netWorkTool.NOT_NETWORK);
                    }
                });
            } catch (e) {
                console.log('login wx error:' + e);
            }
        } else {
            Toast.show(Language().agreement_prompt);
        }

        //统计
        rnRoNativeUtils.onStatistics(Constant.WE_CHAT_LOGIN);
    }

    /**
     *微信验证通过后登录用户系统
     * @param code
     */
    weChatLogin(code) {
        try {
            this.onShowProgressDialog();
            let url = apiDefines.GET_USER_INFO + code;
            Util.get(url, (code, message, data) => {
                this.onHideProgressDialog();
                if (code == 1000) {
                    if (data) {
                        this.onFinishPage();
                        this.props.updateUserInfo(data);
                        try {
                            this.props.openPage(Scenes.SCENE_APP_MAIN, 'global');
                        } catch (e) {
                            console.log(e)
                        }
                        AsyncStorageTool.saveUserInfo(JSON.stringify(data));
                        if (data != null) {
                            RealmManager.initRealmManager(data.id, (callback) => {
                                if (callback) {
                                    this.props.getChatData(data.id, callback);
                                    this.props.getFamilyChat(data.id, callback);
                                    this.props.initFriendRealm(callback);
                                }
                            });
                            rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token);
                            rnRoNativeUtils.onSetLocation(data.lc);
                            this.configLocation(data.lc);
                        }
                    }
                } else {
                    Toast.show(message);
                }
            }, (error) => {
                this.onHideProgressDialog();
                Toast.show(Language().not_network);
            });
        } catch (e) {
            console.log('wechat login error:' + e);
        }

    }

    /**
     * 申请Facebook授权
     */
    onLoginFromFacebook() {
        if (this.state.useragreement) {
            try {
                if (this.state.isFaceBookAppInstalled) {
                    netWorkTool.checkNetworkState((isConnected) => {
                        if (isConnected) {
                            // Facebook获取授权并请求token
                            LoginManager.logInWithReadPermissions(['public_profile']).then(
                                (result) => {
                                    if (result.isCancelled) {
                                        console.log('facebokk login cancelled');
                                    } else {
                                        AccessToken.getCurrentAccessToken().then(
                                            (data) => {
                                                //alert(JSON.stringify(data));
                                                console.log('data:' + JSON.stringify(data));
                                                this.userLoginFacebook(data.accessToken);
                                                LoginManager.logOut();
                                            }
                                        )
                                    }
                                },
                                (error) => {
                                    //alert('Login fail with error: ' + error);
                                    console.log('Login fail with error: ' + error);
                                    Toast.show(Language().facebook_authorization_fail);
                                }
                            );
                        } else {
                            Toast.show(netWorkTool.NOT_NETWORK);
                        }
                    });
                } else {
                    Toast.show(Language().need_install_facebook);
                }
            } catch (e) {
                console.log('login facebook error:' + e);
            }
        } else {
            Toast.show(Language().agreement_prompt);
        }


    }

    /**
     * Facebook授权成功请求服务器获取信息
     * @param accessToken
     */
    userLoginFacebook(accessToken) {
        try {
            this.onShowProgressDialog();
            let url = apiDefines.GET_USER_INFO_FROM_FACEBOOK + accessToken;
            Util.get(url, (code, message, data) => {
                this.onHideProgressDialog();
                if (code == 1000) {
                    if (data) {
                        let userInfo = JSON.stringify(data);
                        this.onFinishPage();
                        this.props.updateUserInfo(data);
                        this.props.openPage(Scenes.SCENE_APP_MAIN, 'global');
                        AsyncStorageTool.saveUserInfo(userInfo);
                        if (data != null) {
                            RealmManager.initRealmManager(data.id, (callback) => {
                                if (callback) {
                                    this.props.getChatData(data.id, callback);
                                    this.props.getFamilyChat(data.id, callback);
                                    this.props.initFriendRealm(callback);
                                }
                            });
                            rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token);
                            rnRoNativeUtils.onSetLocation(data.lc);
                            this.configLocation(data.lc);
                        }
                    }
                } else {
                    Toast.show(message);
                }
            }, (error) => {
                this.onHideProgressDialog();
                Toast.show(Language().not_network);
            });
        } catch (e) {
            console.log('facebook login error:' + e);
        }
        //统计
        rnRoNativeUtils.onStatistics(Constant.FACEBOOK_LOGIN);
    }

    /**
     * QQ登录
     */
    onLoginFromQQ() {
        if (this.state.useragreement) {
            try {
                netWorkTool.checkNetworkState((isConnected) => {
                    if (isConnected) {
                        this.qqTimer = setTimeout(() => {
                            loginEnd = false;
                        }, 3000);
                        QQAPI.isQQInstalled().then((resp) => {
                            console.log('isQQInstalled:' + resp);
                            if (resp) {
                                //QQ登录
                                if (!loginEnd) {
                                    loginEnd = true;
                                    QQAPI.login('get_simple_userinfo').then((resp) => {
                                        loginEnd = false;
                                        this.qqLogin(resp.access_token, resp.expires_in, resp.oauth_consumer_key, resp.openid);
                                    }, (error) => {
                                        loginEnd = false;
                                    });
                                } else {
                                    Toast.show(Language().login_loading);
                                }

                            } else {
                                Toast.show(Language().need_install_qq);
                            }
                        }, (error) => {
                            Toast.show(Language().need_install_qq);
                        });
                    } else {
                        Toast.show(netWorkTool.NOT_NETWORK);
                    }
                });
            } catch (e) {
                console.log('login qq error:' + e);
            }
        } else {
            Toast.show(Language().agreement_prompt);
        }

        //统计
        rnRoNativeUtils.onStatistics(Constant.QQ_LOGIN);
    }

    /**
     * QQ授权后返回的数据
     * @param access_token
     * @param expires_in
     * @param oauth_consumer_key
     * @param openid
     */
    qqLogin(access_token, expires_in, oauth_consumer_key, openid) {
        try {
            this.onShowProgressDialog();
            let url = apiDefines.GET_USER_INFO_FROM_QQ + access_token + apiDefines.EXPIRES_IN + expires_in + apiDefines.OAUTH_CONSUMER_KEY + oauth_consumer_key + apiDefines.OPENID + openid;
            Util.get(url, (code, message, data) => {
                this.onHideProgressDialog();
                if (code == 1000) {
                    if (data) {
                        let userInfo = JSON.stringify(data);
                        this.onFinishPage();
                        this.props.updateUserInfo(data);
                        try {
                            this.props.openPage(Scenes.SCENE_APP_MAIN, 'global');
                        } catch (e) {
                            console.log(e)
                        }
                        AsyncStorageTool.saveUserInfo(userInfo);
                        if (data != null) {
                            RealmManager.initRealmManager(data.id, (callback) => {
                                if (callback) {
                                    this.props.getChatData(data.id, callback);
                                    this.props.getFamilyChat(data.id, callback);
                                    this.props.initFriendRealm(callback);
                                }
                            });
                            rnRoNativeUtils.initUserInfoToNative(data.id, data.name, data.sex, data.image, data.token.access_token);
                            rnRoNativeUtils.onSetLocation(data.lc);
                            this.configLocation(data.lc);
                        }
                    }
                } else {
                    Toast.show(message);
                }
            }, (error) => {
                this.onHideProgressDialog();
                Toast.show(Language().not_network);
            });
        } catch (e) {
            console.log('qq login error:' + e);

        }
    }

    /**
     * location信息设置
     * @param lc
     */
    configLocation(lc) {
        if (lc) {
            this.props.setLocation(lc);
        }
    }
}
const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    rightArrow: {
        width: 15, height: 15, resizeMode: 'contain', marginLeft: 10, marginRight: 10, alignSelf: 'center'
    },
    loginButton: {
        borderRadius: 3, backgroundColor: '#F2C454', marginRight: 10, marginLeft: 10, marginTop: 20
    },
    inputStyle: {borderWidth: 1, borderColor: '#CCC', borderRadius: 3, paddingLeft: 10}
});
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        updateUserInfo: (data) => dispatch((userInfo(data))),
        getChatData: (id, realm) => dispatch(getChatData(id, realm)),
        getFamilyChat: (id, realm) => dispatch(getFamilyChat(id, realm)),
        initFriendRealm: (realm) => dispatch(initFriendRealm(realm)),
        setLocation: (lc) => dispatch(location(lc)),
    }
);
const mapStateToProps = state => ({
    countryInfo: state.countryListReducer.data,
    checkOpenRegisterData: state.checkRegisterReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps,)(LoginOther);