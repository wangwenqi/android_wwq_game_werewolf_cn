/**
 * Created by wangxu on 2017/3/17.
 */
'use strict';
import React, {Component} from 'react';
import Language from '../../../resources/language';
import Color from '../../../resources/themColor';
import * as SCENES from '../../../support/actions/scene';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    Image,
    StyleSheet,
    View,
    Platform,
    TouchableOpacity,
    Dimensions,
    NativeModules,
    ListView,
    DeviceEventEmitter,
    NativeEventEmitter,
} from 'react-native';
import Toast from '../../../support/common/Toast';
import netWorkTool from '../../../support/common/netWorkTool';
import Util from '../../../support/common/utils';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import {getUserInfo} from '../../../support/actions/userInfoActions';
// import CachedImage from 'react-native-cached-image';
import CachedImage from '../../../support/common/CachedImage';

import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import * as Constant from '../../../support/common/constant';

import {
    Button,
    Left,
    Text,
    Right,
    Body,
    Content, CardItem, ListItem, Input, Container, Spinner
} from 'native-base';
import Dialog, {Prompt} from '../../../support/common/dialog';
import ImagePicker from 'react-native-image-crop-picker';
import Loading from '../../../support/common/Loading';
import UtilsTool from '../../../support/common/UtilsTool';

const {
    pushRoute,
    popRoute
} = actions;
const bgBack = require('../../../resources/imgs/ic_back.png');
const rightArrow = require('../../../resources/imgs/bg_arrow_right.png');
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const headHeight = (Platform.OS === 'ios') ? 60 : 50;

const ScreenHeight = Dimensions.get('window').height;
const ScreenWidth = Dimensions.get('window').width;
let progressDialog;
//获取屏幕宽度
let {width} = Dimensions.get('window');
//常量设置
let cols = 3;
let cellWH = 80;
let vMargin = ((width - 90) - cellWH * cols) / (cols + 1);
let hMargin = 5;
var that;
let images;
const {RNMessageSender}=NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
class EditUserInfo extends Component {
    constructor(props) {
        super(props);
        that = this;
        images = new Array();
        if (this.props.userInfo != null && this.props.userInfo.photos != null) {
            images = this.props.userInfo.photos;
        }
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        //2.设置返回数据
        let newSignature = this.props.userInfo.signature;
        this.state = {
            name: this.props.userInfo.name,
            image: this.props.userInfo.image,
            gender: this.props.userInfo.sex,
            signature: newSignature,
            imageInfo: null,
            dataSource: ds.cloneWithRows(images),
            height: 0,
            isFinishing: false
        }
    }

    static propTypes = {
        navigator: React.PropTypes.shape({}),
        openPage: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        code: React.PropTypes.number,
        message: React.PropTypes.string,
        updateUserInfo: React.PropTypes.func,
        leancloudNode: React.PropTypes.string
    }

    onFinishPage() {
        if (!this.state.isFinishing) {
            this.state.isFinishing = true;
            this.props.finishPage('global');
        }
        this.isFinishingTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }


    render() {
        if (that.props.userInfo.photos != null) {
            that.state.dataSource = that.state.dataSource.cloneWithRows(that.props.userInfo.photos);
            images = that.props.userInfo.photos;
        }
        return (
            <Container style={{backgroundColor:Color.content_color}}>
                <CardItem
                    style={{backgroundColor:(Color.headColor),height:headHeight,paddingTop:headPaddingTop,paddingBottom:0}}>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={this.onFinishPage.bind(this)}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf:'center',flex:3}}>
                    <Text style={{color: '#fff',fontSize: 18,alignSelf:'center'}}>{Language().edit_user_info}</Text>
                    </Body>
                    <Right style={{flex:1}}>
                        <TouchableOpacity
                            style={{justifyContent:'center'}}
                            onPress={this.changeUserInfo.bind(this)}>
                            <Text
                                style={{color:'#fff',fontSize:17,alignSelf:'center',padding:5}}>{Language().save}</Text>
                        </TouchableOpacity>
                    </Right>
                </CardItem>
                <Content>
                    <View style={[styles.rowSeparator,{marginTop:15}]}></View>
                    <View style={{backgroundColor:Color.colorWhite}}>
                        <ListItem style={{height:65,marginLeft:5}} button onPress={this.onSetHead.bind(this)}>
                            <Left>
                                <Text
                                    style={{fontSize: 16,color: (Color.setting_text_color),padding: 5}}>{Language().avatar}</Text>
                            </Left>
                            <Right>
                                <CachedImage style={styles.head}
                                             defaultSource={require('../../../resources/imgs/ic_default_user.png')}
                                             source={(this.state.image==null||this.state.image=='')?require('../../../resources/imgs/ic_default_user.png'):{uri:this.state.image}}/>
                            </Right>
                            <Image source={rightArrow}
                                   style={styles.rightArrow}></Image>
                        </ListItem>
                        <ListItem style={{height:65,marginLeft:5}}>
                            <Left>
                                <Text
                                    style={{fontSize: 16,color: (Color.setting_text_color),padding: 5}}>{Language().nickname}</Text>
                            </Left>
                            <View style={{flex:3}}>
                                <Input
                                    style={{fontSize: 16,color: (Color.editUserTextColor),padding: 5,textAlign:'right',paddingRight:20,flex:1}}
                                    maxLength={20}
                                    placeholder={Language().placeholder_nickname}
                                    placeholderTextColor={Color.loginTextColor}
                                    onChangeText={(text)=>this.onChangeName(text)} value={this.state.name}/>
                            </View>
                        </ListItem>
                        <ListItem style={{height:65,marginLeft:5}} button onPress={this.onSetGender.bind(this)}>
                            <Left>
                                <Text
                                    style={{fontSize: 16,color: (Color.setting_text_color),padding: 5}}>{Language().gender}</Text>
                            </Left>
                            <Right>
                                <Text
                                    style={{fontSize: 16,color: (Color.editUserTextColor),padding: 5}}>{(this.state.gender == 1) ? Language().male : Language().female}</Text>
                            </Right>
                            <Image source={rightArrow}
                                   style={styles.rightArrow}></Image>
                        </ListItem>
                    </View>
                    <View style={styles.rowSeparator}></View>
                    <View style={[styles.rowSeparator,{marginTop:15}]}></View>
                    <CardItem style={{backgroundColor:(Color.colorWhite),padding:10}}>
                        <Text style={{color:(Color.setting_text_color),fontSize: 16,padding:5}}
                              numberOfLines={1}>{Language().signature}</Text>
                        <Input
                            multiline={true}
                            style={{fontSize: 14,color: (Color.editUserTextColor),padding: 5,flex:1,height:Math.max(35,this.state.height)}}
                            placeholder={Language().prompt_signature}
                            placeholderTextColor={Color.prompt_text_color}
                            maxLength={200}
                            onChangeText={(text)=>this.onChangeSignature(text)} value={this.state.signature}
                            onContentSizeChange={this.onContentSizeChange.bind(this)}/>
                    </CardItem>
                    <View style={styles.rowSeparator}></View>
                    <View style={[styles.rowSeparator,{marginTop:15}]}></View>
                    <CardItem style={{backgroundColor:(Color.colorWhite),padding:10}} button
                              onPress={this.editPhotos.bind(this)}>
                        <View flexDirection='row' style={{justifyContent:'center'}}>
                            <Text
                                style={{color:(Color.setting_text_color),fontSize: 16,padding:5,paddingTop:10,paddingBottom:10}}>{Language().photo_wall}</Text>
                        </View>
                        {this._photoWallView()}
                        <Image source={rightArrow}
                               style={styles.rightArrow}></Image>
                    </CardItem>
                    <View style={styles.rowSeparator}></View>
                </Content>
                <Loading ref="progressDialog" loadingTitle={Language().save_loading}/>
                {Dialog.inject()}
                <Prompt ref='setAvatar' message='' title='' posText='' negText=''
                        contentStyle={{backgroundColor:(Color.transparent),width:ScreenWidth,paddingLeft: 15,paddingBottom: 0,paddingRight: 15}}
                        dialogStyle={{backgroundColor:'rgba(0, 0, 0, 0.25)',justifyContent: 'flex-end'}}>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8}}>
                        <Text
                            style={{alignSelf:'center',padding:15,color:(Color.loginTextColor),fontSize:18}}>{Language().title_upload_type}</Text>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:12}} onPress={this.pickSingle.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().with_photo_album}</Text>
                        </TouchableOpacity>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:12}} onPress={this.pickSingleWithCamera.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().with_camera}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8,marginTop:10}}>
                        <TouchableOpacity style={{padding:12}} onPress={this.onDismissDialog.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().cancel}</Text>
                        </TouchableOpacity>
                    </View>
                </Prompt>
                <Prompt ref='selectGenderDialog' message='' title='' posText='' negText=''
                        contentStyle={{backgroundColor:(Color.transparent),width:ScreenWidth,paddingLeft: 15,paddingBottom: 0,paddingRight: 15}}
                        dialogStyle={{backgroundColor:'rgba(0, 0, 0, 0.25)',justifyContent: 'flex-end'}}>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8}}>
                        <Text
                            style={{alignSelf:'center',padding:15,color:(Color.loginTextColor),fontSize:18}}>{Language().title_select_gender}</Text>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:12}} onPress={this.onChangeGenderMale.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().male}</Text>
                        </TouchableOpacity>
                        <View style={{backgroundColor:Color.line_color,height:0.5}}/>
                        <TouchableOpacity style={{padding:12}} onPress={this.onChangeGenderFemale.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().female}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{backgroundColor:(Color.colorWhite),borderRadius: 8,marginTop:10}}>
                        <TouchableOpacity style={{padding:12}}
                                          onPress={this.onDismissSelectGenderDialog.bind(this)}>
                            <Text
                                style={{fontSize:16,color:(Color.selectDialogTextColor),padding:5,alignSelf:'center'}}>{Language().cancel}</Text>
                        </TouchableOpacity>
                    </View>
                </Prompt>
            </Container>
        );
    }

    /**
     * 照片墙
     * @private
     */
    _photoWallView() {
        return images.length == 0 ? (<Text
                style={{color:(Color.prompt_gift_color),fontSize: 14,padding:10,flex:1}}>{Language().no_photos_prompt}</Text>) : (
                <ListView dataSource={this.state.dataSource}
                          renderRow={this.renderRow}
                          removeClippedSubviews={false}
                          contentContainerStyle={styles.listViewStyle}/>);
    }

    /**
     * 个性签名输入框换行并自增加
     * @param event
     */
    onContentSizeChange(event) {
        this.setState({height: event.nativeEvent.contentSize.height});
    }

    /**
     *
     * @param data
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    renderRow(data, sectionId, rowId) {
        return (
            <View style={styles.cellBackStyle}>
                <CachedImage
                    source={(data.url==''||data.url==null)?(require('../../../resources/imgs/me_photo_default.png')):({uri:data.url})}
                    defaultSource={require('../../../resources/imgs/me_photo_default.png')}
                    style={{width:75,height:75,alignSelf:'center'}}
                    type='photo'>

                </CachedImage>
            </View>
        );
    }

    /**
     * 编辑照片墙
     */
    editPhotos() {
        try {this.props.openPage(SCENES.SCENE_PHOTO_WALL, 'global');}catch (e){console.log(e)}
    }

    /**
     * 设置头像
     */
    onSetHead() {
        this.refs.setAvatar.show();
    }

    /**
     * 设置性别
     */
    onSetGender() {
        this.refs.selectGenderDialog.show();
    }

    /**
     * 昵称的修改
     * @param text
     */
    onChangeName(text) {
        this.setState({
            name: text
        });
    }

    /**
     * 修改个性签名
     * @param text
     */
    onChangeSignature(text) {
        this.setState({
            signature: text
        });
    }

    /**
     * 关闭设置头像页面
     */
    onDismissDialog() {
        this.refs.setAvatar.hide();
    }

    /**
     * 关闭选取性别的dialog
     */
    onDismissSelectGenderDialog() {
        this.refs.selectGenderDialog.hide();
    }

    /**
     * 选择中性别为男
     */
    onChangeGenderMale() {
        this.onDismissSelectGenderDialog();
        this.setState({
            gender: 1,
        });
    }

    /**
     * 选择中性别为女
     */
    onChangeGenderFemale() {
        this.onDismissSelectGenderDialog();
        this.setState({
            gender: 2,
        });
    }

    /**
     * 选取相册照片，单张
     */
    pickSingle() {
        this.onDismissDialog();
        this.pickPhotoimer = setTimeout(() => {
            ImagePicker.openPicker({
                width: 400,
                height: 400,
                cropping: true,
                cropperTintColor: Color.headColor
            }).then(image => {
                this.state.imageInfo = image;
                this.setState({
                    image: image.path
                });
            }).catch(e => {
                console.log(e);
                this.state.imageInfo = null;
                this.setState({
                    image: this.props.userInfo.image
                });
            });
        }, 200)
    }

    /**
     * 拍照选取
     */
    pickSingleWithCamera() {
        this.onDismissDialog();
        this.pickCameraTimer = setTimeout(() => {
            ImagePicker.openCamera({
                width: 400,
                height: 400,
                cropping: true,
                cropperTintColor: Color.headColor
            }).then(image => {
                this.state.imageInfo = image;
                this.setState({
                    image: image.path
                });
            }).catch(e => {
                this.state.imageInfo = null;
                this.setState({
                    image: this.props.userInfo.image
                });
                console.log(e)
            });
        }, 200)
    }

    /**
     * 上传图片
     * @param image
     */
    photoFileUpload(image) {
        try {
            /**
             * 上传图片
             */
            this.onShowProgressDialog();
            UtilsTool.filePathFormat(UtilsTool.fileHeader2,image.path,(newPath)=>{
                rnRoNativeUtils.uploadFile([newPath],Constant.ACTION_UP_LOAD_USER_HEAD, 10000);
            });
        } catch (e) {
            this.onHideProgressDialog();
            Toast.show(Language().upload_fail);
        }

    }

    /**
     * 判断点击修改昵称的操作
     */
    changeUserInfo() {
        let {name} = this.state;
        if (!name.length) {
            Toast.show(Language().placeholder_nickname);
            return;
        }
        try {
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    try {
                        //need change user image
                        if ((this.state.image != '' || this.state.image != null) && this.state.image != this.props.userInfo.image) {
                            //进行头像上传，上传成功后调用修改信息接口
                            if (this.state.imageInfo == null) {
                                this.onShowProgressDialog();
                                this.doUserChangeInfo(this.state.image, '');
                            } else {
                                this.photoFileUpload(this.state.imageInfo);
                            }
                        } else if (this.state.name != this.props.userInfo.name || this.state.gender != this.props.userInfo.sex || (this.state.signature != this.props.userInfo.signature)) {
                            //直接进行修改信息的接口
                            this.onShowProgressDialog();
                            if (this.state.signature != this.props.userInfo.signature) {
                                //统计修改签名
                                rnRoNativeUtils.onStatistics(Constant.CHANGE_SIGNATURE);
                            }
                            this.doUserChangeInfo(this.state.image, '');
                        } else {
                            //直接退出页面
                            this.onFinishPage();
                        }
                    } catch (e) {

                    }
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        } catch (e) {
            console.log('error:' + e);
        }
    }

    /**
     * 通知原生去跟新个人信息
     * @param image
     */
    updateGlobalUserState(image) {
        let name = this.state.name;
        let sex = this.state.gender;
        if (Platform.OS === 'ios') {
            var main = NativeModules.MainViewController;
            if (main && main.modifyUserInfo) {
                main.modifyUserInfo(name, sex, image);
            }
        } else {
            NativeModules.NativeJSModule.updateGlobalUserState(name, sex, image);
        }
    }

    /**
     * 执行修改的操作
     */
    doUserChangeInfo(image, imageId) {
        let {name} = this.state;
        name = name.trim();
        if (!name.length) {
            this.onHideProgressDialog();
            Toast.show(Language().placeholder_nickname);
            return;
        }
        try {
            let url = apiDefines.CHANGE_USER_INFO + name + apiDefines.GENDER + this.state.gender + apiDefines.AVATAR + image;
            if (imageId && image != '') {
                url = url + apiDefines.OBJECTID + imageId + apiDefines.HOST + this.props.leancloudNode;
            }
            if (this.state.signature) {
                url = url + apiDefines.SIGNATURE + this.state.signature;
            } else {
                url = url + apiDefines.SIGNATURE + '';
            }
            Util.get(url, (code, message, data) => {
                if (code == 1000) {
                    if (this.props.userInfo != null) {
                        this.props.updateUserInfo(this.props.userInfo.id);
                    }
                    this.updateGlobalUserState(image);
                    this.onHideProgressDialog();
                    Toast.show(Language().change_success);
                    this.onFinishPage();

                } else {
                    this.onHideProgressDialog();
                    Toast.show(Language().change_fail);
                }
            }, (error) => {
                this.onHideProgressDialog();
                Toast.show(Language().not_network);
            });
        } catch (e) {
            this.onHideProgressDialog();
            console.log('change user info error:' + e);

        }
    }

    componentDidMount() {
        //页面渲染完毕后定义加载view
        progressDialog = this.refs.progressDialog;
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        try {
            if (Platform.OS === 'android') {
                this.onNativeMessageLister = DeviceEventEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
            } else {
                this.onNativeMessageLister = rNMessageSenderEmitter.addListener('ReactNativeBridge', this.nativeMessage.bind(this));
            }
        } catch (e) {
            //alert(e)
        }
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.pickCameraTimer && clearTimeout(this.pickCameraTimer);
        this.pickPhotoimer && clearTimeout(this.pickPhotoimer);
        this.isFinishingTimer && clearTimeout(this.isFinishingTimer);
        this.onNativeMessageLister.remove();
    }

    handleMethod(isConnected) {

    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     * 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    /**
     * 统一处理native的消息
     * @param event
     */
    nativeMessage(event) {
        if (event && event.Data) {
            let data = JSON.parse(event.Data);
            switch (data.action) {
                case Constant.ACTION_UP_LOAD_USER_HEAD:
                    if (data.params) {
                        let code = data.params.code;
                        let images = data.params.data;
                        if (code == 0) {
                            //上传成功
                            if (images && images[0]) {
                                this.doUserChangeInfo(images[0].url, images[0].key);
                            } else {
                                this.onHideProgressDialog();
                                Toast.show(Language().upload_fail);
                            }
                        } else {
                            //上传失败
                            Toast.show(data.params.message)
                            this.onHideProgressDialog();
                        }
                    } else {
                        this.onHideProgressDialog();
                        Toast.show(Language().upload_fail);
                    }
                    break;
            }
        }
    }
}
const styles = StyleSheet.create({
    textTitle: {
        color: '#fff',
        fontSize: 16,
        alignSelf: 'center'
    }, bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    },
    head: {
        width: 55,
        height: 55,
        borderRadius: 27
    },
    rightArrow: {
        width: 15, height: 15, resizeMode: 'contain', marginLeft: 5
    },
    textStyle: {
        fontSize: 16,
        color: (Color.editUserTextColor),
        padding: 5
    }, cellBackStyle: {
        width: cellWH,
        height: cellWH,
        marginLeft: vMargin,
        marginTop: hMargin,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    listViewStyle: {
        flexDirection: 'row',
    },
    rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: 'lightgray',
    },

});
const mapDispatchToProps = dispatch => ({
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        finishPage: (key) => dispatch(popRoute(key)),
        updateUserInfo: (userId) => dispatch((getUserInfo(userId))),
    }
)
const mapStateToProps = state => ({
    code: state.userInfoReducer.code,
    message: state.userInfoReducer.message,
    userInfo: state.userInfoReducer.data,
    leancloudNode: state.configReducer.leanCloud_node
});
export default connect(mapStateToProps, mapDispatchToProps)(EditUserInfo);