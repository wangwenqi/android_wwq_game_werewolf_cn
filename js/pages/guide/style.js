/**
 * Created by wangxu on 2017/3/4.
 */
'use strict';
import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
    wrapper: {},
    slideOne: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    slideTwo: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5',
    },
    slideThird: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
        marginTop: 200
    }
});
module.exports = styles;