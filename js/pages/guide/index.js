/**
 * Created by wangxu on 2017/3/4.
 */
'use strict';
import React, {Component} from 'react';
import * as SCENES from '../../../support/actions/scene';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {Container, Content, Text, Button, Icon} from 'native-base';
import Swiper from 'react-native-swiper';
import styles from "./style";
const {
    pushRoute,
} = actions;
class Guide extends Component {
    static propTypes = {
        navigator: React.PropTypes.shape({}),
        openHome: React.PropTypes.func
    }

    onOpenHome() {
        this.props.openHome(SCENES.SCENE_LOGIN, 'global')
    }

    render() {
        return (
            <Swiper style={styles.wrapper} showsButtons={false} loop={false}>
                <Container style={{ flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor: '#9DD6EB'}}>
                    <Text style={styles.text}>第一个页面</Text>
                </Container>
                <Container style={{ flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor: '#97CAE5'}}>
                    <Text style={styles.text}>第二个页面</Text>
                </Container>
                <Container style={{ flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor: '#92BBD9'}}>
                    <Text style={styles.text}>第三个页面</Text>
                    <Button iconRight bordered success small block onPress={this.onOpenHome.bind(this) }
                            style={{alignSelf:'center',marginTop:50}}>
                        <Text>Next</Text>
                        <Icon name='arrow-forward'/>
                    </Button>
                </Container>
            </Swiper>
        )
    }
}
const mapDispatchToProps = dispatch => ({
        openHome: (route, key) => dispatch(pushRoute({key: route}, key))
    }
)
export default connect(null, mapDispatchToProps)(Guide);