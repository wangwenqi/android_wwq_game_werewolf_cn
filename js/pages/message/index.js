/**
 * Created by wangxu on 2017/3/1.
 */
import React, {Component} from 'react';
import * as types from '../../../support/actions/actionTypes';
import * as SCENES from '../../../support/actions/scene'
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {Text, Left, Right, Body} from 'native-base';
import {
    StyleSheet,
    Image,
    Dimensions,
    View,
    Platform,
    AsyncStorage,
    NativeModules,
    TouchableOpacity,
    ListView, RefreshControl, InteractionManager
} from 'react-native';
import Dialog, {Confirm,} from '../../../support/common/dialog';
import Toast from '../../../support/common/Toast';
import CachedImage from '../../../support/common/CachedImage';
import {removeChat, readMessageData, deleteChatData, getChatMessageForPage} from '../../../support/actions/chatActions';
import {readFamilyChat, getNewFamilyChat, deleteFamilyChat} from '../../../support/actions/groupFamilyChatActions';
import {userInfo, updateTourist, updateGroupInfo} from '../../../support/actions/userInfoActions';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import {clearFriendList} from '../../../support/actions/friendActions';
import {clearMessageList} from '../../../support/actions/messageActions';
import {removeFriendStatus} from '../../../support/actions/friendStatusAction';
import Language from '../../../resources/language';
import Badge from 'react-native-smart-badge';
import Color from '../../../resources/themColor';
import Moment from 'moment';
import {SwipeListView} from 'react-native-swipe-list-view';
import * as Constant from '../../../support/common/constant';
import LoadMoreFooter from '../../../support/common/LoadMoreFooter'
import Util from '../../../support/common/utils';
import  * as apiDefines from '../../../support/common/gameApiDefines';
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
const {
    pushRoute,
    popRoute
} = actions;
const gender = require('../../../resources/imgs/ic_male.png');
const genderFemale = require('../../../resources/imgs/ic_female.png');
const defaultMessageBg = require('../../../resources/imgs/bg_messages_blank.png');
const defaultFamilyIcon = require('../../../resources/imgs/ic_group_family.png');
const arrows = require('../../../resources/imgs/bg_arrow_right.png');
const bgFamily = require('../../../resources/imgs/ic_family_bg.png');
const bgFamilyImageDefault = require('../../../resources/imgs/ico_family_default.png');
const activeOpacityValue = (Platform.OS === 'ios') ? 1 : 0.6;
const disableLiftSwipe = (Platform.OS === 'ios') ? false : true;
let deleteMessageDialog;
let chats;
let that;
class Message extends Component {
    static propTypes = {
        openPage: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        familyChatData: React.PropTypes.object,
        chatData: React.PropTypes.object,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        removeChatData: React.PropTypes.func,
        updateUserInfo: React.PropTypes.func,
        updateTourist: React.PropTypes.func,
        clearMessageList: React.PropTypes.func,
        removeFriendStatus: React.PropTypes.func,
        clearFriendList: React.PropTypes.func,
        readMessageData: React.PropTypes.func,
        deleteChatData: React.PropTypes.func,
        getChatMessageForPage: React.PropTypes.func,
        readFamilyChat: React.PropTypes.func,
        isRefreshing: React.PropTypes.bool,
        isNoMore: React.PropTypes.bool,
        getNewFamilyChat: React.PropTypes.func,
        updateGroupInfo: React.PropTypes.func,
        deleteFamilyChat: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        Moment.defineLocale('zh-cn', {weekdays: Language().weekdays.split('_')});
        that = this;
        chats = new Array();
        if (this.props.chatData) {
            chats = this.props.chatData.list;
        }
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            userId: '',
            sectionID: null,
            rowID: null,
            chatDataSource: ds.cloneWithRows(chats)
        };
    }


    onMessageDetail() {
        try {
            this.props.openPage(SCENES.SCENE_MESSAGE_DETAIL, 'global');
        } catch (e) {
            console.log(e)
        }

    }

    componentDidMount() {
        deleteMessageDialog = this.refs.deleteChatMessage;
    }

    componentWillUnmount() {

    }

    componentWillMount() {
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (!_.isEqual(this.props.chatData, nextProps.data) || !_.isEqual(this.props.familyChatData, nextProps.data));
    }

    /**
     * 消息
     * @param item
     */
    actionChatMessage(item) {
        this.state.userId = item.id;
        let conversation_id = '';
        if (item.conversation_id) {
            conversation_id = item.conversation_id;
        }
        if (Platform.OS === 'ios') {
            try {
                var main = NativeModules.MainViewController;
                let sex = '1';
                if (item.sex == '0') {
                    sex = '2';
                } else {
                    sex = item.sex;
                }
                let image = item.image;
                if (image == null) {
                    image = '';
                }
                main.didSelectUser(item.id, 'chat', sex, image, item.name, conversation_id);
            } catch (error) {

            }

        } else {
            try {
                if (item.id != null && item.id != '') {
                    let userSex = this.props.userInfo.sex;
                    if (!userSex) {
                        userSex = 2;
                    }
                    NativeModules.NativeJSModule.startChat(conversation_id, item.id, item.name, parseInt(item.sex), item.image, this.props.userInfo.id, this.props.userInfo.name, userSex, this.props.userInfo.image);
                }
            } catch (error) {

            }
        }
        if (item != null && item.id != '') {
            this.props.readMessageData(item.id);
        }
    }

    /**
     * 删除消息
     * @param item
     */
    actionLongPressMessage(item) {
        if (Platform.OS === 'android') {
            this.state.userId = item.id;
            this.onShowDeleteMessage();
        }
    }

    /**
     * 侧滑删除
     * @param item
     */
    deleteRow(item, rowId, secId, rowMap) {
        try {
            rowMap[`${secId}${rowId}`].closeRow();
        } catch (e) {

        }
        if (item && item.id) {
            that.state.userId = item.id;
            that.onShowDeleteMessage();
        }
    }

    /**
     * show 删除消息的dialog
     */
    onShowDeleteMessage() {
        if (deleteMessageDialog) {
            deleteMessageDialog.show();
        }
    }

    /**
     * hide 删除消息
     */
    onHideDeleteMessage() {
        if (deleteMessageDialog) {
            deleteMessageDialog.hide();
        }
    }

    /**
     * 执行删除
     */
    onDeleteMessage() {
        this.props.deleteChatData(this.state.userId);
        this.onHideDeleteMessage();
        //统计删除消息事件
        rnRoNativeUtils.onStatistics(Constant.DELETE_MESSAGE);
    }

    /**
     * 取消删除
     */
    onCancelDelete() {
        this.onHideDeleteMessage();
    }

    onShowLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.show();
        }
    }

    onHideLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.hide();
        }
    }

    /**
     * 默认view
     * @returns {*}
     */
    defaultView() {
        return chats.length > 0 ? null : (<View
                style={{justifyContent: 'center',backgroundColor:'#fff',position: 'absolute',top:0,bottom:0,left:0,right:0}}>
                <Image style={styles.defaultBg} source={defaultMessageBg}/>
                <Text
                    style={{alignSelf:'center',padding:5,marginTop:20,color:'#9B9B9B'}}>{Language().title_without_message}</Text>
                <Text
                    style={{alignSelf:'center',padding:5,color:'#9B9B9B'}}>{Language().content_without_message}</Text>
            </View>);
    }

    render() {
        if (this.props.chatData != null && this.props.chatData.list) {
            chats = this.props.chatData.list;
        }
        return (
            <View style={{backgroundColor:Color.colorWhite,flex:1}}>
                {/*{this.defaultView()}*/}
                <SwipeListView dataSource={this.state.chatDataSource.cloneWithRows(chats)}
                               renderRow={this.renderRow.bind(this)}
                               renderFooter={this.renderFooter}
                               onEndReached={this._onEndReach}
                               renderHeader={this.renderHeader}
                               initialListSize={1}
                               removeClippedSubviews={true}
                               pageSize={6}
                               enableEmptySections={true}
                               disableRightSwipe={true}
                               disableLeftSwipe={disableLiftSwipe}
                               closeOnRowBeginSwipe={true}
                               rightOpenValue={-75}
                               refreshControl={
                                     <RefreshControl
                                        refreshing={this.props.isRefreshing}
                                         onRefresh={this._onRefresh}
                                         colors={[Color.list_loading_color]} />
                                    }
                               renderHiddenRow={ (data, secId, rowId, rowMap) => (
						       this._onDeleteRowView(data,rowId,secId,rowMap))}/>
                {Dialog.inject()}
                <Confirm ref='deleteChatMessage' message={Language().delete_message} title={Language().delete}
                         posText={Language().confirm} negText={Language().cancel}
                         titleStyle={{alignSelf:'center',fontSize: 17,padding: 5}}
                         messageStyle={{alignSelf:'center',fontSize: 14,marginBottom:10}}
                         contentStyle={{paddingLeft: 1,paddingBottom: 3,paddingRight: 1,width: ScreenWidth/3*2+30}}
                         buttonBarStyle={{justifyContent:'center'}}
                         enumeSeparator={true}
                         rowSeparator={true}
                         buttonStyle={{textAlign: 'center',fontSize: 16,padding: 10,marginLeft: 0,marginRight: 0,alignSelf:'center',justifyContent:'center',flex:1,color:Color.system_dialog_bt_text_color}}
                         onNegClick={this.onCancelDelete.bind(this)} onPosClick={this.onDeleteMessage.bind(this)}>
                </Confirm>
            </View>
        );
    }

    /**
     * load more
     * @private
     */
    _onEndReach = () => {
        //如果网络异常情况下处理
        let data = this.props.chatData;
        if (data) {
            if (data.page < data.total_page) {
                this.props.getChatMessageForPage(data.page + 1, types.GET_MORE_CHAT_DATA);
            } else {
                this.props.getChatMessageForPage(data.page, types.GET_MORE_CHAT_DATA);
            }
        }
    }
    /**
     * 刷新
     * @private
     */
    _onRefresh = () => {
        this.props.isRefreshing = true;
        InteractionManager.runAfterInteractions(() => {
            //刷新消息列表
            this.props.getChatMessageForPage(1, types.REFRESH_CHAT_DATA);
            //更新家族信息
            this.props.updateGroupInfo();
        });
        that.updateGroupInfoMessage();
    };

    /**
     *
     */
    updateGroupInfoMessage() {
        InteractionManager.runAfterInteractions(() => {
            let url = apiDefines.GROUP_INFO;
            Util.get(url, (code, message, data) => {
                if (code == 1000) {
                    if (data && data.group) {

                    } else {
                        let familyChatData = that.props.familyChatData;
                        let unReadFamilyChat = 0;
                        if (familyChatData && familyChatData.unreadNumber) {
                            unReadFamilyChat = familyChatData.unreadNumber;
                        }
                        if (unReadFamilyChat) {
                            that.props.deleteFamilyChat();
                        }
                    }
                }
            }, (failed) => {
            });
        });
    }

    /**
     * 加载更多
     * @private
     */
    renderFooter() {
        return chats.length < 30 ? null : (<LoadMoreFooter isNoMore={that.props.isNoMore}/>);
    }

    /**
     * 显示每条数据的未读消息个数
     * @param count
     * @returns {*}
     * @private
     */
    _onBadgeView(count) {
        return count == 0 ? (null) : (
                <Badge minWidth={5} minHeight={5} textStyle={{fontSize:9,paddingVertical:1}} extraPaddingHorizontal={4}
                       style={{width:6,height:6,marginRight:15,backgroundColor:Color.badge_color}}>{count}</Badge>)
    }

    /**
     * 头部
     */
    renderHeader() {
        let userInfo = that.props.userInfo;
        let groupFamilyInfo = null;
        if (userInfo) {
            if (userInfo.group) {
                groupFamilyInfo = userInfo.group;
            }
        }
        let familyChatData = that.props.familyChatData;
        let unReadFamilyChat = 0;
        if (familyChatData && familyChatData.unreadNumber) {
            unReadFamilyChat = familyChatData.unreadNumber;
        }
        if (groupFamilyInfo) {
            let familyLevel = Language().family;
            let groupMessage = Language().default_family_message;
            let updateChatTime = '';
            if (familyChatData) {
                if (familyChatData.message) {
                    groupMessage = familyChatData.message;
                }
                if (familyChatData.time) {
                    updateChatTime = that._onDateFormat(familyChatData.time);
                }
            }
            if (groupFamilyInfo.level) {
                familyLevel = groupFamilyInfo.level;
            }
            return (<TouchableOpacity onPress={()=>that.enterFamilyGroupMessage(groupFamilyInfo)}
                                      style={{backgroundColor:Color.transparent}}
                                      activeOpacity={0.8}>
                <View style={styles.defaultFamilyView}>
                    <View style={{flex:1,alignSelf:'center',justifyContent:'center',marginLeft:5}}>
                        <View flexDirection='row'>
                            <View style={{justifyContent:'center'}}>
                                <Image style={styles.bgFamilyStyle} source={bgFamily}/>
                                <View style={styles.familyImageView}>
                                    <CachedImage style={styles.bgFamilyImage}
                                                 source={(groupFamilyInfo.image)?{uri:groupFamilyInfo.image}:bgFamilyImageDefault}
                                                 defaultSource={bgFamilyImageDefault}/>
                                </View>
                                <View style={styles.familyLevelView}>
                                    <Text numberOfLines={1}
                                          style={{fontSize: 10, alignSelf: 'center', color: Color.colorWhite,backgroundColor:Color.transparent}}>{familyLevel}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View
                        style={{flex:4,paddingBottom:0,paddingLeft:12,alignSelf:'center',justifyContent:'center',marginLeft:8}}>
                        <Text numberOfLines={1}
                              style={{color:'#313035',padding:2,fontSize:15}}>{groupFamilyInfo.name}</Text>
                        <Text numberOfLines={1}
                              style={{color: '#9B9B9B',padding:2,fontSize:14}}>{groupMessage}</Text>
                    </View>
                    <Right style={{height:60,flex:1.2}}>
                        <Text numberOfLines={1}
                              style={{color:Color.line_color,padding:2,fontSize:12,marginBottom:5,marginTop:5,alignSelf:'center'}}>{updateChatTime}</Text>
                        {that._onBadgeView(unReadFamilyChat)}
                    </Right>
                </View>
            </TouchableOpacity>);
        } else {
            return (
                <TouchableOpacity onPress={()=>{that.enterFamilyGroupList()}}
                                  style={{backgroundColor:Color.transparent}}
                                  activeOpacity={0.6}>
                    <View style={styles.defaultFamilyView}>
                        <Image source={defaultFamilyIcon} style={styles.defaultFamilyImageView}/>
                        <Text
                            style={{flex:1,margin:5,alignSelf:'center',fontSize:13,color:'#9B9B9B'}}>{Language().no_group_message_prompt}</Text>
                        <Image source={arrows} style={styles.arrows}></Image>
                    </View>
                </TouchableOpacity>);
        }
        ;
    }

    /**
     * 进入家族聊天室
     */
    enterFamilyGroupMessage(groupInfo) {
        if (groupInfo && groupInfo.lc_id) {
            let nativeData = {
                action: Constant.ACTION_ENTER_FAMILY_CONVERSATION,
                params: {conversationId: groupInfo.lc_id},
                type: Constant.RN_NATIVE
            }
            rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
            that.props.readFamilyChat(groupInfo.lc_id);
        }
    }

    /**
     * 进入家族列表
     */
    enterFamilyGroupList() {
        that.props.openPage(SCENES.SCENE_GROUP_LIST, 'global');
        //进入家族列表更新
        let group;
        rnRoNativeUtils.initFamilyGroup(group);
    }

    /**
     * 显示删除的view
     * @private
     */
    _onDeleteRowView(data, rowId, secId, rowMap) {
        return Platform.OS === 'ios' ? <View style={styles.rowBack}>
                <TouchableOpacity style={[styles.backRightBtn, styles.backRightBtnRight]}
                                  onPress={()=> that.deleteRow(data,rowId,secId,rowMap) }>
                    <Text style={{color:Color.colorWhite}}>{Language().delete}</Text>
                </TouchableOpacity>
            </View> : null;
    }

    /**
     * 时间格式化
     * @param time
     * @private
     */
    _onDateFormat(time) {
        let formatTime = time;
        try {
            if (formatTime instanceof Date) {

            } else {
                formatTime = new Date(formatTime);
            }
            let day = (new Date()).getDate() - formatTime.getDate();
            if (day == 0) {
                formatTime = Moment(formatTime).format('HH:mm');
            } else if (day == 1) {
                formatTime = Language().yesterday;
            } else if (day <= 7 && day > 1) {
                formatTime = Moment(formatTime).format('dddd');
            } else {
                formatTime = Moment(formatTime).format('MM-DD');
            }
            return formatTime;
        } catch (e) {
            return formatTime;
        }
    }

    /**
     *
     * @param item
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    renderRow(item) {
        return (
            <TouchableOpacity onPress={()=>this.actionChatMessage(item)} style={{backgroundColor:Color.colorWhite}}
                              onLongPress={()=>this.actionLongPressMessage(item)}
                              activeOpacity={activeOpacityValue}>
                <View style={{height: 65, marginLeft: -17, paddingLeft: 15,alignSelf:'flex-start',overflow:'hidden'}}
                      flexDirection='row'>
                    <View style={{flex:1,alignSelf:'center',justifyContent:'center',marginLeft:15}}>
                        <View style={styles.rowImage}>
                            <CachedImage style={styles.headThumbnail}
                                         source={(item.image==''||item.image==null)?(require('../../../resources/imgs/ic_default_user.png')):({uri:item.image})}
                                         defaultSource={require('../../../resources/imgs/ic_default_user.png')}/>
                            <Image source={(item.sex==null||item.sex=='1')?gender:genderFemale}
                                   style={styles.gender}></Image>
                        </View>
                    </View>
                    <View
                        style={{flex:4,paddingBottom:0,paddingLeft:12,alignSelf:'center',justifyContent:'center'}}>
                        <Text numberOfLines={1} style={{color:'#313035',padding:2,fontSize:15}}>{item.name}</Text>
                        <Text numberOfLines={1}
                              style={{color: '#9B9B9B',padding:2,fontSize:14}}>{item.message}</Text>
                    </View>
                    <Right style={{height:60,flex:1.2}}>
                        <Text numberOfLines={1}
                              style={{color:Color.line_color,padding:2,fontSize:12,marginBottom:5,marginTop:5,alignSelf:'center'}}>{that._onDateFormat(item.time)}</Text>
                        {this._onBadgeView(item.unreadNumber)}
                    </Right>
                </View>
                <View style={styles.rowSeparator}></View>
            </TouchableOpacity>
        );
    }
}

const
    styles = StyleSheet.create({
        headThumbnail: {
            width: 50,
            height: 50,
            borderRadius: 25,
            alignSelf: 'center'
        },
        nameTextStyle: {
            color: '#313035',
        },
        messageTextStyle: {
            color: '#9B9B9B'
        },
        defaultBg: {
            alignSelf: 'center',
            width: ScreenWidth / 2 - 40,
            height: ScreenWidth / 2 - 60,
            resizeMode: 'contain',
        },
        rowImage: {
            position: 'absolute',
            left: 0,
            top: 0,
            bottom: 0,
            right: 0,
            alignSelf: 'center',
            justifyContent: 'center'
        },
        gender: {
            alignSelf: 'center',
            width: 15,
            height: 15,
            position: 'absolute',
            left: 20,
            top: 10,
            bottom: 0,
            right: 0,
        },
        rowSeparator: {
            height: StyleSheet.hairlineWidth,
            backgroundColor: Color.rowSeparator_line_color,
        },
        rowBack: {
            alignItems: 'center',
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingLeft: 15,
        },
        backRightBtn: {
            alignItems: 'center',
            bottom: 0,
            justifyContent: 'center',
            position: 'absolute',
            top: 0,
            width: 75
        },
        backRightBtnRight: {
            backgroundColor: Color.delete_bg_color,
            right: 0
        }, defaultFamilyView: {
            flexDirection: 'row',
            justifyContent: 'center', backgroundColor: '#F2F2F2', height: 65
        }, defaultFamilyImageView: {
            width: 50,
            height: 50,
            alignSelf: 'center',
            resizeMode: 'contain', marginLeft: 15
        }, arrows: {
            width: 15,
            height: 15,
            resizeMode: 'contain',
            marginRight: 10,
            alignSelf: 'center',
        }, bgFamilyImage: {
            width: 37,
            height: 37,
            borderRadius: 20,
            alignSelf: 'center'
        }, familyLevelView: {
            position: 'absolute',
            left: 0, bottom: (Platform.OS === 'ios') ? 9 : 8, flex: 1, right: 0
        }, familyImageView: {
            position: 'absolute',
            left: 0, top: 8, right: 0, justifyContent: 'center'
        }, familyText: {
            fontSize: 11, alignSelf: 'center', color: Color.colorWhite, backgroundColor: Color.transparent
        }, bgFamilyStyle: {
            width: 70,
            height: 55,
            resizeMode: 'contain',
            alignSelf: 'center', padding: 5, marginTop: 5, marginBottom: 5
        },
    });
const
    mapDispatchToProps = dispatch => ({
            openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
            finishPage: (key) => dispatch(popRoute(key)),
            removeChatData: () => dispatch(removeChat()),
            updateUserInfo: (data) => dispatch((userInfo(data))),
            updateTourist: (isTourist) => dispatch((updateTourist(isTourist))),
            clearMessageList: () => dispatch(clearMessageList()),
            removeFriendStatus: () => dispatch(removeFriendStatus()),
            clearFriendList: () => dispatch(clearFriendList()),
            readMessageData: (id) => dispatch(readMessageData(id)),
            deleteChatData: (id) => dispatch(deleteChatData(id)),
            getChatMessageForPage: (page, actionType) => dispatch(getChatMessageForPage(page, actionType)),
            readFamilyChat: (id) => dispatch(readFamilyChat(id)),
            getNewFamilyChat: (cid) => dispatch(getNewFamilyChat(cid)),
            updateGroupInfo: () => dispatch(updateGroupInfo()),
            deleteFamilyChat: () => dispatch(deleteFamilyChat()),
        }
    );
const mapStateToProps = state => ({
    userInfo: state.userInfoReducer.data,
    chatData: state.chatReducer.data,
    navigation: state.cardNavigation,
    isNoMore: state.chatReducer.isNoMore,
    isRefreshing: state.chatReducer.isRefreshing,
    familyChatData: state.familyChatReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps)(Message);