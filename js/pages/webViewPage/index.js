/**
 * Created by wangxu on 2017/8/30.
 */
'use strict';
import React, {Component} from 'react';
import Language from '../../../resources/language';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {Container, Left, Text, CardItem, Button, Body, Right} from 'native-base';
import {Image, StyleSheet, View, Platform, Dimensions, TouchableOpacity, WebView} from 'react-native';
import Color from '../../../resources/themColor';
import Toast from '../../../support/common/Toast';
import  * as apiDefines from '../../../support/common/gameApiDefines';
const {
    popRoute,
} = actions;
const bgBack = require('../../../resources/imgs/ic_back.png');
const bgReload = require('../../../resources/imgs/ic_reload.png');
const headPaddingTop = (Platform.OS === 'ios') ? 14 : 0;
const headHeight = (Platform.OS === 'ios') ? 60 : 50;
const bgWebViewError = require('../../../resources/imgs/icon_webview_error.png');
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
let webView;
class WebViewPage extends Component {
    static propTypes = {
        finishPage: React.PropTypes.func,
        token: React.PropTypes.string
    }

    constructor(props, context) {
        super(props, context);

        this.onNavigationStateChange = this._onNavigationStateChange.bind(this);
        this.canGoBack = false;
        this.state = {
            loadEnd: false,
        }
    }

    componentDidMount() {
        webView = this.refs.webView;
    }

    componentWillUnmount() {
    }

    render() {

        return (
            <Container style={{flex:1,backgroundColor:Color.content_color}}>
                <CardItem
                    style={{backgroundColor:(Color.headColor),height:headHeight,paddingTop:headPaddingTop,paddingBottom:0}}>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={this._onBackButton.bind(this)}>
                            <Image source={bgBack} style={styles.bgBack}></Image>
                        </Button>
                    </Left>
                    <Body style={{alignSelf:'center',flex:3}}>
                    <Text
                        style={{color: Color.colorWhite,fontSize: 18,alignSelf:'center'}}>{Language().game_strategy}</Text>
                    </Body>
                    <Right style={{flex:1}}>
                        <Button transparent onPress={this._onReloadButton.bind(this)}>
                            <Image source={bgReload} style={styles.bgBack}></Image>
                        </Button>
                    </Right>
                </CardItem>
                <View style={{flex: 1}}>
                    <WebView
                        ref="webView"
                        source={{uri: apiDefines.URL_BLOG}}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        decelerationRate="normal"
                        startInLoadingState={true}
                        renderError={this._webViewError.bind(this)}
                        onNavigationStateChange={this.onNavigationStateChange}
                    />
                </View>
            </Container>
        );
    }

    _webViewError() {
        return (
            <View style={{flex: 1,backgroundColor:Color.content_color,justifyContent:'center'}}>
                <TouchableOpacity style={styles.webErrorBt} onPress={this._onReloadButton.bind(this)}>
                    <Image style={styles.webViewError} source={bgWebViewError}/>
                </TouchableOpacity>
            </View>);
    }

    _onNavigationStateChange(navState) {
        this.canGoBack = navState.canGoBack;
    }

    _onBackButton() {
        if (this.canGoBack) {
            if (webView) {
                webView.goBack();
            } else {
                this.onFinishPage();
            }
        } else {
            this.onFinishPage();
        }
    }

    _onReloadButton() {
        if (webView) {
            webView.reload();
        }
    }

    onFinishPage() {
        this.props.finishPage('global');
    }
}
const styles = StyleSheet.create({
    bgBack: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        padding: 5
    }, webViewError: {
        alignSelf: 'center',
        width: ScreenWidth / 2 - 40,
        height: ScreenWidth / 2 - 60,
        resizeMode: 'contain',
        alignItems: 'center'
    },
    webErrorBt: {
        justifyContent: 'center',
        backgroundColor: Color.transparent,
        alignSelf: 'center',
    }
});
const mapDispatchToProps = dispatch => ({
        finishPage: (key) => dispatch(popRoute(key)),
    }
);
const mapStateToProps = state => ({
    token: state.userInfoReducer.token,
});
export default connect(mapStateToProps, mapDispatchToProps)(WebViewPage);