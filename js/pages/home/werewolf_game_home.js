/**
 *  Created by wangxu on 20/02/2017.
 * 应用的首页，进行提供游戏入口，以及房间的搜素，以及其他功能的展示
 */
import React, {Component} from 'react';
import {
    Container,
    Button,
    Left,
    Thumbnail,
    Right,
    Body,
    ListItem,
    Item,
    Input,
    Card,
    CardItem,
    Icon,
    Spinner
} from 'native-base';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {
    Image,
    ProgressViewIOS,
    NativeModules,
    AsyncStorage,
    Dimensions,
    View,
    Platform,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Keyboard,
    Modal, DeviceEventEmitter, NativeEventEmitter, InteractionManager, StyleSheet, Switch, WebView, ListView, Text
} from 'react-native';
//##自定义工具
import * as SCENES from '../../../support/actions/scene';
import Util from '../../../support/common/utils';
import * as apiDefines from '../../../support/common/gameApiDefines';
import * as MiniGameConstant from './constant';
import Toast from '../../../support/common/Toast';
import netWorkTool from '../../../support/common/netWorkTool';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import {clearFriendList} from '../../../support/actions/friendActions';
import {clearMessageList} from '../../../support/actions/messageActions';
import Dialog, {Alert, Confirm, Prompt,} from '../../../support/common/dialog';
import {removeChat} from '../../../support/actions/chatActions';
import {userInfo, updateTourist, updateRoomId, getUserInfo} from '../../../support/actions/userInfoActions';
import CachedImage from '../../../support/common/CachedImage';
import LoadImageView from '../../../support/common/LoadImageView';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import * as Constant from '../../../support/common/constant';
import Loading from '../../../support/common/Loading';
import {getMoreFriendStatus, removeFriendStatus} from '../../../support/actions/friendStatusAction';
import {updateNoticeVer} from '../../../support/actions/configActions';
import {taskInfo} from '../../../support/actions/taskInfoActions';
import {updateGiftManifest} from '../../../support/actions/giftManifestActions';
import {checkOpenRegister} from '../../../support/actions/checkRegisterActions';
import styles from "./style";
import Language from '../../../resources/language';
import Dropdown from '../../../support/common/Dropdown';
import ImageManager from '../../../resources/imageManager';
import Color from '../../../resources/themColor';
import GameRule from '../../../support/common/GameRule';
import GameRole from '../../../support/common/GameRole';
import GameSimpleModel from '../../../support/common/GameSimpleModel';
import GameStAdModel from '../../../support/common/GameStAdModel';
import GameProfessionalLlg from '../../../support/common/GameProfessionalLlg';
import InternationSourceManager from '../../../resources/internationSourceManager';//国际化资源管理
const roleIntroduction = require('../../../resources/imgs/ic_role_introduction.png');//角色介绍
const videoHelp = require('../../../resources/imgs/ic_video_help.png');//视频宝典
const gameRule = require('../../../resources/imgs/ic_game_rule.png');//游戏规则
const simpleNovModel = require('../../../resources/imgs/ic_simple_nov.png');//新手简单模式介绍
const stAdModel = require('../../../resources/imgs/ic_st_ad_model.png');//标准高阶模式介绍
const professionalLg = require('../../../resources/imgs/ic_professional_lg.png');//专业术语
const {
    pushRoute,
    popRoute
} = actions;
const bgPopular = require('../../../resources/imgs/icon_popular.png');
const {RNMessageSender} = NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
//
const dialogHelpHeight = ScreenHeight - 10;
const refreshMorePadding = (Platform.OS === 'ios') ? 4 : 0;
const gameModelTop = (Platform.OS === 'ios') ? 25 : 20;
const numberPaddingLeft = (Platform.OS === 'ios') ? 25 : 22;
const dialogPaddingTop = 30;
//游戏类型
const GAME_TYPE_SIMPLE = 'simple';
const GAME_TYPE_SIMPLE_6 = 'simple_6';//简单6人局（简单）
const GAME_TYPE_SIMPLE_9 = 'simple_9';//简单9人局（简单）
const GAME_TYPE_SIMPLE_10 = 'simple_10';//简单10人局（简单）
const GAME_TYPE_NORMAL = 'normal';//标准12人丘比特（标准）
const GAME_TYPE_NORMAL_GUARD = 'normal_guard';//标准12人守卫（标准)
const GAME_TYPE_HIGH_KING = 'high_king';//12高级房间（高阶模式）
const GAME_TYPE_PRE_SIMPLE = 'pre_simple';//6-10局（新手模式）
const GAME_TYPE_PRE_SIMPLE_NEW = 'pre_simple_new';//6人猎人局（新手模式）
const GAME_TYPE_AUDIO = 'audio';//语音房间
//
//游戏模式
const GAME_MODEL_SIMPLE = 'simple';
const GAME_MODEL_STANDARD = 'standard';
const GAME_MODEL_ADVANCED = 'advanced';
const GAME_MODEL_NOVICE = 'novice';
const PersonalInfoActivity = 'com.game_werewolf.activity.PersonalInfoActivity';
let progressDialog;
let needLoginDialog;
let webView;
let image = '';
let that;
let friendStatusList = new Array();
const roleType = {
    manager: 1 << 0,
    teacher: 1 << 1
}

class MiniGameHome extends Component {
    static propTypes = {
        navigator: React.PropTypes.shape({}),
        openPage: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        configData: React.PropTypes.object,
        friendStatusDataList: React.PropTypes.array,
        code: React.PropTypes.number,
        message: React.PropTypes.string,
        token: React.PropTypes.string,
        newRoomId: React.PropTypes.string,
        newPassword: React.PropTypes.string,
        updateRoomId: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        removeChatData: React.PropTypes.func,
        updateUserInfo: React.PropTypes.func,
        updateTourist: React.PropTypes.func,
        isTourist: React.PropTypes.bool,
        clearMessageList: React.PropTypes.func,
        clearFriendList: React.PropTypes.func,
        removeFriendStatus: React.PropTypes.func,
        getMoreFriendStatus: React.PropTypes.func,
        getUserInfo: React.PropTypes.func,
        updateNoticeVer: React.PropTypes.func,
        taskInfo: React.PropTypes.func,
        enterNoviceCount: React.PropTypes.number,
        hostLocation: React.PropTypes.string,
        taskInfoData: React.PropTypes.object,
        noticeVer: React.PropTypes.number,
        updateGiftManifest: React.PropTypes.func,
        updateConfig: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        that = this;
        if (this.props.friendStatusDataList) {
            friendStatusList = this.props.friendStatusDataList;
        }
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        //2.设置返回数据
        this.state = {
            dataSource: ds.cloneWithRows(friendStatusList),
            dialogShow: false,
            dialogIndex: 0,
            dialogTitle: 'title',
            dialogConfirm: 'confirm',
            dialogCancel: 'cancel',
            hideConfirm: false,
            hideCancel: false,
            roomId: '',
            textRoomID: '',
            password: '',
            switchIsOn: false,
            needPassword: false,
            gameHelpType: '',
            gameHelpTitle: '',
            gameHelpDialogShow: false,
            gameHelpMenu: true,
            simple_model_radio: true,
            standard_model_radio: false,
            game_type: GAME_TYPE_SIMPLE_6,
            room_id_focus: false,
            updateGame: true,
            touristCanEnterOtherRoom: false,
            room_type: '',
            showModelProgress: false,
            showAnnouncementModel: false,
            showGameModelManager: false,
            activityGameMode: GAME_MODEL_SIMPLE,//当前选中的模式
            game_limit_level: 0,
            game_limit_level_name: Language().no_limit,
            select_game_play: Language().game_simple_6,
            search_dialog_activity: 0,//0房间，1好友，2家族
            create_room_count: 0
        };
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    componentDidUpdate() {
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
        this.onLeaveGameListener.remove();
        this.checkTokenTimer && clearTimeout(this.checkTokenTimer);
        this.leaveGameTimer && clearTimeout(this.leaveGameTimer);
        this.taskInfoTimer && clearTimeout(this.taskInfoTimer);
        this.finishTimer && clearTimeout(this.finishTimer);
    }

    componentDidMount() {
        //页面渲染完毕后定义加载view
        progressDialog = this.refs.progressDialog;
        needLoginDialog = this.refs.login;
        webView = this.refs.webView;
        this.checkTokenTimer = setTimeout(() => {
            this.checkToken();
        }, 500);
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        try {
            if (Platform.OS === 'android') {
                this.onLeaveGameListener = DeviceEventEmitter.addListener('EVENT_USER_LEAVE', this.onLeaveGameRoom.bind(this));
            } else {
                this.onLeaveGameListener = rNMessageSenderEmitter.addListener('EVENT_USER_LEAVE', this.onLeaveGameRoom.bind(this));
            }
        } catch (e) {

        }
        //this.searchAnimationDialog.show();
    }

    render() {
        if (this.props.friendStatusDataList) {
            friendStatusList = this.props.friendStatusDataList;
        } else {
            friendStatusList = new Array();
        }
        let helpDialogCancel = this.state.gameHelpMenu ? (Language().return_home) : (Language().return_game_help);
        let activityGameModelBg = this._getActivityGameModelBg();
        return (
            <Container>
                {(Platform.OS === 'ios') ? (<View style={{height: 20, backgroundColor: Color.statusBarColor}}/>) : null}
                <ScrollView style={{backgroundColor: Color.colorWhite}} showsVerticalScrollIndicator={false}>
                    {/*用户信息*/}
                    {this.userInfoView()}
                    {/*新手模式入口*/}
                    {this.gameModelViewItem(MiniGameConstant.WEREWOLF_HOME_OPEN_NOVICE, ImageManager.ic_werewolf_novice, Language().game_model_novice, Language().game_model_novice_content)}
                    {/*其他游戏辅助模块*/}
                    {this.gameAuxiliaryView()}
                    {/*简单模式入口*/}
                    {this.gameModelViewItem(MiniGameConstant.WEREWOLF_HOME_OPEN_SIMPLE, ImageManager.ic_werewolf_simple, Language().game_model_simple, Language().game_model_simple_content)}
                    {/*进阶模式*/}
                    {this.gameModelViewItem(MiniGameConstant.WEREWOLF_HOME_OPEN_ADVANCED, ImageManager.ic_werewolf_advanced, Language().game_model_advanced_more, Language().game_model_advanced_more_content)}
                    {/*好友在线*/}
                    {this.onLineFriendView()}
                </ScrollView>
                {/*创建房间以及搜索房间的弹框*/}
                {/*<Modal visible={this.state.dialogShow} transparent={true} animationType='fade'*/}
                       {/*onRequestClose={() => {*/}
                           {/*console.log('close')*/}
                       {/*}}>*/}
                    {/*<Container flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'*/}
                               {/*style={[styles.dialogContainer]}>*/}

                        {/*<ScrollView alwaysBounceVertical={false}>*/}
                            {/*<View*/}
                                {/*style={{*/}
                                    {/*borderColor: Color.action_room_color,*/}
                                    {/*borderRadius: 5,*/}
                                    {/*borderWidth: 2,*/}
                                    {/*width: ScreenWidth - 50,*/}
                                    {/*maxHeight: ScreenHeight - 50,*/}
                                    {/*alignSelf: 'center',*/}
                                    {/*backgroundColor: Color.colorWhite*/}
                                {/*}}>*/}
                                {/*{this.dialogContentManager()}*/}
                                {/*<View flexDirection='row' style={{justifyContent: 'center'}}>*/}
                                    {/*<TouchableOpacity activeOpacity={0.9}*/}
                                                      {/*onPress={this.onCloseModal.bind(this)}*/}
                                                      {/*style={styles.actionButton}>*/}
                                        {/*<Text*/}
                                            {/*style={{*/}
                                                {/*alignSelf: 'center',*/}
                                                {/*color: Color.colorWhite*/}
                                            {/*}}>{this.state.dialogCancel}</Text>*/}
                                    {/*</TouchableOpacity>*/}
                                    {/*<View style={{*/}
                                        {/*backgroundColor: '#8732ff',*/}
                                        {/*width: 1,*/}
                                        {/*paddingBottom: 4,*/}
                                        {/*paddingTop: 4*/}
                                    {/*}}/>*/}
                                    {/*<TouchableOpacity activeOpacity={0.8} onPress={this.onConfirmModal.bind(this)}*/}
                                                      {/*style={styles.actionButton}>*/}
                                        {/*<Text*/}
                                            {/*style={{*/}
                                                {/*alignSelf: 'center',*/}
                                                {/*color: Color.colorWhite*/}
                                            {/*}}>{this.state.dialogConfirm}</Text>*/}
                                    {/*</TouchableOpacity>*/}
                                {/*</View>*/}
                            {/*</View>*/}
                            {/*{this.state.showModelProgress ? (*/}
                                {/*<View style={[styles.loading, {left: ScreenWidth / 2 - 55, top: 45}]}>*/}
                                    {/*<Spinner size='small' style={{height: 35}}/>*/}
                                    {/*<Text*/}
                                        {/*style={{*/}
                                            {/*marginTop: 5,*/}
                                            {/*fontSize: 14,*/}
                                            {/*color: Color.colorWhite*/}
                                        {/*}}>{Language().request_loading}</Text>*/}
                                {/*</View>) : null}*/}
                        {/*</ScrollView>*/}
                    {/*</Container>*/}
                {/*</Modal>*/}
                <Modal visible={this.state.dialogShow} transparent={true} animationType='fade'
                       onRequestClose={() => {
                           console.log('close')
                       }}>
                    <Container flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                               style={[styles.dialogContainer]}>

                        <ScrollView alwaysBounceVertical={false}>
                            <View
                                style={{
                                    borderRadius: 10,
                                    width: ScreenWidth - 50,
                                    maxHeight: ScreenHeight - 50,
                                    alignSelf: 'center',
                                    backgroundColor: Color.colorWhite
                                }}>
                                {this.dialogContentManager()}
                                <View flexDirection='row' style={{justifyContent: 'center',width:ScreenWidth - 100,alignSelf:'center',marginBottom:10}}>
                                    <TouchableOpacity activeOpacity={0.9}
                                                      onPress={this.onCloseModal.bind(this)}
                                                      style={styles.actionButton}>
                                        <Text
                                            style={{
                                                alignSelf: 'center',
                                                color: '#818080'
                                            }}>{this.state.dialogCancel}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={0.8} onPress={this.onConfirmModal.bind(this)}
                                                      style={[styles.actionButton,{backgroundColor:'#ffdb42'}]}>
                                        <Text
                                            style={{
                                                alignSelf: 'center',
                                                color:'#1c1c25'
                                            }}>{this.state.dialogConfirm}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.showModelProgress ? (
                                <View style={[styles.loading, {left: ScreenWidth / 2 - 55, top: 45}]}>
                                    <Spinner size='small' style={{height: 35}}/>
                                    <Text
                                        style={{
                                            marginTop: 5,
                                            fontSize: 14,
                                            color: Color.colorWhite
                                        }}>{Language().request_loading}</Text>
                                </View>) : null}
                        </ScrollView>
                    </Container>
                </Modal>
                {/*帮助手册*/}
                <Modal visible={this.state.gameHelpDialogShow} transparent={true} animationType='fade'
                       onRequestClose={() => {
                           console.log('close')
                       }}>
                    <Container flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.65)'
                               style={[styles.dialogContainer]}>
                        <Container
                            style={{
                                height: dialogHelpHeight,
                                backgroundColor: 'rgba(0, 0, 0, 0)',
                                width: ScreenWidth,
                                paddingTop: dialogPaddingTop
                            }}>
                            <CardItem cardBody style={{height: 80, backgroundColor: 'rgba(0, 0, 0, 0)'}}>
                                <CachedImage source={ImageManager.helpDialogTitleBg}
                                             style={{resizeMode: 'contain', flex: 1, alignSelf: 'center'}}>
                                    <Left style={{flex: 1}}>
                                        <Text numberOfLines={1}
                                              style={{
                                                  fontSize: 21,
                                                  fontWeight: 'bold',
                                                  textAlign: 'center',
                                                  alignSelf: 'center',
                                                  color: Color.colorWhite
                                              }}>{this.state.gameHelpTitle}</Text>
                                    </Left>
                                </CachedImage>
                            </CardItem>
                            <View style={{marginLeft: 5, marginRight: 5, flex: 1, top: -5, backgroundColor: '#32184C'}}>
                                <View
                                    style={{
                                        flex: 6,
                                        backgroundColor: (Color.gameHelpDialogBgColor),
                                        marginLeft: 1,
                                        marginRight: 1
                                    }}>
                                    {this.gameHelpContentManager()}
                                </View>
                                <CardItem cardBody
                                          style={{
                                              backgroundColor: '#3F1C62',
                                              height: 52,
                                              borderRadius: 1,
                                              marginLeft: 1,
                                              marginRight: 1
                                          }}>
                                    <Button full transparent
                                            style={{alignSelf: 'center', flex: 1, height: 50, justifyContent: 'center'}}
                                            onPress={this.closeGameHelpDialog.bind(this)}>
                                        <Text
                                            style={{
                                                color: (Color.colorWhite),
                                                fontSize: 18,
                                                fontWeight: 'bold',
                                                alignSelf: 'center',
                                                textAlign: 'center',
                                                marginBottom: 5
                                            }}>{helpDialogCancel}</Text>
                                    </Button>
                                </CardItem>
                            </View>

                        </Container>
                    </Container>
                </Modal>
                {/*房间模式*/}
                <Modal visible={this.state.showGameModelManager} transparent={true} animationType='fade'
                       onRequestClose={() => {
                           this.closeGameModelManager()
                       }}>
                    <CachedImage source={activityGameModelBg.modelSource} style={styles.image}/>
                    <TouchableOpacity activeOpacity={1} onPress={this.closeGameModelManager.bind(this)}
                                      style={[styles.dialogContainer, {backgroundColor: Color.transparent}]}>
                        <Text
                            style={{
                                color: Color.colorWhite,
                                fontSize: 25,
                                fontWeight: 'bold',
                                top: gameModelTop,
                                padding: 5
                            }}>{this._getActivityGameModel()}</Text>
                        <Container flexDirection='row' backgroundColor={Color.transparent}
                                   style={{flex: 1, marginTop: 30}}>
                            <ScrollView alwaysBounceVertical={false}>
                                {this._gameModelManager()}
                            </ScrollView>
                        </Container>
                        <View flexDirection='row' style={{marginBottom: 15}}>
                            <CachedImage
                                style={{width: 40, resizeMode: 'contain', alignSelf: 'center', height: 40}}
                                source={ImageManager.launcherWerewolf}/>
                            <View style={{marginLeft: 5, alignSelf: 'center'}}>
                                <Text
                                    style={{
                                        fontSize: 16,
                                        fontWeight: 'bold',
                                        color: Color.colorWhite
                                    }}>{Language().mini_game_app_name}</Text>
                                <Text
                                    style={{
                                        fontSize: 13,
                                        color: activityGameModelBg.modelTextColor,
                                        marginTop: 3
                                    }}>{Language().werewolf_publicity}</Text>
                            </View>
                        </View>

                    </TouchableOpacity>
                </Modal>
                <Loading ref="progressDialog" loadingTitle={Language().request_loading}/>
                {Dialog.inject()}
                <Alert ref='login' title={Language().title_login} message={Language().need_login_message}
                       messageStyle={{alignSelf: 'center', fontSize: 14, marginBottom: 10}}
                       titleStyle={{alignSelf: 'center', fontSize: 17, padding: 5}}
                       contentStyle={{
                           paddingLeft: 1,
                           paddingBottom: 3,
                           paddingRight: 1,
                           width: ScreenWidth / 3 * 2 + 30
                       }}
                       buttonBarStyle={{justifyContent: 'center'}}
                       buttonStyle={{
                           textAlign: 'center',
                           fontSize: 16,
                           padding: 10,
                           marginLeft: 0,
                           marginRight: 0,
                           alignSelf: 'center',
                           justifyContent: 'center',
                           flex: 1,
                           color: Color.system_dialog_bt_text_color
                       }}
                       posText={Language().confirm}
                       rowSeparator={true}
                       onPosClick={this.login.bind(this)}> </Alert>
                <Confirm ref='touristModal' title={Language().title_prompt_tourist_login} posText={Language().go_login}
                         messageStyle={{alignSelf: 'center', fontSize: 14, marginBottom: 10}}
                         titleStyle={{alignSelf: 'center', fontSize: 17, padding: 5}}
                         contentStyle={{
                             paddingLeft: 1,
                             paddingBottom: 3,
                             paddingRight: 1,
                             width: ScreenWidth / 3 * 2 + 30
                         }}
                         buttonBarStyle={{justifyContent: 'center'}}
                         buttonStyle={{
                             textAlign: 'center',
                             fontSize: 16,
                             padding: 10,
                             marginLeft: 0,
                             marginRight: 0,
                             alignSelf: 'center',
                             justifyContent: 'center',
                             flex: 1,
                             color: Color.system_dialog_bt_text_color
                         }}
                         negText={Language().cancel}
                         enumeSeparator={true}
                         rowSeparator={true}
                         onNegClick={this.dismissTouristDialog.bind(this)} onPosClick={this.toLogin.bind(this)}>
                </Confirm>
            </Container>
        );
    }

    /**
     *
     */
    userInfoView = () => {
        let userInfo = this.props.userInfo;
        let gold = 0;
        let dim = 0;
        let popular = 0;
        let name = '';
        let image = ''
        let level = 0;
        let win = 0;
        let lose = 0;
        let odds = 0;
        let escapeRate = 0;
        let escape = 0;
        let game;
        if (userInfo != null) {
            game = userInfo.game;
            if (userInfo.name) {
                name = userInfo.name;
                if (name.toString().length > 10) {
                    name = name.toString().slice(0, 10) + '..';
                }
            }
            if (userInfo.image && userInfo.image != '') {
                image = userInfo.image;
            }
            if (userInfo.money) {
                if (userInfo.money.gold > 0) {
                    gold = userInfo.money.gold;
                    if (gold.toString().length > 6) {
                        gold = gold.toString().slice(0, 6) + '+';
                    }
                }
                if (userInfo.money.dim > 0) {
                    dim = userInfo.money.dim;
                    if (dim.toString().length > 6) {
                        dim = dim.toString().slice(0, 6) + '+';
                    }
                }
            }
            if (userInfo.popular) {
                popular = userInfo.popular;
            }

            if (game != null && game.level != null && game.lose != null) {
                level = game.level;
                win = game.win;
                lose = game.lose;
                escape = game.escape;
                if ((win + lose + escape) > 0) {
                    odds = win / (win + lose + escape) * 100;
                    odds = odds.toFixed(0);
                    escapeRate = escape / (win + lose + escape) * 100;
                    escapeRate = escapeRate.toFixed(0);
                }
            }
        }
        return (
            <View
                style={{
                    backgroundColor: Color.colorWhite,
                    marginBottom: 0,
                    height: ScreenWidth * 0.381 + ScreenWidth * 0.381 * 0.45
                }}>
                <LoadImageView style={{width: ScreenWidth, height: ScreenWidth * 0.381, position: 'absolute', top: 0}}
                               source={ImageManager.ic_werewolf_info_bg}/>
                <View flexDirection='row' style={{position: 'absolute', top: 3, justifyContent: 'center'}}>
                    <TouchableOpacity
                        style={{backgroundColor: Color.transparent,}}
                        activeOpacity={0.8}
                        onPress={() => {
                            this.finishPage()
                        }}>
                        <View style={{width: 50, height: 30, justifyContent: 'center'}}>
                            <Image source={ImageManager.icBack} style={styles.backStyle}/>
                        </View>
                    </TouchableOpacity>
                    <View style={{flex: 1}}/>
                    {Platform.OS === 'ios' ? this.myWealthView(gold, dim) : null}
                </View>
                <View
                    style={{
                        backgroundColor: Color.transparent,
                        width: ScreenWidth,
                        height: ScreenWidth * 0.381 * 0.71 + 60,
                        position: 'absolute',
                        bottom: 0,
                        justifyContent: 'flex-end'
                    }}>
                    <LoadImageView
                        style={{
                            width: ScreenWidth - 17,
                            height: (ScreenWidth - 17) * 0.32,
                            position: 'absolute',
                            bottom: 0,
                            left: 4
                        }}
                        source={ImageManager.ic_werewolf_info_bg_shadow}/>
                    <View
                        style={{
                            backgroundColor: Color.colorWhite,
                            borderRadius: 5,
                            alignSelf: 'center',
                            width: ScreenWidth - 22,
                            height: (ScreenWidth - 17) * 0.32,
                            marginBottom: 5,
                            paddingTop: 30
                        }}>
                        <Text numberOfLines={1}
                              style={{
                                  color: '#191919',
                                  fontSize: 16,
                                  alignSelf: 'center',
                                  fontWeight: '400'
                              }}>{name}</Text>
                        <View flexDirection='row' style={{marginTop: 7}}>
                            <View style={{flex: 1, justifyContent: 'center'}}>
                                <Text numberOfLines={1}
                                      style={{
                                          color: '#363636',
                                          fontSize: 13,
                                          alignSelf: 'center'
                                      }}>{Language().grade}</Text>
                                <Text numberOfLines={1}
                                      style={{
                                          color: '#dc7657',
                                          fontSize: 14,
                                          alignSelf: 'center',
                                          marginTop: 7
                                      }}>LV.{level}</Text>
                            </View>
                            <View style={{flex: 1, justifyContent: 'center'}}>
                                <Text numberOfLines={1}
                                      style={{
                                          color: '#363636',
                                          fontSize: 13,
                                          alignSelf: 'center'
                                      }}>{Language().popular}</Text>
                                <View flexDirection='row' style={{marginTop: 7, justifyContent: 'center'}}>
                                    <Image style={{width: 15, height: 15, alignSelf: 'center', resizeMode: 'contain',}}
                                           source={bgPopular}/>
                                    <Text numberOfLines={1}
                                          style={{
                                              color: '#dc7657',
                                              fontSize: 14,
                                              alignSelf: 'center',
                                              marginLeft: 2
                                          }}>{popular}</Text>
                                </View>
                            </View>
                            <View style={{flex: 1, justifyContent: 'center'}}>
                                <Text numberOfLines={1}
                                      style={{
                                          color: '#363636',
                                          fontSize: 13,
                                          alignSelf: 'center'
                                      }}>{Language().odds}</Text>
                                <Text numberOfLines={1}
                                      style={{
                                          color: '#dc7657',
                                          fontSize: 14,
                                          alignSelf: 'center',
                                          marginTop: 7
                                      }}>{odds}%</Text>
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity activeOpacity={0.8}
                                      style={{
                                          justifyContent: 'center',
                                          position: 'absolute',
                                          top: 5,
                                          left: ScreenWidth / 2 - 33
                                      }}
                                      onPress={this.onOpenUserInfo.bind(this)}>
                        <View
                            style={{
                                justifyContent: 'center',
                                width: 66,
                                height: 66,
                                borderRadius: 33,
                                backgroundColor: Color.colorWhite
                            }}>
                            <LoadImageView style={{width: 60, height: 60, alignSelf: 'center', borderRadius: 30}}
                                           defaultSource={ImageManager.defaultUser}
                                           source={(image) ? {uri: image} : ImageManager.defaultUser}/>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
    /**
     *
     * @param type
     * @param image
     * @param title
     * @param message
     */
    gameModelViewItem = (type, image, title, message) => {
        return (<View style={[styles.game_home_view_style]}>
                <TouchableOpacity
                    style={{backgroundColor: Color.transparent, justifyContent: 'center'}}
                    onPress={() => this.onClick(type)}
                    activeOpacity={0.8}>
                    <LoadImageView source={image}
                                   style={{
                                       resizeMode: 'contain',
                                       width: ScreenWidth - 20,
                                       height: (ScreenWidth - 20) * 0.26
                                   }}/>
                    <View
                        style={{justifyContent: 'center', position: 'absolute', right: 15, top: 0, bottom: 0}}>
                        <Text
                            style={{
                                fontWeight: 'bold',
                                fontSize: 20,
                                color: Color.colorWhite,
                                textAlign: 'right'
                            }}>{title}</Text>
                        <Text
                            style={{
                                fontWeight: '400',
                                fontSize: 14,
                                color: Color.colorWhite,
                                marginTop: 3,
                                textAlign: 'right'
                            }}>{message}</Text>
                    </View>
                </TouchableOpacity>
                {this.werewolfGameMoleLock(type)}
            </View>
        );

    }
    /**
     *
     * @param type
     * @returns {*}
     */
    werewolfGameMoleLock = (type) => {
        let needShowLock = false;
        let lockMessage = '';
        //TODO 去掉 各个板子枷锁
        // switch (type) {
        //     case MiniGameConstant.WEREWOLF_HOME_OPEN_NOVICE:
        //         if (this.getUserLevel() > 15 && !this.checkIsRole()) {
        //             needShowLock = true;
        //             lockMessage = Language().limit_novice_grade;
        //         }
        //         break;
        //     case MiniGameConstant.WEREWOLF_HOME_OPEN_SIMPLE:
        //         if (this.getUserLevel() < 3) {
        //             needShowLock = true;
        //             lockMessage = Language().limit_simple_grade;
        //         }
        //         break;
        //     case MiniGameConstant.WEREWOLF_HOME_OPEN_ADVANCED:
        //         if (this.getUserLevel() < 12) {
        //             needShowLock = true;
        //             lockMessage = Language().limit_advanced_grade;
        //         }
        //         break;
        // }
        return needShowLock ? (<View flexDirection='row'
                                     style={{
                                         backgroundColor: 'rgba(0, 0, 0, 0.4)',
                                         position: 'absolute',
                                         justifyContent: 'center',
                                         width: ScreenWidth - 20,
                                         height: (ScreenWidth - 20) * 0.26,
                                         borderRadius: 5
                                     }}>
            <TouchableOpacity
                style={{backgroundColor: Color.transparent, justifyContent: 'center', flex: 1}}
                onPress={() => this.onClick(type)}
                activeOpacity={0.8}>
                <View flexDirection='row' style={{alignSelf: 'center'}}>
                    <LoadImageView source={ImageManager.ic_werewolf_model_lock}
                                   style={{resizeMode: 'contain', width: 32, height: 32, alignSelf: 'center'}}/>
                    <Text
                        style={{
                            fontWeight: '400',
                            fontSize: 20,
                            color: Color.colorWhite,
                            alignSelf: 'center',
                            marginLeft: 2
                        }}>{lockMessage}</Text>
                </View>
            </TouchableOpacity>
        </View>) : null;
    }
    /**
     *
     * @param type
     * @param image
     * @param message
     * @param left
     * @param backgroundColor1
     * @param backgroundColor2
     * @returns {XML}
     */
    gameAuxiliaryViewItem = (type, message, left, backgroundColor1, backgroundColor2) => {
        return (<View
                style={{
                    width: (ScreenWidth - 30) / 3,
                    height: (ScreenWidth - 30) * 0.45 / 3,
                    marginLeft: left,
                    borderRadius: 5,
                    backgroundColor: backgroundColor2
                }}>
                <View
                    style={{
                        width: (ScreenWidth - 30) / 3,
                        height: (ScreenWidth - 30) * 0.45 / 3 - 5,
                        borderRadius: 5,
                        backgroundColor: backgroundColor1
                    }}>
                    <TouchableOpacity
                        style={{backgroundColor: Color.transparent, justifyContent: 'center', flex: 1}}
                        onPress={() => this.onClick(type)}
                        activeOpacity={0.8}>
                        <View
                            style={{justifyContent: 'center', alignSelf: 'center', margin: 10}}>
                            <Text
                                style={{
                                    fontWeight: '400',
                                    fontSize: 18,
                                    color: Color.colorWhite,
                                    marginTop: 0
                                }}>{message}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );

    }

    /**
     * 游戏辅助模块view
     * @param imageTW
     */
    gameAuxiliaryView = () => {
        return (
            <View flexDirection='row' style={[styles.game_home_view_style]}>
                {this.gameAuxiliaryViewItem(MiniGameConstant.GAME_HOME_CREATE, Language().create, 0, '#ec8b93', '#db7179')}
                {this.gameAuxiliaryViewItem(MiniGameConstant.GAME_HOME_SEARCH, Language().search, 5, '#79bfe6', '#4398c7')}
                {this.gameAuxiliaryViewItem(MiniGameConstant.GAME_HOME_HELP, Language().game_help, 5, '#fe9b87', '#d37d6c')}
            </View>
        );
    }

    /**
     * 个人财富view
     */
    myWealthView = (gold, dim) => {
        return (<View flexDirection='row' style={{alignSelf: 'center'}}>
            <TouchableOpacity
                onPress={() => {
                    this.onOpenBox()
                }}
                style={{backgroundColor: Color.transparent, justifyContent: 'center'}}
                activeOpacity={0.8}>
                <CachedImage source={ImageManager.goldIcon} style={styles.homeCurrency}/>
                <View flexDirection='row'
                      style={{
                          backgroundColor: Color.transparent,
                          justifyContent: 'center',
                          alignSelf: 'center',
                          width: (ScreenWidth * 0.45 - 10) / 2,
                          height: (ScreenWidth * 0.45 - 10) / 2 * 0.296,
                      }}>
                    <Text numberOfLines={1}
                          style={{
                              fontSize: 9,
                              paddingLeft: numberPaddingLeft,
                              alignSelf: 'center',
                              flex: 1,
                              color: '#700018',
                              backgroundColor: Color.transparent
                          }}>{gold}</Text>
                    <CachedImage source={ImageManager.addGold} style={styles.homeAddCurrency}/>
                </View>

            </TouchableOpacity>
            <TouchableOpacity
                onPress={this.openStoreRecharge.bind(this)}
                style={{backgroundColor: Color.transparent, justifyContent: 'center', marginLeft: 5}}
                activeOpacity={0.8}>
                <CachedImage source={ImageManager.jewelIcon} style={styles.homeCurrency}/>

                <View flexDirection='row'
                      style={{
                          backgroundColor: Color.transparent,
                          justifyContent: 'center',
                          alignSelf: 'center',
                          width: (ScreenWidth * 0.45 - 10) / 2,
                          height: (ScreenWidth * 0.45 - 10) / 2 * 0.296,
                          marginTop: 0.5
                      }}>
                    <Text numberOfLines={1}
                          style={{
                              fontSize: 9,
                              paddingLeft: numberPaddingLeft,
                              alignSelf: 'center',
                              flex: 1,
                              color: '#283ebb',
                              backgroundColor: Color.transparent
                          }}>{dim}</Text>
                    <CachedImage source={ImageManager.addJewel} style={[styles.homeAddCurrency]}/>
                </View>
            </TouchableOpacity>
        </View>);
    }

    /**
     * 监测是否shi
     * @returns {boolean}
     */
    checkIsNovice() {
        if (this.props.userInfo && this.props.userInfo.game) {
            let game = this.props.userInfo.game;
            if (game.level <= 15) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    login() {
        this.clearData();
        this.onHideLoginDialog();

    }

    /**
     *
     */
    dismissTouristDialog() {
        if (this.refs.touristModal) {
            this.refs.touristModal.hide();
        }
        if (this.state.touristCanEnterOtherRoom) {
            //进入游戏
            this.onShowProgressDialog();
            this.enterRoom(this.state.roomId);
            //统计
            rnRoNativeUtils.onStatistics(Constant.SIMPLE_MODE);
        }
    }

    /**
     *
     */
    toLogin() {
        this.clearData();
        if (this.refs.touristModal) {
            this.refs.touristModal.hide();
        }
    }

    /**
     *  清理数据缓存
     */
    clearData() {
        const routes = this.props.navigation.routes;
        if (routes.length > 1) {
            for (var i = 0; i < routes.length; i++) {
                this.props.finishPage('global');
            }
        }
        if (this.props.userInfo != null && this.props.userInfo.id != null) {
            let userId = this.props.userInfo.id;
            rnRoNativeUtils.closeLeanCloud(userId);
        }
        AsyncStorageTool.removeUserInfo();
        try {
            this.props.openPage(SCENES.SCENE_SPLASH, 'global');
        } catch (e) {
            console.log(e)
        }
        AsyncStorageTool.removeSignTourist();
        this.props.removeChatData();
        this.props.updateUserInfo(null);
        this.props.updateTourist(false);
        this.props.clearFriendList();
        this.props.removeFriendStatus();
        this.props.clearMessageList();
    }

    onShowLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.show();
        }
    }

    onHideLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.hide();
        }
    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     * 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    checkToken() {
        if (this.props.code == 1004) {
            //token 失效
            try {
                this.onShowLoginDialog();
            } catch (error) {
                console.log('token error show:' + error);
            }
        }
    }

    /**
     * 充值
     */
    openStoreRecharge = () => {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().openStore_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                //如果flag判断不是游客，但是确实用的游客的ID或是token，直接提示信息失效去登录
                if (this._checkTourist()) {
                    this.onShowLoginDialog();
                } else {
                    if (Platform.OS === 'ios') {
                        rnRoNativeUtils.openStoreRecharge();
                    } else {
                        Toast.show(Language().temporarily_not_opened);
                    }
                }
            }
        });
    }

    /**
     * 游戏离开房间通知更新
     */
    onLeaveGameRoom() {
    }

    _keyboardDidShow() {

    }

    _keyboardDidHide() {
    }

    handleMethod(isConnected) {
        console.log('test', (isConnected ? 'online' : 'offline'));
    }

    /**
     *
     */
    onOpenUserInfo() {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().content_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                this.props.openPage(SCENES.SCENE_CONTACT_DETAIL, 'global');
            }
        });

    }

    /**
     *
     */
    onSearchRoom = () => {
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().search_prompt_tourist_login});
            }
            this.state.touristCanEnterOtherRoom = false;
        } else {
            this.setState({
                dialogIndex: 0,
                search_dialog_activity: 0,
                dialogTitle: Language().search_room,
                dialogConfirm: Language().enter,
                dialogCancel: Language().cancel,
                hideCancel: false,
                hideConfirm: false,
                password: ''
            });
            this.onOpenModal();
            //统计
            rnRoNativeUtils.onStatistics(Constant.SEARCH_ROOM);
        }
    }

    /**
     * 搜索房间的具体操作
     */
    doSearchRoom() {
        //网络检测
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                try {
                    if (this.state.textRoomID == '') {
                        Toast.show(Language().room_number_empty);
                    } else {
                        this.setState({
                            showModelProgress: true
                        });
                        let data = {
                            room_id: this.state.textRoomID
                        }
                        this.state.roomId = this.state.textRoomID;
                        let url = apiDefines.SEARCH_ROOM_INFO + this.state.roomId + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id;
                        if (this.props.hostLocation) {
                            url = url + apiDefines.PARAMETER_LOCATION + this.props.hostLocation
                        }
                        Util.get(url, (code, message, data) => {
                            this.setState({
                                showModelProgress: false
                            });
                            if (code == 1000) {
                                //TODO 是否能进入也需要处理
                                if (!this.checkCanEnterGame(data.level)) {
                                    return;
                                }
                                this.state.game_type = data.level;
                                this.state.roomId = data.room_id;
                                if (data.password_needed) {
                                    this.setState({
                                        needPassword: data.password_needed
                                    });
                                } else {
                                    //直接进入
                                    this.onShowProgressDialog();
                                    this.enterRoom(this.state.roomId);
                                    this.onCloseModal();
                                }

                            }
                            else if (code == 1004) {
                                //token 失效
                                this.onShowLoginDialog();
                            } else {
                                Toast.show(message);
                            }
                        }, (failed) => {
                            this.onHideProgressDialog();
                            Toast.show(Language().not_network)
                        });
                    }
                } catch (e) {
                    console.log('do search room error:' + e);
                }

            } else {
                //无网络提示
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 检测等级能否进入游戏
     * @param type
     *
     GAME_TYPE_SIMPLE_6 = 'simple_6';//简单6人局（简单）
     GAME_TYPE_SIMPLE_9 = 'simple_9';//简单9人局（简单）
     GAME_TYPE_SIMPLE_10 = 'simple_10';//简单10人局（简单）
     GAME_TYPE_NORMAL = 'normal';//标准12人丘比特（标准）
     const GAME_TYPE_NORMAL_GUARD = 'normal_guard';//标准12人守卫（标准)
     const GAME_TYPE_HIGH_KING = 'high_king';//12高级房间（高阶模式）
     const GAME_TYPE_PRE_SIMPLE = 'pre_simple';//6-10局（新手模式）
     const GAME_TYPE_PRE_SIMPLE_NEW = 'pre_simple_new';//6人猎人局（新手模式）
     */
    checkCanEnterGame = (type) => {
        let configType;
        if (this.props.configData) {
            if (this.props.configData.type_config) {
                configType = this.props.configData.type_config;
            }
        }
        if (!configType) {
            return true;
        }
        let gameType = configType[type];
        if (!gameType) {
            return true;
        }
        let canEnterRoom = true;
        if (this.getUserLevel() < gameType.min_level) {
            Toast.show(gameType.min_level + '级以上开启');
            canEnterRoom = false;
        } else if (gameType.max_level != -1 && this.getUserLevel() > gameType.max_level) {
            Toast.show(gameType.max_level + '级以下开启');
            canEnterRoom = false;
        }
        return canEnterRoom;
    }
    /**
     *
     * @param type
     * @returns {boolean}
     */
    checkNeedShowGame = (type) => {
        let showGame = true;
        let configType;
        if (this.props.configData) {
            if (this.props.configData.type_config) {
                configType = this.props.configData.type_config;
            }
        }
        if (!configType) {
            return true;
        }
        let gameType = configType[type];
        if (!gameType) {
            return true;
        } else {
            showGame = gameType.show;
        }
        return showGame;
    }

    /**
     *
     * @param text
     */
    onChangePassword(text) {
        if (/^[\d]+$/.test(text)) {
            this.setState({
                password: text
            });
        } else {
            if (/^[\d{1}]$/.test(this.state.password)) {
                this.setState({
                    password: ''
                });
            }
        }
    }

    /**
     *
     * @param text
     */
    changeRoomId(text) {
        let format = /^[0-9]*$/;
        if (format.test(text)) {
            this.setState({
                textRoomID: text
            });
        }
    }

    /**
     * 开启宝箱
     */
    onOpenBox = () => {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().openBox_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                //如果flag判断不是游客，但是确实用的游客的ID或是token，直接提示信息失效去登录
                if (this._checkTourist()) {
                    this.onShowLoginDialog();
                } else {
                    rnRoNativeUtils.openBox();
                }
            }
        });
    }
    /**
     * 战绩详情
     */
    openRecordDetails = () => {
        // 启动战绩
        // action "standings"
        // type "RN_Native"
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().record_detail_prompt_tourist_login});
            }
            this.state.touristCanEnterOtherRoom = false;
        } else {
            let NativeData = {
                action: Constant.STANDINGS,
                type: Constant.RN_NATIVE
            }
            rnRoNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
        }
    }

    /**
     * 判断是否是
     * @returns {boolean}
     * @private
     */
    _checkTourist() {
        if (this.props.userInfo) {
            let token = this.props.userInfo.token.access_token;
            let userId = this.props.userInfo.id;
            if (token == apiDefines.DEFAULT_ACCESS_TOKEN || userId == apiDefines.DEFAULT_USER_ID) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    onCloseModal() {
        this.setState({
            dialogShow: false,
            switchIsOn: false,
            password: '',
            roomId: '',
            simple_model_radio: true,
            standard_model_radio: false,
            game_type: GAME_TYPE_SIMPLE_6,
            needPassword: false,
            textRoomID: '',
            showModelProgress: false,
            activityGameMode: GAME_MODEL_SIMPLE,
            game_limit_level: 0,
            game_limit_level_name: Language().no_limit,
            select_game_play: Language().game_simple_6,
            dialogIndex: 0,
            search_dialog_activity: 0
        });

    }

    onSubmitEditing() {
        this.onConfirmModal();
    }

    onConfirmModal() {
        try {
            if (this.state.dialogIndex == 0 || this.state.dialogIndex == 2) {
                //搜索房间后进入
                if (this.state.search_dialog_activity == 0) {
                    if (this.state.needPassword) {
                        let password = this.state.password;
                        password = password.trim();
                        if (!password) {
                            Toast.show(Language().placeholder_password);
                        } else {
                            this.onShowProgressDialog();
                            this.enterRoom(this.state.roomId);
                            this.onCloseModal();
                        }
                    } else {
                        this.doSearchRoom();
                    }
                } else {
                    this.requestSearch();
                }
            } else if (this.state.dialogIndex == 1) {
                //创建房间进入
                if (this.checkMoney()) {
                    let roomType = this.state.game_type;
                    if (this.state.switchIsOn && this.state.password == '') {
                        Toast.show(Language().room_password_empty);
                    } else {
                        let password = this.state.password;
                        this.onCloseModal();
                        let url = apiDefines.CREATE_ROOM + roomType;
                        if (this.props.hostLocation) {
                            url = url + apiDefines.PARAMETER_LOCATION + this.props.hostLocation
                        }
                        if (password != '' && password != null) {
                            url = url + apiDefines.ROOM_PASSWORD + this.state.password;
                        }
                        if (this.state.game_limit_level) {
                            url = url + apiDefines.PARAMETER_LIMIT_LEVEL + this.state.game_limit_level;
                        }
                        this.onShowProgressDialog();
                        Util.get(url, (code, message, data) => {
                            if (code == 1000) {
                                if (data != null) {
                                    this.state.roomId = data.room_id;
                                    this.state.password = data.password;
                                    this.enterCreateRoom(data.room_id, data.password, roomType);
                                }
                            } else if (code == 1004) {
                                this.onHideProgressDialog();
                                //token 失效
                                this.onShowLoginDialog();
                            } else {
                                Toast.show(message);
                                //请求失败，dismiss进度对话框
                                this.onHideProgressDialog();
                            }
                        }, (failed) => {
                            this.onHideProgressDialog();
                            Toast.show(Language().not_network);
                        });
                    }
                } else {
                    Toast.show(Language().not_enough_gold);
                }

            }
        } catch (e) {
            console.log('onConfirmModal error:' + e);
        }
    }

    onOpenModal() {
        this.setState({dialogShow: true});
    }

    /**
     *
     */
    onCreateRoom = () => {
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().prompt_create_room});
            }
            this.state.touristCanEnterOtherRoom = false;
        } else {
            //请求用户详情，创建房间需要一百金币
            if (this.props.userInfo) {
                this.props.getUserInfo(this.props.userInfo.id);
            }
            this.doCreateRoom();
            //统计
            rnRoNativeUtils.onStatistics(Constant.CREATE_ROOM);
        }
    }
    /**
     * 创建房间的具体操作
     */
    doCreateRoom = () => {
        let createRoomConfig;
        let costConfig;
        let configData = this.props.configData;
        if (configData) {
            createRoomConfig = configData.create_room;
            if (createRoomConfig && createRoomConfig.cost) {
                costConfig = createRoomConfig.cost;
            }
        }
        if (costConfig && costConfig.count) {
            //创建房间需要金币
            this.state.create_room_count = costConfig.count;
        }
        if (this.state.create_room_count == 0) {
            this.props.updateConfig();
        }
        //网络检测
        //创建房间的时候默认为简单6人局
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                this.setState({
                    dialogIndex: 1,
                    dialogTitle: Language().create_room,
                    dialogConfirm: Language().confirm,
                    dialogCancel: Language().cancel,
                    hideCancel: false,
                    hideConfirm: false,
                    game_type: GAME_TYPE_SIMPLE_6
                });
                this.onOpenModal();
            } else {
                //无网络提示
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    //进入房间
    enterRoom(roomId) {
        if (Platform.OS === 'ios') {
            //IOS 进入游戏房间
            this.onHideProgressDialog();
            var main = NativeModules.MainViewController;
            let userImage = this.props.userInfo.image;
            if (userImage == null) {
                userImage = '';
            }
            if (this.state.showGameModelManager) {
                this.closeGameModelManager();
            }
            main.enter(this.state.password, this.state.game_type, roomId, this.props.userInfo.name, this.props.userInfo.id, userImage, this.props.token);
        } else {
            try {
                let experience = 0;
                let sex = 2;
                if (this.props.userInfo != null) {
                    if (this.props.userInfo.game != null && this.props.userInfo.game.experience != null) {
                        experience = this.props.userInfo.game.experience;
                    }
                    if (this.props.userInfo.sex == 1 || this.props.userInfo.sex == 2) {
                        sex = this.props.userInfo.sex;
                    }
                }
                NativeModules.NativeJSModule.rnStartGame(this.state.password, this.state.game_type, roomId, this.props.userInfo.name, this.props.userInfo.id, sex, this.props.userInfo.image, experience, this.props.token, successinfo => {
                    this.onHideProgressDialog();
                    if (this.state.showGameModelManager) {
                        this.closeGameModelManager();
                    }
                }, failed => {
                    this.onHideProgressDialog();
                    Toast.show(failed);
                });
            } catch (error) {
                this.onHideProgressDialog();
                Toast.show(Language().enter_room_error);
            }
        }
    }

    /**
     * 检查是否是管理员
     * @returns {boolean}
     */
    checkIsRole() {
        let type = 0;
        if (this.props.userInfo) {
            if (this.props.userInfo.role) {
                type = this.props.userInfo.role.type
            }
            if ((type & roleType.manager) != 0 || (type & roleType.teacher) != 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
     * 獲取用户等级
     * @returns {number}
     */
    getUserLevel() {
        let lever = 0;
        if (this.props.userInfo != null && this.props.userInfo.game != null && this.props.userInfo.game.level != null) {
            lever = this.props.userInfo.game.level;
        }
        return lever;
    }

    /**
     * 搜素弹框view
     */
    /**
     * 搜素弹框view
     */
    searchDialogView() {
        let activity = this.state.search_dialog_activity;
        let placeholderText = Language().placeholder_room_number;
        if (activity == 1) {
            placeholderText = Language().placeholder_contact_number;
        } else if (activity == 2) {
            placeholderText = Language().placeholder_group_id;
        }
        let passwordContent = (
            <Input placeholder={Language().placeholder_room_password}
                   style={{
                       borderColor: '#f29b76',
                       borderWidth: StyleSheet.hairlineWidth,
                       borderRadius: 5,
                       height: 35,
                       width: ScreenWidth / 3 * 2 - 20,
                       alignSelf: 'center',
                       fontSize: 14,
                       paddingTop: 5,
                       paddingBottom: 5,
                       paddingLeft: 10,
                       paddingRight: 10,
                       marginTop: 10
                   }}
                   onChangeText={(text) => this.onChangePassword(text)}
                   placeholderTextColor='#cacaca'
                   autoFocus={true} maxLength={4}
                   keyboardType='numeric'
                   value={this.state.password}/>);
        return (<View>
            <View flexDirection='row' style={{justifyContent: 'center'}}>
                <TouchableOpacity activeOpacity={0.6} onPress={() => this.activitySearchIndex(0)}
                                  style={{
                                      backgroundColor: Color.transparent,
                                      justifyContent: 'center',
                                      flex: 1,
                                      height: 40
                                  }}>
                    <View flexDirection='row'
                          style={{
                              justifyContent: 'center',
                              padding: 5,
                              flex: 1,
                              backgroundColor: activity == 0 ?'#ffdb42' : Color.transparent,
                              borderTopLeftRadius: 10
                          }}>
                        <Image
                            source={activity == 0 ? ImageManager.searchRoomIconActivity : ImageManager.searchRoomIcon}
                            style={[styles.actionRoomIcon]}/>
                        <Text
                            style={{
                                color: activity == 0 ? '#ff4d5f' : '#ff8a96',
                                alignSelf: 'center',
                                fontSize: 17,
                                fontWeight: '500',
                                textAlign: 'center',
                                padding: 5
                            }}>{Language().recreation_text_room}</Text>
                    </View>
                </TouchableOpacity>
                <View style={{width: StyleSheet.hairlineWidth, backgroundColor:'#dbb57c',alignSelf:'center',height:32}}/>
                <TouchableOpacity activeOpacity={0.6} onPress={() => this.activitySearchIndex(1)}
                                  style={{
                                      backgroundColor: Color.transparent,
                                      justifyContent: 'center',
                                      flex: 1,
                                      height: 40
                                  }}>
                    <View flexDirection='row'
                          style={{
                              justifyContent: 'center',
                              padding: 5,
                              flex: 1,
                              backgroundColor: activity == 1 ?'#ffdb42' : Color.transparent,
                          }}>
                        <Image
                            source={activity == 1 ? ImageManager.searchUserIconActivity : ImageManager.searchUserIcon}
                            style={[styles.actionRoomIcon]}/>
                        <Text
                            style={{
                                color: activity == 1 ? '#4570ff' :'#8aa5ff',
                                alignSelf: 'center',
                                fontSize: 17,
                                fontWeight: '500',
                                textAlign: 'center',
                                padding: 5
                            }}>{Language().contact}</Text>
                    </View>
                </TouchableOpacity>
                <View style={{width: StyleSheet.hairlineWidth, backgroundColor:'#dbb57c',alignSelf:'center',height:32}}/>
                <TouchableOpacity activeOpacity={0.6} onPress={() => this.activitySearchIndex(2)}
                                  style={{
                                      backgroundColor: Color.transparent,
                                      justifyContent: 'center',
                                      flex: 1,
                                      height: 40
                                  }}>
                    <View flexDirection='row'
                          style={{
                              justifyContent: 'center',
                              padding: 5,
                              flex: 1,
                              backgroundColor: activity == 2 ?'#ffdb42' : Color.transparent,
                              borderTopRightRadius: 10
                          }}>
                        <Image
                            source={activity == 2 ? ImageManager.searchGroupIconActivity : ImageManager.searchGroupIcon}
                            style={[styles.actionRoomIcon]}/>
                        <Text
                            style={{
                                color: activity == 2 ?'#8543ff' : '#b38aff',
                                alignSelf: 'center',
                                fontSize: 17,
                                fontWeight: '500',
                                textAlign: 'center',
                                padding: 5
                            }}>{Language().family}</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View
                style={{
                    marginLeft: 3,
                    marginRight: 3,
                    backgroundColor: (Color.transparent),
                    paddingBottom: 20,
                    paddingTop: 30,
                    justifyContent: 'center'
                }}>
                <Input placeholder={placeholderText}
                       onChangeText={(text) => this.changeRoomId(text)}
                       placeholderTextColor='#cacaca'
                       style={{
                           borderColor: '#f29b76',
                           borderWidth: StyleSheet.hairlineWidth,
                           borderRadius: 5,
                           height: 35,
                           width: ScreenWidth / 3 * 2 - 20,
                           alignSelf: 'center',
                           fontSize: 14,
                           paddingTop: 5,
                           paddingBottom: 5,
                           paddingLeft: 10,
                           paddingRight: 10
                       }}
                       keyboardType='numeric'
                       value={this.state.textRoomID}/>
                {this.state.needPassword ? passwordContent : null}
            </View>
        </View>);
    }

    /**
     *
     * @param index
     */
    activitySearchIndex(index) {
        let dialogConfirmText = Language().confirm;
        if (index == 0) {
            dialogConfirmText = Language().enter;
        }
        that.setState({
            search_dialog_activity: index,
            dialogConfirm: dialogConfirmText
        });
    }

    /**
     * 在线好友进入游戏需要密码
     */
    needPasswordDialogView() {
        return (<View>
            <View flexDirection='row' style={{justifyContent: 'center', padding: 5}}>
                <Text
                    style={{
                        color: '#1c1c25',
                        alignSelf: 'center',
                        fontSize: 17,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        padding: 5
                    }}>{Language().room_number + this.state.textRoomID}</Text>
            </View>
            <View
                style={{
                    marginLeft: 3,
                    marginRight: 3,
                    backgroundColor: (Color.transparent),
                    paddingBottom: 10,
                    paddingTop: 5,
                    justifyContent: 'center'
                }}>
                <Input placeholder={Language().placeholder_room_password}
                       style={{
                           borderColor: '#f29b76',
                           borderWidth: StyleSheet.hairlineWidth,
                           borderRadius: 5,
                           height: 35,
                           width: ScreenWidth / 3 * 2 - 20,
                           alignSelf: 'center',
                           fontSize: 14,
                           paddingTop: 5,
                           paddingBottom: 5,
                           paddingLeft: 10,
                           paddingRight: 10,
                           marginTop: 10
                       }}
                       onChangeText={(text) => this.onChangePassword(text)}
                       placeholderTextColor='#cacaca'
                       autoFocus={true} maxLength={4}
                       keyboardType='numeric'
                       value={this.state.password}/>
            </View>
        </View>);
    }

    /**
     *搜索房间／好友在玩匹配进入弹框
     */
    dialogContentManager() {
        switch (this.state.dialogIndex) {
            case 0:
                return this.searchDialogView();
            case 1:
                return this.newCreateGameView();
            case 2:
                return this.needPasswordDialogView();
        }
    }

    /**
     * 创建房间检测金币是否足够
     * @returns {boolean}
     */
    checkMoney() {
        if (this.state.create_room_count == 0) {
            return true;
        }
        let userInfo = this.props.userInfo;
        if (userInfo && userInfo.money) {
            if (userInfo.money.gold >= 100) {
                return true;
            } else if (userInfo.money.dim) {
                return (userInfo.money.gold + userInfo.money.dim * 66) >= 100;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    //进入房间
    enterCreateRoom(roomId, password, gameType) {
        if (Platform.OS === 'ios') {
            //IOS 进入游戏房间
            this.onHideProgressDialog();
            var main = NativeModules.MainViewController;
            let userImage = this.props.userInfo.image;
            if (userImage == null) {
                userImage = '';
            }
            main.enter(password, gameType, roomId, this.props.userInfo.name, this.props.userInfo.id, userImage, this.props.token);
            this.state.password = '';
        } else {
            try {
                let experience = 0;
                let sex = 2;
                if (this.props.userInfo != null) {
                    if (this.props.userInfo.game != null && this.props.userInfo.game.experience != null) {
                        experience = this.props.userInfo.game.experience;
                    }
                    if (this.props.userInfo.sex == 1 || this.props.userInfo.sex == 2) {
                        sex = this.props.userInfo.sex;
                    }
                }
                NativeModules.NativeJSModule.rnStartGame(password, gameType, roomId, this.props.userInfo.name, this.props.userInfo.id, sex, this.props.userInfo.image, experience, this.props.token, successinfo => {
                    this.onHideProgressDialog();
                }, failed => {
                    this.onHideProgressDialog();
                    Toast.show(failed);
                });
                this.state.password = '';
            } catch (error) {
                this.onHideProgressDialog();
                Toast.show(Language().enter_room_error);
                this.state.password = '';
            }
        }
    }

    /**
     * 创建房间新view
     */
    newCreateGameView() {
        let setPassword = (<View
            style={{
                margin: 3,
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: '#1c1c25',
                marginLeft: 30,
                marginRight: 35,
                marginBottom: 8
            }}>
            <Input
                style={{
                    paddingLeft: 0,
                    color: '#1c1c25',
                    fontSize: 14,
                    height: 30,
                    padding: 5,
                    paddingLeft: 0,
                }}
                placeholder={Language().placeholder_set_password}
                placeholderTextColor={'#cacaca'}
                autoFocus={true} maxLength={4}
                onChangeText={(text) => this.onChangePassword(text)}
                onSubmitEditing={this.onSubmitEditing.bind(this)}
                keyboardType='numeric'
                value={this.state.password}/>
        </View>);
        let createRoomCountView = null;
        if (this.state.create_room_count > 0) {
            createRoomCountView = (<View flexDirection='row' style={{justifyContent: 'center', marginTop: 5}}>
                <Text
                    style={{
                        fontSize: 10,
                        alignSelf: 'center',
                        color: Color.setting_text_color
                    }}>{Language().create_room_need_gold}</Text>
                <Image source={ImageManager.createRoomGold}
                       style={{width: 25, height: 25, resizeMode: 'contain', alignSelf: 'center'}}/>
                <Text
                    style={{
                        fontSize: 10,
                        alignSelf: 'center',
                        color: Color.setting_text_color
                    }}>{this.state.create_room_count + Language().one_hundred_gold}</Text>
            </View>);
        }
        return (<View>
            <View flexDirection='row' style={{justifyContent: 'center', padding: 5}}>
                <Image source={ImageManager.searchRoomIcon} style={styles.actionRoomIcon}/>
                <Text
                    style={{
                        color: '#1c1c25',
                        alignSelf: 'center',
                        fontSize: 17,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        padding: 5
                    }}>{this.state.dialogTitle}</Text>
            </View>
            <Text
                style={{
                    alignSelf: 'center',
                    fontSize: 14,
                    color:'#1c1c25',
                    fontWeight: 'bold',
                    padding: 5,
                    marginTop: 10
                }}>{Language().set_game_model}</Text>
            <View style={styles.set_game_rule_view}>
                <Text
                    style={{
                        flex: 1,
                        alignSelf: 'center',
                        fontSize: 14,
                        color:'#1c1c25',
                        textAlign: 'center'
                    }}>{this._getSelectGameModelName()}</Text>
                {this._selectGameModel()}
            </View>
            <Text
                style={{
                    alignSelf: 'center',
                    fontSize: 14,
                    color:'#1c1c25',
                    fontWeight: 'bold',
                    padding: 5
                }}>{Language().set_game_play}</Text>
            <View style={styles.set_game_rule_view}>
                <Text
                    style={{
                        flex: 1,
                        alignSelf: 'center',
                        fontSize: 14,
                        color:'#1c1c25',
                        textAlign: 'center'
                    }}>{this.state.select_game_play}</Text>
                {this._selectGamePlay()}
            </View>
            <Text
                style={{
                    alignSelf: 'center',
                    fontSize: 14,
                    color:'#1c1c25',
                    fontWeight: 'bold',
                    padding: 5
                }}>{Language().set_level_limit}</Text>
            <View style={styles.set_game_rule_view}>
                <Text
                    style={{
                        flex: 1,
                        alignSelf: 'center',
                        fontSize: 14,
                        color:'#1c1c25',
                        textAlign: 'center'
                    }}>{this.state.game_limit_level_name}</Text>
                {this._selectGameLimit()}
            </View>
            {createRoomCountView}
            <View flexDirection='row'
                  style={{
                      justifyContent: 'center',
                      marginTop: 5,
                      alignItems: 'center',
                      marginLeft: 35,
                      marginRight: 35,
                      marginBottom: 5
                  }}>
                <Text
                    style={{
                        color:'#1c1c25',
                        fontSize: 14,
                        marginRight: 15,
                        flex: 1
                    }}>{Language().set_password}</Text>
                <Switch onValueChange={(value) => this.setState({switchIsOn: value})}
                        value={this.state.switchIsOn}/>
            </View>
            {this.state.switchIsOn ? setPassword : null}
        </View>);

    }

    /**
     *设置游戏模式
     * @returns {*}
     * @private
     */
    _selectGameModel() {
        let selectContent = [{
            name: Language().game_model_simple,
            type: GAME_MODEL_SIMPLE
        }, {name: Language().game_model_standard, type: GAME_MODEL_STANDARD}, {
            name: Language().game_model_advanced,
            type: GAME_MODEL_ADVANCED
        }]
        return (<Dropdown style={[styles.dropDown]}
                          dropdownStyle={[styles.dropdownDropdown, {height: 110}]}
                          options={selectContent}
                          renderRow={this._gameModelRenderRow.bind(this)}
                          onSelect={(idx, value) => this._gameModelOnSelect(idx, value)}>
            <View style={styles.dropDownView}>
                <Image style={styles.game_arrow} source={ImageManager.game_arrow_icon}/>
            </View>
        </Dropdown>);
    }

    /**
     *  游戏模式item
     * @param rowData
     * @param rowID
     * @param highlighted
     * @returns {XML}
     * @private
     */
    _gameModelRenderRow(rowData, rowID, highlighted) {
        return (
            <TouchableOpacity activeOpacity={0.6}>
                <View style={[styles.dropdownRow]}>
                    <Text
                        style={{fontSize: 13, flex: 1, textAlign: 'center', color: Color.createRoomDialogTextColor}}>
                        {rowData.name}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    /**
     * 选中的对应的游戏类型
     * @param idx
     * @param value
     * @private
     */
    _gameModelOnSelect(idx, value) {
        let gamePlay = Language().game_simple_6;
        let gameType = GAME_TYPE_SIMPLE_6;
        if (value.type == GAME_MODEL_STANDARD) {
            gamePlay = Language().normal_guard;
            gameType = GAME_TYPE_NORMAL_GUARD;
        } else if (value.type == GAME_MODEL_ADVANCED) {
            gamePlay = Language().high_king;
            gameType = GAME_TYPE_HIGH_KING;
        }
        this.setState({
            activityGameMode: value.type,
            select_game_play: gamePlay,
            game_type: gameType,
            game_limit_level: 0,
            game_limit_level_name: Language().no_limit
        })
    }

    /**
     *设置游戏玩法
     * @returns {*}
     * @private
     */
    _selectGamePlay() {
        let selectContent = [{
            name: Language().game_simple_6,
            type: GAME_TYPE_SIMPLE_6
        }, {name: Language().game_simple_9, type: GAME_TYPE_SIMPLE_9}, {
            name: Language().game_simple_10,
            type: GAME_TYPE_SIMPLE_10
        }]
        if (this.state.activityGameMode == GAME_MODEL_STANDARD) {
            selectContent = [{
                name: Language().normal_guard,
                type: GAME_TYPE_NORMAL_GUARD
            }, {name: Language().normal_cupid, type: GAME_TYPE_NORMAL}]
        } else if (this.state.activityGameMode == GAME_MODEL_ADVANCED) {
            selectContent = [{
                name: Language().high_king,
                type: GAME_TYPE_HIGH_KING
            }]
        }
        return (<Dropdown style={[styles.dropDown]}
                          dropdownStyle={[styles.dropdownDropdown, {height: selectContent.length * 30 + 15}]}
                          options={selectContent}
                          renderRow={this._gamePlayRenderRow.bind(this)}
                          onSelect={(idx, value) => this._gamePlayOnSelect(idx, value)}>
            <View style={styles.dropDownView}>
                <Image style={styles.game_arrow} source={ImageManager.game_arrow_icon}/>
            </View>
        </Dropdown>);
    }

    /**
     *  游戏玩法item
     * @param rowData
     * @param rowID
     * @param highlighted
     * @returns {XML}
     * @private
     */
    _gamePlayRenderRow(rowData, rowID, highlighted) {
        return (
            <TouchableOpacity activeOpacity={0.6}>
                <View style={[styles.dropdownRow]}>
                    <Text
                        style={{fontSize: 13, flex: 1, textAlign: 'center', color: Color.createRoomDialogTextColor}}>
                        {rowData.name}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    /**
     * 选中的对应的游戏玩法类型
     * @param idx
     * @param value
     * @private
     */
    _gamePlayOnSelect(idx, value) {
        this.setState({
            game_type: value.type,
            select_game_play: value.name
        })
    }

    /**
     *设置游戏限制
     * @returns {*}
     * @private
     */
    _selectGameLimit() {
        let selectContent = [{
            name: Language().no_limit,
            type: 0
        }, {name: '15' + Language().rank, type: 15}, {name: '30' + Language().rank, type: 30}, {
            name: '50' + Language().rank,
            type: 50
        }];
        if (this.state.activityGameMode == GAME_MODEL_ADVANCED) {
            selectContent = [{
                name: Language().no_limit,
                type: 0
            }, {name: '80' + Language().rank, type: 80}, {name: '120' + Language().rank, type: 120}, {
                name: '200' + Language().rank,
                type: 200
            }];
        } else if (this.state.activityGameMode == GAME_MODEL_STANDARD) {
            selectContent = [{
                name: Language().no_limit,
                type: 0
            }, {name: '30' + Language().rank, type: 30}, {name: '50' + Language().rank, type: 50}, {
                name: '100' + Language().rank,
                type: 100
            }];
        }
        return (<Dropdown style={[styles.dropDown]}
                          dropdownStyle={[styles.dropdownDropdown, {height: selectContent.length * 30 + 15}]}
                          options={selectContent}
                          renderRow={this._gameLimitRenderRow.bind(this)}
                          onSelect={(idx, value) => this._gameLimitOnSelect(idx, value)}>
            <View style={styles.dropDownView}>
                <Image style={styles.game_arrow} source={ImageManager.game_arrow_icon}/>
            </View>
        </Dropdown>);
    }

    /**
     *  游戏玩法item
     * @param rowData
     * @param rowID
     * @param highlighted
     * @returns {XML}
     * @private
     */
    _gameLimitRenderRow(rowData, rowID, highlighted) {
        return (
            <TouchableOpacity activeOpacity={0.6}>
                <View style={[styles.dropdownRow]}>
                    <Text
                        style={{fontSize: 13, flex: 1, textAlign: 'center', color: Color.createRoomDialogTextColor}}>
                        {rowData.name}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    /**
     * 选中的对应的游戏玩法类型
     * @param idx
     * @param value
     * @private
     */
    _gameLimitOnSelect(idx, value) {
        this.setState({
            game_limit_level: value.type,
            game_limit_level_name: value.name
        })
    }

    /**
     *
     * @returns {*}
     * @private
     */
    _getSelectGameModelName = () => {
        switch (this.state.activityGameMode) {
            case GAME_MODEL_SIMPLE:
                return Language().game_model_simple;
            case GAME_MODEL_STANDARD:
                return Language().game_model_standard;
            case GAME_MODEL_ADVANCED:
                return Language().game_model_advanced;
            default:
                return Language().game_model_simple;
        }
    }

    /**
     * 搜索好友／家族
     */
    requestSearch() {
        this.setState({
            showModelProgress: true
        });
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                let activityIndex = this.state.search_dialog_activity;
                let api = apiDefines.GET_FRIEND_INFO_FROM_UID;
                if (activityIndex == 2) {
                    api = apiDefines.GROUP_INFO_FROM_ID;
                }
                let url = api + this.state.textRoomID;
                Util.get(url, (code, message, data) => {
                    this.setState({
                        showModelProgress: false
                    });
                    if (code == 1000) {
                        this.onCloseModal();
                        if (activityIndex == 2) {
                            let group_id = '';
                            if (data && data.group) {
                                group_id = data.group._id;
                            }
                            this.openFamilyDetail(group_id);
                        } else {
                            this.actionContact(data);
                        }

                    } else {
                        Toast.show(message);
                    }
                }, (failed) => {
                    this.setState({
                        showModelProgress: false
                    });
                    Toast.show(netWorkTool.NOT_NETWORK);
                });
            } else {
                this.setState({
                    showModelProgress: false
                });
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 进入家族详情
     * groupId
     */
    openFamilyDetail(groupId) {
        if (groupId) {
            let nativeData = {
                action: Constant.ACTION_SHOW_FAMILY_INFO,
                params: {group_id: groupId},
                type: Constant.RN_NATIVE
            }
            rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
        }
    }

    /**
     * 执行进入房间的操作
     */
    doEnterVoiceRoom(roomData) {
        let NativeData = {
            action: Constant.ACTION_ENTER_AUDIO,
            params: roomData,
            options: null
        }
        rnRoNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }


    /**
     * 好友在玩view
     * @returns {XML}
     */
    friendStatusView() {
        return friendStatusList.length == 0 ? (
            <Text
                style={{
                    padding: 20,
                    fontSize: 14,
                    color: '#fb5c88',
                    alignSelf: 'center'
                }}>{Language().no_friend_online_message}</Text>) : (
            <View style={{flex: 1, paddingTop: 5, paddingBottom: 5}}>
                <View style={{marginLeft: 10, marginRight: 10}}>
                    <ListView
                        ref='friendStatusList'
                        dataSource={this.state.dataSource.cloneWithRows(friendStatusList)}
                        renderRow={this.friendStatusRenderRow}
                        renderFooter={this.renderFooter}
                        removeClippedSubviews={true}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={styles.listViewStyle}/>
                </View>
            </View>);
    }

    /**
     * 换一批
     */
    fetchMoreFriendStatus() {
        if (this.refs.friendStatusList) {
            this.refs.friendStatusList.scrollTo(0, 0);
        }
        this.props.getMoreFriendStatus();
    }

    /**
     * 好友在线信息item
     * @param data
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    friendStatusRenderRow(data, sectionId, rowId) {
        return (<View
            style={{margin: 3, justifyContent: 'center', width: ScreenWidth / 5}}>
            <TouchableOpacity onPress={() => {
                that.actionFriendView(data)
            }}
                              activeOpacity={0.8}
                              style={{backgroundColor: Color.transparent, justifyContent: 'center'}}>
                <View
                    style={{
                        justifyContent: 'center',
                        width: 71,
                        height: 71,
                        alignSelf: 'center',
                        borderRadius: 36,
                        backgroundColor: '#dddddd'
                    }}>
                    <LoadImageView style={{width: 70, height: 70, alignSelf: 'center', borderRadius: 35}}
                                   defaultSource={ImageManager.defaultUser}
                                   source={(data.image == '' || data.image == null) ? ImageManager.defaultUser : ({uri: data.image})}/>
                    {that.gameStatusView(data)}
                </View>
                <Text numberOfLines={1}
                      style={{
                          alignSelf: 'center',
                          fontSize: 12,
                          margin: 2,
                          marginTop: 8,
                          color: '#454545'
                      }}>{data.name}</Text>
                <View flexDirection='row'
                      style={{justifyContent: 'flex-end', width: 70, height: 18, alignSelf: 'center'}}>
                    <View
                        style={{
                            backgroundColor: '#fb5c88',
                            borderRadius: 3,
                            justifyContent: 'center',
                            width: 55,
                            height: 14,
                            alignSelf: 'center'
                        }}>
                        <Text numberOfLines={1}
                              style={{
                                  fontSize: 10,
                                  color: Color.colorWhite,
                                  alignSelf: 'center',
                                  backgroundColor: Color.transparent
                              }}>{that.formatGameType(data.status)}</Text>
                    </View>
                    <LoadImageView
                        style={{width: 14, height: 15, alignSelf: 'center', resizeMode: 'contain', marginLeft: 1}}
                        source={ImageManager.icButton}/>
                </View>
            </TouchableOpacity>
        </View>);
    }

    /**
     * 当前游戏状态view
     * 只有是游戏房间才有观战的状态
     *
     * @param data
     */
    gameStatusView(data) {
        let gameStatus = that.getGameStatus(data.status);
        if (gameStatus) {
            if (gameStatus.game_status == 2) {
                //准备中
                return (<View
                    style={{position: 'absolute', bottom: 5, right: 5, borderRadius: 5, backgroundColor: '#ff9319'}}>
                    <Text
                        style={{
                            fontSize: 8,
                            color: Color.colorWhite,
                            marginLeft: 5,
                            marginRight: 5,
                            alignSelf: 'center'
                        }}>{Language().game_prepare}</Text>
                </View>);
            } else if (gameStatus.game_status == 3) {
                let gameStatusText = Language().game_start;
                let position = 0;
                if (gameStatus.position) {
                    position = gameStatus.position;
                }
                if (position > 11) {
                    gameStatusText = Language().game_watching;
                }
                //游戏中或是准备中
                return (
                    <View style={{
                        position: 'absolute',
                        bottom: 5,
                        right: 5,
                        borderRadius: 5,
                        backgroundColor: '#ff23c7'
                    }}>
                        <Text
                            style={{
                                fontSize: 8,
                                color: Color.colorWhite,
                                marginLeft: 5,
                                marginRight: 5,
                                alignSelf: 'center'
                            }}>{gameStatusText}</Text>
                    </View>);
            }
        }
        return null;
    }

    /**
     * 好友在线footer
     */
    renderFooter() {
        let friendCount = friendStatusList.length;
        let defaultWidth = ScreenWidth / 2 + 30;
        if (friendCount == 2) {
            defaultWidth = ScreenWidth / 4 + 30;
        }
        return (friendCount > 0 && friendCount < 3) ? (<View style={{justifyContent: 'center', width: defaultWidth}}>
            <Image style={{width: 75, height: 80, alignSelf: 'center', resizeMode: 'contain',}}
                   source={ImageManager.defaultFriendStatus}/>
            {/*<Text*/}
            {/*style={{padding:5,fontSize:12,color:'#DB6BFA',alignSelf:'center'}}>{Language().add_game_friend}</Text>*/}
        </View>) : null;
    }

    /**
     *
     * @param data
     * 游戏房间如果是游戏中，可观战
     */
    actionFriendView(data) {
        let gameStatus = that.getGameStatus(data.status);
        if (gameStatus) {
            if (gameStatus.game_status == 2) {
                //在房间中并且是准备状态
                that.enterFriendRoom(gameStatus.room_id);
            } else if (gameStatus.game_status == 3) {
                that.enterFriendRoom(gameStatus.room_id);
            } else {
                //进入好友详情
                that.actionContact(data);
            }
        } else {
            //进入好友详情
            that.actionContact(data);
        }
    }

    /**
     * 查看好友详情
     * @param item
     */
    actionContact(item) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                if (item && item.id) {
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                    return;
                }
                if (Platform.OS === 'ios') {
                    var main = NativeModules.MainViewController;
                    let sex = '2';
                    if (item.sex == 1) {
                        sex = '1';
                    }
                    let image = item.image;
                    if (image == null) {
                        image = '';
                    }
                    main.didSelectUser(item.id, 'detail', sex, image, item.name, '');
                } else {
                    NativeModules.NativeJSModule.rnStartPersonalInfoActivtiy(PersonalInfoActivity, item.id, this.props.token, false);
                }
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     *
     * @param roomId
     */
    enterFriendRoom(roomId) {
        //网络检测
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                try {
                    this.onShowProgressDialog();
                    this.state.roomId = roomId;
                    let url = apiDefines.SEARCH_ROOM_INFO + this.state.roomId + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id;
                    if (this.props.hostLocation) {
                        url = url + apiDefines.PARAMETER_LOCATION + this.props.hostLocation
                    }
                    Util.get(url, (code, message, data) => {
                        this.onHideProgressDialog();
                        if (code == 1000) {
                            if (!this.checkCanEnterGame(data.level)) {
                                return;
                            }
                            this.state.game_type = data.level;
                            this.state.roomId = data.room_id;
                            if (data.password_needed) {
                                //弹框直接进入输入密码
                                this.setState({
                                    dialogIndex: 2,
                                    dialogConfirm: Language().enter,
                                    dialogCancel: Language().cancel,
                                    hideCancel: false,
                                    hideConfirm: false,
                                    password: '',
                                    needPassword: data.password_needed,
                                    textRoomID: roomId
                                });
                                this.onOpenModal();
                            } else {
                                //直接进入
                                this.enterRoom(this.state.roomId);
                            }

                        }
                        else if (code == 1004) {
                            //token 失效
                            this.onShowLoginDialog();
                        } else {
                            Toast.show(message);
                        }
                    }, (failed) => {
                        this.onHideProgressDialog();
                        Toast.show(Language().not_network)
                    });
                } catch (e) {
                    console.log('do search room error:' + e);
                }

            } else {
                //无网络提示
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 获取用户状态信息
     * @param status
     * @returns {{}}
     */
    getGameStatus(status) {
        let gameStatus = {};
        let arrayStatus = new Array();
        arrayStatus = status.split(':');
        for (let i = 0; i < arrayStatus.length; i++) {
            if (i == 0) {
                gameStatus.game_status = parseInt(arrayStatus[i]);
            } else if (i == 1) {
                gameStatus.updateTime = arrayStatus[i];
            } else if (i == 2) {
                gameStatus.room_id = arrayStatus[i];
            } else if (i == 3) {
                gameStatus.type = arrayStatus[i];
            } else if (i == 5) {
                gameStatus.position = parseInt(arrayStatus[i]);
            }
        }
        return gameStatus;
    }

    /**
     * 格式化状态信息
     * @param status
     * @returns {string|string}
     */
    formatGameType(status) {
        let gameType = Language().on_line;
        let gameStatus = that.getGameStatus(status);
        switch (gameStatus.type) {
            case GAME_TYPE_SIMPLE_6:
                gameType = Language().game_simple_6;
                break;
            case GAME_TYPE_SIMPLE_9:
                gameType = Language().game_simple_9;
                break;
            case GAME_TYPE_SIMPLE_10:
                gameType = Language().game_simple_10;
                break;
            case GAME_TYPE_NORMAL:
                gameType = Language().normal_cupid;
                break;
            case GAME_TYPE_NORMAL_GUARD:
                gameType = Language().normal_guard;
                break;
            case GAME_TYPE_HIGH_KING:
                gameType = Language().high_king;
                break;
            case GAME_TYPE_PRE_SIMPLE:
                gameType = Language().novice_model;
                break;
            case GAME_TYPE_PRE_SIMPLE_NEW:
                gameType = Language().hunts_model_6;
                break;
            case GAME_TYPE_AUDIO:
                gameType = Language().voice_model;
                break;
        }
        return gameType;
    }

    /**
     * 在线好友
     * @returns {XML}
     */
    onLineFriendView() {
        return (<View>
            <View flexDirection='row' style={{height: 30, marginTop: 10}}>
                <Text
                    style={{
                        fontSize: 16,
                        alignSelf: 'center',
                        color: '#333333',
                        marginLeft: 10,
                        flex: 1
                    }}>{Language().friend_on_line}</Text>
                {friendStatusList.length == 0 ? null : (
                    <TouchableOpacity onPress={this.fetchMoreFriendStatus.bind(this)}
                                      activeOpacity={0.8}
                                      style={{backgroundColor: Color.transparent, justifyContent: 'center', margin: 2}}>
                        <View flexDirection='row' style={{justifyContent: 'center', marginRight: 10}}>
                            <Text
                                style={{
                                    fontSize: 14,
                                    color: '#fb5c88',
                                    alignSelf: 'center',
                                    alignSelf: 'center'
                                }}>{Language().refresh_more}</Text>
                            <LoadImageView source={ImageManager.ic_refresh_more}
                                           style={{
                                               width: 16,
                                               height: 16,
                                               alignSelf: 'center',
                                               marginLeft: 5,
                                               resizeMode: 'contain'
                                           }}/>
                        </View>
                    </TouchableOpacity>
                )}
            </View>
            {this.friendStatusView()}
        </View>);
    }

    /**
     * 游戏帮助管理器
     */
    gameHelpContentManager() {
        switch (this.state.gameHelpType) {
            case 'simple':
                return (<GameSimpleModel/>);
            case 'standard':
                return (<GameStAdModel/>);
            case 'dictionary':
                //专业术语
                return (<GameProfessionalLlg/>);
            case 'role':
                //角色
                return (<GameRole/>);
            case 'rule':
                //游戏规则
                return (<GameRule/>);
            default:
                return this.gameHelpMenu();
        }
    }

    /**
     *游戏帮助首页
     */
    gameHelpMenu = () => {
        return (<View style={{flex: 1, justifyContent: 'center', marginTop: 10, marginBottom: 10}}>
            <View style={{alignSelf: 'center', justifyContent: 'center', flex: 1}}>
                <TouchableOpacity activeOpacity={0.6} onPress={() => {
                    this.openStrategyWeb()
                }}
                                  style={{
                                      justifyContent: 'center',
                                      marginTop: 0,
                                      width: ScreenWidth / 3 * 2,
                                      alignSelf: 'center',
                                      flex: 1,
                                      backgroundColor: '#6711B7',
                                      borderRadius: 3,
                                  }}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={videoHelp}/>
                        <Text
                            style={{
                                flex: 2,
                                alignSelf: 'center',
                                color: '#e1fbfd',
                                fontSize: 17,
                                textAlign: 'right',
                                marginRight: 20
                            }}>{Language().game_video}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6}
                                  onPress={() => {
                                      this.selectGameModelHelp(Language().game_rule, 'rule')
                                  }}
                                  style={{
                                      justifyContent: 'center',
                                      marginTop: 10,
                                      width: ScreenWidth / 3 * 2,
                                      alignSelf: 'center',
                                      flex: 1,
                                      backgroundColor: '#6711B7',
                                      borderRadius: 3
                                  }}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={gameRule}/>
                        <Text
                            style={{
                                flex: 2,
                                alignSelf: 'center',
                                color: '#e1fbfd',
                                fontSize: 17,
                                textAlign: 'right',
                                marginRight: 20
                            }}>{Language().game_rule}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6}
                                  onPress={() => {
                                      this.selectGameModelHelp(Language().role_detail, 'role')
                                  }}
                                  style={{
                                      justifyContent: 'center',
                                      marginTop: 10,
                                      width: ScreenWidth / 3 * 2,
                                      alignSelf: 'center',
                                      flex: 1,
                                      backgroundColor: '#6711B7',
                                      borderRadius: 3
                                  }}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={roleIntroduction}/>
                        <Text
                            style={{
                                flex: 2,
                                alignSelf: 'center',
                                color: '#e1fbfd',
                                fontSize: 17,
                                textAlign: 'right',
                                marginRight: 20
                            }}>{Language().role_detail}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6}
                                  onPress={() => {
                                      this.selectGameModelHelp(Language().sim_nov_model, 'simple')
                                  }}
                                  style={{
                                      justifyContent: 'center',
                                      marginTop: 10,
                                      width: ScreenWidth / 3 * 2,
                                      alignSelf: 'center',
                                      flex: 1,
                                      backgroundColor: '#6711B7',
                                      borderRadius: 3
                                  }}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={simpleNovModel}/>
                        <Text
                            style={{
                                flex: 2,
                                alignSelf: 'center',
                                color: '#e1fbfd',
                                fontSize: 17,
                                textAlign: 'right',
                                marginRight: 20
                            }}>{Language().sim_nov_detail}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6}
                                  onPress={() => {
                                      this.selectGameModelHelp(Language().st_ad_model, 'standard')
                                  }}
                                  style={{
                                      justifyContent: 'center',
                                      marginTop: 10,
                                      width: ScreenWidth / 3 * 2,
                                      alignSelf: 'center',
                                      flex: 1,
                                      backgroundColor: '#6711B7',
                                      borderRadius: 3
                                  }}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={stAdModel}/>
                        <Text
                            style={{
                                flex: 2,
                                alignSelf: 'center',
                                color: '#e1fbfd',
                                fontSize: 17,
                                textAlign: 'right',
                                marginRight: 20
                            }}>{Language().st_ad_detail}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6}
                                  onPress={() => {
                                      this.selectGameModelHelp(Language().professional_Lg, 'dictionary')
                                  }}
                                  style={{
                                      justifyContent: 'center',
                                      marginTop: 10,
                                      width: ScreenWidth / 3 * 2,
                                      alignSelf: 'center',
                                      flex: 1,
                                      backgroundColor: '#6711B7',
                                      borderRadius: 3
                                  }}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={professionalLg}/>
                        <Text
                            style={{
                                flex: 2,
                                alignSelf: 'center',
                                color: '#e1fbfd',
                                fontSize: 17,
                                textAlign: 'right',
                                marginRight: 20
                            }}>{Language().professional_Lg}</Text>
                    </View>
                </TouchableOpacity>
            </View>

        </View>);
    }
    /**
     * 帮助
     */
    onOpenGameHelp = () => {
        this.setState({
            gameHelpDialogShow: true,
            gameHelpMenu: true,
            gameHelpTitle: Language().game_help,
            gameHelpType: ''
        });
    }
    /**
     *
     * @param helpTitle
     * @param gameType
     */
    selectGameModelHelp = (helpTitle, gameType) => {
        this.setState({
            gameHelpDialogShow: false,
            gameHelpTitle: helpTitle,
            gameHelpMenu: false,
            gameHelpType: gameType
        });
        this.setState({gameHelpDialogShow: true,});
    }
    /**
     *
     */
    closeGameHelpDialog = () => {
        if (this.state.gameHelpMenu) {
            this.setState({
                gameHelpDialogShow: false,
                gameHelpTitle: '',
                gameHelpMenu: true,
                gameHelpType: ''
            });
        } else {
            this.onOpenGameHelp();
        }

    }
    /**
     *
     */
    openStrategyWeb = () => {
        this.closeGameHelpDialog();
        try {
            this.props.openPage(SCENES.SCENE_WEBVIEW, 'global');
        } catch (e) {
            console.log(e)
        }

    }

    /**
     * 游戏模式管理
     * @private
     */
    _gameModelManager() {
        switch (this.state.activityGameMode) {
            case GAME_MODEL_SIMPLE:
                return this._simpleModelGroupView();
            case GAME_MODEL_STANDARD:
            case GAME_MODEL_ADVANCED:
                return this._standardModelGroupView();
            case GAME_MODEL_NOVICE:
                return this._noviceModelGroupView();
            default:
                return this._simpleModelGroupView();
        }
    }

    /**
     * 简单模式组合
     */
    _simpleModelGroupView() {
        let needShowSimple6 = this.checkNeedShowGame(GAME_TYPE_SIMPLE_6);
        let needShowSimple9 = this.checkNeedShowGame(GAME_TYPE_SIMPLE_9);
        let needShowSimple10 = this.checkNeedShowGame(GAME_TYPE_SIMPLE_10);
        let simple9Source = InternationSourceManager().simple9Source;
        let simple10Source = InternationSourceManager().simple10Source;
        return (!needShowSimple6 && !needShowSimple9 && !needShowSimple10) ? null : (<View>
            <View style={{height: 50}}></View>
            {
                needShowSimple6 ? <TouchableOpacity onPress={() => {
                    this.doEnterGameModel(GAME_TYPE_SIMPLE_6)
                }}
                                                    activeOpacity={0.8}
                                                    style={{
                                                        flex: 1,
                                                        backgroundColor: Color.transparent,
                                                        marginRight: 8,
                                                        marginLeft: 8,
                                                        marginTop: 5
                                                    }}>
                    <CachedImage source={InternationSourceManager().simple6Source}
                                 style={styles.model_image_style}/>
                </TouchableOpacity> : null
            }
            {
                needShowSimple9 ? <TouchableOpacity onPress={() => {
                    this.doEnterGameModel(GAME_TYPE_SIMPLE_9)
                }}
                                                    activeOpacity={0.8}
                                                    style={{
                                                        flex: 1,
                                                        backgroundColor: Color.transparent,
                                                        marginRight: 8,
                                                        marginLeft: 8,
                                                        marginTop: 5
                                                    }}>
                    <CachedImage source={simple9Source}
                                 style={styles.model_image_style}/>
                </TouchableOpacity> : null
            }
            {
                needShowSimple10 ? <TouchableOpacity onPress={() => {
                    this.doEnterGameModel(GAME_TYPE_SIMPLE_10)
                }}
                                                     activeOpacity={0.8}
                                                     style={{
                                                         flex: 1,
                                                         backgroundColor: Color.transparent,
                                                         marginRight: 8,
                                                         marginLeft: 8,
                                                         marginTop: 5
                                                     }}>
                    <CachedImage source={simple10Source}
                                 style={styles.model_image_style}/>
                </TouchableOpacity> : null
            }

        </View>);

    }

    /**
     * 标准模式组
     */
    _standardModelGroupView() {
        let needShowNormalGuard = this.checkNeedShowGame(GAME_TYPE_NORMAL_GUARD);
        let needShowNormal = this.checkNeedShowGame(GAME_TYPE_NORMAL);
        let needShowHighKing = this.checkNeedShowGame(GAME_TYPE_HIGH_KING);
        let highKingSource = InternationSourceManager().highKingSource;
        return (!needShowNormalGuard && !needShowNormal && !needShowHighKing) ? null : (<View>
            {(!needShowNormalGuard && !needShowNormal) ? null : <View style={{height: 50, justifyContent: 'flex-end'}}>
                <Text
                    style={{
                        color: Color.colorWhite,
                        fontSize: 20,
                        fontWeight: 'bold',
                        alignSelf: 'center'
                    }}>{Language().game_model_standard}</Text>
            </View>

            }

            {
                needShowNormalGuard ?
                    <TouchableOpacity onPress={() => {
                        this.doEnterGameModel(GAME_TYPE_NORMAL_GUARD)
                    }}
                                      activeOpacity={0.8}
                                      style={{
                                          flex: 1,
                                          backgroundColor: Color.transparent,
                                          marginRight: 8,
                                          marginLeft: 8,
                                          marginTop: 0
                                      }}>
                        <CachedImage source={InternationSourceManager().normalGuardSource}
                                     style={styles.model_image_style}/>
                    </TouchableOpacity> : null
            }

            {
                needShowNormal ? <TouchableOpacity onPress={() => {
                    this.doEnterGameModel(GAME_TYPE_NORMAL)
                }}
                                                   activeOpacity={0.8}
                                                   style={{
                                                       flex: 1,
                                                       backgroundColor: Color.transparent,
                                                       marginRight: 8,
                                                       marginLeft: 8,
                                                       marginTop: 5
                                                   }}>
                    <CachedImage source={InternationSourceManager().normalCupidSource}
                                 style={styles.model_image_style}/>
                </TouchableOpacity> : null
            }
            {needShowHighKing ?
                <View>
                    <View style={{height: 40, justifyContent: 'flex-end'}}>
                        <Text
                            style={{
                                color: Color.colorWhite,
                                fontSize: 20,
                                fontWeight: 'bold',
                                alignSelf: 'center'
                            }}>{Language().game_model_advanced}</Text>

                    </View>
                    <TouchableOpacity onPress={() => {
                        this.doEnterGameModel(GAME_TYPE_HIGH_KING)
                    }}
                                      activeOpacity={0.8}
                                      style={{
                                          flex: 1,
                                          backgroundColor: Color.transparent,
                                          marginRight: 8,
                                          marginLeft: 8,
                                          marginTop: 5
                                      }}>
                        <CachedImage source={highKingSource}
                                     style={styles.model_image_style}/>
                    </TouchableOpacity>
                </View> : null
            }

        </View>);
    }

    /**
     * 新手模式组
     */
    _noviceModelGroupView() {
        let needShowPreSimpleNew = this.checkNeedShowGame(GAME_TYPE_PRE_SIMPLE_NEW);
        let needShowPreSimple = this.checkNeedShowGame(GAME_TYPE_PRE_SIMPLE);
        return (!needShowPreSimpleNew && !needShowPreSimple) ? null : (<View>
            <View style={{height: 50}}></View>
            {needShowPreSimpleNew ? <TouchableOpacity onPress={() => {
                this.doEnterGameModel(GAME_TYPE_PRE_SIMPLE_NEW)
            }}
                                                      activeOpacity={0.8}
                                                      style={{
                                                          flex: 1,
                                                          backgroundColor: Color.transparent,
                                                          marginRight: 8,
                                                          marginLeft: 8,
                                                          marginTop: 5
                                                      }}>
                <CachedImage
                    source={InternationSourceManager().preSimpleNewSource}
                    style={styles.model_image_style}/>
            </TouchableOpacity> : null}
            {needShowPreSimple ?
                <TouchableOpacity onPress={() => {
                    this.doEnterGameModel(GAME_TYPE_PRE_SIMPLE)
                }}
                                  activeOpacity={0.8}
                                  style={{
                                      flex: 1,
                                      backgroundColor: Color.transparent,
                                      marginRight: 8,
                                      marginLeft: 8,
                                      marginTop: 5
                                  }}>
                    <CachedImage source={InternationSourceManager().preSimpleSource}
                                 style={styles.model_image_style}/>
                </TouchableOpacity> : null
            }

        </View>);
    }

    /**
     *
     * @returns {*}
     * @private
     */
    _getActivityGameModel() {
        switch (this.state.activityGameMode) {
            case GAME_MODEL_SIMPLE:
                return Language().game_model_simple;
            case GAME_MODEL_STANDARD:
            case GAME_MODEL_ADVANCED:
                return Language().game_model_advanced_more;
            case GAME_MODEL_NOVICE:
                return Language().game_model_novice;
            default:
                return Language().game_model_simple;
        }
    }

    /**
     *
     * @private
     */
    _getActivityGameModelBg() {
        let gameModelSource = {
            modelSource: ImageManager.bgGameModelNew,
            modelTextColor: '#ffff72'
        }
        switch (this.state.activityGameMode) {
            case GAME_MODEL_SIMPLE:
                gameModelSource.modelSource = ImageManager.bgGameModelSp;
                gameModelSource.modelTextColor = '#b2ffff';
                break;
            case GAME_MODEL_STANDARD:
            case GAME_MODEL_ADVANCED:
                gameModelSource.modelSource = ImageManager.bgGameModelAd;
                gameModelSource.modelTextColor = '#ffb0ca';
                break;
            case GAME_MODEL_NOVICE:
                gameModelSource.modelSource = ImageManager.bgGameModelNew;
                gameModelSource.modelTextColor = '#ffff72';
                break;
        }
        return gameModelSource;
    }

    /**
     * 统一游戏入口
     * @param gameType
     */
    doEnterGameModel(gameType) {
        // if (gameType == GAME_TYPE_SIMPLE_10 || gameType == GAME_TYPE_SIMPLE_9) {
        //     if (this.getUserLevel() > 7) {
        //         this.state.game_type = gameType;
        //         this.state.roomId = '';
        //         this.state.password = '';
        //         this.onShowProgressDialog();
        //         this.enterRoom(this.state.roomId);
        //     }
        // } else {
        //     this.state.game_type = gameType;
        //     this.state.roomId = '';
        //     this.state.password = '';
        //     this.onShowProgressDialog();
        //     this.enterRoom(this.state.roomId);
        // }
        if (!this.checkCanEnterGame(gameType)) {
            return;
        }
        this.state.game_type = gameType;
        this.state.roomId = '';
        this.state.password = '';
        this.onShowProgressDialog();
        this.enterRoom(this.state.roomId);
    }

    /**
     * 展示对应的游戏模式
     * @param gameMode
     */
    showGameModelManager(gameMode) {
        this.setState({
            showGameModelManager: true,
            activityGameMode: gameMode
        });
    }

    /**
     * 关闭游戏模式
     */
    closeGameModelManager() {
        this.setState({
            showGameModelManager: false,
            activityGameMode: GAME_MODEL_SIMPLE,
        });
    }

    /**
     * 进入标准模式
     */
    enterStandardModel = () => {
        //TODO 添加等级
        let needShowNormalGuard = this.checkNeedShowGame(GAME_TYPE_NORMAL_GUARD);
        let needShowNormal = this.checkNeedShowGame(GAME_TYPE_NORMAL);
        let needShowHighKing = this.checkNeedShowGame(GAME_TYPE_HIGH_KING);
        if (needShowNormalGuard ^ needShowNormal ? needShowHighKing : needShowNormalGuard) {
            this.showGameModelManager(GAME_MODEL_STANDARD);
        } else if (needShowNormalGuard) {
            this.doEnterGameModel(GAME_TYPE_NORMAL_GUARD);
        } else if (needShowNormal) {
            this.doEnterGameModel(GAME_TYPE_NORMAL);
        } else if (needShowHighKing) {
            this.doEnterGameModel(GAME_TYPE_HIGH_KING);

        }
        // if (this.props.userInfo != null && this.props.userInfo.game != null && this.props.userInfo.game.level != null) {
        //     if (this.props.userInfo.game.level > 11) {
        //         this.showGameModelManager(GAME_MODEL_STANDARD);
        //     } else {
        //         this.toastStandardModel('LV.12' + Language().open);
        //     }
        // } else {
        //     this.toastStandardModel('LV.12' + Language().open);
        // }
        // //统计
        // rnRoNativeUtils.onStatistics(Constant.STANDARD_MODEL);
    }
    /**
     * 新手专区
     */
    enterNoviceModel = () => {
        //TODO 添加等级
        let needShowPreSimpleNew = this.checkNeedShowGame(GAME_TYPE_PRE_SIMPLE_NEW);
        let needShowPreSimple = this.checkNeedShowGame(GAME_TYPE_PRE_SIMPLE);
        if (needShowPreSimpleNew && needShowPreSimple) {
            this.showGameModelManager(GAME_MODEL_NOVICE);
        } else if (needShowPreSimpleNew) {
            this.doEnterGameModel(GAME_TYPE_PRE_SIMPLE_NEW)
        } else if (needShowPreSimple) {
            this.doEnterGameModel(GAME_TYPE_PRE_SIMPLE)
        }
        // if (this.props.userInfo) {
        //     if (this.getUserLevel() > 15) {
        //         if (this.checkIsRole()) {
        //             this.showGameModelManager(GAME_MODEL_NOVICE);
        //         } else {
        //             Toast.show(Language().prompt_primary_model);
        //         }
        //     } else {
        //         this.showGameModelManager(GAME_MODEL_NOVICE);
        //     }
        // } else {
        //     Toast.show(netWorkTool.NOT_NETWORK);
        // }
    }
    /**
     * 进入简单模式
     */
    enterSimpleModel = () => {
        //TODO 添加等级
        let needShowSimple6 = this.checkNeedShowGame(GAME_TYPE_SIMPLE_6);
        let needShowSimple9 = this.checkNeedShowGame(GAME_TYPE_SIMPLE_9);
        let needShowSimple10 = this.checkNeedShowGame(GAME_TYPE_SIMPLE_10);
        if (needShowSimple10 ^ needShowSimple9 ? needShowSimple6:needShowSimple10) {
            this.showGameModelManager(GAME_MODEL_SIMPLE);
        } else if (needShowSimple6) {
            this.doEnterGameModel(GAME_TYPE_SIMPLE_6);
        } else if (needShowSimple10) {
            this.doEnterGameModel(GAME_TYPE_SIMPLE_10);
        } else if (needShowSimple9) {
            this.doEnterGameModel(GAME_TYPE_SIMPLE_9);
        }
        // if (this.props.userInfo != null && this.props.userInfo.game != null && this.props.userInfo.game.level != null) {
        //     if (this.props.userInfo.game.level > 2) {
        //         this.showGameModelManager(GAME_MODEL_SIMPLE);
        //     } else {
        //         this.toastStandardModel('LV.3' + Language().open);
        //     }
        // } else {
        //     this.toastStandardModel('LV.3' + Language().open);
        // }
    }

    /**
     *
     * @param message
     */
    toastStandardModel(message) {
        Toast.show(message);
    }

    /**
     * 点击按钮
     * @param type
     */
    onClick = (type) => {
        switch (type) {
            case MiniGameConstant.GAME_HOME_SEARCH:
                this.onSearchRoom();
                break;
            case MiniGameConstant.GAME_HOME_CREATE:
                this.onCreateRoom();
                break;
            case MiniGameConstant.GAME_HOME_HELP:
                this.onOpenGameHelp();
                break;
            case MiniGameConstant.WEREWOLF_HOME_OPEN_NOVICE:
                this.enterNoviceModel();
                break;
            case MiniGameConstant.WEREWOLF_HOME_OPEN_SIMPLE:
                this.enterSimpleModel();
                break;
            case MiniGameConstant.WEREWOLF_HOME_OPEN_ADVANCED:
                this.enterStandardModel();
                break;
        }

    }
    /**
     *离开
     */
    finishPage = () => {
        if (!this.state.isFinishing) {
            this.props.finishPage('global');
            this.state.isFinishing = true;
        }
        this.finishTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }
}

const mapDispatchToProps = dispatch => ({
    openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
    updateRoomId: (roomId, password) => dispatch((updateRoomId(roomId, password))),
    finishPage: (key) => dispatch(popRoute(key)),
    removeChatData: () => dispatch(removeChat()),
    updateUserInfo: (data) => dispatch((userInfo(data))),
    updateTourist: (isTourist) => dispatch((updateTourist(isTourist))),
    clearMessageList: () => dispatch(clearMessageList()),
    clearFriendList: () => dispatch(clearFriendList()),
    removeFriendStatus: () => dispatch(removeFriendStatus()),
    getMoreFriendStatus: () => dispatch(getMoreFriendStatus()),
    getUserInfo: (id) => dispatch(getUserInfo(id)),
    updateNoticeVer: (noticeVar) => dispatch(updateNoticeVer(noticeVar)),
    taskInfo: () => dispatch(taskInfo()),
    updateGiftManifest: (gifts) => dispatch(updateGiftManifest(gifts)),
    updateConfig: () => dispatch(checkOpenRegister()),
});
const mapStateToProps = state => ({
    code: state.userInfoReducer.code,
    message: state.userInfoReducer.message,
    userInfo: state.userInfoReducer.data,
    token: state.userInfoReducer.token,
    newRoomId: state.userInfoReducer.roomId,
    newPassword: state.userInfoReducer.password,
    navigation: state.cardNavigation,
    isTourist: state.userInfoReducer.isTourist,
    enterNoviceCount: state.configReducer.enter_novice_count,
    noticeVer: state.configReducer.newNoticeVer,
    hostLocation: state.configReducer.location,
    friendStatusDataList: state.friendStatusDataReducer.data,
    taskInfoData: state.taskInfoReducer.data,
    configData: state.checkRegisterReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps)(MiniGameHome);
