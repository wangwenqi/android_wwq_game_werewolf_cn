/**
 * Created by wangxu on 2017/3/1.
 */
'use strict';
import {StyleSheet, Dimensions} from 'react-native';
const ScreenWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
    cardView: {
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 4,
        flex: 1
    },
    dialogContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
    },
    cardItem: {alignSelf: 'center', flex: 1},
    dialogCard: {
        margin: 30,
        borderRadius: 3,
        padding: 5
    },
    dialogTitle: {fontSize: 16, fontWeight: 'bold', textAlign: 'center'},
    buttonImage: {resizeMode: 'contain', flex: 1, alignSelf: 'center'},
    rightArrows: {width: 15, height: 15, resizeMode: 'contain'},
    dialogHelpBt: {width: 40, height: 40, resizeMode: 'contain'},
    dialogHelpTitleContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
    },
    HelpCardItem: {
        backgroundColor: 'rgba(0, 0, 0, 0)'
    },
    thumbnail: {
        width: 70, height: 80
    },
    headThumbnail: {
        width: 60,
        height: 60,
        borderRadius: 30,
        alignSelf: 'center',
    },
    homeCurrency: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        resizeMode: 'contain',
        alignSelf: 'center',
        flex: 1,
        height: (ScreenWidth * 0.45 - 9) / 2 * 0.296,
        width: (ScreenWidth * 0.45 - 9) / 2, top: 0
    },
    homeAddCurrency: {
        width: 8,
        height: 8,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginRight: 3.5,
        marginTop: 0.5
    },
    homeBox: {
        width: 40,
        height: 50,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    dropDown: {
        width: ScreenWidth - 125,
        height: 35,
        borderWidth: 0,
        backgroundColor: 'rgba(0, 0, 0, 0)',
        justifyContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        right:0
    },
    dropDownView: {
        width: 28,
        height: 35,
        borderWidth: 0,
        borderBottomRightRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: '#f29b76',
        justifyContent: 'center',
        alignSelf: 'flex-end',
    },
    dropdownDropdown: {
        borderRadius: 5,
        width: ScreenWidth - 125,
        borderWidth: 1,
        borderColor: '#f29b76',
    },
    dropdownRow: {
        flexDirection: 'row',
        height: 35,
        alignItems: 'center',
        justifyContent: 'center'
    },
    dropdownRowText: {
        fontSize: 12,
        color: '#1c1c25',
        textAlign: 'center',
        alignSelf: 'center'
    },
    actionRoomIcon: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    actionButton: {
        backgroundColor: '#e9e9e9', justifyContent: 'center', height: 40,margin:10,borderRadius:8,flex:1
    },
    loading: {
        position: 'absolute',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        height: 70,
        width: 110,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    announcementHeader: {
        position: 'absolute',
        width: 90,
        height: 110,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    announcementClose: {
        position: 'absolute',
        width: 30,
        height: 30,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    webViewError: {
        alignSelf: 'center',
        resizeMode: 'contain',
        alignItems: 'center'
    },
    webErrorBt: {
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0)',
        alignSelf: 'center',
    },
    image: {
        position: 'absolute',
        left: 0,
        top: 0,
        bottom: 0,
        right: 0,
        width: null,
        height: null,
        resizeMode: 'stretch',
        backgroundColor: 'rgba(0, 0, 0, 0)',
    }, set_game_rule_view: {
        flexDirection: 'row',
        borderColor: '#f29b76',
        borderRadius: 5,
        borderWidth: 1,
        marginRight: 35,
        marginLeft: 35,
        marginTop: 5,
        marginBottom: 5,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    }, game_arrow: {
        width: 12,
        height: 12,
        resizeMode: 'contain',
        alignSelf: 'center',
    }, gameHelpMenu: {
        resizeMode: 'contain',
        margin: 5,
        marginLeft: 30,
        flex: 1
    }, listViewStyle: {
        flexDirection: 'row',
    }, model_image_style: {
        flex: 1,
        resizeMode: 'contain',
        width: (ScreenWidth - 60),
        height: (ScreenWidth - 60) * 0.37,
        alignSelf: 'center'
    }, game_auxiliary_view: {justifyContent: 'center', alignSelf: 'center'},
    game_auxiliary_image: {
        resizeMode: 'contain',
        width: (ScreenWidth - 5) / 5,
        height: (ScreenWidth - 5) / 5,
        marginRight: 0,
        alignSelf: 'center'
    },
    game_view_style: {justifyContent: 'center', margin: 2.5},
    game_home_view_style: {justifyContent: 'center', margin: 8, marginBottom: 0, alignSelf: 'center'},
    backStyle: {
        width: 15,
        height: 18,
        alignSelf: 'center',
        resizeMode: 'contain',
    }
});
module.exports = styles;