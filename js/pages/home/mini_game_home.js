/**
 *  Created by wangxu on 20/02/2017.
 * 应用的首页，进行提供游戏入口，以及房间的搜素，以及其他功能的展示
 */
import React, {Component} from 'react';
import {
    Container,
    Button,
    Left,
    Thumbnail,
    Right,
    Body,
    ListItem,
    Item,
    Input,
    Card,
    CardItem,
    Icon,
    Spinner
} from 'native-base';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {
    Image,
    ProgressViewIOS,
    NativeModules,
    AsyncStorage,
    Dimensions,
    View,
    Platform,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Keyboard,
    Modal, DeviceEventEmitter, NativeEventEmitter, InteractionManager, StyleSheet, Switch, WebView, ListView, Text
} from 'react-native';
//##自定义工具
import * as SCENES from '../../../support/actions/scene';
import Util from '../../../support/common/utils';
import * as apiDefines from '../../../support/common/gameApiDefines';
import * as MiniGameConstant from './constant';
import Toast from '../../../support/common/Toast';
import netWorkTool from '../../../support/common/netWorkTool';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import {clearFriendList} from '../../../support/actions/friendActions';
import {clearMessageList} from '../../../support/actions/messageActions';
import Dialog, {Alert, Confirm, Prompt,} from '../../../support/common/dialog';
import {removeChat} from '../../../support/actions/chatActions';
import {userInfo, updateTourist, updateRoomId, getUserInfo} from '../../../support/actions/userInfoActions';
import CachedImage from '../../../support/common/CachedImage';
import LoadImageView from '../../../support/common/LoadImageView';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import * as Constant from '../../../support/common/constant';
import Loading from '../../../support/common/Loading';
import {getMoreFriendStatus, removeFriendStatus} from '../../../support/actions/friendStatusAction';
import {updateNoticeVer} from '../../../support/actions/configActions';
import {taskInfo} from '../../../support/actions/taskInfoActions';
import {updateGiftManifest} from '../../../support/actions/giftManifestActions';
import {checkOpenRegister} from '../../../support/actions/checkRegisterActions';
import styles from "./style";
import Language from '../../../resources/language';
import InternationSourceManager from '../../../resources/internationSourceManager';//国际化资源管理
import ImageManager from '../../../resources/imageManager';
import Color from '../../../resources/themColor';

const {
    pushRoute,
    popRoute
} = actions;
const bgPopular = require('../../../resources/imgs/icon_popular.png');
const {RNMessageSender} = NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
//
const refreshMorePadding = (Platform.OS === 'ios') ? 4 : 0;
const numberPaddingLeft = (Platform.OS === 'ios') ? 25 : 23;
//游戏类型
const GAME_TYPE_SIMPLE = 'simple';
const GAME_TYPE_SIMPLE_6 = 'simple_6';//简单6人局
const GAME_TYPE_SIMPLE_9 = 'simple_9';//简单9人局
const GAME_TYPE_SIMPLE_10 = 'simple_10';//简单10人局
const GAME_TYPE_NORMAL = 'normal';//标准12人丘比特
const GAME_TYPE_NORMAL_GUARD = 'normal_guard';//标准12人守卫（新)
const GAME_TYPE_HIGH_KING = 'high_king';//12高级房间
const GAME_TYPE_PRE_SIMPLE = 'pre_simple';
const GAME_TYPE_PRE_SIMPLE_NEW = 'pre_simple_new';//6人新手房（亮身份）
const GAME_TYPE_AUDIO = 'audio';//语音房间
//
//游戏模式
const GAME_MODEL_SIMPLE = 'simple';
const GAME_MODEL_STANDARD = 'standard';
const GAME_MODEL_ADVANCED = 'advanced';
const GAME_MODEL_NOVICE = 'novice';
const PersonalInfoActivity = 'com.game_werewolf.activity.PersonalInfoActivity';
let progressDialog;
let needLoginDialog;
let webView;
let image = '';
let that;
let friendStatusList = new Array();
const roleType = {
    manager: 1 << 0,
    teacher: 1 << 1
}

class MiniGameHome extends Component {
    static propTypes = {
        navigator: React.PropTypes.shape({}),
        openPage: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        configData: React.PropTypes.object,
        friendStatusDataList: React.PropTypes.array,
        code: React.PropTypes.number,
        message: React.PropTypes.string,
        token: React.PropTypes.string,
        newRoomId: React.PropTypes.string,
        newPassword: React.PropTypes.string,
        updateRoomId: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        removeChatData: React.PropTypes.func,
        updateUserInfo: React.PropTypes.func,
        updateTourist: React.PropTypes.func,
        isTourist: React.PropTypes.bool,
        clearMessageList: React.PropTypes.func,
        clearFriendList: React.PropTypes.func,
        removeFriendStatus: React.PropTypes.func,
        getMoreFriendStatus: React.PropTypes.func,
        getUserInfo: React.PropTypes.func,
        updateNoticeVer: React.PropTypes.func,
        taskInfo: React.PropTypes.func,
        enterNoviceCount: React.PropTypes.number,
        hostLocation: React.PropTypes.string,
        taskInfoData: React.PropTypes.object,
        noticeVer: React.PropTypes.number,
        updateGiftManifest: React.PropTypes.func,
        updateConfig: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        that = this;
        if (this.props.friendStatusDataList) {
            friendStatusList = this.props.friendStatusDataList;
        }
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        //2.设置返回数据
        this.state = {
            dataSource: ds.cloneWithRows(friendStatusList),
            dialogShow: false,
            dialogIndex: 0,
            dialogTitle: 'title',
            dialogConfirm: 'confirm',
            dialogCancel: 'cancel',
            hideConfirm: false,
            hideCancel: false,
            roomId: '',
            textRoomID: '',
            password: '',
            switchIsOn: false,
            needPassword: false,
            gameHelpType: '',
            gameHelpTitle: '',
            gameHelpDialogShow: false,
            gameHelpMenu: true,
            simple_model_radio: true,
            standard_model_radio: false,
            game_type: GAME_TYPE_SIMPLE_6,
            room_id_focus: false,
            updateGame: true,
            touristCanEnterOtherRoom: false,
            room_type: '',
            showModelProgress: false,
            showAnnouncementModel: false,
            showGameModelManager: false,
            activityGameMode: GAME_MODEL_SIMPLE,//当前选中的模式
            game_limit_level: 0,
            game_limit_level_name: Language().no_limit,
            select_game_play: Language().game_simple_6,
            search_dialog_activity: 0,//0房间，1好友，2家族
        };
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    componentDidUpdate() {
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
        this.onLeaveGameListener.remove();
        this.checkTokenTimer && clearTimeout(this.checkTokenTimer);
        this.leaveGameTimer && clearTimeout(this.leaveGameTimer);
        this.taskInfoTimer && clearTimeout(this.taskInfoTimer);
    }

    componentDidMount() {
        //页面渲染完毕后定义加载view
        progressDialog = this.refs.progressDialog;
        needLoginDialog = this.refs.login;
        webView = this.refs.webView;
        this.checkTokenTimer = setTimeout(() => {
            this.checkToken();
        }, 500);
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        try {
            if (Platform.OS === 'android') {
                this.onLeaveGameListener = DeviceEventEmitter.addListener('EVENT_USER_LEAVE', this.onLeaveGameRoom.bind(this));
            } else {
                this.onLeaveGameListener = rNMessageSenderEmitter.addListener('EVENT_USER_LEAVE', this.onLeaveGameRoom.bind(this));
            }
        } catch (e) {

        }
        if (!this.props.isTourist) {
            //已完成任务
            this.taskInfoTimer = setTimeout(() => {
                this.props.taskInfo();
            }, 1500);
            //非游客同步礼物数据
            if (this.props.userInfo) {
                let gifts = this.props.userInfo.gift;
                if (gifts && gifts.length > 0) {
                    this.props.updateGiftManifest(gifts);
                }
            }
        }
    }

    render() {
        if (this.props.friendStatusDataList) {
            friendStatusList = this.props.friendStatusDataList;
        } else {
            friendStatusList = new Array();
        }
        return (
            <Container>
                <ScrollView style={{backgroundColor: Color.colorWhite}} showsVerticalScrollIndicator={false}>
                    {/*用户信息*/}
                    {this.userInfoView()}
                    {/*魔幻变变变*/}
                    {this.gameModelViewItem(MiniGameConstant.ENTER_CHANGE_FACE, ImageManager.changeFaceBg, Language().changeFaceTitle, Language().changeFaceMessage)}
                    {/*其他游戏辅助模块*/}
                    {this.gameAuxiliaryView()}
                    {/*小游戏模块*/}
                    {this.miniGameView()}
                    {/*终极狼人杀入口*/}
                    {this.gameModelViewItem(MiniGameConstant.ENTER_WEREWOLF, ImageManager.ic_game_werewolf_enter_bg, Language().game_werewolf, Language().game_werewolf_content)}
                    {/*谁是卧底*/}
                    {this.gameModelViewItem(MiniGameConstant.ENTER_UNDERCOVER, ImageManager.ic_undercover_bg, Language().voice_type_undercover, Language().undercover_content)}
                    {/*好友在线*/}
                    {this.onLineFriendView()}
                    {/*faceback*/}
                    {this.faceBackView()}
                </ScrollView>
                {/*创建房间以及搜索房间的弹框*/}
                <Modal visible={this.state.dialogShow} transparent={true} animationType='fade'
                       onRequestClose={() => {
                           console.log('close')
                       }}>
                    <Container flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                               style={[styles.dialogContainer]}>

                        <ScrollView alwaysBounceVertical={false}>
                            <View
                                style={{
                                    borderRadius: 10,
                                    width: ScreenWidth - 50,
                                    maxHeight: ScreenHeight - 50,
                                    alignSelf: 'center',
                                    backgroundColor: Color.colorWhite
                                }}>
                                {this.dialogContentManager()}
                                <View flexDirection='row' style={{
                                    justifyContent: 'center',
                                    width: ScreenWidth - 100,
                                    alignSelf: 'center',
                                    marginBottom: 10
                                }}>
                                    <TouchableOpacity activeOpacity={0.9}
                                                      onPress={this.onCloseModal.bind(this)}
                                                      style={styles.actionButton}>
                                        <Text
                                            style={{
                                                alignSelf: 'center',
                                                color: '#818080'
                                            }}>{this.state.dialogCancel}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={0.8} onPress={this.onConfirmModal.bind(this)}
                                                      style={[styles.actionButton, {backgroundColor: '#ffdb42'}]}>
                                        <Text
                                            style={{
                                                alignSelf: 'center',
                                                color: '#1c1c25'
                                            }}>{this.state.dialogConfirm}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.showModelProgress ? (
                                <View style={[styles.loading, {left: ScreenWidth / 2 - 55, top: 45}]}>
                                    <Spinner size='small' style={{height: 35}}/>
                                    <Text
                                        style={{
                                            marginTop: 5,
                                            fontSize: 14,
                                            color: Color.colorWhite
                                        }}>{Language().request_loading}</Text>
                                </View>) : null}
                        </ScrollView>
                    </Container>
                </Modal>
                {/*公告*/}
                <Modal visible={this.state.showAnnouncementModel} transparent={true} animationType='fade'
                       onRequestClose={() => {
                           console.log('close')
                       }}>
                    <Container flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                               style={styles.dialogContainer}>
                        <View
                            style={{
                                borderColor: Color.dialog_announcement_color,
                                borderRadius: 5,
                                borderWidth: 2,
                                width: ScreenWidth - 60,
                                height: ScreenHeight / 3 * 2 - 20,
                                alignSelf: 'center',
                                backgroundColor: Color.dialog_announcement_content_color,
                                left: 30,
                                position: 'absolute',
                                top: (ScreenHeight - (ScreenHeight / 3 * 2 - 20)) / 2,
                                overflow: 'hidden',
                                padding: 2
                            }}>
                            <WebView
                                style={{
                                    flex: 1,
                                    borderRadius: 5,
                                    backgroundColor: Color.dialog_announcement_content_color
                                }}
                                ref="webView"
                                source={{uri: apiDefines.NOTICE_URL + this.props.token}}
                                javaScriptEnabled={true}
                                domStorageEnabled={true}
                                decelerationRate="normal"
                                startInLoadingState={true}
                                onLoad={this.loadSuccessNotice.bind(this)}
                                renderError={this._webViewError.bind(this)}/>
                        </View>
                        <View
                            style={{
                                position: 'absolute',
                                alignSelf: 'center',
                                left: ScreenWidth / 2 - 45,
                                top: (ScreenHeight - (ScreenHeight / 3 * 2 - 20)) / 2 - 75
                            }}>
                            <Image source={ImageManager.iconAnnouncementHeader} style={styles.announcementHeader}/>
                            <Text
                                style={{
                                    top: 60,
                                    left: 26,
                                    color: '#7F4D11',
                                    fontSize: 18,
                                    fontWeight: 'bold'
                                }}>{Language().notice}</Text>
                        </View>
                        <View
                            style={{
                                position: 'absolute',
                                alignSelf: 'center',
                                left: ScreenWidth / 2 - 15,
                                top: (ScreenHeight / 3 * 2 + 100)
                            }}>
                            <Image source={ImageManager.iconAnnouncementClose} style={styles.announcementClose}/>

                            <TouchableOpacity activeOpacity={0.8} onPress={this.closeAnnouncementModel.bind(this)}
                                              style={{width: 30, height: 30, backgroundColor: Color.transparent}}>
                            </TouchableOpacity>
                        </View>
                    </Container>
                </Modal>
                <Loading ref="progressDialog" loadingTitle={Language().request_loading}/>
                {Dialog.inject()}
                <Alert ref='login' title={Language().title_login} message={Language().need_login_message}
                       messageStyle={{alignSelf: 'center', fontSize: 14, marginBottom: 10}}
                       titleStyle={{alignSelf: 'center', fontSize: 17, padding: 5}}
                       contentStyle={{
                           paddingLeft: 1,
                           paddingBottom: 3,
                           paddingRight: 1,
                           width: ScreenWidth / 3 * 2 + 30
                       }}
                       buttonBarStyle={{justifyContent: 'center'}}
                       buttonStyle={{
                           textAlign: 'center',
                           fontSize: 16,
                           padding: 10,
                           marginLeft: 0,
                           marginRight: 0,
                           alignSelf: 'center',
                           justifyContent: 'center',
                           flex: 1,
                           color: Color.system_dialog_bt_text_color
                       }}
                       posText={Language().confirm}
                       rowSeparator={true}
                       onPosClick={this.login.bind(this)}> </Alert>
                <Confirm ref='touristModal' title={Language().title_prompt_tourist_login} posText={Language().go_login}
                         messageStyle={{alignSelf: 'center', fontSize: 14, marginBottom: 10}}
                         titleStyle={{alignSelf: 'center', fontSize: 17, padding: 5}}
                         contentStyle={{
                             paddingLeft: 1,
                             paddingBottom: 3,
                             paddingRight: 1,
                             width: ScreenWidth / 3 * 2 + 30
                         }}
                         buttonBarStyle={{justifyContent: 'center'}}
                         buttonStyle={{
                             textAlign: 'center',
                             fontSize: 16,
                             padding: 10,
                             marginLeft: 0,
                             marginRight: 0,
                             alignSelf: 'center',
                             justifyContent: 'center',
                             flex: 1,
                             color: Color.system_dialog_bt_text_color
                         }}
                         negText={Language().cancel}
                         enumeSeparator={true}
                         rowSeparator={true}
                         onNegClick={this.dismissTouristDialog.bind(this)} onPosClick={this.toLogin.bind(this)}>
                </Confirm>
            </Container>
        );
    }

    /**
     *
     */
    userInfoView = () => {
        let userInfo = this.props.userInfo;
        let gold = 0;
        let dim = 0;
        let popular = 0;
        let name = '';
        let image = ''
        if (userInfo != null) {
            if (userInfo.name) {
                name = userInfo.name;
                if (name.toString().length > 10) {
                    name = name.toString().slice(0, 10) + '..';
                }
            }
            if (userInfo.image && userInfo.image != '') {
                image = userInfo.image;
            }
            if (userInfo.money) {
                if (userInfo.money.gold > 0) {
                    gold = userInfo.money.gold;
                    if (gold.toString().length > 6) {
                        gold = gold.toString().slice(0, 6) + '+';
                    }
                }
                if (userInfo.money.dim > 0) {
                    dim = userInfo.money.dim;
                    if (dim.toString().length > 6) {
                        dim = dim.toString().slice(0, 6) + '+';
                    }
                }
            }
            if (userInfo.popular) {
                popular = userInfo.popular;
            }
        }
        return (<View flexDirection='row'
                      style={[styles.game_home_view_style, {marginBottom: 4, paddingTop: 15, paddingBottom: 15}]}>
            <View style={{flex: 1, justifyContent: 'center'}}>
                <View flexDirection='row' style={{marginLeft: 10}}>
                    <TouchableOpacity activeOpacity={0.8}
                                      style={{justifyContent: 'center'}}
                                      onPress={this.onOpenUserInfo.bind(this)}>
                        <View
                            style={{
                                justifyContent: 'center',
                                width: 71,
                                height: 71,
                                alignSelf: 'center',
                                borderRadius: 36,
                                backgroundColor: '#dddddd'
                            }}>
                            <LoadImageView style={{width: 70, height: 70, alignSelf: 'center', borderRadius: 35}}
                                           defaultSource={ImageManager.defaultUser}
                                           source={(image) ? {uri: image} : ImageManager.defaultUser}/>
                        </View>
                    </TouchableOpacity>
                    <View style={{alignSelf: 'center', marginLeft: 15}}>
                        <Text numberOfLines={1}
                              style={{color: '#333333', fontSize: 17, fontWeight: '500'}}>{name}</Text>
                        {this.popularityView(popular)}
                        {Platform.OS === 'ios' ? this.myWealthView(gold, dim) : null}
                    </View>
                </View>
            </View>
            <TouchableOpacity activeOpacity={0.8}
                              style={{justifyContent: 'center'}}
                              onPress={() => {
                                  this.onOpenBox()
                              }}>
                <LoadImageView
                    style={{
                        width: (ScreenWidth - 50) / 4,
                        height: (ScreenWidth - 50) / 4 * 0.617,
                        alignSelf: 'center',
                        resizeMode: 'contain'
                    }}
                    source={ImageManager.ic_mini_home_box}/>
                {this.taskInfoView()}
            </TouchableOpacity>

        </View>);
    }

    /**
     * 人气值view
     * @param popular
     * @returns {*}
     */
    popularityView = (popular) => {
        return popular ? (<View style={{flexDirection: 'row', marginTop: 5}}>
            <Image style={{width: 15, height: 15, alignSelf: 'center', resizeMode: 'contain',}} source={bgPopular}/>
            <Text
                style={{color: '#ef8468', fontSize: 14, fontWeight: '400', padding: 0}}>{Language().popular}</Text>
            <Text
                style={{color: '#ef8468', fontSize: 14, fontWeight: '400', padding: 0, marginLeft: 5}}>{popular}</Text>
        </View>) : null;
    }

    /**
     *
     * @param type
     * @param image
     * @param title
     * @param message
     */
    gameModelViewItem = (type, image, title, message) => {
        return (<View style={[styles.game_home_view_style]}>
                <TouchableOpacity
                    style={{backgroundColor: Color.transparent, justifyContent: 'center'}}
                    onPress={() => this.onClick(type)}
                    activeOpacity={0.8}>
                    <LoadImageView source={image}
                                   style={{
                                       resizeMode: 'contain',
                                       width: ScreenWidth - 20,
                                       height: (ScreenWidth - 20) * 0.26
                                   }}/>
                    <View
                        style={{
                            justifyContent: 'center',
                            position: 'absolute',
                            right: 15,
                            top: (ScreenWidth - 20) * 0.26 * 0.153
                        }}>
                        <Text
                            style={{
                                fontWeight: 'bold',
                                fontSize: 20,
                                color: Color.colorWhite,
                                textAlign: 'right'
                            }}>{title}</Text>
                        <Text
                            style={{
                                fontWeight: '400',
                                fontSize: 14,
                                color: Color.colorWhite,
                                marginTop: 3,
                                textAlign: 'right'
                            }}>{message}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );

    }
    /**
     *
     * @param type
     * @param image
     * @param message
     * @param left
     * @param backgroundColor1
     * @param backgroundColor2
     * @returns {XML}
     */
    gameAuxiliaryViewItem = (type, image, message, left, backgroundColor1, backgroundColor2) => {
        let vaule = Platform.OS === 'ios' ? 4 : 3;
        let ViewWidth = Platform.OS === 'ios' ? (ScreenWidth - 35) : (ScreenWidth - 30);
        return (<View
                style={{
                    width: ViewWidth / vaule,
                    height: ViewWidth * 0.784 / 4,
                    marginLeft: left,
                    borderRadius: 5,
                    backgroundColor: backgroundColor2
                }}>
                <View
                    style={{
                        width: ViewWidth / vaule,
                        height: ViewWidth * 0.784 / 4 - 5,
                        borderRadius: 5,
                        backgroundColor: backgroundColor1
                    }}>
                    <TouchableOpacity
                        style={{backgroundColor: Color.transparent, justifyContent: 'center', flex: 1}}
                        onPress={() => this.onClick(type)}
                        activeOpacity={0.8}>
                        <View
                            style={{justifyContent: 'center', alignSelf: 'center', margin: 10}}>
                            <CachedImage source={image}
                                         style={{
                                             resizeMode: 'contain',
                                             width: ViewWidth / 4 / 3,
                                             height: ViewWidth / 4 / 3
                                         }}/>
                            <Text
                                style={{
                                    fontWeight: '400',
                                    fontSize: 16,
                                    color: Color.colorWhite,
                                    marginTop: 0
                                }}>{message}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );

    }
    /**
     * @param type
     * @param image
     * @param message
     * @returns {XML}
     */
    miniGameModelViewItem = (type, bgImage, image, message, left) => {
        return (<TouchableOpacity
                style={{backgroundColor: Color.transparent, justifyContent: 'center', marginLeft: left}}
                onPress={() => this.onClick(type)}
                activeOpacity={0.8}>
                <LoadImageView source={bgImage}
                               style={{
                                   resizeMode: 'contain',
                                   width: (ScreenWidth - 25) / 2,
                                   height: (ScreenWidth - 25) / 2 * 0.413
                               }}/>
                <View flexDirection='row'
                      style={{justifyContent: 'center', position: 'absolute', top: 0, bottom: 0, left: 20}}>
                    <LoadImageView
                        style={{
                            resizeMode: 'contain',
                            width: (ScreenWidth - 25) / 2 * 0.413 * 0.54,
                            height: (ScreenWidth - 25) / 2 * 0.413 * 0.54,
                            alignSelf: 'center'
                        }}
                        source={image}/>
                    <Text
                        style={{
                            fontWeight: 'bold',
                            fontSize: 18,
                            color: Color.colorWhite,
                            alignSelf: 'center',
                            marginLeft: 8
                        }}>{message}</Text>
                </View>
            </TouchableOpacity>
        );

    }

    /**
     *小游戏
     */
    miniGameView = () => {
        let configData = this.props.configData;
        let needShowMiniGame = false;
        if (configData && configData.cnw_upgrade) {
            needShowMiniGame = true;
        }

        return needShowMiniGame ? (
            <View style={[styles.game_home_view_style, {marginBottom: 4}]}>
                <View flexDirection='row'>
                    {this.miniGameModelViewItem(MiniGameConstant.GAME_ANIMAL_FIGHT, ImageManager.ic_mini_game_pink, ImageManager.ic_game_animal_fight, Language().animal_checker, 0)}
                    {this.miniGameModelViewItem(MiniGameConstant.GAME_LINK_FIVE, ImageManager.ic_mini_game_yellow, ImageManager.ic_game_link_five, Language().ic_gobang, 5)}
                </View>
                <View flexDirection='row' style={{marginTop: 5}}>
                    {this.miniGameModelViewItem(MiniGameConstant.GAME_CATCH_THIEF, ImageManager.ic_mini_game_green, ImageManager.ic_game_catch_thief, Language().game_catch_thief, 0)}
                    {this.miniGameModelViewItem(MiniGameConstant.ENTER_MORE_MINI_GAME, ImageManager.ic_mini_game_blue, ImageManager.ic_more_mini_game, Language().icon_more_mini_game, 5)}
                </View>
            </View>
        ) : null;
    }

    /**
     *开启小游戏
     */
    openMoreMiniGame = () => {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().openMiniGame_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                //如果flag判断不是游客，但是确实用的游客的ID或是token，直接提示信息失效去登录
                if (this._checkTourist()) {
                    this.onShowLoginDialog();
                } else {
                    try {
                        this.props.openPage(SCENES.SCENE_MINI_GAME_MANAGER, 'global')
                    } catch (e) {
                    }
                }
            }
        });
    }
    /**
     * 游戏辅助模块view
     * @param imageTW
     */
    gameAuxiliaryView = () => {
        return (
            <View flexDirection='row' style={[styles.game_home_view_style, {marginBottom: 4}]}>
                {this.gameAuxiliaryViewItem(MiniGameConstant.GAME_HOME_SEARCH, ImageManager.ic_mini_home_search, Language().search, 0, '#DA9197', '#DB7179')}
                {Platform.OS === 'ios' ? this.gameAuxiliaryViewItem(MiniGameConstant.GAME_HOME_OPEN_STORE, ImageManager.ic_mini_home_store, Language().store, 5, '#DFA481', '#CE8051') : null}
                {this.gameAuxiliaryViewItem(MiniGameConstant.GAME_HOME_OPEN_GROUP, ImageManager.ic_mini_home_group, Language().family, 5, '#80BBDC', '#348AB9')}
                <View>
                    {this.gameAuxiliaryViewItem(MiniGameConstant.GAME_HOME_OPEN_NOTIFICATION, ImageManager.ic_mini_home_notification, Language().notice, 5, '#FAA494', '#D37D6C')}
                    {this.announcementNewView()}
                </View>
            </View>
        );
    }

    /**
     * 个人财富view
     */
    myWealthView = (gold, dim) => {
        return (<View flexDirection='row' style={{marginTop: 5}}>
            <TouchableOpacity
                onPress={() => {
                    this.onOpenBox()
                }}
                style={{backgroundColor: Color.transparent, justifyContent: 'center'}}
                activeOpacity={0.8}>
                <CachedImage source={ImageManager.goldIcon} style={styles.homeCurrency}/>
                <View flexDirection='row'
                      style={{
                          backgroundColor: Color.transparent,
                          justifyContent: 'center',
                          alignSelf: 'center',
                          width: (ScreenWidth * 0.45 - 10) / 2,
                          height: (ScreenWidth * 0.45 - 10) / 2 * 0.296,
                      }}>
                    <Text numberOfLines={1}
                          style={{
                              fontSize: 9,
                              paddingLeft: numberPaddingLeft,
                              alignSelf: 'center',
                              flex: 1,
                              color: '#700018',
                              backgroundColor: Color.transparent
                          }}>{gold}</Text>
                    <CachedImage source={ImageManager.addGold} style={styles.homeAddCurrency}/>
                </View>

            </TouchableOpacity>
            <TouchableOpacity
                onPress={this.openStoreRecharge.bind(this)}
                style={{backgroundColor: Color.transparent, justifyContent: 'center', marginLeft: 5}}
                activeOpacity={0.8}>
                <CachedImage source={ImageManager.jewelIcon} style={styles.homeCurrency}/>

                <View flexDirection='row'
                      style={{
                          backgroundColor: Color.transparent,
                          justifyContent: 'center',
                          alignSelf: 'center',
                          width: (ScreenWidth * 0.45 - 10) / 2,
                          height: (ScreenWidth * 0.45 - 10) / 2 * 0.296,
                          marginTop: 0.5
                      }}>
                    <Text numberOfLines={1}
                          style={{
                              fontSize: 9,
                              paddingLeft: numberPaddingLeft,
                              alignSelf: 'center',
                              flex: 1,
                              color: '#283ebb',
                              backgroundColor: Color.transparent
                          }}>{dim}</Text>
                    <CachedImage source={ImageManager.addJewel} style={[styles.homeAddCurrency]}/>
                </View>
            </TouchableOpacity>
        </View>);
    }

    /**
     * 检测是否显示支付
     * @returns {boolean}
     */
    checkShowPay() {
        if (Platform.OS === 'ios') {
            if (this.props.isTourist) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    _onReloadView() {
        if (webView) {
            webView.reload();
        }
    }

    /**
     *
     * @returns {XML}
     * @private
     */
    _webViewError() {
        return (
            <View style={{flex: 1, backgroundColor: Color.dialog_announcement_content_color, justifyContent: 'center'}}>
                <TouchableOpacity style={styles.webErrorBt} onPress={this._onReloadView.bind(this)}>
                    <Image style={[styles.webViewError, {width: ScreenWidth / 3, height: ScreenWidth / 3 + 20}]}
                           source={ImageManager.bgWebViewError}/>
                </TouchableOpacity>
            </View>);
    }

    /**
     * 公告
     */
    openAnnouncement = () => {
        this.showAnnouncementModel();
    }

    /**
     * 排行榜
     */
    openLeaderboard = () => {
        rnRoNativeUtils.openLeaderboard();
    }

    /**
     * 监测是否shi
     * @returns {boolean}
     */
    checkIsNovice() {
        if (this.props.userInfo && this.props.userInfo.game) {
            let game = this.props.userInfo.game;
            if (game.level <= 15) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    login() {
        this.clearData();
        this.onHideLoginDialog();

    }

    /**
     *
     */
    dismissTouristDialog() {
        if (this.refs.touristModal) {
            this.refs.touristModal.hide();
        }
        if (this.state.touristCanEnterOtherRoom) {
            //进入游戏
            this.onShowProgressDialog();
            this.enterRoom(this.state.roomId);
            //统计
            rnRoNativeUtils.onStatistics(Constant.SIMPLE_MODE);
        }
    }

    /**
     *
     */
    toLogin() {
        this.clearData();
        if (this.refs.touristModal) {
            this.refs.touristModal.hide();
        }
    }

    /**
     *  清理数据缓存
     */
    clearData() {
        const routes = this.props.navigation.routes;
        if (routes.length > 1) {
            for (var i = 0; i < routes.length; i++) {
                this.props.finishPage('global');
            }
        }
        if (this.props.userInfo != null && this.props.userInfo.id != null) {
            let userId = this.props.userInfo.id;
            rnRoNativeUtils.closeLeanCloud(userId);
        }
        AsyncStorageTool.removeUserInfo();
        try {
            this.props.openPage(SCENES.SCENE_SPLASH, 'global');
        } catch (e) {
            console.log(e)
        }
        AsyncStorageTool.removeSignTourist();
        this.props.removeChatData();
        this.props.updateUserInfo(null);
        this.props.updateTourist(false);
        this.props.clearFriendList();
        this.props.removeFriendStatus();
        this.props.clearMessageList();
    }

    /**
     * 以及反馈
     * @private
     */
    _onFeedback() {
        rnRoNativeUtils.onFeedback();
    }

    onShowLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.show();
        }
    }

    onHideLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.hide();
        }
    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     * 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    checkToken() {
        if (this.props.code == 1004) {
            //token 失效
            try {
                this.onShowLoginDialog();
            } catch (error) {
                console.log('token error show:' + error);
            }
        }
    }

    /**
     * 商城
     */
    onOpenStore() {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().openStore_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                //如果flag判断不是游客，但是确实用的游客的ID或是token，直接提示信息失效去登录
                if (this._checkTourist()) {
                    this.onShowLoginDialog();
                } else {
                    if (Platform.OS === 'ios') {
                        rnRoNativeUtils.onOpenStore();
                    } else {
                        Toast.show(Language().temporarily_not_opened);
                    }

                }
            }
        });
    }

    /**
     * 充值
     */
    openStoreRecharge = () => {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().openStore_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                //如果flag判断不是游客，但是确实用的游客的ID或是token，直接提示信息失效去登录
                if (this._checkTourist()) {
                    this.onShowLoginDialog();
                } else {
                    if (Platform.OS === 'ios') {
                        rnRoNativeUtils.openStoreRecharge();
                    } else {
                        Toast.show(Language().temporarily_not_opened);
                    }
                }
            }
        });
    }

    /**
     * 游戏离开房间通知更新
     */
    onLeaveGameRoom() {
        try {
            this.leaveGameTimer = setTimeout(() => {
                this.setState({
                    updateGame: true
                });
            }, 1500);
        } catch (e) {

        }
    }

    _keyboardDidShow() {

    }

    _keyboardDidHide() {
    }

    handleMethod(isConnected) {
        console.log('test', (isConnected ? 'online' : 'offline'));
    }

    onOpenUserInfo() {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().content_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                this.props.openPage(SCENES.SCENE_CONTACT_DETAIL, 'global');
            }
        });

    }

    onSearchRoom = () => {
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().search_prompt_tourist_login});
            }
            this.state.touristCanEnterOtherRoom = false;
        } else {
            this.setState({
                dialogIndex: 0,
                search_dialog_activity: 0,
                dialogTitle: Language().search_room,
                dialogConfirm: Language().enter,
                dialogCancel: Language().cancel,
                hideCancel: false,
                hideConfirm: false,
                password: ''
            });
            this.onOpenModal();
            //统计
            rnRoNativeUtils.onStatistics(Constant.SEARCH_ROOM);
        }
    }

    /**
     * 搜索房间的具体操作
     */
    doSearchRoom() {
        //网络检测
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                try {
                    if (this.state.textRoomID == '') {
                        Toast.show(Language().room_number_empty);
                    } else {
                        this.setState({
                            showModelProgress: true
                        });
                        let data = {
                            room_id: this.state.textRoomID
                        }
                        this.state.roomId = this.state.textRoomID;
                        let url = apiDefines.SEARCH_ROOM_INFO + this.state.roomId + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id;
                        if (this.props.hostLocation) {
                            url = url + apiDefines.PARAMETER_LOCATION + this.props.hostLocation
                        }
                        Util.get(url, (code, message, data) => {
                            this.setState({
                                showModelProgress: false
                            });
                            if (code == 1000) {
                                if (data.level == GAME_TYPE_PRE_SIMPLE || data.level == GAME_TYPE_PRE_SIMPLE_NEW) {
                                    //15級別以上不能進入新手房間
                                    if (this.getUserLevel() > 15 && !this.checkIsRole()) {
                                        Toast.show(Language().prompt_novice_model);
                                        return;
                                    }
                                }
                                this.state.game_type = data.level;
                                this.state.roomId = data.room_id;
                                if (data.password_needed) {
                                    this.setState({
                                        needPassword: data.password_needed
                                    });
                                } else {
                                    //直接进入
                                    this.onShowProgressDialog();
                                    this.enterRoom(this.state.roomId);
                                    this.onCloseModal();
                                }

                            }
                            else if (code == 1004) {
                                //token 失效
                                this.onShowLoginDialog();
                            } else {
                                Toast.show(message);
                            }
                        }, (failed) => {
                            this.onHideProgressDialog();
                            Toast.show(Language().not_network)
                        });
                    }
                } catch (e) {
                    console.log('do search room error:' + e);
                }

            } else {
                //无网络提示
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     *
     * @param text
     */
    onChangePassword(text) {
        if (/^[\d]+$/.test(text)) {
            this.setState({
                password: text
            });
        } else {
            if (/^[\d{1}]$/.test(this.state.password)) {
                this.setState({
                    password: ''
                });
            }
        }
    }

    changeRoomId(text) {
        let format = /^[0-9]*$/;
        if (format.test(text)) {
            this.setState({
                textRoomID: text
            });
        }
    }

    /**
     * 开启宝箱
     */
    onOpenBox = () => {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().openBox_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                //如果flag判断不是游客，但是确实用的游客的ID或是token，直接提示信息失效去登录
                if (this._checkTourist()) {
                    this.onShowLoginDialog();
                } else {
                    rnRoNativeUtils.openBox();
                }
            }
        });
    }

    /**
     * 战绩详情
     */
    openRecordDetails() {
        // 启动战绩
        // action "standings"
        // type "RN_Native"
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().record_detail_prompt_tourist_login});
            }
            this.state.touristCanEnterOtherRoom = false;
        } else {
            let NativeData = {
                action: Constant.STANDINGS,
                type: Constant.RN_NATIVE
            }
            rnRoNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
        }
    }

    /**
     * 启动家族
     */
    openGroup = () => {
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().group_prompt_tourist_login});
            }
            this.state.touristCanEnterOtherRoom = false;
        } else {
            let groupInfo = this.props.userInfo.group;
            if (groupInfo && groupInfo.group_id) {
                let nativeData = {
                    action: Constant.ACTION_SHOW_FAMILY_INFO,
                    params: {group_id: groupInfo.group_id},
                    type: Constant.RN_NATIVE
                }
                rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
            } else {
                try {
                    this.props.openPage(SCENES.SCENE_GROUP_LIST, 'global');
                } catch (e) {
                    console.log(e)
                }
            }
        }
    }

    /**
     * 判断是否是
     * @returns {boolean}
     * @private
     */
    _checkTourist() {
        if (this.props.userInfo) {
            let token = this.props.userInfo.token.access_token;
            let userId = this.props.userInfo.id;
            if (token == apiDefines.DEFAULT_ACCESS_TOKEN || userId == apiDefines.DEFAULT_USER_ID) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    onCloseModal() {
        this.setState({
            dialogShow: false,
            switchIsOn: false,
            password: '',
            roomId: '',
            simple_model_radio: true,
            standard_model_radio: false,
            game_type: GAME_TYPE_SIMPLE_6,
            needPassword: false,
            textRoomID: '',
            showModelProgress: false,
            activityGameMode: GAME_MODEL_SIMPLE,
            game_limit_level: 0,
            game_limit_level_name: Language().no_limit,
            select_game_play: Language().game_simple_6,
            dialogIndex: 0,
            search_dialog_activity: 0
        });

    }

    onSubmitEditing() {
        this.onConfirmModal();
    }

    onConfirmModal() {
        try {
            if (this.state.dialogIndex == 0 || this.state.dialogIndex == 2) {
                //搜索房间后进入
                if (this.state.search_dialog_activity == 0) {
                    if (this.state.needPassword) {
                        let password = this.state.password;
                        password = password.trim();
                        if (!password) {
                            Toast.show(Language().placeholder_password);
                        } else {
                            this.onShowProgressDialog();
                            this.enterRoom(this.state.roomId);
                            this.onCloseModal();
                        }
                    } else {
                        this.doSearchRoom();
                    }
                } else {
                    this.requestSearch();
                }
            }
        } catch (e) {
            console.log('onConfirmModal error:' + e);
        }
    }

    onOpenModal() {
        this.setState({dialogShow: true});
    }

    //进入房间
    enterRoom(roomId) {
        if (Platform.OS === 'ios') {
            //IOS 进入游戏房间
            this.onHideProgressDialog();
            var main = NativeModules.MainViewController;
            let userImage = this.props.userInfo.image;
            if (userImage == null) {
                userImage = '';
            }
            main.enter(this.state.password, this.state.game_type, roomId, this.props.userInfo.name, this.props.userInfo.id, userImage, this.props.token);
        } else {
            try {
                let experience = 0;
                let sex = 2;
                if (this.props.userInfo != null) {
                    if (this.props.userInfo.game != null && this.props.userInfo.game.experience != null) {
                        experience = this.props.userInfo.game.experience;
                    }
                    if (this.props.userInfo.sex == 1 || this.props.userInfo.sex == 2) {
                        sex = this.props.userInfo.sex;
                    }
                }
                NativeModules.NativeJSModule.rnStartGame(this.state.password, this.state.game_type, roomId, this.props.userInfo.name, this.props.userInfo.id, sex, this.props.userInfo.image, experience, this.props.token, successinfo => {
                    this.onHideProgressDialog();
                }, failed => {
                    this.onHideProgressDialog();
                    Toast.show(failed);
                });
            } catch (error) {
                this.onHideProgressDialog();
                Toast.show(Language().enter_room_error);
            }
        }
    }

    /**
     * 检查是否是管理员
     * @returns {boolean}
     */
    checkIsRole() {
        let type = 0;
        if (this.props.userInfo) {
            if (this.props.userInfo.role) {
                type = this.props.userInfo.role.type
            }
            if ((type & roleType.manager) != 0 || (type & roleType.teacher) != 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
     * 獲取用户等级
     * @returns {number}
     */
    getUserLevel() {
        let lever = 0;
        if (this.props.userInfo != null && this.props.userInfo.game != null && this.props.userInfo.game.level != null) {
            lever = this.props.userInfo.game.level;
        }
        return lever;
    }

    /**
     * 个性房间
     */
    enterPersonalityModel() {
        rnRoNativeUtils.openCallUp();
    }


    showAnnouncementModel = () => {
        this.setState({
            showAnnouncementModel: true
        });
    }

    closeAnnouncementModel() {
        this.setState({
            showAnnouncementModel: false
        });
    }

    /**
     * 搜素弹框view
     */
    searchDialogView() {
        let activity = this.state.search_dialog_activity;
        let placeholderText = Language().placeholder_room_number;
        if (activity == 1) {
            placeholderText = Language().placeholder_contact_number;
        } else if (activity == 2) {
            placeholderText = Language().placeholder_group_id;
        }
        let passwordContent = (
            <Input placeholder={Language().placeholder_room_password}
                   style={{
                       borderColor: '#f29b76',
                       borderWidth: StyleSheet.hairlineWidth,
                       borderRadius: 5,
                       height: 35,
                       width: ScreenWidth / 3 * 2 - 20,
                       alignSelf: 'center',
                       fontSize: 14,
                       paddingTop: 5,
                       paddingBottom: 5,
                       paddingLeft: 10,
                       paddingRight: 10,
                       marginTop: 10
                   }}
                   onChangeText={(text) => this.onChangePassword(text)}
                   placeholderTextColor='#cacaca'
                   autoFocus={true} maxLength={4}
                   keyboardType='numeric'
                   value={this.state.password}/>);
        return (<View>
            <View flexDirection='row' style={{justifyContent: 'center'}}>
                <TouchableOpacity activeOpacity={0.6} onPress={() => this.activitySearchIndex(0)}
                                  style={{
                                      backgroundColor: Color.transparent,
                                      justifyContent: 'center',
                                      flex: 1,
                                      height: 40
                                  }}>
                    <View flexDirection='row'
                          style={{
                              justifyContent: 'center',
                              padding: 5,
                              flex: 1,
                              backgroundColor: activity == 0 ? '#ffdb42' : Color.transparent,
                              borderTopLeftRadius: 10
                          }}>
                        <Image
                            source={activity == 0 ? ImageManager.searchRoomIconActivity : ImageManager.searchRoomIcon}
                            style={[styles.actionRoomIcon]}/>
                        <Text
                            style={{
                                color: activity == 0 ? '#ff4d5f' : '#ff8a96',
                                alignSelf: 'center',
                                fontSize: 17,
                                fontWeight: '500',
                                textAlign: 'center',
                                padding: 5
                            }}>{Language().recreation_text_room}</Text>
                    </View>
                </TouchableOpacity>
                <View style={{
                    width: StyleSheet.hairlineWidth,
                    backgroundColor: '#dbb57c',
                    alignSelf: 'center',
                    height: 32
                }}/>
                <TouchableOpacity activeOpacity={0.6} onPress={() => this.activitySearchIndex(1)}
                                  style={{
                                      backgroundColor: Color.transparent,
                                      justifyContent: 'center',
                                      flex: 1,
                                      height: 40
                                  }}>
                    <View flexDirection='row'
                          style={{
                              justifyContent: 'center',
                              padding: 5,
                              flex: 1,
                              backgroundColor: activity == 1 ? '#ffdb42' : Color.transparent,
                          }}>
                        <Image
                            source={activity == 1 ? ImageManager.searchUserIconActivity : ImageManager.searchUserIcon}
                            style={[styles.actionRoomIcon]}/>
                        <Text
                            style={{
                                color: activity == 1 ? '#4570ff' : '#8aa5ff',
                                alignSelf: 'center',
                                fontSize: 17,
                                fontWeight: '500',
                                textAlign: 'center',
                                padding: 5
                            }}>{Language().contact}</Text>
                    </View>
                </TouchableOpacity>
                <View style={{
                    width: StyleSheet.hairlineWidth,
                    backgroundColor: '#dbb57c',
                    alignSelf: 'center',
                    height: 32
                }}/>
                <TouchableOpacity activeOpacity={0.6} onPress={() => this.activitySearchIndex(2)}
                                  style={{
                                      backgroundColor: Color.transparent,
                                      justifyContent: 'center',
                                      flex: 1,
                                      height: 40
                                  }}>
                    <View flexDirection='row'
                          style={{
                              justifyContent: 'center',
                              padding: 5,
                              flex: 1,
                              backgroundColor: activity == 2 ? '#ffdb42' : Color.transparent,
                              borderTopRightRadius: 10
                          }}>
                        <Image
                            source={activity == 2 ? ImageManager.searchGroupIconActivity : ImageManager.searchGroupIcon}
                            style={[styles.actionRoomIcon]}/>
                        <Text
                            style={{
                                color: activity == 2 ? '#8543ff' : '#b38aff',
                                alignSelf: 'center',
                                fontSize: 17,
                                fontWeight: '500',
                                textAlign: 'center',
                                padding: 5
                            }}>{Language().family}</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View
                style={{
                    marginLeft: 3,
                    marginRight: 3,
                    backgroundColor: (Color.transparent),
                    paddingBottom: 20,
                    paddingTop: 30,
                    justifyContent: 'center'
                }}>
                <Input placeholder={placeholderText}
                       onChangeText={(text) => this.changeRoomId(text)}
                       placeholderTextColor='#cacaca'
                       style={{
                           borderColor: '#f29b76',
                           borderWidth: StyleSheet.hairlineWidth,
                           borderRadius: 5,
                           height: 35,
                           width: ScreenWidth / 3 * 2 - 20,
                           alignSelf: 'center',
                           fontSize: 14,
                           paddingTop: 5,
                           paddingBottom: 5,
                           paddingLeft: 10,
                           paddingRight: 10
                       }}
                       keyboardType='numeric'
                       value={this.state.textRoomID}/>
                {this.state.needPassword ? passwordContent : null}
            </View>
        </View>);
    }

    /**
     *
     * @param index
     */
    activitySearchIndex(index) {
        let dialogConfirmText = Language().confirm;
        if (index == 0) {
            dialogConfirmText = Language().enter;
        }
        that.setState({
            search_dialog_activity: index,
            dialogConfirm: dialogConfirmText
        });
    }

    /**
     * 在线好友进入游戏需要密码
     */
    needPasswordDialogView() {
        return (<View>
            <View flexDirection='row' style={{justifyContent: 'center', padding: 5}}>
                <Text
                    style={{
                        color: '#1c1c25',
                        alignSelf: 'center',
                        fontSize: 17,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        padding: 5
                    }}>{Language().room_number + this.state.textRoomID}</Text>
            </View>
            <View
                style={{
                    marginLeft: 3,
                    marginRight: 3,
                    backgroundColor: (Color.transparent),
                    paddingBottom: 10,
                    paddingTop: 5,
                    justifyContent: 'center'
                }}>
                <Input placeholder={Language().placeholder_room_password}
                       style={{
                           borderColor: '#f29b76',
                           borderWidth: StyleSheet.hairlineWidth,
                           borderRadius: 5,
                           height: 35,
                           width: ScreenWidth / 3 * 2 - 20,
                           alignSelf: 'center',
                           fontSize: 14,
                           paddingTop: 5,
                           paddingBottom: 5,
                           paddingLeft: 10,
                           paddingRight: 10,
                           marginTop: 10
                       }}
                       onChangeText={(text) => this.onChangePassword(text)}
                       placeholderTextColor='#cacaca'
                       autoFocus={true} maxLength={4}
                       keyboardType='numeric'
                       value={this.state.password}/>
            </View>
        </View>);
    }

    /**
     *搜索房间／好友在玩匹配进入弹框
     */
    dialogContentManager() {
        switch (this.state.dialogIndex) {
            case 0:
                return this.searchDialogView();
            case 2:
                return this.needPasswordDialogView();
        }
    }

    /**
     * 有领取的任务
     * @returns {XML}
     */
    taskInfoView = () => {
        if (this.props.taskInfoData) {
            if (this.props.taskInfoData.complete) {
                return (<View
                    style={{
                        backgroundColor: Color.badge_color,
                        width: 8,
                        height: 8,
                        borderRadius: 8,
                        alignSelf: 'center',
                        left: 10,
                        position: 'absolute',
                        top: 12
                    }}/>);
            }
        }
    }

    /*
     * 是否有新的公告
     */
    announcementNewView = () => {
        return this.checkNoticeVar() ? (<View
            style={{position: 'absolute', right: (ScreenWidth - 35) / 16 - 10, top: 5}}>
            <CachedImage source={ImageManager.icHomeNew}
                         style={{width: 22, height: 12, resizeMode: 'contain'}}/>
        </View>) : null;
    }

    /**
     * 公告咯load success
     */
    loadSuccessNotice() {
        try {
            if (this.checkNoticeVar()) {
                if (this.props.configData) {
                    if (this.props.configData.notice_ver) {
                        this.props.updateNoticeVer(this.props.configData.notice_ver)
                        let data = {
                            notice_ver: this.props.configData.notice_ver
                        }
                        AsyncStorageTool.saveNoticeData(JSON.stringify(data));
                    }
                }
            }
        } catch (e) {

        }
    }

    /**
     *
     * @returns {boolean}
     */
    checkNoticeVar = () => {
        try {
            let localNoticeVar = 0;
            if (this.props.noticeVer) {
                localNoticeVar = this.props.noticeVer;
            }
            if (this.props.configData) {
                if (this.props.configData.notice_ver) {
                    if (localNoticeVar != this.props.configData.notice_ver) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (err) {
            return false;
        }
    }

    /**
     * 搜索好友／家族
     */
    requestSearch() {
        this.setState({
            showModelProgress: true
        });
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                let activityIndex = this.state.search_dialog_activity;
                let api = apiDefines.GET_FRIEND_INFO_FROM_UID;
                if (activityIndex == 2) {
                    api = apiDefines.GROUP_INFO_FROM_ID;
                }
                let url = api + this.state.textRoomID;
                Util.get(url, (code, message, data) => {
                    this.setState({
                        showModelProgress: false
                    });
                    if (code == 1000) {
                        this.onCloseModal();
                        if (activityIndex == 2) {
                            let group_id = '';
                            if (data && data.group) {
                                group_id = data.group._id;
                            }
                            this.openFamilyDetail(group_id);
                        } else {
                            this.actionContact(data);
                        }

                    } else {
                        Toast.show(message);
                    }
                }, (failed) => {
                    this.setState({
                        showModelProgress: false
                    });
                    Toast.show(netWorkTool.NOT_NETWORK);
                });
            } else {
                this.setState({
                    showModelProgress: false
                });
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 进入家族详情
     * groupId
     */
    openFamilyDetail(groupId) {
        if (groupId) {
            let nativeData = {
                action: Constant.ACTION_SHOW_FAMILY_INFO,
                params: {group_id: groupId},
                type: Constant.RN_NATIVE
            }
            rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
        }
    }

    /**
     * 进入谁是卧底
     */
    enterUndercover = () => {
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().enter_undercover_prompt_tourist_login});
            }
        } else {
            this.onShowProgressDialog();
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    Util.get(apiDefines.GET_AUDIO_RANDOM_ROOM + Constant.VOICE_TYPE_UNDERCOVER, (code, message, data) => {
                        this.onHideProgressDialog();
                        if (code == 1000) {
                            if (data) {
                                let roomInfo = {
                                    roomId: data.room_id,
                                    roomPassword: ''
                                }
                                this.doEnterVoiceRoom(roomInfo);
                            }
                        } else {
                            Toast.show(message);
                        }
                    }, (error) => {
                        this.onHideProgressDialog();
                        Toast.show(Language().not_network);
                    });
                } else {
                    this.onHideProgressDialog();
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        }
    }

    /**
     * 执行进入房间的操作
     */
    doEnterVoiceRoom(roomData) {
        let NativeData = {
            action: Constant.ACTION_ENTER_AUDIO,
            params: roomData,
            options: null
        }
        rnRoNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }


    /**
     * 好友在玩view
     * @returns {XML}
     */
    friendStatusView() {
        return friendStatusList.length == 0 ? (
            <Text
                style={{
                    padding: 20,
                    fontSize: 14,
                    color: '#fb5c88',
                    alignSelf: 'center'
                }}>{Language().no_friend_online_message}</Text>) : (
            <View style={{flex: 1, paddingTop: 5, paddingBottom: 5}}>
                <View style={{marginLeft: 10, marginRight: 10}}>
                    <ListView
                        ref='friendStatusList'
                        dataSource={this.state.dataSource.cloneWithRows(friendStatusList)}
                        renderRow={this.friendStatusRenderRow}
                        renderFooter={this.renderFooter}
                        removeClippedSubviews={true}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={styles.listViewStyle}/>
                </View>
            </View>);
    }

    /**
     * 换一批
     */
    fetchMoreFriendStatus() {
        if (this.refs.friendStatusList) {
            this.refs.friendStatusList.scrollTo(0, 0);
        }
        this.props.getMoreFriendStatus();
    }

    /**
     * 好友在线信息item
     * @param data
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    friendStatusRenderRow(data, sectionId, rowId) {
        return (<View
            style={{margin: 3, justifyContent: 'center', width: ScreenWidth / 5}}>
            <TouchableOpacity onPress={() => {
                that.actionFriendView(data)
            }}
                              activeOpacity={0.8}
                              style={{backgroundColor: Color.transparent, justifyContent: 'center'}}>
                <View
                    style={{
                        justifyContent: 'center',
                        width: 71,
                        height: 71,
                        alignSelf: 'center',
                        borderRadius: 36,
                        backgroundColor: '#dddddd'
                    }}>
                    <LoadImageView style={{width: 70, height: 70, alignSelf: 'center', borderRadius: 35}}
                                   defaultSource={ImageManager.defaultUser}
                                   source={(data.image == '' || data.image == null) ? ImageManager.defaultUser : ({uri: data.image})}/>
                    {that.gameStatusView(data)}
                </View>
                <Text numberOfLines={1}
                      style={{
                          alignSelf: 'center',
                          fontSize: 12,
                          margin: 2,
                          marginTop: 8,
                          color: '#454545'
                      }}>{data.name}</Text>
                <View flexDirection='row'
                      style={{justifyContent: 'flex-end', width: 70, height: 18, alignSelf: 'center'}}>
                    <View
                        style={{
                            backgroundColor: '#fb5c88',
                            borderRadius: 3,
                            justifyContent: 'center',
                            width: 55,
                            height: 14,
                            alignSelf: 'center'
                        }}>
                        <Text numberOfLines={1}
                              style={{
                                  fontSize: 10,
                                  color: Color.colorWhite,
                                  alignSelf: 'center',
                                  backgroundColor: Color.transparent
                              }}>{that.formatGameType(data.status)}</Text>
                    </View>
                    <LoadImageView
                        style={{width: 14, height: 15, alignSelf: 'center', resizeMode: 'contain', marginLeft: 1}}
                        source={ImageManager.icButton}/>
                </View>
            </TouchableOpacity>
        </View>);
    }

    /**
     * 当前游戏状态view
     * 只有是游戏房间才有观战的状态
     *
     * @param data
     */
    gameStatusView(data) {
        let gameStatus = that.getGameStatus(data.status);
        if (gameStatus) {
            if (gameStatus.game_status == 2) {
                //准备中
                let gameStatusText = Language().game_prepare;
                let position = 0;
                if (gameStatus.position) {
                    position = gameStatus.position;
                }
                if (position > 11) {
                    gameStatusText = Language().game_watching;
                }
                return (<View
                    style={{position: 'absolute', bottom: 5, right: 5, borderRadius: 5, backgroundColor: '#ff9319'}}>
                    <Text
                        style={{
                            fontSize: 8,
                            color: Color.colorWhite,
                            marginLeft: 5,
                            marginRight: 5,
                            alignSelf: 'center'
                        }}>{gameStatusText}</Text>
                </View>);
            } else if (gameStatus.game_status == 3) {
                let gameStatusText = Language().game_start;
                let position = 0;
                if (gameStatus.position) {
                    position = gameStatus.position;
                }
                if (position > 11) {
                    gameStatusText = Language().game_watching;
                }
                //游戏中或是准备中
                return (
                    <View style={{
                        position: 'absolute',
                        bottom: 5,
                        right: 5,
                        borderRadius: 5,
                        backgroundColor: '#ff23c7'
                    }}>
                        <Text
                            style={{
                                fontSize: 8,
                                color: Color.colorWhite,
                                marginLeft: 5,
                                marginRight: 5,
                                alignSelf: 'center'
                            }}>{gameStatusText}</Text>
                    </View>);
            }
        }
        return null;
    }

    /**
     * 好友在线footer
     */
    renderFooter() {
        let friendCount = friendStatusList.length;
        let defaultWidth = ScreenWidth / 2 + 30;
        if (friendCount == 2) {
            defaultWidth = ScreenWidth / 4 + 30;
        }
        return (friendCount > 0 && friendCount < 3) ? (<View style={{justifyContent: 'center', width: defaultWidth}}>
            <Image style={{width: 75, height: 80, alignSelf: 'center', resizeMode: 'contain',}}
                   source={ImageManager.defaultFriendStatus}/>
            {/*<Text*/}
            {/*style={{padding:5,fontSize:12,color:'#DB6BFA',alignSelf:'center'}}>{Language().add_game_friend}</Text>*/}
        </View>) : null;
    }

    /**
     *
     * @param data
     * 游戏房间如果是游戏中，可观战
     */
    actionFriendView(data) {
        let gameStatus = that.getGameStatus(data.status);
        if (gameStatus) {
            if (gameStatus.game_status == 2) {
                //在房间中并且是准备状态
                that.enterFriendRoom(gameStatus.room_id);
            } else if (gameStatus.game_status == 3) {
                that.enterFriendRoom(gameStatus.room_id);
            } else {
                //进入好友详情
                that.actionContact(data);
            }
        } else {
            //进入好友详情
            that.actionContact(data);
        }
    }

    /**
     * 查看好友详情
     * @param item
     */
    actionContact(item) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                if (item && item.id) {
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                    return;
                }
                if (Platform.OS === 'ios') {
                    var main = NativeModules.MainViewController;
                    let sex = '2';
                    if (item.sex == 1) {
                        sex = '1';
                    }
                    let image = item.image;
                    if (image == null) {
                        image = '';
                    }
                    main.didSelectUser(item.id, 'detail', sex, image, item.name, '');
                } else {
                    NativeModules.NativeJSModule.rnStartPersonalInfoActivtiy(PersonalInfoActivity, item.id, this.props.token, false);
                }
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     *
     * @param roomId
     */
    enterFriendRoom(roomId) {
        //网络检测
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                try {
                    this.onShowProgressDialog();
                    this.state.roomId = roomId;
                    let url = apiDefines.SEARCH_ROOM_INFO + this.state.roomId + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id;
                    if (this.props.hostLocation) {
                        url = url + apiDefines.PARAMETER_LOCATION + this.props.hostLocation
                    }
                    Util.get(url, (code, message, data) => {
                        this.onHideProgressDialog();
                        if (code == 1000) {
                            if (data.level == GAME_TYPE_PRE_SIMPLE || data.level == GAME_TYPE_PRE_SIMPLE_NEW) {
                                //15級別以上不能進入新手房間
                                if (this.getUserLevel() > 15 && !this.checkIsRole()) {
                                    Toast.show(Language().prompt_novice_model);
                                    return;
                                }
                            }
                            this.state.game_type = data.level;
                            this.state.roomId = data.room_id;
                            if (data.password_needed) {
                                //弹框直接进入输入密码
                                this.setState({
                                    dialogIndex: 2,
                                    dialogConfirm: Language().enter,
                                    dialogCancel: Language().cancel,
                                    hideCancel: false,
                                    hideConfirm: false,
                                    password: '',
                                    needPassword: data.password_needed,
                                    textRoomID: roomId
                                });
                                this.onOpenModal();
                            } else {
                                //直接进入
                                this.enterRoom(this.state.roomId);
                            }

                        }
                        else if (code == 1004) {
                            //token 失效
                            this.onShowLoginDialog();
                        } else {
                            Toast.show(message);
                        }
                    }, (failed) => {
                        this.onHideProgressDialog();
                        Toast.show(Language().not_network)
                    });
                } catch (e) {
                    console.log('do search room error:' + e);
                }

            } else {
                //无网络提示
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 获取用户状态信息
     * @param status
     * @returns {{}}
     */
    getGameStatus(status) {
        let gameStatus = {};
        let arrayStatus = new Array();
        arrayStatus = status.split(':');
        for (let i = 0; i < arrayStatus.length; i++) {
            if (i == 0) {
                gameStatus.game_status = parseInt(arrayStatus[i]);
            } else if (i == 1) {
                gameStatus.updateTime = arrayStatus[i];
            } else if (i == 2) {
                gameStatus.room_id = arrayStatus[i];
            } else if (i == 3) {
                gameStatus.type = arrayStatus[i];
            } else if (i == 5) {
                gameStatus.position = parseInt(arrayStatus[i]);
            }
        }
        return gameStatus;
    }

    /**
     * 格式化状态信息
     * @param status
     * @returns {string|string}
     */
    formatGameType(status) {
        let gameType = Language().on_line;
        let gameStatus = that.getGameStatus(status);
        switch (gameStatus.type) {
            case GAME_TYPE_SIMPLE_6:
                gameType = Language().game_simple_6;
                break;
            case GAME_TYPE_SIMPLE_9:
                gameType = Language().game_simple_9;
                break;
            case GAME_TYPE_SIMPLE_10:
                gameType = Language().game_simple_10;
                break;
            case GAME_TYPE_NORMAL:
                gameType = Language().normal_cupid;
                break;
            case GAME_TYPE_NORMAL_GUARD:
                gameType = Language().normal_guard;
                break;
            case GAME_TYPE_HIGH_KING:
                gameType = Language().high_king;
                break;
            case GAME_TYPE_PRE_SIMPLE:
                gameType = Language().novice_model;
                break;
            case GAME_TYPE_PRE_SIMPLE_NEW:
                gameType = Language().hunts_model_6;
                break;
            case GAME_TYPE_AUDIO:
                gameType = Language().voice_model;
                break;
        }
        return gameType;
    }

    /**
     * 在线好友
     * @returns {XML}
     */
    onLineFriendView() {
        return (<View>
            <View flexDirection='row' style={{height: 30, marginTop: 10}}>
                <Text
                    style={{
                        fontSize: 16,
                        alignSelf: 'center',
                        color: '#333333',
                        marginLeft: 10,
                        flex: 1
                    }}>{Language().friend_on_line}</Text>
                {friendStatusList.length == 0 ? null : (
                    <TouchableOpacity onPress={this.fetchMoreFriendStatus.bind(this)}
                                      activeOpacity={0.8}
                                      style={{backgroundColor: Color.transparent, justifyContent: 'center', margin: 2}}>
                        <View flexDirection='row' style={{justifyContent: 'center', marginRight: 10}}>
                            <Text
                                style={{
                                    fontSize: 14,
                                    color: '#fb5c88',
                                    alignSelf: 'center',
                                    alignSelf: 'center'
                                }}>{Language().refresh_more}</Text>
                            <LoadImageView source={ImageManager.ic_refresh_more}
                                           style={{
                                               width: 16,
                                               height: 16,
                                               alignSelf: 'center',
                                               marginLeft: 5,
                                               resizeMode: 'contain'
                                           }}/>
                        </View>
                    </TouchableOpacity>
                )}
            </View>
            {this.friendStatusView()}
        </View>);
    }

    /**
     * 意见反馈
     * @returns {XML}
     */
    faceBackView() {
        return (<TouchableOpacity onPress={this._onFeedback.bind(this)}
                                  activeOpacity={0.8}
                                  style={{flex: 1, backgroundColor: '#F7F7F7'}}>
            <Text style={{color: '#C4BECD', fontSize: 14, alignSelf: 'center', margin: 10}}>{Language().feedback}</Text>
        </TouchableOpacity>);
    }

    /**
     * 点击按钮
     * @param type
     */
    onClick = (type) => {
        switch (type) {
            case MiniGameConstant.ENTER_MORE_MINI_GAME:
                this.openMoreMiniGame();
                break;
            case MiniGameConstant.ENTER_UNDERCOVER:
                this.enterUndercover();
                break;
            case MiniGameConstant.GAME_ANIMAL_FIGHT:
            case MiniGameConstant.GAME_LINK_FIVE:
            case MiniGameConstant.GAME_CATCH_THIEF:
                this.startMiniGame(type);
                break;
            case MiniGameConstant.ENTER_WEREWOLF:
                try {
                    this.props.openPage(SCENES.SCENE_WEREWOLF_GAME_HOME, 'global');
                } catch (e) {
                    console.log(e)
                }
                break;
            case MiniGameConstant.GAME_HOME_OPEN_NOTIFICATION:
                this.openAnnouncement();
                break;
            case MiniGameConstant.GAME_HOME_OPEN_GROUP:
                this.openGroup();
                break;
            case MiniGameConstant.GAME_HOME_OPEN_STORE:
                this.openStoreRecharge();
                break;
            case MiniGameConstant.GAME_HOME_SEARCH:
                this.onSearchRoom();
                break;
            case MiniGameConstant.ENTER_CHANGE_FACE:
                rnRoNativeUtils.openWebView(apiDefines.URL_HOST + apiDefines.OPEN_CHANGE_FACE);
                break;
        }
    }
    /**
     *
     * @param type
     */
    startMiniGame = (type) => {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().openMiniGame_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                //如果flag判断不是游客，但是确实用的游客的ID或是token，直接提示信息失效去登录
                if (this._checkTourist()) {
                    this.onShowLoginDialog();
                } else {
                    rnRoNativeUtils.rnStartMiniGame(type);
                }
            }
        });
    }
}

const mapDispatchToProps = dispatch => ({
    openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
    updateRoomId: (roomId, password) => dispatch((updateRoomId(roomId, password))),
    finishPage: (key) => dispatch(popRoute(key)),
    removeChatData: () => dispatch(removeChat()),
    updateUserInfo: (data) => dispatch((userInfo(data))),
    updateTourist: (isTourist) => dispatch((updateTourist(isTourist))),
    clearMessageList: () => dispatch(clearMessageList()),
    clearFriendList: () => dispatch(clearFriendList()),
    removeFriendStatus: () => dispatch(removeFriendStatus()),
    getMoreFriendStatus: () => dispatch(getMoreFriendStatus()),
    getUserInfo: (id) => dispatch(getUserInfo(id)),
    updateNoticeVer: (noticeVar) => dispatch(updateNoticeVer(noticeVar)),
    taskInfo: () => dispatch(taskInfo()),
    updateGiftManifest: (gifts) => dispatch(updateGiftManifest(gifts)),
    updateConfig: () => dispatch(checkOpenRegister()),
});
const mapStateToProps = state => ({
    code: state.userInfoReducer.code,
    message: state.userInfoReducer.message,
    userInfo: state.userInfoReducer.data,
    token: state.userInfoReducer.token,
    newRoomId: state.userInfoReducer.roomId,
    newPassword: state.userInfoReducer.password,
    navigation: state.cardNavigation,
    isTourist: state.userInfoReducer.isTourist,
    enterNoviceCount: state.configReducer.enter_novice_count,
    noticeVer: state.configReducer.newNoticeVer,
    hostLocation: state.configReducer.location,
    friendStatusDataList: state.friendStatusDataReducer.data,
    taskInfoData: state.taskInfoReducer.data,
    configData: state.checkRegisterReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps)(MiniGameHome);
