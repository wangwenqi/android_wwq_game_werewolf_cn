/**
 *  Created by wangxu on 20/02/2017.
 * 应用的首页，进行提供游戏入口，以及房间的搜素，以及其他功能的展示
 */
import React, {Component} from 'react';
import {
    Container,
    Button,
    Left,
    Thumbnail,
    Right,
    Body,
    ListItem,
    Item,
    Input,
    Card,
    CardItem,
    Icon,
    Radio,
    Content,
    Title,
    Spinner,
    CheckBox
} from 'native-base';
import {actions} from 'react-native-navigation-redux-helpers';
import {connect} from 'react-redux';
import {
    Image,
    ProgressViewIOS,
    NativeModules,
    AsyncStorage,
    Dimensions,
    View,
    Platform,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Keyboard,
    Modal, DeviceEventEmitter, NativeEventEmitter, InteractionManager, StyleSheet, Switch, WebView, ListView, Text
} from 'react-native';
//##自定义工具
import * as SCENES from '../../../support/actions/scene';
import * as GAME_TYPES from '../../../support/actions/game_types';
import Util from '../../../support/common/utils';
import  * as apiDefines from '../../../support/common/gameApiDefines';
import Toast from '../../../support/common/Toast';
import netWorkTool from '../../../support/common/netWorkTool';
import AsyncStorageTool from '../../../support/common/AsyncStorageTool';
import {clearFriendList} from '../../../support/actions/friendActions';
import {clearMessageList} from '../../../support/actions/messageActions';
import Dialog, {Alert, Confirm, Prompt,} from '../../../support/common/dialog';
import {removeChat} from '../../../support/actions/chatActions';
import {userInfo, updateTourist, updateRoomId, getUserInfo} from '../../../support/actions/userInfoActions';
import CachedImage from '../../../support/common/CachedImage';
import LoadImageView from '../../../support/common/LoadImageView';
import rnRoNativeUtils from '../../../support/common/rnToNativeUtils';
import * as Constant from '../../../support/common/constant';
import Loading from '../../../support/common/Loading';
import Dropdown from '../../../support/common/Dropdown';
import {getMoreFriendStatus, removeFriendStatus} from '../../../support/actions/friendStatusAction';
import {updateNoticeVer} from '../../../support/actions/configActions';
import {taskInfo} from '../../../support/actions/taskInfoActions';
import {updateGiftManifest} from '../../../support/actions/giftManifestActions';
import {checkOpenRegister} from '../../../support/actions/checkRegisterActions';
import styles from "./style";
import Language from '../../../resources/language';
import InternationSourceManager from '../../../resources/internationSourceManager';//国际化资源管理
import ImageManager from '../../../resources/imageManager';
import Color from '../../../resources/themColor';
const {
    pushRoute,
    popRoute
} = actions;
const {RNMessageSender}=NativeModules;
const rNMessageSenderEmitter = new NativeEventEmitter(RNMessageSender);
const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;
//
const dialogHelpHeight = ScreenHeight - 10;
const refreshMorePadding = (Platform.OS === 'ios') ? 4 : 0;
const gameModelTop = (Platform.OS === 'ios') ? 25 : 20;
const numberPaddingLeft = (Platform.OS === 'ios') ? 25 : 22;
const dialogPaddingTop = 30;
//游戏类型
const GAME_TYPE_SIMPLE = 'simple';
const GAME_TYPE_SIMPLE_6 = 'simple_6';//简单6人局
const GAME_TYPE_SIMPLE_9 = 'simple_9';//简单9人局
const GAME_TYPE_SIMPLE_10 = 'simple_10';//简单10人局
const GAME_TYPE_NORMAL = 'normal';//标准12人丘比特
const GAME_TYPE_NORMAL_GUARD = 'normal_guard';//标准12人守卫（新)
const GAME_TYPE_HIGH_KING = 'high_king';//12高级房间
const GAME_TYPE_PRE_SIMPLE = 'pre_simple';
const GAME_TYPE_PRE_SIMPLE_NEW = 'pre_simple_new';//6人新手房（亮身份）
const GAME_TYPE_AUDIO = 'audio';//语音房间
//
//游戏模式
const GAME_MODEL_SIMPLE = 'simple';
const GAME_MODEL_STANDARD = 'standard';
const GAME_MODEL_ADVANCED = 'advanced';
const GAME_MODEL_NOVICE = 'novice';
const PersonalInfoActivity = 'com.game_werewolf.activity.PersonalInfoActivity';
let progressDialog;
let needLoginDialog;
let webView;
let image = '';
let that;
let friendStatusList = new Array();
const roleType = {
    manager: 1 << 0,
    teacher: 1 << 1
}
class Home extends Component {
    static propTypes = {
        navigator: React.PropTypes.shape({}),
        openPage: React.PropTypes.func,
        userInfo: React.PropTypes.object,
        configData: React.PropTypes.object,
        friendStatusDataList: React.PropTypes.array,
        code: React.PropTypes.number,
        message: React.PropTypes.string,
        token: React.PropTypes.string,
        newRoomId: React.PropTypes.string,
        newPassword: React.PropTypes.string,
        updateRoomId: React.PropTypes.func,
        finishPage: React.PropTypes.func,
        navigation: React.PropTypes.shape({
            index: React.PropTypes.number,
            key: React.PropTypes.string,
            routes: React.PropTypes.array,

        }),
        removeChatData: React.PropTypes.func,
        updateUserInfo: React.PropTypes.func,
        updateTourist: React.PropTypes.func,
        isTourist: React.PropTypes.bool,
        clearMessageList: React.PropTypes.func,
        clearFriendList: React.PropTypes.func,
        removeFriendStatus: React.PropTypes.func,
        getMoreFriendStatus: React.PropTypes.func,
        getUserInfo: React.PropTypes.func,
        updateNoticeVer: React.PropTypes.func,
        taskInfo: React.PropTypes.func,
        enterNoviceCount: React.PropTypes.number,
        hostLocation: React.PropTypes.string,
        taskInfoData: React.PropTypes.object,
        noticeVer: React.PropTypes.number,
        updateGiftManifest: React.PropTypes.func,
        updateConfig: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        that = this;
        if (this.props.friendStatusDataList) {
            friendStatusList = this.props.friendStatusDataList;
        }
        //1.设置数据源
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        //2.设置返回数据
        this.state = {
            dataSource: ds.cloneWithRows(friendStatusList),
            dialogShow: false,
            dialogIndex: 0,
            dialogTitle: 'title',
            dialogConfirm: 'confirm',
            dialogCancel: 'cancel',
            hideConfirm: false,
            hideCancel: false,
            roomId: '',
            textRoomID: '',
            password: '',
            switchIsOn: false,
            needPassword: false,
            gameHelpType: '',
            gameHelpTitle: '',
            gameHelpDialogShow: false,
            gameHelpMenu: true,
            simple_model_radio: true,
            standard_model_radio: false,
            game_type: GAME_TYPE_SIMPLE_6,
            room_id_focus: false,
            updateGame: true,
            touristCanEnterOtherRoom: false,
            room_type: '',
            showModelProgress: false,
            showAnnouncementModel: false,
            showGameModelManager: false,
            activityGameMode: GAME_MODEL_SIMPLE,//当前选中的模式
            game_limit_level: 0,
            game_limit_level_name: Language().no_limit,
            select_game_play: Language().game_simple_6,
            search_dialog_activity: 0,//0房间，1好友，2家族
            create_room_count: 0
        };
    }

    render() {
        let imageTW = false;
        let gameInfo = null;
        let userInfo = this.props.userInfo;
        let level = 0;
        let odds = 0;
        let gold = 0;
        let dim = 0;
        let name = '';
        let image = ''
        if (userInfo != null) {
            gameInfo = userInfo.game;
            name = userInfo.name;
            if (userInfo.image && userInfo.image != '') {
                image = userInfo.image;
            }
            if (userInfo.money) {
                if (userInfo.money.gold > 0) {
                    gold = userInfo.money.gold;
                    if (gold.toString().length > 6) {
                        gold = gold.toString().slice(0, 6) + '+';
                    }
                }
                if (userInfo.money.dim > 0) {
                    dim = userInfo.money.dim;
                    if (dim.toString().length > 6) {
                        dim = dim.toString().slice(0, 6) + '+';
                    }
                }
            }
        }
        if (gameInfo != null && gameInfo.level != null && gameInfo.lose != null) {
            level = gameInfo.level;
            if ((gameInfo.win + gameInfo.lose + gameInfo.escape) > 0) {
                odds = gameInfo.win / (gameInfo.win + gameInfo.lose + gameInfo.escape) * 100;
                odds = odds.toFixed(0);
            }
        }
        if (this.props.friendStatusDataList) {
            friendStatusList = this.props.friendStatusDataList;
        } else {
            friendStatusList = new Array();
        }
        let helpDialogCancel = this.state.gameHelpMenu ? (Language().return_home) : (Language().return_game_help);
        return (
            <Container>
                <ScrollView style={{backgroundColor:'#f7f7f7'}} showsVerticalScrollIndicator={false}>
                    <View style={{backgroundColor:Color.transparent,width:ScreenWidth}}>
                        <CachedImage
                            style={{resizeMode: 'contain', alignSelf: 'center',width:ScreenWidth,height:ScreenWidth*0.379}}
                            source={ImageManager.icGameHomeHead}/>
                        <View flexDirection='row' style={{position: 'absolute',bottom:12,left:0,right:0}}>
                            <View style={{flex:1.2,justifyContent:'center',height:75}}>
                                <TouchableOpacity activeOpacity={0.8}
                                                  style={{justifyContent:'center'}}
                                                  onPress={this.onOpenUserInfo.bind(this)}>
                                    <View flexDirection='row'>
                                        <View
                                            style={{backgroundColor:'rgba(0, 0, 26, 0.5)',width:68,height:68,borderRadius:34,justifyContent:'center',marginLeft:13}}>
                                            <View
                                                style={{backgroundColor:Color.colorWhite,width:63,height:63,borderRadius:32,justifyContent:'center',alignSelf:'center'}}>
                                                <LoadImageView style={styles.headThumbnail}
                                                               source={(image)?{uri:image}:ImageManager.defaultUser}
                                                               defaultSource={ImageManager.defaultUser}/>
                                            </View>
                                        </View>
                                        <View style={{alignSelf:'center',marginLeft:2}}>
                                            <Text numberOfLines={1}
                                                  style={{color:Color.colorWhite,fontSize:15}}>{name}</Text>
                                            {this.props.isTourist ? (<Text
                                                    style={{color:(Color.tourist_prompt_text_color),fontSize:11,alignSelf:'flex-start',marginTop:15}}
                                                    numberOfLines={2}>{Language().tourist_prompt}</Text>) : (
                                                    <View flexDirection='row' style={{marginTop:8}}>
                                                        <Text numberOfLines={1}
                                                              style={{color:'#4fffff',fontSize:13}}>LV.{level}</Text>
                                                        <Text numberOfLines={1}
                                                              style={{color:Color.colorWhite,fontSize:13,marginLeft:5}}>{Language().odds}{odds}%</Text>
                                                    </View>)}
                                        </View>
                                    </View>

                                </TouchableOpacity>
                            </View>
                            <View style={{flex:1}}>
                                {/*个人财富*/}
                                {this.myWealthView(gold, dim)}
                                {/*家族／任务／战绩*/}
                                {this.recordGroupBoxView(imageTW)}
                            </View>
                        </View>
                    </View>
                    {/*其他游戏辅助模块*/}
                    {this.gameAuxiliaryView(imageTW)}
                    {/*小游戏模块*/}
                    {this.miniGameView()}
                    {/*第一梯队游戏模块*/}
                    {this.gameFirstLine(imageTW)}
                    {/*第二梯队游戏模块*/}
                    {this.gameSecondLine(imageTW, level)}
                    {/*第三梯队游戏模块*/}
                    {this.gameThirdLine(imageTW, level)}
                    {/*好友在线*/}
                    {this.onLineFriendView()}
                    {/*faceback*/}
                    {this.faceBackView()}
                </ScrollView>
                {/*创建房间以及搜索房间的弹框*/}
                <Modal visible={this.state.dialogShow} transparent={true} animationType='fade'
                       onRequestClose={() => {console.log('close')}}>
                    <Container flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                               style={[styles.dialogContainer]}>

                        <ScrollView alwaysBounceVertical={false}>
                            <View
                                style={{borderColor:Color.action_room_color,borderRadius:5,borderWidth:2,width:ScreenWidth-50,maxHeight:ScreenHeight-50,alignSelf:'center',backgroundColor:Color.colorWhite}}>
                                {this.dialogContentManager()}
                                <View flexDirection='row' style={{justifyContent:'center'}}>
                                    <TouchableOpacity activeOpacity={0.9}
                                                      onPress={this.onCloseModal.bind(this)}
                                                      style={styles.actionButton}>
                                        <Text
                                            style={{alignSelf:'center',color:Color.colorWhite}}>{this.state.dialogCancel}</Text>
                                    </TouchableOpacity>
                                    <View style={{backgroundColor:'#8732ff',width:1,paddingBottom:4,paddingTop:4}}/>
                                    <TouchableOpacity activeOpacity={0.8} onPress={this.onConfirmModal.bind(this)}
                                                      style={styles.actionButton}>
                                        <Text
                                            style={{alignSelf:'center',color:Color.colorWhite}}>{this.state.dialogConfirm}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.showModelProgress ? (
                                    <View style={[styles.loading,{left:ScreenWidth/2-55,top:45}]}>
                                        <Spinner size='small' style={{height:35}}/>
                                        <Text
                                            style={{ marginTop: 5,fontSize: 14,color:Color.colorWhite}}>{Language().request_loading}</Text>
                                    </View>) : null}
                        </ScrollView>
                    </Container>
                </Modal>
                {/*帮助手册*/}
                <Modal visible={this.state.gameHelpDialogShow} transparent={true} animationType='fade'
                       onRequestClose={() => {console.log('close')}}>
                    <Container flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.65)'
                               style={[styles.dialogContainer]}>
                        <Container
                            style={{height:dialogHelpHeight,backgroundColor:'rgba(0, 0, 0, 0)',width:ScreenWidth,paddingTop:dialogPaddingTop}}>
                            <CardItem cardBody style={{height:80,backgroundColor:'rgba(0, 0, 0, 0)'}}>
                                <CachedImage source={ImageManager.helpDialogTitleBg}
                                             style={{resizeMode: 'contain',flex:1,alignSelf:'center'}}>
                                    <Left style={{flex:1}}>
                                        <Text numberOfLines={1}
                                              style={{fontSize:21,fontWeight:'bold',textAlign:'center',alignSelf:'center',color:Color.colorWhite}}>{this.state.gameHelpTitle}</Text>
                                    </Left>
                                </CachedImage>
                            </CardItem>
                            <View style={{marginLeft:5,marginRight:5,flex:1,top:-5,backgroundColor:'#32184C'}}>
                                <View
                                    style={{flex:6,backgroundColor:(Color.gameHelpDialogBgColor),marginLeft:1,marginRight:1}}>
                                    {this.gameHelpContentManager()}
                                </View>
                                <CardItem cardBody
                                          style={{backgroundColor:'#3F1C62',height:52,borderRadius:1,marginLeft:1,marginRight:1}}>
                                    <Button full transparent
                                            style={{alignSelf:'center',flex:1,height:50,justifyContent:'center'}}
                                            onPress={this.closeGameHelpDialog.bind(this)}>
                                        <Text
                                            style={{color:(Color.colorWhite),fontSize:18,fontWeight:'bold',alignSelf:'center',textAlign:'center',marginBottom:5}}>{helpDialogCancel}</Text>
                                    </Button>
                                </CardItem>
                            </View>

                        </Container>
                    </Container>
                </Modal>
                {/*公告*/}
                <Modal visible={this.state.showAnnouncementModel} transparent={true} animationType='fade'
                       onRequestClose={() => {console.log('close')}}>
                    <Container flexDirection='row' alignItems='center' backgroundColor='rgba(0, 0, 0, 0.6)'
                               style={styles.dialogContainer}>
                        <View
                            style={{borderColor:Color.dialog_announcement_color,borderRadius:5,borderWidth:2,width:ScreenWidth-60,height:ScreenHeight / 3 * 2-20,alignSelf:'center',backgroundColor:Color.dialog_announcement_content_color,left:30, position: 'absolute',top:(ScreenHeight-(ScreenHeight / 3 * 2-20))/2,overflow:'hidden',padding:2}}>
                            <WebView
                                style={{flex:1,borderRadius:5,backgroundColor:Color.dialog_announcement_content_color}}
                                ref="webView"
                                source={{uri: apiDefines.NOTICE_URL+this.props.token}}
                                javaScriptEnabled={true}
                                domStorageEnabled={true}
                                decelerationRate="normal"
                                startInLoadingState={true}
                                onLoad={this.loadSuccessNotice.bind(this)}
                                renderError={this._webViewError.bind(this)}/>
                        </View>
                        <View
                            style={{position: 'absolute',alignSelf:'center',left:ScreenWidth/2-45,top:(ScreenHeight-(ScreenHeight / 3 * 2-20))/2-75}}>
                            <Image source={ImageManager.iconAnnouncementHeader} style={styles.announcementHeader}/>
                            <Text
                                style={{top:60,left:26,color:'#7F4D11',fontSize: 18,fontWeight: 'bold'}}>{Language().notice}</Text>
                        </View>
                        <View
                            style={{position: 'absolute',alignSelf:'center',left:ScreenWidth/2-15,top:(ScreenHeight / 3 * 2+100)}}>
                            <Image source={ImageManager.iconAnnouncementClose} style={styles.announcementClose}/>

                            <TouchableOpacity activeOpacity={0.8} onPress={this.closeAnnouncementModel.bind(this)}
                                              style={{ width: 30,height: 30,backgroundColor:Color.transparent}}>
                            </TouchableOpacity>
                        </View>
                    </Container>
                </Modal>
                {/*房间模式*/}
                <Modal visible={this.state.showGameModelManager} transparent={true} animationType='fade'
                       onRequestClose={() => {this.closeGameModelManager()}}>
                    <CachedImage source={this._getActivityGameModelBg().modelSource} style={styles.image}/>
                    <TouchableOpacity activeOpacity={1} onPress={this.closeGameModelManager.bind(this)}
                                      style={[styles.dialogContainer,{backgroundColor:Color.transparent}]}>
                        <Text
                            style={{color:Color.colorWhite,fontSize: 25,fontWeight: 'bold',top:gameModelTop,padding:5}}>{this._getActivityGameModel()}</Text>
                        <Container flexDirection='row' backgroundColor={Color.transparent}
                                   style={{flex:1,marginTop:30}}>
                            <ScrollView alwaysBounceVertical={false}>
                                {this._gameModelManager()}
                            </ScrollView>
                        </Container>
                        <View flexDirection='row' style={{marginBottom:15}}>
                            <CachedImage
                                style={{width:40,resizeMode: 'contain', alignSelf: 'center',height:40}}
                                source={ImageManager.game_model_icon}/>
                            <View style={{marginLeft:5,alignSelf: 'center'}}>
                                <Text
                                    style={{fontSize:16,fontWeight:'bold',color:Color.colorWhite}}>{Language().game_werewolf}</Text>
                                <Text
                                    style={{fontSize:13,color:this._getActivityGameModelBg().modelTextColor,marginTop:3}}>{Language().werewolf_publicity}</Text>
                            </View>
                        </View>

                    </TouchableOpacity>
                </Modal>
                <Loading ref="progressDialog" loadingTitle={Language().request_loading}/>
                {Dialog.inject()}
                <Alert ref='login' title={Language().title_login} message={Language().need_login_message}
                       messageStyle={{alignSelf:'center',fontSize: 14,marginBottom:10}}
                       titleStyle={{alignSelf:'center',fontSize: 17,padding: 5}}
                       contentStyle={{paddingLeft:1,paddingBottom: 3,paddingRight: 1,width: ScreenWidth/3*2+30}}
                       buttonBarStyle={{justifyContent:'center'}}
                       buttonStyle={{textAlign: 'center',fontSize: 16,padding: 10,marginLeft: 0,marginRight: 0,alignSelf:'center',justifyContent:'center',flex:1,color:Color.system_dialog_bt_text_color}}
                       posText={Language().confirm}
                       rowSeparator={true}
                       onPosClick={this.login.bind(this)}> </Alert>
                <Confirm ref='touristModal' title={Language().title_prompt_tourist_login} posText={Language().go_login}
                         messageStyle={{alignSelf:'center',fontSize: 14,marginBottom:10}}
                         titleStyle={{alignSelf:'center',fontSize: 17,padding: 5}}
                         contentStyle={{paddingLeft:1,paddingBottom: 3,paddingRight: 1,width: ScreenWidth/3*2+30}}
                         buttonBarStyle={{justifyContent:'center'}}
                         buttonStyle={{textAlign: 'center',fontSize: 16,padding: 10,marginLeft: 0,marginRight: 0,alignSelf:'center',justifyContent:'center',flex:1,color:Color.system_dialog_bt_text_color}}
                         negText={Language().cancel}
                         enumeSeparator={true}
                         rowSeparator={true}
                         onNegClick={this.dismissTouristDialog.bind(this)} onPosClick={this.toLogin.bind(this)}>
                </Confirm>
            </Container>
        );
    }

    /**
     * 意见反馈
     * @returns {XML}
     */
    faceBackView() {
        return ( <TouchableOpacity onPress={this._onFeedback.bind(this)}
                                   activeOpacity={0.8}
                                   style={{flex:1,backgroundColor:'rgba(0, 0, 0, 0)',padding:10}}>
            <Text style={{color:'#C4BECD',fontSize: 14,alignSelf:'center'}}>{Language().feedback}</Text>
        </TouchableOpacity>);
    }

    /**
     * 在线好友
     * @returns {XML}
     */
    onLineFriendView() {
        return ( <View>
            <View flexDirection='row' style={{height:30}}>
                <CachedImage source={ImageManager.icHomeContactLine}
                             style={{resizeMode: 'contain',width:15,height:8,marginLeft:8,marginRight:5,alignSelf:'center'}}/>
                <Text style={{fontSize:12,alignSelf:'center',color:'#e944a7'}}>{Language().friend_on_line}</Text>
                {friendStatusList.length == 0 ? null : (<View
                        style={{borderColor:'#e944a7',borderRadius:12,borderWidth:1,padding:1,justifyContent:'center',margin:2,position: 'absolute',top:6,bottom:0,right:10,height:16}}>
                        <TouchableOpacity onPress={this.fetchMoreFriendStatus.bind(this)}
                                          activeOpacity={0.7}
                                          style={{backgroundColor:Color.transparent,justifyContent:'center'}}>
                            <Text
                                style={{fontSize:10,color:'#e944a7',paddingLeft:7,paddingRight:7,paddingTop:refreshMorePadding,paddingBottom:refreshMorePadding}}>{Language().refresh_more}</Text>
                        </TouchableOpacity>
                    </View>)}
            </View>
            {this.friendStatusView()}
        </View>);
    }

    /**'
     *第一梯队游戏
     * 1.新手用户显示新手模块／个性房间；
     * 2.非新手用户显示简单模式／个性房间
     * @param imageTW
     */
    gameFirstLine(imageTW, level) {
        if (this.checkIsNovice()) {
            return ( <View flexDirection='row' style={[styles.game_view_style,{marginTop:0}]}>
                <TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center'}}
                    onPress={this.enterNoviceModel.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={InternationSourceManager().bgPrimary}
                                 style={{resizeMode: 'contain',width:(ScreenWidth-7.5)/2*0.6*2.28,height:(ScreenWidth-7.5)/2*0.6}}/>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center'}}
                    onPress={this.enterPersonalityModel.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={InternationSourceManager().bgSamllPersonality}
                                 style={{resizeMode: 'contain',width:(ScreenWidth-7.5)/2*0.6,height:(ScreenWidth-7.5)/2*0.6,marginLeft:2.5}}/>
                </TouchableOpacity>
            </View>);
        } else {
            return ( <View flexDirection='row' style={[styles.game_view_style,{marginTop:1}]}>
                <TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center'}}
                    onPress={this.enterSimpleModel.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={InternationSourceManager().bgSimpleModel}
                                 style={{resizeMode: 'contain',width:(ScreenWidth-8)/2,height:(ScreenWidth-7.5)/2*0.6}}/>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center'}}
                    onPress={this.enterPersonalityModel.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={InternationSourceManager().bgPersonality}
                                 style={{resizeMode: 'contain',width:(ScreenWidth-8)/2,height:(ScreenWidth-7.5)/2*0.6,marginLeft:2.5}}/>
                </TouchableOpacity>
            </View>);
        }

    }

    /**
     *第二梯队游戏
     * 1.新手用户显示简单模块／谁是卧底；
     * 2.非新手用户显示标准高阶组合模式
     * @param imageTw
     */
    gameSecondLine(imageTW, level) {
        if (this.checkIsNovice()) {
            let simpleSource = InternationSourceManager().bgSimpleModel;
            if (level < 3) {
                simpleSource = InternationSourceManager().bgSimpleModelLock;
            }
            return ( <View flexDirection='row' style={[styles.game_view_style,{marginTop:2}]}>
                <TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center'}}
                    onPress={this.enterSimpleModel.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={simpleSource}
                                 style={{resizeMode: 'contain',width:(ScreenWidth-8)/2,height:(ScreenWidth-7.5)/2*0.6}}/>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center'}}
                    onPress={this.enterUndercover.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={InternationSourceManager().bgUndercoverSamll}
                                 style={{resizeMode: 'contain',width:(ScreenWidth-8)/2,height:(ScreenWidth-7.5)/2*0.6,marginLeft:2.5}}/>
                </TouchableOpacity>
            </View>);
        } else {
            let adSource = InternationSourceManager().bgAdvancedModel;
            if (level < 12) {
                adSource = InternationSourceManager().bgAdvancedModelLock;
            }
            return ( <View flexDirection='row' style={[styles.game_view_style,{marginTop:2}]}>
                <TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center'}}
                    onPress={this.enterStandardModel.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={adSource}
                                 style={{resizeMode: 'contain',width:ScreenWidth-5,height:(ScreenWidth-5)*0.3}}/>
                </TouchableOpacity>
            </View>);
        }
    }

    /**
     *第三梯队游戏
     * 1.新手用户显示显示标准高阶组合模式
     * 2.非新手用户显示谁是卧底／新手模式
     * @param imageTW
     */
    gameThirdLine(imageTW, level) {
        if (this.checkIsNovice()) {
            let adSource = InternationSourceManager().bgAdvancedModel;
            if (level < 12) {
                adSource = InternationSourceManager().bgAdvancedModelLock;
            }
            return ( <View flexDirection='row' style={[styles.game_view_style,{marginTop:2}]}>
                <TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center'}}
                    onPress={this.enterStandardModel.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={adSource}
                                 style={{resizeMode: 'contain',width:ScreenWidth-5,height:(ScreenWidth-5)*0.3}}/>
                </TouchableOpacity>
            </View>);
        } else {
            let PrimarySource = InternationSourceManager().bgPrimarySamll;
            if (level > 15 && !this.checkIsRole()) {
                PrimarySource = InternationSourceManager().bgPrimaryLock;
            }
            return ( <View flexDirection='row' style={[styles.game_view_style,{marginTop:2}]}>
                <TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center'}}
                    onPress={this.enterUndercover.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={InternationSourceManager().bgUndercover}
                                 style={{resizeMode: 'contain',width:(ScreenWidth-7.5)/2*0.6*2.28,height:(ScreenWidth-7.5)/2*0.6}}/>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center'}}
                    onPress={this.enterNoviceModel.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={PrimarySource}
                                 style={{resizeMode: 'contain',width:(ScreenWidth-7.5)/2*0.6,height:(ScreenWidth-7.5)/2*0.6,marginLeft:5}}/>
                </TouchableOpacity>
            </View>);
        }
    }

    /**
     * 家族／任务／战绩
     * @param imageTW
     * @returns {XML}
     */
    recordGroupBoxView(imageTW) {
        return (<View flexDirection='row' style={{marginRight:5,justifyContent:'flex-end',marginTop:5}}>
            {this.checkShowPay() ? (<TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center'}}
                    onPress={this.onOpenBox.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={InternationSourceManager().bgHomeBox} style={styles.homeBox}/>
                    {this.taskInfoView()}
                </TouchableOpacity>) : null}
            <TouchableOpacity
                style={{backgroundColor:Color.transparent,justifyContent:'center',marginLeft:10}}
                onPress={this.openGroup.bind(this)}
                activeOpacity={0.8}>
                <CachedImage source={InternationSourceManager().bgHomeGroup}
                             style={styles.homeBox}/>
            </TouchableOpacity>
            {this.checkShowPay() ? ( <TouchableOpacity
                    style={{backgroundColor:Color.transparent,justifyContent:'center',marginLeft:10}}
                    onPress={this.onOpenStore.bind(this)}
                    activeOpacity={0.8}>
                    <CachedImage source={InternationSourceManager().bgHomeStore}
                                 style={styles.homeBox}/>
                </TouchableOpacity>) : null
            }

        </View>);
    }

    /**
     *小游戏
     */
    miniGameView() {
        return ( <View>
            <View style={{height:5,backgroundColor:'#ebebeb'}}/>
            <TouchableOpacity
                activeOpacity={0.6}
                onPress={this.openMoreMiniGame.bind(this)}
                style={{backgroundColor:Color.transparent,justifyContent:'center'}}>
                <View flexDirection='row' style={{height:15,marginTop:2.5}}>
                    <CachedImage source={ImageManager.ic_home_mini_game_lineLine}
                                 style={{resizeMode: 'contain',width:15,height:8,marginLeft:8,marginRight:5,alignSelf:'center'}}/>
                    <Text style={{fontSize:12,alignSelf:'center',color:'#5c00b7'}}>{Language().more_mini_game}</Text>
                </View>
                <View flexDirection='row'
                      style={{justifyContent:'center',width:ScreenWidth,height:(ScreenWidth-5)*0.25,paddingLeft:8}}>
                    {this.rowMiniGame(ImageManager.ic_animal_checker, Language().animal_checker)}
                    {this.rowMiniGame(ImageManager.ic_gobang, Language().ic_gobang)}
                    {this.rowMiniGame(ImageManager.ic_basketball, Language().ic_basketball)}
                    {this.rowMiniGame(ImageManager.icon_more_mini_game, Language().icon_more_mini_game)}
                    <CachedImage source={ImageManager.icon_enter_mini_game}
                                 style={{width:15,height:25,alignSelf:'center',resizeMode: 'contain',marginRight:8}}/>
                </View>
            </TouchableOpacity>
            <View style={{height:5,backgroundColor:'#ebebeb'}}/>
        </View>);
    }

    /**
     *开启小游戏
     */
    openMoreMiniGame() {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().openMiniGame_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                //如果flag判断不是游客，但是确实用的游客的ID或是token，直接提示信息失效去登录
                if (this._checkTourist()) {
                    this.onShowLoginDialog();
                } else {
                    try {
                        this.props.openPage(SCENES.SCENE_MINI_GAME_MANAGER, 'global')
                    } catch (e) {
                    }
                }
            }
        });
    }

    /**
     *
     * @param sourceImage
     * @param name
     * @returns {XML}
     */
    rowMiniGame(sourceImage, name) {
        return (<View style={{justifyContent:'center',alignSelf:'center',flex:1}}>
            <View
                style={{backgroundColor:'rgba(201, 201, 201, 0.5)',width:(ScreenWidth-5)*0.25*0.68+4,height:(ScreenWidth-5)*0.25*0.68+4,alignSelf:'center',borderRadius:14,justifyContent:'center'}}>
                <CachedImage source={sourceImage}
                             style={{width:(ScreenWidth-5)*0.25*0.68,height:(ScreenWidth-5)*0.25*0.68,alignSelf:'center'}}/>
            </View>
            <Text style={{alignSelf:'center',fontSize:10,marginTop:2,color:'#808080'}}>{name}</Text>
        </View>);
    }

    /**
     * 游戏辅助模块view
     * @param imageTW
     */
    gameAuxiliaryView(imageTW) {
        return (<View flexDirection='row' style={{justifyContent:'center',margin:2.5,marginBottom:0.5}}>
            <View style={{justifyContent:'center'}}>
                <TouchableOpacity onPress={this.onSearchRoom.bind(this)}
                                  activeOpacity={0.8}
                                  style={styles.game_auxiliary_view}>
                    <CachedImage source={InternationSourceManager().bgSearch}
                                 style={styles.game_auxiliary_image}/>
                </TouchableOpacity>
            </View>
            <View style={{justifyContent:'center'}}>
                <TouchableOpacity onPress={this.onCreateRoom.bind(this)}
                                  activeOpacity={0.8}
                                  style={styles.game_auxiliary_view}>
                    <CachedImage source={InternationSourceManager().bgCreate}
                                 style={styles.game_auxiliary_image}/>
                </TouchableOpacity>
            </View>
            <View style={{justifyContent:'center'}}>
                <TouchableOpacity onPress={this.onOpenGameHelp.bind(this)}
                                  activeOpacity={0.8}
                                  style={styles.game_auxiliary_view}>
                    <CachedImage source={InternationSourceManager().bgHelp}
                                 style={styles.game_auxiliary_image}/>
                </TouchableOpacity>
            </View>
            <View style={{justifyContent:'center'}}>
                <TouchableOpacity onPress={this.openLeaderboard.bind(this)}
                                  activeOpacity={0.8}
                                  style={styles.game_auxiliary_view}>
                    <CachedImage source={InternationSourceManager().bgLeaderboard}
                                 style={styles.game_auxiliary_image}/>
                </TouchableOpacity>
            </View>
            <View style={{justifyContent:'center'}}>
                <TouchableOpacity onPress={this.openAnnouncement.bind(this)}
                                  activeOpacity={0.8}
                                  style={styles.game_auxiliary_view}>
                    <CachedImage source={InternationSourceManager().bgAnnouncement}
                                 style={[styles.game_auxiliary_image,{marginRight:0}]}/>
                    {this.announcementNewView()}
                </TouchableOpacity>
            </View>
        </View>)
    }

    /**
     * 个人财富view
     */
    myWealthView(gold, dim) {
        if (this.checkShowPay()) {
            return (<View flexDirection='row' style={{marginTop:5,marginRight:5}}>
                <TouchableOpacity
                    onPress={this.onOpenBox.bind(this)}
                    style={{backgroundColor:Color.transparent,justifyContent:'center',flex:1}}
                    activeOpacity={0.8}>
                    <CachedImage source={ImageManager.goldIcon} style={styles.homeCurrency}/>
                    <View flexDirection='row'
                          style={{backgroundColor:Color.transparent,justifyContent:'center',alignSelf:'center',width:(ScreenWidth*0.45-10)/2,height:(ScreenWidth*0.45-10)/2*0.296,}}>
                        <Text numberOfLines={1}
                              style={{fontSize:9,paddingLeft:numberPaddingLeft,alignSelf:'center',flex:1,color:'rgba(0, 0, 26, 1)',backgroundColor:Color.transparent}}>{gold}</Text>
                        <CachedImage source={ImageManager.addGold} style={styles.homeAddCurrency}/>
                    </View>

                </TouchableOpacity>
                <TouchableOpacity
                    onPress={this.openStoreRecharge.bind(this)}
                    style={{backgroundColor:Color.transparent,justifyContent:'center',marginLeft:5,flex:1}}
                    activeOpacity={0.8}>
                    <CachedImage source={ImageManager.jewelIcon} style={styles.homeCurrency}/>

                    <View flexDirection='row'
                          style={{backgroundColor:Color.transparent,justifyContent:'center',alignSelf:'center',width:(ScreenWidth*0.45-10)/2,height:(ScreenWidth*0.45-10)/2*0.296,marginTop:0.5}}>
                        <Text numberOfLines={1}
                              style={{fontSize:9,paddingLeft:numberPaddingLeft,alignSelf:'center',flex:1,color:'rgba(0, 0, 26, 1)',backgroundColor:Color.transparent}}>{dim}</Text>
                        <CachedImage source={ImageManager.addJewel} style={[styles.homeAddCurrency]}/>
                    </View>
                </TouchableOpacity>
            </View>);
        } else {
            return (<View style={{height:22}}/>);
        }
    }

    /**
     * 检测是否显示支付
     * @returns {boolean}
     */
    checkShowPay() {
        if (Platform.OS === 'ios') {
            if (this.props.isTourist) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    _onReloadView() {
        if (webView) {
            webView.reload();
        }
    }

    /**
     *
     * @returns {XML}
     * @private
     */
    _webViewError() {
        return (
            <View style={{flex: 1,backgroundColor:Color.dialog_announcement_content_color,justifyContent:'center'}}>
                <TouchableOpacity style={styles.webErrorBt} onPress={this._onReloadView.bind(this)}>
                    <Image style={[styles.webViewError,{width:ScreenWidth/3,height:ScreenWidth/3+20}]}
                           source={ImageManager.bgWebViewError}/>
                </TouchableOpacity>
            </View>);
    }

    /**
     * 公告
     */
    openAnnouncement() {
        this.showAnnouncementModel();
    }

    /**
     * 排行榜
     */
    openLeaderboard() {
        rnRoNativeUtils.openLeaderboard();
    }

    /**
     * 监测是否shi
     * @returns {boolean}
     */
    checkIsNovice() {
        if (this.props.userInfo && this.props.userInfo.game) {
            let game = this.props.userInfo.game;
            if (game.level <= 15) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    login() {
        this.clearData();
        this.onHideLoginDialog();

    }

    /**
     *
     */
    dismissTouristDialog() {
        if (this.refs.touristModal) {
            this.refs.touristModal.hide();
        }
        if (this.state.touristCanEnterOtherRoom) {
            //进入游戏
            this.onShowProgressDialog();
            this.enterRoom(this.state.roomId);
            //统计
            rnRoNativeUtils.onStatistics(Constant.SIMPLE_MODE);
        }
    }

    /**
     *
     */
    toLogin() {
        this.clearData();
        if (this.refs.touristModal) {
            this.refs.touristModal.hide();
        }
    }

    /**
     *  清理数据缓存
     */
    clearData() {
        const routes = this.props.navigation.routes;
        if (routes.length > 1) {
            for (var i = 0; i < routes.length; i++) {
                this.props.finishPage('global');
            }
        }
        if (this.props.userInfo != null && this.props.userInfo.id != null) {
            let userId = this.props.userInfo.id;
            rnRoNativeUtils.closeLeanCloud(userId);
        }
        AsyncStorageTool.removeUserInfo();
        try {
            this.props.openPage(SCENES.SCENE_SPLASH, 'global');
        } catch (e) {
            console.log(e)
        }
        AsyncStorageTool.removeSignTourist();
        this.props.removeChatData();
        this.props.updateUserInfo(null);
        this.props.updateTourist(false);
        this.props.clearFriendList();
        this.props.removeFriendStatus();
        this.props.clearMessageList();
    }

    /**
     * 以及反馈
     * @private
     */
    _onFeedback() {
        rnRoNativeUtils.onFeedback();
    }

    onFromBrowerEnterRoom() {
        this.doFromBrowerEnterRoom();
    }

    /**
     *从浏览器进入的具体操作
     */
    doFromBrowerEnterRoom() {
        //网络检测
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                try {
                    this.onShowProgressDialog();
                    this.state.roomId = this.props.newRoomId;
                    this.state.password = this.props.newPassword;
                    this.props.updateRoomId('', '');
                    let url = apiDefines.SEARCH_ROOM_INFO + this.state.roomId + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id;
                    if (this.props.hostLocation) {
                        url = url + apiDefines.PARAMETER_LOCATION + this.props.hostLocation;
                    }
                    Util.get(url, (code, message, data) => {
                        this.onHideProgressDialog();
                        if (code == 1000) {
                            if (data.level == GAME_TYPE_PRE_SIMPLE || data.level == GAME_TYPE_PRE_SIMPLE_NEW) {
                                //15級別以上不能進入新手房間
                                if (this.getUserLevel() > 15 && !this.checkIsRole()) {
                                    Toast.show(Language().prompt_novice_model);
                                    return;
                                }
                            }
                            this.state.game_type = data.level;
                            this.state.roomId = data.room_id;
                            this.enterRoom(this.state.roomId);
                        } else if (code == 1004) {
                            //token 失效
                            this.onShowLoginDialog();
                        } else {
                            Toast.show(message);
                        }
                    }, (failed) => {
                        Toast.show(Language().not_network);
                    });
                } catch (e) {
                    console.log('doFromBrowerEnterRoom error:' + e);
                }
            } else {
                this.props.updateRoomId('', '');
                //无网络提示
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
        if (this.props.newRoomId != '') {
            this.onFromBrowerEnterRoom();
        }
    }

    componentDidUpdate() {
        if (this.props.newRoomId != '') {
            this.onFromBrowerEnterRoom();
        }
    }

    componentDidMount() {
        //页面渲染完毕后定义加载view
        progressDialog = this.refs.progressDialog;
        needLoginDialog = this.refs.login;
        webView = this.refs.webView;
        this.checkTokenTimer = setTimeout(() => {
            this.checkToken();
        }, 500);
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        try {
            if (Platform.OS === 'android') {
                this.onLeaveGameListener = DeviceEventEmitter.addListener('EVENT_USER_LEAVE', this.onLeaveGameRoom.bind(this));
            } else {
                this.onLeaveGameListener = rNMessageSenderEmitter.addListener('EVENT_USER_LEAVE', this.onLeaveGameRoom.bind(this));
            }
        } catch (e) {

        }
        if (!this.props.isTourist) {
            //已完成任务
            this.taskInfoTimer = setTimeout(() => {
                this.props.taskInfo();
            }, 1500);
            //非游客同步礼物数据
            if (this.props.userInfo) {
                let gifts = this.props.userInfo.gift;
                if (gifts && gifts.length > 0) {
                    this.props.updateGiftManifest(gifts);
                }
            }
        }
    }

    onShowLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.show();
        }
    }

    onHideLoginDialog() {
        if (needLoginDialog != null) {
            needLoginDialog.hide();
        }
    }

    /**
     * 展示加载进度提示
     */
    onShowProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    /**
     * 隐藏加载进度提示
     */
    onHideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
        }
    }

    checkToken() {
        if (this.props.code == 1004) {
            //token 失效
            try {
                this.onShowLoginDialog();
            } catch (error) {
                console.log('token error show:' + error);
            }
        }
    }

    /**
     * 商城
     */
    onOpenStore() {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().openStore_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                //如果flag判断不是游客，但是确实用的游客的ID或是token，直接提示信息失效去登录
                if (this._checkTourist()) {
                    this.onShowLoginDialog();
                } else {
                    rnRoNativeUtils.onOpenStore();
                }
            }
        });
    }

    /**
     * 充值
     */
    openStoreRecharge() {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().openStore_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                //如果flag判断不是游客，但是确实用的游客的ID或是token，直接提示信息失效去登录
                if (this._checkTourist()) {
                    this.onShowLoginDialog();
                } else {
                    rnRoNativeUtils.openStoreRecharge();
                }
            }
        });
    }

    componentWillUnmount() {
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, this.handleMethod);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
        this.onLeaveGameListener.remove();
        this.checkTokenTimer && clearTimeout(this.checkTokenTimer);
        this.leaveGameTimer && clearTimeout(this.leaveGameTimer);
        this.taskInfoTimer && clearTimeout(this.taskInfoTimer);
    }

    /**
     * 游戏离开房间通知更新
     */
    onLeaveGameRoom() {
        try {
            this.leaveGameTimer = setTimeout(() => {
                this.setState({
                    updateGame: true
                });
            }, 1500);
        } catch (e) {

        }
    }

    _keyboardDidShow() {

    }

    _keyboardDidHide() {
    }

    handleMethod(isConnected) {
        console.log('test', (isConnected ? 'online' : 'offline'));
    }

    onOpenUserInfo() {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().content_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                this.props.openPage(SCENES.SCENE_CONTACT_DETAIL, 'global');
            }
        });

    }

    onSearchRoom() {
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().search_prompt_tourist_login});
            }
            this.state.touristCanEnterOtherRoom = false;
        } else {
            this.setState({
                dialogIndex: 0,
                search_dialog_activity: 0,
                dialogTitle: Language().search_room,
                dialogConfirm: Language().enter,
                dialogCancel: Language().cancel,
                hideCancel: false,
                hideConfirm: false,
                password: ''
            });
            this.onOpenModal();
            //统计
            rnRoNativeUtils.onStatistics(Constant.SEARCH_ROOM);
        }
    }

    /**
     * 搜索房间的具体操作
     */
    doSearchRoom() {
        //网络检测
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                try {
                    if (this.state.textRoomID == '') {
                        Toast.show(Language().room_number_empty);
                    } else {
                        this.setState({
                            showModelProgress: true
                        });
                        let data = {
                            room_id: this.state.textRoomID
                        }
                        this.state.roomId = this.state.textRoomID;
                        let url = apiDefines.SEARCH_ROOM_INFO + this.state.roomId + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id;
                        if (this.props.hostLocation) {
                            url = url + apiDefines.PARAMETER_LOCATION + this.props.hostLocation
                        }
                        Util.get(url, (code, message, data) => {
                            this.setState({
                                showModelProgress: false
                            });
                            if (code == 1000) {
                                if (data.level == GAME_TYPE_PRE_SIMPLE || data.level == GAME_TYPE_PRE_SIMPLE_NEW) {
                                    //15級別以上不能進入新手房間
                                    if (this.getUserLevel() > 15 && !this.checkIsRole()) {
                                        Toast.show(Language().prompt_novice_model);
                                        return;
                                    }
                                }
                                this.state.game_type = data.level;
                                this.state.roomId = data.room_id;
                                if (data.password_needed) {
                                    this.setState({
                                        needPassword: data.password_needed
                                    });
                                } else {
                                    //直接进入
                                    this.onShowProgressDialog();
                                    this.enterRoom(this.state.roomId);
                                    this.onCloseModal();
                                }

                            }
                            else if (code == 1004) {
                                //token 失效
                                this.onShowLoginDialog();
                            } else {
                                Toast.show(message);
                            }
                        }, (failed) => {
                            this.onHideProgressDialog();
                            Toast.show(Language().not_network)
                        });
                    }
                } catch (e) {
                    console.log('do search room error:' + e);
                }

            } else {
                //无网络提示
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     *
     * @param text
     */
    onChangePassword(text) {
        if (/^[\d]+$/.test(text)) {
            this.setState({
                password: text
            });
        } else {
            if (/^[\d{1}]$/.test(this.state.password)) {
                this.setState({
                    password: ''
                });
            }
        }
    }

    changeRoomId(text) {
        let format = /^[0-9]*$/;
        if (format.test(text)) {
            this.setState({
                textRoomID: text
            });
        }
    }

    /**
     * 开启宝箱
     */
    onOpenBox() {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.isTourist) {
                if (this.refs.touristModal) {
                    this.refs.touristModal.show({message: Language().openBox_prompt_tourist_login});
                }
                this.state.touristCanEnterOtherRoom = false;
            } else {
                //如果flag判断不是游客，但是确实用的游客的ID或是token，直接提示信息失效去登录
                if (this._checkTourist()) {
                    this.onShowLoginDialog();
                } else {
                    rnRoNativeUtils.openBox();
                }
            }
        });
    }

    /**
     * 战绩详情
     */
    openRecordDetails() {
        // 启动战绩
        // action "standings"
        // type "RN_Native"
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().record_detail_prompt_tourist_login});
            }
            this.state.touristCanEnterOtherRoom = false;
        } else {
            let NativeData = {
                action: Constant.STANDINGS,
                type: Constant.RN_NATIVE
            }
            rnRoNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
        }
    }

    /**
     * 启动家族
     */
    openGroup() {
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().group_prompt_tourist_login});
            }
            this.state.touristCanEnterOtherRoom = false;
        } else {
            let groupInfo = this.props.userInfo.group;
            if (groupInfo && groupInfo.group_id) {
                let nativeData = {
                    action: Constant.ACTION_SHOW_FAMILY_INFO,
                    params: {group_id: groupInfo.group_id},
                    type: Constant.RN_NATIVE
                }
                rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
            } else {
                try {
                    this.props.openPage(SCENES.SCENE_GROUP_LIST, 'global');
                } catch (e) {
                    console.log(e)
                }
            }
        }
    }

    /**
     * 判断是否是
     * @returns {boolean}
     * @private
     */
    _checkTourist() {
        if (this.props.userInfo) {
            let token = this.props.userInfo.token.access_token;
            let userId = this.props.userInfo.id;
            if (token == apiDefines.DEFAULT_ACCESS_TOKEN || userId == apiDefines.DEFAULT_USER_ID) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    onCreateRoom() {
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().prompt_create_room});
            }
            this.state.touristCanEnterOtherRoom = false;
        } else {
            //请求用户详情，创建房间需要一百金币
            if (this.props.userInfo) {
                this.props.getUserInfo(this.props.userInfo.id);
            }
            this.doCreateRoom();
            //统计
            rnRoNativeUtils.onStatistics(Constant.CREATE_ROOM);
        }
    }

    /**
     * 创建房间的具体操作
     */
    doCreateRoom() {
        let createRoomConfig;
        let costConfig;
        let configData = this.props.configData;
        if (configData) {
            createRoomConfig = configData.create_room;
            if (createRoomConfig && createRoomConfig.cost) {
                costConfig = createRoomConfig.cost;
            }
        }
        if (costConfig && costConfig.count) {
            //创建房间需要金币
            this.state.create_room_count = costConfig.count;
        }
        if (this.state.create_room_count == 0) {
            this.props.updateConfig();
        }
        //网络检测
        //创建房间的时候默认为简单6人局
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                this.setState({
                    dialogIndex: 1,
                    dialogTitle: Language().create_room,
                    dialogConfirm: Language().confirm,
                    dialogCancel: Language().cancel,
                    hideCancel: false,
                    hideConfirm: false,
                    game_type: GAME_TYPE_SIMPLE_6
                });
                this.onOpenModal();
            } else {
                //无网络提示
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    onCloseModal() {
        this.setState({
            dialogShow: false,
            switchIsOn: false,
            password: '',
            roomId: '',
            simple_model_radio: true,
            standard_model_radio: false,
            game_type: GAME_TYPE_SIMPLE_6,
            needPassword: false,
            textRoomID: '',
            showModelProgress: false,
            activityGameMode: GAME_MODEL_SIMPLE,
            game_limit_level: 0,
            game_limit_level_name: Language().no_limit,
            select_game_play: Language().game_simple_6,
            dialogIndex: 0,
            search_dialog_activity: 0
        });

    }

    onSubmitEditing() {
        this.onConfirmModal();
    }

    onConfirmModal() {
        try {
            if (this.state.dialogIndex == 0 || this.state.dialogIndex == 2) {
                //搜索房间后进入
                if (this.state.search_dialog_activity == 0) {
                    if (this.state.needPassword) {
                        let password = this.state.password;
                        password = password.trim();
                        if (!password) {
                            Toast.show(Language().placeholder_password);
                        } else {
                            this.onShowProgressDialog();
                            this.enterRoom(this.state.roomId);
                            this.onCloseModal();
                        }
                    } else {
                        this.doSearchRoom();
                    }
                } else {
                    this.requestSearch();
                }
            } else if (this.state.dialogIndex == 1) {
                //创建房间进入
                if (this.checkMoney()) {
                    let roomType = this.state.game_type;
                    if (this.state.switchIsOn && this.state.password == '') {
                        Toast.show(Language().room_password_empty);
                    } else {
                        let password = this.state.password;
                        this.onCloseModal();
                        let url = apiDefines.CREATE_ROOM + roomType;
                        if (this.props.hostLocation) {
                            url = url + apiDefines.PARAMETER_LOCATION + this.props.hostLocation
                        }
                        if (password != '' && password != null) {
                            url = url + apiDefines.ROOM_PASSWORD + this.state.password;
                        }
                        if (this.state.game_limit_level) {
                            url = url + apiDefines.PARAMETER_LIMIT_LEVEL + this.state.game_limit_level;
                        }
                        this.onShowProgressDialog();
                        Util.get(url, (code, message, data) => {
                            if (code == 1000) {
                                if (data != null) {
                                    this.state.roomId = data.room_id;
                                    this.state.password = data.password;
                                    this.enterCreateRoom(data.room_id, data.password, roomType);
                                }
                            } else if (code == 1004) {
                                this.onHideProgressDialog();
                                //token 失效
                                this.onShowLoginDialog();
                            } else {
                                Toast.show(message);
                                //请求失败，dismiss进度对话框
                                this.onHideProgressDialog();
                            }
                        }, (failed) => {
                            this.onHideProgressDialog();
                            Toast.show(Language().not_network);
                        });
                    }
                } else {
                    Toast.show(Language().not_enough_gold);
                }

            }
        } catch (e) {
            console.log('onConfirmModal error:' + e);
        }
    }

    /**
     * 创建房间检测金币是否足够
     * @returns {boolean}
     */
    checkMoney() {
        if (this.state.create_room_count == 0) {
            return true;
        }
        let userInfo = this.props.userInfo;
        if (userInfo && userInfo.money) {
            if (userInfo.money.gold >= 100) {
                return true;
            } else if (userInfo.money.dim) {
                return (userInfo.money.gold + userInfo.money.dim * 66) >= 100;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    //进入房间
    enterCreateRoom(roomId, password, gameType) {
        if (Platform.OS === 'ios') {
            //IOS 进入游戏房间
            this.onHideProgressDialog();
            var main = NativeModules.MainViewController;
            let userImage = this.props.userInfo.image;
            if (userImage == null) {
                userImage = '';
            }
            main.enter(password, gameType, roomId, this.props.userInfo.name, this.props.userInfo.id, userImage, this.props.token);
            this.state.password = '';
        } else {
            try {
                let experience = 0;
                let sex = 2;
                if (this.props.userInfo != null) {
                    if (this.props.userInfo.game != null && this.props.userInfo.game.experience != null) {
                        experience = this.props.userInfo.game.experience;
                    }
                    if (this.props.userInfo.sex == 1 || this.props.userInfo.sex == 2) {
                        sex = this.props.userInfo.sex;
                    }
                }
                NativeModules.NativeJSModule.rnStartGame(password, gameType, roomId, this.props.userInfo.name, this.props.userInfo.id, sex, this.props.userInfo.image, experience, this.props.token, successinfo => {
                    this.onHideProgressDialog();
                }, failed => {
                    this.onHideProgressDialog();
                    Toast.show(failed);
                });
                this.state.password = '';
            } catch (error) {
                this.onHideProgressDialog();
                Toast.show(Language().enter_room_error);
                this.state.password = '';
            }
        }
    }

    onOpenModal() {
        this.setState({dialogShow: true});
    }

    onOpenGameHelp() {
        this.setState({
            gameHelpDialogShow: true,
            gameHelpMenu: true,
            gameHelpTitle: Language().game_help,
            gameHelpType: ''
        });
    }

    selectGameModelHelp(helpTitle, gameType) {
        this.setState({
            gameHelpDialogShow: false,
            gameHelpTitle: helpTitle,
            gameHelpMenu: false,
            gameHelpType: gameType
        });
        this.setState({gameHelpDialogShow: true,});
    }

    closeGameHelpDialog() {
        if (this.state.gameHelpMenu) {
            this.setState({
                gameHelpDialogShow: false,
                gameHelpTitle: '',
                gameHelpMenu: true,
                gameHelpType: ''
            });
        } else {
            this.onOpenGameHelp();
        }

    }

    simpleModelHelp() {
        this.onCloseModal();
        this.state.gameHelpType = GAME_TYPES.SIMPLE_MODEL_ROLE;
    }

    standardModelHelp() {
        this.onCloseModal();
        this.state.gameHelpType = GAME_TYPES.STANDARD_MODEL_ROLE;
    }

    dictionaryHelp() {
        this.onCloseModal();
        this.state.gameHelpType = GAME_TYPES.DICTIONARY;
    }

    /**
     * 进入简单模式
     */
    enterSimpleModel() {
        if (this.props.userInfo != null && this.props.userInfo.game != null && this.props.userInfo.game.level != null) {
            if (this.props.userInfo.game.level > 2) {
                this.showGameModelManager(GAME_MODEL_SIMPLE);
            } else {
                this.toastStandardModel('LV.3' + Language().open);
            }
        } else {
            this.toastStandardModel('LV.3' + Language().open);
        }
    }

    /**
     * 统一游戏入口
     * @param gameType
     */
    doEnterGameModel(gameType) {
        if (gameType == GAME_TYPE_SIMPLE_10 || gameType == GAME_TYPE_SIMPLE_9) {
            if (this.getUserLevel() > 7) {
                this.state.game_type = gameType;
                this.state.roomId = '';
                this.state.password = '';
                this.onShowProgressDialog();
                this.enterRoom(this.state.roomId);
            }
        } else {
            this.state.game_type = gameType;
            this.state.roomId = '';
            this.state.password = '';
            this.onShowProgressDialog();
            this.enterRoom(this.state.roomId);
        }
    }

    //进入房间
    enterRoom(roomId) {
        if (Platform.OS === 'ios') {
            //IOS 进入游戏房间
            this.onHideProgressDialog();
            var main = NativeModules.MainViewController;
            let userImage = this.props.userInfo.image;
            if (userImage == null) {
                userImage = '';
            }
            if (this.state.showGameModelManager) {
                this.closeGameModelManager();
            }
            main.enter(this.state.password, this.state.game_type, roomId, this.props.userInfo.name, this.props.userInfo.id, userImage, this.props.token);
        } else {
            try {
                let experience = 0;
                let sex = 2;
                if (this.props.userInfo != null) {
                    if (this.props.userInfo.game != null && this.props.userInfo.game.experience != null) {
                        experience = this.props.userInfo.game.experience;
                    }
                    if (this.props.userInfo.sex == 1 || this.props.userInfo.sex == 2) {
                        sex = this.props.userInfo.sex;
                    }
                }
                NativeModules.NativeJSModule.rnStartGame(this.state.password, this.state.game_type, roomId, this.props.userInfo.name, this.props.userInfo.id, sex, this.props.userInfo.image, experience, this.props.token, successinfo => {
                    this.onHideProgressDialog();
                    if (this.state.showGameModelManager) {
                        this.closeGameModelManager();
                    }
                }, failed => {
                    this.onHideProgressDialog();
                    Toast.show(failed);
                });
            } catch (error) {
                this.onHideProgressDialog();
                Toast.show(Language().enter_room_error);
            }
        }
    }

    /**
     * 进入标准模式
     */
    enterStandardModel() {
        if (this.props.userInfo != null && this.props.userInfo.game != null && this.props.userInfo.game.level != null) {
            if (this.props.userInfo.game.level > 11) {
                this.showGameModelManager(GAME_MODEL_STANDARD);
            } else {
                this.toastStandardModel('LV.12' + Language().open);
            }
        } else {
            this.toastStandardModel('LV.12' + Language().open);
        }
        //统计
        rnRoNativeUtils.onStatistics(Constant.STANDARD_MODEL);
    }

    toastStandardModel(message) {
        Toast.show(message);
    }

    toggleSimple() {
        this.setState({
            simple_model_radio: true,
            standard_model_radio: false,
        });
    }

    toggleStandard() {
        this.setState({
            simple_model_radio: false,
            standard_model_radio: true,
        });
    }

    /**
     * 检查是否是管理员
     * @returns {boolean}
     */
    checkIsRole() {
        let type = 0;
        if (this.props.userInfo) {
            if (this.props.userInfo.role) {
                type = this.props.userInfo.role.type
            }
            if ((type & roleType.manager) != 0 || (type & roleType.teacher) != 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
     * 獲取用户等级
     * @returns {number}
     */
    getUserLevel() {
        let lever = 0;
        if (this.props.userInfo != null && this.props.userInfo.game != null && this.props.userInfo.game.level != null) {
            lever = this.props.userInfo.game.level;
        }
        return lever;
    }

    /**
     * 新手专区
     */
    enterNoviceModel() {
        if (this.props.userInfo) {
            if (this.getUserLevel() > 15) {
                if (this.checkIsRole()) {
                    this.showGameModelManager(GAME_MODEL_NOVICE);
                } else {
                    Toast.show(Language().prompt_primary_model);
                }
            } else {
                this.showGameModelManager(GAME_MODEL_NOVICE);
            }
        } else {
            Toast.show(netWorkTool.NOT_NETWORK);
        }
    }

    /**
     * 个性房间
     */
    enterPersonalityModel() {
        rnRoNativeUtils.openCallUp();
    }

    openStrategyWeb() {
        this.closeGameHelpDialog();
        try {
            this.props.openPage(SCENES.SCENE_WEBVIEW, 'global');
        } catch (e) {
            console.log(e)
        }

    }

    showAnnouncementModel() {
        this.setState({
            showAnnouncementModel: true
        });
    }

    closeAnnouncementModel() {
        this.setState({
            showAnnouncementModel: false
        });
    }

    /**
     * 简单模式组合
     */
    _simpleModelGroupView() {
        let simple9Source = InternationSourceManager().simple9LockSource;
        let simple10Source = InternationSourceManager().simple10LockSource;
        if (this.getUserLevel() > 7) {
            simple9Source = InternationSourceManager().simple9Source;
            simple10Source = InternationSourceManager().simple10Source;
        }
        return ( <View>
            <View style={{height:50}}></View>
            <TouchableOpacity onPress={()=>{this.doEnterGameModel(GAME_TYPE_SIMPLE_6)}}
                              activeOpacity={0.8}
                              style={{flex:1,backgroundColor:Color.transparent,marginRight:8,marginLeft:8,marginTop:5}}>
                <CachedImage source={InternationSourceManager().simple6Source}
                             style={styles.model_image_style}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.doEnterGameModel(GAME_TYPE_SIMPLE_9)}}
                              activeOpacity={0.8}
                              style={{flex:1,backgroundColor:Color.transparent,marginRight:8,marginLeft:8,marginTop:5}}>
                <CachedImage source={simple9Source}
                             style={styles.model_image_style}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.doEnterGameModel(GAME_TYPE_SIMPLE_10)}}
                              activeOpacity={0.8}
                              style={{flex:1,backgroundColor:Color.transparent,marginRight:8,marginLeft:8,marginTop:5}}>
                <CachedImage source={simple10Source}
                             style={styles.model_image_style}/>
            </TouchableOpacity>
        </View>);

    }

    /**
     * 标准模式组
     */
    _standardModelGroupView() {
        let highKingSource = InternationSourceManager().highKingSource;
        return (<View>
            <View style={{height:50,justifyContent:'flex-end'}}>
                <Text
                    style={{color:Color.colorWhite,fontSize: 20,fontWeight: 'bold',alignSelf:'center'}}>{Language().game_model_standard}</Text>
            </View>
            <TouchableOpacity onPress={()=>{this.doEnterGameModel(GAME_TYPE_NORMAL_GUARD)}}
                              activeOpacity={0.8}
                              style={{flex:1,backgroundColor:Color.transparent,marginRight:8,marginLeft:8,marginTop:0}}>
                <CachedImage source={InternationSourceManager().normalGuardSource}
                             style={styles.model_image_style}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.doEnterGameModel(GAME_TYPE_NORMAL)}}
                              activeOpacity={0.8}
                              style={{flex:1,backgroundColor:Color.transparent,marginRight:8,marginLeft:8,marginTop:5}}>
                <CachedImage source={InternationSourceManager().normalCupidSource}
                             style={styles.model_image_style}/>
            </TouchableOpacity>
            <View style={{height:40,justifyContent:'flex-end'}}>
                <Text
                    style={{color:Color.colorWhite,fontSize: 20,fontWeight: 'bold',alignSelf:'center'}}>{Language().game_model_advanced}</Text>

            </View>
            <TouchableOpacity onPress={()=>{this.doEnterGameModel(GAME_TYPE_HIGH_KING)}}
                              activeOpacity={0.8}
                              style={{flex:1,backgroundColor:Color.transparent,marginRight:8,marginLeft:8,marginTop:5}}>
                <CachedImage source={highKingSource}
                             style={styles.model_image_style}/>
            </TouchableOpacity>
        </View>);
    }

    /**
     * 新手模式组
     */
    _noviceModelGroupView() {
        return (<View>
            <View style={{height:50}}></View>
            <TouchableOpacity onPress={()=>{this.doEnterGameModel(GAME_TYPE_PRE_SIMPLE_NEW)}}
                              activeOpacity={0.8}
                              style={{flex:1,backgroundColor:Color.transparent,marginRight:8,marginLeft:8,marginTop:5}}>
                <CachedImage
                    source={InternationSourceManager().preSimpleNewSource}
                    style={styles.model_image_style}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.doEnterGameModel(GAME_TYPE_PRE_SIMPLE)}}
                              activeOpacity={0.8}
                              style={{flex:1,backgroundColor:Color.transparent,marginRight:8,marginLeft:8,marginTop:5}}>
                <CachedImage source={InternationSourceManager().preSimpleSource}
                             style={styles.model_image_style}/>
            </TouchableOpacity>
        </View>);
    }

    doHighKingMore(type) {
    }

    /**
     * 游戏模式管理
     * @private
     */
    _gameModelManager() {
        switch (this.state.activityGameMode) {
            case GAME_MODEL_SIMPLE:
                return this._simpleModelGroupView();
            case GAME_MODEL_STANDARD:
            case GAME_MODEL_ADVANCED:
                return this._standardModelGroupView();
            case GAME_MODEL_NOVICE:
                return this._noviceModelGroupView();
            default:
                return this._simpleModelGroupView();
        }
    }

    /**
     *
     * @returns {*}
     * @private
     */
    _getActivityGameModel() {
        switch (this.state.activityGameMode) {
            case GAME_MODEL_SIMPLE:
                return Language().game_model_simple;
            case GAME_MODEL_STANDARD:
            case GAME_MODEL_ADVANCED:
                return Language().game_model_advanced_more;
            case GAME_MODEL_NOVICE:
                return Language().game_model_novice;
            default:
                return Language().game_model_simple;
        }
    }
    _getSelectGameModelName() {
        switch (this.state.activityGameMode) {
            case GAME_MODEL_SIMPLE:
                return Language().game_model_simple;
            case GAME_MODEL_STANDARD:
                return Language().game_model_standard;
            case GAME_MODEL_ADVANCED:
                return Language().game_model_advanced;
            default:
                return Language().game_model_simple;
        }
    }
    /**
     *
     * @private
     */
    _getActivityGameModelBg() {
        let gameModelSource = {
            modelSource: ImageManager.bgGameModelNew,
            modelTextColor: '#ffff72'
        }
        switch (this.state.activityGameMode) {
            case GAME_MODEL_SIMPLE:
                gameModelSource.modelSource = ImageManager.bgGameModelSp;
                gameModelSource.modelTextColor = '#b2ffff';
                break;
            case GAME_MODEL_STANDARD:
            case GAME_MODEL_ADVANCED:
                gameModelSource.modelSource = ImageManager.bgGameModelAd;
                gameModelSource.modelTextColor = '#ffb0ca';
                break;
            case GAME_MODEL_NOVICE:
                gameModelSource.modelSource = ImageManager.bgGameModelNew;
                gameModelSource.modelTextColor = '#ffff72';
                break;
        }
        return gameModelSource;
    }

    _getSelectGameModelName() {
        switch (this.state.activityGameMode) {
            case GAME_MODEL_SIMPLE:
                return Language().game_model_simple;
            case GAME_MODEL_STANDARD:
                return Language().game_model_standard;
            case GAME_MODEL_ADVANCED:
                return Language().game_model_advanced;
            default:
                return Language().game_model_simple;
        }
    }

    /**
     * 展示对应的游戏模式
     * @param gameMode
     */
    showGameModelManager(gameMode) {
        this.setState({
            showGameModelManager: true,
            activityGameMode: gameMode
        });
    }

    /**
     * 关闭游戏模式
     */
    closeGameModelManager() {
        this.setState({
            showGameModelManager: false,
            activityGameMode: GAME_MODEL_SIMPLE,
        });
    }

    /**
     * 搜素弹框view
     */
    searchDialogView() {
        let activity = this.state.search_dialog_activity;
        let placeholderText = Language().placeholder_room_number;
        if (activity == 1) {
            placeholderText = Language().placeholder_contact_number;
        } else if (activity == 2) {
            placeholderText = Language().placeholder_group_id;
        }
        let passwordContent = (
            <Input placeholder={Language().placeholder_room_password}
                   style={{borderColor:(Color.createRoomDialogTextColor),borderWidth: StyleSheet.hairlineWidth, borderRadius: 3,height:35,width:ScreenWidth/3*2-20,alignSelf:'center',fontSize:14,paddingTop:5,paddingBottom:5,paddingLeft:10,paddingRight:10,marginTop:10}}
                   onChangeText={(text)=>this.onChangePassword(text)}
                   placeholderTextColor='#AFB0B1'
                   autoFocus={true} maxLength={4}
                   keyboardType='numeric'
                   value={this.state.password}/>);
        return ( <View>
            <View flexDirection='row' style={{justifyContent:'center'}}>
                <TouchableOpacity activeOpacity={0.6} onPress={()=>this.activitySearchIndex(0)}
                                  style={{backgroundColor:Color.transparent,justifyContent:'center',flex:1,height:37}}>
                    <View flexDirection='row'
                          style={{justifyContent:'center',padding:5,flex:1,backgroundColor:activity==0?Color.action_room_color:Color.colorWhite,borderTopLeftRadius:3}}>
                        <Image source={activity==0?ImageManager.searchRoomIconActivity:ImageManager.searchRoomIcon}
                               style={[styles.actionRoomIcon,{width:20,height:20}]}/>
                        <Text
                            style={{color:activity==0?Color.colorWhite:'#3A0042',alignSelf:'center',fontSize:17,fontWeight: '500',textAlign:'center',padding:5}}>{Language().recreation_text_room}</Text>
                    </View>
                </TouchableOpacity>
                <View style={{width:1.5,backgroundColor:Color.action_room_color}}/>
                <TouchableOpacity activeOpacity={0.6} onPress={()=>this.activitySearchIndex(1)}
                                  style={{backgroundColor:Color.transparent,justifyContent:'center',flex:1,height:37}}>
                    <View flexDirection='row'
                          style={{justifyContent:'center',padding:5,flex:1,backgroundColor:activity==1?Color.action_room_color:Color.colorWhite}}>
                        <Image source={activity==1?ImageManager.searchUserIconActivity:ImageManager.searchUserIcon}
                               style={[styles.actionRoomIcon,{width:20,height:20}]}/>
                        <Text
                            style={{color:activity==1?Color.colorWhite:'#3A0042',alignSelf:'center',fontSize:17,fontWeight: '500',textAlign:'center',padding:5}}>{Language().contact}</Text>
                    </View>
                </TouchableOpacity>
                <View style={{width:1.5,backgroundColor:Color.action_room_color}}/>
                <TouchableOpacity activeOpacity={0.6} onPress={()=>this.activitySearchIndex(2)}
                                  style={{backgroundColor:Color.transparent,justifyContent:'center',flex:1,height:37}}>
                    <View flexDirection='row'
                          style={{justifyContent:'center',padding:5,flex:1,backgroundColor:activity==2?Color.action_room_color:Color.colorWhite,borderTopRightRadius:3}}>
                        <Image source={activity==2?ImageManager.searchGroupIconActivity:ImageManager.searchGroupIcon}
                               style={[styles.actionRoomIcon,{width:20,height:20}]}/>
                        <Text
                            style={{color:activity==2?Color.colorWhite:'#3A0042',alignSelf:'center',fontSize:17,fontWeight: '500',textAlign:'center',padding:5}}>{Language().family}</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{backgroundColor:Color.action_room_color,height:1.5}}/>
            <View
                style={{marginLeft:3,marginRight:3,backgroundColor:(Color.transparent),paddingBottom:30,paddingTop:30,justifyContent:'center'}}>
                <Input placeholder={placeholderText}
                       onChangeText={(text)=>this.changeRoomId(text)}
                       placeholderTextColor='#AFB0B1'
                       style={{borderColor:(Color.createRoomDialogTextColor),borderWidth: StyleSheet.hairlineWidth, borderRadius: 3,height:35,width:ScreenWidth/3*2-20,alignSelf:'center',fontSize:14,paddingTop:5,paddingBottom:5,paddingLeft:10,paddingRight:10}}
                       keyboardType='numeric'
                       value={this.state.textRoomID}/>
                {this.state.needPassword ? passwordContent : null}
            </View>
        </View>);
    }

    /**
     *
     * @param index
     */
    activitySearchIndex(index) {
        let dialogConfirmText = Language().confirm;
        if (index == 0) {
            dialogConfirmText = Language().enter;
        }
        that.setState({
            search_dialog_activity: index,
            dialogConfirm: dialogConfirmText
        });
    }

    /**
     * 在线好友进入游戏需要密码
     */
    needPasswordDialogView() {
        return ( <View>
            <View flexDirection='row' style={{justifyContent:'center',padding:5}}>
                <Text
                    style={{color:'#3A0042',alignSelf:'center',fontSize:17,fontWeight: 'bold',textAlign:'center',padding:5}}>{Language().room_number + this.state.textRoomID}</Text>
            </View>
            <View style={{backgroundColor:Color.action_room_color,height:1.5}}/>
            <View
                style={{marginLeft:3,marginRight:3,backgroundColor:(Color.transparent),paddingBottom:10,paddingTop:5,justifyContent:'center'}}>
                <Input placeholder={Language().placeholder_room_password}
                       style={{borderColor:(Color.createRoomDialogTextColor),borderWidth: StyleSheet.hairlineWidth, borderRadius: 3,height:35,width:ScreenWidth/3*2-20,alignSelf:'center',fontSize:14,paddingTop:5,paddingBottom:5,paddingLeft:10,paddingRight:10,marginTop:10}}
                       onChangeText={(text)=>this.onChangePassword(text)}
                       placeholderTextColor='#AFB0B1'
                       autoFocus={true} maxLength={4}
                       keyboardType='numeric'
                       value={this.state.password}/>
            </View>
        </View>);
    }

    /**
     * 创建房间新view
     */
    newCreateGameView() {
        let setPassword = (<View
            style={{margin:3,borderBottomWidth:StyleSheet.hairlineWidth,borderBottomColor:(Color.createRoomDialogTextColor),marginLeft:30,marginRight:35,marginBottom:8}}>
            <Input
                style={{paddingLeft:0,color:(Color.createRoomDialogTextColor),fontSize:14,height:30,padding:5,paddingLeft:0,}}
                placeholder={Language().placeholder_set_password}
                placeholderTextColor={Color.createRoomDialogTextColor}
                autoFocus={true} maxLength={4}
                onChangeText={(text)=>this.onChangePassword(text)}
                onSubmitEditing={this.onSubmitEditing.bind(this)}
                keyboardType='numeric'
                value={this.state.password}/>
        </View>);
        let createRoomCountView = null;
        if (this.state.create_room_count > 0) {
            createRoomCountView = (<View flexDirection='row' style={{justifyContent:'center',marginTop:5}}>
                <Text
                    style={{fontSize:10,alignSelf:'center',color:Color.setting_text_color}}>{Language().create_room_need_gold}</Text>
                <Image source={ImageManager.createRoomGold}
                       style={{width:25,height:25,resizeMode: 'contain',alignSelf:'center'}}/>
                <Text
                    style={{fontSize:10,alignSelf:'center',color:Color.setting_text_color}}>{this.state.create_room_count + Language().one_hundred_gold}</Text>
            </View>);
        }
        return (<View>
            <View flexDirection='row' style={{justifyContent:'center',padding:5}}>
                <Image source={ImageManager.iconCreate} style={styles.actionRoomIcon}/>
                <Text
                    style={{color:'#3A0042',alignSelf:'center',fontSize:17,fontWeight: 'bold',textAlign:'center',padding:5}}>{this.state.dialogTitle}</Text>
            </View>
            <View style={{backgroundColor:Color.action_room_color,height:1.5}}/>
            <Text
                style={{alignSelf:'center',fontSize:14,color:Color.createRoomDialogTextColor,fontWeight:'bold',padding:5,marginTop:10}}>{Language().set_game_model}</Text>
            <View style={styles.set_game_rule_view}>
                <Text
                    style={{flex:1,alignSelf:'center',fontSize:14,color:Color.createRoomDialogTextColor,textAlign:'center'}}>{this._getSelectGameModelName()}</Text>
                {this._selectGameModel()}
            </View>
            <Text
                style={{alignSelf:'center',fontSize:14,color:Color.createRoomDialogTextColor,fontWeight:'bold',padding:5}}>{Language().set_game_play}</Text>
            <View style={styles.set_game_rule_view}>
                <Text
                    style={{flex:1,alignSelf:'center',fontSize:14,color:Color.createRoomDialogTextColor,textAlign:'center'}}>{this.state.select_game_play}</Text>
                {this._selectGamePlay()}
            </View>
            <Text
                style={{alignSelf:'center',fontSize:14,color:Color.createRoomDialogTextColor,fontWeight:'bold',padding:5}}>{Language().set_level_limit}</Text>
            <View style={styles.set_game_rule_view}>
                <Text
                    style={{flex:1,alignSelf:'center',fontSize:14,color:Color.createRoomDialogTextColor,textAlign:'center'}}>{this.state.game_limit_level_name}</Text>
                {this._selectGameLimit()}
            </View>
            {createRoomCountView}
            <View flexDirection='row'
                  style={{justifyContent:'center',marginTop:5,alignItems:'center',marginLeft:35,marginRight:35,marginBottom:5}}>
                <Text
                    style={{color:(Color.createRoomDialogTextColor),fontSize:14,marginRight:15,flex:1}}>{Language().set_password}</Text>
                <Switch onValueChange={(value)=>this.setState({switchIsOn:value})}
                        value={this.state.switchIsOn}/>
            </View>
            {this.state.switchIsOn ? setPassword : null}
        </View>);

    }

    /**
     *设置游戏模式
     * @returns {*}
     * @private
     */
    _selectGameModel() {
        let selectContent = [{
            name: Language().game_model_simple,
            type: GAME_MODEL_SIMPLE
        }, {name: Language().game_model_standard, type: GAME_MODEL_STANDARD}, {
            name: Language().game_model_advanced,
            type: GAME_MODEL_ADVANCED
        }]
        return (<Dropdown style={[styles.dropDown]}
                          dropdownStyle={[styles.dropdownDropdown,{height:110}]}
                          options={selectContent}
                          renderRow={this._gameModelRenderRow.bind(this)}
                          onSelect={(idx, value) => this._gameModelOnSelect(idx, value)}>
            <View style={styles.dropDownView}>
                <Image style={styles.game_arrow} source={ImageManager.game_arrow_icon}/>
            </View>
        </Dropdown>);
    }

    /**
     *  游戏模式item
     * @param rowData
     * @param rowID
     * @param highlighted
     * @returns {XML}
     * @private
     */
    _gameModelRenderRow(rowData, rowID, highlighted) {
        return (
            <TouchableOpacity activeOpacity={0.6}>
                <View style={[styles.dropdownRow]}>
                    <Text
                        style={{fontSize: 13,flex:1,textAlign:'center',color:Color.createRoomDialogTextColor}}>
                        {rowData.name}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    /**
     * 选中的对应的游戏类型
     * @param idx
     * @param value
     * @private
     */
    _gameModelOnSelect(idx, value) {
        let gamePlay = Language().game_simple_6;
        let gameType = GAME_TYPE_SIMPLE_6;
        if (value.type == GAME_MODEL_STANDARD) {
            gamePlay = Language().normal_guard;
            gameType = GAME_TYPE_NORMAL_GUARD;
        } else if (value.type == GAME_MODEL_ADVANCED) {
            gamePlay = Language().high_king;
            gameType = GAME_TYPE_HIGH_KING;
        }
        this.setState({
            activityGameMode: value.type,
            select_game_play: gamePlay,
            game_type: gameType,
            game_limit_level: 0,
            game_limit_level_name: Language().no_limit
        })
    }

    /**
     *设置游戏玩法
     * @returns {*}
     * @private
     */
    _selectGamePlay() {
        let selectContent = [{
            name: Language().game_simple_6,
            type: GAME_TYPE_SIMPLE_6
        }, {name: Language().game_simple_9, type: GAME_TYPE_SIMPLE_9}, {
            name: Language().game_simple_10,
            type: GAME_TYPE_SIMPLE_10
        }]
        if (this.state.activityGameMode == GAME_MODEL_STANDARD) {
            selectContent = [{
                name: Language().normal_guard,
                type: GAME_TYPE_NORMAL_GUARD
            }, {name: Language().normal_cupid, type: GAME_TYPE_NORMAL}]
        } else if (this.state.activityGameMode == GAME_MODEL_ADVANCED) {
            selectContent = [{
                name: Language().high_king,
                type: GAME_TYPE_HIGH_KING
            }]
        }
        return (<Dropdown style={[styles.dropDown]}
                          dropdownStyle={[styles.dropdownDropdown,{height:selectContent.length*30+15}]}
                          options={selectContent}
                          renderRow={this._gamePlayRenderRow.bind(this)}
                          onSelect={(idx, value) => this._gamePlayOnSelect(idx, value)}>
            <View style={styles.dropDownView}>
                <Image style={styles.game_arrow} source={ImageManager.game_arrow_icon}/>
            </View>
        </Dropdown>);
    }

    /**
     *  游戏玩法item
     * @param rowData
     * @param rowID
     * @param highlighted
     * @returns {XML}
     * @private
     */
    _gamePlayRenderRow(rowData, rowID, highlighted) {
        return (
            <TouchableOpacity activeOpacity={0.6}>
                <View style={[styles.dropdownRow]}>
                    <Text
                        style={{fontSize: 13,flex:1,textAlign:'center',color:Color.createRoomDialogTextColor}}>
                        {rowData.name}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    /**
     * 选中的对应的游戏玩法类型
     * @param idx
     * @param value
     * @private
     */
    _gamePlayOnSelect(idx, value) {
        this.setState({
            game_type: value.type,
            select_game_play: value.name
        })
    }

    /**
     *设置游戏限制
     * @returns {*}
     * @private
     */
    _selectGameLimit() {
        let selectContent = [{
            name: Language().no_limit,
            type: 0
        }, {name: '15' + Language().rank, type: 15}, {name: '30' + Language().rank, type: 30}, {
            name: '50' + Language().rank,
            type: 50
        }];
        if (this.state.activityGameMode == GAME_MODEL_ADVANCED) {
            selectContent = [{
                name: Language().no_limit,
                type: 0
            }, {name: '80' + Language().rank, type: 80}, {name: '120' + Language().rank, type: 120}, {
                name: '200' + Language().rank,
                type: 200
            }];
        } else if (this.state.activityGameMode == GAME_MODEL_STANDARD) {
            selectContent = [{
                name: Language().no_limit,
                type: 0
            }, {name: '30' + Language().rank, type: 30}, {name: '50' + Language().rank, type: 50}, {
                name: '100' + Language().rank,
                type: 100
            }];
        }
        return (<Dropdown style={[styles.dropDown]}
                          dropdownStyle={[styles.dropdownDropdown,{height:selectContent.length*30+15}]}
                          options={selectContent}
                          renderRow={this._gameLimitRenderRow.bind(this)}
                          onSelect={(idx, value) => this._gameLimitOnSelect(idx, value)}>
            <View style={styles.dropDownView}>
                <Image style={styles.game_arrow} source={ImageManager.game_arrow_icon}/>
            </View>
        </Dropdown>);
    }

    /**
     *  游戏玩法item
     * @param rowData
     * @param rowID
     * @param highlighted
     * @returns {XML}
     * @private
     */
    _gameLimitRenderRow(rowData, rowID, highlighted) {
        return (
            <TouchableOpacity activeOpacity={0.6}>
                <View style={[styles.dropdownRow]}>
                    <Text
                        style={{fontSize: 13,flex:1,textAlign:'center',color:Color.createRoomDialogTextColor}}>
                        {rowData.name}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    /**
     * 选中的对应的游戏玩法类型
     * @param idx
     * @param value
     * @private
     */
    _gameLimitOnSelect(idx, value) {
        this.setState({
            game_limit_level: value.type,
            game_limit_level_name: value.name
        })
    }

    /**
     * 游戏帮助管理器
     */
    gameHelpContentManager() {
        switch (this.state.gameHelpType) {
            case 'simple':
                return (<GameSimpleModel/>);
            case 'standard':
                return (<GameStAdModel/>);
            case 'dictionary':
                //专业术语
                return (<GameProfessionalLlg/>);
            case 'role':
                //角色
                return (<GameRole/>);
            case 'rule':
                //游戏规则
                return (<GameRule/>);
            default:
                return this.gameHelpMenu();
        }
    }

    /**
     *创建房间／搜索房间／好友在玩匹配进入弹框
     */
    dialogContentManager() {
        switch (this.state.dialogIndex) {
            case 0:
                return this.searchDialogView();
            case 1:
                return this.newCreateGameView();
            case 2:
                return this.needPasswordDialogView();
        }
    }

    /**
     *游戏帮助首页
     */
    gameHelpMenu() {
        return (<View style={{flex:1,justifyContent:'center',marginTop:10,marginBottom:10}}>
            <View style={{alignSelf:'center',justifyContent:'center',flex:1}}>
                <TouchableOpacity activeOpacity={0.6} onPress={this.openStrategyWeb.bind(this)}
                                  style={{justifyContent:'center',marginTop:0,width:ScreenWidth/3*2,alignSelf:'center',flex:1,backgroundColor:'#6711B7',borderRadius:3,}}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={videoHelp}/>
                        <Text
                            style={{flex:2,alignSelf:'center',color:'#e1fbfd',fontSize:17,textAlign:'right',marginRight:20}}>{Language().game_video}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6}
                                  onPress={()=>{this.selectGameModelHelp(Language().game_rule,'rule')}}
                                  style={{justifyContent:'center',marginTop:10,width:ScreenWidth/3*2,alignSelf:'center',flex:1,backgroundColor:'#6711B7',borderRadius:3}}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={gameRule}/>
                        <Text
                            style={{flex:2,alignSelf:'center',color:'#e1fbfd',fontSize:17,textAlign:'right',marginRight:20}}>{Language().game_rule}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6}
                                  onPress={()=>{this.selectGameModelHelp(Language().role_detail,'role')}}
                                  style={{justifyContent:'center',marginTop:10,width:ScreenWidth/3*2,alignSelf:'center',flex:1,backgroundColor:'#6711B7',borderRadius:3}}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={roleIntroduction}/>
                        <Text
                            style={{flex:2,alignSelf:'center',color:'#e1fbfd',fontSize:17,textAlign:'right',marginRight:20}}>{Language().role_detail}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6}
                                  onPress={()=>{this.selectGameModelHelp(Language().sim_nov_model,'simple')}}
                                  style={{justifyContent:'center',marginTop:10,width:ScreenWidth/3*2,alignSelf:'center',flex:1,backgroundColor:'#6711B7',borderRadius:3}}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={simpleNovModel}/>
                        <Text
                            style={{flex:2,alignSelf:'center',color:'#e1fbfd',fontSize:17,textAlign:'right',marginRight:20}}>{Language().sim_nov_detail}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6}
                                  onPress={()=>{this.selectGameModelHelp(Language().st_ad_model,'standard')}}
                                  style={{justifyContent:'center',marginTop:10,width:ScreenWidth/3*2,alignSelf:'center',flex:1,backgroundColor:'#6711B7',borderRadius:3}}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={stAdModel}/>
                        <Text
                            style={{flex:2,alignSelf:'center',color:'#e1fbfd',fontSize:17,textAlign:'right',marginRight:20}}>{Language().st_ad_detail}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6}
                                  onPress={()=>{this.selectGameModelHelp(Language().professional_Lg,'dictionary')}}
                                  style={{justifyContent:'center',marginTop:10,width:ScreenWidth/3*2,alignSelf:'center',flex:1,backgroundColor:'#6711B7',borderRadius:3}}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.gameHelpMenu} source={professionalLg}/>
                        <Text
                            style={{flex:2,alignSelf:'center',color:'#e1fbfd',fontSize:17,textAlign:'right',marginRight:20}}>{Language().professional_Lg}</Text>
                    </View>
                </TouchableOpacity>
            </View>

        </View>);
    }

    /**
     * 好友在玩view
     * @returns {XML}
     */
    friendStatusView() {
        return friendStatusList.length == 0 ? (
                <Text
                    style={{padding:10,fontSize:12,color:'#e944a7',alignSelf:'center'}}>{Language().no_friend_online_message}</Text>) : (
                <View style={{flex:1,backgroundColor:Color.colorWhite,paddingTop:5,paddingBottom:5}}>
                    <View style={{ marginLeft: 8,marginRight: 8}}>
                        <ListView
                            ref='friendStatusList'
                            dataSource={this.state.dataSource.cloneWithRows(friendStatusList)}
                            renderRow={this.friendStatusRenderRow}
                            renderFooter={this.renderFooter}
                            removeClippedSubviews={true}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={styles.listViewStyle}/>
                    </View>
                </View>);
    }

    /**
     * 换一批
     */
    fetchMoreFriendStatus() {
        if (this.refs.friendStatusList) {
            this.refs.friendStatusList.scrollTo(0, 0);
        }
        this.props.getMoreFriendStatus();
    }

    /**
     * 好友在线信息item
     * @param data
     * @param sectionId
     * @param rowId
     * @returns {XML}
     */
    friendStatusRenderRow(data, sectionId, rowId) {
        return (<View
            style={{margin:3,justifyContent:'center',width:70}}>
            <TouchableOpacity onPress={()=>{that.actionFriendView(data)}}
                              activeOpacity={0.8}
                              style={{backgroundColor:Color.transparent,justifyContent:'center'}}>
                <View style={{margin:0,justifyContent:'center'}}>
                    <CachedImage style={{width:70,height:70,alignSelf:'center',borderRadius:3}}
                                 defaultSource={ImageManager.defaultUser}
                                 source={(data.image==''||data.image==null)?ImageManager.defaultUser:({uri:data.image})}/>
                    <CachedImage
                        style={{width:70,height:70,alignSelf:'center',position: 'absolute',left:0,top:0,bottom:0,right:0}}
                        source={ImageManager.icBorderHearder}/>
                    {that.gameStatusView(data)}

                </View>
                <Text numberOfLines={1} style={{alignSelf:'center',fontSize:10,margin:2,marginTop:5}}>{data.name}</Text>
                <View flexDirection='row' style={{justifyContent:'flex-end',width:70,height:18,alignSelf:'center'}}>
                    <View
                        style={{backgroundColor:'#e944a7',borderRadius:3,justifyContent:'center',width:55,height:14,alignSelf:'center'}}>
                        <Text numberOfLines={1}
                              style={{fontSize:10,color:Color.colorWhite,alignSelf:'center',backgroundColor:Color.transparent}}>{that.formatGameType(data.status)}</Text>
                    </View>
                    <CachedImage
                        style={{width:10,height:13,alignSelf:'center',resizeMode: 'contain',marginLeft:1}}
                        source={ImageManager.icButton}/>
                </View>
            </TouchableOpacity>
        </View>);
    }

    /**
     * 当前游戏状态view
     * 只有是游戏房间才有观战的状态
     *
     * @param data
     */
    gameStatusView(data) {
        let gameStatus = that.getGameStatus(data.status);
        if (gameStatus) {
            if (gameStatus.game_status == 2) {
                //准备中
                return (<View style={{ position: 'absolute',bottom:5,right:5,borderRadius:5,backgroundColor:'#ff9319'}}>
                    <Text
                        style={{fontSize:8,color:Color.colorWhite,marginLeft:5,marginRight:5,alignSelf:'center'}}>{Language().game_prepare}</Text>
                </View>);
            } else if (gameStatus.game_status == 3) {
                let gameStatusText = Language().game_start;
                let position = 0;
                if (gameStatus.position) {
                    position = gameStatus.position;
                }
                if (position > 11) {
                    gameStatusText = Language().game_watching;
                }
                //游戏中或是准备中
                return (
                    <View style={{ position: 'absolute',bottom:5,right:5,borderRadius:5,backgroundColor:'#ff23c7'}}>
                        <Text
                            style={{fontSize:8,color:Color.colorWhite,marginLeft:5,marginRight:5,alignSelf:'center'}}>{gameStatusText}</Text>
                    </View>);
            }
        }
        return null;
    }

    /**
     * 好友在线footer
     */
    renderFooter() {
        let friendCount = friendStatusList.length;
        let defaultWidth = ScreenWidth / 2 + 30;
        if (friendCount == 2) {
            defaultWidth = ScreenWidth / 4 + 30;
        }
        return (friendCount > 0 && friendCount < 3) ? (<View style={{justifyContent:'center',width:defaultWidth}}>
                <Image style={{width:75,height:80,alignSelf:'center',resizeMode: 'contain',}}
                       source={ImageManager.defaultFriendStatus}/>
                {/*<Text*/}
                {/*style={{padding:5,fontSize:12,color:'#DB6BFA',alignSelf:'center'}}>{Language().add_game_friend}</Text>*/}
            </View>) : null;
    }

    /**
     *
     * @param data
     * 游戏房间如果是游戏中，可观战
     */
    actionFriendView(data) {
        let gameStatus = that.getGameStatus(data.status);
        if (gameStatus) {
            if (gameStatus.game_status == 2) {
                //在房间中并且是准备状态
                that.enterFriendRoom(gameStatus.room_id);
            } else if (gameStatus.type != GAME_TYPE_AUDIO && gameStatus.position > 11) {
                that.enterFriendRoom(gameStatus.room_id);
            } else {
                //进入好友详情
                that.actionContact(data);
            }
        } else {
            //进入好友详情
            that.actionContact(data);
        }
    }

    /**
     * 查看好友详情
     * @param item
     */
    actionContact(item) {
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                if (item && item.id) {
                } else {
                    Toast.show(netWorkTool.NOT_NETWORK);
                    return;
                }
                if (Platform.OS === 'ios') {
                    var main = NativeModules.MainViewController;
                    let sex = '2';
                    if (item.sex == 1) {
                        sex = '1';
                    }
                    let image = item.image;
                    if (image == null) {
                        image = '';
                    }
                    main.didSelectUser(item.id, 'detail', sex, image, item.name, '');
                } else {
                    NativeModules.NativeJSModule.rnStartPersonalInfoActivtiy(PersonalInfoActivity, item.id, this.props.token, false);
                }
            } else {
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     *
     * @param roomId
     */
    enterFriendRoom(roomId) {
        //网络检测
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                try {
                    this.onShowProgressDialog();
                    this.state.roomId = roomId;
                    let url = apiDefines.SEARCH_ROOM_INFO + this.state.roomId + apiDefines.PARAMETER_USER_ID + this.props.userInfo.id;
                    if (this.props.hostLocation) {
                        url = url + apiDefines.PARAMETER_LOCATION + this.props.hostLocation
                    }
                    Util.get(url, (code, message, data) => {
                        this.onHideProgressDialog();
                        if (code == 1000) {
                            if (data.level == GAME_TYPE_PRE_SIMPLE || data.level == GAME_TYPE_PRE_SIMPLE_NEW) {
                                //15級別以上不能進入新手房間
                                if (this.getUserLevel() > 15 && !this.checkIsRole()) {
                                    Toast.show(Language().prompt_novice_model);
                                    return;
                                }
                            }
                            this.state.game_type = data.level;
                            this.state.roomId = data.room_id;
                            if (data.password_needed) {
                                //弹框直接进入输入密码
                                this.setState({
                                    dialogIndex: 2,
                                    dialogConfirm: Language().enter,
                                    dialogCancel: Language().cancel,
                                    hideCancel: false,
                                    hideConfirm: false,
                                    password: '',
                                    needPassword: data.password_needed,
                                    textRoomID: roomId
                                });
                                this.onOpenModal();
                            } else {
                                //直接进入
                                this.enterRoom(this.state.roomId);
                            }

                        }
                        else if (code == 1004) {
                            //token 失效
                            this.onShowLoginDialog();
                        } else {
                            Toast.show(message);
                        }
                    }, (failed) => {
                        this.onHideProgressDialog();
                        Toast.show(Language().not_network)
                    });
                } catch (e) {
                    console.log('do search room error:' + e);
                }

            } else {
                //无网络提示
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 获取用户状态信息
     * @param status
     * @returns {{}}
     */
    getGameStatus(status) {
        let gameStatus = {};
        let arrayStatus = new Array();
        arrayStatus = status.split(':');
        for (let i = 0; i < arrayStatus.length; i++) {
            if (i == 0) {
                gameStatus.game_status = parseInt(arrayStatus[i]);
            } else if (i == 1) {
                gameStatus.updateTime = arrayStatus[i];
            } else if (i == 2) {
                gameStatus.room_id = arrayStatus[i];
            } else if (i == 3) {
                gameStatus.type = arrayStatus[i];
            } else if (i == 5) {
                gameStatus.position = parseInt(arrayStatus[i]);
            }
        }
        return gameStatus;
    }

    /**
     * 格式化状态信息
     * @param status
     * @returns {string|string}
     */
    formatGameType(status) {
        let gameType = Language().on_line;
        let gameStatus = that.getGameStatus(status);
        switch (gameStatus.type) {
            case GAME_TYPE_SIMPLE_6:
                gameType = Language().game_simple_6;
                break;
            case GAME_TYPE_SIMPLE_9:
                gameType = Language().game_simple_9;
                break;
            case GAME_TYPE_SIMPLE_10:
                gameType = Language().game_simple_10;
                break;
            case GAME_TYPE_NORMAL:
                gameType = Language().normal_cupid;
                break;
            case GAME_TYPE_NORMAL_GUARD:
                gameType = Language().normal_guard;
                break;
            case GAME_TYPE_HIGH_KING:
                gameType = Language().high_king;
                break;
            case GAME_TYPE_PRE_SIMPLE:
                gameType = Language().novice_model;
                break;
            case GAME_TYPE_PRE_SIMPLE_NEW:
                gameType = Language().hunts_model_6;
                break;
            case GAME_TYPE_AUDIO:
                gameType = Language().voice_model;
                break;
        }
        return gameType;
    }

    /**
     * 有领取的任务
     * @returns {XML}
     */
    taskInfoView() {
        if (this.props.taskInfoData) {
            if (this.props.taskInfoData.complete) {
                return (<View
                    style={{backgroundColor:Color.badge_color,width:6,height:6,borderRadius:6,alignSelf:'center',right:0,position: 'absolute',bottom:10}}/>);
            }
        }
    }

    /*
     * 是否有新的公告
     */
    announcementNewView() {
        return this.checkNoticeVar() ? (<View
                style={{position: 'absolute',right:3,top:5}}>
                <CachedImage source={ImageManager.icHomeNew}
                             style={{width:22,height:12,resizeMode: 'contain'}}/>
            </View>) : null;
    }

    /**
     * 公告咯load success
     */
    loadSuccessNotice() {
        try {
            if (this.checkNoticeVar()) {
                if (this.props.configData) {
                    if (this.props.configData.notice_ver) {
                        this.props.updateNoticeVer(this.props.configData.notice_ver)
                        let data = {
                            notice_ver: this.props.configData.notice_ver
                        }
                        AsyncStorageTool.saveNoticeData(JSON.stringify(data));
                    }
                }
            }
        } catch (e) {

        }
    }

    /**
     *
     * @returns {boolean}
     */
    checkNoticeVar() {
        try {
            let localNoticeVar = 0;
            if (this.props.noticeVer) {
                localNoticeVar = this.props.noticeVer;
            }
            if (this.props.configData) {
                if (this.props.configData.notice_ver) {
                    if (localNoticeVar != this.props.configData.notice_ver) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (err) {
            return false;
        }
    }

    /**
     * 搜索好友／家族
     */
    requestSearch() {
        this.setState({
            showModelProgress: true
        });
        netWorkTool.checkNetworkState((isConnected) => {
            if (isConnected) {
                let activityIndex = this.state.search_dialog_activity;
                let api = apiDefines.GET_FRIEND_INFO_FROM_UID;
                if (activityIndex == 2) {
                    api = apiDefines.GROUP_INFO_FROM_ID;
                }
                let url = api + this.state.textRoomID;
                Util.get(url, (code, message, data) => {
                    this.setState({
                        showModelProgress: false
                    });
                    if (code == 1000) {
                        this.onCloseModal();
                        if (activityIndex == 2) {
                            let group_id = '';
                            if (data && data.group) {
                                group_id = data.group._id;
                            }
                            this.openFamilyDetail(group_id);
                        } else {
                            this.actionContact(data);
                        }

                    } else {
                        Toast.show(message);
                    }
                }, (failed) => {
                    this.setState({
                        showModelProgress: false
                    });
                    Toast.show(netWorkTool.NOT_NETWORK);
                });
            } else {
                this.setState({
                    showModelProgress: false
                });
                Toast.show(netWorkTool.NOT_NETWORK);
            }
        });
    }

    /**
     * 进入家族详情
     * groupId
     */
    openFamilyDetail(groupId) {
        if (groupId) {
            let nativeData = {
                action: Constant.ACTION_SHOW_FAMILY_INFO,
                params: {group_id: groupId},
                type: Constant.RN_NATIVE
            }
            rnRoNativeUtils.sendMessageToNative(JSON.stringify(nativeData));
        }
    }

    /**
     * 进入谁是卧底
     */
    enterUndercover() {
        if (this.props.isTourist) {
            if (this.refs.touristModal) {
                this.refs.touristModal.show({message: Language().enter_undercover_prompt_tourist_login});
            }
        } else {
            this.onShowProgressDialog();
            netWorkTool.checkNetworkState((isConnected) => {
                if (isConnected) {
                    Util.get(apiDefines.GET_AUDIO_RANDOM_ROOM + Constant.VOICE_TYPE_UNDERCOVER, (code, message, data) => {
                        this.onHideProgressDialog();
                        if (code == 1000) {
                            if (data) {
                                let roomInfo = {
                                    roomId: data.room_id,
                                    roomPassword: ''
                                }
                                this.doEnterVoiceRoom(roomInfo);
                            }
                        } else {
                            Toast.show(message);
                        }
                    }, (error) => {
                        this.onHideProgressDialog();
                        Toast.show(Language().not_network);
                    });
                } else {
                    this.onHideProgressDialog();
                    Toast.show(netWorkTool.NOT_NETWORK);
                }
            });
        }
    }

    /**
     * 执行进入房间的操作
     */
    doEnterVoiceRoom(roomData) {
        let NativeData = {
            action: Constant.ACTION_ENTER_AUDIO,
            params: roomData,
            options: null
        }
        rnRoNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
    }
}
import GameRule from '../../../support/common/GameRule';
import GameRole from '../../../support/common/GameRole';
import GameSimpleModel from '../../../support/common/GameSimpleModel';
import GameStAdModel from '../../../support/common/GameStAdModel';
import GameProfessionalLlg from '../../../support/common/GameProfessionalLlg';

const roleIntroduction = require('../../../resources/imgs/ic_role_introduction.png');//角色介绍
const videoHelp = require('../../../resources/imgs/ic_video_help.png');//视频宝典
const gameRule = require('../../../resources/imgs/ic_game_rule.png');//游戏规则
const simpleNovModel = require('../../../resources/imgs/ic_simple_nov.png');//新手简单模式介绍
const stAdModel = require('../../../resources/imgs/ic_st_ad_model.png');//标准高阶模式介绍
const professionalLg = require('../../../resources/imgs/ic_professional_lg.png');//专业术语
const mapDispatchToProps = dispatch => ({
    openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
    updateRoomId: (roomId, password) => dispatch((updateRoomId(roomId, password))),
    finishPage: (key) => dispatch(popRoute(key)),
    removeChatData: () => dispatch(removeChat()),
    updateUserInfo: (data) => dispatch((userInfo(data))),
    updateTourist: (isTourist) => dispatch((updateTourist(isTourist))),
    clearMessageList: () => dispatch(clearMessageList()),
    clearFriendList: () => dispatch(clearFriendList()),
    removeFriendStatus: () => dispatch(removeFriendStatus()),
    getMoreFriendStatus: () => dispatch(getMoreFriendStatus()),
    getUserInfo: (id) => dispatch(getUserInfo(id)),
    updateNoticeVer: (noticeVar) => dispatch(updateNoticeVer(noticeVar)),
    taskInfo: () => dispatch(taskInfo()),
    updateGiftManifest: (gifts) => dispatch(updateGiftManifest(gifts)),
    updateConfig: () => dispatch(checkOpenRegister()),
});
const mapStateToProps = state => ({
    code: state.userInfoReducer.code,
    message: state.userInfoReducer.message,
    userInfo: state.userInfoReducer.data,
    token: state.userInfoReducer.token,
    newRoomId: state.userInfoReducer.roomId,
    newPassword: state.userInfoReducer.password,
    navigation: state.cardNavigation,
    isTourist: state.userInfoReducer.isTourist,
    enterNoviceCount: state.configReducer.enter_novice_count,
    noticeVer: state.configReducer.newNoticeVer,
    hostLocation: state.configReducer.location,
    friendStatusDataList: state.friendStatusDataReducer.data,
    taskInfoData: state.taskInfoReducer.data,
    configData: state.checkRegisterReducer.data
});
export default connect(mapStateToProps, mapDispatchToProps)(Home);
