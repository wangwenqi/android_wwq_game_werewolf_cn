/**
 * Created by wangxu on 2017/3/29.
 */
'use strict';
export const ENTER_WEREWOLF = 'enter_werewolf';
export const ENTER_CHANGE_FACE = 'enter_change_face';
export const ENTER_UNDERCOVER = 'enter_undercover';
export const ENTER_MORE_MINI_GAME = 'enter_more_mini_game';
export const GAME_LINK_FIVE = 'game_link_five';
export const GAME_CATCH_THIEF = 'game_catch_thief';
export const GAME_ANIMAL_FIGHT = 'game_animal_fight';
export const GAME_HOME_SEARCH = 'game_home_search';
export const GAME_HOME_CREATE = 'game_home_create';
export const GAME_HOME_HELP = 'game_home_help';
export const GAME_HOME_OPEN_STORE = 'game_home_open_store';
export const GAME_HOME_OPEN_GROUP = 'game_home_open_group';
export const GAME_HOME_OPEN_NOTIFICATION = 'game_home_open_notification';
export const WEREWOLF_HOME_OPEN_NOVICE = 'werewolf_home_open_novice';
export const WEREWOLF_HOME_OPEN_SIMPLE = 'werewolf_home_open_simple';
export const WEREWOLF_HOME_OPEN_ADVANCED = 'werewolf_home_open_advanced';

