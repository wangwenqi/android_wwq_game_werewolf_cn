'use strict';

import React, {Component} from 'react';
import {Provider} from 'react-redux';

import configureStore from '../support/store/index';

let store = configureStore();
import AppNavigation from './navigator'


export default class App extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: true,
            store: configureStore(() => {
                this.setState({isLoading: false})
            })
        }
    }

    render() {
        return (
            <Provider store={this.state.store}>
                <AppNavigation />
            </Provider>
        )
    }
}

