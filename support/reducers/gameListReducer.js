/**
 * Created by wangxu on 2017/12/7.
 */
import * as types from '../actions/actionTypes';
const defaultItem = {name: "", icon: "", type: "random_mini_game"};
const initialState = {
    data: [defaultItem, {
        "type": "game_link_five",
        "name": "五子棋",
        "icon": "http://werewolf-image.xiaobanhui.com/game/icons/five.png"
    }, {
        "type": "game_animal_fight",
        "name": "斗兽棋",
        "icon": "http://werewolf-image.xiaobanhui.com/game/icons/animal.png"
    }, {
        "type": "game_catch_thief",
        "name": "狼人快跑",
        "icon": "http://werewolf-image.xiaobanhui.com/game/icons/police.png"
    }, {
        "type": "game_basketball",
        "name": "终极篮球",
        "icon": "http://werewolf-image.xiaobanhui.com/game/icons/basketball.png"
    }],
}
let gameListReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_GAME_LIST_DATA:
            return {
                ...state,
                data: action.data,
            }
        case types.GET_GAME_LIST_DATA_ERR:
            return {
                ...state
            }
        default:
            return state;
    }
}
export default gameListReducer;