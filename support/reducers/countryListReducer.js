/**
 * Created by wangxu on 2017/5/16.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    data: null
}
let countryListReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SELECTED_COUNTRY:
            return {
                ...state,
                data: action.data,
            }
        case types.INIT_COUNTRY:
            return {
                ...state,
                data: action.data
            }
        default:
            return state;
    }
}
export default countryListReducer;