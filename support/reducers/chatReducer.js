/**
 * Created by wangxu on 2017/4/25.
 */
import * as types from '../actions/actionTypes';
import AsyncStorageTool from '../common/AsyncStorageTool';

const initialState = {
    data: null,
    messageNumber: 0,
    isRefreshing: false,
    isNoMore: false,
}
// var mapChat;
// var number = 0;
let chatReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.UPDATE_CHAT_DATA:
            return {
                ...state,
                data: action.data,
                messageNumber: action.count,
                isRefreshing: false,
            }
        case types.DELETE_CHAT_DATA:
            return {
                ...state,
                data: action.data,
                messageNumber: action.count,
            }
        case types.GET_CHAT_DATA:
            return {
                ...state,
                data: action.data,
                messageNumber: action.count,
            }
        case types.REMOVE_CHAT_DATA:
            return {
                ...state,
                data: null,
                messageNumber: 0,
            }
        case types.READ_CHAT_DATA:
            return {
                ...state,
                data: action.data,
                messageNumber: action.count,
            }
        case types.GET_MORE_CHAT_DATA:
            return {
                ...state,
                data: action.data,
                isNoMore: action.data.total_page == action.data.page,
                isRefreshing: false
            }
        case types.REFRESH_CHAT_DATA:
            return {
                ...state,
                data: action.data,
                isNoMore: false,
                isRefreshing: false
            }
        default:
            return state;
    }
}
export default chatReducer;