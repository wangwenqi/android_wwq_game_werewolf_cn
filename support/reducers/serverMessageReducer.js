/**
 * Created by wangxu on 2017/3/29.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    code: 0,
    data: null,
}
let serverMessageReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SERVER_MESSAGE_DATA:
            return {
                ...state,
                data: action.data,
                code: action.code,
            }
        case types.SERVER_MESSAGE_ACTION_ERROR:
            return {
                ...state,
                code: action.code,
                data: null
            }
        default:
            return state;
    }
}
export default serverMessageReducer;