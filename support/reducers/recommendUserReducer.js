/**
 * Created by wangxu on 2017/9/15.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    code: 0,
    data: null,
    isRefreshing: false,
    isNoMore: false,
}
//     "data": [
//     {
//         "_id": "59f8335ad84a7e49cf72b589",
//         "sex": 1,
//         "name": "haha",
//         "image": "http://werewolf-image.xiaobanhui.com/Fi6sAwIJJEgHoWMsoVgUaJ3OAgUQ?imageslim",
//         "room": {
//             "roomType": "audio",
//             "roomId": "066393",
//             "childType": "sing"
//         },
//         "pop": 412110
//     },
// ]
/**
 *
 * @param state
 * @param action
 * @returns {*}
 *
 */
let recommendUserReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_RECOMMEND_LIST_DATA:
            return {
                ...state,
                data: action.data,
                code: action.code,
                isRefreshing: false,
                isNoMore: false,
            }
        case types.GET_MORE_RECOMMEND_LIST_DATA:
            //获取更多没有取到值设置为没有更多
            let newData = action.data;
            let oldData = state.data;
            let isNoMore = false;
            //暂时
            if (newData) {
                if (oldData) {
                    newData = oldData.concat(newData);
                }
            } else {
                isNoMore = true;
                newData = oldData;
            }
            return {
                ...state,
                data: newData,
                code: action.code,
                isNoMore: isNoMore,
                isRefreshing: false,
            }
        case types.GET_RECOMMEND_LIST_DATA_ERR:
            return {
                ...state,
                code: action.code,
                isRefreshing: false,
            }
        default:
            return state;
    }
}
export default recommendUserReducer;