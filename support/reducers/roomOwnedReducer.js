/**
 * Created by wangxu on 2017/9/15.
 */
import * as types from '../actions/actionTypes';
import Language from '../../resources/language';
const initialState = {
    code: 0,
    data: null,
    isRefreshing: false,
    types: '',
    message: ''
}
/**
 *
 * @param state
 * @param action
 * @returns {*}
 *
 */
let roomOwnedReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ROOM_OWNED_DATA:
            let formatData = action.data;
            try {
                if (formatData) {
                    if (formatData.length > 0) {
                        formatData.sort(function (x, y) {
                            return x.owner_type > y.owner_type ? 1 : -1;
                        });
                    }
                }
            } catch (e) {

            }
            return {
                ...state,
                data: formatData,
                code: action.code,
                isRefreshing: false,
                isNoMore: false,
                types: types.ROOM_OWNED_DATA,
                message: ''
            }
        case types.ROOM_OWNED_DATA_ERROR:
            let message = Language().request_error;
            if (action.message) {
                message = action.message;
            }
            return {
                ...state,
                code: action.code,
                isRefreshing: false,
                types: types.ROOM_OWNED_DATA_ERROR,
                message: message
            }
        case types.REFRESHING_ROOM_OWNED:
            return {
                ...state,
                isRefreshing: true,
                message: ''
            }
        case types.CLEAR_ROOM_OWNED:
            return {
                ...state,
                data: null,
                isRefreshing: false,
                types: '',
                message: '',
            }
        default:
            return state;
    }
}
export default roomOwnedReducer;