/**
 * Created by wangxu on 2017/4/25.
 */
import * as types from '../actions/actionTypes';

const initialState = {
    data: null,
}
let familyChatReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.UPDATE_FAMILY_CHAT_DATA:
            return {
                ...state,
                data: action.data,
            }
        case types.GET_FAMILY_CHAT_DATA:
            return {
                ...state,
                data: action.data,
            }
        case types.REMOVE_FAMILY_CHAT_DATA:
            return {
                ...state,
                data: null,
            }
        case types.READ_FAMILY_CHAT_DATA:
            return {
                ...state,
                data: action.data,
            }
        case types.DELETE_FAMILY_CHAT_DATA:
            return {
                ...state,
                data: null
            }
        default:
            return state;
    }
}
export default familyChatReducer;