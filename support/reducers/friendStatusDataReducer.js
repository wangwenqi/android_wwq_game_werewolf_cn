/**
 * Created by wangxu on 2017/10/9.
 */
import * as types from '../actions/actionTypes';
const count = 8;
const initialState = {
    code: 0,
    message: '',
    data: new Array(),
    allData: new Array(),
    page: 0,
    pages: 0
}
let friendStatusDataReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.FRIEND_STATUS_DATA:
            let pages = 0;
            let page = 0;
            let data = new Array();
            if (action.data) {
                pages = Math.ceil(action.data.length / count);
                if (pages) {
                    page = 1
                }
                data = action.data.slice(0, count);
            }
            return {
                ...state,
                allData: action.data,
                message: action.message,
                code: action.code,
                pages: pages,
                page: page,
                data: data
            }
        case types.FRIEND_STATUS_DATA_ERROR:
            return {
                ...state
            }
        case types.FRIEND_STATUS_MORE_DATA:
            if (state.page < state.pages) {
                state.page = state.page + 1;
            } else if (state.page == state.pages) {
                state.page = 1;
            }
            let index = 0;
            let endIndex = count;

            if (state.page) {
                index = (state.page - 1) * count;
                endIndex = state.page * count;
            }
            let newData;
            if(state.allData){
                newData=state.allData.slice(index,endIndex);
            }
            return {
                ...state,
                data:newData
            }
        case types.REMOVE_FRIEND_STATUS_DATA:
            return{
                ...state,
                allData:new Array(),
                data:new Array(),
                page:0,
                pages:0
            }
        default:
            return state;
    }
}
export default friendStatusDataReducer;