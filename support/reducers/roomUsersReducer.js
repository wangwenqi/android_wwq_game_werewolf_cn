/**
 * Created by wangxu on 2017/9/15.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    code: 0,
    data: [],
}
/**
 *
 * @param state
 * @param action
 * @returns {*}
 *
 */
let roomUsersReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ROOM_USERS_DATA:
            return {
                ...state,
                data: action.data,
            }
        case types.ROOM_USERS_DATA_ERROR:
            return {
                ...state,
            }
        case  types.CLEAR_ROOM_USERS_DATA:
            return {
                ...state,
                data: []
            }
        default:
            return state;
    }
}
export default roomUsersReducer;