/**
 * Created by wangxu on 2017/9/15.
 */
import * as types from '../actions/actionTypes';
import * as Constant from '../common/constant';
/*{
 "code": 1000,
 "data": {
 "limit": 30,
 "skip": 0,
 "page": 1,
 "pages": 3,
 "total": 78,
 "rooms": [
 {
 "room_id": "099825",
 "level": "audio",
 "type": "private",
 "created_at": 1505457128986,
 "image": "http://q.qlogo.cn/qqapp/1106115686/391457C86B3C5CEFEA4E7D3CF58598B6/100",
 "roomStatus": 1,
 "users": [
 {
 "id": "599ec207e91aeae068fc73ef",
 "name": "天下第一",
 "avatar": "http://werewolf.bj.bcebos.com/default_avatar/7.png"
 }
 ],
 "needPassword": false
 },
 ]
 }*/
const initialState = {
    code: 0,
    voiceType: Constant.VOICE_TYPE_ALL,
    dataSing: null,
    isRefreshingSing: false,
    isNoMoreSing: false,
    typesSing: '',
    dataChat: null,
    isRefreshingChat: false,
    isNoMoreChat: false,
    typesChat: '',
    dataLove: null,
    isRefreshingLove: false,
    isNoMoreLove: false,
    typesLove: '',
    dataFate: null,
    isRefreshingFate: false,
    isNoMoreFate: false,
    typesFate: '',
    dataMerry: null,
    isRefreshingMerry: false,
    isNoMoreMerry: false,
    typesMerry: '',
    dataGame: null,
    isRefreshingGame: false,
    isNoMoreGame: false,
    typesGame: '',
    dataUndercover: null,
    isRefreshingUndercover: false,
    isNoMoreUndercover: false,
    typesUndercover: '',
    dataSeek: null,
    isRefreshingSeek: false,
    isNoMoreSeek: false,
    typesSeek: '',
    dataAll: null,
    isRefreshingAll: false,
    isNoMoreAll: false,
    typesAll: '',
}
/**
 *
 * @param state
 * @param action
 * @returns {*}
 *
 */
let recreationReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.RECREATION_ALL_REFRESH_DATA:
            //all
            return {
                ...state,
                dataAll: action.data,
                isRefreshingAll: false,
                isNoMoreAll: false,
                typesAll: types.RECREATION_ALL_REFRESH_DATA
            }
        case types.RECREATION_ALL_DATA:
            return {
                ...state,
                dataAll: action.data,
                isRefreshingAll: false,
                isNoMoreAll: false,
                typesAll: types.RECREATION_ALL_DATA
            }
        case types.RECREATION_ALL_MORE_DATA: {
            let newList = new Array();
            let newData = action.data;
            let oldData = state.dataAll;
            let isNoMore = false;
            if (oldData.page < newData.page) {
                newList = oldData.rooms.concat(newData.rooms);
                newData.rooms = newList;
            } else {
                newData = oldData;
            }
            if (oldData.pages == newData.page) {
                isNoMore = true;
            }
            return {
                ...state,
                dataAll: newData,
                isNoMoreAll: isNoMore,
                isRefreshingAll: false,
                typesAll: types.RECREATION_ALL_MORE_DATA
            }
        }

        case types.RECREATION_ALL_DATA_ERROR:
            return {
                ...state,
                isRefreshingAll: false,
                typesAll: types.RECREATION_ALL_DATA_ERROR
            }
        case types.RECREATION_SING_REFRESH_DATA:
            //k 歌
            return {
                ...state,
                dataSing: action.data,
                isRefreshingSing: false,
                isNoMoreSing: false,
                typesSing: types.RECREATION_SING_REFRESH_DATA
            }
        case types.RECREATION_SING_DATA:
            return {
                ...state,
                dataSing: action.data,
                isRefreshingSing: false,
                isNoMoreSing: false,
                typesSing: types.RECREATION_SING_DATA
            }
        case types.RECREATION_SING_MORE_DATA: {
            let newList = new Array();
            let newData = action.data;
            let oldData = state.dataSing;
            let isNoMore = false;
            if (oldData.page < newData.page) {
                newList = oldData.rooms.concat(newData.rooms);
                newData.rooms = newList;
            } else {
                newData = oldData;
            }
            if (oldData.pages == newData.page) {
                isNoMore = true;
            }
            return {
                ...state,
                dataSing: newData,
                isNoMoreSing: isNoMore,
                isRefreshingSing: false,
                typesSing: types.RECREATION_SING_MORE_DATA
            }
        }

        case types.RECREATION_SING_DATA_ERROR:
            return {
                ...state,
                isRefreshingSing: false,
                typesSing: types.RECREATION_SING_DATA_ERROR
            }
        case types.RECREATION_CHAT_REFRESH_DATA:
            //聊天
            return {
                ...state,
                dataChat: action.data,
                isRefreshingChat: false,
                isNoMoreChat: false,
                typesChat: types.RECREATION_CHAT_REFRESH_DATA
            }
        case types.RECREATION_CHAT_DATA:
            return {
                ...state,
                dataChat: action.data,
                isRefreshingChat: false,
                isNoMoreChat: false,
                typesChat: types.RECREATION_CHAT_DATA
            }
        case types.RECREATION_CHAT_MORE_DATA: {
            let newList = new Array();
            let newData = action.data;
            let oldData = state.dataChat;
            let isNoMore = false;
            if (oldData.page < newData.page) {
                newList = oldData.rooms.concat(newData.rooms);
                newData.rooms = newList;
            } else {
                newData = oldData;
            }
            if (oldData.pages == newData.page) {
                isNoMore = true;
            }
            return {
                ...state,
                dataChat: newData,
                isNoMoreChat: isNoMore,
                isRefreshingChat: false,
                typesChat: types.RECREATION_CHAT_MORE_DATA
            }
        }
        case types.RECREATION_CHAT_DATA_ERROR:
            return {
                ...state,
                isRefreshingChat: false,
                typesChat: types.RECREATION_CHAT_DATA_ERROR
            }
        case types.RECREATION_LOVE_REFRESH_DATA:
            //婚礼
            return {
                ...state,
                dataLove: action.data,
                isRefreshingLove: false,
                isNoMoreLove: false,
                typesLove: types.RECREATION_LOVE_REFRESH_DATA
            }
        case types.RECREATION_LOVE_DATA:
            return {
                ...state,
                dataLove: action.data,
                isRefreshingLove: false,
                isNoMoreLove: false,
                typesLove: types.RECREATION_LOVE_DATA
            }
        case types.RECREATION_LOVE_MORE_DATA: {
            let newList = new Array();
            let newData = action.data;
            let oldData = state.dataLove;
            let isNoMore = false;
            if (oldData.page < newData.page) {
                newList = oldData.rooms.concat(newData.rooms);
                newData.rooms = newList;
            } else {
                newData = oldData;
            }
            if (oldData.pages == newData.page) {
                isNoMore = true;
            }
            return {
                ...state,
                dataLove: newData,
                isNoMoreLove: isNoMore,
                isRefreshingLove: false,
                typesLove: types.RECREATION_LOVE_MORE_DATA
            }
        }
        case types.RECREATION_LOVE_DATA_ERROR:
            return {
                ...state,
                isRefreshingLove: false,
                typesLove: types.RECREATION_LOVE_DATA_ERROR
            }
        case types.RECREATION_FATE_REFRESH_DATA:
            //算命
            return {
                ...state,
                dataFate: action.data,
                isRefreshingFate: false,
                isNoMoreFate: false,
                typesFate: types.RECREATION_FATE_REFRESH_DATA
            }
        case types.RECREATION_FATE_DATA:
            return {
                ...state,
                dataFate: action.data,
                isRefreshingFate: false,
                isNoMoreFate: false,
                typesFate: types.RECREATION_FATE_DATA
            }
        case types.RECREATION_FATE_MORE_DATA: {
            let newList = new Array();
            let newData = action.data;
            let oldData = state.dataFate;
            let isNoMore = false;
            if (oldData.page < newData.page) {
                newList = oldData.rooms.concat(newData.rooms);
                newData.rooms = newList;
            } else {
                newData = oldData;
            }
            if (oldData.pages == newData.page) {
                isNoMore = true;
            }
            return {
                ...state,
                dataFate: newData,
                isNoMoreFate: isNoMore,
                isRefreshingFate: false,
                typesFate: types.RECREATION_FATE_MORE_DATA
            }
        }

        case types.RECREATION_FATE_DATA_ERROR:
            return {
                ...state,
                isRefreshingFate: false,
                typesFate: types.RECREATION_FATE_DATA_ERROR
            }
        case types.RECREATION_MERRY_REFRESH_DATA:
            //婚礼
            return {
                ...state,
                dataMerry: action.data,
                isRefreshingMerry: false,
                isNoMoreMerry: false,
                typesMerry: types.RECREATION_MERRY_REFRESH_DATA
            }
        case types.RECREATION_MERRY_DATA:
            return {
                ...state,
                dataMerry: action.data,
                isRefreshingMerry: false,
                isNoMoreMerry: false,
                typesMerry: types.RECREATION_MERRY_DATA
            }
        case types.RECREATION_MERRY_MORE_DATA: {
            let newList = new Array();
            let newData = action.data;
            let oldData = state.dataMerry;
            let isNoMore = false;
            if (oldData.page < newData.page) {
                newList = oldData.rooms.concat(newData.rooms);
                newData.rooms = newList;
            } else {
                newData = oldData;
            }
            if (oldData.pages == newData.page) {
                isNoMore = true;
            }
            return {
                ...state,
                dataMerry: newData,
                isNoMoreMerry: isNoMore,
                isRefreshingMerry: false,
                typesMerry: types.RECREATION_MERRY_MORE_DATA
            }
        }
        case types.RECREATION_MERRY_DATA_ERROR:
            return {
                ...state,
                isRefreshingMerry: false,
                typesMerry: types.RECREATION_MERRY_DATA_ERROR
            }
        case types.RECREATION_SEEK_REFRESH_DATA:
            //情感咨询
            return {
                ...state,
                dataSeek: action.data,
                isRefreshingSeek: false,
                isNoMoreSeek: false,
                typesSeek: types.RECREATION_SEEK_REFRESH_DATA
            }
        case types.RECREATION_SEEK_DATA:
            return {
                ...state,
                dataSeek: action.data,
                isRefreshingSeek: false,
                isNoMoreSeek: false,
                typesSeek: types.RECREATION_SEEK_DATA
            }
        case types.RECREATION_SEEK_MORE_DATA: {
            let newList = new Array();
            let newData = action.data;
            let oldData = state.dataSeek;
            let isNoMore = false;
            if (oldData.page < newData.page) {
                newList = oldData.rooms.concat(newData.rooms);
                newData.rooms = newList;
            } else {
                newData = oldData;
            }
            if (oldData.pages == newData.page) {
                isNoMore = true;
            }
            return {
                ...state,
                dataSeek: newData,
                isNoMoreSeek: isNoMore,
                isRefreshingSeek: false,
                typesSeek: types.RECREATION_SEEK_MORE_DATA
            }
        }

        case types.RECREATION_SEEK_DATA_ERROR:
            return {
                ...state,
                isRefreshingSeek: false,
                typesSeek: types.RECREATION_SEEK_DATA_ERROR
            }
        case types.RECREATION_GAME_REFRESH_DATA:
            //小游戏
            return {
                ...state,
                dataGame: action.data,
                isRefreshingGame: false,
                isNoMoreGame: false,
                typesGame: types.RECREATION_GAME_REFRESH_DATA
            }
        case types.RECREATION_GAME_DATA:
            return {
                ...state,
                dataGame: action.data,
                isRefreshingGame: false,
                isNoMoreGame: false,
                typesGame: types.RECREATION_GAME_DATA
            }
        case types.RECREATION_GAME_MORE_DATA: {
            let newList = new Array();
            let newData = action.data;
            let oldData = state.dataGame;
            let isNoMore = false;
            if (oldData.page < newData.page) {
                newList = oldData.rooms.concat(newData.rooms);
                newData.rooms = newList;
            } else {
                newData = oldData;
            }
            if (oldData.pages == newData.page) {
                isNoMore = true;
            }
            return {
                ...state,
                dataGame: newData,
                isNoMoreGame: isNoMore,
                isRefreshingGame: false,
                typesGame: types.RECREATION_GAME_MORE_DATA
            }
        }

        case types.RECREATION_GAME_DATA_ERROR:
            return {
                ...state,
                isRefreshingGame: false,
                typesGame: types.RECREATION_GAME_DATA_ERROR
            }
        case types.RECREATION_UNDERCOVER_REFRESH_DATA:
            //谁是卧底
            return {
                ...state,
                dataUndercover: action.data,
                isRefreshingUndercover: false,
                isNoMoreUndercover: false,
                typesUndercover: types.RECREATION_UNDERCOVER_REFRESH_DATA
            }
        case types.RECREATION_UNDERCOVER_DATA:
            return {
                ...state,
                dataUndercover: action.data,
                isRefreshingUndercover: false,
                isNoMoreUndercover: false,
                typesUndercover: types.RECREATION_UNDERCOVER_DATA
            }
        case types.RECREATION_UNDERCOVER_MORE_DATA: {
            let newList = new Array();
            let newData = action.data;
            let oldData = state.dataUndercover;
            let isNoMore = false;
            if (oldData.page < newData.page) {
                newList = oldData.rooms.concat(newData.rooms);
                newData.rooms = newList;
            } else {
                newData = oldData;
            }
            if (oldData.pages == newData.page) {
                isNoMore = true;
            }
            return {
                ...state,
                dataUndercover: newData,
                isNoMoreUndercover: isNoMore,
                isRefreshingUndercover: false,
                typesUndercover: types.RECREATION_UNDERCOVER_MORE_DATA
            }
        }

        case types.RECREATION_UNDERCOVER_DATA_ERROR:
            return {
                ...state,
                isRefreshingUndercover: false,
                typesUndercover: types.RECREATION_UNDERCOVER_DATA_ERROR
            }
        case types.VOICE_ROOM_TYPE:
            return {
                ...state,
                voiceType: action.data,
            }
        case types.CLEAR_FRIEND_DATA:
            return {
                ...state,
                dataSing: null,
                dataChat: null,
                dataLove: null,
                dataFate: null,
                dataMerry: null,
                typesUndercover: null,
                dataGame: null,
                dataSeek: null,
                dataAll:null,
                voiceType:  Constant.VOICE_TYPE_ALL,
            }
        default:
            return state;
    }
}
export default recreationReducer;