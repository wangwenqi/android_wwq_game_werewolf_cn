/**
 * Created by wangxu on 2017/10/27.
 */
import * as types from '../actions/actionTypes';

const initialState = {
    code: 0,
    data: null,
    isRefreshing: false,
}
/**
 *
 * @param state
 * @param action
 * @returns {*}
 *
 */
/*{
 "code": 1000,
 "data": {
 "groups": [
 {
 "_id": "59ed6711bf6916371eb920a4",
 "gid": 10011,
 "name": "value1value1",
 "short_name": "shna",
 "image": "imageimageimageimageimage",
 "own_id": "59b239d3e91aeae068154e73",
 "member_count": 1,
 "created_at": 1508730641415,
 "max_members": 50,
 "own_name": "始祖马哈哈哈"
 }
 ],
 "total": 1,
 "skip": 0,
 "limit": 20
 }
 }*/
let familyGroupReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.FAMILY_GROUP_DATA:
            return {
                ...state,
                data: action.data,
                code: action.code,
                isRefreshing: false,
            }
        case types.FAMILY_GROUP_DATA_MORE:
            return {
                ...state,
                data: action.data,
                code: action.code,
                isRefreshing: false,
            }
        case types.FAMILY_GROUP_DATA_ERROR:
            return {
                ...state,
                code: action.code,
                isRefreshing: false,
            }
        case types.UPDATE_FAMILY_GROUP_REQUEST:
            let id = action._id;
            let rowId = action.rowId;
            let stateDataJson = JSON.stringify(state.data);
            let newData = JSON.parse(stateDataJson);
            if (newData && newData.groups) {
                if (rowId < newData.groups.length) {
                    let groupData = newData.groups[rowId];
                    if (groupData) {
                        if (groupData._id == id) {
                            groupData.require_statsu = 1;
                        }
                    }
                }
            }
            return {
                ...state,
                data: newData,
            }
        default:
            return state;
    }
}
export default familyGroupReducer;