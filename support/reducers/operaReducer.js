/**
 * Created by wangxu on 2017/12/7.
 */
import * as types from '../actions/actionTypes';
// {
//     "code": 1000,
//     "data": {
//     "ads": [
//         {
//             "type": "url",
//             "img": "https://game-cdn.xiaobanhui.com/img/recomend/banner/j-10t.png",
//             "src": {
//                 "url": "http://staging.intviu.cn:8200"
//             }
//         },
//         {
//             "type": "enter",
//             "img": "https://game-cdn.xiaobanhui.com/img/recomend/banner/b-12a.png",
//             "src": {
//                 "roomId": "123456",
//                 "roomPassword": ""
//             }
//         }
//     ]
// }
// }
const initialState = {
    data:null
}
let operaReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_OPERA_LIST_DATA:
            return {
                ...state,
                data: action.data,
            }
        case types.GET_OPERA_LIST_DATA_REEOR:
            return {
                ...state
            }
        default:
            return state;
    }
}
export default operaReducer;