/**
 * Created by wangxu on 2017/3/29.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    code: 0,
    message: '',
    data: null,
    token: '',
    roomId: '',
    password: '',
    isLoginIn: false,
    isTourist: false,
    tabName: types.ACTION_HOME
}
let userInfoReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.UPDATE_USER_INFO:
            let newData = action.data;
            let oldData = state.data;
            try {
                if (oldData != null) {
                    newData.token = oldData.token
                }
            } catch (e) {

            }
            return {
                ...state,
                code: action.code,
                message: action.message,
                data: newData,
            }
        case types.USER_INFO:
            return {
                ...state,
                code: action.code,
                message: action.message,
                data: action.data,
                token: action.token,
                isLoginIn: true
            }
        case types.USER_INFO_ACTION_ERRO:
            return {
                ...state,
                message: action.message,
                code: action.code,
            }
        case types.GET_ROOM_ID:
            return {
                ...state,
                roomId: action.roomId,
                password: action.password
            }
        case types.IS_TOURIST:
            return {
                ...state,
                isTourist: action.Tourist,
                tabName: types.ACTION_HOME
            }
        case types.ACTION_TAB:
            return {
                ...state,
                tabName: action.TabName
            }
        case types.UPDATE_GROUP_INFO:
            let olderData = state.data;
            let newDataJson=JSON.stringify(olderData);
            let newGroupData=JSON.parse(newDataJson);
            let newGroupInfo = action.data;
            if (newGroupData) {
                newGroupData.group = newGroupInfo;
            }
            return {
                ...state,
                data: newGroupData,
            }
        case types.UPDATE_GROUP_INFO_ERROR:
            return {
                ...state
            }
        default:
            return state;
    }
}
export default userInfoReducer;