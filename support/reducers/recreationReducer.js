/**
 * Created by wangxu on 2017/9/15.
 */
import * as types from '../actions/actionTypes';
/*{
 "code": 1000,
 "data": {
 "limit": 30,
 "skip": 0,
 "page": 1,
 "pages": 3,
 "total": 78,
 "rooms": [
 {
 "room_id": "099825",
 "level": "audio",
 "type": "private",
 "created_at": 1505457128986,
 "image": "http://q.qlogo.cn/qqapp/1106115686/391457C86B3C5CEFEA4E7D3CF58598B6/100",
 "roomStatus": 1,
 "users": [
 {
 "id": "599ec207e91aeae068fc73ef",
 "name": "天下第一",
 "avatar": "http://werewolf.bj.bcebos.com/default_avatar/7.png"
 }
 ],
 "needPassword": false
 },
 ]
 }*/
const initialState = {
    code: 0,
    data: null,
    isRefreshing: false,
    isNoMore: false,
    filterFlag:0,
    types:''
}
/**
 *
 * @param state
 * @param action
 * @returns {*}
 *
 */
let recreationReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.RECREATION_REFRESH_DATA:
            return {
                ...state,
                data: action.data,
                code: action.code,
                isRefreshing: false,
                isNoMore: false,
                types:types.RECREATION_REFRESH_DATA
            }
        case types.RECREATION_DATA:
            return {
                ...state,
                data: action.data,
                code: action.code,
                isRefreshing: false,
                isNoMore: false,
                types:types.RECREATION_DATA
            }
        case types.RECREATION_MORE_DATA:
            let newList = new Array();
            let newData = action.data;
            let oldData = state.data;
            let isNoMore = false;
            if (oldData.page < newData.page) {
                newList = oldData.rooms.concat(newData.rooms);
                newData.rooms = newList;
            } else {
                newData = oldData;
            }
            if (oldData.pages == newData.page) {
                isNoMore = true;
            }
            return {
                ...state,
                data: newData,
                code: action.code,
                isNoMore: isNoMore,
                isRefreshing: false,
                types:types.RECREATION_MORE_DATA
            }
        case types.RECREATION_DATA_ERROR:
            return {
                ...state,
                code: action.code,
                isRefreshing: false,
                types:types.RECREATION_DATA_ERROR
            }
        case types.RECREATION_DATA_FILTER:
            return{
                ...state,
                filterFlag:action.data,
                types:types.RECREATION_DATA_FILTER
            }
        case types.CLEAR_FRIEND_DATA:
            return {
                ...state,
                data: null,
                filterFlag:0,
                types:types.RECREATION_DATA_FILTER
            }
        default:
            return state;
    }
}
export default recreationReducer;