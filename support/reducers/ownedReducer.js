/**
 * Created by wangxu on 2017/9/15.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    data: null,
}
/**
 *
 * @param state
 * @param action
 * @returns {*}
 *
 */
let ownedReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.MY_OWNED_DATA:
            return {
                ...state,
                data: action.data,
            }
        default:
            return state;
    }
}
export default ownedReducer;