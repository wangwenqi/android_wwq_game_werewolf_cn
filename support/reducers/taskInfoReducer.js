/**
 * Created by wangxu on 2017/10/27.
 */
import * as types from '../actions/actionTypes';

const initialState = {
    code: 0,
    data: {complete: 0},
}
/**
 *
 * @param state
 * @param action
 * @returns {*}
 *
 */
/*{
 /*{
 code: ErrorCode.Success,
 payload: {
 complete: count //已经完成的个数
 }
 }*/
let taskInfoReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.TASK_INFO:
            return {
                ...state,
                data: action.data,
            }
        case types.RESET_TASK_INFO:
            return {
                ...state,
                data: null
            }
        default:
            return state;
    }
}
export default taskInfoReducer;