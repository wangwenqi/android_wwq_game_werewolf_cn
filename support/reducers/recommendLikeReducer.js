/**
 * Created by wangxu on 2017/9/15.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    code: 0,
    data: null,
}
//     "data": [
//     {
//         "_id": "59f8335ad84a7e49cf72b589",
//         "sex": 1,
//         "name": "haha",
//         "image": "http://werewolf-image.xiaobanhui.com/Fi6sAwIJJEgHoWMsoVgUaJ3OAgUQ?imageslim",
//         "room": {
//             "roomType": "audio",
//             "roomId": "066393",
//             "childType": "sing"
//         },
//         "pop": 412110
//     },
// ]
/**
 *
 * @param state
 * @param action
 * @returns {*}
 *
 */
let recommendLikeReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_RECOMMEND_LIKE_LIST_DATA:
            return {
                ...state,
                data: action.data,
                code: action.code,
            }
        case types.GET_RECOMMEND_LIKE_LIST_DATA_ERR:
            return {
                ...state,
                code: action.code,
            }
        default:
            return state;
    }
}
export default recommendLikeReducer;