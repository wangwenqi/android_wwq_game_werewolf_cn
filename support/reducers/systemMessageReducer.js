/**
 * Created by wangxu on 2017/3/29.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    code: 0,
    data: null,
}
let systemMessageReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SYSTEM_MESSAGE_DATA:
            return {
                ...state,
                data: action.data,
                code: action.code,
            }
        case types.SYSTEM_MESSAGE_ACTION_ERROR:
            return {
                ...state,
                code: action.code,
            }
        default:
            return state;
    }
}
export default systemMessageReducer;