import {combineReducers} from 'redux';
import userReducer from './user';
import sceneReducer from './scene';
import cardNavigation from './card_navigation'
import messageReducer from './messageReducer';
import friendReducer from './friendReducer';
import userInfoReducer from './userInfoReducer';
import systemMessageReducer from './systemMessageReducer';
import chatReducer from './chatReducer';
import registerReducer from './registerReducer';
import countryListReducer from './countryListReducer';
import checkRegisterReducer from './checkRegisterReducer';
import serverMessageReducer from './serverMessageReducer';
import promotedReducer from './promotedReducer';
import configReducer from './configReducer';
import recreationReducer from './recreationReducer';
import friendStatusDataReducer from './friendStatusDataReducer';
import familyGroupReducer from './familyGroupReducer';
import taskInfoReducer from './taskInfoReducer';
import familyChatReducer from './familychatReducer';
import giftManifestReducer from './giftManifestReducer';
import voiceRecreationReducer from './voiceRecreationReducer';
import rankingReducer from './rankingReducer';
import operaReducer from './operaReducer';
import recommendUserReducer from './recommendUserReducer';
import gameListReducer from './gameListReducer';
import gameHistoryListReducer from './gameHistoryListReducer';
import roomOwnedReducer from './roomOwnedReducer';
import roomUsersReducer from './roomUsersReducer';
import ownedReducer from './ownedReducer';
import recommendLikeReducer from './recommendLikeReducer';
export default combineReducers({
    userStore: userReducer,
    sceneReducer: sceneReducer,
    cardNavigation: cardNavigation,
    messageReducer: messageReducer,
    friendReducer: friendReducer,
    userInfoReducer: userInfoReducer,
    systemMessageReducer: systemMessageReducer,
    chatReducer: chatReducer,
    registerReducer: registerReducer,
    countryListReducer: countryListReducer,
    checkRegisterReducer: checkRegisterReducer,
    serverMessageReducer: serverMessageReducer,
    promotedReducer: promotedReducer,
    configReducer: configReducer,
    recreationReducer: recreationReducer,
    friendStatusDataReducer: friendStatusDataReducer,
    familyGroupReducer: familyGroupReducer,
    taskInfoReducer: taskInfoReducer,
    familyChatReducer: familyChatReducer,
    giftManifestReducer: giftManifestReducer,
    voiceRecreationReducer: voiceRecreationReducer,
    rankingReducer: rankingReducer,
    operaReducer: operaReducer,
    recommendUserReducer: recommendUserReducer,
    gameListReducer: gameListReducer,
    gameHistoryListReducer: gameHistoryListReducer,
    roomOwnedReducer: roomOwnedReducer,
    roomUsersReducer: roomUsersReducer,
    ownedReducer: ownedReducer,
    recommendLikeReducer: recommendLikeReducer,
});
