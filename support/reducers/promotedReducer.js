/**
 * Created by wangxu on 2017/3/29.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    data: {
        prompted: false,
        needPrompt: true,
        level:0
    },
}
let promotedReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.PROMOTED_DATA:
            return {
                ...state,
                data: action.data,
            }
        default:
            return state;
    }
}
export default promotedReducer;