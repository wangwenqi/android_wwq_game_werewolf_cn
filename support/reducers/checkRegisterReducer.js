/**
 * Created by wangxu on 2017/6/6.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    code: 0,
    data: null,
}
let checkRegisterReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CHECK_OPEN_REGISTER:
            return {
                ...state,
                data: action.data,
                code: action.code,
            }
        case types.CHECK_OPEN_REGISTER_ERROR:
            return {
                ...state,
                code: action.code,
            }
        default:
            return state;
    }
}
export default checkRegisterReducer;