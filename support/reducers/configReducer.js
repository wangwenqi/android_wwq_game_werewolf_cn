/**
 * Created by wangxu on 2017/3/29.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    version_name: '1.0',
    leanCloud_node: 'cn',
    location: '',
    enter_novice_count: 0,
    isNewUser: false,
    isTraditional: false,
    friendVersion: 0,
    newNoticeVer: 0
}
let configReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.VERSION_DATA:
            return {
                ...state,
                version_name: action.versionName,
            }
        case types.LEANCLOUD_CONFIG:
            let node = 'cn';
            if (action.leancloudConfig) {
                node = action.leancloudConfig.node
            }
            return {
                ...state,
                leanCloud_node: node
            }
        case types.LOCATION_CONFIG:
            return {
                ...state,
                location: action.location
            }
        case types.ENTER_NOVICE_COUNT:
            return {
                ...state,
                enter_novice_count: action.noviceCount
            }
        case types.NEW_USER_CONFIG:
            return {
                ...state,
                isNewUser: true
            }
        case types.LANGUAGE_CONFIG:
            return {
                ...state,
                isTraditional: true
            }
        case types.UPDATE_FRIEND_VERSION:
            return {
                ...state,
                friendVersion: action.friendVersion
            }
        case types.UPDATE_NOTICE_VER:
            return {
                ...state,
                newNoticeVer: action.notice_ver
            }
        default:
            return state;
    }
}
export default configReducer;