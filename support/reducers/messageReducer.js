/**
 * Created by wangxu on 2017/3/29.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    code: 0,
    message: '',
    data: null,
    type: '',
    isRefreshing: false,
    isNoMore: false,
}
let messageReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.MESSAGE_DATA:
            return {
                ...state,
                data: action.data,
                message: action.message,
                code: action.code,
                type: types.MESSAGE_DATA,
            }
        case types.MESSAGE_ACTION_ERROR:
            return {
                ...state,
                message: action.message,
                code: action.code,
                type: types.MESSAGE_ACTION_ERROR,
            }
        case types.ACCEPT_ADD:
            return {
                ...state,
                message: action.message,
                code: action.code,
                type: types.ACCEPT_ADD,
            }
        case types.REFUSE_ADD:
            return {
                ...state,
                message: action.message,
                code: action.code,
                type: types.REFUSE_ADD,
            }
        case types.CLEAR_MESSAGE_DATA:
            return {
                ...state,
                data: null,
                message: '',
                code: 0
            }
        case types.FRIEND_REQUEST_PAGE_DATA:
            return {
                ...state,
                data: action.data,
                message: action.message,
                code: action.code,
            }
        case types.FRIEND_REQUEST_REFRESH_DATA:
            return {
                ...state,
                data: action.data,
                message: action.message,
                code: action.code,
                isRefreshing: false,
                isNoMore: false,
            }
        case types.FRIEND_REQUEST_MORE_DATA:
            let newList = new Array();
            let newData = action.data;
            let oldData = state.data;
            let isNoMore = false;
            if (oldData.page < newData.page) {
                newList = oldData.list.concat(newData.list);
                newData.list = newList;
            } else {
                newData = oldData;
            }
            if (oldData.total_page == newData.page) {
                isNoMore = true;
            }
            return {
                ...state,
                data: newData,
                message: action.message,
                code: action.code,
                isNoMore: isNoMore,
            }
        case types.FRIEND_REQUEST_REMOVE_DATA:
            let oldFriendRequest = JSON.stringify(state.data);
            let newFriendRequest = JSON.parse(oldFriendRequest);
            let deleteIndex = action.data.index;
            try {
                if (deleteIndex < newFriendRequest.list.length) {
                    let friend = newFriendRequest.list[deleteIndex];
                    if (friend && friend.id == action.data.id) {
                        newFriendRequest.list.splice(deleteIndex, 1);
                        if (newFriendRequest.total_user > 0) {
                            newFriendRequest.total_user = newFriendRequest.total_user - 1;
                        } else {
                            newFriendRequest.total_user = 0;
                        }

                    }
                }
            } catch (err) {

            }
            return {
                ...state,
                data: newFriendRequest,
                message: action.message,
                code: action.code,
            }
        default:
            return state;
    }
}
export default messageReducer;