/**
 * Created by wangxu on 2017/3/29.
 */
import * as types from '../actions/actionTypes';
import giftListData from '../common/giftListData';

const giftLoadTypeCache = 'cache';
let arrayList = new Array();
giftListData.getLocalGiftList(list => {
    arrayList = list;
})
const initialState = {
    gifts: arrayList
}
let giftManifestReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.UPDATE_LOCAL_GIFTS:
            let olderListJson = JSON.stringify(state.gifts);
            let olderList = JSON.parse(olderListJson);
            let updateList = action.data;
            try {
                for (let i = 0; updateList[i]; i += 1) {
                    let newGift = updateList[i];
                    let needUpdate = false;
                    for (let b = 0; olderList[b]; b += 1) {
                        if (olderList[b].type == newGift.type) {
                            olderList[b].gift_name = newGift.gift_name;
                            olderList[b].gift_image = newGift.gift_image;
                            olderList[b].load_type = giftLoadTypeCache;
                            needUpdate = true;
                            break;
                        }
                    }
                    if (!needUpdate) {
                        newGift.load_type = giftLoadTypeCache;
                        olderList.push(newGift);
                    }
                }
            } catch (e) {
                alert(e);
            }
            return {
                ...state,
                gifts: olderList,
            }
        default:
            return state;
    }
}
export default giftManifestReducer;