/**
 * Created by wangxu on 2017/12/7.
 */
import * as types from '../actions/actionTypes';
/*"data": {
 "skip": 0,
 "limit": 10,
 "data": [
 {
 "_id": "5993a281781fb4261145ed00",
 "image": "http://q.qlogo.cn/qqapp/1106115686/7FAFDB4CEB2127AC039A12738B5FB4D4/100",
 "name": "My-Mirror",
 "score": 20001
 },
 {
 "_id": "5930cb2c3eb2ad00074afce7",
 "image": "http://q.qlogo.cn/qqapp/1106115686/DDB2A14B92F6072D2006AFC88BC24A75/100",
 "name": "xchl6",
 "score": 41
 },
 {
 "_id": "599531f852f36315b789b4f8",
 "image": "http://werewolf.bj.bcebos.com/default_avatar/3.png",
 "name": "Tony",
 "score": 1
 }
 ]
 }*/
const initialState = {
    data: null,
}
let rankingReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_RANKING_LIST_DATA:
            return {
                ...state,
                data: action.data,
            }
        case types.GET_RANKING_LIST_DATA_REEOR:
            return {
                ...state
            }
        default:
            return state;
    }
}
export default rankingReducer;