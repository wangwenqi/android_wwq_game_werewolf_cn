/**
 * Created by wangxu on 2017/3/29.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    code: 0,
    message: '',
    data: null,
    isRefreshing: false,
    isNoMore: false,
    updateTime: null,
    pages: 0
}
let friendReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.FRIEND_DATA:
            return {
                ...state,
                data: action.data,
                message: action.message,
                code: action.code,
                isRefreshing: false,
                isNoMore: false,
                updateTime: new Date()
            }
        case types.SYNCHRONIZATION_FRIEND_DATA:
            return {
                ...state,
                data: action.data,
                message: action.message,
                code: action.code,
                isRefreshing: false,
                isNoMore: false,
                pages: action.pages,
                updateTime: new Date()
            }
        case types.FRIEND_PAGE_DATA:
            return {
                ...state,
                data: action.data,
                message: action.message,
                code: action.code,
            }
        case types.FRIEND_REFRESH_DATA:
            return {
                ...state,
                data: action.data,
                message: action.message,
                code: action.code,
                isRefreshing: false,
                isNoMore: false,
            }
        case types.FRIEND_MORE_DATA:
            return {
                ...state,
                data: action.data,
                message: action.message,
                code: action.code,
                isNoMore: action.data.total_page == action.data.page,
            }
        case types.FRIEND_ACTION_ERROR:
            return {
                ...state,
                message: action.message,
                code: action.code,
            }
        case types.FRIEND_DELETE:
            return {
                ...state,
                data: action.data,
                message: action.message,
                code: action.code,
                isRefreshing: false,
                isNoMore: false,
            }
        case types.CLEAR_FRIEND_DATA:
            return {
                ...state,
                data: null,
                code: 0,
                message: '',
            }
        case types.FRIEND_DELETE_ERROR:
            return {
                ...state,
                code: action.code,
                message: action.message,
            }
        default:
            return state;
    }
}
export default friendReducer;