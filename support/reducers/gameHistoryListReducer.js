/**
 * Created by wangxu on 2017/12/7.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    data: null,
    isRefreshing: false,
    isNoMore: false,
}
let gameHistoryListReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_GAME_HISTORY_LIST_DATA:
            return {
                ...state,
                data: action.data,
                isRefreshing: false,
                isNoMore: false,
            }
        case types.GET_GAME_HISTORY_LIST_DATA_ERR:
            return {
                ...state,
                isRefreshing: false,
            }
        case types.GET_GAME_HISTORY_LIST_DATA_MORE:
            //暂时不确定数据
            let newList = new Array();
            let newData = action.data;
            let oldData = state.data;
            let isNoMore = false;
            if (newData && newData.data) {
                if (newData.data.length < newData.limit) {
                    isNoMore = true;
                }
            } else {
                isNoMore = true;
            }
            if (oldData && oldData.data) {
                if (newData && newData.data) {
                    newList = oldData.data.concat(newData.data);
                    newData.data = newList;
                }
            }
            return {
                ...state,
                data: newData,
                isRefreshing: false,
                isNoMore: isNoMore
            }
        case types.GAME_HISTORY_LIST_ISREFRESH:
            return {
                ...state,
                isRefreshing: true
            }
        case types.CLEAR_GAME_HISTORY_LIST_DATA:
            return {
                ...state,
                data: null
            }
        default:
            return state;
    }
}
export default gameHistoryListReducer;