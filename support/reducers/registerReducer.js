/**
 * Created by wangxu on 2017/5/9.
 */
import * as types from '../actions/actionTypes';
const initialState = {
    data: null,
}
let registerReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.REGISTER_DATA:
            return {
                ...state,
                data: action.data,
            }
        default:
            return state;
    }
}
export default registerReducer;