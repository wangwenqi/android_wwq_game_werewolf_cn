/**
 * Created by wangxu on 2017/10/30.
 */
import * as types from './actionTypes';
import {AsyncStorage, Platform} from 'react-native';
import RealmManager from '../common/realmManager';

let familyChatRealm = null;
let familyChatKey = '';
/**
 *
 * @param userid
 * @param newRealm
 * @returns {function(*)}
 */
export const getFamilyChat = (userid, newRealm) => {
    return dispatch => {
        let count = 0;
        try {
            if (userid != '') {
                familyChatKey = userid + RealmManager.FAMILY_KEY;
                familyChatRealm = newRealm;
                let results = familyChatRealm.objects(familyChatKey);
                let requestData;
                if (results[0]) {
                    let dataJson = JSON.stringify(results[0]);
                    requestData = JSON.parse(dataJson);
                }
                dispatch({'type': types.GET_FAMILY_CHAT_DATA, 'data': requestData});
            }
        }
        catch (e) {
            console.log('error:' + JSON.stringify(e));
            //alert(e);
        }
    }
}
/**
 *
 */
export const getNewFamilyChat = (conversation_id) => {
    return dispatch => {
        try {
            if (familyChatRealm && conversation_id) {
                let results = familyChatRealm.objects(familyChatKey).filtered('id==' + '"' + conversation_id + '"');
                let requestData;
                if (results[0]) {
                    let dataJson = JSON.stringify(results[0]);
                    requestData = JSON.parse(dataJson);
                }
                dispatch({'type': types.GET_FAMILY_CHAT_DATA, 'data': requestData});
            }else {
                dispatch({'type': types.GET_FAMILY_CHAT_DATA, 'data': null});
            }
        }
        catch (e) {
            console.log('error:' + JSON.stringify(e));
            //alert(e);
        }
    }
}
/**
 *
 * @param chat
 * "CHAT_MESSAGE": "QQ",
 * "CONVERSATION_ID": "",
 * READ:""
 */
export const updateFamilyChat = (chat) => {
    return dispatch => {
        let count = 0;
        if (familyChatRealm) {
            try {
                familyChatRealm.write(() => {
                    let unRead = unReadMessageCount(chat.READ);
                    let familyConversationId = chat.CONVERSATION_ID;
                    if (familyConversationId) {
                        let message = familyChatRealm.objects(familyChatKey).filtered('id==' + '"' + familyConversationId + '"');
                        if (message.length == 0) {
                            //直接存储消息
                            familyChatRealm.create(familyChatKey, {
                                id: familyConversationId,
                                message: chat.CHAT_MESSAGE,
                                time: new Date(),
                                unreadNumber: unRead,
                            });
                        } else {
                            //更新消息
                            familyChatRealm.create(familyChatKey, {
                                id: familyConversationId,
                                unreadNumber: message[0].unreadNumber + unRead,
                                message: chat.CHAT_MESSAGE,
                                time: new Date(),
                            }, true);
                        }
                        let results = familyChatRealm.objects(familyChatKey).filtered('id==' + '"' + familyConversationId + '"');
                        let requestData;
                        if (results[0]) {
                            let dataJson = JSON.stringify(results[0]);
                            requestData = JSON.parse(dataJson);
                        }
                        dispatch({'type': types.UPDATE_FAMILY_CHAT_DATA, 'data': requestData});
                    }
                });
            } catch (e) {
                console.log(e);
               // alert(e)
            }

        }
    }
}
/**
 *
 * @param read
 * @returns {number}
 */
const unReadMessageCount = (read) => {
    let unRead = 1;
    if (Platform.OS === 'ios') {
        if (read == 1) {
            unRead = 0;
        }
    } else {
        if (read) {
            unRead = 0;
        }
    }
    return unRead;
}
/**
 *
 * @param conversation_id
 * @returns {function(*)}
 */
export const readFamilyChat = (familyConversationId) => {
    return dispatch => {
        if (familyChatRealm && familyConversationId) {
            try {
                familyChatRealm.write(() => {
                    let message = familyChatRealm.objects(familyChatKey).filtered('id==' + '"' + familyConversationId + '"');
                    if (message.length != 0) {
                        familyChatRealm.create(familyChatKey, {id: familyConversationId, unreadNumber: 0}, true);
                    }
                    let results = familyChatRealm.objects(familyChatKey).filtered('id==' + '"' + familyConversationId + '"');
                    let requestData;
                    if (results[0]) {
                        let dataJson = JSON.stringify(results[0]);
                        requestData = JSON.parse(dataJson);
                    }
                    dispatch({'type': types.READ_FAMILY_CHAT_DATA, 'data': requestData});
                });
            } catch (e) {
                //alert(e)
            }

        }
    }

}
/**
 * 移除聊天消息
 * @returns {function(*)}
 */
export const removeFamilyChat = () => {
    return dispatch => {
        dispatch({'type': types.REMOVE_FAMILY_CHAT_DATA});
    }
}
/**
 * 删除
 * @returns {function(*)}
 */
export const deleteFamilyChat = () => {
    return dispatch => {
        try {
            if (familyChatRealm) {
                familyChatRealm.write(() => {
                    // 获取Chat对象
                    let Chats = familyChatRealm.objects(familyChatKey);
                    // 删除
                    if (Chats.length > 0) {
                        familyChatRealm.delete(Chats);
                    }
                });
            }
        } catch (e) {
        }
        dispatch({'type': types.DELETE_FAMILY_CHAT_DATA});
    }
}