/**
 * Created by wangxu on 2017/11/14.
 * 本地支持礼物
 */
import * as types from './actionTypes';
import rnRoNativeUtils from '../common/rnToNativeUtils';
/**
 * 同步出来需要从缓存中取的数据
 * 1。用于支持显示新的礼物类型；
 * 2。用户修改的礼物；
 * @param gifts
 * @returns {function(*)}
 */
export const updateGiftManifest = (gifts) => {
    return dispatch => {
        rnRoNativeUtils.getGiftInfo(gifts, (newGifts) => {
            dispatch({'type': types.UPDATE_LOCAL_GIFTS, 'data': newGifts});
        });
    }
}