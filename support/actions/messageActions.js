/**
 * Created by wangxu on 2017/3/29.
 */
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';
import Language from '../../resources/language';
export function fetchMessageList() {
    return dispatch => {
        let url = apiDefines.GET_MESSAGE_LIST;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.MESSAGE_DATA, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.MESSAGE_ACTION_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.MESSAGE_ACTION_ERROR, 'code': 0, 'message': ''});
        });
    }
}

export function refuseAdd(userID) {
    return dispatch => {
        let url = apiDefines.REJECT_FRIEND + userID;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.REFUSE_ADD, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.MESSAGE_ACTION_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.MESSAGE_ACTION_ERROR, 'code': 0, 'message': Language().not_network});
        });
    }
}
export function acceptAdd(userID) {
    return dispatch => {
        let url = apiDefines.ACCEPT_FRIEND + userID;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.ACCEPT_ADD, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.MESSAGE_ACTION_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.MESSAGE_ACTION_ERROR, 'code': 0, 'message': Language().not_network});
        });
    }
}
export function clearMessageList() {
    return dispatch => {
        dispatch({'type': types.CLEAR_MESSAGE_DATA});
    }
}
export function fetchPageFriendRequestList() {
    return dispatch => {
        let url = apiDefines.GET_FRIEND_REQUEST_LIST;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.FRIEND_REQUEST_PAGE_DATA, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.MESSAGE_ACTION_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.MESSAGE_ACTION_ERROR, 'code': 0, 'message': ''});
        });
    }
}
export function refreshPageFriendRequestList() {
    return dispatch => {
        let url = apiDefines.GET_FRIEND_REQUEST_LIST;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.FRIEND_REQUEST_REFRESH_DATA, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.MESSAGE_ACTION_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.MESSAGE_ACTION_ERROR, 'code': 0, 'message': ''});
        });
    }
}
export function fetchMoreFriendRequest(page) {
    return dispatch => {
        let url = apiDefines.GET_FRIEND_REQUEST_LIST + apiDefines.PAGE + page;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.FRIEND_REQUEST_MORE_DATA, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.MESSAGE_ACTION_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.MESSAGE_ACTION_ERROR, 'code': 0, 'message': ''});
        });
    }
}
export function removeFriendRequest(userID, index) {
    let deleteData = {
        id: userID,
        index: index
    }
    return dispatch => {
        dispatch({
            'type': types.FRIEND_REQUEST_REMOVE_DATA, 'code': 1000, 'message': 'success', 'data': deleteData
        });
    }
}