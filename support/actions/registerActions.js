/**
 * Created by wangxu on 2017/5/9.
 */
import * as types from './actionTypes';
export function saveRegisterData(registerData) {
    return dispatch => {
        dispatch({'type': types.REGISTER_DATA, 'data': registerData});
    }
}