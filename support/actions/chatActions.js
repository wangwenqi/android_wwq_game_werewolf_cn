/**
 * Created by wangxu on 2017/4/25.
 */
import * as types from './actionTypes';
import {AsyncStorage, Platform} from 'react-native';
import AsyncStorageTool from '../common/AsyncStorageTool';
//初始化Realm
let realm = null;
let userId = '';
// {
//     "USER_SEX": "2",
//     "USER_ICON": "http://q.qlogo.cn/qqapp/1106115686/43AEF4932A38678EDBB6921975C1CA91/100",
//     "USER_NAME": "MM",
//     "USER_ID": "59239c7943f4e700077326db",
//     "MESSAGE_TYPE": "MESSAGE_TYPE_CHAT",
//     "CHAT_MESSAGE": "QQ",
//     "CONVERSATION_ID": "",
// }
/**
 * 更新消息
 * @param chat
 * @returns {function(*)}
 */
export function updateChatData(chat) {
    return dispatch => {
        let count = 0;
        if (realm != null) {
            try {
                realm.write(() => {
                    let message = realm.objects(userId).filtered('id==' + '"' + chat.USER_ID + '"');
                    let unRead = 1;
                    if (Platform.OS === 'ios') {
                        if (chat.READ == 1) {
                            unRead = 0;
                        }
                    } else {
                        if (chat.READ) {
                            unRead = 0;
                        }
                    }
                    if (message.length == 0) {
                        realm.create(userId, {
                            id: chat.USER_ID,
                            name: chat.USER_NAME,
                            sex: chat.USER_SEX,
                            image: chat.USER_ICON,
                            message_type: chat.MESSAGE_TYPE,
                            message: chat.CHAT_MESSAGE,
                            time: new Date(),
                            unreadNumber: unRead,
                            conversation_id: chat.CONVERSATION_ID ? chat.CONVERSATION_ID : ''
                        });
                    } else {
                        realm.create(userId, {
                            id: chat.USER_ID,
                            unreadNumber: message[0].unreadNumber + unRead,
                            image: chat.USER_ICON,
                            message: chat.CHAT_MESSAGE,
                            time: new Date(),
                            sex: chat.USER_SEX,
                            conversation_id: chat.CONVERSATION_ID ? chat.CONVERSATION_ID : ''
                        }, true);
                    }
                    let results = realm.objects(userId);
                    if (results.length != 0) {
                        results = results.sorted('time', {'ascending': true});
                        for (var i = 0; i < results.length; i++) {
                            count += results[i].unreadNumber;
                        }
                    }
                    let chatCount = 30;
                    let totalPage = Math.ceil(results.length / chatCount);
                    let data = {
                        list: results.slice(0, chatCount),
                        page: 1,
                        total_page: totalPage
                    }
                    let dataJson = JSON.stringify(data);
                    let requestData = JSON.parse(dataJson);
                    dispatch({'type': types.UPDATE_CHAT_DATA, 'data': requestData, 'count': count});
                });
            } catch (e) {
                console.log(e);
            }

        }
    }
}
/**
 *
 * @param id
 * @returns {function(*)}
 * 删除聊天消息
 */
export function deleteChatData(id) {
    try {
        return dispatch => {
            let count = 0;
            if (userId != null && realm != null) {
                realm.write(() => {
                    // 获取Chat对象
                    let Chat = realm.objects(userId).filtered('id==' + '"' + id + '"');
                    // 删除
                    if (Chat) {
                        realm.delete(Chat);
                    }
                })
                let results = realm.objects(userId);
                if (results.length != 0) {
                    results = results.sorted('time', {'ascending': true});
                    for (var i = 0; i < results.length; i++) {
                        count += results[i].unreadNumber;
                    }
                }
                let chatCount = 30;
                let totalPage = Math.ceil(results.length / chatCount);
                let data = {
                    list: results.slice(0, chatCount),
                    page: 1,
                    total_page: totalPage
                }
                let dataJson = JSON.stringify(data);
                let requestData = JSON.parse(dataJson);
                dispatch({'type': types.DELETE_CHAT_DATA, 'data': requestData, 'count': count});
            }
        }
    } catch (e) {

    }

}
/**
 * 获取所有消息
 * @param userid
 * @returns {function(*)}
 */
export function getChatData(userid, newRealm) {
    return dispatch => {
        let count = 0;
        try {
            if (userid != '') {
                userId = userid;
                realm = newRealm;
                let results = realm.objects(userId);
                if (results.length != 0) {
                    results = results.sorted('time', {'ascending': true});
                    for (var i = 0; i < results.length; i++) {
                        count += results[i].unreadNumber;
                    }
                }
                let chatCount = 30;
                let totalPage = Math.ceil(results.length / chatCount);
                let data = {
                    list: results.slice(0, chatCount),
                    page: 1,
                    total_page: totalPage
                }
                let dataJson = JSON.stringify(data);
                let requestData = JSON.parse(dataJson);
                dispatch({'type': types.GET_CHAT_DATA, 'data': requestData, 'count': count});
            }
        }
        catch (e) {
            console.log('error:' + JSON.stringify(e));
        }
    }
}
/**
 * 清除当前缓存数据
 * @returns {function(*)}
 */
export function removeChat() {
    return dispatch => {
        dispatch({'type': types.REMOVE_CHAT_DATA});
    }
}
/**
 * 标记消息为已读
 * @param id
 * @returns {function(*)}
 */
export function readMessageData(id) {
    return dispatch => {
        let count = 0;
        if (realm && id != '') {
            try {
                realm.write(() => {
                    let message = realm.objects(userId).filtered('id==' + '"' + id + '"');
                    if (message.length != 0) {
                        realm.create(userId, {id: id, unreadNumber: 0}, true);
                    }
                    let results = realm.objects(userId);
                    if (results.length != 0) {
                        results = results.sorted('time', {'ascending': true});
                        for (var i = 0; i < results.length; i++) {
                            count += results[i].unreadNumber;
                        }
                    }
                    let chatCount = 30;
                    let totalPage = Math.ceil(results.length / chatCount);
                    let data = {
                        list: results.slice(0, chatCount),
                        page: 1,
                        total_page: totalPage
                    }
                    let dataJson = JSON.stringify(data);
                    let requestData = JSON.parse(dataJson);
                    dispatch({'type': types.READ_CHAT_DATA, 'data': requestData, 'count': count});
                });
            } catch (e) {
            }

        }
    }
}
/**
 * 分页获取聊天消息列表
 * @param page
 */
export const getChatMessageForPage = (page, actionType) => {
    return dispatch => {
        try {
            if (userId) {
                let results = realm.objects(userId);
                if (results.length != 0) {
                    results = results.sorted('time', {'ascending': true});
                }
                if (results) {
                    let count = 30;
                    let totalPage = Math.ceil(results.length / count);
                    if (page <= totalPage) {
                        count = page * count;
                    } else {
                        count = totalPage * count;
                    }
                    let data = {
                        list: results.slice(0, count),
                        page: page,
                        total_page: totalPage
                    }
                    let dataJson = JSON.stringify(data);
                    let requestData = JSON.parse(dataJson);
                    dispatch({'type': actionType, 'code': 1000, 'message': 'ok', 'data': requestData});
                }
            }

        } catch (e) {
            console.log('分页获取聊天消息列表error' + e);
        }
    }
}
/**
 * 一键阅读所有聊天消息
 */
export const readAllMessage = () => {
    return dispatch => {
        try {
            if (userId && realm) {
                realm.write(() => {
                    let results = realm.objects(userId);
                    if (results.length != 0) {
                        for (let i = 0; results[i]; i += 1) {
                            let chatData = results[i];
                            if (chatData) {
                                realm.create(userId, {
                                    id: chatData.id,
                                    unreadNumber: 0,
                                }, true);
                            }
                        }
                    }
                    if (results.length != 0) {
                        results = results.sorted('time', {'ascending': true});
                    }
                    let chatCount = 30;
                    let totalPage = Math.ceil(results.length / chatCount);
                    let data = {
                        list: results.slice(0, chatCount),
                        page: 1,
                        total_page: totalPage
                    }
                    let dataJson = JSON.stringify(data);
                    let requestData = JSON.parse(dataJson);
                    dispatch({'type': types.READ_CHAT_DATA, 'data': requestData, 'count': 0});
                });
            }
        } catch (e) {
        }
    }
}
export function deleteAllChatData(deleteList) {
    try {
        return dispatch => {
            let count = 0;
            if (userId != null && realm != null) {
                realm.write(() => {
                    for (let i = 0; deleteList[i]; i += 1) {
                        try {
                            let id=deleteList[i];
                            // 获取Chat对象
                            let Chat = realm.objects(userId).filtered('id==' + '"' + id + '"');
                            // 删除
                            if (Chat) {
                                realm.delete(Chat);
                            }
                        }catch (e){
                        }
                    }
                })
                let results = realm.objects(userId);
                if (results.length != 0) {
                    results = results.sorted('time', {'ascending': true});
                    for (var i = 0; i < results.length; i++) {
                        count += results[i].unreadNumber;
                    }
                }
                let chatCount = 30;
                let totalPage = Math.ceil(results.length / chatCount);
                let data = {
                    list: results.slice(0, chatCount),
                    page: 1,
                    total_page: totalPage
                }
                let dataJson = JSON.stringify(data);
                let requestData = JSON.parse(dataJson);
                dispatch({'type': types.DELETE_CHAT_DATA, 'data': requestData, 'count': count});
            }
        }
    } catch (e) {

    }

}