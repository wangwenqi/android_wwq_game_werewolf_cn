/**
 * Created by vellee on 21/02/2017.
 */
//公共部分
export const SCENE_SPLASH = 'splash';
export const SCENE_DELETE_ALL_MESSAGE = 'delete_all_message';
export const SCENE_DELETE_ALL_CONTACT = 'delete_all_contact';
export const SCENE_MINI_GAME_MANAGER = 'mini_game_manager';
export const SCENE_HOUSING_ESTATES = 'housing_estates';
export const SCENE_LIKE_EACH_OTHER = 'like_each_other';
//游戏
export const SCENE_HOME = 'home';
export const SCENE_MESSAGE = 'message';
export const SCENE_APP_MAIN = 'app_main';//
export const SCENE_CONTACT = 'contact';
export const SCENE_MESSAGE_DETAIL = 'message_detail';
export const SCENE_CONTACT_DETAIL = 'contact_detail';
export const SCENE_GAME_RULE = 'game_rule';
export const SCENE_GUIDE = 'guide';
export const SCENE_SETTING = 'setting';
export const SCENE_ADD_CONTACT = 'add_contact';
export const SCENE_EDIT_USER_INFO = 'edit_user_info';
export const SCENE_LOGIN_OTHER = 'login_other';
export const SCENE_REGISTER = 'register';
export const SCENE_CHANGE_PASSWORD = 'change_password';
export const SCENE_REGISTER_USERINFO = 'register_user_info';
export const SCENE_USER_AGREEMENT='user_agreement';
export const SCENE_COUNTRY_LIST='country_list';
export const SCENE_SELECT_COUNTRY='select_country';
export const SCENE_ABOUT='about';
export const SCENE_PHOTO_WALL='photo_wall';
export const SCENE_DEVELOPER='developer';
export const SCENE_WEBVIEW='webview_page';
export const SCENE_NEW_FRIEND='scene_new_friend';
export const SCENE_VOICE_ROOM='scene_voice_room';
export const SCENE_CREATE_GROUP='scene_create_group';
export const SCENE_GROUP_LIST='scene_group_list';
export const SCENE_VOICE_RECOMEND_USER='scene_voice_recomend_user';
export const SCENE_HOUSE_PROPERTY_CARD='house_property_card';
export const SCENE_WEREWOLF_GAME_HOME='werewolf_game_home';