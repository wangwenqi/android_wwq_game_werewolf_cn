/**
 * Created by wangxu on 2017/9/15.
 * 娱乐房间
 */
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';
/**
 * 获取房间中成员
 * @returns {function(*)}
 */
export const fetchRoomUsersList = (roomId) => {
    return dispatch => {
        let url = apiDefines.ROOM_USERS + roomId;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                let roomUsers = [];
                if (data && data.users) {
                    roomUsers = data.users;
                }
                dispatch({'type': types.ROOM_USERS_DATA, code: code, message: message, data: roomUsers});
            } else {
                dispatch({'type': types.ROOM_USERS_DATA_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.ROOM_USERS_DATA_ERROR, 'code': 0, 'message': ''});
        });
    }
}
/**
 *
 * @returns {function(*)}
 */
export const clearRoomUsersList = () => {
    return dispatch => {
        dispatch({'type': types.CLEAR_ROOM_USERS_DATA});
    }
}