/**
 * Created by wangxu on 2017/3/29.
 */
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';
export function fetchSystemMessage() {
    return dispatch => {
        let url = apiDefines.GET_SYSTEM_MESSAGE;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.SYSTEM_MESSAGE_DATA, code: code, data: data});
            } else {
                dispatch({'type': types.SYSTEM_MESSAGE_ACTION_ERROR, code: code});
            }
        }, (failed) => {
            dispatch({'type': types.SYSTEM_MESSAGE_ACTION_ERROR, 'code': 0});
        });
    }
}
export function fetchServerMessage() {
    return dispatch => {
        let url = apiDefines.GET_SEVER_MESSAGE;
        Util.get(url, (code, message, data) => {
            //data":{"text":"欢迎来到终极狼人杀","type":"toast","title":"欢迎","id":3,"time":1}
            if (code == 1000) {
                dispatch({'type': types.SERVER_MESSAGE_DATA, code: code, data: data});
            } else {
                dispatch({'type': types.SERVER_MESSAGE_ACTION_ERROR, code: code});
            }
        }, (failed) => {
            dispatch({'type': types.SERVER_MESSAGE_ACTION_ERROR, 'code': 0});
        });
    }
}
export function promotedData(data) {
    return dispatch => {
        dispatch({'type': types.PROMOTED_DATA, 'data': data});
    }
}