/**
 * Created by wangxu on 2017/3/29.
 */
import Util from '../common/utils';
import RealmManager from '../common/realmManager';
import  * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';
const FRIEND_KEY = 'friend_key';
//初始化Realm
let friendRealm = null;
/**
 * 初始化数据库
 */
export function initFriendRealm(realm) {
    return dispatch => {
        try {
            friendRealm = realm;
            if (friendRealm) {
                let results = friendRealm.objects(FRIEND_KEY);
                let onLineResults = results.filtered('game_status > 0');
                let offLineResults = results.filtered('game_status == 0');
                //results = results.sorted('game_status', {'ascending': true});
                onLineResults = onLineResults.sorted('game_status', {'ascending': true});
                offLineResults = offLineResults.sorted('update_time', {'ascending': false});
                let onLineArray = [];
                let offLineArray = [];
                for (let i = 0; onLineResults[i]; i += 1) {
                    onLineArray.push(onLineResults[i]);
                }
                for (let i = 0; offLineResults[i]; i += 1) {
                    offLineArray.push(offLineResults[i]);
                }
                let allResults = onLineArray;
                if (onLineArray) {
                    allResults = onLineArray.concat(offLineArray);
                }
                if (results) {
                    let count = 10;
                    let totalPage = Math.ceil(results.length / count);
                    let data = {
                        list: allResults.slice(0, count),
                        page: 1,
                        total_page: totalPage
                    }
                    if (data) {
                        let dataJson = JSON.stringify(data);
                        let requestData = JSON.parse(dataJson);
                        dispatch({'type': types.FRIEND_DATA, 'code': 1000, 'message': 'ok', 'data': requestData});
                    }
                }
            }
        } catch (e) {
            //alert(e)
        }

    }
}
/**
 * 分页获取数据
 * @param page
 * @returns {function(*)}
 */
export const getFriendList = (page) => {
    try {
        return dispatch => {
            if (friendRealm) {
                let results = friendRealm.objects(FRIEND_KEY);
                let onLineResults = results.filtered('game_status > 0');
                let offLineResults = results.filtered('game_status == 0');
                onLineResults = onLineResults.sorted('game_status', {'ascending': true});
                offLineResults = offLineResults.sorted('update_time', {'ascending': false});
                let onLineArray = [];
                let offLineArray = [];
                for (let i = 0; onLineResults[i]; i += 1) {
                    onLineArray.push(onLineResults[i]);
                }
                for (let i = 0; offLineResults[i]; i += 1) {
                    offLineArray.push(offLineResults[i]);
                }
                let allResults = onLineArray;
                if (allResults) {
                    allResults = onLineArray.concat(offLineArray);
                }
                //results = results.sorted('game_status', {'ascending': true});
                if (results) {
                    let count = 10;
                    let totalPage = Math.ceil(results.length / count);
                    if (page <= totalPage) {
                        count = page * count;
                    } else {
                        count = totalPage * count;
                    }
                    let data = {
                        list: allResults.slice(0, count),
                        page: page,
                        total_page: totalPage
                    }
                    if (data) {
                        let dataJson = JSON.stringify(data);
                        let requestData = JSON.parse(dataJson);
                        dispatch({'type': types.FRIEND_MORE_DATA, 'code': 1000, 'message': 'ok', 'data': requestData});
                    }
                }
            }
        }
    } catch (e) {
        //alert(e)
    }

}
/**
 * 更新用户在线状态信息
 * @returns {function(*)}
 */
export const updateGameStatus = () => {
    return dispatch => {
        try {
            let newResults = friendRealm.objects(FRIEND_KEY);
            //newResults = newResults.sorted('game_status', {'ascending': true});
            let onLineResults = newResults.filtered('game_status > 0');
            let offLineResults = newResults.filtered('game_status == 0');
            onLineResults = onLineResults.sorted('game_status', {'ascending': true});
            offLineResults = offLineResults.sorted('update_time', {'ascending': false});
            let onLineArray = [];
            let offLineArray = [];
            for (let i = 0; onLineResults[i]; i += 1) {
                onLineArray.push(onLineResults[i]);
            }
            for (let i = 0; offLineResults[i]; i += 1) {
                offLineArray.push(offLineResults[i]);
            }
            let allResults = onLineArray;
            if (allResults) {
                allResults = onLineArray.concat(offLineArray);
            }
            let newData = {
                list: allResults.slice(0, 10),
                page: 1,
                total_page: Math.ceil(newResults.length / 10)
            }
            if (newData) {
                let dataJson = JSON.stringify(newData);
                let requestData = JSON.parse(dataJson);
                dispatch({'type': types.FRIEND_DATA, 'code': 1000, 'message': 'success', 'data': requestData});
            }
        } catch (e) {
            console.log(e);
            //alert(e)
        }
    }
}
/**
 * 插入一条数据
 * @param data
 */
export function insertData(data) {
    return dispatch => {
        try {
            if (data && friendRealm) {
                let friend = friendRealm.objects(FRIEND_KEY).filtered('id==' + '"' + data.id + '"');
                let status = data.status;
                let game_status = 0;
                let room_id = '';
                let updateTime = '';
                let signature = '';
                if (friend.signature) {
                    signature = friend.signature;
                }
                if (status) {
                    let arrayStatus = new Array();
                    arrayStatus = status.split(':');
                    for (let i = 0; i < arrayStatus.length; i++) {
                        if (i == 0) {
                            game_status = parseInt(arrayStatus[i]);
                        } else if (i == 1) {
                            updateTime = arrayStatus[i];
                        } else if (i == 2) {
                            room_id = arrayStatus[i];
                        }
                    }
                } else {
                    status = null
                }
                if (friend.length != 0) {
                    friendRealm.write(() => {
                        friendRealm.create(FRIEND_KEY, {
                            id: data.id,
                            name: data.name ? data.name : '',
                            sex: data.sex ? data.sex : 0,
                            image: data.image ? data.image : '',
                            game: {
                                experience: data.game.experience,
                                win: data.game.win,
                                lose: data.game.lose,
                                escape: data.game.escape ? data.game.escape : 0,
                                level: data.game.level
                            },
                            status: status,
                            room_id: room_id,
                            update_time: updateTime,
                            game_status: game_status,
                            signature: signature
                        }, true);
                    });
                } else {
                    friendRealm.write(() => {
                        friendRealm.create(FRIEND_KEY, {
                            id: data.id,
                            name: data.name ? data.name : '',
                            sex: data.sex ? data.sex : 0,
                            image: data.image ? data.image : '',
                            game: {
                                experience: data.game.experience,
                                win: data.game.win,
                                lose: data.game.lose,
                                escape: data.game.escape ? data.game.escape : 0,
                                level: data.game.level
                            },
                            status: status,
                            room_id: room_id,
                            update_time: updateTime,
                            game_status: game_status,
                            signature: signature
                        });
                    });
                }
                let results = friendRealm.objects(FRIEND_KEY);
                let onLineResults = results.filtered('game_status > 0');
                let offLineResults = results.filtered('game_status == 0');
                onLineResults = onLineResults.sorted('game_status', {'ascending': true});
                offLineResults = offLineResults.sorted('update_time', {'ascending': false});
                let onLineArray = [];
                let offLineArray = [];
                for (let i = 0; onLineResults[i]; i += 1) {
                    onLineArray.push(onLineResults[i]);
                }
                for (let i = 0; offLineResults[i]; i += 1) {
                    offLineArray.push(offLineResults[i]);
                }
                let allResults = onLineArray;
                if (allResults) {
                    allResults = onLineArray.concat(offLineArray);
                }
                if (results) {
                    let count = 10;
                    let totalPage = Math.ceil(results.length / count);
                    let data = {
                        list: allResults.slice(0, count),
                        page: 1,
                        total_page: totalPage
                    }
                    if (data) {
                        let dataJson = JSON.stringify(data);
                        let requestData = JSON.parse(dataJson);
                        dispatch({'type': types.FRIEND_DATA, 'code': 1000, 'message': 'ok', 'data': requestData});

                    }
                }
            }
        } catch (e) {
            console.log(e);
            //TODO 测试
            //alert('好友信息数据库异常：' + e)
        }
    }
}
/**
 * 删除数据 id
 * @returns {function(*)}
 */
export const deleteFriendData = (id) => {
    return dispatch => {
        if (id && friendRealm) {
            let friend = friendRealm.objects(FRIEND_KEY).filtered('id==' + '"' + id + '"');
            friendRealm.write(() => {
                friendRealm.delete(friend);
            });
            let results = friendRealm.objects(FRIEND_KEY);
            //results = results.sorted('game_status', {'ascending': true});
            let onLineResults = results.filtered('game_status > 0');
            let offLineResults = results.filtered('game_status == 0');
            onLineResults = onLineResults.sorted('game_status', {'ascending': true});
            offLineResults = offLineResults.sorted('update_time', {'ascending': false});
            let onLineArray = [];
            let offLineArray = [];
            for (let i = 0; onLineResults[i]; i += 1) {
                onLineArray.push(onLineResults[i]);
            }
            for (let i = 0; offLineResults[i]; i += 1) {
                offLineArray.push(offLineResults[i]);
            }
            let allResults = onLineArray;
            if (allResults) {
                allResults = onLineArray.concat(offLineArray);
            }
            if (results) {
                let count = 10;
                let totalPage = Math.ceil(results.length / count);
                let data = {
                    list: allResults.slice(0, count),
                    page: 1,
                    total_page: totalPage
                }
                if (data) {
                    let dataJson = JSON.stringify(data);
                    let requestData = JSON.parse(dataJson);
                    dispatch({'type': types.FRIEND_DATA, 'code': 1000, 'message': 'ok', 'data': requestData});
                }
            }
        }
    }

}
/*
 * 分页同步数据
 * @param needRemoveOld
 * @param page
 */
export const synchronizationFriendList = (pages) => {
    return dispatch => {
        try {
            if (friendRealm) {
                let newResults = friendRealm.objects(FRIEND_KEY);
                if (newResults) {
                    let onLineResults = newResults.filtered('game_status > 0');
                    let offLineResults = newResults.filtered('game_status == 0');
                    onLineResults = onLineResults.sorted('game_status', {'ascending': true});
                    offLineResults = offLineResults.sorted('update_time', {'ascending': false});
                    let onLineArray = [];
                    let offLineArray = [];
                    for (let i = 0; onLineResults[i]; i += 1) {
                        onLineArray.push(onLineResults[i]);
                    }
                    for (let i = 0; offLineResults[i]; i += 1) {
                        offLineArray.push(offLineResults[i]);
                    }
                    let allResults = onLineArray;
                    if (allResults) {
                        allResults = onLineArray.concat(offLineArray);
                    }
                    let newData = {
                        list: allResults.slice(0, 10),
                        page: 1,
                        total_page: Math.ceil(newResults.length / 10)
                    }
                    if (newData) {
                        let dataJson = JSON.stringify(newData);
                        let requestData = JSON.parse(dataJson);
                        dispatch({
                            'type': types.SYNCHRONIZATION_FRIEND_DATA,
                            'code': 1000,
                            'message': 'success',
                            'data': requestData,
                            'pages': pages
                        });
                    }
                }
            }

        } catch (e) {
            //alert(e)
        }
    }
}
export function fetchPageFriendList() {
    return dispatch => {
        let url = apiDefines.GET_FRIEND_PAGE_LIST;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.FRIEND_PAGE_DATA, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.FRIEND_ACTION_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.FRIEND_ACTION_ERROR, 'code': 0, 'message': ''});
        });
    }
}
export function refreshPageFriendList() {
    return dispatch => {
        let url = apiDefines.GET_FRIEND_PAGE_LIST;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.FRIEND_REFRESH_DATA, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.FRIEND_ACTION_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.FRIEND_ACTION_ERROR, 'code': 0, 'message': ''});
        });
    }
}
export function fetchMoreFriend(page) {
    return dispatch => {
        let url = apiDefines.GET_FRIEND_PAGE_LIST + apiDefines.PAGE + page;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.FRIEND_MORE_DATA, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.FRIEND_ACTION_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.FRIEND_ACTION_ERROR, 'code': 0, 'message': ''});
        });
    }
}
export function clearFriendList() {
    try {
        return dispatch => {
            RealmManager.resetRealmManger()
            dispatch({'type': types.CLEAR_FRIEND_DATA});
        }
    } catch (e) {

    }

}
export function deleteFriend(userID, index) {
    let deleteData = {
        id: userID,
        index: index
    }
    return dispatch => {
        dispatch({
            'type': types.FRIEND_DELETE, 'code': 1000, 'message': 'success', 'data': deleteData
        });
    }
}
/**
 * 删除数据 id
 * @returns {function(*)}
 */
export const deleteAllFriendData = (deleteList) => {
    return dispatch => {
        if (friendRealm) {
            friendRealm.write(() => {
                for (let i = 0; deleteList[i]; i += 1) {
                    try {
                        let id = deleteList[i];
                        let friend = friendRealm.objects(FRIEND_KEY).filtered('id==' + '"' + id + '"');
                        if(friend.length>0){
                            friendRealm.delete(friend);
                        }
                    } catch (e) {
                    }
                }

            });
            let results = friendRealm.objects(FRIEND_KEY);
            let onLineResults = results.filtered('game_status > 0');
            let offLineResults = results.filtered('game_status == 0');
            onLineResults = onLineResults.sorted('game_status', {'ascending': true});
            offLineResults = offLineResults.sorted('update_time', {'ascending': false});
            let onLineArray = [];
            let offLineArray = [];
            for (let i = 0; onLineResults[i]; i += 1) {
                onLineArray.push(onLineResults[i]);
            }
            for (let i = 0; offLineResults[i]; i += 1) {
                offLineArray.push(offLineResults[i]);
            }
            let allResults = onLineArray;
            if (allResults) {
                allResults = onLineArray.concat(offLineArray);
            }
            if (results) {
                let count = 10;
                let totalPage = Math.ceil(results.length / count);
                let data = {
                    list: allResults.slice(0, count),
                    page: 1,
                    total_page: totalPage
                }
                if (data) {
                    let dataJson = JSON.stringify(data);
                    let requestData = JSON.parse(dataJson);
                    dispatch({'type': types.FRIEND_DATA, 'code': 1000, 'message': 'ok', 'data': requestData});
                }
            }
        }
    }

}