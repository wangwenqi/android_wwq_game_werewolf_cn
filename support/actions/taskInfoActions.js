/**
 * Created by wangxu on 2017/10/27.
 * 家族列表
 */
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';
/**
 *
 * @param page
 * @param actionType
 * @returns {function(*)}
 */
/*{
 code: ErrorCode.Success,
 payload: {
 complete: count //已经完成的个数
 }
 }*/
export const taskInfo = () => {
    return dispatch => {
        let url = apiDefines.TASK_INFO;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.TASK_INFO, code: code, message: message, data: data});
            }
        }, (failed) => {
        });
    }
}
/**
 * 移除当前用户有可领取任务提示
 */
export const resetTaskInfo = () => {
    return dispatch => {
        dispatch({'type': types.RESET_TASK_INFO});
    }
}