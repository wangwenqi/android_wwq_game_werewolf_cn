/**
 * Created by wangxu on 2017/3/29.
 */
import * as types from './actionTypes';
import {Platform, NativeModules} from 'react-native';
var main = NativeModules.MainViewController;
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
export function versionData() {
    return dispatch => {
        let version_name = '1.0';
        if (Platform.OS === 'ios') {
            if (main && main.getCurrentVersion) {
                version_name = main.getCurrentVersion((error, version) => {
                    if (!error) {
                        version_name = version;
                        dispatch({'type': types.VERSION_DATA, 'versionName': version_name});
                    }
                });
            }
        } else {
            if (NativeModules.NativeJSModule.appVersion) {
                NativeModules.NativeJSModule.appVersion(version => {
                    version_name = version;
                    dispatch({'type': types.VERSION_DATA, 'versionName': version_name});
                });
            }
        }
    }
}
export function leancloudConfig(config) {
    return dispatch => {
        dispatch({'type': types.LEANCLOUD_CONFIG, 'leancloudConfig': config});
    }
}
export function location(lc) {
    return dispatch => {
        dispatch({'type': types.LOCATION_CONFIG, 'location': lc});
    }
}
export function updateEnterNoviceCount(count) {
    return dispatch => {
        dispatch({'type': types.ENTER_NOVICE_COUNT, 'noviceCount': count});
    }
}
export function setIsNewUser() {
    return dispatch => {
        dispatch({'type': types.NEW_USER_CONFIG});
    }
}
/**\
 *更换为繁体
 * @returns {function(*)}
 */
export function isTraditional() {
    return dispatch => {
        dispatch({'type': types.LANGUAGE_CONFIG});
    }
}
/**\
 *好友列表版本号
 * @returns {function(*)}
 */
export function updateFriendVersion(version) {
    return dispatch => {
        dispatch({'type': types.UPDATE_FRIEND_VERSION, 'friendVersion': version});
    }
}

/**\
 *初始化好友列表版本号
 * @returns {function(*)}
 */
export function initFriendVersion() {
    return dispatch => {
        let url = apiDefines.FRIEND_VERSION;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                if (data) {
                    dispatch({'type': types.UPDATE_FRIEND_VERSION, 'friendVersion': data.version});
                }
            }
        }, (failed) => {
        });
    }
}
export const updateNoticeVer = (notice_ver) => {
    return dispatch => {
        dispatch({'type': types.UPDATE_NOTICE_VER, 'notice_ver': notice_ver});
    }
}