/**
 * Created by wangxu on 2017/3/29.
 */
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';
import AsyncStorageTool from '../common/AsyncStorageTool';
import rnRoNativeUtils from '../common/rnToNativeUtils';
export function userInfo(data) {
    return dispatch => {
        let token = '';
        if (data != null) {
            token = data.token.access_token;
        }
        return dispatch({'type': types.USER_INFO, 'code': 0, 'message': 'ok', 'data': data, 'token': token});
    }

}
export function updateTourist(isTourist) {
    return dispatch => {
        return dispatch({'type': types.IS_TOURIST, 'Tourist': isTourist});
    }
}
export function actionTab(tabName) {
    return dispatch => {
        return dispatch({'type': types.ACTION_TAB, 'TabName': tabName});
    }
}
export function updateRoomId(roomId, password) {
    return dispatch => {
        return dispatch({'type': types.GET_ROOM_ID, 'roomId': roomId, 'password': password});
    }

}
export function getUserInfo(userId) {
    return dispatch => {
        let url = apiDefines.GET_FRIEND_INFO + userId;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.UPDATE_USER_INFO, code: code, message: message, data: data});
                if (data != null) {
                    AsyncStorageTool.updateGameInfo(data);
                    rnRoNativeUtils.initFamilyGroup(data.group);
                }
            } else {
                dispatch({'type': types.USER_INFO_ACTION_ERRO, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.USER_INFO_ACTION_ERRO, 'code': 0, 'message': JSON.stringify(failed)});
        });
    }
}
/**
 * 更新家族信息
 */
export const updateGroupInfo = () => {
    return dispatch => {
        let url = apiDefines.GROUP_INFO;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                let group =null;
                if (data && data.group) {
                    group = data.group;
                }
                dispatch({'type': types.UPDATE_GROUP_INFO, code: code, message: message, data: group});
            } else {
                dispatch({'type': types.UPDATE_GROUP_INFO_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.UPDATE_GROUP_INFO_ERROR, 'code': 0, 'message': JSON.stringify(failed)});
        });
    }
}