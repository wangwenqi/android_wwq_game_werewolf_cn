/**
 * Created by wangxu on 2017/10/9.
 * 好友在玩action
 */
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';
/**
 * 同步整个好友在玩列表
 */
export const fetchFriendStatus = () => {
    return dispatch => {
        let url = apiDefines.GET_FRIEND_STATUSLIST + '2,3,1' + apiDefines.PARAMETER_WITHINFO + 1;
        Util.get(url, (code, message, data) => {
            if (data) {
                dispatch({'type': types.FRIEND_STATUS_DATA, code: code, message: message, data: data.data})
            } else {
                dispatch({'type': types.FRIEND_STATUS_DATA_ERROR, 'code': 1100, 'message': 'error'});
            }
        }, (failed) => {
            dispatch({'type': types.FRIEND_STATUS_DATA_ERROR, 'code': 1100, 'message': 'error'});
        });
    }
}
/**
 * 换一批
 * @param page
 */
export const getMoreFriendStatus = () => {
    return dispatch => {
        dispatch({'type': types.FRIEND_STATUS_MORE_DATA});
    }
}
/**
 *清理用户在线信息
 */
export const removeFriendStatus=()=>{
    return dispatch => {
        dispatch({'type': types.REMOVE_FRIEND_STATUS_DATA});
    }
}