/**
 * Created by wangxu on 2018/1/18.
 * 小游戏相关操作action
 */
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';
/**
 * 获取支持的游戏列表
 * @returns {function(*)}
 */
const defaultItem = {name: "", icon: "", type: "random_mini_game"};

export const supportGameList = () => {
    return dispatch => {
        let url = apiDefines.GAME_LIST;
        let requestList = new Array();
        requestList.push(defaultItem);
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                if (data && data.games) {
                    requestList = requestList.concat(data.games);
                }
                dispatch({'type': types.GET_GAME_LIST_DATA, code: code, message: message, data: requestList});
            } else {
                dispatch({'type': types.GET_GAME_LIST_DATA_ERR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.GET_GAME_LIST_DATA_ERR, 'code': 0, 'message': ''});
        });
    }
}
export const gameHistoryList = (actionType, skip) => {
    return dispatch => {
        let url = apiDefines.MINI_GAME_REECORDS + skip;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': actionType, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.GET_GAME_HISTORY_LIST_DATA_ERR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.GET_GAME_HISTORY_LIST_DATA_ERR, 'code': 0, 'message': ''});
        });
    }
}
export const isRefreshGameHistory = () => {
    return dispatch => {
        dispatch({'type': types.GAME_HISTORY_LIST_ISREFRESH});
    }
}

export const clearGameHistory = () => {
    return dispatch => {
        dispatch({'type': types.CLEAR_GAME_HISTORY_LIST_DATA});
    }
}