/**
 * Created by wangxu on 2017/12/7.
 */
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';

export const getRankingList = () => {
    return dispatch => {
        let url = apiDefines.GET_RANKING_LIST;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.GET_RANKING_LIST_DATA, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.GET_RANKING_LIST_DATA_REEOR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.GET_RANKING_LIST_DATA_REEOR, 'code': 0, 'message': ''});
        });
    }
}