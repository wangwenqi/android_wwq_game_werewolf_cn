/**
 * Created by wangxu on 2017/10/27.
 * 家族列表
 */
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';
/**
 *
 * @param page
 * @param actionType
 * @returns {function(*)}
 */
export const familyGroupList = (page, actionType) => {
    return dispatch => {
        let url = apiDefines.GROUP_LIST + page;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': actionType, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.FAMILY_GROUP_DATA_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.FAMILY_GROUP_DATA_ERROR, 'code': 0, 'message': ''});
        });
    }
}
export const updateFamilyStatus = (id, rowId) => {
    return dispatch => {
        dispatch({'type': types.UPDATE_FAMILY_GROUP_REQUEST, '_id': id, 'rowId': rowId});
    }
}