/**
 * Created by wangxu on 2017/9/15.
 * 娱乐房间
 */
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
import  * as Constant from '../common/constant';
import * as types from './actionTypes';
/**
 * 获取娱乐房间列表
 * @param page
 */
const user = 'user';
const like = 'like';
const pop = 'pop';
export const fetchRecreationPageList = (page, actionType, filter) => {
    return dispatch => {
        let orderBy = user;
        switch (filter) {
            case 1:
                orderBy = pop;
                break;
            case 2:
                orderBy = like;
                break;
            default:
                orderBy = user;
                break;
        }
        let url = apiDefines.VOICE_ROME_PAGE_LIST + page + apiDefines.PARAMETER_ORDERBY + orderBy;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': actionType, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.RECREATION_DATA_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.RECREATION_DATA_ERROR, 'code': 0, 'message': ''});
        });
    }
}
/**
 * 更新当前filter，用于按照不同类型去个更新
 * @param filter
 * @returns {function(*)}
 */
export const updateFilter = (filter) => {
    return dispatch => {
        dispatch({'type': types.RECREATION_DATA_FILTER, 'data': filter});
    }
}
/**
 *  各种类型请求数据
 * @param page
 * @param type
 */
export const fetchRecreationPageListFromType = (page, actionType, type) => {
    let actionError = types.RECREATION_ALL_DATA_ERROR;
    switch (type) {
        case Constant.VOICE_TYPE_LOVE:
            actionError = types.RECREATION_LOVE_DATA_ERROR;
            break;
        case Constant.VOICE_TYPE_CHAT:
            actionError = types.RECREATION_CHAT_DATA_ERROR;
            break;
        case Constant.VOICE_TYPE_SEEK:
            actionError = types.RECREATION_SEEK_DATA_ERROR;
            break;
        case Constant.VOICE_TYPE_FATE:
            actionError = types.RECREATION_FATE_DATA_ERROR;
            break;
        case Constant.VOICE_TYPE_MERRY:
            actionError = types.RECREATION_MERRY_DATA_ERROR;
            break;
        case Constant.VOICE_TYPE_GAME:
            actionError = types.RECREATION_GAME_DATA_ERROR;
            break;
        case Constant.VOICE_TYPE_UNDERCOVER:
            actionError = types.RECREATION_UNDERCOVER_DATA_ERROR;
            break;
        case Constant.VOICE_TYPE_SING:
            actionError = types.RECREATION_SING_DATA_ERROR;
            break;
    }
    return dispatch => {
        let url = apiDefines.VOICE_ROME_PAGE_LIST + page;
        if (type != Constant.VOICE_TYPE_ALL) {
            url = url + apiDefines.PARAMETER_CHILDTYPE + type;
        }
        //console.log('fetchRecreationPageListFromType --->:'+url);
        Util.get(url, (code, message, data) => {
            //console.log('fetchRecreationPageListFromType --->:'+JSON.stringify(data));
            if (code == 1000) {
                //console.log('fetchRecreationPageListFromType --->:'+actionError);
                dispatch({'type': actionType, code: code, message: message, data: data});
            } else {
                //console.log('fetchRecreationPageListFromType --->:'+actionError);
                dispatch({'type': actionError, code: code, message: message});
            }
        }, (failed) => {
            //console.log('fetchRecreationPageListFromType --->:'+actionError);
            dispatch({'type': actionError, 'code': 0, 'message': ''});
        });
    }
}
/**
 * 更新当前语音房间类型
 * @param type
 */
export const updateVoiceType = (type) => {
    return dispatch => {
        dispatch({'type': types.VOICE_ROOM_TYPE, data: type});
    }
}
/**
 * 获取我的房间列表
 * @returns {function(*)}
 */
export const fetchRoomOwnedList = () => {
    return dispatch => {
        let url = apiDefines.ROOM_OWNED_LIST;
        Util.get(url, (code, message, data) => {
            let listData = [];
            if (code == 1000) {
                if (data) {
                    if (data.rooms) {
                        listData = data.rooms;
                    }
                }
                dispatch({'type': types.ROOM_OWNED_DATA, code: code, message: message, data: listData});
            } else {
                dispatch({'type': types.ROOM_OWNED_DATA_ERROR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.ROOM_OWNED_DATA_ERROR, 'code': 0, 'message': ''});
        });
    }
}
export const fetchingRoomOwned = () => {
    return dispatch => {
        dispatch({'type': types.REFRESHING_ROOM_OWNED});
    }
}
export const clearRoomOwnedList = () => {
    return dispatch => {
        dispatch({'type': types.CLEAR_ROOM_OWNED});
    }
}
export const updateHouseOwnedData = (data) => {
    return dispatch => {
        dispatch({'type': types.MY_OWNED_DATA, 'data': data});
    }
}