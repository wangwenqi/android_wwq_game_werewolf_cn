/**
 * Created by wangxu on 2017/12/7.
 */
import Util from '../common/utils';
import  * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';
// {
//     "code": 1000,
//     "data": [
//     {
//         "_id": "59f8335ad84a7e49cf72b589",
//         "sex": 1,
//         "name": "haha",
//         "image": "http://werewolf-image.xiaobanhui.com/Fi6sAwIJJEgHoWMsoVgUaJ3OAgUQ?imageslim",
//         "room": {
//             "roomType": "audio",
//             "roomId": "066393",
//             "childType": "sing"
//         },
//         "pop": 412110
//     },
//     {
//         "_id": "599ec207e91aeae068fc73ef",
//         "name": "hhhhhhhhhhhhhhhh",
//         "sex": 2,
//         "image": "http://werewolf-image.xiaobanhui.com/078aff40-d65a-11e7-97af-5fda91bb3a2d",
//         "room": {
//             "roomType": "audio",
//             "roomId": "085387"
//         },
//         "pop": 483175
//     },
//     {
//         "_id": "5923fcc928696700063f6b61",
//         "sex": 1,
//         "name": "小🍎蘋果",
//         "image": "http://werewolf-image.xiaobanhui.com/FkSH8vydgk6KjgnAmxcpi5-nMhrX",
//         "room": {
//             "roomType": "audio",
//             "roomId": "098176"
//         },
//         "pop": 16545200
//     }
// ]
// }
export const recommendUserList = (actionType, localDataArray) => {
    return dispatch => {
        let localRoomList;
        if (localDataArray) {
            for (let i = 0; localDataArray[i]; i += 1) {
                let localData = localDataArray[i];
                if (localData&&localData.room) {
                    if (localRoomList) {
                        localRoomList += ',' + localData.room.roomId;
                    } else {
                        localRoomList = localData.room.roomId;
                    }
                }
            }
        }
        let url = apiDefines.GET_RECOMMEND_USER;
        if (localRoomList) {
            url = url + localRoomList;
        }
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': actionType, code: code, message: message, data: data});
            } else {
                dispatch({'type': types.GET_RECOMMEND_LIST_DATA_ERR, code: code, message: message});
            }
        }, (failed) => {
            dispatch({'type': types.GET_RECOMMEND_LIST_DATA_ERR, 'code': 0, 'message': ''});
        });
    }
}