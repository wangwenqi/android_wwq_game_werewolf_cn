/**
 * Created by wangxu on 2017/6/6.
 */
import Util from '../common/utils';
import * as apiDefines from '../common/gameApiDefines';
import * as types from './actionTypes';
import {Platform} from 'react-native';
import rnToNativeUtils from '../common/rnToNativeUtils';

export function checkOpenRegister() {
    return dispatch => {
        let platform = 'android'
        if (Platform.OS === 'ios') {
            platform = 'ios'
        }
        let url = apiDefines.GET_OTHER_LOGIN_VISIBLE + platform;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                dispatch({'type': types.CHECK_OPEN_REGISTER, code: code, data: data});
                let cnw_upgrade = false;
                if (data.cnw_upgrade) {
                    cnw_upgrade = true;
                }
                let configData = {mg_upgrade: cnw_upgrade};
                let NativeData = {
                    action: 'ACTION_SHOW_MINI_GAME_CONFIG',
                    params: configData,
                }
                rnToNativeUtils.sendMessageToNative(JSON.stringify(NativeData));
            } else {
                dispatch({'type': types.CHECK_OPEN_REGISTER_ERROR, code: code});
            }
        }, (failed) => {
            dispatch({'type': types.CHECK_OPEN_REGISTER_ERROR, 'code': 0});
        });
    }
}