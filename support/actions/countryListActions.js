/**
 * Created by wangxu on 2017/5/16.
 */
import * as types from './actionTypes';
import DeviceInfo from 'react-native-device-info';
import Language from '../../resources/language'

let country = DeviceInfo.getDeviceCountry();
let CN = 'CN';//中国大陆
let HK = 'HK';//香港
let TW = 'TW';//台湾
let SG = 'SG';//新加坡
let MY = 'MY';//马来西亚
let CH_CODE = '86';
let CH_NAME = Language().china;
let HK_CODE = '852'
let HK_NAME = Language().hongkong;
let TW_CODE = '886'
let TW_NAME = Language().taiwan;
let SG_CODE = '65'
let SG_NAME = Language().singapore;
let MY_CODE = '60'
let MY_NAME = Language().malaysia;
export function selectedCountry(countryInfo) {
    return dispatch => {
        dispatch({'type': types.SELECTED_COUNTRY, 'data': countryInfo});
    }
}
export function initCountry() {
    let countryInfo = {
        c: TW_CODE,
        n: TW_NAME
    }
    switch (country) {
        case CN:
            countryInfo.c = CH_CODE;
            countryInfo.n = CH_NAME;
            break;
        case HK:
            countryInfo.c = HK_CODE;
            countryInfo.n = HK_NAME;
            break;
        case TW:
            countryInfo.c = TW_CODE;
            countryInfo.n = TW_NAME;
            break;
        case SG:
            countryInfo.c = SG_CODE;
            countryInfo.n = SG_NAME;
            break;
        case MY:
            countryInfo.c = MY_CODE;
            countryInfo.n = MY_NAME;
            break;
        default:
            countryInfo.c = TW_CODE;
            countryInfo.n = TW_NAME;
            break;
    }
    return dispatch => {
        dispatch({'type': types.INIT_COUNTRY, 'data': countryInfo});
    }
}