/**
 * Created by wangxu on 2017/3/29.
 */
'use strict';
// action 类型
//消息
export const MESSAGE_ACTION_ERROR = 'message_action_error';
export const MESSAGE_DATA = 'message_data';
export const CLEAR_MESSAGE_DATA = 'clear_message_data';
//系统消息
export const SYSTEM_MESSAGE_ACTION_ERROR = 'system_message_action_error';
export const SYSTEM_MESSAGE_DATA = 'system_message_data';
export const SERVER_MESSAGE_DATA = 'server_message_data';
export const SERVER_MESSAGE_ACTION_ERROR = 'server_message_action_error';
//是否需要显示要好评弹框
export const PROMOTED_DATA = 'promoted_data';

//拒绝添加好友
export const REFUSE_ADD = 'refuse_add';
//同意添加好友
export const ACCEPT_ADD = 'accept_add';
//好友
export const FRIEND_DATA = 'friend_data';
export const SYNCHRONIZATION_FRIEND_DATA = 'synchronization_friend_data';
export const FRIEND_PAGE_DATA = 'friend_page_data';
export const FRIEND_REFRESH_DATA = 'friend_refresh_data';
export const FRIEND_MORE_DATA = 'friend_more_data';
export const CLEAR_FRIEND_DATA = 'clear_friend_data';
export const FRIEND_ACTION_ERROR = 'friend_action_error';
export const FRIEND_DELETE_ERROR = 'friend_delete_error';
export const FRIEND_DELETE = 'friend_delete';
//home
export const UPDATE_USER_INFO = 'update_user_info';
//更新家族信息
export const UPDATE_GROUP_INFO = 'update_group_info';
export const UPDATE_GROUP_INFO_ERROR = 'update_group_info_error';
export const GET_ROOM_ID = 'get_room_id';
//user
export const USER_INFO = 'user_info';
export const USER_INFO_ACTION_ERRO = 'user_info_action_erro';
export const IS_TOURIST = 'is_tourist';
export const ACTION_HOME = 'action_home';
export const ACTION_MESSAGE = 'action_message';
export const ACTION_CONTACT = 'action_contact';
export const ACTION_RECREATION = 'action_recreation';
export const ACTION_ME = 'action_me';
export const ACTION_TAB = 'action_tab';
//聊天消息
export const GET_CHAT_DATA = 'get_chat_data';
export const UPDATE_CHAT_DATA = 'update_chat_data';
export const REMOVE_CHAT_DATA = 'remove_chat_data';
export const READ_CHAT_DATA = 'read_chat_data';
export const DELETE_CHAT_DATA = 'delete_chat_data';
export const GET_UNREAD_CHAT_DATA = 'get_unread_chat_data';
export const GET_MORE_CHAT_DATA = 'get_more_chat_data';
export const REFRESH_CHAT_DATA = 'refresh_chat_data';
//家族聊天消息
export const GET_FAMILY_CHAT_DATA = 'get_family_chat_data';
export const UPDATE_FAMILY_CHAT_DATA = 'update_family_chat_data';
export const REMOVE_FAMILY_CHAT_DATA = 'remove_family_chat_data';
export const READ_FAMILY_CHAT_DATA = 'read_family_chat_data';
export const DELETE_FAMILY_CHAT_DATA = 'delete_family_chat_data';
//游戏进行状态
export const UPDATE_GAME_STATUS = 'update_game_status';
//注册信息
export const REGISTER_DATA = 'register_data'
//更新国家列表
export const SELECTED_COUNTRY = 'selected_country';
export const INIT_COUNTRY = 'init_country';
//是否开放注册功能
export const CHECK_OPEN_REGISTER = 'check_open_register'
export const CHECK_OPEN_REGISTER_ERROR = 'check_open_register_error'
//config
export const VERSION_DATA = 'version_data';
export const LEANCLOUD_CONFIG = 'leancloud_config'
export const LOCATION_CONFIG = 'location_config'
export const ENTER_NOVICE_COUNT = 'enter_novice_count'
export const NEW_USER_CONFIG = 'new_user_config'
export const LANGUAGE_CONFIG = 'language_config'
//
export const FRIEND_REQUEST_PAGE_DATA = 'friend_request_page_data';
export const FRIEND_REQUEST_REFRESH_DATA = 'friend_request_refresh_data';
export const FRIEND_REQUEST_MORE_DATA = 'friend_request_more_data';
export const FRIEND_REQUEST_REMOVE_DATA = 'friend_request_remove_data';

//娱乐房间
export const RECREATION_DATA = 'recreation_data';
export const RECREATION_REFRESH_DATA = 'recreation_refresh_data';
export const RECREATION_MORE_DATA = 'recreation_more_data';
export const RECREATION_DATA_ERROR = 'recreation_data_error';
export const RECREATION_DATA_FILTER = 'recreation_data_filter';
//我的房间列表
export const ROOM_OWNED_DATA = 'room_owned_data';
export const REFRESHING_ROOM_OWNED = 'refreshing_room_owned';
export const CLEAR_ROOM_OWNED = 'clear_room_owned';
export const ROOM_OWNED_DATA_ERROR = 'room_owned_data_error';
export const MY_OWNED_DATA= 'my_owned_data';
//房间用户列表
export const ROOM_USERS_DATA = 'room_users_data';
export const CLEAR_ROOM_USERS_DATA = 'clear_room_users_data';
export const ROOM_USERS_DATA_ERROR = 'room_users_data_error';
//
export const UPDATE_FRIEND_VERSION = 'update_friend_version';
export const UPDATE_NOTICE_VER = 'update_notice_ver';
//好友在玩信息
export const FRIEND_STATUS_DATA='friend_status_data';
export const REMOVE_FRIEND_STATUS_DATA='remove_friend_status_data';
export const FRIEND_STATUS_MORE_DATA='friend_status_more_data';
export const FRIEND_STATUS_DATA_ERROR='friend_status_data_error';
//家族列表
export const FAMILY_GROUP_DATA='family_group_data';
export const FAMILY_GROUP_DATA_ERROR='family_group_data_error';
export const FAMILY_GROUP_DATA_MORE='family_group_data_more';
export const UPDATE_FAMILY_GROUP_REQUEST='update_family_group_request';
//已完成的任务
export const TASK_INFO='task_info';
export const RESET_TASK_INFO='reset_task_info';
//礼物列表同步
export const UPDATE_LOCAL_GIFTS='update_local_gifts';
//独立应用房间列表
//娱乐房间
export const RECREATION_SING_DATA = 'recreation_sing_data';
export const RECREATION_SING_REFRESH_DATA = 'recreation_sing_refresh_data';
export const RECREATION_SING_MORE_DATA = 'recreation_sing_more_data';
export const RECREATION_SING_DATA_ERROR = 'recreation_sing_data_error';
//
export const RECREATION_CHAT_DATA = 'recreation_chat_data';
export const RECREATION_CHAT_REFRESH_DATA = 'recreation_chat_refresh_data';
export const RECREATION_CHAT_MORE_DATA = 'recreation_chat_more_data';
export const RECREATION_CHAT_DATA_ERROR = 'recreation_chat_data_error';
//
export const RECREATION_LOVE_DATA = 'recreation_love_data';
export const RECREATION_LOVE_REFRESH_DATA = 'recreation_love_refresh_data';
export const RECREATION_LOVE_MORE_DATA = 'recreation_love_more_data';
export const RECREATION_LOVE_DATA_ERROR = 'recreation_love_data_error';
//
export const RECREATION_FATE_DATA = 'recreation_fate_data';
export const RECREATION_FATE_REFRESH_DATA = 'recreation_fate_refresh_data';
export const RECREATION_FATE_MORE_DATA = 'recreation_fate_more_data';
export const RECREATION_FATE_DATA_ERROR = 'recreation_fate_data_error';
//
export const RECREATION_MERRY_DATA = 'recreation_merry_data';
export const RECREATION_MERRY_REFRESH_DATA = 'recreation_merry_refresh_data';
export const RECREATION_MERRY_MORE_DATA = 'recreation_merry_more_data';
export const RECREATION_MERRY_DATA_ERROR = 'recreation_merry_data_error';
//
export const RECREATION_SEEK_DATA = 'recreation_seek_data';
export const RECREATION_SEEK_REFRESH_DATA = 'recreation_seek_refresh_data';
export const RECREATION_SEEK_MORE_DATA = 'recreation_seek_more_data';
export const RECREATION_SEEK_DATA_ERROR = 'recreation_seek_data_error';
//
export const RECREATION_GAME_DATA = 'recreation_game_data';
export const RECREATION_GAME_REFRESH_DATA = 'recreation_game_refresh_data';
export const RECREATION_GAME_MORE_DATA = 'recreation_game_more_data';
export const RECREATION_GAME_DATA_ERROR = 'recreation_game_data_error';
//
export const RECREATION_UNDERCOVER_DATA = 'recreation_undercover_data';
export const RECREATION_UNDERCOVER_REFRESH_DATA = 'recreation_undercover_refresh_data';
export const RECREATION_UNDERCOVER_MORE_DATA = 'recreation_undercover_more_data';
export const RECREATION_UNDERCOVER_DATA_ERROR = 'recreation_undercover_data_error';
//
export const RECREATION_ALL_DATA = 'recreation_all_data';
export const RECREATION_ALL_REFRESH_DATA = 'recreation_all_refresh_data';
export const RECREATION_ALL_MORE_DATA = 'recreation_all_more_data';
export const RECREATION_ALL_DATA_ERROR = 'recreation_all_data_error';
//
export const VOICE_ROOM_TYPE = 'voice_room_type';
//排行榜
export const GET_RANKING_LIST_DATA = 'get_ranking_list_data';
export const GET_RANKING_LIST_DATA_REEOR = 'get_ranking_list_data_reeor';
//运营页面
export const GET_OPERA_LIST_DATA = 'get_opera_list_data';
export const GET_OPERA_LIST_DATA_REEOR = 'get_opera_list_data_reeor';
//推荐有趣的人
export const GET_MORE_RECOMMEND_LIST_DATA='get_more_recommend_list_data';//获取更多
export const GET_RECOMMEND_LIST_DATA='get_recommend_list_data';//重置
export const GET_RECOMMEND_LIST_DATA_ERR='get_recommend_list_data_err';
//支持的小游戏
export const GET_GAME_LIST_DATA='get_game_list_data';
export const GET_GAME_LIST_DATA_ERR='get_game_list_data_err';
//对战列表
export const GET_GAME_HISTORY_LIST_DATA='get_game_history_list_data';
export const GAME_HISTORY_LIST_ISREFRESH='game_history_list_isrefresh';
export const GET_GAME_HISTORY_LIST_DATA_ERR='get_game_history_list_data_err';
export const GET_GAME_HISTORY_LIST_DATA_MORE='get_game_history_list_data_more';
export const CLEAR_GAME_HISTORY_LIST_DATA='clear_game_history_list_data';
//like 列表
export const GET_RECOMMEND_LIKE_LIST_DATA='get_recommend_like_list_data';
export const GET_RECOMMEND_LIKE_LIST_DATA_ERR='get_recommend_like_list_data_err';