'use strict';


import {applyMiddleware, createStore, compose} from 'redux';
import {composeWithDevTools} from 'remote-redux-devtools';
import thunk from 'redux-thunk';
import {persistStore, autoRehydrate} from 'redux-persist';
import reducers from '../reducers';

const logger = store => next => action => {
    if (typeof action === 'function') {
        //console.log('dispatching a function');
    } else {
        //打印数据传递的信息
        //console.log('dispatching', action);
    }
    let result = next(action);
    //console.log('next state', store.getState());
    return result;
}

let middlewares = [
    logger,
    thunk
];


export default function configureStore(onComplete: ()=>void) {


    const store = createStore(reducers, composeWithDevTools(
        applyMiddleware(...middlewares)
    ));
    // onComplete();
    return store;
    // const store = autoRehydrate()(createAppStore)(reducers);
    // let opt = {
    // 	storage: AsyncStorage,
    // 	transform: [],
    // 	//whitelist: ['userStore'],
    // };
    // persistStore(store, opt, onComplete);
    // return store;
}


