/**
 * Created by wangxu on 2017/6/1.
 */
'use strict';

import React, {Component} from 'react';
import Color from '../../resources/themColor';
import Language from '../../resources/language';
import {connect} from 'react-redux';
import CachedImage from '../../support/common/CachedImage';

import {
    StyleSheet, View, TouchableOpacity, Text, Image, Platform, Dimensions, ScrollView
} from 'react-native';
const ScreenWidth = Dimensions.get('window').width;
const lines = require('../../resources/imgs/ic_lines.png');
const frameL = require('../../resources/imgs/ic_frame_l.png');
const frameR = require('../../resources/imgs/ic_frame_r.png');
const roleWolf = require('../../resources/imgs/role_wolf.png');
const cupid = require('../../resources/imgs/role_cupid.png');//爱神
const hunter = require('../../resources/imgs/role_hunter.png');//猎人
const people = require('../../resources/imgs/role_people.png');//好人
const seek = require('../../resources/imgs/role_seek.png');//预言家
const sergeant = require('../../resources/imgs/role_sergeant.png');//警长
const witch = require('../../resources/imgs/role_witch.png');//女巫
const wolf = require('../../resources/imgs/role_wolf.png');//狼人
const king_wolf = require('../../resources/imgs/role_king_wolf.png');//白狼王
const guard = require('../../resources/imgs/role_guard.png');//守卫
const magician = require('../../resources/imgs/role_magician.png');//魔术师
const evilSpirit = require('../../resources/imgs/role_evil_spirit.png');//恶魔
class GameRole extends Component {
    componentDidMount() {
    }

    render() {
        return (
            <View style={styles.view}>
                <ScrollView alwaysBounceVertical={false} showsVerticalScrollIndicator={false}>
                    <View flexDirection='row'>
                        <CachedImage style={styles.lines} source={lines}/>
                        <Text
                            style={{flex:1,alignSelf:'center',fontSize:15,color:Color.colorWhite,textAlign:'center',fontWeight: 'bold'}}>{Language().werewolf_camp}</Text>
                        <CachedImage style={styles.lines} source={lines}/>
                    </View>
                    <View style={styles.itemView}>
                        <CachedImage style={styles.frameL} source={frameL}/>
                        <View flexDirection='row'>
                            <View style={{flex:1.5,justifyContent:'center'}}>
                                <CachedImage style={styles.roleImage} source={roleWolf}/>
                            </View>
                            <View style={{flex:3}}>
                                <Text style={{color:Color.colorWhite,fontSize:14}}>{Language().werewolf}</Text>
                                    <View flexDirection='row' style={{marginRight:10}}>
                                        <Text style={{fontSize:12,color:'#a2eaff'}}>{Language().jineng}：</Text>
                                        <Text style={{flex:1,fontSize:12,color:'#a2eaff'}}>{Language().werewolf_skills}</Text>
                                    </View>
                            </View>
                        </View>
                        <CachedImage style={styles.frameR} source={frameR}/>
                    </View>
                    <View style={styles.itemView}>
                        <CachedImage style={styles.frameL} source={frameL}/>
                        <View flexDirection='row'>
                            <View style={{flex:1.5,justifyContent:'center'}}>
                                <CachedImage style={styles.roleImage} source={king_wolf}/>
                            </View>
                            <View style={{flex:3}}>
                                <Text style={{color:Color.colorWhite,fontSize:14}}>{Language().role_high_king}</Text>
                                <View flexDirection='row' style={{marginRight:10}}>
                                    <Text style={{fontSize:12,color:'#a2eaff'}}>{Language().jineng}：</Text>
                                    <Text style={{flex:1,fontSize:12,color:'#a2eaff'}}>{Language().high_king_skills}</Text>
                                </View>
                            </View>
                        </View>
                        <CachedImage style={styles.frameR} source={frameR}/>
                    </View>
                    <View flexDirection='row'>
                        <CachedImage style={styles.lines} source={lines}/>
                        <Text
                            style={{flex:1.5,alignSelf:'center',fontSize:15,color:Color.colorWhite,textAlign:'center',fontWeight: 'bold'}}>{Language().people_camp}</Text>
                        <CachedImage style={styles.lines} source={lines}/>
                    </View>
                    <View style={styles.itemView}>
                        <CachedImage style={styles.frameL} source={frameL}/>
                        <View flexDirection='row'>
                            <View style={{flex:1.5,justifyContent:'center'}}>
                                <CachedImage style={styles.roleImage} source={seek}/>
                            </View>
                            <View style={{flex:3}}>
                                <Text style={{color:Color.colorWhite,fontSize:14}}>{Language().seer}</Text>
                                <View flexDirection='row' style={{marginRight:10}}>
                                    <Text style={{fontSize:12,color:'#a2eaff'}}>{Language().jineng}：</Text>
                                    <Text style={{flex:1,fontSize:12,color:'#a2eaff'}}>{Language().seer_skills}</Text>
                                </View>
                            </View>
                        </View>
                        <CachedImage style={styles.frameR} source={frameR}/>
                    </View>
                    <View style={styles.itemView}>
                        <CachedImage style={styles.frameL} source={frameL}/>
                        <View flexDirection='row'>
                            <View style={{flex:1.5,justifyContent:'center'}}>
                                <Image style={styles.roleImage} source={witch}/>
                            </View>
                            <View style={{flex:3}}>
                                <Text style={{color:Color.colorWhite,fontSize:14}}>{Language().witch}</Text>
                                <View flexDirection='row' style={{marginRight:10}}>
                                    <Text style={{fontSize:12,color:'#a2eaff'}}>{Language().jineng}：</Text>
                                    <Text style={{flex:1,fontSize:12,color:'#a2eaff'}}>{Language().witch_skills}</Text>
                                </View>
                            </View>
                        </View>
                        <CachedImage style={styles.frameR} source={frameR}/>
                    </View>
                    <View style={styles.itemView}>
                        <CachedImage style={styles.frameL} source={frameL}/>
                        <View flexDirection='row'>
                            <View style={{flex:1.5,justifyContent:'center'}}>
                                <CachedImage style={styles.roleImage} source={hunter}/>
                            </View>
                            <View style={{flex:3}}>
                                <Text style={{color:Color.colorWhite,fontSize:14}}>{Language().hunter}</Text>
                                <View flexDirection='row' style={{marginRight:10}}>
                                    <Text style={{fontSize:12,color:'#a2eaff'}}>{Language().jineng}：</Text>
                                    <Text style={{flex:1,fontSize:12,color:'#a2eaff'}}>{Language().hunter_skills}</Text>
                                </View>
                            </View>
                        </View>
                        <CachedImage style={styles.frameR} source={frameR}/>
                    </View>
                    <View style={styles.itemView}>
                        <CachedImage style={styles.frameL} source={frameL}/>
                        <View flexDirection='row'>
                            <View style={{flex:1.5,justifyContent:'center'}}>
                                <CachedImage style={styles.roleImage} source={people}/>
                            </View>
                            <View style={{flex:3}}>
                                <Text style={{color:Color.colorWhite,fontSize:14}}>{Language().people}</Text>
                                <View flexDirection='row' style={{marginRight:10}}>
                                    <Text style={{fontSize:12,color:'#a2eaff'}}>{Language().jineng}：</Text>
                                    <Text style={{flex:1,fontSize:12,color:'#a2eaff'}}>{Language().people_skills}</Text>
                                </View>
                            </View>
                        </View>
                        <CachedImage style={styles.frameR} source={frameR}/>
                    </View>
                    <View style={styles.itemView}>
                        <CachedImage style={styles.frameL} source={frameL}/>
                        <View flexDirection='row'>
                            <View style={{flex:1.5,justifyContent:'center'}}>
                                <CachedImage style={styles.roleImage} source={guard}/>
                            </View>
                            <View style={{flex:3}}>
                                <Text style={{color:Color.colorWhite,fontSize:14}}>{Language().guard}</Text>
                                <View flexDirection='row' style={{marginRight:10}}>
                                    <Text style={{fontSize:12,color:'#a2eaff'}}>{Language().jineng}：</Text>
                                    <Text style={{flex:1,fontSize:12,color:'#a2eaff'}}>{Language().guard_skills}</Text>
                                </View>
                            </View>
                        </View>
                        <CachedImage style={styles.frameR} source={frameR}/>
                    </View>
                    <View style={styles.itemView}>
                        <CachedImage style={styles.frameL} source={frameL}/>
                        <View flexDirection='row'>
                            <View style={{flex:1.5,justifyContent:'center'}}>
                                <CachedImage style={styles.roleImage} source={cupid}/>
                            </View>
                            <View style={{flex:3}}>
                                <Text style={{color:Color.colorWhite,fontSize:14}}>{Language().cupid}</Text>
                                <View flexDirection='row' style={{marginRight:10}}>
                                    <Text style={{fontSize:12,color:'#a2eaff'}}>{Language().jineng}：</Text>
                                    <Text style={{flex:1,fontSize:12,color:'#a2eaff'}}>{Language().cupid_skills}</Text>
                                </View>
                                <View flexDirection='row' style={{marginRight:10}}>
                                    <Text style={{fontSize:12,color:'#a2eaff'}}>{Language().victory_means}：</Text>
                                    <Text style={{flex:1,fontSize:12,color:'#a2eaff'}}>{Language().cupid_victory_means}</Text>
                                </View>
                            </View>
                        </View>
                        <CachedImage style={styles.frameR} source={frameR}/>
                    </View>
                    <View style={styles.itemView}>
                        <CachedImage style={styles.frameL} source={frameL}/>
                        <View flexDirection='row'>
                            <View style={{flex:1.5,justifyContent:'center'}}>
                                <CachedImage style={styles.roleImage} source={magician}/>
                            </View>
                            <View style={{flex:3}}>
                                <Text style={{color:Color.colorWhite,fontSize:14}}>{Language().magician}</Text>
                                <View flexDirection='row' style={{marginRight:10}}>
                                    <Text style={{fontSize:12,color:'#a2eaff'}}>{Language().jineng}：</Text>
                                    <Text style={{flex:1,fontSize:12,color:'#a2eaff'}}>{Language().magician_skills}</Text>
                                </View>
                            </View>
                        </View>
                        <CachedImage style={styles.frameR} source={frameR}/>
                    </View>
                    <View style={styles.itemView}>
                        <CachedImage style={styles.frameL} source={frameL}/>
                        <View flexDirection='row'>
                            <View style={{flex:1.5,justifyContent:'center'}}>
                                <CachedImage style={styles.roleImage} source={evilSpirit}/>
                            </View>
                            <View style={{flex:3}}>
                                <Text style={{color:Color.colorWhite,fontSize:14}}>{Language().evilSpirit}</Text>
                                <View flexDirection='row' style={{marginRight:10}}>
                                    <Text style={{fontSize:12,color:'#a2eaff'}}>{Language().jineng}：</Text>
                                    <Text style={{flex:1,fontSize:12,color:'#a2eaff'}}>{Language().evilSpirit_skills}</Text>
                                </View>
                            </View>
                        </View>
                        <CachedImage style={styles.frameR} source={frameR}/>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        margin: 15,
        borderRadius: 3,
        backgroundColor: '#6614bc',
        flex: 1, padding: 5
    },
    lines: {
        height: 5,
        resizeMode: 'contain',
        flex: 1, alignSelf: 'center'
    },
    itemView: {
        left: 0,
        top: 0,
        bottom: 0,
        right: 0,
        borderRadius: 5,
        backgroundColor: '#450d85',
        margin: 10,
        padding:5
    },
    frameR: {
        height: 13,
        width: 13,
        resizeMode: 'contain',
        position: 'absolute',
        bottom:0,
        right:0
    },
    frameL: {
        height: 13,
        width: 13,
        resizeMode: 'contain',
        position: 'absolute',
        top:0,
        left:0
    },
    roleImage: {
        height: 65,
        width: 75,
        resizeMode: 'contain',
        alignSelf: 'center',
        margin:5
    },
    contentView: {
        position: 'absolute',
        left: 0,
        top: 0,
        bottom: 0,
        right: 0,
    },
});

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps,)(GameRole);
