/**
 * Created by wangxu on 2017/6/1.
 */
'use strict';

import React, {Component} from 'react';
import Color from '../../resources/themColor';
import Badge from 'react-native-smart-badge';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {
    StyleSheet, View, TouchableOpacity, Text, Image, Platform, Dimensions, InteractionManager
} from 'react-native';
import * as Constant from '../../support/common/constant';
import * as SCENES from '../../support/actions/scene';
import ImageManager from '../../resources/imageManager';
import Language from '../../resources/language';
import Dropdown from '../../support/common/Dropdown';
import CachedImage from '../../support/common/CachedImage';
import {readFamilyChat} from '../../support/actions/groupFamilyChatActions';
import {readAllMessage} from '../../support/actions/chatActions';

const ScreenWidth = Dimensions.get('window').width;
const tabIconWidth = (Platform.OS === 'ios') ? 23 : 21;
const tabIconHeight = (Platform.OS === 'ios') ? 23 : 21;
const ContactMenuData = [{
    key: Constant.DELETE,
    value: Language().batch_delete,
    img: ImageManager.ic_delete_bg
}];
const messageMenuData = [{key: Constant.READ, value: Language().read_all_message, img: ImageManager.ic_read_message}, {
    key: Constant.DELETE,
    value: Language().batch_delete,
    img: ImageManager.ic_delete_bg
}];
const {pushRoute, popRoute} = actions;
let MenuData = ContactMenuData;
let unreadMessageCount = 0;
class MessageTabBar extends Component {

    propTypes = {
        goToPage: React.PropTypes.func, // 跳转到对应tab的方法
        activeTab: React.PropTypes.number, // 当前被选中的tab下标
        tabs: React.PropTypes.array, // 所有tabs集合
        tabNames: React.PropTypes.array, // 保存Tab名称
        unReadMessageNumber: React.PropTypes.number,
        friendRequestListData: React.PropTypes.object,
        familyChatData: React.PropTypes.object,
        openPage: React.PropTypes.func,
        readAllMessage: React.PropTypes.func,
        readFamilyChat: React.PropTypes.func,
        userInfo: React.PropTypes.object,
    }

    setAnimationValue({value}) {
    }

    componentDidMount() {
        // Animated.Value监听范围 [0, tab数量-1]
        this.props.scrollValue.addListener(this.setAnimationValue);
    }

    renderTabOption(tab, i) {
        let borderBottomLeftRadius = (i == 0) ? 20 : 0;
        let borderTopLeftRadius = (i == 0) ? 20 : 0;
        let borderBottomRightRadius = (i == 0) ? 0 : 20;
        let borderTopRightRadius = (i == 0) ? 0 : 20;
        let color = (this.props.activeTab == i) ? '#8460e4' : Color.colorWhite;
        let textColor = (this.props.activeTab == i) ? Color.colorWhite : '#8460e4';
        let requestFriendCount = 0;
        let requestData = this.props.friendRequestListData;
        if (requestData && requestData.total_user > 0) {
            requestFriendCount = requestData.total_user;
        }
        let chatUnreadMessageCount = 0;
        let familyChatUnreadMessageCount = 0;
        if (this.props.unReadMessageNumber) {
            chatUnreadMessageCount = this.props.unReadMessageNumber;
        }
        if (this.props.familyChatData && this.props.familyChatData.unreadNumber) {
            familyChatUnreadMessageCount = this.props.familyChatData.unreadNumber;
        }
        unreadMessageCount = chatUnreadMessageCount + familyChatUnreadMessageCount;
        let unReadMessageNumber = (i == 0) ? requestFriendCount : unreadMessageCount;
        if (unReadMessageNumber > 99) {
            unReadMessageNumber = 99 + '+';
        }
        let left = (ScreenWidth / 6) + 12;
        let BadgeView = (unReadMessageNumber == 0) ? null : (
                <Badge minWidth={5} minHeight={5} textStyle={{fontSize:9,paddingVertical:1}} extraPaddingHorizontal={4}
                       style={{width:6,height:6, position: 'absolute',left:left,top: 2,bottom: 0,right: 0,backgroundColor:Color.badge_color}}>{unReadMessageNumber}</Badge>);
        return (
            <View style={[styles.tabItem]}>
                <View
                    style={{backgroundColor:color,alignSelf:'center',width:ScreenWidth/3,height:32,borderBottomLeftRadius:borderBottomLeftRadius,borderTopLeftRadius:borderTopLeftRadius,borderBottomRightRadius:borderBottomRightRadius,borderTopRightRadius:borderTopRightRadius}}>
                    <TouchableOpacity
                        style={styles.tabButton}
                        onPress={()=>this.onGoToPage(i)} activeOpacity={0.6}>
                        <Text
                            style={[styles.tabText,{color:textColor}]}>
                            {this.props.tabNames[i]}
                        </Text>
                    </TouchableOpacity>
                </View>
                {BadgeView}
            </View>

        );
    }

    /**
     * 跳转到指定页面的操作
     * @param i
     */
    onGoToPage(i) {
        this.props.goToPage(i)
        switch (i) {
            case 0:
                MenuData = ContactMenuData;
                break;
            case 1:
                MenuData = messageMenuData;
                break;
        }
    }

    render() {
        return (
            <View style={styles.tabs}>
                <View style={{width:50}}/>
                <View flexDirection='row'
                      style={{ borderColor:'#6932c3',borderWidth:2,borderRadius:18,width:ScreenWidth/3*2,height: 35,alignSelf:'center'}}>
                    {this.props.tabs.map((tab, i) => this.renderTabOption(tab, i))}
                </View>
                {this.Menu()}
            </View>
        );
    }

    /**
     *消息更多操作
     * @returns {*}
     * @private
     */
    Menu() {
        return (<Dropdown style={[styles.menuStyle]}
                          dropdownStyle={[styles.menuDropDown]}
                          options={MenuData}
                          renderRow={this.menuRenderRow.bind(this)}
                          onSelect={(idx, value) => this.menuOnSelect(idx, value)}>
            <View flexDirection='row' style={{justifyContent:'center',alignSelf:'flex-end',height:30}}>
                <View style={styles.messageMenuPoint}/>
                <View style={styles.messageMenuPoint}/>
                <View style={styles.messageMenuPoint}/>
            </View>
        </Dropdown>);
    }

    /**
     *
     * @param rowData
     * @param rowID
     * @param highlighted
     */
    menuRenderRow(rowData, rowID, highlighted) {
        return ( <TouchableOpacity activeOpacity={0.6}>
            <View flexDirection='row' style={{alignSelf:'center',marginTop:3,marginBottom:3}}>
                <CachedImage style={{width:20,height:20,margin:5,marginLeft:0}} source={rowData.img}/>
                <Text style={{color:Color.voice_me_text_color,alignSelf:'center'}}>{rowData.value}</Text>
            </View></TouchableOpacity>);
    }

    /**
     *
     * @param idx
     * @param value
     */
    menuOnSelect(idx, value) {
        switch (value.key) {
            case Constant.READ:
                if (unreadMessageCount) {
                    //全部阅读消息
                    InteractionManager.runAfterInteractions(() => {
                        this.props.readAllMessage();
                        let userInfo = this.props.userInfo;
                        let groupFamilyInfo;
                        if (userInfo) {
                            if (userInfo.group) {
                                groupFamilyInfo = userInfo.group;
                                if (groupFamilyInfo.lc_id) {
                                    this.props.readFamilyChat(groupFamilyInfo.lc_id);
                                }
                            }
                        }
                    });
                }
                break;
            case Constant.DELETE:
                //进入删除界面
                try {
                    if (MenuData.length == 1) {
                        this.props.openPage(SCENES.SCENE_DELETE_ALL_CONTACT, 'global');
                    } else if (MenuData.length == 2) {
                        this.props.openPage(SCENES.SCENE_DELETE_ALL_MESSAGE, 'global');
                    }
                } catch (e) {
                }
                break;
        }
    }
}

const styles = StyleSheet.create({
    messageMenuPoint: {
        width: 4, height: 4, backgroundColor: '#191919', borderRadius: 4, alignSelf: 'center', margin: 2
    },
     menuStyle: {
        width: 50,
        alignSelf: 'center',
        height: 50,
        justifyContent: 'center',
        paddingRight: 0,
    },
    menuDropDown: {
        width: 100,
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: Color.colorWhite
    },
    tabs: {
        flexDirection: 'row',
        height: 65,
        justifyContent: 'center',
    },

    tab: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.footerColor
    },

    tabItem: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    tabButton: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 33,
        width: ScreenWidth / 3 - 2
    },
    tabText: {
        fontSize: 16, textAlign: 'center', fontWeight: 'bold', backgroundColor: Color.transparent
    }
});

const mapStateToProps = state => ({
    unReadMessageNumber: state.chatReducer.messageNumber,
    friendRequestListData: state.messageReducer.data,
    familyChatData: state.familyChatReducer.data,
    userInfo: state.userInfoReducer.data,

});
const mapDispatchToProps = dispatch => ({
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        readFamilyChat: (id) => dispatch(readFamilyChat(id)),
        readAllMessage: () => dispatch(readAllMessage()),
    }
);
export default connect(mapStateToProps, mapDispatchToProps,)(MessageTabBar);
