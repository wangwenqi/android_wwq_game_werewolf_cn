/**
 * Created by wangxu on 2017/3/29.
 */
'use strict';
export const SEARCH_ROOM = 'search_room';
export const SIMPLE_MODE = 'simple_mode';
export const STANDARD_MODEL = 'standard_model';
export const CREATE_ROOM = 'create_room';
export const QQ_LOGIN = 'qq_login';
export const PHONE_LOGIN = 'phone_login';
export const GUEST_LOGIN = 'guest_login';
export const WE_CHAT_LOGIN = 'we_chat_login';
export const FACEBOOK_LOGIN = 'facebook_login';
export const LINE_LOGIN = 'line_login';
export const DELETE_FRIEND = 'delete_friend';
export const DELETE_MESSAGE = 'delete_message';
export const CHANGE_SIGNATURE = 'change_signature';
export const UPLOAD_PHOTO = 'upload_photo';
export const UPLOAD_PHOTO_FAIL = 'upload_photo_fail';
export const UPLOAD_PHOTO_TO_LEAN_CLOUD_ERROR = 'upload_photo_to_lean_cloud_error';
export const FEEDBACK = 'feedback';
export const UPGRADE = 'upgrade';
export const APPLICATION_MARKET = 'application_market';
export const ABOUT = 'about';
//===============================================礼物===================================================
export const GIFT_ROSE = "flower_1"; // 玫瑰花
export const GIFT_ROSE3 = "flower_3"; //3朵玫瑰花
export const GIFT_ROSE99 = "flower_99"; //99朵玫瑰花
export const GIFT_VEGETABLE = "cabbage"; // 烂菜叶
export const GIFT_EGG = "egg"; // 臭鸡蛋
export const GIFT_CLAP = "clap"; // 啪啪啪
export const GIFT_CHOCOLATE = "chocolate"; // 巧克力
export const GIFT_HEART = "heart"; //520
export const GIFT_JAWBREAKER = "lollipop"; //棒棒糖
export const GIFT_KISS = "kiss"; // 么么哒
export const GIFT_RING = "dim_ring"; // 钻戒
export const GIFT_TEDDYBEAR = "tidy"; // 小熊
export const GIFT_WEDDINGDRESS = "wedding_dress"; // 婚纱
export const GIFT_CROWN = "crown"; // 皇冠
export const GIFT_CAR = "ferrari"; // 法拉利
export const GIFT_CAKE = "cake"; // 蛋糕
export const GIFT_BOAT = "ship"; // 友谊的小船
export const GIFT_AIRPLANE = "airplane"; // 私人飞机
export const GIFT_ROCK = "rocket"; // 幸福火箭
export const GIFT_GOLDEN_MICROPHONE = "golden_microphone";//金话筒
export const GIFT_PERFUME = "perfume";//香水
export const GIFT_WEAK_SAUCE = "weak_sauce";//聊爆了
export const GIFT_BOMB = "bomb";//炸弹
export const GIFT_BYEBYE = "byebye";//ByeBye
export const GIFT_DISS = "diss";//diss
export const GIFT_LIPSTICK = "lipstick";//口红
export const GIFT_HANDBAG = "handbag";//包包
export const GIFT_MARLBORO = "marlboro";//万宝路
export const GIFT_IPHONEX = "iphonex";//iphonex
export const GIFT_TEA = "tea";//倒茶
export const GIFT_CHRISTMAS_SANTARIDE = "christmas_santaride";//驯鹿礼车
export const GIFT_CHRISTMAS_SNOWMAN = "christmas_snowman";//雪人
export const GIFT_CHRISTMAS_BOX = "christmas_box";//礼盒
export const GIFT_CHRISTMAS_TREE = "christmas_tree";//圣诞树
export const GIFT_FORMAL_DRESS = "formal_dress";//礼服
export const GIFT_RED_PACKET = "red_packet";//红包雨
export const GIFT_ONE_RED_PACKET = "one_red_packet";//红包
export const GIFT_SOLAR_SYSTEM = "solar_system";//太阳系
export const GIFT_MORE_MONEY = "more_money";//金元宝
export const GIFT_MONEY_TREE = "money_tree";//金玉满堂
export const GIFT_MAMMON = "mammon";//财神驾到
export const GIFT_FIRECRACKER = "firecracker";//鞭炮
export const GIFT_FINAL_WEREWOLF = "final_werewolf";//礼花2018
export const GIFT_GRAMMY = "grammy";//格莱美
export const GIFT_FAECES = "faeces";//便便
//===============================================礼物===================================================

//
export const SAVE_USER_ERROR = "save_user_error"; //存储用户信息
export const REMOVE_USER_ERROR = "remove_user_error"; // 移除用户信息失败
//#######
export const ACTION_REQUEST_FRIENDS_LIST = "ACTION_REQUEST_FRIENDS_LIST"; //请求好友列表
export const ACTION_REQUEST_FRIENDS_LIST_MINI_GAME = "ACTION_REQUEST_FRIENDS_LIST_MINI_GAME"; //邀请小游戏好友
export const RN_UPDATE_CONTACT = "RN_UPDATE_CONTACT"; //更新好友
export const ACTION_DELETE_FRIEND = "ACTION_DELETE_FRIEND"; //删除好友
export const ACTION_ADD_FRIEND = "ACTION_ADD_FRIEND"; //添加好友
export const LEAVE_AUDIO_ROOM = "LEAVE_AUDIO_ROOM"; //离开语音房间
export const BLACK_LIST_ADD = "black_list_add"; //拉黑好友
export const BLACK_LIST = "black_list"; //黑名单
export const STANDINGS = "standings"; //战绩
export const RN_NATIVE = "RN_Native"; //rn与native交互的type
export const INTENT = "INTENT"; //rn与native交互的type
export const APPROVE_FRIEND = "APPROVE_FRIEND"; //消息是同意好友请求
export const INIT_REALM_ERROR = "init_realm_error"; //初始化realm数据库失败
export const ACTION_CREATE_FAMILY_CONVERSATION = "ACTION_CREATE_FAMILY_CONVERSATION"; //家族创建
//消息类型
export const FAMILY = "FAMILY"; //家族消息
export const SINGLE = "SINGLE"; //系统消息或是单聊消息
export const noplay = "noplay"; //禁玩消息
//
export const ACTION_ENTER_FAMILY_CONVERSATION = "ACTION_ENTER_FAMILY_CONVERSATION"; //进入家族
export const ACTION_QUIT_FAMILY = "ACTION_QUIT_FAMILY"; //退出家族
export const ACTION_SHOW_FAMILY_INFO = "ACTION_SHOW_FAMILY_INFO"; //进入家族详情
export const ACTION_UP_LOAD_USER_HEAD = "ACTION_UP_LOAD_USER_HEAD"; //用于上传用户头像
export const ACTION_UP_LOAD_PHOTOS = "ACTION_UP_LOAD_PHOTOS"; //用于上传用户头像
export const ACTION_UP_LOAD_FAMILY_HEAD = "ACTION_UP_LOAD_FAMILY_HEAD"; //用于上传家族头像
export const ACTION_FAMILY_INFO_INIT = "ACTION_FAMILY_INFO_INIT"; //初始化家族详情
export const ACTION_LOGIN_OUT = "ACTION_LOGIN_OUT"; //退出登录
export const SCROLL_RECREATION_LIST = "scroll_recreation_list"; //滑动语音房间列表
export const SCROLL_VOICE_RECREATION_LIST = "scroll_voice_recreation_list"; //独立应用滑动语音房间列表
export const REACTNATIVEBRIDGE = "ReactNativeBridge"; //滑动语音房间列表
export const SHOW_ALL_CHILD_TYPE = "show_all_child_type"; //展示
export const SHARE_HOUSE_DEEDS = "SHARE_HOUSE_DEEDS"; //分享房契
export const ACTION_TOURIST_EVENT = "action_tourist_event"; //游客操作
export const GO_TO_PAGE = "go_to_page"; //展示
//礼物加载type
export const giftLoadTypeCache = 'cache';
export const giftLoadTypeLocal = 'local';
//voice me

export const VOICE_WEALTH = 'wealth';
export const VOICE_GROUP = 'group';
export const VOICE_GIFTS = 'gifts';
export const VOICE_SHARE = 'share';
export const VOICE_SETTING = 'setting';
export const VOICE_PHOTO_WALL = 'photo_wall';
export const VOICE_USER_DETAIL = 'user_detail';
//
//语音房间类型
export const VOICE_TYPE_ALL = 'all';//全部
export const VOICE_TYPE_SING = 'sing';//k歌
export const VOICE_TYPE_CHAT = 'chat';//闲聊
export const VOICE_TYPE_LOVE = 'love';//相亲
export const VOICE_TYPE_FATE = 'fate';//算命
export const VOICE_TYPE_MERRY = 'merry';//婚礼
export const VOICE_TYPE_GAME = 'game';//小游戏
export const VOICE_TYPE_UNDERCOVER = 'undercover';//谁是卧底
export const VOICE_TYPE_SEEK = 'seek';//情感咨询
//运营操作
export const OPERA_TYPE_URL='url';
export const OPERA_TYPE_ENTER='enter';
export const ACTION_ENTER_AUDIO='enterAudioRoom';
//个人等级对应的类型
export const VILLAGER='villager';//村民
export const VILLAGE_HEAD='village_head';//老村长
export const SHERIFF='sheriff';//警长
export const GUARD='guard';//守卫
export const HUNTER='hunter';//猎人
export const PALADIN='paladin';//骑士
export const WITCH='witch';//女巫
export const SEER='seer';//预言家
export const PRIEST='priest';//大祭司
export const BISHOP='bishop';//主教
export const RED_BISHOP='red_bishop';//红衣主教
export const PONTIFF='pontiff';//教皇
export const ANGEL='angel';//天使
export const DEMON='demon';//恶魔
export const CUPID='cupid';//爱神
export const SUN_GOD='sun_god';//太阳神
export const PLUTO='pluto';//冥王
export const ZEUS='zeus';//宙斯
//
export const READ='read';
export const DELETE='delete';

//
export const ZH = 'zh-CN';
export const TW = 'zh-TW';
export const HK = 'zh-HK';
export const ZH_IOS = 'zh-Hans-CN';//(简体中文)
export const ZH_HANT_IOS = 'zh-Hant-CN';//(繁体中文)
export const TW_IOS = 'zh-Hant-TW';//(繁体中文-台湾）
export const HK_IOS = 'zh-Hant-HK';//(繁体中文-香港)
export const JA_JP = 'ja-JP';//日文
export const JA_JP_IOS = 'ja';//日文iOS
export const KO_KR = 'ko-KR';//韩文


