/**
 * Created by wangxu on 2017/7/19.
 */
import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';
import Language from '../../resources/language';
import Color from '../../resources/themColor';
import {Spinner} from 'native-base';
const [width, height] = [Dimensions.get('window').width, Dimensions.get('window').height];

export default class Loading extends React.Component {
    constructor(props) {
        super(props);
        this.state = {hidden: true};
        this.params = this.params || {...this.props};
    }

    static defaultProps = {
        loadingTitle: Language().operation_loading
    }
    static propTypes = {
        loadingTitle: React.PropTypes.string,
        contentStyle: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.object, React.PropTypes.array]),
        container: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.object, React.PropTypes.array]),
    };

    render() {
        if (this.state.hidden) {
            return null;
        }
        const {
            loadingTitle,
            contentStyle,
            container
        } = this.params;
        return (
            <View style={[styles.container,container]}>
                <TouchableWithoutFeedback >
                    <View style={styles.dialogModal}></View>
                </TouchableWithoutFeedback>
                <View style={[styles.loading,contentStyle]}>
                    <Spinner size='small' style={{height:35}}/>
                    <Text style={styles.loadingTitle}>{loadingTitle}</Text>
                </View>
            </View>
        )
    }

    _lockQueue = [];

    show(params) {
        if (!this.state.hidden && this.lock) {
            this._lockQueue.push(params);
            return;
        }
        this.lock = true;
        this.params = {...this.props, ...params};
        this.setState({hidden: false});
    }

    isShowing() {
        return !this.state.hidden;
    }

    hide() {
        try {
            this.setState({hidden: true});
            this.lock = false;
            this._lockQueue[0] && this.show(this._lockQueue[0]);
            this._lockQueue = this._lockQueue.slice(1);
        } catch (e) {
        }
    }

}
const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        height: height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
    },
    loading: {
        backgroundColor: Color.loading_bg_color,
        height: 60,
        width: 80,
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    loadingTitle: {
        marginTop: 2,
        fontSize: 12,
    },
    dialogModal: {
        zIndex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        backgroundColor: Color.transparent,
        width: width,
        height: height,
    },
})