/**
 * Created by wangxu on 2017/9/7.
 */
import Realm from 'realm';
import Util from './utils';
import  * as apiDefines from './gameApiDefines';
import * as types from '../actions/actionTypes';
import rnRoNativeUtils from './rnToNativeUtils';
import * as Constant from './constant';
let initRealmError = false;
const FRIEND_KEY = 'friend_key';
const GAME_KEY = 'game_key';
const FAMILY_KEY = 'family';
const GameSchema = {
    name: GAME_KEY,
    properties: {
        experience: {type: 'int', default: 0},
        win: {type: 'int', default: 0},
        lose: {type: 'int', default: 0},
        escape: {type: 'int', default: 0},
        level: {type: 'int', default: 0},
    }
};
const FriendSchema = {
    name: FRIEND_KEY,
    primaryKey: 'id',
    properties: {
        id: 'string',
        image: 'string',
        name: 'string',
        sex: 'int',
        status: {type: 'string', optional: true},
        game_status: {type: 'int', optional: true},
        room_id: {type: 'string', optional: true},
        signature: {type: 'string', optional: true},
        update_time: {type: 'string', optional: true},
        game: {type: GAME_KEY},
    }
};
//初始化Realm
let realm = null;
let MessageSchema;
let FamilyMessageSchema;
const initRealmManager = (userId, callback) => {
    MessageSchema = {
        name: userId,
        primaryKey: 'id',
        properties: {
            id: 'string',
            image: 'string',
            name: 'string',
            sex: 'string',
            time: 'date',
            message_type: 'string',
            message: 'string',
            unreadNumber: 'int',
            conversation_id: 'string',
        }
    };
    FamilyMessageSchema = {
        name: userId + FAMILY_KEY,
        primaryKey: 'id',
        properties: {
            id: 'string',
            time: 'date',
            message: 'string',
            unreadNumber: 'int',
        }
    }
    try {
        realm = new Realm({
            schema: [FriendSchema, GameSchema, MessageSchema, FamilyMessageSchema],
            schemaVersion: 3,
            migration: function (oldRealm, newRealm) {
            }
        });
    } catch (error) {
        console.log(error);
        //初始化数据库失败
        //alert(error)
        Realm.deleteFile(MessageSchema);
        Realm.deleteFile(GameSchema);
        Realm.deleteFile(MessageSchema);
        Realm.deleteFile(FamilyMessageSchema);
        if (!initRealmError) {
            initRealmError = true;
            initRealmManager(userId, callback);
        }
        rnRoNativeUtils.onStatistics(Constant.INIT_REALM_ERROR);
    }
    callback(realm);
}
const resetRealmManger = () => {
    try {
        if (realm) {
            let results = realm.objects(FRIEND_KEY);
            //1.先删除失效的数据
            realm.write(() => {
                realm.delete(results);
            });
            realm.close();
        }
    } catch (e) {
    }

}
const deleteFriend = (success) => {
    try {
        if (realm) {
            let results = realm.objects(FRIEND_KEY);
            // 1.先删除失效的数据
            realm.write(() => {
                realm.delete(results);
                success();
            });
        }
    } catch (e) {
        //alert(e)
    }
}
const getRealmManger = (request) => {
    request(realm);
}
/**
 * 插入所有好友信息
 * @param listData
 */
const insertAllData = (listData) => {
    if (listData && realm ) {
        for (let i = 0; i < listData.length; i++) {
            let friendData = listData[i];
            try {
                let gameData = friendData.game;
                let status = friendData.status;
                let game_status = 0;
                let room_id = '';
                let updateTime = '0';
                let signature='';
                if(friendData.signature){
                    signature=friendData.signature;
                }
                if (status) {
                    let arrayStatus = new Array();
                    arrayStatus = status.split(':');
                    for (let i = 0; i < arrayStatus.length; i++) {
                        if (i == 0) {
                            game_status = parseInt(arrayStatus[i]);
                        } else if (i == 1) {
                            if (arrayStatus[i] != 'undefined') {
                                updateTime = arrayStatus[i];
                            }
                        } else if (i == 2) {
                            room_id = arrayStatus[i];
                        }
                    }
                } else {
                    status = null
                }
                let friend = realm.objects(FRIEND_KEY).filtered('id==' + '"' + friendData.id + '"');
                realm.write(() => {
                    if (friend.length == 0) {
                        realm.create(FRIEND_KEY, {
                            id: friendData.id,
                            name: friendData.name ? friendData.name : '',
                            sex: friendData.sex,
                            image: friendData.image ? friendData.image : '',
                            game: {
                                experience: gameData.experience ? gameData.experience : 0,
                                win: gameData.win ? gameData.win : 0,
                                lose: gameData.lose ? gameData.lose : 0,
                                escape: gameData.escape ? gameData.escape : 0,
                                level: gameData.level ? gameData.level : 0
                            },
                            status: status,
                            room_id: room_id,
                            update_time: updateTime,
                            game_status: game_status,
                            signature:signature
                        });
                    } else {
                        realm.create(FRIEND_KEY, {
                            id: friendData.id,
                            name: friendData.name ? friendData.name : '',
                            sex: friendData.sex,
                            image: friendData.image ? friendData.image : '',
                            game: {
                                experience: gameData.experience ? gameData.experience : 0,
                                win: gameData.win ? gameData.win : 0,
                                lose: gameData.lose ? gameData.lose : 0,
                                escape: gameData.escape ? gameData.escape : 0,
                                level: gameData.level ? gameData.level : 0
                            },
                            status: status,
                            room_id: room_id,
                            update_time: updateTime,
                            game_status: game_status,
                            signature:signature
                        }, true);
                    }
                });
            } catch (e) {
                //console.log('好友信息数据库异常：' + e);
                //alert('好友信息数据库异常：' + friendData.name)
            }
        }
    }
}
/**
 * 分页同步数据
 * @param needRemoveOld
 * @param page
 */
const synchronizationFriendList = (needRemoveOld, page, requestList) => {
    try {
        let url = apiDefines.GET_FRIEND_PAGE_LIST + apiDefines.PAGE + page;
        Util.get(url, (code, message, data) => {
            if (code == 1000) {
                if (data && data.list) {
                    if (realm) {
                        insertAllData(data.list);
                        requestList();
                    }
                }
            } else {
                requestList();
            }
        }, (failed) => {
            requestList();

        });
    } catch (e) {
        // alert(e)
    }
}
/**
 * 更改游戏状态
 * @param updateSuccess
 */
const updateGameStatus = (updateSuccess) => {
    let url = apiDefines.GET_FRIEND_STATUSLIST + '3,2,1';
    Util.get(url, (code, message, data) => {
        if (code == 1000) {
            let userList = data.data;
            if (userList) {
                if (realm) {
                    //先把所有老的game_status 置为离线
                    let results = realm.objects(FRIEND_KEY);
                    if (results.length > 0) {
                        for (let i = 0; i < results.length; i++) {
                            realm.write(() => {
                                realm.create(FRIEND_KEY, {
                                    id: results[i].id,
                                    game_status: 0
                                }, true);
                            });
                        }
                    }
                    for (let index = 0; index < userList.length; index++) {
                        try {
                            let userId = userList[index].id;
                            let friend = realm.objects(FRIEND_KEY).filtered('id==' + '"' + userId + '"');
                            let status = userList[index].status;
                            let game_status = 0;
                            let room_id = '';
                            let updateTime = '0';
                            if (status) {
                                let arrayStatus = new Array();
                                arrayStatus = status.split(':');
                                for (let i = 0; i < arrayStatus.length; i++) {
                                    if (i == 0) {
                                        game_status = parseInt(arrayStatus[i]);
                                    } else if (i == 1) {
                                        if (arrayStatus[i] != 'undefined') {
                                            updateTime = arrayStatus[i];
                                        }
                                    } else if (i == 2) {
                                        room_id = arrayStatus[i];
                                    }
                                }
                            } else {
                                status = null
                            }
                            if (friend.length != 0) {
                                realm.write(() => {
                                    realm.create(FRIEND_KEY, {
                                        id: userId,
                                        status: status,
                                        room_id: room_id,
                                        update_time: updateTime,
                                        game_status: game_status
                                    }, true);
                                });
                            }
                        } catch (e) {
                            //alert('更改游戏状态错误:' + e);
                        }

                    }
                    updateSuccess();
                }
            }
        }
    }, (failed) => {

    });
}
export default {
    initRealmManager,
    resetRealmManger,
    getRealmManger,
    synchronizationFriendList,
    updateGameStatus,
    deleteFriend,
    FRIEND_KEY,
    FAMILY_KEY
}