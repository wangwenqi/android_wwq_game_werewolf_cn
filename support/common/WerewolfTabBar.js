/**
 * Created by wangxu on 2017/6/1.
 */
'use strict';

import React, {Component} from 'react';
import Color from '../../resources/themColor';
import {Button} from 'native-base';
import Badge from 'react-native-smart-badge';
import {connect} from 'react-redux';
import {actionTab} from '../actions/userInfoActions';
import * as types from '../actions/actionTypes';
import CachedImage from '../../support/common/CachedImage';
import ImageManager from '../../resources/imageManager';
import  * as Constant from './constant';

import {
    StyleSheet,
    View,
    TouchableOpacity,
    Text, Image, Platform, Dimensions, DeviceEventEmitter
} from 'react-native';
const selectedTabIconNames = [require('../../resources/imgs/icon_game_select.png'), require('../../resources/imgs/../../resources/imgs/ic_recreation_selected.png'), require('../../resources/imgs/icon_message_select.png'), require('../../resources/imgs/icon_me_select.png')];
const ScreenWidth = Dimensions.get('window').width;
const tabIconWidth = (Platform.OS === 'ios') ? 23 : 23;
const tabIconHeight = (Platform.OS === 'ios') ? 23 : 23;
class WerewolfTabBar extends Component {

    propTypes = {
        goToPage: React.PropTypes.func, // 跳转到对应tab的方法
        activeTab: React.PropTypes.number, // 当前被选中的tab下标
        tabs: React.PropTypes.array, // 所有tabs集合
        tabNames: React.PropTypes.array, // 保存Tab名称
        tabIconNames: React.PropTypes.array, // 保存Tab图标
        isTourist: React.PropTypes.bool,
        unReadMessageNumber: React.PropTypes.number,
        actionTab: React.PropTypes.func,
        friendRequestListData: React.PropTypes.object,
        familyChatData: React.PropTypes.object,
    }

    setAnimationValue({value}) {
    }

    componentDidMount() {
        // Animated.Value监听范围 [0, tab数量-1]
        this.props.scrollValue.addListener(this.setAnimationValue);
    }

    renderTabOption(tab, i) {
        let color = this.props.activeTab == i ? "#70009a" : "#410051"; // 判断i是否是当前选中的tab，设置不同的颜色
        let tabImage = this.props.activeTab == i ? selectedTabIconNames[i] : this.props.tabIconNames[i];
        let unReadMessageNumber = 0;
        let requestNumber = 0;
        let badgeCount = 0;
        let familyChatCount = 0;
        if (this.props.friendRequestListData && this.props.friendRequestListData.total_user) {
            if (this.props.friendRequestListData.total_user) {
                requestNumber = this.props.friendRequestListData.total_user;
            }
        }
        if (this.props.unReadMessageNumber != 0 && this.props.unReadMessageNumber != null && this.props.unReadMessageNumber != '') {
            unReadMessageNumber = this.props.unReadMessageNumber;
        }
        if (this.props.familyChatData && this.props.familyChatData.unreadNumber) {
            familyChatCount = this.props.familyChatData.unreadNumber;
        }
        badgeCount = unReadMessageNumber + requestNumber + familyChatCount;
        if (badgeCount > 98) {
            badgeCount = 99 + '+';
        }
        let BadgeView = (badgeCount == 0 || i != 2) ? null : (
                <Badge minWidth={5} minHeight={5} textStyle={{fontSize:9,paddingVertical:1}} extraPaddingHorizontal={4}
                       style={{width:6,height:6, position: 'absolute',left:ScreenWidth/selectedTabIconNames.length/2+4,top: 5,bottom: 0,right: 0,backgroundColor:Color.badge_color}}>{badgeCount}</Badge>);
        return (<TouchableOpacity onPress={()=>this.onGoToPage(i)} activeOpacity={0.9}
                                  style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor: Color.colorWhite,height:55,borderRadius:0}}>
                <View style={[styles.tabItem]}>
                    <Image
                        source={tabImage} // 图标
                        style={styles.tabImage}
                        color={color}/>
                    <Text style={{color: color,fontSize:10,textAlign:'center',fontWeight: 'bold'}}>
                        {this.props.tabNames[i]}
                    </Text>
                </View>
                {BadgeView}
            </TouchableOpacity>

        );
    }

    /**
     * 跳转到指定页面的操作
     * @param i
     */
    onGoToPage(i) {
        if (!this.props.isTourist) {
            this.props.goToPage(i);
        } else {
            /**
             * 当时游客模式进入时
             */
                // if (i == 0) {
                //     this.props.actionTab(types.ACTION_HOME);
                // } else if (i == 1) {
                //     this.props.actionTab(types.ACTION_RECREATION);
                // } else if (i == 2) {
                //     this.props.actionTab(types.ACTION_MESSAGE);
                // } else if (i == 3) {
                //     this.props.actionTab(types.ACTION_ME);
                // }
            let event = {needShow: true, type: ''};
            if (i == 0 || i == 1) {
                this.props.goToPage(i);
            } else if (i == 2) {
                event.type = types.ACTION_MESSAGE;
                DeviceEventEmitter.emit(Constant.ACTION_TOURIST_EVENT, event);
                //this.props.actionTab(types.ACTION_MESSAGE);
            } else if (i == 3) {
                event.type = types.ACTION_ME;
                DeviceEventEmitter.emit(Constant.ACTION_TOURIST_EVENT, event);
                //this.props.actionTab(types.ACTION_ME);
            }
        }
    }

    render() {
        return (
            <View style={styles.tabs}>
                {this.props.tabs.map((tab, i) => this.renderTabOption(tab, i))}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tabs: {
        flexDirection: 'row',
        height: 55,
    },

    tab: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.footerColor
    },

    tabItem: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    tabImage: {
        width: tabIconWidth,
        height: tabIconHeight,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginBottom: 3
    },
    tabBetaImage: {
        width: 18,
        height: 9,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginLeft: 2
    },
});

const mapStateToProps = state => ({
    unReadMessageNumber: state.chatReducer.messageNumber,
    isTourist: state.userInfoReducer.isTourist,
    friendRequestListData: state.messageReducer.data,
    familyChatData: state.familyChatReducer.data,
});
const mapDispatchToProps = dispatch => ({
        actionTab: (tabName) => dispatch(actionTab(tabName))
    }
);
export default connect(mapStateToProps, mapDispatchToProps,)(WerewolfTabBar);
