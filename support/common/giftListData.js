/**
 * Created by wangxu on 2017/11/14.
 */
import Language from '../../resources/language';
import * as Constant from './constant';
//礼物资源********************************************************************
const iconAirplane = require('../../resources/imgs/gifts/icon_gift_airplane.png');
const iconBoat = require('../../resources/imgs/gifts/icon_gift_boat.png');
const iconCake = require('../../resources/imgs/gifts/icon_gift_cake.png');
const iconCar = require('../../resources/imgs/gifts/icon_gift_car.png');
const iconChocolate = require('../../resources/imgs/gifts/icon_gift_chocolate.png');
const iconClap = require('../../resources/imgs/gifts/icon_gift_clap.png');
const iconCrown = require('../../resources/imgs/gifts/icon_gift_crown.png');
const iconEgg = require('../../resources/imgs/gifts/icon_gift_egg.png');
const iconHeart = require('../../resources/imgs/gifts/icon_gift_heart.png');
const iconJawbreaker = require('../../resources/imgs/gifts/icon_gift_jawbreaker.png');
const iconKiss = require('../../resources/imgs/gifts/icon_gift_kiss.png');
const iconRing = require('../../resources/imgs/gifts/icon_gift_ring.png');
const iconRocket = require('../../resources/imgs/gifts/icon_gift_rocket.png');
const iconRose = require('../../resources/imgs/gifts/icon_gift_rose.png');
const iconRose3 = require('../../resources/imgs/gifts/icon_gift_rose3.png');
const iconRose99 = require('../../resources/imgs/gifts/icon_gift_rose99.png');
const iconTeddybear = require('../../resources/imgs/gifts/icon_gift_teddybear.png');
const iconVegetable = require('../../resources/imgs/gifts/icon_gift_vegetable.png');
const iconWeddingdress = require('../../resources/imgs/gifts/icon_gift_weddingdress.png');
const iconGoldenMicrophone = require('../../resources/imgs/gifts/icon_gift_golden_microphone.png');//金话筒
const iconPerfume = require('../../resources/imgs/gifts/icon_gift_perfume.png');//香水
const iconWeakSauce = require('../../resources/imgs/gifts/icon_gift_weak_sauce.png');//聊爆了
const iconBomb = require('../../resources/imgs/gifts/icon_gift_bomb.png');//炸弹
const iconByebye = require('../../resources/imgs/gifts/icon_gift_byebye.png');//拜拜
const iconDiss = require('../../resources/imgs/gifts/icon_gift_diss.png');//diss
const iconLipstick = require('../../resources/imgs/gifts/icon_gift_lipstick.png');//口红
const iconHandbag = require('../../resources/imgs/gifts/icon_gift_handbag.png');//包包
const iconMarlboro = require('../../resources/imgs/gifts/icon_gift_marlboro.png');//万宝路
const iconIphonex = require('../../resources/imgs/gifts/icon_gift_iphonex.png');//iphonex
const iconTea = require('../../resources/imgs/gifts/icon_gift_tea.png');//倒茶
//-------------------------------------------------------------------------------------------------------
const iconRedPacket= require('../../resources/imgs/gifts/icon_gift_red_packet.png');//红包雨
const iconOneRedPacket= require('../../resources/imgs/gifts/icon_one_red_packet.png');//红包
const iconSolarSystem= require('../../resources/imgs/gifts/icon_gift_solar_system.png');//太阳系
const iconMoreMoney= require('../../resources/imgs/gifts/icon_gift_more_money.png');//金元宝
const iconMoneyTree= require('../../resources/imgs/gifts/icon_gift_money_tree.png');//金玉满堂
const iconMammon= require('../../resources/imgs/gifts/icon_gift_mammon.png');//财神驾到
const iconFirecracker= require('../../resources/imgs/gifts/icon_gift_firecracker.png');//鞭炮
const iconFinalWerewolf= require('../../resources/imgs/gifts/icon_gift_final_werewolf.png');//礼花2018
const iconGrammy= require('../../resources/imgs/gifts/icon_gift_grammy.png');//格莱美
const iconFaeces= require('../../resources/imgs/gifts/icon_gift_faeces.png');//便便
const iconChristmasSantaride= require('../../resources/imgs/gifts/icon_gift_christmas_santaride.png');//驯鹿礼车
const iconChristmasSownman= require('../../resources/imgs/gifts/icon_gift_christmas_sownman.png');//雪人
const iconChristmasBox= require('../../resources/imgs/gifts/icon_gift_christmas_box.png');//礼盒
const iconChristmasTree= require('../../resources/imgs/gifts/icon_gift_christmas_tree.png');//圣诞树
const iconFormalDress= require('../../resources/imgs/gifts/icon_gift_formal_dress.png');//礼服
//礼物名称
//礼物load type

const getLocalGiftList = (gifts) => {
    let list = [
        {
            gift_image: iconRose,
            gift_name: Language().gift_rose,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_ROSE
        },
        {
            gift_image: iconRose3,
            gift_name: Language().gift_rose3,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_ROSE3
        },
        {
            gift_image: iconRose99,
            gift_name: Language().gift_rose99,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_ROSE99
        },
        {
            gift_image: iconVegetable,
            gift_name: Language().gift_vegetable,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_VEGETABLE
        },
        {
            gift_image: iconEgg,
            gift_name: Language().gift_egg,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_EGG
        },
        {
            gift_image: iconClap,
            gift_name: Language().gift_clap,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_CLAP
        },
        {
            gift_image: iconChocolate,
            gift_name: Language().gift_chocolate,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_CHOCOLATE
        },
        {
            gift_image: iconHeart,
            gift_name: Language().gift_heart,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_HEART
        },
        {
            gift_image: iconJawbreaker,
            gift_name: Language().gift_jawbreaker,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_JAWBREAKER
        },
        {
            gift_image: iconKiss,
            gift_name: Language().gift_kiss,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_KISS
        },
        {
            gift_image: iconRing,
            gift_name: Language().gift_ring,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_RING
        },
        {
            gift_image: iconTeddybear,
            gift_name: Language().gift_teddybear,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_TEDDYBEAR
        },
        {
            gift_image: iconWeddingdress,
            gift_name: Language().gift_weddingdress,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_WEDDINGDRESS
        },
        {
            gift_image: iconCrown,
            gift_name: Language().gift_crown,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_CROWN
        },
        {
            gift_image: iconCar,
            gift_name: Language().gift_car,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_CAR
        },
        {
            gift_image: iconCake,
            gift_name: Language().gift_cake,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_CAKE
        },
        {
            gift_image: iconBoat,
            gift_name: Language().gift_boat,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_BOAT
        },
        {
            gift_image: iconAirplane,
            gift_name: Language().gift_airplane,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_AIRPLANE
        },
        {
            gift_image: iconRocket,
            gift_name: Language().gift_rock,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_ROCK
        },
        {
            gift_image: iconGoldenMicrophone,
            gift_name: Language().gift_golden_microphone,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_GOLDEN_MICROPHONE
        },
        {
            gift_image: iconWeakSauce,
            gift_name: Language().gift_weak_sauce,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_WEAK_SAUCE
        },
        {
            gift_image: iconPerfume,
            gift_name: Language().gift_perfume,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_PERFUME
        }, {
            gift_image: iconBomb,
            gift_name: Language().gift_bomb,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_BOMB
        }, {
            gift_image: iconByebye,
            gift_name: Language().gift_byebye,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_BYEBYE
        }, {
            gift_image: iconDiss,
            gift_name: Language().gift_diss,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_DISS
        }, {
            gift_image: iconLipstick,
            gift_name: Language().gift_lipstick,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_LIPSTICK
        }, {
            gift_image: iconHandbag,
            gift_name: Language().gift_handbag,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_HANDBAG
        }, {
            gift_image: iconMarlboro,
            gift_name: Language().gift_marlboro,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_MARLBORO
        }, {
            gift_image: iconIphonex,
            gift_name: Language().gift_iphonex,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_IPHONEX
        }, {
            gift_image: iconTea,
            gift_name: Language().gift_tea,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_TEA
        }, {
            gift_image:iconFormalDress ,
            gift_name: Language().gift_formal_dress,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_FORMAL_DRESS
        },{
            gift_image:iconChristmasBox ,
            gift_name: Language().gift_christmas_box,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_CHRISTMAS_BOX
        },{
            gift_image:iconChristmasSantaride ,
            gift_name: Language().gift_christmas_santaride,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_CHRISTMAS_SANTARIDE
        },{
            gift_image:iconChristmasSownman ,
            gift_name: Language().gift_christmas_snowman,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_CHRISTMAS_SNOWMAN
        },{
            gift_image:iconChristmasTree ,
            gift_name: Language().gift_christmas_tree,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_CHRISTMAS_TREE
        },{
            gift_image:iconRedPacket ,
            gift_name: Language().gift_red_packet,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_RED_PACKET
        },{
            gift_image:iconOneRedPacket ,
            gift_name: Language().gift_one_red_packet,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_ONE_RED_PACKET
        },{
            gift_image:iconSolarSystem ,
            gift_name: Language().gift_solar_system,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_SOLAR_SYSTEM
        },{
            gift_image:iconMoreMoney ,
            gift_name: Language().gift_more_money,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_MORE_MONEY
        },{
            gift_image:iconMoneyTree ,
            gift_name: Language().gift_money_tree,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_MONEY_TREE
        },{
            gift_image:iconMammon ,
            gift_name: Language().gift_mammon,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_MAMMON
        },{
            gift_image:iconFirecracker ,
            gift_name: Language().gift_firecracker,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_FIRECRACKER
        },{
            gift_image:iconFinalWerewolf ,
            gift_name: Language().gift_final_werewolf,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_FINAL_WEREWOLF
        },{
            gift_image:iconGrammy ,
            gift_name: Language().gift_grammy,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_GRAMMY
        },{
            gift_image:iconFaeces ,
            gift_name: Language().gift_faeces,
            load_type: Constant.giftLoadTypeLocal,
            type: Constant.GIFT_FAECES
        },
    ];
    gifts(list);
}
export default {
    getLocalGiftList
};