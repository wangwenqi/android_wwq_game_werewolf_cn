/**
 * Created by wangxu on 2017/6/1.
 */
'use strict';

import React, {Component} from 'react';
import Color from '../../resources/themColor';
import CachedImage from '../../support/common/CachedImage';
import ImageManager from '../../resources/imageManager';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {StyleSheet, View, TouchableOpacity, Text, Platform, Dimensions, InteractionManager} from 'react-native';
const ScreenWidth = Dimensions.get('window').width;
const {pushRoute, popRoute} = actions;
class MiniGameTabBar extends Component {
    propTypes = {
        goToPage: React.PropTypes.func, // 跳转到对应tab的方法
        activeTab: React.PropTypes.number, // 当前被选中的tab下标
        tabs: React.PropTypes.array, // 所有tabs集合
        tabNames: React.PropTypes.array, // 保存Tab名称
        openPage: React.PropTypes.func,
        finishPage: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        this.state = {
            isFinishing: false,
        }
    }

    setAnimationValue({value}) {
    }

    componentDidMount() {
        // Animated.Value监听范围 [0, tab数量-1]
        this.props.scrollValue.addListener(this.setAnimationValue);
    }

    componentWillUnmount() {
        this.finishTimer && clearTimeout(this.finishTimer);
    }

    renderTabOption(tab, i) {
        let borderBottomLeftRadius = (i == 0) ? 22 : 0;
        let borderTopLeftRadius = (i == 0) ? 22 : 0;
        let borderBottomRightRadius = (i == 0) ? 0 : 22;
        let borderTopRightRadius = (i == 0) ? 0 : 22;
        let color = (this.props.activeTab == i) ? '#8460e4' : Color.colorWhite;
        let textColor = (this.props.activeTab == i) ? Color.colorWhite : Color.blackColor;
        return (
            <View style={[styles.tabItem]}>
                <View
                    style={{backgroundColor:color,alignSelf:'center',width:ScreenWidth/3-11,height:36,borderBottomLeftRadius:borderBottomLeftRadius,borderTopLeftRadius:borderTopLeftRadius,borderBottomRightRadius:borderBottomRightRadius,borderTopRightRadius:borderTopRightRadius}}>
                    <TouchableOpacity
                        style={styles.tabButton}
                        onPress={()=>this.onGoToPage(i)} activeOpacity={0.6}>
                        <Text
                            style={[styles.tabText,{color:textColor,alignSelf:'center'}]}>
                            {this.props.tabNames[i]}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>

        );
    }

    /**
     * 跳转到指定页面的操作
     * @param i
     */
    onGoToPage(i) {
        this.props.goToPage(i)
    }

    render() {
        return (
            <View style={styles.tabs}>
                <View style={{justifyContent:'center',flex:1}}>
                    <TouchableOpacity
                        style={{justifyContent:'center',backgroundColor:Color.transparent,flex:1}}
                        onPress={this.finishPage.bind(this)} activeOpacity={0.6}>
                        <CachedImage style={{width:28,height:16,alignSelf:'center',resizeMode: 'contain',}} source={ImageManager.icMiniGameBack}/>
                    </TouchableOpacity>
                </View>
                <View flexDirection='row'
                      style={{ borderColor:'#6932c3',borderWidth:2,borderRadius:20,width:ScreenWidth/3*2-20,height: 40,alignSelf:'center'}}>
                    {this.props.tabs.map((tab, i) => this.renderTabOption(tab, i))}
                </View>
                <View style={{flex:1}}/>
            </View>
        );
    }

    finishPage() {
        if (!this.state.isFinishing) {
            this.props.finishPage('global');
            this.state.isFinishing = true;
        }
        this.finishTimer = setTimeout(() => {
            this.state.isFinishing = false;
        }, 2000);
    }
}

const styles = StyleSheet.create({
    tabs: {
        flexDirection: 'row',
        height: 65,
        justifyContent: 'center',
    },

    tab: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.footerColor
    },

    tabItem: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    tabButton: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 33,
        width: ScreenWidth / 3 - 2
    },
    tabText: {
        fontSize: 16, textAlign: 'center', fontWeight: '300', backgroundColor: Color.transparent
    }
});

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
        openPage: (route, key) => dispatch(pushRoute({key: route}, key)),
        finishPage: (key) => dispatch(popRoute(key)),
    }
);
export default connect(mapStateToProps, mapDispatchToProps,)(MiniGameTabBar);
