/**
 * Created by wangxu on 2017/6/1.
 */
'use strict';

import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, Text, Image, Platform, Dimensions,ScrollView} from 'react-native';
//
import Color from '../../resources/themColor';
import {connect} from 'react-redux';
//
const ScreenWidth = Dimensions.get('window').width;
class WerewolfTabBar extends Component {
    propTypes = {
        goToPage: React.PropTypes.func, // 跳转到对应tab的方法
        activeTab: React.PropTypes.number, // 当前被选中的tab下标
        tabs: React.PropTypes.array, // 所有tabs集合
        tabNames: React.PropTypes.array, // 保存Tab名称
        tabIconNames: React.PropTypes.array, // 保存Tab图标
    }

    /**
     *
     * @param value
     */
    setAnimationValue({value}) {

    }

    /**
     *
     */
    componentDidMount() {
        this.props.scrollValue.addListener(this.setAnimationValue);
    }

    /**
     *
     * @param tab
     * @param i
     * @returns {XML}
     */
    renderTabOption(tab, i) {
        let color = this.props.activeTab == i ? Color.voice_active_text_color : Color.voice_inactiveTextColor; // 判断i是否是当前选中的tab，设置不同的颜色
        let underlineColor = this.props.activeTab == i ? Color.colorWhite : Color.voice_room_list_bg_color; // 判断i是否是当前选中的tab，设置不同的颜色
        return (
            <TouchableOpacity onPress={()=>this.onGoToPage(i)}
                              activeOpacity={0.8}
                              style={{flex: 1,justifyContent: 'center',backgroundColor: Color.transparent,borderRadius:0}}>
                <View style={styles.tabItem}>
                    <Text style={{color: color,fontSize:14,textAlign:'center',flex:1}}>
                        {this.props.tabNames[i]}
                    </Text>
                    <View style={{borderRadius:2,height:1,width:20,alignSelf:'center',backgroundColor:underlineColor}}/>
                </View>
            </TouchableOpacity>
        );
    }

    /**
     * 跳转到指定页面的操作
     * @param i
     */
    onGoToPage(i) {
        this.props.goToPage(i);
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        return (
            <View style={styles.tabs}>
                <ScrollView
                    style={{flex:1}}
                    ref={(scrollView) => { this._scrollView = scrollView;}}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    directionalLockEnabled={true}
                    bounces={false}
                    scrollsToTop={false}>
                {this.props.tabs.map((tab, i) => this.renderTabOption(tab, i))}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tabs: {
        flexDirection: 'row',
        height: 26,
        backgroundColor: Color.voice_room_list_bg_color,
        width:ScreenWidth
    },
    tabItem: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: 25,
        backgroundColor: Color.voice_room_list_bg_color,
        width:ScreenWidth/6
    },
});

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps,)(WerewolfTabBar);
