/**
 * Created by wangxu on 2017/3/27.
 */
import React, {
    NetInfo, Platform
} from 'react-native';
import Language from '../../resources/language';

const NOT_NETWORK = Language().not_network;
const TAG_NETWORK_CHANGE = "NetworkChange";

/***
 * 检查网络链接状态
 * @param callback
 */
const checkNetworkState = (callback) => {
    try {
        NetInfo.isConnected.fetch().done(
            (isConnected) => {
                callback(isConnected);
            }
        );
    } catch (e) {
        console.log('checkNetworkState error:'+e);
    }
}

/***
 * 移除网络状态变化监听
 * @param tag
 * @param handler
 */
const removeEventListener = (tag, handler) => {
    try {
        NetInfo.isConnected.removeEventListener(tag, handler);
    } catch (e) {
        console.log('removeEventListener error:'+e);

    }
}

/***
 * 添加网络状态变化监听
 * @param tag
 * @param handler
 */
const addEventListener = (tag, handler) => {
    try {
        NetInfo.isConnected.addEventListener(tag, handler);
    } catch (e) {
        console.log('addEventListener error:'+e);
    }
}

export default{
    checkNetworkState,
    addEventListener,
    removeEventListener,
    NOT_NETWORK,
    TAG_NETWORK_CHANGE
}