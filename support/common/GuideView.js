/**
 * Created by wangxu on 2017/8/25.
 * 功能引导页面
 */
import React from 'react';
import {StyleSheet, View, Text, Dimensions, Modal, Platform, Image, TouchableOpacity} from 'react-native';
import rnRoNativeUtils from './rnToNativeUtils';
import InternationSourceManager from '../../resources/internationSourceManager';
import ViewPager from 'react-native-viewpager'
const [width, height] = [Dimensions.get('window').width, Dimensions.get('window').height];
const bg_delete = require('../../resources/imgs/ic_delete.png');
const bg_know = require('../../resources/imgs/ic_know.png');
const bg_next = require('../../resources/imgs/ic_next.png');

const images = [InternationSourceManager().bg_novice_guide_one, InternationSourceManager().bg_novice_guide_two, InternationSourceManager().bg_novice_guide_three, InternationSourceManager().bg_novice_guide_four, InternationSourceManager().bg_novice_guide_five];
let count = 0;
let traditional = false;
export default class GuideView extends React.Component {
    constructor(props) {
        super(props);
        //1.设置数据源
        let ds = new ViewPager.DataSource({
            pageHasChanged: (p1, p2) => p1 !== p2,
        });
        //2.设置返回数据
        this.state = {
            hidden: true,
            page: 0,
            dataSource: ds.cloneWithPages(images),
        };
        this.params = this.params || {...this.props};
    }

    static defaultProps = {
        title: '',
        isTraditional: false
    }
    static propTypes = {
        title: React.PropTypes.string,
        isTraditional: React.PropTypes.bool,
    };

    render() {
        if (this.state.hidden) {
            return null;
        }
        const {
            loadingTitle,
            isTraditional
        } = this.params;
        let imagesData = images;
        return (
            <Modal transparent={true} visible={!this.state.hidden} animationType='fade'>
                <View style={styles.container}>
                    <View style={{flex:1,alignSelf:'flex-end'}}>
                        <TouchableOpacity onPress={this.closeGuide.bind(this)}
                                          style={{alignSelf:'flex-end',marginRight:60,marginTop:30}}>
                            <Image source={bg_delete} style={styles.closeImage}></Image>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.contentView}>
                        <ViewPager
                            ref='viewpager'
                            style={this.props.style}
                            dataSource={this.state.dataSource.cloneWithPages(imagesData)}
                            renderPage={this._renderPage}
                            isLoop={false}
                            autoPlay={false}
                            renderPageIndicator={false}
                            onChangePage={this._onChangePage.bind(this)}/>
                    </View>
                    <View style={{flex:1}}>
                        <TouchableOpacity onPress={this.nextGuide.bind(this)}>
                            <Image source={this.state.page==4?bg_know:bg_next} style={styles.nextImage}></Image>
                        </TouchableOpacity>
                    </View>

                </View>
            </Modal>
        )
    }

    _onChangePage(data) {
        if (data == 4) {
            this.setState({
                page: 4
            })
        }
    }

    closeGuide() {
        this.hide();
        rnRoNativeUtils.onApplyPermission();
    }

    nextGuide() {
        if (this.refs.viewpager) {
            count = count + 1;
            if (count > 4) {
                this.closeGuide();
            } else {
                this.refs.viewpager.goToPage(count);
            }
        }
    }

    _renderPage(data) {
        return (
            <View style={styles.guideContainer}>
                <Image source={data} style={styles.guideView}/>
            </View>
        )
    }

    _lockQueue = [];

    show(params) {
        if (!this.state.hidden && this.lock) {
            this._lockQueue.push(params);
            return;
        }
        this.lock = true;
        this.params = {...this.props, ...params};
        this.setState({hidden: false});
    }

    isShowing() {
        return !this.state.hidden;
    }

    hide() {
        try {
            this.setState({hidden: true});
            this.lock = false;
            this._lockQueue[0] && this.show(this._lockQueue[0]);
            this._lockQueue = this._lockQueue.slice(1);
        } catch (e) {
        }
    }

}
const styles = StyleSheet.create({
    container: {
        width: width,
        height: height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
    },
    contentView: {
        height: width - 100,
        width: width,
        flex: 2
    },
    guideView: {
        width: width - 80,
        alignSelf: 'center',
        height: width - 50,
        resizeMode: 'contain',
    },
    guideContainer: {
        width: width,
        justifyContent: 'center'
    },
    nextImage: {
        width: 130,
        height: 40,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    closeImage: {
        width: 25,
        height: 25,
        alignSelf: 'center',
        resizeMode: 'contain',
    }
})