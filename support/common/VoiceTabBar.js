/**
 * Created by wangxu on 2017/6/1.
 */
'use strict';

import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, Text, Image, Platform, Dimensions} from 'react-native';
//
import Color from '../../resources/themColor';
import Badge from 'react-native-smart-badge';
import {connect} from 'react-redux';
import {actionTab} from '../actions/userInfoActions';
import * as types from '../actions/actionTypes';
import ImageManager from '../../resources/imageManager';
//
const ScreenWidth = Dimensions.get('window').width;
const tabIconWHidth = (Platform.OS === 'ios') ? 28 : 25;
const MaxBadgeCount = 99;
class WerewolfTabBar extends Component {
    propTypes = {
        goToPage: React.PropTypes.func, // 跳转到对应tab的方法
        activeTab: React.PropTypes.number, // 当前被选中的tab下标
        tabs: React.PropTypes.array, // 所有tabs集合
        tabNames: React.PropTypes.array, // 保存Tab名称
        tabIconNames: React.PropTypes.array, // 保存Tab图标
        isTourist: React.PropTypes.bool,
        unReadMessageNumber: React.PropTypes.number,
        actionTab: React.PropTypes.func,
        friendRequestListData: React.PropTypes.object,
        familyChatData: React.PropTypes.object,
    }

    /**
     *
     * @param value
     */
    setAnimationValue({value}) {

    }

    /**
     *
     */
    componentDidMount() {
        this.props.scrollValue.addListener(this.setAnimationValue);
    }

    /**
     *
     * @param tab
     * @param i
     * @returns {XML}
     */
    renderTabOption(tab, i) {
        let color = this.props.activeTab == i ? Color.voice_tab_selected_color : Color.voice_tab_normal_color; // 判断i是否是当前选中的tab，设置不同的颜色
        let tabImage = this.props.activeTab == i ? ImageManager.voiceTabsSelected[i] : this.props.tabIconNames[i];
        let unReadMessageNumber = 0;
        let requestNumber = 0;
        let badgeCount = 0;
        let familyChatCount = 0;
        if (this.props.friendRequestListData && this.props.friendRequestListData.total_user) {
            if (this.props.friendRequestListData.total_user) {
                requestNumber = this.props.friendRequestListData.total_user;
            }
        }
        if (this.props.unReadMessageNumber != 0 && this.props.unReadMessageNumber != null && this.props.unReadMessageNumber != '') {
            unReadMessageNumber = this.props.unReadMessageNumber;
        }
        if (this.props.familyChatData && this.props.familyChatData.unreadNumber) {
            familyChatCount = this.props.familyChatData.unreadNumber;
        }
        badgeCount = unReadMessageNumber + familyChatCount;
        if (badgeCount > MaxBadgeCount) {
            badgeCount = MaxBadgeCount + '+';
        }
        if (i == 1) {
            badgeCount = requestNumber;
        }
        let BadgeView = (badgeCount > 0 && (i == 1 || i == 3)) ?(
                <Badge minWidth={5} minHeight={5} textStyle={{fontSize:9,paddingVertical:1}} extraPaddingHorizontal={4}
                       style={{width:6,height:6, position: 'absolute',left:ScreenWidth/(ImageManager.voiceTabsSelected).length/2+5,top: 3,bottom: 0,right: 0,backgroundColor:Color.voice_badge_color}}>{badgeCount}</Badge>):null;
        return (
            <TouchableOpacity onPress={()=>this.onGoToPage(i)}
                              activeOpacity={0.8}
                              style={{flex: 1,justifyContent: 'center',backgroundColor: Color.transparent,borderRadius:0}}>
                <View style={styles.tabItem}>
                    <Image
                        source={tabImage} // 图标
                        style={styles.tabImage}
                        color={color}/>
                    <Text style={{color: color,fontSize:12,textAlign:'center'}}>
                        {this.props.tabNames[i]}
                    </Text>
                </View>
                {BadgeView}
            </TouchableOpacity>
        );
    }

    /**
     * 跳转到指定页面的操作
     * @param i
     */
    onGoToPage(i) {
        if (!this.props.isTourist) {
            this.props.goToPage(i);
        } else {
            /**
             * 当时游客模式进入时
             */
            if (i == 0) {
                this.props.actionTab(types.ACTION_HOME);
            } else if (i == 1) {
                this.props.actionTab(types.ACTION_RECREATION);
            } else if (i == 2) {
                this.props.actionTab(types.ACTION_MESSAGE);
            } else if (i == 3) {
                this.props.actionTab(types.ACTION_ME);
            }
            this.props.goToPage(0);
        }
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        return (
            <View style={styles.tabs}>
                {this.props.tabs.map((tab, i) => this.renderTabOption(tab, i))}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tabs: {
        flexDirection: 'row',
        height: 55,
        backgroundColor: Color.colorWhite
    },
    tabItem: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: 55,
        backgroundColor: Color.colorWhite
    },
    tabImage: {
        width: tabIconWHidth,
        height: tabIconWHidth,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginBottom: 2
    }
});

const mapStateToProps = state => ({
    unReadMessageNumber: state.chatReducer.messageNumber,
    isTourist: state.userInfoReducer.isTourist,
    friendRequestListData: state.messageReducer.data,
    familyChatData: state.familyChatReducer.data,
});
const mapDispatchToProps = dispatch => ({
        actionTab: (tabName) => dispatch(actionTab(tabName))
    }
);
export default connect(mapStateToProps, mapDispatchToProps,)(WerewolfTabBar);
