/**
 *
 */
'use strict';
import {AsyncStorage, Platform} from 'react-native';
import Language from '../../resources/language';
import DeviceInfo from 'react-native-device-info';
import Moment from 'moment';
import AsyncStorageTool from './AsyncStorageTool';
import  * as apiDefines from './gameApiDefines';
import rnRoNativeUtils from './rnToNativeUtils';
let deviceId = '';
let devicePid = '';
const sv = 9;
let Util = {
    /**
     * http get 请求简单封装
     * @param url 请求的URL
     * @param successCallback 请求成功回调
     * @param failCallback 请求失败回调
     */

    get: (url, successCallback, failCallback) => {
        let accessToken = '';
        try {
            AsyncStorageTool.getStorageData(AsyncStorageTool.USER_INFO, (data) => {
                if (data != null && data.token != null && data.token != apiDefines.DEFAULT_ACCESS_TOKEN) {
                    accessToken = data.token.access_token;
                }
                //accessToken='TX23k1JJ63xCXqfSY-LsgB1qdqBmaIWhOMj9gEEembngh7UWdoJo3TG2U5p1gFQfdgSLNswZWdLGecr9y0L01dMy_AfSBBc_LNh1wNo2wF0'
                //accessToken='5-RWP5T-79mVdusJcw1DAFJXNmulsa2tZBDP3nhmnSrEqlGyEPpeNUdxfVRze6kdCrHZ7xfGhyodOUH27wBeO0s1b3FR0xxHSe2HSisFiiw'
                let fetchOptions = {
                    headers: {
                        'x-access-token': accessToken,
                        'lg': DeviceInfo.getDeviceLocale(),
                        'v': DeviceInfo.getBuildNumber(),
                        'sv': sv,
                        'tz': Moment(new Date()).format('Z'),
                        'pt': (Platform.OS === 'ios') ? 'ios' : 'android',
                        'did': deviceId,
                        'app': apiDefines.APP_KEY,
                        'pid': devicePid
                    },
                };
                let isTimeOut = true;
                setTimeout(() => {
                    if (isTimeOut) {
                        isTimeOut = false;
                        successCallback(1101, Language().time_out, null);
                    }
                }, 30000);
                fetch(apiDefines.URL_HOST + url, fetchOptions)
                    .then((response) => response.text())
                    .then((responseText) => {
                        let result = JSON.parse(responseText);
                        if (isTimeOut) {
                            successCallback(result.code, result.message, result.data);
                        }
                        isTimeOut = false;
                    })
                    .catch((err) => {
                        if (isTimeOut) {
                            failCallback(err);
                        }
                        isTimeOut = false;
                    });
            });
        } catch (error) {
        }
    },

    /**
     * http post 请求简单封装
     * @param url 请求的URL
     * @param data post的数据
     * @param successCallback 请求成功回调
     * @param failCallback failCallback 请求失败回调
     */
    post: (url, data, successCallback, failCallback) => {
        let accessToken = '';
        try {
            AsyncStorageTool.getStorageData(AsyncStorageTool.USER_INFO, (userInfo) => {
                if (userInfo != null && userInfo.token != null && userInfo.token != apiDefines.DEFAULT_ACCESS_TOKEN) {
                    accessToken = userInfo.token.access_token;
                }
                let fetchOptions = {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'x-access-token': accessToken,
                        'lg': DeviceInfo.getDeviceLocale(),
                        'v': DeviceInfo.getBuildNumber(),
                        'sv': sv,
                        'tz': Moment(new Date()).format('Z'),
                        'pt': (Platform.OS === 'ios') ? 'ios' : 'android',
                        'did': deviceId,
                        'app': apiDefines.APP_KEY,
                        'pid': devicePid
                    },
                    body: JSON.stringify(data)
                };
                let isTimeOut = true;
                setTimeout(() => {
                    if (isTimeOut) {
                        isTimeOut = false;
                        successCallback(1101, Language().time_out, null);
                    }
                }, 30000);
                fetch(apiDefines.URL_HOST + url, fetchOptions)
                    .then((response) => response.text())
                    .then((responseText) => {
                        let result = JSON.parse(responseText);
                        if (isTimeOut) {
                            successCallback(result.code, result.message, result.data);
                        }
                        isTimeOut = false;
                        console.log('result:' + responseText);
                    })
                    .catch((err) => {
                        console.log('error:' + err);
                        if (isTimeOut) {
                            failCallback(err);
                        }
                        isTimeOut = false;
                    });
            });
        } catch (error) {
            console.log('失败' + error);
        }

    },

    /**
     * 日志打印
     * @param obj
     */
    log: (obj) => {
        var description = "";
        for (let i in obj) {
            let property = obj[i];
            description += i + " = " + property + "\n";
        }
        alert(description);
    },
    /**
     * 设置设备ID
     */
    setDeviceId: () => {
        rnRoNativeUtils.getDeviceId((did) => {
            deviceId = did;
        });
    },
    setDevicePid: () => {
        rnRoNativeUtils.getDevicePid((pid) => {
            devicePid = pid;
        });
    }
};

export default Util;