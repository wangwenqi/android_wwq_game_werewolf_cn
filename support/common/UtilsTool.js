/**
 * Created by wangxu on 2017/11/1.
 * 工具类
 */
import ImageManager from '../../resources/imageManager';
import * as Constant from '../../support/common/constant';

const fileHeader1 = 'file:';
const fileHeader2 = 'file://';
const filePathFormat = (fileHeader, filePath, formatPath) => {
    let newFilePath = filePath;
    if (newFilePath && fileHeader) {
        if (newFilePath.indexOf(fileHeader) >= 0) {
            newFilePath = newFilePath.replace(fileHeader, '');
        }
    }
    formatPath(newFilePath);
}
const regroupFilePath = (path, newPath) => {
    let regroupPath = path;
    if (regroupPath) {
        if (regroupPath.indexOf(fileHeader1) < 0) {
            regroupPath = fileHeader2 + regroupPath;
        }
    }
    newPath(regroupPath);
}
/**
 * 获取用户等级对应的图片资源
 * @param type
 * @param level
 * @param source
 */
const userLevelSource = (type, source) => {
    let levelSource;
    switch (type) {
        case Constant.VILLAGER:
            //村民
            levelSource = ImageManager.villager;
            break;
        case Constant.VILLAGE_HEAD:
            //老村长
            levelSource = ImageManager.village_head;
            break;
        case Constant.SHERIFF:
            //警长
            levelSource = ImageManager.sheriff;
            break;
        case Constant.GUARD:
            //守卫
            levelSource = ImageManager.guard;
            break;
        case Constant.HUNTER:
            //猎人
            levelSource = ImageManager.hunter;
            break;
        case Constant.PALADIN:
            //骑士
            levelSource = ImageManager.paladin;
            break;
        case Constant.WITCH:
            //女巫
            levelSource = ImageManager.witch;
            break;
        case Constant.SEER:
            //预言家
            levelSource = ImageManager.seer;
            break;
        case Constant.PRIEST:
            //大祭司
            levelSource = ImageManager.priest;
            break;
        case Constant.BISHOP:
            //主教
            levelSource = ImageManager.bishop;
            break;
        case Constant.RED_BISHOP:
            //红衣主教
            levelSource = ImageManager.red_bishop;
            break;
        case Constant.PONTIFF:
            //教皇
            levelSource = ImageManager.pontiff;
            break;
        case Constant.ANGEL:
            //天使
            levelSource = ImageManager.angel;
            break;
        case Constant.DEMON:
            //恶魔
            levelSource = ImageManager.demon;
            break;
        case Constant.CUPID:
            //爱神
            levelSource = ImageManager.cupid;
            break;
        case Constant.SUN_GOD:
            //太阳神
            levelSource = ImageManager.sun_god;
            break;
        case Constant.PLUTO:
            //冥王
            levelSource = ImageManager.pluto;
            break;
        case Constant.ZEUS:
            //宙斯
            levelSource = ImageManager.zeus;
            break;

    }
    source(levelSource);
}
export default {
    filePathFormat,
    fileHeader2,
    fileHeader1,
    regroupFilePath,
    userLevelSource
}