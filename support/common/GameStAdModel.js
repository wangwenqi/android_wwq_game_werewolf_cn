/**
 * Created by wangxu on 2017/6/1.
 */
'use strict';

import React, {Component} from 'react';
import Color from '../../resources/themColor';
import Language from '../../resources/language';
import {connect} from 'react-redux';

import {
    StyleSheet, View, TouchableOpacity, Text, Image, Platform, Dimensions, ScrollView
} from 'react-native';
const ScreenWidth = Dimensions.get('window').width;
const lines = require('../../resources/imgs/ic_lines.png');
const roleSergeant = require('../../resources/imgs/role_sergeant.png');
class GameStAdModel extends Component {
    componentDidMount() {
    }

    render() {
        return (
            <View style={styles.view}>
                <ScrollView alwaysBounceVertical={false} showsVerticalScrollIndicator={false}>
                    <Text
                        style={{flex:1,fontSize:12,color:'#fffd45',marginTop:10,alignSelf:'center'}}>{Language().sergeant_detail}</Text>

                    <View flexDirection='row'>
                        <View style={{flex:1.5}}>
                            <Image style={styles.roleImage} source={roleSergeant}/>
                        </View>
                        <View style={{flex:3,marginTop:3}}>
                            <View style={{marginRight:10}}>
                                <Text style={{fontSize:14,color:'#a5d4fc'}}>{Language().sergeant}</Text>
                                <Text
                                    style={{flex:1,fontSize:12,color:Color.colorWhite}}>{Language().role_sergeant_detail}</Text>
                            </View>
                        </View>
                    </View>
                    <View flexDirection='row' style={{marginTop:10}}>
                        <Image style={styles.lines} source={lines}/>
                        <Text
                            style={{flex:1.5,alignSelf:'center',fontSize:15,color:Color.colorWhite,textAlign:'center',fontWeight: 'bold'}}>{Language().game_model_standard}</Text>
                        <Image style={styles.lines} source={lines}/>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row'>
                            <Text style={{color:Color.colorWhite,padding:1,flex:1}}>{Language().normal_cupid}</Text>
                            <View
                                style={{backgroundColor:'#a5d4fc',padding:1,borderRadius:3,marginLeft:0,height:20,justifyContent:'center'}}>
                                <Text style={{fontSize:11,textAlign:'center'}}>{Language().jing_xuan}</Text>
                            </View>
                            <View
                                style={{backgroundColor:'#ffc000',padding:1,borderRadius:3,marginLeft:5,height:20,justifyContent:'center'}}>
                                <Text style={{fontSize:11,textAlign:'center'}}>{Language().tu_bian}</Text>
                            </View>
                        </View>
                        <Text style={{color:Color.colorWhite}}>{Language().normal_cupid_rule1}</Text>
                        <Text style={{color:Color.colorWhite}}>{Language().normal_cupid_rule2}</Text>
                        <Text style={{color:Color.colorWhite}}>{Language().normal_cupid_rule3}</Text>
                        <Text style={{color:'#fffd45',fontSize:12}}>{Language().normal_cupid_rule4}</Text>

                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row'>
                            <Text style={{color:Color.colorWhite,padding:1,flex:1}}>{Language().normal_guard}</Text>
                            <View
                                style={{backgroundColor:'#a5d4fc',padding:1,borderRadius:3,marginLeft:0,height:20,justifyContent:'center'}}>
                                <Text style={{fontSize:11,textAlign:'center'}}>{Language().jing_xuan}</Text>
                            </View>
                            <View
                                style={{backgroundColor:'#ffc000',padding:1,borderRadius:3,marginLeft:5,height:20,justifyContent:'center'}}>
                                <Text style={{fontSize:11,textAlign:'center'}}>{Language().tu_bian}</Text>
                            </View>
                        </View>
                        <Text style={{color:Color.colorWhite}}>{Language().normal_guard_rule1}</Text>
                        <Text style={{color:Color.colorWhite}}>{Language().normal_guard_rule2}</Text>
                        <Text style={{color:'#fffd45',fontSize:12}}>{Language().normal_guard_rule3}</Text>

                    </View>
                    <View flexDirection='row' style={{marginTop:10}}>
                        <Image style={styles.lines} source={lines}/>
                        <Text
                            style={{flex:1.5,alignSelf:'center',fontSize:15,color:Color.colorWhite,textAlign:'center',fontWeight: 'bold'}}>{Language().game_model_advanced}</Text>
                        <Image style={styles.lines} source={lines}/>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row'>
                            <Text style={{color:Color.colorWhite,padding:1,flex:1}}>{Language().high_king}</Text>
                            <View
                                style={{backgroundColor:'#a5d4fc',padding:1,borderRadius:3,marginLeft:0,height:20,justifyContent:'center'}}>
                                <Text style={{fontSize:11,textAlign:'center'}}>{Language().jing_xuan}</Text>
                            </View>
                            <View
                                style={{backgroundColor:'#ffc000',padding:1,borderRadius:3,marginLeft:5,height:20,justifyContent:'center'}}>
                                <Text style={{fontSize:11,textAlign:'center'}}>{Language().tu_bian}</Text>
                            </View>
                        </View>
                        <Text style={{color:Color.colorWhite}}>{Language().high_king_rule1}</Text>
                        <Text style={{color:Color.colorWhite}}>{Language().high_king_rule2}</Text>
                        <Text style={{color:'#fffd45',fontSize:12}}>{Language().high_king_rule3}</Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        margin: 15,
        borderRadius: 3,
        backgroundColor: '#6614bc',
        flex: 1, padding: 5
    },
    lines: {
        height: 5,
        resizeMode: 'contain',
        flex: 1, alignSelf: 'center'
    },
    itemView: {
        borderRadius: 5,
        margin: 10,
        borderColor: '#b87fff',
        borderWidth: 1, padding: 5
    },
    roleImage: {
        height: 80,
        width: 70,
        resizeMode: 'contain',
        alignSelf: 'center',
        margin: 5
    },
});

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps,)(GameStAdModel);
