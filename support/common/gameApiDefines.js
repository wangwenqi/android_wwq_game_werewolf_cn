/**
 * Created by wangxu on 2017/3/24.
 */
import AsyncStorageTool from '../../support/common/AsyncStorageTool';
import * as HostUrl from '../../support/common/HostUrl';

export const WEREWOLF = 'werewolf_cn';//（快玩小游戏吧）
//---------------------------支持的三个type（兼容android）---------------------
const APP_TYPE_COVER_WEREWOLF = 'cover_werewolf';//(覆盖国内狼人杀)
const APP_TYPE_COVER_INTVIU = 'cover_intviu';//（覆盖橙云面试）
const APP_TYPE_KUAIWAN = 'kuaiwan';//（快玩小游戏吧）
//---------------------------设置type／host／appKey--------------------------
export const APP_TYPE = APP_TYPE_KUAIWAN;//(国内版本3个之间配置切换)TODO 修改此处用于android版本（3个之间切换）

let URL_HOST_WEREWOLF = HostUrl.URL_HOST_GAME_WEREWOLF_DEV;
export const APP_KEY = WEREWOLF;
//---------------------------微信appID---------------------------------------
//微信appID
export const WECHAT_PLATFORM = {};
WECHAT_PLATFORM[APP_TYPE_KUAIWAN] = 'wxa214fc8471ec6cc6';//快玩小游戏吧微信appID
WECHAT_PLATFORM[APP_TYPE_COVER_INTVIU] = 'wx593f3f7ae6585587';//覆盖国内橙云面试
WECHAT_PLATFORM[APP_TYPE_COVER_WEREWOLF] = 'wxa7b819d69796b241';//覆盖国内狼人杀
//---------------------------第三方登录回调接口---------------------------------
//login_callback
const LOGIN_CALLBACK = {};
LOGIN_CALLBACK[APP_TYPE_COVER_WEREWOLF] = '';
LOGIN_CALLBACK[APP_TYPE_KUAIWAN] = '_werewolf_cn';
LOGIN_CALLBACK[APP_TYPE_COVER_INTVIU] = '_wcn_cy';
//---------------------------服务器地址----------------------------------------
export let URL_HOST = URL_HOST_WEREWOLF;
//---------------------------------------------------------------------------

//读取developer设置的host
AsyncStorageTool.getStorageData(AsyncStorageTool.HOST_URL, (hostData) => {
    if (hostData) {
        if (hostData.host) {
            URL_HOST = hostData.host;
        }
    }
});
//---------------------------------接口参数------------------------------------
//微信登录
export const GET_USER_INFO = '/auth/weixin/callback' + LOGIN_CALLBACK[APP_TYPE] + '?code=';
//QQ登录
export const GET_USER_INFO_FROM_QQ = '/auth/qq/callback' + LOGIN_CALLBACK[APP_TYPE] + '?access_token=';
export const EXPIRES_IN = '&expires_in=';
export const OAUTH_CONSUMER_KEY = '&oauth_consumer_key=';
export const OPENID = '&openid=';
//Facebook登录
export const GET_USER_INFO_FROM_FACEBOOK = '/auth/facebook' + LOGIN_CALLBACK[APP_TYPE] + '?access_token=';
//line登录
export const GET_USER_INFO_FROM_LINE = '/auth/line';
//搜索房间
export const SEARCH_ROOM_INFO = '/server/get?room_id=';
//创建房间
export const CREATE_ROOM = '/room/create_v2?type=';
export const ROOM_PASSWORD = '&password=';
//获取好友
export const GET_FRIEND_LIST = '/friend/list';
//分页获取好友信息
export const GET_FRIEND_PAGE_LIST = '/friend/pageList';
export const PAGE = '?page=';
//获取消息列表
export const GET_MESSAGE_LIST = '/friend/received';
//分页获取好友请求
export const GET_FRIEND_REQUEST_LIST = '/friend/requestList';
//同意加好友
export const ACCEPT_FRIEND = '/friend/accept?friend_id=';
//拒绝加好友
export const REJECT_FRIEND = '/friend/reject?friend_id=';
//获取用户详情
export const GET_FRIEND_INFO = '/user/info?user_id=';
export const GET_FRIEND_INFO_FROM_UID = '/user/info?uid=';
//系统消息提示
export const GET_SYSTEM_MESSAGE = '/game/warning';
export const GET_SEVER_MESSAGE = '/server/warning';
//是否开放游客登录
export const GET_OTHER_LOGIN_VISIBLE = '/config?platform=';
//手机号登录
export const LOGIN_FROM_PHONE_POST = '/account/login';
export const LOGIN_FROM_PHONE = '/account/login?phone=';
export const PASSWORD = '&password=';
//手机注册
export const REGISTER = '/account/register?phone=';
//export const REGISTER = URL_HOST + '/account/register';
export const NAME = '&name=';
export const CODE = '&code=';
export const GENDER = '&gender='
//忘记密码
export const CHANGE_PASSWORD = '/account/changePassword?phone=';
//注册获取code
export const GET_REGISTER_CODE = '/account/regCode?phone=';
//修改密码获取code
export const GET_CHANGE_PASSWORD_CODE = '/account/changeCode?phone=';
//短信校验
export const CHECK_VERIFY = '/account/verify?phone=';
//用户协议
export const USER_AGREEMENT = URL_HOST + '/aggrement?s=' + APP_KEY;//TODO 需要
//修改用户信息
export const CHANGE_USER_INFO = '/account/changeProfile?name=';
export const AVATAR = '&avatar=';
//个性签名
export const SIGNATURE = '&signature=';
export const HOST = '&host=';
//图片上传后在leancloud上存储数据的ID
export const OBJECTID = '&objectId=';
export const UPDATE_PHOTOS = '/account/updatePhotos';
export const DEFAULT_AVATAR = 'http://werewolf.bj.bcebos.com/default_avatar/';
//操作的上报服务器
export const TASK_DO = '/task/do?type=';
export const CONTINUE_LOGIN = 'continue_login';
export const LEANCLOUD_DONE = '&host=';
//删除好友
export const DELETE_FRIEND = '/friend/delete?friend_id=';
//Facebook adv config
export const FACEBOOK_ADV_CONFIG = '/config/gg';
//游客token
export const DEFAULT_ACCESS_TOKEN = 'c988abf458ff7b14907e1ecc8c397ac9cb9b6470';
export const DEFAULT_USER_ID = '5924f2339d70830006a5fb8f';
//攻略
export const URL_BLOG = URL_HOST + '/blog';//TODO 需要
//用戶状态
export const USER_STATUS = '/user/status/users/';
//
export const PARAMETER_LOCATION = '&lc=';

export const NOTICE_URL = URL_HOST + '/notice?access_token=';//TODO 需要
//audio------------------------
export const AUDIO_URL = URL_HOST + '/audio_room?access_token=';//TODO 需要
//获取音频房间列表
export const VOICE_ROME_PAGE_LIST = '/room/audioList?limit=10&page='
export const PARAMETER_ORDERBY = '&orderBy=';
export const PARAMETER_CHILDTYPE = '&childType=';
//
export const SERVER_GET = '/server/get?room_id=';
export const PARAMETER_USER_ID = '&user_id=';
export const PARAMETER_LIMIT_LEVEL = '&limit_level=';
//audio create
export const CREATE_AUDIO_ROOM = '/room/create_v2'
//---------------------
export const GET_SIMPLE_USERINFO = '/user/info/simple/';
//获取在线好友列表
export const GET_FRIEND_STATUSLIST = '/friend/statuslist/';
//是否需要获取用户详信息
export const PARAMETER_WITHINFO = '?withInfo=';
//好友是否更新
export const FRIEND_VERSION = '/friend/version';
//家族创建
export const GROUP_CREATE = '/group/create';
//家族列表
export const GROUP_LIST = '/group/list_v2?limit=10&skip=';
//申请加入家族
export const APPLY_JOIN_FAMILY = '/group/member/join/';
//完成的任务
export const TASK_INFO = '/task/info';
//语音任务
export const XIAOYU_TASK = URL_HOST + '/audioapp/task';//TODO 需要
//家族详情
export const GROUP_INFO = '/user/group/info';
//根据ID查查找家族
export const GROUP_INFO_FROM_ID = '/group/find/gid/';
//一键清除清除好友请求
export const CLEAR_REQUEST_FRIEND = '/friend/clearReceives';
//收到礼物排行榜
export const GET_RANKING_LIST = '/rank/d/rir/g?skip=0&limit=3';
//广告运营
export const GET_OPERA_LIST = '/opera/index';
//推荐有趣的人
export const GET_RECOMMEND_USER = '/room/recommand?type=audio&maxCount=20&exclude_room_ids=';
//随机获取语音房间
export const GET_AUDIO_RANDOM_ROOM = '/room/random?type=audio&child_type=';
//战力详情
export const RANK_INSTRUCTION = URL_HOST + '/rank_instruction';//TODO 需要
//语音房间排行
export const RANK_AUDIO = URL_HOST + '/rank_audio';//TODO 需要
//支持游戏列表
export const GAME_LIST = '/game/list';
//我的房间
export const ROOM_OWNED_LIST = '/room/owned';
//申请房契
export const ROOM_PURCHASE = '/room/purchase?room_id=';
export const PARAMS_PEER = '&peer=';
//转移房契
export const ROOM_HANDOVER = '/room/handover?room_id=';
//房间中的人
export const ROOM_USERS = '/room/users?room_id=';
//分享小游戏
export const SHARE_MINI_GAME = '/share/game_kuaiwan?gametype=';
//我的对战列表
export const MINI_GAME_REECORDS = '/game/mini_game_reecords?limit=20&skip=';
//打开魔幻变变变
export const OPEN_CHANGE_FACE = '/change_face';

