/**
 * Created by wangxu on 2017/3/18.
 */
'use strict';
import React, {Component, PropTypes, Children} from 'react';
import {
    StyleSheet, Dimensions,
    Text, View, TouchableWithoutFeedback, TextInput, ScrollView,
    BackAndroid, Modal
} from 'react-native';
const StyleSheetPropType = PropTypes.instanceOf(StyleSheet);

export default class Dialog extends Component {
    constructor(props) {
        super(props);
        this.state = {hidden: true};
        this.onCancel = this.props.onCancel.bind(this);
        this.params = this.params || {...this.props, onCancel: this.onCancel,};
    }

    static propTypes = {
        title: PropTypes.string,
        message: PropTypes.string,
        negText: PropTypes.string,
        onNegClick: PropTypes.func,
        posText: PropTypes.string,
        onPosClick: PropTypes.func,
        onCancel: PropTypes.func,
        dialogStyle: StyleSheetPropType,
        modalStyle: StyleSheetPropType,
        contentStyle: StyleSheetPropType,
        titleStyle: StyleSheetPropType,
        messageStyle: StyleSheetPropType,
        buttonBarStyle: StyleSheetPropType,
        buttonStyle: StyleSheetPropType,
        progress: PropTypes.boolean,
        rowSeparator: PropTypes.boolean,
        enumeSeparator: PropTypes.boolean,
    };
    static defaultProps = {
        posText: 'OK',
        progress: false,
        rowSeparator: false,
        enumeSeparator: false,
        onNegClick: function () {
            this.hide();
        },
        onPosClick: function () {
            this.hide();
        },
        onCancel: function () {
            //this.hide();
            return true;
        },
    };

    render() {
        if (this.state.hidden) {
            return null;
        }
        const {
            title,
            message,
            children,
            onCancel,
            negText,
            onNegClick,
            posText,
            onPosClick,
            dialogStyle,
            modalStyle,
            contentStyle,
            titleStyle,
            messageStyle,
            buttonBarStyle,
            buttonStyle,
            progress,
            rowSeparator,
            enumeSeparator
        } = this.params;
        if (this.props.progress) {
            return (<View style={[styles.dialog, dialogStyle]}>
                <TouchableWithoutFeedback onPress={onCancel}>
                    <View style={[styles.dialogModal, modalStyle]}></View>
                </TouchableWithoutFeedback>
                <View style={[styles.dialogContent, contentStyle]}>
                    {title ? <Text style={[styles.dialogTitle, titleStyle]}>{title}</Text> : null}
                    <ScrollView>{
                        message ? <Text style={[styles.dialogMessage, messageStyle]}>{message}</Text>
                            : children ? Children.map(children, (children) => children) : null
                    }</ScrollView>
                    <View style={[styles.dialogButtonBar, buttonBarStyle]}>
                        {negText ? <Text
                                style={[styles.dialogButton, buttonStyle]}
                                onPress={onNegClick.bind(this)}>
                                {negText}
                            </Text> : null}
                        <Text style={[styles.dialogButton, buttonStyle]}
                              onPress={onPosClick.bind(this)}>{posText}</Text>
                    </View>
                </View>
            </View>);
        } else {
            return (
                <Modal transparent={true} visible={!this.state.hidden}><View style={[styles.dialog, dialogStyle]}>
                    <TouchableWithoutFeedback onPress={onCancel}>
                        <View style={[styles.dialogModal, modalStyle]}></View>
                    </TouchableWithoutFeedback>
                    <View style={[styles.dialogContent, contentStyle]}>
                        {title ? <Text style={[styles.dialogTitle, titleStyle]} numberOfLines={1}>{title}</Text> : null}
                        <ScrollView>{
                            message ? <Text style={[styles.dialogMessage, messageStyle]}>{message}</Text>
                                : children ? Children.map(children, (children) => children) : null
                        }</ScrollView>
                        {this.props.rowSeparator ? <View style={styles.rowSeparator}/> : null}
                        <View style={[styles.dialogButtonBar, buttonBarStyle]}>
                            {negText ? <Text
                                    style={[styles.dialogButton, buttonStyle]}
                                    onPress={onNegClick.bind(this)}>
                                    {negText}
                                </Text> : null}
                            {this.props.enumeSeparator ? <View style={styles.enumeSeparator}/> : null}
                            <Text style={[styles.dialogButton, buttonStyle]}
                                  onPress={onPosClick.bind(this)}>{posText}</Text>
                        </View>
                    </View>
                </View>
                </Modal>
            );
        }
    }

    _lockQueue = [];

    show(params) {
        // this.state.hidden || this.hide();
        if (!this.state.hidden && this.lock) {
            // console.warn('This dialog has already shown.');
            // return;
            this._lockQueue.push(params);
            return;
        }
        this.lock = true;
        this.params = {...this.props, onCancel: this.onCancel, ...params,};
        this.setState({hidden: false,});
        BackAndroid.addEventListener('cancelDialog', this.params.onCancel);
    }

    /**
     * 返回当前是否正在弹框提示
     * @returns {boolean}
     */
    isShowing() {
        return !this.state.hidden;
    }

    hide() {
        try {
            this.setState({hidden: true});
            this.lock = false;
            this._lockQueue[0] && this.show(this._lockQueue[0]);
            this._lockQueue = this._lockQueue.slice(1);
            BackAndroid.removeEventListener('cancelDialog', this.params.onCancel);
        } catch (e) {
            console.log('hide dialog error:' + e);
        }
    }

    alert(message, params) {
        this.show({
            title: 'Alert',
            message: message,
            ...params,
        });
    }

    confirm(message, params) {
        this.show({
            title: 'Confirm',
            message: message,
            negText: 'cancel',
            ...params,
        })
    }

    prompt(message, placeholder, onChangeText, params) {
        this.show({
            title: 'Prompt',
            negText: 'Cancel',
            children: [
                <Text
                    style={[styles.dialogMessage, params && params.messageStyle || this.props.messageStyle]}>{message}</Text>,
                <TextInput
                    style={{ height: 40 }}
                    placeholder={placeholder}
                    onChangeText={onChangeText && onChangeText.bind(this)}
                />
            ],
            ...params,
        })
    }

    static inject(ref) {
        return <Dialog ref={ref || 'dialog'}></Dialog>;
    }
};
export class Alert extends Dialog {
    static defaultProps = {
        ...Dialog.defaultProps,
        title: 'Alert',
    };

    static inject(ref) {
        return <Alert ref={ref || 'alert'}></Alert>
    }
}
;
export class Confirm extends Dialog {
    static defaultProps = {
        ...Dialog.defaultProps,
        title: 'Confirm',
        negText: 'Cancel',
        rowSeparator: false,
        enumeSeparator: false,
    };

    static inject(ref) {
        return <Confirm ref={ref || 'confirm'}></Confirm>
    }
}
;
export class Prompt extends Dialog {
    static propTypes = {
        placeholder: PropTypes.string,
        onChangeText: PropTypes.func,
        progress: PropTypes.boolean,
    };
    static defaultProps = {
        ...Dialog.defaultProps,
        title: 'Prompt',
        message: undefined,
        children: [, ,],
        negText: 'Cancel',
        progress: false,
    };

    render() {
        const {children, placeholder, onChangeText, messageStyle,} = this.params;
        if (children.length == 2 && children[0] === undefined && children[1] === undefined) {
            const _shouldComponentUpdate = this.shouldComponentUpdate;
            this.shouldComponentUpdate = () => false;
            children[1] = <Text style={[styles.dialogMessage, messageStyle]}>undefined</Text> ,
                children[1] = <TextInput
                    style={{ height: 40 }}
                    placeholder={placeholder || ''}
                    onChangeText={onChangeText && onChangeText.bind(this)}
                />;
            this.shouldComponentUpdate = _shouldComponentUpdate;
        }
        return super.render();
    }

    static inject(ref) {
        return <Prompt ref={ref || 'prompt'}></Prompt>
    }
}
;

const [width, height] = [Dimensions.get('window').width, Dimensions.get('window').height];
const styles = StyleSheet.create({
    dialog: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        height: height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.65)',
    },
    dialogModal: {
        zIndex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        backgroundColor: '#000',
        opacity: 0.3,
        width: width,
        height: height,
    },
    dialogContent: {
        zIndex: 1,
        backgroundColor: '#fff',
        borderRadius: 6,
        paddingTop: 10,
        paddingLeft: 20,
        paddingBottom: 10,
        paddingRight: 20,
        width: width - 48,
        maxHeight: height / 3 * 2,
    },
    dialogTitle: {
        color: '#000',
        fontSize: 21,
        padding: 9,
    },
    dialogMessage: {
        color: '#000',
        fontSize: 16,
        padding: 9,
    },
    dialogButtonBar: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    rowSeparator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: 'lightgray'
    },
    enumeSeparator: {
        width: StyleSheet.hairlineWidth,
        backgroundColor: 'lightgray'
    },
    dialogButton: {
        textAlign: 'right',
        color: '#197',
        fontSize: 15,
        padding: 12,
        marginLeft: 24,
        marginRight: 8,
    },
});