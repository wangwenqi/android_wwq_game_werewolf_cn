/**
 * Created by wangxu on 2017/7/19.
 */
import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions, Animated, Easing, Modal, TouchableOpacity, Image
} from 'react-native';
import Language from '../../resources/language';
import Color from '../../resources/themColor';
import ImageManager from '../../resources/imageManager';
const [width, height] = [Dimensions.get('window').width, Dimensions.get('window').height];

export default class LoadingLike extends React.Component {
    constructor(props) {
        super(props);
        this.spinValue = new Animated.Value(0);
        this.state = {hidden: true};
        this.params = this.params || {...this.props};
    }

    static defaultProps = {
        loadingTitle: Language().operation_loading
    }
    static propTypes = {
        onPress: React.PropTypes.func,
    };

    componentDidMount() {
        this.spin();
    }

    render() {
        if (this.state.hidden) {
            return null;
        }
        const {
            loadingTitle,
            contentStyle,
            container
        } = this.params;
        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        });
        return (
            <Modal visible={true} transparent={true}>
                <View style={[styles.container]}>
                    <View
                        style={{position: 'absolute',bottom: 0,left: 0,right: 0,top: 0,justifyContent:'center',alignItems:'center'}}>

                        <Animated.Image
                            style={{width: 300,height: 300,transform: [{rotate: spin}]}}
                            source={ImageManager.ico_like_round_3}/>
                    </View>
                    <View
                        style={{position: 'absolute',bottom: 0,left: 0,right: 0,top: 0,justifyContent:'center',alignItems:'center'}}>
                        <Animated.Image
                            style={{width: 200,height: 200,transform: [{rotate: spin}],}}
                            source={ImageManager.ico_like_round_2}/>
                    </View>
                    <View
                        style={{position: 'absolute',bottom: 0,left: 0,right: 0,top: 0,justifyContent:'center',alignItems:'center'}}>

                        <Animated.Image
                            style={{width: 100,height: 100,transform: [{rotate: spin}]}}
                            source={ImageManager.ico_like_round_1}/>
                    </View>
                </View>
                <TouchableOpacity
                    style={{justifyContent:'center',backgroundColor:Color.transparent,position: 'absolute',left: 0,top: 0}}
                    activeOpacity={this.props.onPress ? 0.3 : 1}
                    onPress={this.clickHandle.bind(this)}>
                    <View style={{width:50,height:50,justifyContent:'center'}}>
                        <Image source={ImageManager.icBack} style={styles.backStyle}/>
                    </View>
                </TouchableOpacity>
            </Modal>
        )
    }

    _lockQueue = [];

    clickHandle() {
        if (this.props.onPress) {
            this.props.onPress();
        }
    }

    show(params) {
        if (!this.state.hidden && this.lock) {
            this._lockQueue.push(params);
            return;
        }
        this.lock = true;
        this.params = {...this.props, ...params};
        this.setState({hidden: false});
    }

    isShowing() {
        return !this.state.hidden;
    }

    hide() {
        try {
            this.setState({hidden: true});
            this.lock = false;
            this._lockQueue[0] && this.show(this._lockQueue[0]);
            this._lockQueue = this._lockQueue.slice(1);
        } catch (e) {
        }
    }

    /**
     * 旋转动画
     */
    spin = () => {
        this.spinValue.setValue(0);
        Animated.timing(
            this.spinValue,
            {
                toValue: 1,
                duration: 5000,
                easing: Easing.linear
            }
        ).start(() => this.spin())
    }

}
const styles = StyleSheet.create({
    container: {
        width: width,
        height: height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.voice_set_text_color,
    }, backStyle: {
        width: 15,
        height: 15,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
})