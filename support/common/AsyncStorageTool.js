/**
 * Created by wangxu on 2017/4/17.
 * 用户统一管理本地存储数据
 */
import {AsyncStorage} from 'react-native';
import rnRoNativeUtils from './rnToNativeUtils';
import * as Constant from './constant';

const USER_INFO = 'userInfo';
const SYSTEM_MESSAGE = 'system_message';
//国家
const COUNTRY_DATA = 'country_data';
//游客标记
const TOURIST_DATA = 'tourist_data';
//等级好评
const PROMOTED_DATA = 'promoted_data';
//聊天消息
const CHAT_DATA = 'chat_data';
//每次一次登录统计存储
const LOGIN_STATE = 'login_state';
//leancloud 节点存储
const LEANCLOUD_NODE = 'leancloud_node';
const TOURIST_CONFIG = 'tourist_config';
//new user
const NEW_USER_CONFIG = 'new_user_config';
//本地存储开发者config信息
const DEVELOPER_CONFIG = 'developer_config';
//本地存储广告信息
const NOTICE_VER_CONFIG='notice_ver_config';
//host
const HOST_URL = 'HOST_URL';
/**
 * 移除本地存储的用户信息
 */
const removeUserInfo = () => {
    try {
        //移除用户信息
        removeUserData();
        AsyncStorage.getAllKeys((error, array) => {
            if (!error) {
                if (array != null) {
                    for (var i = 0; i < array.length; i++) {
                        if (array[i] != PROMOTED_DATA && array[i] != COUNTRY_DATA && array[i] != LEANCLOUD_NODE && array[i] != TOURIST_CONFIG) {
                            if (array[i] != NEW_USER_CONFIG && array[i] != DEVELOPER_CONFIG&& array[i] != NOTICE_VER_CONFIG) {
                                if(array[i]!=HOST_URL){
                                    removeSystemMessage(array[i]);
                                }
                            }
                        }
                    }
                }
            }
        });
    } catch (error) {
        console.log('error' + error);
    }
};
//用户信息移除
const removeUserData = () => {
    try {
        AsyncStorage.setItem(
            USER_INFO,
            '',
            (error) => {
                if (error) {
                    rnRoNativeUtils.onStatistics(Constant.REMOVE_USER_ERROR);
                }
            }
        );
    } catch (error) {
        rnRoNativeUtils.onStatistics(Constant.REMOVE_USER_ERROR);
    }
}
/**
 * 保存本地的用户信息
 * @param userInfo
 */
const saveUserInfo = (userInfo) => {
    try {
        AsyncStorage.setItem(
            USER_INFO,
            userInfo,
            (error) => {
                if (error) {
                    rnRoNativeUtils.onStatistics(Constant.SAVE_USER_ERROR);
                }
            }
        );
    } catch (error) {
        rnRoNativeUtils.onStatistics(Constant.SAVE_USER_ERROR);
    }
};
/**
 * 标记是否是游客
 * @param tourist
 */
const signTourist = (tourist) => {
    try {
        AsyncStorage.setItem(
            TOURIST_DATA,
            tourist,
            (error) => {
                if (error) {
                    console.log('存值失败:', error)
                } else {
                    console.log('success!')
                }
            }
        );
    } catch (error) {
        console.log('失败' + error);
    }
};
const removeSignTourist = () => {
    try {
        AsyncStorage.setItem(
            TOURIST_DATA,
            '',
            (error) => {
                if (error) {
                    console.log('存值失败:', error)
                } else {
                    console.log('success!')
                }
            }
        );
    } catch (error) {
        console.log('失败' + error);
    }
};
/**
 *
 * @param id
 * @param time
 */
const saveSystemMessage = (id, message) => {
    try {
        AsyncStorage.setItem(
            SYSTEM_MESSAGE + id,
            message,
            (error) => {
                if (error) {
                    console.log('存值失败:', error)
                } else {
                    console.log('success!')
                }
            }
        );
    } catch (error) {
        console.log('失败' + error);
    }
}
/**
 *
 * @param id
 * @param message
 */
const mergeSystemMessage = (id, message) => {
    try {
        AsyncStorage.mergeItem(
            SYSTEM_MESSAGE + id,
            message,
            (error) => {
                if (error) {
                    console.log('存值失败:', error)
                } else {
                    console.log('success!')
                }
            }
        );
    } catch (error) {
        console.log('失败' + error);
    }
}
const removeSystemMessage = (key) => {
    try {
        AsyncStorage.setItem(
            key,
            '',
            (error) => {
                if (error) {
                    console.log('存值失败:', error)
                } else {
                    console.log('success')
                }
            }
        )
    } catch (error) {
        console.log('error');
    }
}
const updateGameInfo = (newData) => {
    try {
        AsyncStorage.getItem(
            USER_INFO,
            (error, result) => {
                if (error) {
                    console.log('取值失败:' + error);
                } else {
                    if (result != '' || result != null) {
                        let data = JSON.parse(result);
                        if (data != null) {
                            if (data.game != null && newData.game != null) {
                                data.game = newData.game;
                            }
                            if (newData.money) {
                                data.money = newData.money;
                            }
                            data.photos = newData.photos;
                            data.gift = newData.gift;
                            if (newData.image != null && newData.image != data.image) {
                                data.image = newData.image;
                            }
                            if (newData.name != null && newData.name != data.name) {
                                data.name = newData.name;
                            }
                            if (newData.sex != null && newData.sex != data.sex) {
                                data.sex = newData.sex;
                            }
                            if (newData.signature) {
                                data.signature = newData.signature;
                            } else {
                                data.signature = '';
                            }
                            if (newData.lc) {
                                data.lc = newData.lc
                            }
                            data.location = newData.location;
                            if (newData.role) {
                                data.role = newData.role;
                            }
                            if(newData.popular){
                                data.popular=newData.popular;
                            }
                            if(newData.group){
                                data.group=newData.group;
                            }
                            if(newData.uid){
                                data.uid=newData.uid;
                            }
                            if(newData.active){
                                data.active=newData.active;
                            }
                        }
                        saveUserInfo(JSON.stringify(data));
                    }
                    console.log('success!');
                }
            }
        )
    } catch (error) {
        console.log('失败' + error);
    }
}
/**
 * 存储选取的国家信息
 * @param countryData
 */
const saveCountryData = (countryData) => {
    try {
        AsyncStorage.setItem(
            COUNTRY_DATA,
            countryData,
            (error) => {
                if (error) {
                    console.log('error:', error)
                } else {
                    console.log('success!');
                }
            }
        );
    } catch (error) {
        console.log('error' + error);
    }
}
/**
 * 保存是否需要弹框评论
 * @param data
 */
const savePromotedData = (data) => {
    try {
        AsyncStorage.setItem(
            PROMOTED_DATA,
            data,
            (error) => {
                if (error) {
                    console.log('存值失败:', error)
                } else {
                    console.log('success')
                }
            }
        )
    } catch (error) {
    }
}
/**
 * 清除聊天消息
 * @param chatData
 */
const removeChatData = () => {
    try {
        AsyncStorage.setItem(
            CHAT_DATA,
            '',
            (error) => {
                if (error) {
                    console.log('存值失败:', error)
                } else {
                    console.log('success')
                }
            }
        )
    } catch (error) {
    }
}
/**
 * 保存登录状态
 *
 */
const saveLoginState = (time) => {
    try {
        AsyncStorage.setItem(
            LOGIN_STATE,
            time,
            (error) => {
                if (error) {
                    console.log('存值失败:', error)
                } else {
                    console.log('success')
                }
            }
        )
    } catch (error) {
    }
}
const saveNoticeData = (NoticeData) => {
    try {
        AsyncStorage.setItem(
            NOTICE_VER_CONFIG,
            NoticeData,
            (error) => {
                if (error) {
                    console.log('存值失败:', error)
                } else {
                    console.log('success')
                }
            }
        )
    } catch (error) {
    }
}
/**
 * 保存leancloud node
 *
 */
const saveLeanCloudNode = (node) => {
    try {
        AsyncStorage.setItem(
            LEANCLOUD_NODE,
            node,
            (error) => {
                if (error) {
                    console.log('err:', error)
                } else {
                    console.log('success')
                }
            }
        )
    } catch (error) {
    }
}
/**
 *
 * @param node
 */
const setTouristConfig = (data) => {
    try {
        AsyncStorage.setItem(
            TOURIST_CONFIG,
            data,
            (error) => {
                if (error) {
                    console.log('err:', error)
                } else {
                    console.log('success')
                }
            }
        )
    } catch (error) {
    }
}
/**
 *
 * @param node
 */
const setNewUserConfig = (data) => {
    try {
        AsyncStorage.setItem(
            NEW_USER_CONFIG,
            data,
            (error) => {
                if (error) {
                    console.log('err:', error)
                } else {
                    console.log('success')
                }
            }
        )
    } catch (error) {
    }
}
/**
 * 1--获取本地存储信息
 * 2--返回一个对象
 * @param key
 * @returns {*}
 */
const getStorageData = (key, callback) => {
    try {
        return AsyncStorage.getItem(
            key,
            (error, result) => {
                if (error) {
                    callback(null);
                } else {
                    let jsonValue = null;
                    if (result != '') {
                        jsonValue = JSON.parse(result);
                    }
                    callback(jsonValue);
                }
            }
        );
    } catch (error) {
        console.log('get storage error:' + error);
    }
}
/**
 * 存储开发者信息
 * @param config
 * config:{
 * location:cn,
 * developer:false
 * }
 */
const setDeveloperConfig=(config)=>{
    try {
        AsyncStorage.setItem(
            DEVELOPER_CONFIG,
            config,
            (error) => {
                if (error) {
                    console.log('err:', error)
                } else {
                    console.log('success')
                }
            }
        )
    } catch (error) {
    }
}
/**
 * 更新host
 * @param host
 */
const updateHost = (hostData, hostUrl) => {
    try {
        AsyncStorage.setItem(
            HOST_URL,
            hostData,
            (error) => {
                if (error) {
                    console.log('err:', error)
                } else {
                    setTimeout(() => {
                        rnRoNativeUtils.updateRequestUrl(hostUrl);
                    }, 1500);
                }
            }
        )
    } catch (error) {
        alert(error)
    }
}
export default {
    removeUserInfo,
    saveUserInfo,
    updateGameInfo,
    saveSystemMessage,
    removeSystemMessage,
    mergeSystemMessage,
    saveCountryData,
    signTourist,
    removeSignTourist,
    savePromotedData,
    removeChatData,
    saveLoginState,
    saveLeanCloudNode,
    getStorageData,
    setTouristConfig,
    setNewUserConfig,
    setDeveloperConfig,
    saveNoticeData,
    updateHost,
    USER_INFO,
    COUNTRY_DATA,
    TOURIST_DATA,
    PROMOTED_DATA,
    LOGIN_STATE,
    LEANCLOUD_NODE,
    TOURIST_CONFIG,
    NEW_USER_CONFIG,
    DEVELOPER_CONFIG,
    NOTICE_VER_CONFIG,
    HOST_URL
}

