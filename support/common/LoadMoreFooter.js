/**
 * Created by wangxu on 2017/7/14.
 */
import React, { Component } from 'react';
import Language from '../../resources/language'

import {
    ActivityIndicator,
    View,
    Text,
    StyleSheet,
} from 'react-native';

export default class LoadMoreFooter extends Component {
    static propTypes = {
        isNoMore: React.PropTypes.bool,
    }

    static defaultProps = {
        isNoMore: false
    }

    render() {
        const {isNoMore} = this.props
        const title = isNoMore ? Language().no_more_data : Language().fetch_more_data

        return (
            <View style={styles.loadingContainer}>
                {!isNoMore && <ActivityIndicator />}
                <Text style={styles.title}>{title}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loadingContainer: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        alignSelf:'center'
    },
    title: {
        fontSize: 14,
        marginLeft: 5,
        color: 'gray'
    }
})