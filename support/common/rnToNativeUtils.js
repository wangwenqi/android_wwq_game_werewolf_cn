/**
 * Created by wangxu on 2017/5/9.
 */
'use strict';
import React, {Platform, NativeModules, AsyncStorage} from 'react-native';
import AsyncStorageTool from './AsyncStorageTool';
import * as Constant from './constant';
import * as types from '../actions/actionTypes';
import UtilsTool from './UtilsTool';

const main = NativeModules.MainViewController;
const androidNative = NativeModules.NativeJSModule;

const initUserInfoToNative = (userId, userName, userSex, userImage, token) => {
    if (userSex != 1 && userSex != 2) {
        userSex = 2;
    }
    if (userImage == null) {
        userImage = '';
    }
    try {
        AsyncStorageTool.getStorageData(AsyncStorageTool.TOURIST_DATA, (data) => {
            // console.log('本地存储游客：' + JSON.stringify(data));
            let isTourist = false;
            if (data != null) {
                isTourist = data.isTourist;
            }
            if (Platform.OS === 'ios') {
                if (main && main.loginSuccess) {
                    main.loginSuccess(userId, userName, userSex, userImage, token, isTourist);
                }
            } else {
                if (androidNative && androidNative.initLeanCloud) {
                    androidNative.initLeanCloud(token, isTourist, userId, userName, userSex, userImage);
                }
            }
        });
    } catch (error) {

    }
}
const closeLeanCloud = (userId) => {
    try {
        if (Platform.OS === 'android') {
            if (androidNative && androidNative.closeLeanCloud) {
                androidNative.closeLeanCloud(userId);
            }
            clearAllNotification();
        }
    } catch (e) {

    }
}
const onShareMessage = (name) => {
    try {
        if (Platform.OS === 'ios') {
            //var main = NativeModules.MainViewController;
            if (main && main.share) {
                main.share();
            }
        } else {
            if (androidNative && androidNative.rnShareMessage) {
                androidNative.rnShareMessage(name);
            }
        }
    } catch (e) {

    }
}
const onApplicationMarket = () => {
    try {
        if (Platform.OS === 'ios') {
            // var main = NativeModules.MainViewController;
            if (main && main.appStoreReviewAndRating) {
                main.appStoreReviewAndRating();
            }
        } else {
            if (androidNative && androidNative.launchAppDetail) {
                androidNative.launchAppDetail('');
            }
        }
        onStatistics(Constant.APPLICATION_MARKET);
    } catch (e) {

    }
}
const onCheckUpgrade = (initiative) => {
    try {
        if (Platform.OS === 'ios') {
            //var main = NativeModules.MainViewController;
            if (main && main.checkUpdate) {
                main.checkUpdate();
            }
        } else {
            if (androidNative && androidNative.checkUpgrade) {
                androidNative.checkUpgrade(initiative);
            }
        }
        onStatistics(Constant.UPGRADE);
    } catch (e) {

    }
}
const onFeedback = () => {
    try {
        if (Platform.OS === 'ios') {
            // var main = NativeModules.MainViewController;
            if (main && main.feedBack) {
                main.feedBack();
            }
        } else {
            if (androidNative && androidNative.launchFeedBack) {
                androidNative.launchFeedBack();
            }
        }
        onStatistics(Constant.FEEDBACK);
    } catch (e) {
        console.log('onFeedback error:' + JSON.stringify(e));
    }
}
const onStatistics = (eventKey) => {
    try {
        if (Platform.OS === 'ios') {
            //var main = NativeModules.MainViewController;
            if (main && main.mobAnalytics) {
                main.mobAnalytics(eventKey);
            }
        } else {
            if (androidNative && androidNative.uMengReport) {
                androidNative.uMengReport(eventKey);
            }
        }
    } catch (e) {
    }
}
const onErrorReport = (type, error) => {
    try {
        if (Platform.OS === 'ios') {

        } else {
            if (androidNative && androidNative.uMengErrorReport) {
                androidNative.uMengErrorReport(type, error);
            }
        }
    } catch (e) {
    }
}
const openBox = () => {
    try {
        if (Platform.OS === 'ios') {
            if (main && main.gameAndMissionCenter) {
                main.gameAndMissionCenter();
            }
        } else {
            if (androidNative && androidNative.openTreasureBox) {
                androidNative.openTreasureBox();
            }
        }
    } catch (e) {
    }
}
const enterExhibitionRecord = (userId) => {
    try {
        if (Platform.OS === 'ios') {
            if (main && main.showAllGifts) {
                main.showAllGifts(userId);
            }
        } else {
            if (androidNative && androidNative.enterExhibitionRecord) {
                androidNative.enterExhibitionRecord(userId);
            }
        }
    } catch (e) {
    }
}
const onReactNativeInitDone = () => {
    try {
        if (Platform.OS === 'ios') {
            if (main && main.checkServiceRegion) {
                main.checkServiceRegion();
            }
        } else {
            if (androidNative && androidNative.reactNativeInitDone) {
                androidNative.reactNativeInitDone();
            }
        }
    } catch (e) {
    }
}
const onApplyPermission = () => {
    try {
        if (Platform.OS === 'ios') {
            if (main && main.checkPermission) {
                main.checkPermission();
            }
        } else {
            //TODO android 暂时不申请权限
            // if (androidNative && androidNative.applyPermission) {
            //     androidNative.applyPermission();
            // }
        }
    } catch (e) {
    }
}
/**
 * 设置分区
 * @param location
 */
const onSetLocation = (location) => {
    try {
        if (location) {
            if (Platform.OS === 'ios') {
                if (main && main.setLocation) {
                    main.setLocation(location);
                }
            } else {
                if (androidNative && androidNative.setLocation) {
                    androidNative.setLocation(location);
                }
            }
            // * config:{
            //     * location:cn,
            //         * }
            //存储到本地
            let developerConfig = {
                location: location,
            }
            AsyncStorageTool.setDeveloperConfig(JSON.stringify(developerConfig));
        }
    } catch (e) {
    }
}
/**
 * android stop load adv
 */
const onStartLoadAv = () => {
    if (Platform.OS === 'android') {
        if (androidNative && androidNative.startLoadAv) {
            androidNative.startLoadAv();
        }
    }
}
const onStopLoadAv = () => {
    if (Platform.OS === 'android') {
        if (androidNative && androidNative.stopLoadAv) {
            androidNative.stopLoadAv();
        }
    }
}
/**
 * 开启应用商城
 */
const onOpenStore = () => {
    if (Platform.OS === 'ios') {
        if (main && main.onOpenStore) {
            main.onOpenStore();
        }
    } else {
        if (androidNative && androidNative.openStore) {
            androidNative.openStore();
        }
    }
}
/**
 *  android清理通知栏信息
 */
const clearAllNotification = () => {
    if (Platform.OS === 'android') {
        if (androidNative && androidNative.clearAllNotification) {
            androidNative.clearAllNotification();
        }
    }
}
/**
 * 支付常见问题
 */
const openPayFAQ = () => {
    if (Platform.OS === 'ios') {
        if (main && main.openPayFAQ) {
            main.openPayFAQ();
        }
    } else {
        if (androidNative && androidNative.openPayFAQ) {
            androidNative.openPayFAQ();
        }
    }
}
/**
 * 充值页面
 */
const openStoreRecharge = () => {
    if (Platform.OS === 'ios') {
        if (main && main.openStoreRecharge) {
            main.openStoreRecharge();
        }
    } else {
        if (androidNative && androidNative.openStoreRecharge) {
            androidNative.openStoreRecharge();
        }
    }
}
/**
 * 进入个性房间
 */
const openCallUp = () => {
    if (Platform.OS === 'ios') {
        if (main && main.openCallUp) {
            main.openCallUp();
        }
    } else {
        if (androidNative && androidNative.openCallUp) {
            androidNative.openCallUp();
        }
    }
}

export const sendMessageToNative = (json) => {
    if (Platform.OS === 'ios') {
        if (main && main.sendMessageToNative) {
            main.sendMessageToNative(json);
        }
    } else {
        if (androidNative && androidNative.sendMessageToNative) {
            androidNative.sendMessageToNative(json);
        }
    }
}
export const sendMessageToNativeNew = (json, sendSuccess, sendFail) => {
    if (Platform.OS === 'ios') {
        if (main && main.sendMessageToNative) {
            main.sendMessageToNative(json);
            sendSuccess();
        } else {
            sendFail();
        }
    } else {
        if (androidNative && androidNative.sendMessageToNative) {
            androidNative.sendMessageToNative(json);
            sendSuccess();
        } else {
            sendFail();
        }
    }
}
export const openLeaderboard = () => {
    if (Platform.OS === 'ios') {
        if (main && main.openLeaderboard) {
            main.openLeaderboard();
        }
    } else {
        if (androidNative && androidNative.openLeaderboard) {
            androidNative.openLeaderboard();
        }
    }
}
export const uploadFile = (imagePathArray, action, timeOut) => {
    let options = {
        needcallback: true,
        timeout: timeOut
    }
    let params = {
        path: imagePathArray,
        platform: "qiniu"
    }
    let nativeData = {
        action: action,
        type: Constant.RN_NATIVE,
        options: options,
        params: params
    }
    sendMessageToNative(JSON.stringify(nativeData));
}
export const initFamilyGroup = (group) => {
    let options = {
        needcallback: false,
        timeout: 0,
        sync: false,
    }
    let params = {
        group: group,
    }
    let nativeData = {
        action: Constant.ACTION_FAMILY_INFO_INIT,
        type: Constant.RN_NATIVE,
        options: options,
        params: params
    }
    sendMessageToNative(JSON.stringify(nativeData));
}
export const getGiftInfo = (gifts, callBack) => {
    try {
        if (Platform.OS === 'ios') {
            if (main && main.getGiftInfo) {
                let newGiftList = new Array();
                let count = 0;
                for (let i = 0; gifts[i]; i += 1) {
                    let giftType = gifts[i].type;
                    main.getGiftInfo(giftType, (error, success) => {
                        if (!error) {
                            let updateGift = JSON.parse(success);
                            if (updateGift.gift_image && updateGift.gift_image != Constant.giftLoadTypeLocal) {
                                updateGift.load_type = Constant.giftLoadTypeCache;
                                updateGift.type = giftType;
                                newGiftList.push(updateGift);
                            }
                        }
                        count++;
                        if (count == gifts.length) {
                            callBack(newGiftList);
                        }
                    });
                }
            }
        } else {
            if (androidNative && androidNative.getGiftInfo) {
                let newGiftList = new Array();
                let count = 0;
                for (let i = 0; gifts[i]; i += 1) {
                    let giftType = gifts[i].type;
                    androidNative.getGiftInfo(giftType, (success) => {
                        let updateGift = JSON.parse(success);
                        if (updateGift.gift_image && updateGift.gift_image != Constant.giftLoadTypeLocal) {
                            updateGift.load_type = Constant.giftLoadTypeCache;
                            UtilsTool.regroupFilePath(updateGift.gift_image, (newPath) => {
                                updateGift.gift_image = newPath;
                                updateGift.type = giftType;
                                newGiftList.push(updateGift);
                            });
                        }
                        count++;
                        if (count == gifts.length) {
                            callBack(newGiftList);
                        }
                    });
                }
            }
        }
    } catch (e) {
        alert(e);
        callBack(new Array());
    }

}
export const getDeviceId = (success) => {
    let deviceID = '';
    if (Platform.OS === 'ios') {
        if (main && main.getDeviceId) {
            main.getDeviceId((error, did) => {
                if (!error) {
                    deviceID = did;
                    success(deviceID);
                }
            });
        }
    } else {
        if (androidNative && androidNative.getDeviceId) {
            androidNative.getDeviceId((did) => {
                deviceID = did;
                success(deviceID);
            });
        }
    }
}
export const getDevicePid = (success) => {
    let devicePid = '';
    if (Platform.OS === 'android') {
        if (androidNative && androidNative.getUniquePsuedoID) {
            androidNative.getUniquePsuedoID((pid) => {
                devicePid = pid;
                success(devicePid);
            });
        } else {
            success(devicePid);
        }
    } else {
        success(devicePid);
    }
}
export const openWebView = (url) => {
    if (Platform.OS === 'ios') {
        if (main && main.openWebView) {
            main.openWebView(url);
        }
    } else {
        if (androidNative && androidNative.openWebView) {
            androidNative.openWebView(url);
        }
    }
}
export const updateRequestUrl = (hostUrl) => {
    if (Platform.OS === 'ios') {
        if (main && main.updateRequestUrl) {
            main.updateRequestUrl(hostUrl);
        }
    } else {
        if (androidNative && androidNative.updateRequestUrl) {
            androidNative.updateRequestUrl(hostUrl, hostUrl);
        }
    }
}
/**
 * 匹配小游戏
 * @param gameType
 */
export const rnStartMiniGame = (gameType) => {
    if (Platform.OS === 'ios') {
        if (main && main.rnStartMiniGame) {
            main.rnStartMiniGame(gameType);
        }
    } else {
        if (androidNative && androidNative.rnStartMiniGame) {
            androidNative.rnStartMiniGame(gameType);
        }
    }
}
/**
 * 邀请
 * @param title
 * @param message
 * @param url
 */
export const rnStartMiniGameIntviu = (title, message, url) => {
    if (Platform.OS === 'ios') {
        if (main && main.rnStartMiniGameIntviu) {
            main.rnStartMiniGameIntviu(title, message, url);
        }
    } else {
        if (androidNative && androidNative.rnStartMiniGameIntviu) {
            androidNative.rnStartMiniGameIntviu(title, message, url);
        }
    }
}
export const openChatMessage = (leftDate, rightData) => {
    let conversation_id = '';
    let leftSex = 1;
    let RightSex = 1;
    if (leftDate.sex) {
        leftSex = leftDate.sex;
    }
    if (rightData.sex) {
        RightSex = rightData.sex;
    }
    if (leftDate.conversation_id) {
        conversation_id = leftDate.conversation_id;
    }
    if (Platform.OS === 'ios') {
        if (main && main.didSelectUser) {
            main.didSelectUser(leftDate.id, 'chat', leftSex + '', leftDate.image, leftDate.name, conversation_id);
        }
    } else {
        if (androidNative && androidNative.startChat) {
            androidNative.startChat(conversation_id, leftDate.id, leftDate.name, leftSex, leftDate.image, rightData.id, rightData.name, RightSex, rightData.image);

        }
    }
}
export default{
    initUserInfoToNative,
    closeLeanCloud,
    onShareMessage,
    onApplicationMarket,
    onFeedback,
    onCheckUpgrade,
    onStatistics,
    onErrorReport,
    openBox,
    enterExhibitionRecord,
    onReactNativeInitDone,
    onApplyPermission,
    onSetLocation,
    onStartLoadAv,
    onStopLoadAv,
    onOpenStore,
    openPayFAQ,
    openStoreRecharge,
    openCallUp,
    sendMessageToNative,
    sendMessageToNativeNew,
    openLeaderboard,
    uploadFile,
    initFamilyGroup,
    getDeviceId,
    getGiftInfo,
    openWebView,
    updateRequestUrl,
    rnStartMiniGame,
    rnStartMiniGameIntviu,
    openChatMessage,
    getDevicePid
}
