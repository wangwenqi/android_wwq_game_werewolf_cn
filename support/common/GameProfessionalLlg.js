/**
 * Created by wangxu on 2017/6/1.
 */
'use strict';

import React, {Component} from 'react';
import Color from '../../resources/themColor';
import Language from '../../resources/language';
import {connect} from 'react-redux';

import {
    StyleSheet, View, TouchableOpacity, Text, Image, Platform, Dimensions, ScrollView
} from 'react-native';
const ScreenWidth = Dimensions.get('window').width;
class GameProfessionalLlg extends Component {
    componentDidMount() {
    }

    render() {
        return (
            <View style={styles.view}>
                <ScrollView alwaysBounceVertical={false} showsVerticalScrollIndicator={false}>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().tucheng}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().tucheng_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().tubian}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().tubian_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().cha_sha}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().cha_sha_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().dang_dao}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().dang_dao_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().han_tiao}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().han_tiao_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().dui_tiao}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().dui_tiao_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().ren_chu}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().ren_chu_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().jin_shui}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().jin_shui_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().yin_shui}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().yin_shui_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().tui_shui}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().tui_shui_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().pai_shui}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().pai_shui_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().liao_bao}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().liao_bao_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().kang_tui}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().kang_tui_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().lao}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().lao_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().gui_piao}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().gui_piao_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().dao_fa}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().dao_fa_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().chong_fen_werewolf}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().chong_fen_werewolf_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().shenshui_werewolf}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().shenshui_werewolf_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().daogou_werewolf}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().daogou_werewolf_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().jingang_werewolf}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().jingang_werewolf_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().kongshou}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().kongshou_content}</Text>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <View flexDirection='row' style={{marginTop:3}}>
                            <Text style={{color:'#9ec3f5',marginLeft:20,marginRight:10}}>{Language().zibao}</Text>
                            <Text style={{color:Color.colorWhite,marginRight:10,flex:1}}>{Language().zibao_centetn}</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        margin: 15,
        borderRadius: 3,
        backgroundColor: '#6614bc',
        flex: 1, padding: 10
    },
});

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps,)(GameProfessionalLlg);
