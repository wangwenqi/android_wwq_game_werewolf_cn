/**
 * Created by wangxu on 2017/6/1.
 */
'use strict';

import React, {Component} from 'react';
import Color from '../../resources/themColor';
import Language from '../../resources/language';
import {connect} from 'react-redux';

import {
    StyleSheet, View, TouchableOpacity, Text, Image, Platform, Dimensions, ScrollView
} from 'react-native';
const ScreenWidth = Dimensions.get('window').width;
const lines = require('../../resources/imgs/ic_lines.png');
const dayTime = require('../../resources/imgs/ic_day_time.png');
const night = require('../../resources/imgs/ic_night.png');
const alternateL = require('../../resources/imgs/ic_alternate_l.png');
const alternateR = require('../../resources/imgs/ic_alternate_r.png');
class GameRule extends Component {
    componentDidMount() {
    }

    render() {
        return (
            <View style={styles.view}>
                <ScrollView alwaysBounceVertical={false} showsVerticalScrollIndicator={false}>
                    <View flexDirection='row'>
                        <Image style={styles.lines} source={lines}/>
                        <Text
                            style={{flex:1,alignSelf:'center',fontSize:16,color:Color.colorWhite,textAlign:'center',fontWeight: 'bold'}}>{Language().game_flow}</Text>
                        <Image style={styles.lines} source={lines}/>
                    </View>
                    <View flexDirection='row' style={{marginTop:10}}>
                        <View flexDirection='row' style={{flex:1.5,justifyContent:'center',height:30}}>
                            <Text style={{color:'#fedd59',padding:5}}>{Language().game_night}</Text>
                            <Image style={styles.icon} source={night}/>
                        </View>
                        <Text
                            style={{color:Color.colorWhite,padding:5,marginRight:10,flex:4}}>{Language().game_night_detail}</Text>
                    </View>
                    <View flexDirection='row'>
                        <View flexDirection='row' style={{flex:1.5,justifyContent:'center',height:30}}>
                            <Text style={{color:'#a2eaff',padding:5}}>{Language().game_day}</Text>
                            <Image style={styles.icon} source={dayTime}/>
                        </View>
                        <Text
                            style={{color:Color.colorWhite,padding:5,marginRight:10,flex:4}}>{Language().game_day_detail}</Text>
                    </View>
                    <View flexDirection='row' style={{justifyContent:'center',marginTop:10}}>
                        <Image style={styles.alternate} source={alternateL}/>
                        <Text
                            style={{alignSelf:'center',fontSize:14,color:'#d5b3ff',textAlign:'center'}}>{Language().game_day_night}</Text>
                        <Image style={styles.alternate} source={alternateR}/>
                    </View>
                    <View flexDirection='row' style={{marginTop:25}}>
                        <Image style={styles.lines} source={lines}/>
                        <Text
                            style={{flex:1,alignSelf:'center',fontSize:16,color:Color.colorWhite,textAlign:'center',fontWeight: 'bold'}}>{Language().multiple_rounds}</Text>
                        <Image style={styles.lines} source={lines}/>
                    </View>
                    <View flexDirection='row' style={{marginTop:15}}>
                        <View flexDirection='row' style={{flex:1.5,justifyContent:'center',height:20}}>
                            <View style={{backgroundColor:'#db86ff',borderRadius:3,padding:2,justifyContent:'center'}}>
                                <Text style={{color:'#31184e',textAlign:'center'}}>{Language().tu_cheng}</Text>
                            </View>
                        </View>
                        <View style={{marginRight:10,flex:4}}>
                            <Text style={{color:Color.colorWhite}}>{Language().tu_cheng_rule1}</Text>
                            <Text style={{color:Color.colorWhite}}>{Language().tu_cheng_rule2}</Text>
                        </View>
                    </View>
                    <View flexDirection='row' style={{marginTop:15}}>
                        <View flexDirection='row' style={{flex:1.5,justifyContent:'center',height:20}}>
                            <View style={{backgroundColor:'#FFEA31',borderRadius:3,padding:2,justifyContent:'center'}}>
                                <Text style={{color:'#31184e',textAlign:'center'}}>{Language().tu_bian}</Text>
                            </View>
                        </View>
                        <View style={{marginRight:10,flex:4}}>
                            <Text style={{color:Color.colorWhite}}>{Language().tu_bian_rule1}</Text>
                            <Text style={{color:Color.colorWhite}}>{Language().tu_bian_rule2}</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        margin: 15,
        borderRadius: 3,
        backgroundColor: '#6614bc',
        flex: 1, padding: 5
    },
    lines: {
        height: 5,
        resizeMode: 'contain',
        flex: 1, alignSelf: 'center'
    },
    icon: {
        width: 15,
        resizeMode: 'contain',
        height: 15,
        alignSelf: 'center'
    },
    alternate: {
        height: 7,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
});

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps,)(GameRule);
