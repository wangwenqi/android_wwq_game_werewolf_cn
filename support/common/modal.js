/**
 * Created by wangxu on 2017/3/6.
 */
import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View, Button, Modal, ActivityIndicator} from 'react-native';

export class CustomModal extends React.Component {
    updateVisibleStateTimer;

    constructor(props) {
        super();
        this.state = {isVisible: props.visible};
    }

    componentWillReceiveProps(nextProps) {
        if (this.updateVisibleStateTimer !== null) {
            clearTimeout(this.updateVisibleStateTimer);
        }

        this.updateVisibleStateTimer = setTimeout(this.updateVisibleState, 100);
    }

    componentWillUnmount() {
        if (this.updateVisibleStateTimer !== null) {
            clearTimeout(this.updateVisibleStateTimer);
        }
    }

    render() {
        let props = this.props;

        return (
            <Modal {...props} ref={undefined} visible={this.state.isVisible}>
                {props.children}
            </Modal>
        )
    }

    updateVisibleState = () => {
        this.setState({isVisible: this.props.visible});
        this.updateVisibleStateTimer = null;
    }
}
