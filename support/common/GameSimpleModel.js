/**
 * Created by wangxu on 2017/6/1.
 */
'use strict';

import React, {Component} from 'react';
import Color from '../../resources/themColor';
import Language from '../../resources/language';
import {connect} from 'react-redux';

import {
    StyleSheet, View, TouchableOpacity, Text, Image, Platform, Dimensions, ScrollView
} from 'react-native';
const ScreenWidth = Dimensions.get('window').width;
const lines = require('../../resources/imgs/ic_lines.png');
class GameSimpleModel extends Component {
    componentDidMount() {
    }

    render() {
        return (
            <View style={styles.view}>
                <ScrollView alwaysBounceVertical={false} showsVerticalScrollIndicator={false}>
                    <View flexDirection='row'>
                        <Image style={styles.lines} source={lines}/>
                        <Text
                            style={{flex:1.5,alignSelf:'center',fontSize:15,color:Color.colorWhite,textAlign:'center',fontWeight: 'bold'}}>{Language().game_model_novice}</Text>
                        <Image style={styles.lines} source={lines}/>
                    </View>
                    <View style={styles.itemView}>
                        <Text style={{color:Color.colorWhite}}>{Language().simple_6} </Text>
                        <Text style={{color:Color.colorWhite}}>{Language().simple_6_rule1}</Text>
                        <Text style={{color:Color.colorWhite}}>{Language().simple_6_rule2}</Text>
                        <View flexDirection='row' style={{marginTop:5,marginBottom:5}}>
                            <Text
                                style={{color:'#fffd45',padding:1,flex:1,fontSize:12}}>{Language().simple_6_rule3}</Text>
                            <View style={{backgroundColor:'#ffc000',padding:1,borderRadius:3}}>
                                <Text style={{fontSize:12}}>{Language().tu_cheng}</Text>
                            </View>
                        </View>
                    </View>
                    <View flexDirection='row'>
                        <Image style={styles.lines} source={lines}/>
                        <Text
                            style={{flex:1.5,alignSelf:'center',fontSize:15,color:Color.colorWhite,textAlign:'center',fontWeight: 'bold'}}>{Language().game_model_simple}</Text>
                        <Image style={styles.lines} source={lines}/>
                    </View>
                    <View style={styles.itemView}>
                        <Text style={{color:Color.colorWhite}}>{Language().game_simple_6} </Text>
                        <Text style={{color:Color.colorWhite}}>{Language().game_simple_6_rule1}</Text>
                        <Text style={{color:Color.colorWhite}}>{Language().game_simple_6_rule2}</Text>
                        <View flexDirection='row' style={{marginTop:5,marginBottom:5}}>
                            <Text
                                style={{color:'#fffd45',padding:1,flex:1,fontSize:12}}>{Language().game_simple_6_rule3}</Text>
                            <View style={{backgroundColor:'#ffc000',padding:1,borderRadius:3}}>
                                <Text style={{fontSize:12}}>{Language().tu_cheng}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.itemView}>
                        <Text style={{color:Color.colorWhite}}>{Language().game_simple_9} </Text>
                        <Text style={{color:Color.colorWhite}}>{Language().game_simple_9_rule1}</Text>
                        <Text style={{color:Color.colorWhite}}>{Language().game_simple_9_rule2}</Text>
                        <View flexDirection='row' style={{marginTop:5,marginBottom:5}}>
                            <Text
                                style={{color:'#fffd45',padding:1,flex:1,fontSize:12}}>{Language().game_simple_9_rule3}</Text>
                            <View style={{backgroundColor:'#a5d4fc',padding:1,borderRadius:3}}>
                                <Text style={{fontSize:12}}>{Language().tu_bian}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.itemView}>
                        <Text style={{color:Color.colorWhite}}>{Language().game_simple_10} </Text>
                        <Text style={{color:Color.colorWhite}}>{Language().game_simple_10_rule1}</Text>
                        <Text style={{color:Color.colorWhite}}>{Language().game_simple_10_rule2}</Text>
                        <View flexDirection='row' style={{marginTop:5,marginBottom:5}}>
                            <Text
                                style={{color:'#fffd45',padding:1,flex:1,fontSize:12}}>{Language().game_simple_10_rule3}</Text>
                            <View style={{backgroundColor:'#ffc000',padding:1,borderRadius:3}}>
                                <Text style={{fontSize:12}}>{Language().tu_cheng}</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        margin: 15,
        borderRadius: 3,
        backgroundColor: '#6614bc',
        flex: 1, padding: 5
    },
    lines: {
        height: 5,
        resizeMode: 'contain',
        flex: 1, alignSelf: 'center'
    },
    itemView: {
        borderRadius: 5,
        margin: 10,
        borderColor: '#b87fff',
        borderWidth: 1, padding: 5
    },
});

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps,)(GameSimpleModel);
