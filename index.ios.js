/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import App from './js/index';

import React from 'react';
const werewolf = 'game_werewolf';
import  {
    AppRegistry,
} from 'react-native';

AppRegistry.registerComponent(werewolf, () => App);